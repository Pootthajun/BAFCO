﻿Partial Class ViewDocument
    Inherits System.Web.UI.Page

    Private Property JobID() As Integer
        Get
            If IsNumeric(ViewState("JobID")) Then
                Return ViewState("JobID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("JobID") = value
        End Set
    End Property

    Private Property DocumentType() As Integer
        Get
            If IsNumeric(ViewState("DocumentType")) Then
                Return ViewState("DocumentType")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DocumentType") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Panel.Enabled = False
            JobID = Request.QueryString("JobID").Replace("?", "")
            DocumentType = Request.QueryString("DocumentType").Replace("?", "")

        End If
    End Sub

End Class

﻿
Imports System.Data

Imports System.Data.SqlClient
Partial Class Search
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        If Not IsPostBack Then
            MC.BindDDlStatus(ddlStatus, "--- All ---")
            MC.BindDDlDepartment(ddlDepartment, "--- All ---")
            MC.BindDDlCustomerFromUpload(ddlCustomer, "--- All ---")
            trJobStatus.Visible = True
            trJobAnalysis.Visible = False
            ClearData()
        End If
    End Sub

    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT * FROM (" & vbCrLf
        SQL &= "SELECT REF_ID,NULL AS REF_NO,HD.JOB_ID,HD.JOB_NO,HD.CUSTOMER,DEPARTMENT_ID,DEPARTMENT_NAME,HD.DOC_TYPE_ID,DOC_TYPE_NAME,HD.STATUS," & vbCrLf
        SQL &= "HD.OPEN_DATE,HD.CLOSE_DATE,DATEDIFF(D,HD.OPEN_DATE,HD.CLOSE_DATE) AS OPEN_CLOSE," & vbCrLf
        SQL &= "NULL AS SUBMIT_APPROVE_DATE,NULL AS CLOSE_SUBMIT," & vbCrLf
        SQL &= "NULL AS RECEIVE_DATE,NULL AS SUBMIT_RECEIVE," & vbCrLf
        SQL &= "NULL AS CHECK_DATE,NULL AS RECEIVE_CHECK," & vbCrLf
        SQL &= "NULL AS PRICE_DATE,NULL AS CHECK_PRICE," & vbCrLf
        SQL &= "NULL AS APP_DATE,NULL AS PRICE_APPROVE," & vbCrLf
        SQL &= "NULL AS BILL_DATE,NULL AS APPROVE_BILL," & vbCrLf
        SQL &= "NULL AS COMPLETE_DATE,NULL AS BILL_COMPLETE," & vbCrLf
        SQL &= "NULL AS INCOMPLETE_DATE,NULL AS INCOMPLETE," & vbCrLf
        SQL &= "NULL AS HOLD_DATE,NULL AS HOLD,NULL AS FOC_DATE," & vbCrLf
        SQL &= "DATEDIFF(D,HD.OPEN_DATE,GETDATE()) AS SUM_DATE" & vbCrLf
        SQL &= "FROM JOB_HEADER HD LEFT JOIN MS_DOC_TYPE ON HD.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "LEFT JOIN REF_DETAIL DT ON HD.JOB_ID = DT.JOB_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_DEPARTMENT DPT ON HD.DEPARTMENT = DPT.DEPARTMENT_FWP" & vbCrLf
        SQL &= "WHERE REF_ID IS NULL" & vbCrLf

        SQL &= "UNION ALL" & vbCrLf

        SQL &= "SELECT HD.REF_ID,REF_NO,DT.JOB_ID,JOB_NO,CUSTOMER,DEPARTMENT_ID,DEPARTMENT_NAME,DT.DOC_TYPE_ID,DOC_TYPE_NAME,DT.STATUS," & vbCrLf
        SQL &= "OPEN_DATE,DT.CLOSE_DATE,DATEDIFF(D,OPEN_DATE,DT.CLOSE_DATE) AS OPEN_CLOSE," & vbCrLf
        SQL &= "DT.SUBMIT_APPROVE_DATE,DATEDIFF(D,DT.CLOSE_DATE,DT.SUBMIT_APPROVE_DATE) AS CLOSE_SUBMIT," & vbCrLf
        SQL &= "DT.RECEIVE_DATE,DATEDIFF(D,DT.SUBMIT_APPROVE_DATE,DT.RECEIVE_DATE) AS SUBMIT_RECEIVE," & vbCrLf
        SQL &= "DT.CHECK_DATE,DATEDIFF(D,DT.RECEIVE_DATE,DT.CHECK_DATE) AS RECEIVE_CHECK," & vbCrLf
        SQL &= "DT.PRICE_DATE,DATEDIFF(D,DT.CHECK_DATE,DT.PRICE_DATE) AS CHECK_PRICE," & vbCrLf
        SQL &= "DT.APP_DATE,DATEDIFF(D,DT.PRICE_DATE,DT.APP_DATE) AS PRICE_APPROVE," & vbCrLf
        SQL &= "DT.BILL_DATE,DATEDIFF(D,DT.APP_DATE,DT.BILL_DATE) AS APPROVE_BILL," & vbCrLf
        SQL &= "DT.COMPLETE_DATE,DATEDIFF(D,DT.BILL_DATE,DT.COMPLETE_DATE) AS BILL_COMPLETE," & vbCrLf
        SQL &= "CASE WHEN JOB_STATUS = 2 THEN HIS.CLOSE_DATE ELSE NULL END INCOMPLETE_DATE,CASE WHEN JOB_STATUS = 2 THEN HIS_DAYS ELSE NULL END INCOMPLETE," & vbCrLf
        SQL &= "CASE WHEN JOB_STATUS = 5 THEN HIS.CLOSE_DATE ELSE NULL END HOLD_DATE,CASE WHEN JOB_STATUS = 5 THEN HIS_DAYS ELSE NULL END HOLD," & vbCrLf
        SQL &= "CASE WHEN JOB_STATUS = 4 THEN HIS.CLOSE_DATE ELSE NULL END FOC_DATE," & vbCrLf
        SQL &= "DATEDIFF(D,OPEN_DATE,GETDATE()) AS SUM_DATE" & vbCrLf
        SQL &= "FROM REF_HEADER HD LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_DOC_TYPE ON DT.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_DEPARTMENT DPT ON HD.REF_FROM = DPT.DEPARTMENT_ID" & vbCrLf
        SQL &= "LEFT JOIN " & vbCrLf
        SQL &= "(" & vbCrLf
        SQL &= "SELECT REF_ID,JOB_ID,CLOSE_DATE,DATEDIFF(D,CLOSE_DATE,GETDATE()) AS HIS_DAYS,JOB_STATUS FROM" & vbCrLf
        SQL &= "(SELECT ROW_NUMBER() OVER(PARTITION BY REF_ID,JOB_ID ORDER BY DETAIL_ID DESC ) AS Row,* FROM" & vbCrLf
        SQL &= "(SELECT DETAIL_ID,REF_ID,JOB_ID,CLOSE_DATE,JOB_STATUS FROM JOB_HISTORY WHERE JOB_STATUS IN (2,4,5) GROUP BY DETAIL_ID,REF_ID,JOB_ID,CLOSE_DATE,JOB_STATUS) TB1) TB2" & vbCrLf
        SQL &= "WHERE Row = 1" & vbCrLf
        SQL &= ") HIS ON DT.REF_ID = HIS.REF_ID AND DT.JOB_ID = HIS.JOB_ID" & vbCrLf
        SQL &= ") TB" & vbCrLf
        SQL &= "WHERE 1=1 AND "

        If txtRefNo.Text <> "" Then
            SQL &= "REF_NO like '%" & txtRefNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtJobNo.Text <> "" Then
            SQL &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If ddlStatus.SelectedIndex > 0 Then
            SQL &= "STATUS = " & ddlStatus.SelectedValue.Replace("'", "''") & " AND "
        End If
        If ddlDepartment.SelectedIndex > 0 Then
            SQL &= "DEPARTMENT_ID = " & ddlDepartment.SelectedValue.Replace("'", "''") & " AND "
        End If
        If ddlCustomer.SelectedIndex > 0 Then
            SQL &= "CUSTOMER = '" & ddlCustomer.SelectedValue.Replace("'", "''") & "' AND "
        End If
        If txtOpenDateFrom.Text.Trim <> "" Then
            If IsDate(txtOpenDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),OPEN_DATE,112) >= '" & txtOpenDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtOpenDateTo.Text.Trim <> "" Then
            If IsDate(txtOpenDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),OPEN_DATE,112) <= '" & txtOpenDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtCloseDateFrom.Text.Trim <> "" Then
            If IsDate(txtCloseDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),CLOSE_DATE,112) >= '" & txtCloseDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtCloseDateTo.Text.Trim <> "" Then
            If IsDate(txtCloseDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),CLOSE_DATE,112) <= '" & txtCloseDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtSubmitDateFrom.Text.Trim <> "" Then
            If IsDate(txtSubmitDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),SUBMIT_APPROVE_DATE,112) >= '" & txtSubmitDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtSubmitDateTo.Text.Trim <> "" Then
            If IsDate(txtSubmitDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),SUBMIT_APPROVE_DATE,112) <= '" & txtSubmitDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtReceiveDateFrom.Text.Trim <> "" Then
            If IsDate(txtReceiveDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),RECEIVE_DATE,112) >= '" & txtReceiveDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtReceiveDateTo.Text.Trim <> "" Then
            If IsDate(txtReceiveDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),RECEIVE_DATE,112) <= '" & txtReceiveDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtCheckDateFrom.Text.Trim <> "" Then
            If IsDate(txtCheckDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),CHECK_DATE,112) >= '" & txtCheckDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtCheckDateTo.Text.Trim <> "" Then
            If IsDate(txtCheckDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),CHECK_DATE,112) <= '" & txtCheckDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtPriceDateFrom.Text.Trim <> "" Then
            If IsDate(txtPriceDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),PRICE_DATE,112) >= '" & txtPriceDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtPriceDateTo.Text.Trim <> "" Then
            If IsDate(txtPriceDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),PRICE_DATE,112) <= '" & txtPriceDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtApproveDateFrom.Text.Trim <> "" Then
            If IsDate(txtApproveDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),APP_DATE,112) >= '" & txtApproveDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtApproveDateTo.Text.Trim <> "" Then
            If IsDate(txtApproveDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),APP_DATE,112) <= '" & txtApproveDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtBillDateFrom.Text.Trim <> "" Then
            If IsDate(txtBillDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),BILL_DATE,112) >= '" & txtBillDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtBillDateTo.Text.Trim <> "" Then
            If IsDate(txtBillDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),BILL_DATE,112) <= '" & txtBillDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtCompleteDateFrom.Text.Trim <> "" Then
            If IsDate(txtCompleteDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),COMPLETE_DATE,112) >= '" & txtCompleteDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtCompleteDateTo.Text.Trim <> "" Then
            If IsDate(txtCompleteDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),COMPLETE_DATE,112) <= '" & txtCompleteDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If

        SQL = SQL.Substring(0, SQL.Length - 4)

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 15 Then
            NavigationJobStatus.Visible = False
            NavigationJobAnalysis.Visible = False
        Else
            NavigationJobStatus.Visible = True
            NavigationJobAnalysis.Visible = True
        End If

        Session("SearchData") = DT
        NavigationJobStatus.SesssionSourceName = "SearchData"
        NavigationJobStatus.RenderLayout()
        NavigationJobAnalysis.SesssionSourceName = "SearchData"
        NavigationJobAnalysis.RenderLayout()
    End Sub

    Protected Sub NavigationJobStatus_PageChanging(ByVal Sender As PageNavigation) Handles NavigationJobStatus.PageChanging
        NavigationJobStatus.TheRepeater = rptJobStatus
    End Sub

    Protected Sub NavigationJobAnalysis_PageChanging(ByVal Sender As PageNavigation) Handles NavigationJobAnalysis.PageChanging
        NavigationJobAnalysis.TheRepeater = rptJobAnalysis
    End Sub

    Protected Sub rptJobStatus_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptJobStatus.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblDepartment As Label = e.Item.FindControl("lblDepartment")
        Dim lblOpenDate As Label = e.Item.FindControl("lblOpenDate")
        Dim lblReceiveDate As Label = e.Item.FindControl("lblReceiveDate")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        lblJobNo.Attributes.Add("JobID", e.Item.DataItem("JOB_ID"))
        lblNo.Text = ((NavigationJobStatus.CurrentPage - 1) * NavigationJobStatus.PageSize) + e.Item.ItemIndex + 1
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        lblDepartment.Text = e.Item.DataItem("DEPARTMENT_NAME").ToString
        If IsDate(e.Item.DataItem("OPEN_DATE")) Then
            lblOpenDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("OPEN_DATE"))
        End If
        If IsDate(e.Item.DataItem("RECEIVE_DATE")) Then
            lblReceiveDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("RECEIVE_DATE"))
        End If
        lblStatus.Text = MC.Get_JobStatus(e.Item.DataItem("STATUS"))

    End Sub

    Protected Sub rptJobAnalysis_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptJobAnalysis.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblDepartment As Label = e.Item.FindControl("lblDepartment")
        Dim lblOpenDate As Label = e.Item.FindControl("lblOpenDate")
        Dim lblCloseDate As Label = e.Item.FindControl("lblCloseDate")
        Dim lblSubmitDate As Label = e.Item.FindControl("lblSubmitDate")
        Dim lblReceiveDate As Label = e.Item.FindControl("lblReceiveDate")
        Dim lblCheckDate As Label = e.Item.FindControl("lblCheckDate")
        Dim lblPriceDate As Label = e.Item.FindControl("lblPriceDate")
        Dim lblAppDate As Label = e.Item.FindControl("lblAppDate")
        Dim lblBillDate As Label = e.Item.FindControl("lblBillDate")
        Dim lblCompleteDate As Label = e.Item.FindControl("lblCompleteDate")
        Dim lblInCompleteDate As Label = e.Item.FindControl("lblInCompleteDate")
        Dim lblHoldDate As Label = e.Item.FindControl("lblHoldDate")
        Dim lblFocDate As Label = e.Item.FindControl("lblFocDate")

        Dim lblOpenClose As Label = e.Item.FindControl("lblOpenClose")
        Dim lblCloseSubmit As Label = e.Item.FindControl("lblCloseSubmit")
        Dim lblSubmitReceive As Label = e.Item.FindControl("lblSubmitReceive")
        Dim lblReceiveCheck As Label = e.Item.FindControl("lblReceiveCheck")
        Dim lblCheckPrice As Label = e.Item.FindControl("lblCheckPrice")
        Dim lblPriceApp As Label = e.Item.FindControl("lblPriceApp")
        Dim lblAppBill As Label = e.Item.FindControl("lblAppBill")
        Dim lblBillComplete As Label = e.Item.FindControl("lblBillComplete")
        Dim lblInComplete As Label = e.Item.FindControl("lblInComplete")
        Dim lblHold As Label = e.Item.FindControl("lblHold")
        Dim lblSum As Label = e.Item.FindControl("lblSum")

        lblJobNo.Attributes.Add("JobID", e.Item.DataItem("JOB_ID"))
        lblNo.Text = ((NavigationJobStatus.CurrentPage - 1) * NavigationJobStatus.PageSize) + e.Item.ItemIndex + 1
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        lblDepartment.Text = e.Item.DataItem("DEPARTMENT_NAME").ToString
        If IsDate(e.Item.DataItem("OPEN_DATE")) Then
            lblOpenDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("OPEN_DATE"))
        End If
        If IsDate(e.Item.DataItem("CLOSE_DATE")) Then
            lblCloseDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("CLOSE_DATE"))
        End If
        If IsDate(e.Item.DataItem("SUBMIT_APPROVE_DATE")) Then
            lblSubmitDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("SUBMIT_APPROVE_DATE"))
        End If
        If IsDate(e.Item.DataItem("RECEIVE_DATE")) Then
            lblReceiveDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("RECEIVE_DATE"))
        End If
        If IsDate(e.Item.DataItem("CHECK_DATE")) Then
            lblCheckDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("CHECK_DATE"))
        End If
        If IsDate(e.Item.DataItem("PRICE_DATE")) Then
            lblPriceDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("PRICE_DATE"))
        End If
        If IsDate(e.Item.DataItem("APP_DATE")) Then
            lblAppDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("APP_DATE"))
        End If
        If IsDate(e.Item.DataItem("BILL_DATE")) Then
            lblBillDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("BILL_DATE"))
        End If
        If IsDate(e.Item.DataItem("COMPLETE_DATE")) Then
            lblCompleteDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("COMPLETE_DATE"))
        End If
        If IsDate(e.Item.DataItem("INCOMPLETE_DATE")) Then
            lblInCompleteDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("INCOMPLETE_DATE"))
        End If
        If IsDate(e.Item.DataItem("HOLD_DATE")) Then
            lblHoldDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("HOLD_DATE"))
        End If
        If IsDate(e.Item.DataItem("FOC_DATE")) Then
            lblFocDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("FOC_DATE"))
            lblCompleteDate.Text = ""
        End If

        If e.Item.DataItem("OPEN_CLOSE").ToString <> "" Then
            lblOpenClose.Text = FormatNumber(e.Item.DataItem("OPEN_CLOSE").ToString, 0)
        End If
        If e.Item.DataItem("CLOSE_SUBMIT").ToString <> "" Then
            lblCloseSubmit.Text = FormatNumber(e.Item.DataItem("CLOSE_SUBMIT").ToString, 0)
        End If
        If e.Item.DataItem("SUBMIT_RECEIVE").ToString <> "" Then
            lblSubmitReceive.Text = FormatNumber(e.Item.DataItem("SUBMIT_RECEIVE").ToString, 0)
        End If
        If e.Item.DataItem("RECEIVE_CHECK").ToString <> "" Then
            lblReceiveCheck.Text = FormatNumber(e.Item.DataItem("RECEIVE_CHECK").ToString, 0)
        End If
        If e.Item.DataItem("CHECK_PRICE").ToString <> "" Then
            lblCheckPrice.Text = FormatNumber(e.Item.DataItem("CHECK_PRICE").ToString, 0)
        End If
        If e.Item.DataItem("PRICE_APPROVE").ToString <> "" Then
            lblPriceApp.Text = FormatNumber(e.Item.DataItem("PRICE_APPROVE").ToString, 0)
        End If
        If e.Item.DataItem("APPROVE_BILL").ToString <> "" Then
            lblAppBill.Text = FormatNumber(e.Item.DataItem("APPROVE_BILL").ToString, 0)
        End If
        If e.Item.DataItem("BILL_COMPLETE").ToString <> "" Then
            lblBillComplete.Text = FormatNumber(e.Item.DataItem("BILL_COMPLETE").ToString, 0)
        End If
        If e.Item.DataItem("INCOMPLETE").ToString <> "" Then
            lblInComplete.Text = FormatNumber(e.Item.DataItem("INCOMPLETE").ToString, 0)
        End If
        If e.Item.DataItem("HOLD").ToString <> "" Then
            lblHold.Text = FormatNumber(e.Item.DataItem("HOLD").ToString, 0)
        End If

        lblSum.Text = FormatNumber(e.Item.DataItem("SUM_DATE").ToString, 0)

    End Sub

    Protected Sub rblReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblReportType.SelectedIndexChanged
        If rblReportType.SelectedIndex = 0 Then
            trJobStatus.Visible = True
            trJobAnalysis.Visible = False
        Else
            trJobStatus.Visible = False
            trJobAnalysis.Visible = True
        End If
    End Sub

    Protected Sub SrarchData(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefNo.TextChanged, txtJobNo.TextChanged, ddlCustomer.SelectedIndexChanged, ddlStatus.SelectedIndexChanged, ddlDepartment.SelectedIndexChanged, txtOpenDateFrom.TextChanged, txtOpenDateTo.TextChanged, txtCloseDateFrom.TextChanged, txtCloseDateTo.TextChanged, txtSubmitDateFrom.TextChanged, txtSubmitDateTo.TextChanged, txtReceiveDateFrom.TextChanged, txtReceiveDateTo.TextChanged, txtCheckDateFrom.TextChanged, txtCheckDateTo.TextChanged, txtPriceDateFrom.TextChanged, txtPriceDateTo.TextChanged, txtApproveDateFrom.TextChanged, txtApproveDateTo.TextChanged, txtBillDateFrom.TextChanged, txtBillDateTo.TextChanged, txtCompleteDateFrom.TextChanged, txtCompleteDateTo.TextChanged
        BindData()
    End Sub

    Sub ClearData()
        txtRefNo.Text = ""
        txtJobNo.Text = ""
        ddlCustomer.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        ddlDepartment.SelectedIndex = 0
        txtOpenDateFrom.Text = ""
        txtOpenDateTo.Text = ""
        txtCloseDateFrom.Text = ""
        txtCloseDateTo.Text = ""
        txtSubmitDateFrom.Text = ""
        txtSubmitDateTo.Text = ""
        txtReceiveDateFrom.Text = ""
        txtReceiveDateTo.Text = ""
        txtCheckDateFrom.Text = ""
        txtCheckDateTo.Text = ""
        txtPriceDateFrom.Text = ""
        txtPriceDateTo.Text = ""
        txtApproveDateFrom.Text = ""
        txtApproveDateTo.Text = ""
        txtBillDateFrom.Text = ""
        txtBillDateTo.Text = ""
        txtCompleteDateFrom.Text = ""
        txtCompleteDateTo.Text = ""
        BindData()
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
        ClearData()
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim UID As String = Now.ToOADate().ToString.Replace(".", "")
        If rblReportType.SelectedIndex = 0 Then
            Session("Print_" & UID) = NavigationJobStatus.Datasource
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "openPrintWindow('Print/JobStatus.aspx?Print_ID=" & UID & "&DocType=PDF',800,500);", True)
        Else
            Session("Print_" & UID) = NavigationJobStatus.Datasource
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "openPrintWindow('Print/JobAnalysis.aspx?Print_ID=" & UID & "&DocType=PDF',800,500);", True)
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim UID As String = Now.ToOADate().ToString.Replace(".", "")
        If rblReportType.SelectedIndex = 0 Then
            Session("Print_" & UID) = NavigationJobStatus.Datasource
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "openPrintWindow('Print/JobStatus.aspx?Print_ID=" & UID & "&DocType=EXCEL',800,500);", True)
        Else
            Session("Print_" & UID) = NavigationJobStatus.Datasource
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "openPrintWindow('Print/JobAnalysis.aspx?Print_ID=" & UID & "&DocType=EXCEL',800,500);", True)
        End If
    End Sub
End Class

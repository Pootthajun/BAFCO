﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Checked
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property UserID() As Integer
        Get
            Return ViewState("UserID")
        End Get
        Set(ByVal value As Integer)
            ViewState("UserID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UserID = Session("User_ID")
            BindData()
        End If
    End Sub

#Region "Reference"
    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT HD.REF_ID,REF_NO,HD.STATUS" & vbCrLf
        SQL &= ",DPT.DEPARTMENT_NAME REF_FROM_NAME,COUNT(1) AS JOB" & vbCrLf
        SQL &= ",[CHECK],PRICE,APP,BILL,COMPLETE + [END] AS COMPLETE,FOC,TOTAL,[DELETE]" & vbCrLf
        SQL &= "FROM REF_HEADER HD" & vbCrLf
        SQL &= "LEFT JOIN MS_DEPARTMENT DPT ON HD.REF_FROM = DPT.DEPARTMENT_ID" & vbCrLf
        SQL &= "LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
        SQL &= "LEFT JOIN" & vbCrLf
        SQL &= "(" & vbCrLf
        SQL &= "SELECT REF_ID" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Checked & " THEN 1 ELSE 0 END) [CHECK]" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Priced & " THEN 1 ELSE 0 END) PRICE" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Approving & " THEN 1 ELSE 0 END) APP" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Billed & " THEN 1 ELSE 0 END) BILL" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Completed & " THEN 1 ELSE 0 END) COMPLETE" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.EndProcess & " THEN 1 ELSE 0 END) [END]" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.FOC & " THEN 1 ELSE 0 END) FOC" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Delete & " THEN 1 ELSE 0 END) [DELETE]" & vbCrLf
        SQL &= ",COUNT(1) TOTAL FROM REF_DETAIL GROUP BY REF_ID" & vbCrLf
        SQL &= ") JOB ON HD.REF_ID = JOB.REF_ID" & vbCrLf
        SQL &= "WHERE DT.STATUS = " & MasterControlClass.Job_Status.Checked & " AND " & vbCrLf
        SQL &= "DT.STATUS_CHECK IN (" & MasterControlClass.Job_History.Wait & "," & MasterControlClass.Job_History.Incomplete & "," & MasterControlClass.Job_History.Hold & ")" & " AND "
        If txtRefNoSort.Text <> "" Then
            SQL &= "REF_NO like '%" & txtRefNoSort.Text.Replace("'", "''") & "%' AND "
        End If
        If txtJobNo.Text <> "" Then
            SQL &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtDateFrom.Text.Trim <> "" Then
            If IsDate(txtDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),HD.RECEIVE_DATE,112) >= '" & txtDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtDateTo.Text.Trim <> "" Then
            If IsDate(txtDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),HD.RECEIVE_DATE,112) <= '" & txtDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If

        SQL = SQL.Substring(0, SQL.Length - 4)
        SQL &= vbCrLf & "GROUP BY HD.REF_ID,REF_NO,HD.STATUS,DPT.DEPARTMENT_NAME"
        SQL &= vbCrLf & ",[CHECK],PRICE,APP,BILL,COMPLETE,[END],FOC,TOTAL,[DELETE]"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If
        Session("Reference") = DT
        Navigation.SesssionSourceName = "Reference"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptDataRef
    End Sub

    Protected Sub rptDataRef_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDataRef.ItemCommand
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim RefID As Int32 = lblRefNo.Attributes("REF_ID")
        Dim lblRefReceiveDate As Label = e.Item.FindControl("lblRefReceiveDate")

        Select Case e.CommandName
            Case "Edit"
                divJobList.Visible = True
                lblHead.Text = "Check List"
                txtRefNo.Text = lblRefNo.Text
                txtRefNo.Attributes.Add("REF_ID", RefID)
                BindJob(RefID)
            Case "Delete"
                Dim SQL As String = ""
                'SQL = "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Delete & " WHERE JOB_ID IN " & vbCrLf
                'SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
                'SQL &= "DELETE FROM REF_DETAIL WHERE REF_ID = " & RefID & vbCrLf
                'SQL &= "DELETE FROM REF_HEADER WHERE REF_ID = " & RefID & vbCrLf
                'SQL &= "DELETE FROM JOB_HISTORY WHERE REF_ID = " & RefID & vbCrLf
                SQL = "DELETE FROM JOB_HEADER WHERE JOB_ID IN " & vbCrLf
                SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS_CHECK = " & MasterControlClass.Job_History.Delete & vbCrLf
                SQL &= ",CHECK_BY = " & UserID & vbCrLf
                SQL &= ",CHECK_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
                SQL &= "WHERE REF_ID = " & RefID
              
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
        End Select
    End Sub

    Protected Sub rptDataRef_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDataRef.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblRefFrom As Label = e.Item.FindControl("lblRefFrom")
        Dim lblCheck As Label = e.Item.FindControl("lblCheck")
        Dim lblPrice As Label = e.Item.FindControl("lblPrice")
        Dim lblApp As Label = e.Item.FindControl("lblApp")
        Dim lblBill As Label = e.Item.FindControl("lblBill")
        Dim lblComplete As Label = e.Item.FindControl("lblComplete")
        Dim lblFOC As Label = e.Item.FindControl("lblFOC")
        Dim lblDelete As Label = e.Item.FindControl("lblDelete")
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")

        lblRefNo.Attributes.Add("REF_ID", e.Item.DataItem("REF_ID"))
        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        lblRefFrom.Text = e.Item.DataItem("REF_FROM_NAME").ToString
        lblCheck.Text = e.Item.DataItem("CHECK").ToString
        lblPrice.Text = e.Item.DataItem("PRICE").ToString
        lblApp.Text = e.Item.DataItem("APP").ToString
        lblBill.Text = e.Item.DataItem("BILL").ToString
        lblComplete.Text = e.Item.DataItem("COMPLETE").ToString
        lblFOC.Text = e.Item.DataItem("FOC").ToString
        lblDelete.Text = e.Item.DataItem("DELETE").ToString
        lblTotal.Text = e.Item.DataItem("TOTAL").ToString

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        If Session("User_Type_Name") = "Administrator" Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If
    End Sub

    Protected Sub SortData_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefNoSort.TextChanged, txtDateFrom.TextChanged, txtDateTo.TextChanged, txtJobNo.TextChanged
        BindData()
    End Sub
#End Region

    Sub BindJob(ByVal RefID As Int32)
        Dim SQL As String = ""
        SQL &= "SELECT 1 AS SORT,DETAIL_ID,REF_DETAIL.JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),REF_DETAIL.RECEIVE_DATE,126) RECEIVE_DATE," & vbCrLf
        SQL &= "REF_DETAIL.DOC_TYPE_ID,DOC_TYPE_NAME,PAYMENT,ISNULL(STATUS_CHECK," & MasterControlClass.Job_History.Wait & ") AS STATUS" & vbCrLf
        SQL &= "FROM REF_DETAIL LEFT JOIN REF_HEADER ON REF_DETAIL.REF_ID = REF_HEADER.REF_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_DOC_TYPE ON REF_DETAIL.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "WHERE REF_DETAIL.REF_ID = " & RefID & vbCrLf
        SQL &= "AND STATUS_CHECK IS NOT NULL" & vbCrLf
        SQL &= "AND STATUS_CHECK IN (" & MasterControlClass.Job_History.Wait & "," & MasterControlClass.Job_History.Incomplete & "," & MasterControlClass.Job_History.Hold & ")" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT 2 AS SORT,DETAIL_ID,REF_DETAIL.JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),REF_DETAIL.RECEIVE_DATE,126) RECEIVE_DATE," & vbCrLf
        SQL &= "REF_DETAIL.DOC_TYPE_ID,DOC_TYPE_NAME,PAYMENT,ISNULL(STATUS_CHECK," & MasterControlClass.Job_History.Wait & ") AS STATUS" & vbCrLf
        SQL &= "FROM REF_DETAIL LEFT JOIN REF_HEADER ON REF_DETAIL.REF_ID = REF_HEADER.REF_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_DOC_TYPE ON REF_DETAIL.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "WHERE REF_DETAIL.REF_ID = " & RefID & vbCrLf
        SQL &= "AND STATUS_CHECK IN (" & MasterControlClass.Job_History.Delete & ")" & vbCrLf
        SQL &= "ORDER BY SORT,DETAIL_ID" & vbCrLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim tmpDR() As DataRow
        tmpDR = DT.Select("STATUS <> '" & MasterControlClass.Job_History.Delete & "'")
        If tmpDR.Length = 0 Then
            btnCancel_Click(Nothing, Nothing)
        Else
            rptData.DataSource = DT
            rptData.DataBind()
        End If

        If DT.Rows.Count = 0 Then
            Response.Redirect("Checked.aspx")
        End If
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim DT As New DataTable
                DT = GetDataDetail()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
            Case "View"
                Dim RefID As Int32 = txtRefNo.Attributes("REF_ID")
                Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
                Dim JobID As Int32 = lblJobNo.Attributes("JOB_ID")
                Dim btnView As ImageButton = e.Item.FindControl("btnView")
                Dim PostData As Int32 = 0
                If btnView.ToolTip = "Wait" Then
                    PostData = 1
                End If

                Dim Script As String = "requireTextboxFeatured('ViewJobReference.aspx?RefID=" & RefID & "&JobID=" & JobID & "&RefStatus=" & MasterControlClass.Job_Status.Checked & "&UserID=" & UserID & "&PostData=" & PostData & "','height=600px,width=1020px,center','" & txtRefresh.ClientID & "','" & btnRefresh.ClientID & "');"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblJobReceiveDate As Label = e.Item.FindControl("lblJobReceiveDate")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        Dim lblPayment As Label = e.Item.FindControl("lblPayment")
        Dim btnView As ImageButton = e.Item.FindControl("btnView")
        Dim td As HtmlTableCell = e.Item.FindControl("td1")
        Dim cb As CheckBox = e.Item.FindControl("cb")

        lblJobNo.Attributes.Add("JOB_ID", e.Item.DataItem("JOB_ID").ToString)
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        If e.Item.DataItem("RECEIVE_DATE").ToString <> "" Then
            lblJobReceiveDate.Text = e.Item.DataItem("RECEIVE_DATE").ToString
        End If
        lblDocType.Attributes.Add("DOC_TYPE_ID", e.Item.DataItem("DOC_TYPE_ID").ToString)
        lblDocType.Text = e.Item.DataItem("DOC_TYPE_NAME").ToString
        If e.Item.DataItem("PAYMENT").ToString <> "" Then
            lblPayment.Text = FormatNumber(e.Item.DataItem("PAYMENT").ToString, 2)
        End If

        Select Case e.Item.DataItem("STATUS")
            Case MasterControlClass.Job_History.Wait
                btnView.ImageUrl = "images/Icon/24.png"
                btnView.ToolTip = "Wait"
            Case MasterControlClass.Job_History.Confirmation
                btnView.ImageUrl = "images/Icon/63.png"
                btnView.ToolTip = "Confirmation"
            Case MasterControlClass.Job_History.Incomplete
                btnView.ImageUrl = "images/Icon/21.png"
                btnView.ToolTip = "Incomplete"
                btnView.Enabled = False
                td.Style.Item("background-color") = "#DDDDDD"
                btnView.Style.Item("cursor") = "default"
                cb.Visible = False
            Case MasterControlClass.Job_History.Hold
                btnView.ImageUrl = "images/Icon/03.png"
                btnView.ToolTip = "Hold"
                btnView.Enabled = False
                td.Style.Item("background-color") = "#DDDDDD"
                btnView.Style.Item("cursor") = "default"
                cb.Visible = False
            Case MasterControlClass.Job_History.Delete
                btnView.ImageUrl = "images/Icon/57.png"
                btnView.ToolTip = "Delete"
                td.Style.Item("background-color") = "#DDDDDD"
                cb.Visible = False
            Case MasterControlClass.Job_History.FOC
                btnView.ImageUrl = "images/Icon/36.png"
                btnView.ToolTip = "FOC"
        End Select

    End Sub

    Function GetDataDetail() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("JOB_ID")
        DT.Columns.Add("JOB_NO")
        DT.Columns.Add("CUSTOMER")
        DT.Columns.Add("RECEIVE_DATE")
        DT.Columns.Add("DOC_TYPE_ID")
        DT.Columns.Add("DOC_TYPE_NAME")
        DT.Columns.Add("PAYMENT")
        DT.Columns.Add("STATUS")

        If rptData.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptData.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim lblJobNo As Label = ri.FindControl("lblJobNo")
                Dim lblCustomer As Label = ri.FindControl("lblCustomer")
                Dim lblJobDate As Label = ri.FindControl("lblJobDate")
                Dim lblDocType As Label = ri.FindControl("lblDocType")
                Dim lblPayment As Label = ri.FindControl("lblPayment")

                Dim DR As DataRow = DT.NewRow
                DR("JOB_ID") = lblJobNo.Attributes("JOB_ID")
                DR("JOB_NO") = lblJobNo.Text
                DR("CUSTOMER") = lblCustomer.Text
                DR("RECEIVE_DATE") = lblJobDate.Text
                DR("DOC_TYPE_ID") = lblDocType.Attributes("DOC_TYPE_ID")
                DR("DOC_TYPE_NAME") = lblDocType.Text
                DR("PAYMENT") = lblPayment.Text
                DT.Rows.Add(DR)
            Next
        End If
        Return DT

    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        divJobList.Visible = False
        BindData()
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If txtRefresh.Text.Trim <> "" Then
            BindJob(txtRefresh.Text)
        End If
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
        '----------- Checking Current Status-----------
        Dim IsNotCheck As Boolean = False
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            If cb.Visible = True Then
                Dim btnView As ImageButton = ri.FindControl("btnView")
                If btnView.Enabled = True Then
                    If Not cb.Checked Then
                        IsNotCheck = True '------- มีอย่างน้อย 1 อันที่ยังไม่ได้เช็ค --------
                        Exit For
                    End If
                End If
            End If

        Next

        '---------- Set New Status-------
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            If cb.Visible = True Then
                Dim btnView As ImageButton = ri.FindControl("btnView")
                If btnView.Enabled = True Then
                    cb.Checked = IsNotCheck
                End If
            End If

        Next
    End Sub

    Protected Sub btnConfirmation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmation.Click
        '----------- Checking Current Status-----------
        Dim IsNotCheck As Boolean = False
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            Dim btnView As ImageButton = ri.FindControl("btnView")
            If btnView.Enabled = True Then
                If cb.Checked = True Then
                    IsNotCheck = True
                    Exit For
                End If
            End If
        Next
        If IsNotCheck = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please chose job reference no');", True)
            Exit Sub
        End If

        '---------- Set New Status-------
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            Dim btnView As ImageButton = ri.FindControl("btnView")
            Dim lblJobNo As Label = ri.FindControl("lblJobNo")
            Dim RefID As Int32 = txtRefNo.Attributes("REF_ID")
            Dim JobID As Int32 = lblJobNo.Attributes("JOB_ID")

            If btnView.Enabled = True Then
                If cb.Checked = True Then
                    Dim SQL As String = ""
                    Dim DA As New SqlDataAdapter
                    Dim DT As New DataTable

                    SQL = "SELECT * FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & " ORDER BY DETAIL_ID"
                    DA = New SqlDataAdapter(SQL, ConnStr)
                    DT = New DataTable
                    DA.Fill(DT)
                    Dim DR As DataRow
                    DR = DT.NewRow
                    DR("DETAIL_ID") = GL.FindID("JOB_HISTORY", "DETAIL_ID")
                    DR("REF_ID") = RefID
                    DR("JOB_ID") = JobID
                    DR("DESCRIPTION") = MC.Get_JobDescription(MasterControlClass.Job_Status.Checked)
                    DR("JOB_STATUS") = MasterControlClass.Job_History.Confirmation
                    DR("REMARK") = ""
                    Dim ReceiveDate As String = ""
                    ReceiveDate = MC.Get_ReceiveDate(RefID, JobID)
                    DR("RECEIVE_DATE") = ReceiveDate
                    DR("CLOSE_DATE") = Now
                    DR("UPDATE_BY") = UserID
                    DT.Rows.Add(DR)
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)

                    SQL = ""
                    'Select Case RefStatus
                    '    Case MasterControlClass.Job_Status.Checked
                    SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Priced & vbCrLf
                    SQL &= ",STATUS_CHECK = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                    SQL &= ",STATUS_PRICE = " & MasterControlClass.Job_History.Wait & vbCrLf
                    SQL &= ",CHECK_BY = " & UserID & vbCrLf
                    SQL &= ",CHECK_DATE = GETDATE()" & vbCrLf
                    '    Case MasterControlClass.Job_Status.Priced
                    '        SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Approving & vbCrLf
                    '        SQL &= ",STATUS_PRICE = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                    '        SQL &= ",STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
                    '        SQL &= ",PRICE_BY = " & UserID & vbCrLf
                    '        SQL &= ",PRICE_DATE = GETDATE()" & vbCrLf
                    '    Case MasterControlClass.Job_Status.Approving
                    '        SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Billed & vbCrLf
                    '        SQL &= ",STATUS_APP = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                    '        SQL &= ",STATUS_BILL = " & MasterControlClass.Job_History.Wait & vbCrLf
                    '        SQL &= ",APP_BY = " & UserID & vbCrLf
                    '        SQL &= ",APP_DATE = GETDATE()" & vbCrLf
                    '    Case MasterControlClass.Job_Status.Billed
                    '        SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Completed & vbCrLf
                    '        SQL &= ",STATUS_BILL = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                    '        SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Wait & vbCrLf
                    '        SQL &= ",BILL_BY = " & UserID & vbCrLf
                    '        SQL &= ",BILL_DATE = GETDATE()" & vbCrLf
                    '    Case MasterControlClass.Job_Status.Completed
                    '        SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.EndProcess & vbCrLf
                    '        SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                    '        SQL &= ",COMPLETE_BY = " & UserID & vbCrLf
                    '        SQL &= ",COMPLETE_DATE = GETDATE()" & vbCrLf
                    'End Select
                    SQL &= ",UPDATE_BY = " & UserID & " ,UPDATE_DATE = GETDATE()" & vbCrLf
                    SQL &= "WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
                    Dim conn As New SqlConnection(ConnStr)
                    conn.Open()
                    Dim SqlCmd As SqlCommand
                    SqlCmd = New SqlCommand(SQL, conn)
                    SqlCmd.ExecuteNonQuery()
                    conn.Close()

                End If
            End If
        Next

        divJobList.Visible = False
        BindData()
    End Sub

End Class

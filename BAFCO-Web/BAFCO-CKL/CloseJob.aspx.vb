﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Filled
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib

    Private Property DefaultDocument() As Integer
        Get
            If IsNumeric(ViewState("DefaultDocument")) Then
                Return ViewState("DefaultDocument")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DefaultDocument") = value
        End Set
    End Property

    Private Property DepartmentFWP() As String
        Get
            If ViewState("DepartmentFWP") Is Nothing Then
                Return ""
            Else
                Return ViewState("DepartmentFWP")
            End If
        End Get
        Set(ByVal value As String)
            ViewState("DepartmentFWP") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlCustomerFromUpload(ddlCustomer, "-------------- Select --------------")
            MC.BindDDlJobTypeFromUpload(ddlJobType, "--------- Select ---------")
            MC.BindDDlDepartmentFromUpload(ddlDepartment, "--------- Select ---------")
            DefaultDocument = Session("Default_Document")
            DepartmentFWP = Session("Department_FWP")
            BindData()
        End If
    End Sub

    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT * FROM JOB_HEADER WHERE STATUS = " & MasterControlClass.Job_Status.Open & " AND "
        If Session("User_Type_Name") <> "Administrator" Then
            If InStr(DepartmentFWP.ToString, ",") = 0 Then
                SQL &= "DEPARTMENT = '" & DepartmentFWP.ToString.Replace("'", "''") & "' AND "
            Else
                Dim dpt As String()
                dpt = DepartmentFWP.ToString.Split(",")
                Dim filter As String = ""
                For i As Int32 = 0 To dpt.Length - 1
                    filter &= "DEPARTMENT = '" & dpt(i).ToString.Trim & "' OR "
                Next
                filter = filter.Substring(0, filter.Length - 4)
                SQL &= "(" & filter & ") AND"
            End If

        End If
        If txtJobNo.Text <> "" Then
            SQL &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If ddlCustomer.SelectedIndex > 0 Then
            SQL &= "CUSTOMER = '" & ddlCustomer.SelectedValue.Replace("'", "''") & "' AND "
        End If
        If ddlJobType.SelectedIndex > 0 Then
            SQL &= "JOB_TYPE = '" & ddlJobType.SelectedValue.Replace("'", "''") & "' AND "
        End If
        If ddlDepartment.SelectedIndex > 0 Then
            SQL &= "DEPARTMENT = '" & ddlDepartment.SelectedValue.Replace("'", "''") & "' AND "
        End If
        If txtJobDateFrom.Text.Trim <> "" Then
            If IsDate(txtJobDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),OPEN_DATE,112) >= '" & txtJobDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtJobDateTo.Text.Trim <> "" Then
            If IsDate(txtJobDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),OPEN_DATE,112) <= '" & txtJobDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If

        SQL = SQL.Substring(0, SQL.Length - 4)
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Try
            DA.Fill(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please check config FWP Department');", True)
        End Try

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("Filled") = DT
        Navigation.SesssionSourceName = "Filled"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim ddlDocType As DropDownList = e.Item.FindControl("ddlDocType")
        Dim JobID As Int32 = lblJobNo.Attributes("JobID")
        Dim FieldKey(8) As String
        FieldKey(0) = "ENTRY_TYPE"
        FieldKey(1) = "JOB_TYPE"
        FieldKey(2) = "FEE"
        FieldKey(3) = "JOB_DOC"
        FieldKey(4) = "ACCOUNTING"
        FieldKey(5) = "VEHICLE_BAFCO"
        FieldKey(6) = "VEHICLE_EMPLOY"
        FieldKey(7) = "NAME_VEHICLE_EMPLOY"
        FieldKey(8) = "REMARK"

        Select Case e.CommandName
            Case "CloseJob"
                If ddlDocType.SelectedIndex = 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select Document Type');", True)
                    ddlDocType.Focus()
                    Exit Sub
                End If

                Dim DocType As Int32 = 0
                If ddlDocType.Attributes("DOC_TYPE_ID") <> "" Then
                    DocType = ddlDocType.Attributes("DOC_TYPE_ID")
                End If
                Dim Link As String = ""
                Link = "window.location.href='CloseJobDocument.aspx?JobID=" & JobID & "&DocumentType=" & ddlDocType.SelectedValue & "';"
                If DocType <> ddlDocType.SelectedValue Then

                    '-------------- UPDATE HERDER -------------
                    Dim SQL As String = ""
                    SQL = "SELECT * FROM JOB_HEADER WHERE JOB_NO = '" & lblJobNo.Text.Replace("'", "''") & "'"
                    Dim DA As New SqlDataAdapter(SQL, ConnStr)
                    Dim DT As New DataTable
                    Dim DT_ADD As New DataTable
                    Dim DR As DataRow
                    DA.Fill(DT)
                    DR = DT.Rows(0)
                    DR("DOC_TYPE_ID") = ddlDocType.SelectedValue
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                    '-------------- CLEAR DATA ----------------
                    SQL = ""
                    For i As Int32 = 0 To FieldKey.Length - 1
                        SQL &= "DELETE FROM JOB_DETAIL_" & FieldKey(i) & " WHERE JOB_ID = " & JobID & vbCrLf
                    Next
                    Dim conn As New SqlConnection(ConnStr)
                    conn.Open()
                    Dim SqlCmd As SqlCommand
                    SqlCmd = New SqlCommand(SQL, conn)
                    SqlCmd.ExecuteNonQuery()
                    conn.Close()
                    '-------------- ADD JOB DETAIL ----------------
                    For i As Int32 = 0 To FieldKey.Length - 2
                        AddJobDetail(FieldKey(i), JobID, ddlDocType.SelectedValue)
                    Next
                    '-------------- ADD JOB REMARK ----------------
                    SQL = "SELECT * FROM JOB_DETAIL_REMARK"
                    DA = New SqlDataAdapter(SQL, ConnStr)
                    DT_ADD = New DataTable
                    DA.Fill(DT_ADD)
                    DR = DT_ADD.NewRow
                    DR("DETAIL_ID") = GL.FindID("JOB_DETAIL_REMARK", "DETAIL_ID")
                    DR("JOB_ID") = JobID
                    DR("REMARK1") = ""
                    DR("REMARK2") = ""
                    DR("REMARK3") = ""
                    DR("REMARK4") = ""
                    DT_ADD.Rows.Add(DR)
                    cmd = New SqlCommandBuilder(DA)
                    DA.Update(DT_ADD)
                Else
                    Dim SQL As String = ""
                    For i As Int32 = 0 To FieldKey.Length - 2
                        SQL = "SELECT * FROM MS_" & FieldKey(i) & " WHERE " & MC.Get_DocumentCondition(ddlDocType.SelectedValue)
                        Dim DA_MASTER As New SqlDataAdapter(SQL, ConnStr)
                        Dim DT_MASTER As New DataTable
                        DA_MASTER = New SqlDataAdapter(SQL, ConnStr)
                        DT_MASTER = New DataTable
                        DA_MASTER.Fill(DT_MASTER)
                        If DT_MASTER.Rows.Count > 0 Then
                            SQL &= "DELETE FROM JOB_DETAIL_" & FieldKey(i) & " WHERE " & FieldKey(i) & "_ID NOT IN (SELECT " & FieldKey(i) & "_ID FROM MS_" & FieldKey(i) & " WHERE " & MC.Get_DocumentCondition(ddlDocType.SelectedValue) & ")"
                            Dim conn As New SqlConnection(ConnStr)
                            conn.Open()
                            Dim SqlCmd As SqlCommand
                            SqlCmd = New SqlCommand(SQL, conn)
                            SqlCmd.ExecuteNonQuery()
                            conn.Close()

                            SQL = "SELECT * FROM JOB_DETAIL_" & FieldKey(i) & " WHERE JOB_ID = " & JobID
                            Dim DT_DETAIL As New DataTable
                            Dim DA_DETAIL As New SqlDataAdapter(SQL, ConnStr)
                            DA_DETAIL.Fill(DT_DETAIL)
                            For x As Int32 = 0 To DT_MASTER.Rows.Count - 1
                                DT_DETAIL.DefaultView.RowFilter = FieldKey(i) & "_ID = " & DT_MASTER.Rows(x).Item(FieldKey(i) & "_ID")
                                Dim Temp As New DataTable
                                Temp = DT_DETAIL.DefaultView.ToTable
                                DT_DETAIL.DefaultView.RowFilter = ""
                                If Temp.Rows.Count > 0 Then
                                    If (Temp.Rows(0).Item(FieldKey(i) & "_NAME").ToString <> DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME").ToString) Or (Temp.Rows(0).Item("SORT").ToString <> DT_MASTER.Rows(x).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue)).ToString) Then
                                        Dim row As Int32 = 0
                                        For y As Int32 = 0 To DT_DETAIL.Rows.Count - 1
                                            If DT_DETAIL.Rows(y).Item(FieldKey(i) & "_ID") = Temp.Rows(0).Item(FieldKey(i) & "_ID") Then
                                                row = y
                                            End If
                                        Next
                                        DT_DETAIL.Rows(row).Item(FieldKey(i) & "_NAME") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                                        DT_DETAIL.Rows(row).Item("SORT") = DT_MASTER.Rows(x).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue))
                                        DT_DETAIL.Rows(row).Item("CB") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB1") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB2") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB3") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB4") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB5") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB6") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB7") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB8") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB9") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB10") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("LB11") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB1") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB2") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB3") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB4") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB5") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB6") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB7") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB8") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB9") = DBNull.Value
                                        DT_DETAIL.Rows(row).Item("TB10") = DBNull.Value
                                        If InStr(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"), "[values]") > 0 Then
                                            Dim txtValue As String = ""
                                            txtValue = GL.ReplaceText(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"))
                                            Dim Name() As String
                                            Name = Split(txtValue, "[values]")

                                            For j As Integer = 0 To Name.Length - 1
                                                If j <> Name.Length - 1 Then
                                                    DT_DETAIL.Rows(row).Item("TB" & j + 1) = ""
                                                End If
                                                DT_DETAIL.Rows(row).Item("LB" & j + 1) = Name(j)
                                            Next
                                        Else
                                            DT_DETAIL.Rows(row).Item("LB1") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                                        End If
                                        Dim cmd As New SqlCommandBuilder(DA_DETAIL)
                                        DA_DETAIL.Update(DT_DETAIL)
                                    End If
                                Else
                                    'ADD ถ้ายังไม่เคยมีข้อมูล
                                    Dim DR As DataRow
                                    DR = DT_DETAIL.NewRow
                                    DR("DETAIL_ID") = GL.FindID("JOB_DETAIL_" & FieldKey(i), "DETAIL_ID")
                                    DR("JOB_ID") = JobID
                                    DR(FieldKey(i) & "_ID") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_ID")
                                    DR(FieldKey(i) & "_NAME") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                                    DR("SORT") = DT_MASTER.Rows(x).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue))

                                    If InStr(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"), "[values]") > 0 Then
                                        Dim txtValue As String = ""
                                        txtValue = GL.ReplaceText(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"))
                                        Dim Name() As String
                                        Name = Split(txtValue, "[values]")

                                        For j As Integer = 0 To Name.Length - 1
                                            If j <> Name.Length - 1 Then
                                                DR("TB" & j + 1) = ""
                                            End If
                                            DR("LB" & j + 1) = Name(j)
                                        Next
                                    Else
                                        DR("LB1") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                                    End If

                                    DT_DETAIL.Rows.Add(DR)
                                    Dim cmd As New SqlCommandBuilder(DA_DETAIL)
                                    DA_DETAIL.Update(DT_DETAIL)
                                End If
                            Next

                        Else
                            SQL &= "DELETE FROM JOB_DETAIL_" & FieldKey(i) & " WHERE JOB_ID = " & JobID & vbCrLf
                            Dim conn As New SqlConnection(ConnStr)
                            conn.Open()
                            Dim SqlCmd As SqlCommand
                            SqlCmd = New SqlCommand(SQL, conn)
                            SqlCmd.ExecuteNonQuery()
                            conn.Close()
                        End If
                    Next
                End If
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", Link, True)
            Case "Delete"
                Dim SQL As String = ""
                SQL &= "DELETE FROM JOB_HEADER WHERE JOB_NO = '" & lblJobNo.Text.Replace("'", "''") & "'" & vbCrLf
                For i As Int32 = 0 To FieldKey.Length - 1
                    SQL &= "DELETE FROM JOB_DETAIL_" & FieldKey(i) & " WHERE JOB_ID = " & JobID & vbCrLf
                Next
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblJobType As Label = e.Item.FindControl("lblJobType")
        Dim lblDepartment As Label = e.Item.FindControl("lblDepartment")
        Dim lblOpenDate As Label = e.Item.FindControl("lblOpenDate")
        Dim ddlDocType As DropDownList = e.Item.FindControl("ddlDocType")
        lblJobNo.Attributes.Add("JobID", e.Item.DataItem("JOB_ID"))
        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        lblJobType.Text = e.Item.DataItem("JOB_TYPE").ToString
        lblDepartment.Text = e.Item.DataItem("DEPARTMENT").ToString
        If IsDate(e.Item.DataItem("OPEN_DATE")) Then
            lblOpenDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("OPEN_DATE"))
        End If
        ddlDocType.Attributes.Add("DOC_TYPE_ID", e.Item.DataItem("DOC_TYPE_ID").ToString)

        If e.Item.DataItem("DOC_TYPE_ID").ToString <> "" Then
            MC.BindDDlDocType(ddlDocType, "----- Select -----", e.Item.DataItem("DOC_TYPE_ID").ToString)
        Else
            MC.BindDDlDocType(ddlDocType, "----- Select -----", DefaultDocument)
        End If

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        If Session("User_Type_Name") = "Administrator" Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If

    End Sub

    Sub AddJobDetail(ByVal FieldKey As String, ByVal JobID As String, ByVal DocumentType As MasterControlClass.DocumentType)
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DT_Add As New DataTable
        Dim DR As DataRow

        'SQL = "SELECT " & FieldKey & "_ID," & FieldKey & "_NAME,SORT FROM MS_" & FieldKey & " WHERE ACTIVE_STATUS = 1 AND " & MC.Get_DocumentCondition(DocumentType)
        SQL = "SELECT * FROM MS_" & FieldKey & " WHERE ACTIVE_STATUS = 1 AND " & MC.Get_DocumentCondition(DocumentType)
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        SQL = "SELECT * FROM JOB_DETAIL_" & FieldKey
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT_Add = New DataTable
        DA.Fill(DT_Add)
        For i As Int32 = 0 To DT.Rows.Count - 1
            DR = DT_Add.NewRow
            DR("DETAIL_ID") = GL.FindID("JOB_DETAIL_" & FieldKey, "DETAIL_ID")
            DR("JOB_ID") = JobID
            DR(FieldKey & "_ID") = DT.Rows(i).Item(FieldKey & "_ID")
            DR(FieldKey & "_NAME") = DT.Rows(i).Item(FieldKey & "_NAME")
            DR("SORT") = DT.Rows(i).Item(MC.Get_DocumentSort(DocumentType))

            If InStr(DT.Rows(i).Item(FieldKey & "_NAME"), "[values]") > 0 Then
                Dim txtValue As String = ""
                txtValue = GL.ReplaceText(DT.Rows(i).Item(FieldKey & "_NAME"))
                Dim Name() As String
                Name = Split(txtValue, "[values]")

                For j As Integer = 0 To Name.Length - 1
                    If j <> Name.Length - 1 Then
                        DR("TB" & j + 1) = ""
                    End If
                    DR("LB" & j + 1) = Name(j)
                Next
            Else
                DR("LB1") = DT.Rows(i).Item(FieldKey & "_NAME")
            End If

            DT_Add.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT_Add)
        Next
    End Sub

    Protected Sub txtJobNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJobNo.TextChanged, ddlCustomer.SelectedIndexChanged, ddlJobType.SelectedIndexChanged, ddlDepartment.SelectedIndexChanged, txtJobDateFrom.TextChanged, txtJobDateTo.TextChanged
        BindData()
    End Sub
End Class

﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_Customer.aspx.vb" Inherits="Master_Customer"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="900" valign="top">
                <asp:UpdatePanel ID="udp1" runat="server" >
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="1" style="margin:20px  0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                            <tr>
                                <td width="50" class="Grid_Header" align="center">
                                    No                    
                                </td>
                                <td width="400" class="Grid_Header">                    
                                    Customer Name
                                </td>
                                <td width="100" class="Grid_Header">                  
                                    Action
                                </td>
                            </tr>
                            <tr>
                                <td height="25px">
                                    &nbsp;<asp:Button ID="btnSearch" runat="server" style="display:none;"/>
                                </td>
                                <td height="25px">
                                    <asp:TextBox ID="txtName" runat="server" Width="100%" CssClass="Textbox_Form_White"></asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" 
                                        style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail">
                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                        </td>  
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Edit" ImageUrl="images/icon/61.png" Height="25px" />
                                        </td>                               
                                    </tr>
                                    <tr id="trEdit" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center" valign="middle">
                                            <asp:Image ImageUrl="images/arrow_blue_right.png" ID="imgEdit" runat="server"></asp:Image>
                                        </td>
                                        <td class="Grid_Detail">
                                            <asp:TextBox ID="txtName" runat="server" MaxLength="200" CssClass="Textbox_Form_Required" Width="100%"></asp:TextBox>
                                        </td>
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnSave" CommandName="Save" runat="server" ToolTip="Save" ImageUrl="images/icon/59.png" Height="25px" />
                                            <asp:ImageButton ID="btnCancel" CommandName="Cancel" runat="server" ToolTip="Cancel" ImageUrl="images/icon/21.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                <div style="width:800px;overflow:auto;">
                </div>
                    </ContentTemplate>
                </asp:UpdatePanel>           
            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>


<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_Default_Document.aspx.vb" Inherits="Master_Default_Document" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            height: 27px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table cellpadding=0 cellspacing=0 width="1000px" style="padding-top:10px;">
        <tr style="padding-top:10px; padding-bottom:10px; background-color:Gray; height:40px;">
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="padding-left:10px; width:100px; color:White;">
                            Document Type
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="Dropdown_Form_White" width="120px" style="text-align:center;" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-left:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="text-align:left; vertical-align:top;">
                            <table style="font-family: Tahoma; font-size: 18px; color:Black; background-color:White; width:1000px;">
                                <tr>
                                    <td style="font-weight:bold; text-align:center; height:30px;">
                                        <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-bottom:solid 1px Black;" class="style1"></td>
                                </tr>
                                <tr id="trEntryType1" runat="server">
                                    <td style="font-family: Tahoma; font-size: 14px; color:Black; font-weight:bold; text-align:left; height:25px;">
                                        ������㺢�
                                    </td>
                                </tr>
                                <tr id="trEntryType2" runat="server">
                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                        <table cellpadding="0" cellspacing="1">
                                            <tr>
                                                <td>
                                                    <asp:Repeater ID="rptEntryType" runat="server">
                                                        <ItemTemplate>
                                                            <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td width="20px">
                                                                                <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td width="230px">
                                                                                <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td width="20px">
                                                                                <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td width="230px">
                                                                                <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td width="20px">
                                                                                <asp:CheckBox ID="cbC3" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td width="230px">
                                                                                <asp:Label ID="lbl_C3_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td width="20px">
                                                                                <asp:CheckBox ID="cbC4" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td width="230px">
                                                                                <asp:Label ID="lbl_C4_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>            
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </table>        
                                    </td>
                                </tr>
                                <tr id="trEntryType3" runat="server">
                                    <td style="border-bottom:solid 1px Black; height:5px;"></td>
                                </tr>
                                <tr id="trJobType1" runat="server">
                                    <td style="font-family: Tahoma; font-size: 14px; color:Black; font-weight:bold; text-align:left; height:25px;">
                                        �������ҹ
                                    </td>
                                </tr>
                                <tr id="trJobType2" runat="server">
                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                        <table cellpadding="0" cellspacing="1">
                                            <tr>
                                                <td>
                                                    <asp:Repeater ID="rptJobType" runat="server">
                                                        <ItemTemplate>
                                                            <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                <td style="text-align:left; width:500px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0" width="500px">
                                                                        <tr style="vertical-align:top;">
                                                                            <td width=20px">
                                                                                <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; width:500px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0" width="500px">
                                                                        <tr style="vertical-align:top;">
                                                                            <td width=20px">
                                                                                <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_1" runat="server" CommandName="JOB_TYPE_txt_C2_1" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_2" runat="server" CommandName="JOB_TYPE_txt_C2_2" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_3" runat="server" CommandName="JOB_TYPE_txt_C2_3" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_4" runat="server" CommandName="JOB_TYPE_txt_C2_4" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_5" runat="server" CommandName="JOB_TYPE_txt_C2_5" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_6" runat="server" CommandName="JOB_TYPE_txt_C2_6" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_7" runat="server" CommandName="JOB_TYPE_txt_C2_7" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_8" runat="server" CommandName="JOB_TYPE_txt_C2_8" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_9" runat="server" CommandName="JOB_TYPE_txt_C2_9" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Button ID="btn_txt_C2_10" runat="server" CommandName="JOB_TYPE_txt_C2_10" style="display:none;"></asp:Button>
                                                                                <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>           
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </table>        
                                    </td>
                                </tr>
                                <tr id="trJobType3" runat="server">
                                    <td style="border-bottom:solid 1px Black; height:5px;"></td>
                                </tr>
                                <tr id="trFee1" runat="server">
                                    <td style="font-family: Tahoma; font-size: 13px; color:Black; font-weight:bold; text-align:left; height:10px;">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr id="trFee2" runat="server">
                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                        <table cellpadding="0" cellspacing="1">
                                            <tr>
                                                <td>
                                                    <asp:Repeater ID="rptFee" runat="server">
                                                        <ItemTemplate>
                                                            <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td>
                                                                                <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td>
                                                                                <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td>
                                                                                <asp:CheckBox ID="cbC3" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_C3_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C3_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C3_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="text-align:left; width:250px; vertical-align:top;">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr style="vertical-align:top;">
                                                                            <td>
                                                                                <asp:CheckBox ID="cbC4" runat="server" Visible="false"/>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lbl_C4_1" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_2" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_3" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_4" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_5" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_6" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_7" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_8" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_9" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_10" runat="server"></asp:Label>
                                                                                <asp:TextBox ID="txt_C4_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lbl_C4_11" runat="server"></asp:Label>
                                                                            </td>       
                                                                        </tr>
                                                                    </table>
                                                                </td>            
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </table>
                                                    
                                    </td>
                                </tr>
                                <tr id="trFee3" runat="server">
                                    <td style="border-bottom:solid 1px Black; height:5px;"></td>
                                </tr>
                                <tr id="trJob1" runat="server">
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="font-family: Tahoma; font-size: 14px; color:Black; background-color:White; width:100%;">
                                            <tr>
                                                <td width="500px" align="left" style="font-weight:bold; height:25px;">
                                                    �͡���Ṻ Job
                                                </td>
                                                <td width="500px" align="left" style="font-weight:bold; height:25px;">
                                                    ੾��Ἱ��ѭ��
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                                    <table cellpadding="0" cellspacing="1">
                                                        <tr>
                                                            <td>
                                                                <asp:Repeater ID="rptJobDoc" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                            <td style="text-align:left; vertical-align:top;">
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr style="vertical-align:top;">
                                                                                        <td width="20px">
                                                                                            <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                        </td>
                                                                                        <td width="480px">
                                                                                            <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                        </td>       
                                                                                    </tr>
                                                                                </table>
                                                                            </td>        
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top; padding-left:5px;">
                                                    <table cellpadding="0" cellspacing="1">
                                                        <tr>
                                                            <td>
                                                                <asp:Repeater ID="rptAcc" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                            <td style="text-align:left; vertical-align:top;">
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr style="vertical-align:top;">
                                                                                        <td width="500px">
                                                                                            <asp:Label ID="lblNo" runat="server"></asp:Label>&nbsp;&nbsp;
                                                                                            <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                        </td>       
                                                                                    </tr>
                                                                                </table>
                                                                            </td>        
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trJob2" runat="server">
                                    <td style="border-bottom:solid 1px Black; height:5px;"></td>
                                </tr>
                                <tr id="trVehicle1" runat="server">
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="font-family: Tahoma; font-size: 14px; color:Black; background-color:White; width:100%;">
                                            <tr>
                                                <td colspan="3" style="font-weight:bold; height:25px;">
                                                    ��������´��â���
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="font-weight:bold; height:30px; width:300px; border-right:solid 1px Black;">
                                                    ö Bafco
                                                </td>
                                                <td style="font-weight:bold; height:30px; width:300px; border-right:solid 1px Black; padding-left:10px;">
                                                    ö��ҧ
                                                </td>
                                                <td style="font-weight:bold; height:30px; padding-left:10px;">
                                                    ����ö��ҧ
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td style="border-right:solid 1px Black;">
                                                    <table cellpadding="0" cellspacing="1">
                                                        <tr>
                                                            <td>
                                                                <asp:Repeater ID="rptBafco" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                            <td style="text-align:left; vertical-align:top;">
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr style="vertical-align:top;">
                                                                                        <td>
                                                                                            <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                        </td>       
                                                                                    </tr>
                                                                                </table>
                                                                            </td>        
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="border-right:solid 1px Black; padding-left:10px;">
                                                    <table cellpadding="0" cellspacing="1">
                                                        <tr>
                                                            <td>
                                                                <asp:Repeater ID="rptEmploy" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                            <td style="text-align:left; vertical-align:top;">
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr style="vertical-align:top;">
                                                                                        <td>
                                                                                            <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                        </td>       
                                                                                    </tr>
                                                                                </table>
                                                                            </td>        
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td style="padding-left:10px;">
                                                    <table cellpadding="0" cellspacing="1">
                                                        <tr>
                                                            <td>
                                                                <asp:Repeater ID="rptNameEmploy" runat="server">
                                                                    <ItemTemplate>
                                                                        <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                            <td style="text-align:left; width:250px; vertical-align:top;">
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr style="vertical-align:top;">
                                                                                        <td>
                                                                                            <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                        </td>       
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                            <td style="text-align:left; width:250px; vertical-align:top;">
                                                                                <table border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr style="vertical-align:top;">
                                                                                        <td>
                                                                                            <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                                            <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                            <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                                        </td>       
                                                                                    </tr>
                                                                                </table>
                                                                            </td>          
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trVehicle2" runat="server">
                                    <td style="border-bottom:solid 1px Black; height:5px;"></td>
                                </tr>
                                <tr id="trRemark1" runat="server">  
                                    <td>
                                        <table cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="font-family: Tahoma; font-size: 14px; color:Black; font-weight:bold; text-align:left; height:25px;">
                                                    Remark&nbsp;&nbsp;
                                                </td>
                                                <td style="font-family: Tahoma; font-size: 14px; color:Black; text-align:left; height:25px;" Width="100%">
                                                    <asp:TextBox ID="txtRemark1" runat="server" style="text-align:left;" CssClass="txtValue" Width="100%"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr id="trRemark2" runat="server">
                                    <td style="font-family: Tahoma; font-size: 14px; color:Black; height:25px;">
                                        <asp:TextBox ID="txtRemark2" runat="server" style="text-align:left;" CssClass="txtValue" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trRemark3" runat="server">
                                    <td style="font-family: Tahoma; font-size: 14px; color:Black; text-align:left; height:25px;">
                                        <asp:TextBox ID="txtRemark3" runat="server" style="text-align:left;" CssClass="txtValue" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trRemark4" runat="server">
                                    <td style="font-family: Tahoma; font-size: 14px; color:Black; text-align:left; height:25px;">
                                        <asp:TextBox ID="txtRemark4" runat="server" style="text-align:left;" CssClass="txtValue" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="trSave" runat="server">
                                    <td style="text-align:right; height:25px;">
                                        <asp:Button ID="btnSave" CssClass="Button_Red" runat="server" Text="Save" Width="120px" Height="30px"/>
                                    </td>
                                </tr>
                            </table>
                            <div style="height:20px;"></div>
                        </td>
                    </tr>
                </table>  
            </td>
        </tr>
    </table>
</asp:Content>


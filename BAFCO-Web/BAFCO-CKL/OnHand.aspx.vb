﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class OnHand
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property UserID() As Integer
        Get
            Return ViewState("UserID")
        End Get
        Set(ByVal value As Integer)
            ViewState("UserID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UserID = Session("User_ID")
            BindData()
        End If
    End Sub

#Region "Reference"
    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT HD.REF_ID,REF_NO,HD.STATUS,CONVERT(VARCHAR(10),HD.SUBMIT_APPROVE_DATE,120) SUBMIT_APPROVE_DATE" & vbCrLf
        SQL &= ",DPT.DEPARTMENT_NAME REF_FROM_NAME,COUNT(1) AS JOB" & vbCrLf
        SQL &= "FROM REF_HEADER HD" & vbCrLf
        SQL &= "LEFT JOIN MS_DEPARTMENT DPT ON HD.REF_FROM = DPT.DEPARTMENT_ID" & vbCrLf
        SQL &= "LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
        SQL &= "WHERE HD.STATUS = " & MasterControlClass.Ref_Status.Approve & " AND "

        If txtRefNoSort.Text <> "" Then
            SQL &= "REF_NO like '%" & txtRefNoSort.Text.Replace("'", "''") & "%' AND "
        End If
        If txtJobNo.Text <> "" Then
            SQL &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtDateFrom.Text.Trim <> "" Then
            If IsDate(txtDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),DT.SUBMIT_APPROVE_DATE,112) >= '" & txtDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtDateTo.Text.Trim <> "" Then
            If IsDate(txtDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),DT.SUBMIT_APPROVE_DATE,112) <= '" & txtDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If

        SQL = SQL.Substring(0, SQL.Length - 4)
        SQL &= vbCrLf & "AND DT.STATUS <> " & MasterControlClass.Job_Status.Delete
        SQL &= vbCrLf & "GROUP BY HD.REF_ID,REF_NO,HD.STATUS,HD.SUBMIT_APPROVE_DATE,DPT.DEPARTMENT_NAME"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("Reference") = DT
        Navigation.SesssionSourceName = "Reference"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptDataRef
    End Sub

    Protected Sub rptDataRef_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDataRef.ItemCommand
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim RefID As Int32 = lblRefNo.Attributes("REF_ID")
        Dim lblRefDate As Label = e.Item.FindControl("lblRefDate")

        Select Case e.CommandName
            Case "Edit"
                divNewRef.Visible = True
                lblHead.Text = "Receive Reference List"
                txtRefNo.Text = lblRefNo.Text
                txtRefNo.Attributes.Add("REF_ID", RefID)
                BindJob(RefID)
            Case "Delete"
                Dim SQL As String = ""
                SQL = "DELETE FROM JOB_HEADER WHERE JOB_ID IN " & vbCrLf
                SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
                SQL &= "WHERE REF_ID = " & RefID
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
            Case "RevertCloseJob"
                Dim Sql As String = ""
                Sql &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & "  WHERE JOB_ID IN (SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
                Sql &= "DELETE FROM REF_DETAIL WHERE REF_ID = " & RefID & vbCrLf
                Sql &= "DELETE FROM REF_HEADER WHERE REF_ID = " & RefID & vbCrLf
                Dim conn As New SqlConnection(ConnStr)
                Dim SqlCmd As SqlCommand
                conn.Open()
                SqlCmd = New SqlCommand(Sql, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
        End Select
    End Sub

    Protected Sub rptDataRef_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDataRef.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblRefDate As Label = e.Item.FindControl("lblRefDate")
        Dim lblRefFrom As Label = e.Item.FindControl("lblRefFrom")
        Dim lblRefJob As Label = e.Item.FindControl("lblRefJob")

        lblRefNo.Attributes.Add("REF_ID", e.Item.DataItem("REF_ID"))
        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        If e.Item.DataItem("SUBMIT_APPROVE_DATE").ToString <> "" Then
            lblRefDate.Text = e.Item.DataItem("SUBMIT_APPROVE_DATE").ToString
        End If
        lblRefFrom.Text = e.Item.DataItem("REF_FROM_NAME").ToString
        lblRefJob.Text = e.Item.DataItem("JOB").ToString

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        If Session("User_Type_Name") = "Administrator" Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If
    End Sub

    Protected Sub SortData_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefNoSort.TextChanged, txtDateFrom.TextChanged, txtDateTo.TextChanged, txtJobNo.TextChanged
        BindData()
    End Sub
#End Region

    Sub BindJob(ByVal RefID As Int32)
        Dim SQL As String = ""
        SQL &= "SELECT DETAIL_ID,JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),SUBMIT_APPROVE_DATE,126) SUBMIT_APPROVE_DATE,REF_DETAIL.DOC_TYPE_ID,DOC_TYPE_NAME,PAYMENT" & vbCrLf
        SQL &= "FROM REF_DETAIL LEFT JOIN MS_DOC_TYPE ON REF_DETAIL.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & " ORDER BY DETAIL_ID" & vbCrLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Dim RefID As String = ""
        RefID = txtRefNo.Attributes("REF_ID").ToString
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim JobID As Int32 = lblJobNo.Attributes("JOB_ID")

        Select e.CommandName
            Case "Revert"
                Dim Sql As String = ""
                Sql &= "DELETE FROM REF_DETAIL WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
                Sql &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & "  WHERE JOB_ID = " & JobID & vbCrLf

                Dim conn As New SqlConnection(ConnStr)
                Dim SqlCmd As SqlCommand
                conn.Open()
                SqlCmd = New SqlCommand(Sql, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindJob(RefID)
                BindData()
        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblJobDate As Label = e.Item.FindControl("lblJobDate")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        Dim lblPayment As Label = e.Item.FindControl("lblPayment")

        lblJobNo.Attributes.Add("JOB_ID", e.Item.DataItem("JOB_ID").ToString)
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        If e.Item.DataItem("SUBMIT_APPROVE_DATE").ToString <> "" Then
            lblJobDate.Text = e.Item.DataItem("SUBMIT_APPROVE_DATE").ToString
        End If
        lblDocType.Attributes.Add("DOC_TYPE_ID", e.Item.DataItem("DOC_TYPE_ID").ToString)
        lblDocType.Text = e.Item.DataItem("DOC_TYPE_NAME").ToString
        If e.Item.DataItem("PAYMENT").ToString <> "" Then
            lblPayment.Text = FormatNumber(e.Item.DataItem("PAYMENT").ToString, 2)
        End If

        Dim btnView As ImageButton = e.Item.FindControl("btnView")
        Dim UID As String = Now.ToOADate().ToString.Replace(".", "")
        btnView.Attributes.Add("onClick", "window.open('ViewDocument.aspx?JobID=" & e.Item.DataItem("JOB_ID").ToString & "&DocumentType=" & e.Item.DataItem("DOC_TYPE_ID").ToString & "','" & UID & "','fullscreen, scrollbars,resizable,center');")
    End Sub

    Function GetDataDetail() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("JOB_ID")
        DT.Columns.Add("JOB_NO")
        DT.Columns.Add("CUSTOMER")
        DT.Columns.Add("SUBMIT_APPROVE_DATE")
        DT.Columns.Add("DOC_TYPE_ID")
        DT.Columns.Add("DOC_TYPE_NAME")
        DT.Columns.Add("PAYMENT")

        If rptData.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptData.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim lblJobNo As Label = ri.FindControl("lblJobNo")
                Dim lblCustomer As Label = ri.FindControl("lblCustomer")
                Dim lblJobDate As Label = ri.FindControl("lblJobDate")
                Dim lblDocType As Label = ri.FindControl("lblDocType")
                Dim lblPayment As Label = ri.FindControl("lblPayment")

                Dim DR As DataRow = DT.NewRow
                DR("JOB_ID") = lblJobNo.Attributes("JOB_ID")
                DR("JOB_NO") = lblJobNo.Text
                DR("CUSTOMER") = lblCustomer.Text
                DR("SUBMIT_APPROVE_DATE") = lblJobDate.Text
                DR("DOC_TYPE_ID") = lblDocType.Attributes("DOC_TYPE_ID")
                DR("DOC_TYPE_NAME") = lblDocType.Text
                DR("PAYMENT") = lblPayment.Text
                DT.Rows.Add(DR)
            Next
        End If
        Return DT

    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        divNewRef.Visible = False
    End Sub

    Protected Sub btnReceive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReceive.Click
        Dim DT As New DataTable
        DT = GetDataDetail()

        Dim SQL As String = ""
        Dim RefID As String = ""
        RefID = txtRefNo.Attributes("REF_ID").ToString
        Dim conn As New SqlConnection(ConnStr)
        Dim SqlCmd As SqlCommand

        SQL &= "UPDATE REF_HEADER SET STATUS = " & MasterControlClass.Ref_Status.OnHand & vbCrLf
        SQL &= ",RECEIVE_BY = " & UserID & vbCrLf
        SQL &= ",RECEIVE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & vbCrLf

        SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Checked & vbCrLf
        SQL &= ",STATUS_CHECK = " & MasterControlClass.Job_History.Wait & vbCrLf
        SQL &= ",RECEIVE_BY = " & UserID & ",RECEIVE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & vbCrLf

        conn = New SqlConnection(ConnStr)
        conn.Open()
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()
        divNewRef.Visible = False
        BindData()
    End Sub


End Class

﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_User
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlUserType(ddlType, "--------- Select ---------", 0)
            MC.BindDDlDepartment(ddlDepartment, "--------- Select ---------", 0)
            ClearData()
        End If
    End Sub

    Sub ClearData()
        lblHead.Text = "Add User"
        lblId.Text = ""
        txtCode.Text = ""
        txtFullname.Text = ""
        txtPassword.Text = ""
        ddlStatus.SelectedIndex = 0
        ddlType.SelectedIndex = 0
        ddlDepartment.SelectedIndex = 0
        txtAppPassword.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '********* Validation **********
        If txtCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert User Code / Login');", True)
            txtCode.Focus()
            Exit Sub
        End If
        If txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Password');", True)
            txtPassword.Focus()
            Exit Sub
        End If
        If txtFullname.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Fullname');", True)
            txtFullname.Focus()
            Exit Sub
        End If

        If ddlDepartment.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select Department');", True)
            ddlDepartment.Focus()
            Exit Sub
        End If


        If ddlType.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select User Type');", True)
            ddlType.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        If lblHead.Text = "Add User" Then
            SQL = "SELECT * FROM MS_USER WHERE USER_CODE='" & txtCode.Text.Replace("'", "''") & "' AND ACTIVE_STATUS = 1"
        Else
            SQL = "SELECT * FROM MS_USER WHERE USER_CODE='" & txtCode.Text.Replace("'", "''") & "' AND ACTIVE_STATUS = 1 AND USER_ID <> " & lblId.Text
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This User Code / Login is already exists');", True)
            txtCode.Focus()
            Exit Sub
        End If

        If txtAppPassword.Text <> "" Then
            If lblHead.Text = "Add User" Then
                SQL = "SELECT * FROM MS_USER WHERE APP_PASSWORD='" & txtAppPassword.Text.Replace("'", "''") & "' AND ACTIVE_STATUS = 1"
            Else
                SQL = "SELECT * FROM MS_USER WHERE APP_PASSWORD='" & txtAppPassword.Text.Replace("'", "''") & "' AND ACTIVE_STATUS = 1 AND USER_ID <> " & lblId.Text
            End If
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This Approve Password is already exists');", True)
                txtAppPassword.Focus()
                Exit Sub
            End If
        End If

        Dim ActiveStatus As Boolean = False
        If ddlStatus.SelectedIndex = 0 Then
            ActiveStatus = True
        End If

        If lblHead.Text = "Add User" Then
            Dim DR As DataRow = DT.NewRow
            lblId.Text = GL.FindID("MS_USER", "USER_ID")
            DR("USER_ID") = lblId.Text
            DR("USER_CODE") = txtCode.Text
            DR("USER_PASSWORD") = txtPassword.Text
            DR("USER_FULLNAME") = txtFullname.Text
            DR("DEPARTMENT_ID") = ddlDepartment.SelectedValue.ToString
            DR("USER_TYPE_ID") = ddlType.SelectedValue.ToString
            DR("APP_PASSWORD") = txtAppPassword.Text
            DR("ACTIVE_STATUS") = ActiveStatus
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
            DT.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        Else
            SQL = "SELECT * FROM MS_USER WHERE USER_ID = " & lblId.Text
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            Dim DR As DataRow = DT.Rows(0)
            DR("USER_ID") = lblId.Text
            DR("USER_CODE") = txtCode.Text
            DR("USER_PASSWORD") = txtPassword.Text
            DR("USER_FULLNAME") = txtFullname.Text
            DR("DEPARTMENT_ID") = ddlDepartment.SelectedValue.ToString
            DR("USER_TYPE_ID") = ddlType.SelectedValue.ToString
            DR("APP_PASSWORD") = txtAppPassword.Text
            DR("ACTIVE_STATUS") = ActiveStatus
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
        lblHead.Text = "Edit User"
    End Sub

    Protected Sub imgCodeDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCodeDialog.Click
        Dim Script As String = "requireTextboxDialog('UserFinding.aspx',870,540,'" & txtTmpCode.ClientID & "','" & btnCodeDialog.ClientID & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
    End Sub

    Protected Sub btnCodeDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCodeDialog.Click
        Dim SQL As String = "SELECT * FROM MS_USER" & vbNewLine
        SQL &= "WHERE MS_USER.USER_ID = " & txtTmpCode.Text
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('User has not found');", True)
            Exit Sub
        End If
        lblId.Text = DT.Rows(0).Item("USER_ID").ToString
        txtCode.Text = DT.Rows(0).Item("USER_CODE").ToString
        txtFullname.Text = DT.Rows(0).Item("USER_FULLNAME").ToString
        txtPassword.Text = DT.Rows(0).Item("USER_PASSWORD").ToString
        ddlDepartment.SelectedValue = DT.Rows(0).Item("DEPARTMENT_ID").ToString.Trim
        ddlType.SelectedValue = DT.Rows(0).Item("USER_TYPE_ID").ToString.Trim
        txtAppPassword.Text = DT.Rows(0).Item("APP_PASSWORD").ToString
        If DT.Rows(0).Item("ACTIVE_STATUS") = "True" Then
            ddlStatus.SelectedIndex = 0
        Else
            ddlStatus.SelectedIndex = 1
        End If

        lblHead.Text = "Edit User"

    End Sub
End Class

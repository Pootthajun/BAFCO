﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JobFinding.aspx.vb" Inherits="JobFinding" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head id="Head1" runat="server">
    <title>Job Finding</title>
    <link href="style/css.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="Script/script.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManagerMain" runat="Server"></asp:ScriptManager>
        <table width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
            <tr>
                
                <td height="40" align="center" class="Master_Header_Text">
                    Job Finding
                </td>                
            </tr>
        </table>

        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch" style="height:100%; width:100%;">
            <asp:UpdatePanel ID="udp1" runat="server" >
                <ContentTemplate>
                    <table width="750" cellpadding="0" cellspacing="1" style="margin: 10px 0px 0px 0px;" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="40" class="Grid_Header">
                                <asp:Button ID="btnAll" runat="server" Text="All" CssClass="Button_White"/>
                            </td>
                            <td width="100" class="Grid_Header">
                                Job No
                            </td>
                            <td width="350" class="Grid_Header">
                                Customer
                            </td>
                            <td width="100" class="Grid_Header">
                                Close Date
                            </td>
                            <td width="120" class="Grid_Header">
                                Document Type
                            </td>
                        </tr>
                        <tr>
                            <td height="25px">
                                <asp:Button ID="btnSearch" runat="server" style="display:none;"/>
                            </td>
                            <td height="25px">
                                <asp:TextBox ID="txtJobNo" runat="server" Width="100%" CssClass="Textbox_Form_White" AutoPostBack="true"></asp:TextBox>
                            </td>
                            <td height="25px">
                                <asp:TextBox ID="txtCustomer" runat="server" Width="100%" CssClass="Textbox_Form_White" AutoPostBack="true"></asp:TextBox>
                            </td>
                            <td height="25px"></td>
                            <td height="25px"></td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trRow" runat="server" style="border-bottom:solid 1px #efefef;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td class="Grid_Detail" align="center">
                                        <asp:CheckBox ID="cb" runat="server" />
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" style="text-align:center;">
                                        <asp:Label ID="lblDate" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" style="text-align:center;">
                                        <asp:Label ID="lblDocType" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <table width="750" cellpadding="0" cellspacing="0" style="margin: 10px 0px 0px 0px; background-color: #FFFFFF;">
                        <tr>
                            <td width="40" style="text-align:right;">
                                <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="Button_Red" Width="100px" Height="30px"/>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                        </tr>
                    </table>      
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </form>
</body>

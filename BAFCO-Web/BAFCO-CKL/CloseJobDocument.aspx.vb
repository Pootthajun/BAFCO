﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class CloseJobDocument
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Private Property JobID() As Integer
        Get
            If IsNumeric(ViewState("JobID")) Then
                Return ViewState("JobID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("JobID") = value
        End Set
    End Property

    Private Property DocumentType() As Integer
        Get
            If IsNumeric(ViewState("DocumentType")) Then
                Return ViewState("DocumentType")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DocumentType") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            JobID = Request.QueryString("JobID")
            DocumentType = Request.QueryString("DocumentType")
            Document.ViewDocument()
        End If
    End Sub
End Class

﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_Customer
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim CL As New textControlLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = "SELECT DISTINCT CUSTOMER FROM JOB_HEADER" & vbLf
        SQL &= " ORDER BY CUSTOMER" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Columns.Add("edit_mode", GetType(String), "'Edit'")
        DT.Columns.Add("view_mode", GetType(String), "'View'")
        '--------------- Filter ----------------------
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim txtName As TextBox = e.Item.FindControl("txtName")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim btnSave As ImageButton = e.Item.FindControl("btnSave")
        Dim btnCancel As ImageButton = e.Item.FindControl("btnCancel")

        Dim trView As HtmlTableRow = e.Item.FindControl("trView")
        Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")


        lblNo.Text = e.Item.ItemIndex + 1
        lblName.Text = e.Item.DataItem("CUSTOMER").ToString
        txtName.Text = e.Item.DataItem("CUSTOMER").ToString

        '--------------- เปิด ปิด ฟังก์ชั่น Edit -----------
        trView.Visible = e.Item.DataItem("view_mode") = "View"
        trEdit.Visible = e.Item.DataItem("view_mode") = "Edit"

        '---------------- เก็บ Mode Edit,Add----------------
        Dim imgEdit As Image = e.Item.FindControl("imgEdit")
        imgEdit.Attributes("Mode") = e.Item.DataItem("edit_mode")

    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim txtName As TextBox = e.Item.FindControl("txtName")
                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                trView.Visible = False
                trEdit.Visible = True
                txtName.Focus()
            Case "Save"
                Dim txtName As TextBox = e.Item.FindControl("txtName")
                Dim lblName As Label = e.Item.FindControl("lblName")

                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")

                If txtName.Text.Trim = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Customer');", True)
                    txtName.Focus()
                    Exit Sub
                End If

                Dim SQL As String = ""
                SQL &= "UPDATE JOB_HEADER SET CUSTOMER = '" & txtName.Text.Replace("'", "''") & "' WHERE CUSTOMER = '" & lblName.Text & "'" & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET CUSTOMER = '" & txtName.Text.Replace("'", "''") & "' WHERE CUSTOMER = '" & lblName.Text & "'" & vbCrLf
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
                BindData()
                trView.Visible = True
                trEdit.Visible = False
            Case "Cancel"
                Dim lblName As Label = e.Item.FindControl("lblName")
                '----------------- คืนค่า ----------------
                Dim txtName As TextBox = e.Item.FindControl("txtName")
                txtName.Text = lblName.Text
                '---------------- กลับไป ตารางเดิม --------
                Dim imgEdit As Image = e.Item.FindControl("imgEdit")
                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                trView.Visible = True
                trEdit.Visible = False
        End Select
    End Sub

End Class

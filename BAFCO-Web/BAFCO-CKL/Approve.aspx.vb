﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Approve
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property UserID() As Integer
        Get
            Return ViewState("UserID")
        End Get
        Set(ByVal value As Integer)
            ViewState("UserID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UserID = Session("User_ID")
            AddLineItem()
            BindData()
        End If
    End Sub

#Region "Approve"
    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT APP_NO,CONVERT(VARCHAR(10),APP_CREATE_DATE,120) APP_DATE,COUNT(1) AS JOB,USER_FULLNAME" & vbCrLf
        SQL &= "FROM REF_DETAIL" & vbCrLf
        SQL &= "LEFT JOIN MS_USER ON REF_DETAIL.APP_CREATE_BY = MS_USER.USER_ID" & vbCrLf
        SQL &= "WHERE STATUS = " & MasterControlClass.Job_Status.Approving & " AND "
        SQL &= "STATUS_APP = " & MasterControlClass.Job_History.CreateApprove & " AND "
        If txtAppNoSort.Text <> "" Then
            SQL &= "APP_NO like '%" & txtAppNoSort.Text.Replace("'", "''") & "%' AND "
        End If

        If txtDateFrom.Text.Trim <> "" Then
            If IsDate(txtDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),APP_CREATE_DATE,112) >= '" & txtDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtDateTo.Text.Trim <> "" Then
            If IsDate(txtDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),APP_CREATE_DATE,112) <= '" & txtDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If

        SQL = SQL.Substring(0, SQL.Length - 4)
        SQL &= vbCrLf & "GROUP BY APP_NO,CONVERT(VARCHAR(10),APP_CREATE_DATE,120),USER_FULLNAME"
        SQL &= vbCrLf & "ORDER BY APP_NO DESC"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("APP") = DT
        Navigation.SesssionSourceName = "APP"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptDataRef
    End Sub

    Protected Sub rptDataRef_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDataRef.ItemCommand
        Dim lblAppNo As Label = e.Item.FindControl("lblAppNo")
        Dim lblAppDate As Label = e.Item.FindControl("lblAppDate")
        Select e.CommandName
            Case "Edit"
                divNewRef.Visible = True
                lblHead.Text = "Edit Approval List"
                txtAppNo.Text = lblAppNo.Text
                txtAppNo.Enabled = True
                txtAppNo.CssClass = "Textbox_Form_Disable"
                txtDate.Text = lblAppDate.Text

                btnSave.Visible = True
                btnCreate.Visible = False
                BindJob(lblAppNo.Text)

                PanelData.Enabled = True
            Case "Approve"
                Approve(lblAppNo.Text)
            Case "Print"
                Print(lblAppNo.Text)
            Case "RevertToWait"
                Dim SQL As String = ""
                SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= ",APP_NO = NULL" & vbCrLf
                SQL &= ",APP_REMARK = NULL" & vbCrLf
                SQL &= ",APP_CREATE_BY = NULL" & vbCrLf
                SQL &= ",APP_CREATE_DATE = NULL" & vbCrLf
                SQL &= "WHERE APP_NO = '" & lblAppNo.Text & "'"
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()

            Case "Delete"
                Dim SQL As String = ""
                SQL = "DELETE FROM JOB_HEADER WHERE JOB_ID IN " & vbCrLf
                SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE APP_NO = " & lblAppNo.Text & ")" & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
                SQL &= "WHERE APP_NO = " & lblAppNo.Text
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
        End Select
    End Sub

    Protected Sub rptDataRef_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDataRef.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblAppNo As Label = e.Item.FindControl("lblAppNo")
        Dim lblAppDate As Label = e.Item.FindControl("lblAppDate")
        Dim lblAppJob As Label = e.Item.FindControl("lblAppJob")
        Dim lblAppBy As Label = e.Item.FindControl("lblAppBy")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblAppNo.Text = e.Item.DataItem("APP_NO").ToString
        lblAppJob.Text = e.Item.DataItem("JOB").ToString
        lblAppDate.Text = e.Item.DataItem("APP_DATE").ToString
        lblAppBy.Text = e.Item.DataItem("USER_FULLNAME").ToString

        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        If Session("User_Type_Name") = "Administrator" Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If
    End Sub

    Protected Sub SortData_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAppNoSort.TextChanged, txtDateFrom.TextChanged, txtDateTo.TextChanged
        BindData()
    End Sub
#End Region

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        divNewRef.Visible = True
        lblHead.Text = "Create New Approval List"
        txtAppNo.Text = "Auto Number"
        txtAppNo.ReadOnly = True
        txtAppNo.CssClass = "Textbox_Form_Disable"
        txtDate.Text = GL.ReportProgrammingDate(Date.Now)
        btnSave.Visible = False
        btnCreate.Visible = True
        ClearData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        divNewRef.Visible = False
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Dialog"
                Dim txtTempJobNo As TextBox = e.Item.FindControl("txtTempJobNo")
                Dim btnJobNoDialog As Button = e.Item.FindControl("btnJobNoDialog")
                Dim JobID As String = ""
                Dim DT As New DataTable
                DT = GetDataDetail()
                DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
                DT = DT.DefaultView.ToTable
                If DT.Rows.Count > 0 Then
                    For i As Int32 = 0 To DT.Rows.Count - 1
                        JobID = JobID & DT.Rows(i).Item("JOB_ID").ToString & ","
                    Next
                    JobID = JobID.Substring(0, JobID.Length - 1)
                End If
                Dim Mode As String = "New"
                If txtAppNo.Text <> "Auto Number" Then
                    Mode = "Edit"
                End If
                Dim Script As String = "requireTextboxDialog('JobAppFinding.aspx?JobID=" & JobID & "&AppNo=" & txtAppNo.Text & "&Mode=" & Mode & "',766,580,'" & txtTempJobNo.ClientID & "','" & btnJobNoDialog.ClientID & "');"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
            Case "Select"
                Dim txtTempJobNo As TextBox = e.Item.FindControl("txtTempJobNo")
                Dim SQL As String = ""
                SQL = "SELECT JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),PRICE_DATE,126) PRICE_DATE,APP_REMARK" & vbCrLf
                SQL &= "FROM REF_DETAIL " & vbCrLf
                SQL &= "WHERE JOB_ID IN (" & txtTempJobNo.Text & ")"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim TEMP As New DataTable
                DA.Fill(TEMP)

                Dim DT As New DataTable
                DT = GetDataDetail()
                For i As Int32 = 0 To TEMP.Rows.Count - 1
                    Dim DR As DataRow
                    DR = DT.NewRow
                    DR("JOB_ID") = TEMP.Rows(i).Item("JOB_ID").ToString
                    DR("JOB_NO") = TEMP.Rows(i).Item("JOB_NO").ToString
                    DR("CUSTOMER") = TEMP.Rows(i).Item("CUSTOMER").ToString
                    DR("PRICE_DATE") = TEMP.Rows(i).Item("PRICE_DATE").ToString
                    DR("APP_REMARK") = TEMP.Rows(i).Item("APP_REMARK").ToString
                    DT.Rows.Add(DR)
                Next

                DT.DefaultView.Sort = "JOB_NO ASC"
                DT = DT.DefaultView.ToTable

                rptData.DataSource = DT
                rptData.DataBind()
                AddLineItem()

            Case "Delete"
                Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
                If lblJobNo.Text = "" Then
                    Exit Sub
                End If


                Dim SQL As String = ""
                SQL = "DELETE FROM JOB_HEADER WHERE JOB_ID IN" & vbCrLf
                SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE JOB_NO = '" & lblJobNo.Text & "')" & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
                SQL &= "WHERE JOB_NO = '" & lblJobNo.Text & "'"
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()


                Dim DT As New DataTable
                DT = GetDataDetail()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
                AddLineItem()
            Case "RevertJobToWait"
                Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
                If lblJobNo.Text = "" Then
                    Exit Sub
                End If

                Dim SQL As String = ""
                SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= ",APP_NO = NULL" & vbCrLf
                SQL &= ",APP_REMARK = NULL" & vbCrLf
                SQL &= ",APP_CREATE_BY = NULL" & vbCrLf
                SQL &= ",APP_CREATE_DATE = NULL" & vbCrLf
                SQL &= "WHERE JOB_NO = '" & lblJobNo.Text & "'"
                SQL &= "AND STATUS = " & MasterControlClass.Job_Status.Approving & vbCrLf
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()

                Dim DT As New DataTable
                DT = GetDataDetail()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
                AddLineItem()

        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblPriceDate As Label = e.Item.FindControl("lblPriceDate")
        Dim txtRemark As TextBox = e.Item.FindControl("txtRemark")
        txtRemark.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
        lblJobNo.Attributes.Add("JOB_ID", e.Item.DataItem("JOB_ID").ToString)
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        If e.Item.DataItem("PRICE_DATE").ToString <> "" Then
            lblPriceDate.Text = e.Item.DataItem("PRICE_DATE").ToString
        End If
        If e.Item.DataItem("APP_REMARK").ToString <> "" Then
            txtRemark.Text = e.Item.DataItem("APP_REMARK").ToString
        End If

        Dim btnAdd As ImageButton = e.Item.FindControl("btnAdd")
        If lblJobNo.Text = "" Then
            btnAdd.ImageUrl = "images/Icon/01.png"
        Else
            btnAdd.ImageUrl = "images/Icon/65.png"
        End If

        If btnSave.Visible = False And btnCreate.Visible = False Then
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
            btnAdd.Visible = False
            btnDelete.Visible = False
        End If
    End Sub

    Function GetDataDetail() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("JOB_ID")
        DT.Columns.Add("JOB_NO")
        DT.Columns.Add("CUSTOMER")
        DT.Columns.Add("PRICE_DATE")
        DT.Columns.Add("APP_REMARK")

        If rptData.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptData.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim lblJobNo As Label = ri.FindControl("lblJobNo")
                Dim lblCustomer As Label = ri.FindControl("lblCustomer")
                Dim lblPriceDate As Label = ri.FindControl("lblPriceDate")
                Dim txtRemark As TextBox = ri.FindControl("txtRemark")

                Dim DR As DataRow = DT.NewRow
                DR("JOB_ID") = lblJobNo.Attributes("JOB_ID")
                DR("JOB_NO") = lblJobNo.Text
                DR("CUSTOMER") = lblCustomer.Text
                DR("PRICE_DATE") = lblPriceDate.Text
                DR("APP_REMARK") = txtRemark.Text
                DT.Rows.Add(DR)
            Next
        End If
        Return DT

    End Function

    Sub AddLineItem()
        Dim DT As New DataTable
        DT = GetDataDetail()
        DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
        DT = DT.DefaultView.ToTable

        Dim DR As DataRow
        DR = DT.NewRow
        DR("JOB_ID") = ""
        DR("JOB_NO") = ""
        DR("CUSTOMER") = ""
        DR("PRICE_DATE") = ""
        DR("APP_REMARK") = ""
        DT.Rows.Add(DR)

        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Sub ClearData()
        rptData.DataSource = ""
        rptData.DataBind()
        AddLineItem()
    End Sub

    Sub BindJob(ByVal AppNo As String)
        Dim SQL As String = ""
        SQL = "SELECT JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),PRICE_DATE,126) PRICE_DATE,APP_REMARK" & vbCrLf
        SQL &= "FROM REF_DETAIL " & vbCrLf
        SQL &= "WHERE APP_NO = '" & AppNo & "'"
        SQL &= "AND STATUS = " & MasterControlClass.Job_Status.Approving
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
        AddLineItem()
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        If SaveData() = False Then
            Exit Sub
        End If
        divNewRef.Visible = False
        BindData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SaveData() = False Then
            Exit Sub
        End If
        divNewRef.Visible = False
        BindData()
    End Sub

    Function Validation() As Boolean

        Dim JobID As String = ""
        Dim DT As New DataTable
        DT = GetDataDetail()
        DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
        DT = DT.DefaultView.ToTable
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select job(s)');", True)
            Return False
        Else
            For i As Int32 = 0 To DT.Rows.Count - 1
                JobID &= DT.Rows(i).Item("JOB_ID").ToString & ","
            Next
            JobID = JobID.Substring(0, JobID.Length - 1)
        End If

        Dim SQL As String = ""
        SQL &= "SELECT JOB_ID,JOB_NO FROM REF_DETAIL " & vbCrLf
        SQL &= "WHERE STATUS = " & MasterControlClass.Job_Status.Approving & vbCrLf
        SQL &= "AND STATUS_APP = " & MasterControlClass.Job_History.CreateApprove & vbCrLf
        SQL &= "AND JOB_ID IN (" & JobID & ")" & vbCrLf
        If txtAppNo.Text <> "Auto Number" Then
            SQL &= "AND APP_NO = '" & txtAppNo.Attributes("APP_NO") & "'"
        End If
        Dim DT_ As New DataTable
        Dim DA_ As New SqlDataAdapter(SQL, ConnStr)
        DA_.Fill(DT_)
        If DT_.Rows.Count > 0 Then
            Dim JobNo As String = ""
            For i As Int32 = 0 To DT_.Rows.Count - 1
                JobNo = JobNo + DT_.Rows(i).Item("JOB_NO").ToString & ","
            Next
            JobNo = JobNo.Substring(0, JobNo.Length - 1)
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Job No " & JobNo & " has updated');", True)
            Return False
        End If
        Return True
    End Function

    Function SaveData() As Boolean
        If Validation() = False Then
            Return False
        End If

        Dim JobID As String = ""
        Dim DT As New DataTable
        DT = GetDataDetail()
        DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
        DT = DT.DefaultView.ToTable
        For i As Int32 = 0 To DT.Rows.Count - 1
            JobID &= DT.Rows(i).Item("JOB_ID").ToString & ","
        Next
        JobID = JobID.Substring(0, JobID.Length - 1)

        Dim SQL As String = ""
        Dim cmd As New SqlCommandBuilder
        Dim AppNO As String = ""
        If txtAppNo.Text = "Auto Number" Then
            Dim yy As Int32 = Date.Now.Year
            Dim mm As Int32 = Date.Now.Month
            'Dim dd As Int32 = Date.Now.Day
            Dim ID As String
            If yy > 2500 Then
                yy = yy - 543
            End If
            ID = yy.ToString & mm.ToString.PadLeft(2, "0")
            'SQL = "SELECT MAX(APP_NO) MAX_NO FROM REF_DETAIL WHERE APP_NO LIKE'%" & ID & "%' GROUP BY APP_NO"
            SQL = "select REPLACE(STR(convert(int,isnull(SUBSTRING(max(APP_NO),7,3),0)) +1, 3), SPACE(1), '0') as MAX_NO"
            SQL += " from REF_DETAIL WHERE APP_NO LIKE'%" & ID & "%'"
            Dim DT_ As New DataTable
            Dim DA_ As New SqlDataAdapter(SQL, ConnStr)
            DA_.Fill(DT_)
            If DT_.Rows.Count = 0 Then
                AppNO = ID & "001"
            Else
                AppNO = ID & CStr(DT_.Rows(0).Item("MAX_NO"))
            End If
        Else
            AppNO = txtAppNo.Text
            'SQL = ""
            'SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
            'SQL &= ",APP_NO = NULL" & vbCrLf
            'SQL &= ",APP_REMARK = NULL" & vbCrLf
            'SQL &= ",APP_CREATE_BY = NULL" & vbCrLf
            'SQL &= ",APP_CREATE_DATE = NULL" & vbCrLf
            'SQL &= "WHERE JOB_ID NOT IN (" & JobID & ")" & vbCrLf
            'SQL &= "AND APP_NO = '" & AppNO & "'" & vbCrLf
            'SQL &= "AND STATUS = " & MasterControlClass.Job_Status.Approving & vbCrLf
            'Dim conn As New SqlConnection(ConnStr)
            'conn.Open()
            'Dim SqlCmd As SqlCommand
            'SqlCmd = New SqlCommand(SQL, conn)
            'SqlCmd.ExecuteNonQuery()
            'conn.Close()
        End If

        For i As Int32 = 0 To DT.Rows.Count - 1
            SQL = ""
            SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.CreateApprove & vbCrLf
            SQL &= ",APP_NO = '" & AppNO & "'" & vbCrLf
            SQL &= ",APP_REMARK = '" & DT.Rows(i).Item("APP_REMARK").ToString & "'" & vbCrLf
            SQL &= ",APP_CREATE_BY = " & UserID & vbCrLf
            SQL &= ",APP_CREATE_DATE = GETDATE()" & vbCrLf
            SQL &= "WHERE JOB_ID = " & DT.Rows(i).Item("JOB_ID").ToString
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            Dim SqlCmd As SqlCommand
            SqlCmd = New SqlCommand(SQL, conn)
            SqlCmd.ExecuteNonQuery()
            conn.Close()
        Next
        Return True
    End Function

    Sub Approve(ByVal AppNo As String)
        Dim SQL As String = ""
        SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Billed & vbCrLf
        SQL &= ",STATUS_BILL = " & MasterControlClass.Job_History.Wait & vbCrLf
        SQL &= ",APP_BY = " & UserID & ",APP_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE APP_NO = '" & AppNo & "'" & vbCrLf
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()
        BindData()
    End Sub

    Sub Print(ByVal AppNo As String)
        Dim SQL As String = ""
        SQL &= "SELECT APP_NO,JOB_ID,JOB_NO,CUSTOMER,APP_REMARK," & vbCrLf
        SQL &= "CONVERT(VARCHAR(10),OPEN_DATE,120) OPEN_DATE," & vbCrLf
        SQL &= "CONVERT(VARCHAR(10),CLOSE_DATE,120) CLOSE_DATE," & vbCrLf
        SQL &= "DATEDIFF(D,OPEN_DATE,CLOSE_DATE) D_CLOSE," & vbCrLf
        SQL &= "CONVERT(VARCHAR(10),SUBMIT_DATE,120) SUBMIT_DATE," & vbCrLf
        SQL &= "DATEDIFF(D,CLOSE_DATE,SUBMIT_DATE) D_SUBMIT," & vbCrLf
        SQL &= "CONVERT(VARCHAR(10),RECEIVE_DATE,120) RECEIVE_DATE," & vbCrLf
        SQL &= "DATEDIFF(D,SUBMIT_DATE,RECEIVE_DATE) D_RECEIVE," & vbCrLf
        SQL &= "CONVERT(VARCHAR(10),CHECK_DATE,120) CHECK_DATE," & vbCrLf
        SQL &= "DATEDIFF(D,RECEIVE_DATE,CHECK_DATE) D_CHECK," & vbCrLf
        SQL &= "CONVERT(VARCHAR(10),PRICE_DATE,120) PRICE_DATE," & vbCrLf
        SQL &= "DATEDIFF(D,CHECK_DATE,PRICE_DATE) D_PRICE," & vbCrLf
        SQL &= "CONVERT(VARCHAR(10),GETDATE(),120) PRINT_DATE" & vbCrLf
        SQL &= "FROM REF_DETAIL " & vbCrLf
        SQL &= "WHERE APP_NO = '" & AppNo & "'" & vbCrLf
        SQL &= "ORDER BY JOB_NO" & vbCrLf
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        Dim UID As String = Now.ToOADate().ToString.Replace(".", "")
        Session("Print_" & UID) = DT
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "openPrintWindow('Print/Approve.aspx?Print_ID=" & UID & "',800,500);", True)
    End Sub

End Class

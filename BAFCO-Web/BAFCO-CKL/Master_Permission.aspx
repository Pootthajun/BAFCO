﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_Permission.aspx.vb" Inherits="Master_Permission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" cellspacing="5">
    <tr>
        <td height="40" colspan="4" class="Top_Menu_Bar">
            <ul class="shortcut-buttons-set">
                <li>
                    <asp:LinkButton ID="btnClear" runat="server" CssClass="shortcut-button"> <span><img src="images/icon/41.png"/><br />Clear</span> </asp:LinkButton>
                </li>
                <li>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="shortcut-button"> <span><img src="images/icon/59.png"/><br />
                         Save </span></asp:LinkButton>
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td width="450" valign="top">
            <div class="Fieldset_Container" >
                <table width="500" cellspacing="5px;" style="margin-left:15px; margin-bottom:10px; margin-top:10px; margin-right:15px;">
                    <tr>
                        <td  width="100" height="25" class="NormalTextBlack">
                            User Type 
                        </td>
                        <td width="100" height="25">
                            <asp:DropDownList ID="ddlType" runat="server" CssClass="Dropdown_Form_White" 
                            width="205px" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="800" align="left" cellpadding="0" cellspacing="1" style="margin:10px 0px 0px 0px;" bgColor="#CCCCCC" class="NormalText">
                                <tr>
                                    <td  width="40" class="Grid_Header">
                                        No.
                                    </td>
                                    <td width="200" class="Grid_Header">
                                        Menu
                                    </td>
                                    <td  width="60" class="Grid_Header">
                                       Active
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptData" runat="server">
                                    <ItemTemplate>
                                        <tr id="tbTag" runat="server" style="cursor: pointer; border-bottom: solid 1px #efefef">
                                            <td class="Grid_Detail" align="center">
                                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                                                <asp:Label ID="lblMenuID" runat="server" Visible="false"></asp:Label>
                                            </td>
                                            <td class="Grid_Detail" align="left">
                                                <asp:Label ID="lblMenuName" runat="server"></asp:Label>
                                            </td>
                                            <td class="Grid_Detail" align="center" style="background-color:White">
                                                <asp:CheckBox ID="cbActive" runat="server" Text="" ></asp:CheckBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table> 
            </div>
        </td>
        <td></td>
    </tr>
</table>
</asp:Content>


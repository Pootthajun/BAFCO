﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Document.ascx.vb" Inherits="Document" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="text-align:left; vertical-align:top;">
                    <table style="font-family: Tahoma; font-size: 18px; color:Black; background-color:White; width:1000px;">
                        <tr>
                            <td style="font-weight:bold; text-align:center; height:30px;">
                                <asp:Label ID="lblHeader" runat="server" Text="Label"></asp:Label>
                                <asp:Label ID="lblJobID" runat="server" Text="Label" style="display:none;"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table style="font-family: Tahoma; font-size: 14px; color:Black; background-color:White; width:100%;">
                                    <tr>
                                        <td width="135px" align="left" style="font-weight:bold;">
                                            CUSTOMER NAME
                                        </td>
                                        <td style="border-bottom:solid 1px #CCCCCC; text-align:left;">
                                            <asp:Label ID="lblCustomer" runat="server" Text="-"></asp:Label>
                                        </td>
                                        <td width="60px" align="center" style="font-weight:bold;">
                                            JOB NO
                                        </td>
                                        <td style="border-bottom:solid 1px #CCCCCC; width:150px;  text-align:left;">
                                            <asp:Label ID="lblJobNo" runat="server" Text="-"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:solid 1px Black;"></td>
                        </tr>
                        <tr id="trEntryType1" runat="server">
                            <td style="font-family: Tahoma; font-size: 14px; color:Black; font-weight:bold; text-align:left; height:25px;">
                                ประเภทใบขน
                            </td>
                        </tr>
                        <tr id="trEntryType2" runat="server">
                            <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                <table cellpadding="0" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptEntryType" runat="server">
                                                <ItemTemplate>
                                                    <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td width="20px">
                                                                        <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td width="230px">
                                                                        <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td width="20px">
                                                                        <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td width="230px">
                                                                        <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td width="20px">
                                                                        <asp:CheckBox ID="cbC3" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td width="230px">
                                                                        <asp:Label ID="lbl_C3_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td width="20px">
                                                                        <asp:CheckBox ID="cbC4" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td width="230px">
                                                                        <asp:Label ID="lbl_C4_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>            
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>        
                            </td>
                        </tr>
                        <tr id="trEntryType3" runat="server">
                            <td style="border-bottom:solid 1px Black; height:5px;"></td>
                        </tr>
                        <tr id="trJobType1" runat="server">
                            <td style="font-family: Tahoma; font-size: 14px; color:Black; font-weight:bold; text-align:left; height:25px;">
                                ประเภทงาน
                            </td>
                        </tr>
                        <tr id="trJobType2" runat="server">
                            <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                <table cellpadding="0" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptJobType" runat="server">
                                                <ItemTemplate>
                                                    <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                        <td style="text-align:left; width:500px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="500px">
                                                                <tr style="vertical-align:top;">
                                                                    <td width=20px">
                                                                        <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="text-align:left; width:500px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0" width="500px">
                                                                <tr style="vertical-align:top;">
                                                                    <td width=20px">
                                                                        <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_1" runat="server" CommandName="JOB_TYPE_txt_C2_1" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_2" runat="server" CommandName="JOB_TYPE_txt_C2_2" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_3" runat="server" CommandName="JOB_TYPE_txt_C2_3" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_4" runat="server" CommandName="JOB_TYPE_txt_C2_4" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_5" runat="server" CommandName="JOB_TYPE_txt_C2_5" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_6" runat="server" CommandName="JOB_TYPE_txt_C2_6" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_7" runat="server" CommandName="JOB_TYPE_txt_C2_7" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_8" runat="server" CommandName="JOB_TYPE_txt_C2_8" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_9" runat="server" CommandName="JOB_TYPE_txt_C2_9" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Button ID="btn_txt_C2_10" runat="server" CommandName="JOB_TYPE_txt_C2_10" style="display:none;"></asp:Button>
                                                                        <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>           
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>        
                            </td>
                        </tr>
                        <tr id="trJobType3" runat="server">
                            <td style="border-bottom:solid 1px Black; height:5px;"></td>
                        </tr>
                        <tr id="trFee1" runat="server">
                            <td style="font-family: Tahoma; font-size: 13px; color:Black; font-weight:bold; text-align:left; height:10px;">
                                &nbsp;
                            </td>
                        </tr>
                        <tr id="trFee2" runat="server">
                            <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                <table cellpadding="0" cellspacing="1">
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptFee" runat="server">
                                                <ItemTemplate>
                                                    <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td>
                                                                        <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td>
                                                                        <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td>
                                                                        <asp:CheckBox ID="cbC3" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbl_C3_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C3_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C3_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="text-align:left; width:250px; vertical-align:top;">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr style="vertical-align:top;">
                                                                    <td>
                                                                        <asp:CheckBox ID="cbC4" runat="server" Visible="false"/>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lbl_C4_1" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_2" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_3" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_4" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_5" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_6" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_7" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_8" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_9" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_10" runat="server"></asp:Label>
                                                                        <asp:TextBox ID="txt_C4_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                        <asp:Label ID="lbl_C4_11" runat="server"></asp:Label>
                                                                    </td>       
                                                                </tr>
                                                            </table>
                                                        </td>            
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                                            
                            </td>
                        </tr>
                        <tr id="trFee3" runat="server">
                            <td style="border-bottom:solid 1px Black; height:5px;"></td>
                        </tr>
                        <tr id="trJob1" runat="server">
                            <td>
                                <table cellpadding="0" cellspacing="0" style="font-family: Tahoma; font-size: 14px; color:Black; background-color:White; width:100%;">
                                    <tr>
                                        <td width="500px" align="left" style="font-weight:bold; height:25px;">
                                            เอกสารแนบ Job
                                        </td>
                                        <td width="500px" align="left" style="font-weight:bold; height:25px;">
                                            เฉพาะแผนกบัญชี
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top;">
                                            <table cellpadding="0" cellspacing="1">
                                                <tr>
                                                    <td>
                                                        <asp:Repeater ID="rptJobDoc" runat="server">
                                                            <ItemTemplate>
                                                                <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                    <td style="text-align:left; vertical-align:top;">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tr style="vertical-align:top;">
                                                                                <td width="20px">
                                                                                    <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                </td>
                                                                                <td width="480px">
                                                                                    <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                </td>       
                                                                            </tr>
                                                                        </table>
                                                                    </td>        
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; text-align:left; vertical-align:top; padding-left:5px;">
                                            <table cellpadding="0" cellspacing="1">
                                                <tr>
                                                    <td>
                                                        <asp:Repeater ID="rptAcc" runat="server">
                                                            <ItemTemplate>
                                                                <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                    <td style="text-align:left; vertical-align:top;">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tr style="vertical-align:top;">
                                                                                <td width="500px">
                                                                                    <asp:Label ID="lblNo" runat="server"></asp:Label>&nbsp;&nbsp;
                                                                                    <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                </td>       
                                                                            </tr>
                                                                        </table>
                                                                    </td>        
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trJob2" runat="server">
                            <td style="border-bottom:solid 1px Black; height:5px;"></td>
                        </tr>
                        <tr id="trVehicle1" runat="server">
                            <td>
                                <table cellpadding="0" cellspacing="0" style="font-family: Tahoma; font-size: 14px; color:Black; background-color:White; width:100%;">
                                    <tr>
                                        <td colspan="3" style="font-weight:bold; height:25px;">
                                            รายละเอียดการขนส่ง
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold; height:30px; width:300px; border-right:solid 1px Black;">
                                            รถ Bafco
                                        </td>
                                        <td style="font-weight:bold; height:30px; width:300px; border-right:solid 1px Black; padding-left:10px;">
                                            รถจ้าง
                                        </td>
                                        <td style="font-weight:bold; height:30px; padding-left:10px;">
                                            ชื่อรถจ้าง
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td style="border-right:solid 1px Black;">
                                            <table cellpadding="0" cellspacing="1">
                                                <tr>
                                                    <td>
                                                        <asp:Repeater ID="rptBafco" runat="server">
                                                            <ItemTemplate>
                                                                <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                    <td style="text-align:left; vertical-align:top;">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tr style="vertical-align:top;">
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                </td>       
                                                                            </tr>
                                                                        </table>
                                                                    </td>        
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="border-right:solid 1px Black; padding-left:10px;">
                                            <table cellpadding="0" cellspacing="1">
                                                <tr>
                                                    <td>
                                                        <asp:Repeater ID="rptEmploy" runat="server">
                                                            <ItemTemplate>
                                                                <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                    <td style="text-align:left; vertical-align:top;">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tr style="vertical-align:top;">
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                </td>       
                                                                            </tr>
                                                                        </table>
                                                                    </td>        
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="padding-left:10px;">
                                            <table cellpadding="0" cellspacing="1">
                                                <tr>
                                                    <td>
                                                        <asp:Repeater ID="rptNameEmploy" runat="server">
                                                            <ItemTemplate>
                                                                <tr style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                                    <td style="text-align:left; width:250px; vertical-align:top;">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tr style="vertical-align:top;">
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbC1" runat="server" Visible="false"/>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lbl_C1_1" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_2" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_3" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_4" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_5" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_6" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_7" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_8" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_9" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_10" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C1_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C1_11" runat="server"></asp:Label>
                                                                                </td>       
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td style="text-align:left; width:250px; vertical-align:top;">
                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                            <tr style="vertical-align:top;">
                                                                                <td>
                                                                                    <asp:CheckBox ID="cbC2" runat="server" Visible="false"/>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lbl_C2_1" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_1" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_2" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_2" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_3" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_3" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_4" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_4" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_5" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_5" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_6" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_6" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_7" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_7" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_8" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_8" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_9" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_9" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_10" runat="server"></asp:Label>
                                                                                    <asp:TextBox ID="txt_C2_10" runat="server" CssClass="txtValue" Visible="false"></asp:TextBox>
                                                                                    <asp:Label ID="lbl_C2_11" runat="server"></asp:Label>
                                                                                </td>       
                                                                            </tr>
                                                                        </table>
                                                                    </td>          
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trVehicle2" runat="server">
                            <td style="border-bottom:solid 1px Black; height:5px;"></td>
                        </tr>
                        <tr id="trRemark1" runat="server">  
                            <td>
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="font-family: Tahoma; font-size: 14px; color:Black; font-weight:bold; text-align:left; height:25px;">
                                            Remark&nbsp;&nbsp;
                                        </td>
                                        <td style="font-family: Tahoma; font-size: 14px; color:Black; text-align:left; height:25px;" Width="100%">
                                      
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="trRemark2" runat="server">
                            <td style="font-family: Tahoma; font-size: 14px; color:Black; height:25px;">
                                <asp:TextBox ID="txtRemark1" runat="server" style="text-align:left;" 
                                    CssClass="txtMiltiLineValue" Width="100%" Height="220px" TextMode="MultiLine" Columns="50" Rows="5"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trSave" runat="server" visible="false">
                            <td id="tdButton" style="text-align:right; height:25px;">
                                <input runat="server" type="button" class="Button_Red" id="btnPrintEdit" value="Print" style="width:120px; height:30px;" onclick="doPrint();" />
                                <asp:Button ID="btnPrintView" CssClass="Button_Red" runat="server" Text="Print" Width="120px" Height="30px"/>
                                <asp:Button ID="btnSave" CssClass="Button_Red" runat="server" Text="Save" Width="120px" Height="30px"/>
                                <asp:Button ID="btnCloseJob" runat="server" Text="Close Job" CssClass="Button_Red" Width="120px" Height="30px"/>
                                <asp:Button ID="btnCancel" runat="server" Text="Back" CssClass="Button_Red" Width="120px" Height="30px"/>
                                <Ajax:ConfirmButtonExtender ID="btnCloseJob_Confirm" runat="server" ConfirmText="Are you sure you want to close this job?" TargetControlID="btnCloseJob">
                                </Ajax:ConfirmButtonExtender>
                                <asp:Button ID="btnSaveData" runat="server" Text="Button" style="display:none"/>
                            </td>
                        </tr>
                    </table>
                    <div style="height:20px;"></div>
                </td>
            </tr>
        </table>
        
        <div style="display:none;">
            <asp:TextBox type="text" id="txtTempRemarkHeight" runat="server" Text="220" />
        </div>
        <script src="Script/jquery-1.10.2.min.js" type="text/javascript" language="javascript"></script>
        <script type="text/javascript" language="javascript">
            function doPrint() {
                $("#Document_btnSaveData").click();
                $("#Document_tdButton").hide();
                window.print();
                $("#Document_tdButton").show();
                //window.close();
            }
        </script>
    </ContentTemplate>
</asp:UpdatePanel>




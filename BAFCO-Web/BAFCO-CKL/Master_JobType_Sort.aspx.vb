﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_JobType_Sort
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlDocType(ddlDocType, "", 0)
            BindData()
        End If
    End Sub

    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT JOB_TYPE_ID AS ID,JOB_TYPE_NAME AS NAME," & MC.Get_DocumentSort(ddlDocType.SelectedValue) & " AS SORT " & vbCrLf
        SQL &= "FROM MS_JOB_TYPE " & vbCrLf
        SQL &= "WHERE ACTIVE_STATUS = 1 AND " & MC.Get_DocumentCondition(ddlDocType.SelectedValue) & vbCrLf
        SQL &= "ORDER BY ISNULL(" & MC.Get_DocumentSort(ddlDocType.SelectedValue) & ",100)" & vbCrLf
        SQL &= ",JOB_TYPE_NAME" & vbCrLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub ddlDocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocType.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Sort"
                Dim lblId As Label = e.Item.FindControl("lblId")
                Dim txtSort As TextBox = e.Item.FindControl("txtSort")
                Dim Sql As String = ""
                Sql = "SELECT * FROM MS_JOB_TYPE WHERE JOB_TYPE_ID = " & lblId.Text
                Dim DA As New SqlDataAdapter(Sql, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    If IsNumeric(txtSort.Text) Then
                        DT.Rows(0).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue)) = txtSort.Text
                    Else
                        DT.Rows(0).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue)) = DBNull.Value
                    End If
                    Dim cmb As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If
                BindData()
        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lblName As Label = e.Item.FindControl("lblName")

        lblNo.Text = e.Item.ItemIndex + 1
        lblId.Text = e.Item.DataItem("ID").ToString
        lblName.Text = GL.ReplaceText(e.Item.DataItem("NAME").ToString)

        Dim txt As TextBox = e.Item.FindControl("txtSort")
        Dim btn As Button = e.Item.FindControl("btnSort")
        CL.ImplementJavaIntegerText(txt, textControlLib.Text_Align.Center)
        txt.Attributes("onchange") = "document.getElementById('" & btn.ClientID & "').click();"
        txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
        txt.Text = e.Item.DataItem("SORT").ToString

    End Sub
End Class

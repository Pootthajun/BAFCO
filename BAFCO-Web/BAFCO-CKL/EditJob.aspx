﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditJob.aspx.vb" Inherits="EditJob" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="900" valign="top">
                <asp:UpdatePanel ID="udp1" runat="server">
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="1" style="margin:20px  0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                            <tr>
                                <td width="50" class="Grid_Header" align="center">
                                    No                    
                                </td>
                                <td width="120" class="Grid_Header">                    
                                    Ref Code
                                </td>
                                <td width="120" class="Grid_Header">                    
                                    Job No
                                </td>
                                <td class="Grid_Header">                    
                                    Customer
                                </td>
                                <td width="100" class="Grid_Header">                  
                                    Action
                                </td>
                            </tr>
                            <tr>
                                <td height="25px">
                                    &nbsp;<asp:Button ID="btnSearch" runat="server" style="display:none;"/>
                                </td>
                                <td height="25px">
                                    <asp:TextBox ID="txtFilterRefNo" runat="server" Width="100%" CssClass="Textbox_Form_White" style="text-align:center;"></asp:TextBox>
                                </td>
                                <td height="25px">
                                    <asp:TextBox ID="txtFilterJobNo" runat="server" Width="100%" CssClass="Textbox_Form_White" style="text-align:center;"></asp:TextBox>
                                </td>
                                <td height="25px">
                                    <asp:DropDownList ID="ddlFilterCustomer" runat="server" width="100%" CssClass="Dropdown_Form_White" AutoPostBack="true" style="padding-left:5px;">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" 
                                        style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblRefId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblRefName" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblJobId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblJobName" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="left" style="padding-left:10px;">
                                            <asp:Label ID="lblCustomer" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Edit" ImageUrl="images/icon/61.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                    <tr id="trEdit" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center" valign="middle">
                                            <asp:Image ImageUrl="images/arrow_blue_right.png" ID="imgEdit" runat="server"></asp:Image>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblEditRefId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblEditRefName" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblEditJobId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblEditJobName" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:DropDownList ID="ddlCustomer" runat="server" CssClass="Dropdown_Form_Required" width="100%"></asp:DropDownList>
                                        </td>
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnSave" CommandName="Save" runat="server" ToolTip="Save" ImageUrl="images/icon/59.png" Height="25px" />
                                            <asp:ImageButton ID="btnCancel" CommandName="Cancel" runat="server" ToolTip="Cancel" ImageUrl="images/icon/21.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>                        
                        <div style="margin-left:10px">
                            <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="15"/>
                        </div>
                        <br />
                    </ContentTemplate>
                </asp:UpdatePanel>           
            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>


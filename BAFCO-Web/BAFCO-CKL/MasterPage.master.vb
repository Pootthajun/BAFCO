﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Protected Sub lnkSignout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSignout.Click
        lblUName.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Default.aspx';", True)
        Session.Abandon()
        divImage.Visible = True
    End Sub

    Protected Sub btnToggleLeftMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnToggleLeftMenu.Click
        LeftMenu.Visible = Not LeftMenu.Visible
        Select Case LeftMenu.Visible
            Case True
                btnToggleLeftMenu.ImageUrl = "images/dbLeft.png"
                pnlLeftMenu.Style.Item("width") = "220px !important"
            Case False
                btnToggleLeftMenu.ImageUrl = "images/dbRight.png"
                pnlLeftMenu.Style.Item("width") = "18px"
        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            PerMissionMenu()
            WriteHeaderPage()
        End If

        If Not IsNothing(Session("User_ID")) AndAlso Session("User_ID") <> "" Then
            divImage.Visible = False
        Else
            divImage.Visible = True
        End If
        ProblemDocument()
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim SQL As String = ""
        SQL = "SELECT MS_USER.*,DEPARTMENT_NAME,DEFAULT_DOC,USER_TYPE_NAME,DEPARTMENT_FWP FROM MS_USER LEFT JOIN MS_DEPARTMENT ON MS_USER.DEPARTMENT_ID = MS_DEPARTMENT.DEPARTMENT_ID LEFT JOIN MS_USER_TYPE ON MS_USER.USER_TYPE_ID = MS_USER_TYPE.USER_TYPE_ID WHERE USER_CODE='" & txtUsername.Value.ToString.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid username');", True)
            txtUsername.Focus()
            Exit Sub
        End If

        If DT.Rows(0).Item("USER_PASSWORD") <> txtPassword.Value.ToString Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid password');", True)
            txtPassword.Focus()
            Exit Sub
        End If

        If Not DT.Rows(0).Item("ACTIVE_STATUS") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This username is unavailable');", True)
            txtUsername.Focus()
            Exit Sub
        End If

        Session("Department_ID") = DT.Rows(0).Item("Department_ID").ToString
        Session("Department_Name") = DT.Rows(0).Item("Department_Name").ToString
        Session("Department_FWP") = DT.Rows(0).Item("DEPARTMENT_FWP").ToString
        Session("User_ID") = DT.Rows(0).Item("User_ID").ToString
        Session("User_Fullname") = DT.Rows(0).Item("User_Fullname").ToString
        Session("User_Type_ID") = DT.Rows(0).Item("USER_TYPE_ID").ToString
        Session("User_Type_Name") = DT.Rows(0).Item("USER_TYPE_NAME").ToString
        Session("Default_Document") = DT.Rows(0).Item("DEFAULT_DOC").ToString
        divImage.Visible = False
        PerMissionMenu()
        ProblemDocument()
        'If ProblemDocument() = False Then
        '    LeftMenu.SelectedIndex = GetMenuSelectedIndex(1)
        'End If
    End Sub

    Sub ProblemDocument()
        If Not IsNothing(Session("Department_ID")) AndAlso Session("Department_ID") <> "" Then
            Dim Incomplete As Boolean = False
            Dim Hold As Boolean = False

            Dim SQL As String = ""
            SQL &= "SELECT COUNT(1) AS REC" & vbCrLf
            SQL &= "FROM REF_HEADER HD " & vbCrLf
            SQL &= "LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
            SQL &= "WHERE" & vbCrLf
            If Session("User_Type_Name") <> "Administrator" And Session("User_Type_Name") <> "Accounting" And Not Session("User_ID") Is Nothing Then
                SQL &= "HD.REF_FROM = " & Session("Department_ID") & " AND " & vbCrLf
            End If
            SQL &= "(" & vbCrLf
            SQL &= " DT.STATUS_CHECK = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            SQL &= "OR DT.STATUS_PRICE = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            SQL &= "OR DT.STATUS_APP = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            SQL &= "OR DT.STATUS_BILL = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            SQL &= "OR DT.STATUS_COMPLETE = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            SQL &= ")"
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            a_Incomplete.InnerHtml = "Incomplete " & DT.Rows(0).Item("REC").ToString & " Job(s)"
            If CInt(DT.Rows(0).Item("REC")) > 0 Then
                Incomplete = True
            Else
                Incomplete = False
            End If

            SQL = ""
            SQL &= "SELECT COUNT(1) AS REC" & vbCrLf
            SQL &= "FROM REF_HEADER HD " & vbCrLf
            SQL &= "LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
            SQL &= "WHERE" & vbCrLf
            If Session("User_Type_Name") <> "Administrator" And Session("User_Type_Name") <> "Accounting" And Not Session("User_ID") Is Nothing Then
                SQL &= "HD.REF_FROM = " & Session("Department_ID") & " AND " & vbCrLf
            End If
            SQL &= "(" & vbCrLf
            SQL &= " DT.STATUS_CHECK = " & MasterControlClass.Job_History.Hold & vbCrLf
            SQL &= "OR DT.STATUS_PRICE = " & MasterControlClass.Job_History.Hold & vbCrLf
            SQL &= "OR DT.STATUS_APP = " & MasterControlClass.Job_History.Hold & vbCrLf
            SQL &= "OR DT.STATUS_BILL = " & MasterControlClass.Job_History.Hold & vbCrLf
            SQL &= "OR DT.STATUS_COMPLETE = " & MasterControlClass.Job_History.Hold & vbCrLf
            SQL &= ")"
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            a_Hold.InnerHtml = "Hold " & DT.Rows(0).Item("REC").ToString & " Job(s)"
            If CInt(DT.Rows(0).Item("REC")) > 0 Then
                Hold = True
            Else
                Hold = False
            End If

            If Incomplete Then
                a_Incomplete.Attributes("class") = "Left_Menu_Link_Red"
            Else
                a_Incomplete.Attributes("class") = "Left_Menu_Link"
            End If

            If Hold Then
                a_Hold.Attributes("class") = "Left_Menu_Link_Red"
            Else
                a_Hold.Attributes("class") = "Left_Menu_Link"
            End If

            If Incomplete Or Hold Then
                a_Problem.Attributes("class") = "Left_Menu_Link_Red"
                imgProblem.Visible = True
            Else
                a_Problem.Attributes("class") = "Left_Header_Link"
                imgProblem.Visible = False
            End If


            'If Incomplete = True And Hold = True Then
            '    a_Problem.Attributes("class") = "Left_Menu_Link_Red"
            '    a_Hold.Attributes("class") = "Left_Menu_Link_Red"
            '    a_Incomplete.Attributes("class") = "Left_Menu_Link_Red"
            '    imgProblem.Visible = True
            'ElseIf Incomplete = True And Hold = False Then
            '    a_Problem.Attributes("class") = "Left_Menu_Link_Red"
            '    a_Hold.Attributes("class") = "Left_Menu_Link"
            '    a_Incomplete.Attributes("class") = "Left_Menu_Link_Red"
            '    imgProblem.Visible = True
            'ElseIf Incomplete = False And Hold = True Then
            '    a_Problem.Attributes("class") = "Left_Menu_Link"
            '    a_Hold.Attributes("class") = "Left_Menu_Link_Red"
            '    a_Incomplete.Attributes("class") = "Left_Menu_Link_Red"
            '    imgProblem.Visible = True
            'Else
            '    a_Problem.Attributes("class") = "Left_Menu_Link"
            '    imgProblem.Visible = False
            'End If
        End If
    End Sub

    Sub WriteHeaderPage()
        Select Case Me.Page.TemplateControl.ToString
            '   ------------- Job -----------
            Case "ASP." & "OpenJob.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Open Job(s)"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "CloseJob.aspx".ToLower.Replace(".", "_"), "ASP." & "CloseJobDocument.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Close Job(s)"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Submitted.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Submitted"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "OnHand.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "On Hand"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Checked.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Checked"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Priced.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Priced"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Approving.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Approving"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Approve.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Approving"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Billed.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Billed"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Completed.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Completed"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Search.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Search Data"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "EditJob.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Edit Job"
                LeftMenu.SelectedIndex = 0
                '---------------- Incomplete ---------------
            Case "ASP." & "Incomplete.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Incomplete"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(1)
            Case "ASP." & "Hold.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Hold"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(1)
                '---------------- Master Data ---------------
            Case "ASP." & "Master_Department.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Department"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_UserType.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "User Type"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_User.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "User"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Permission.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Permission"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Customer.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Customer"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_EntryType.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ประเภทใบขน"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_EntryType_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ประเภทใบขน (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_JobType.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ประเภทงาน"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_JobType_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ประเภทงาน (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Fee.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ค่าธรรมเนียม"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Fee_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ค่าธรรมเนียม (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_JobDoc.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "เอกสารแนบ"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_JobDoc_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "เอกสารแนบ (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Accounting.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "เฉพาะแผนกบัญชี"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Accounting_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "เฉพาะแผนกบัญชี (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_VehicleBafco.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "รถ Bafco"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_VehicleBafco_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "รถ Bafco (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_VehicleEmploy.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "รถจ้าง"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_VehicleEmploy_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "รถจ้าง (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_NameVehicleEmploy.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ชื่อรถจ้าง"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_NameVehicleEmploy_Sort.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "ชื่อรถจ้าง (ลำดับการแสดงผล)"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Default_Document.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Default Document"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case Else
                LeftMenu.RequireOpenedPane = False
                LeftMenu.SelectedIndex = -1
        End Select
    End Sub

    Function GetMenuSelectedIndex(ByVal DefaultSelectedIndex As Integer) As Integer
        Select Case DefaultSelectedIndex
            Case 0
                Return 0
            Case 1
                If LeftMenu.Panes("ADP_Job").Visible = True Then
                    Return 1
                Else
                    Return 0
                End If
            Case Else
                Dim Ret As Integer = 2
                If LeftMenu.Panes("ADP_Job").Visible = False Then
                    Ret = Ret - 1
                End If
                If LeftMenu.Panes("ADP_Incomplete").Visible = False Then
                    Ret = Ret - 1
                End If
                Return Ret
        End Select
    End Function

    Sub PerMissionMenu()
        If Not IsNothing(Session("User_Fullname")) AndAlso Session("User_Fullname") <> "" Then
            lblUName.Text = Session("User_Fullname")

            '------ Permission Menu -----
            Dim sql As String = ""
            Dim MenuList As String = ""
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter
            sql = "SELECT MENU_ID FROM MS_USER_TYPE_MENU WHERE active = 1 and USER_TYPE_ID = " & Session("User_Type_ID").ToString
            da = New SqlDataAdapter(sql, ConnStr)
            da.Fill(dt)

            For i As Int32 = 0 To dt.Rows.Count - 1
                MenuList = MenuList & "|" & dt.Rows(i).Item("MENU_ID").ToString & ";"
            Next

            Dim CountMenu As Int32 = 0
            '   ------------- Job -----------
            If InStr(MenuList, "|1;") > 0 Then
                mnOpenJob.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnOpenJob.Visible = False
            End If

            If InStr(MenuList, "|2;") > 0 Then
                mnCloseJob.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnCloseJob.Visible = False
            End If

            If InStr(MenuList, "|3;") > 0 Then
                mnSubmitJob.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnSubmitJob.Visible = False
            End If

            If InStr(MenuList, "|4;") > 0 Then
                mnOnHandJob.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnOnHandJob.Visible = False
            End If

            If InStr(MenuList, "|5;") > 0 Then
                mnCheckJob.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnCheckJob.Visible = False
            End If

            If InStr(MenuList, "|6;") > 0 Then
                mnPrice.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnPrice.Visible = False
            End If

            If InStr(MenuList, "|7;") > 0 Then
                mnApprove.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnApprove.Visible = False
            End If

            If InStr(MenuList, "|8;") > 0 Then
                mnBill.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnBill.Visible = False
            End If

            If InStr(MenuList, "|9;") > 0 Then
                mnComplete.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnComplete.Visible = False
            End If

            If InStr(MenuList, "|10;") > 0 Then
                mnSearch.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnSearch.Visible = False
            End If

            If InStr(MenuList, "|34;") > 0 Then
                mnEditJob.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnEditJob.Visible = False
            End If

            If CountMenu > 0 Then
                LeftMenu.Panes("ADP_Job").Visible = True
            Else
                LeftMenu.Panes("ADP_Job").Visible = False
            End If

            '   ------------- Problem Documents -----------
            CountMenu = 0
            If InStr(MenuList, "|11;") > 0 Then
                mnIncomplete.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnIncomplete.Visible = False
            End If

            If InStr(MenuList, "|12;") > 0 Then
                mnHold.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnHold.Visible = False
            End If

            If CountMenu > 0 Then
                LeftMenu.Panes("ADP_Incomplete").Visible = True
            Else
                LeftMenu.Panes("ADP_Incomplete").Visible = False
            End If

            '*************** Setting ****************
            CountMenu = 0
            If InStr(MenuList, "|13;") > 0 Then
                mnDepartment.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnDepartment.Visible = False
            End If

            If InStr(MenuList, "|14;") > 0 Then
                mnUserType.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnUserType.Visible = False
            End If

            If InStr(MenuList, "|15;") > 0 Then
                mnUser.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnUser.Visible = False
            End If

            If InStr(MenuList, "|16;") > 0 Then
                mnPermission.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnPermission.Visible = False
            End If

            If InStr(MenuList, "|17;") > 0 Then
                mnEntryType.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnEntryType.Visible = False
            End If

            If InStr(MenuList, "|18;") > 0 Then
                mnEntryTypeSort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnEntryTypeSort.Visible = False
            End If

            If InStr(MenuList, "|19;") > 0 Then
                mnJobType.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnJobType.Visible = False
            End If

            If InStr(MenuList, "|20;") > 0 Then
                mnJobTypeSort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnJobTypeSort.Visible = False
            End If

            If InStr(MenuList, "|21;") > 0 Then
                mnFee.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnFee.Visible = False
            End If

            If InStr(MenuList, "|22;") > 0 Then
                mnFeeSort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnFeeSort.Visible = False
            End If

            If InStr(MenuList, "|23;") > 0 Then
                mnJobDoc.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnJobDoc.Visible = False
            End If

            If InStr(MenuList, "|24;") > 0 Then
                mnJobDocSort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnJobDocSort.Visible = False
            End If

            If InStr(MenuList, "|25;") > 0 Then
                mnAccounting.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnAccounting.Visible = False
            End If

            If InStr(MenuList, "|26;") > 0 Then
                mnAccountingSort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnAccountingSort.Visible = False
            End If

            If InStr(MenuList, "|27;") > 0 Then
                mnVehicleBafco.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnVehicleBafco.Visible = False
            End If

            If InStr(MenuList, "|28;") > 0 Then
                mnVehicleBafcoSort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnVehicleBafcoSort.Visible = False
            End If

            If InStr(MenuList, "|29;") > 0 Then
                mnVehicleEmploy.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnVehicleEmploy.Visible = False
            End If

            If InStr(MenuList, "|30;") > 0 Then
                mnVehicleEmploySort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnVehicleEmploySort.Visible = False
            End If

            If InStr(MenuList, "|31;") > 0 Then
                mnNameVehicleEmploy.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnNameVehicleEmploy.Visible = False
            End If

            If InStr(MenuList, "|32;") > 0 Then
                mnNameVehicleEmploySort.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnNameVehicleEmploySort.Visible = False
            End If

            If InStr(MenuList, "|33;") > 0 Then
                mnDefaultDoc.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnDefaultDoc.Visible = False
            End If

            If CountMenu > 0 Then
                LeftMenu.Panes("ADP_Setting").Visible = True
            Else
                LeftMenu.Panes("ADP_Setting").Visible = False
            End If

        End If

    End Sub

End Class


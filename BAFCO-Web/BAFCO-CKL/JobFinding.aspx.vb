﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class JobFinding
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib

    Private Property RefID() As String
        Get
            Return ViewState("RefID")
        End Get
        Set(ByVal value As String)
            ViewState("RefID") = value
        End Set
    End Property

    Private Property JobID() As String
        Get
            Return ViewState("JobID")
        End Get
        Set(ByVal value As String)
            ViewState("JobID") = value
        End Set
    End Property

    Private Property CloseBy() As Integer
        Get
            If IsNumeric(ViewState("CloseBy")) Then
                Return ViewState("CloseBy")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("CloseBy") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            RefID = Request.QueryString("RefID").Replace("?", "")
            JobID = Request.QueryString("JobID").Replace("?", "")
            CloseBy = Session("User_ID")
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT JOB_HEADER.JOB_ID,JOB_NO,CUSTOMER,CLOSE_DATE,JOB_HEADER.DOC_TYPE_ID,DOC_TYPE_NAME" & vbCrLf
        SQL &= "FROM JOB_HEADER LEFT JOIN MS_DOC_TYPE " & vbCrLf
        SQL &= "ON JOB_HEADER.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "WHERE STATUS = " & MasterControlClass.Job_Status.Close & vbCrLf
        If Session("User_Type_Name") <> "Administrator" Then
            SQL &= "AND JOB_HEADER.CLOSE_BY = " & CloseBy & vbCrLf
        End If
        If JobID <> "" Then
            SQL &= "AND JOB_HEADER.JOB_ID NOT IN (" & JobID & " )" & vbCrLf
        End If

        If RefID <> "" Then
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT JOB_ID,JOB_NO,CUSTOMER,CLOSE_DATE,REF_DETAIL.DOC_TYPE_ID,DOC_TYPE_NAME" & vbCrLf
            SQL &= "FROM REF_DETAIL LEFT JOIN MS_DOC_TYPE " & vbCrLf
            SQL &= "ON REF_DETAIL.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
            SQL &= "WHERE REF_ID = " & RefID & " " & vbCrLf
            If JobID <> "" Then
                SQL &= "AND JOB_ID NOT IN (" & JobID & " )" & vbCrLf
            End If

        End If

        SQL &= "ORDER BY JOB_NO"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim Filter As String = ""
        If txtJobNo.Text <> "" Then
            Filter &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtCustomer.Text <> "" Then
            Filter &= "CUSTOMER like '%" & txtCustomer.Text.Replace("'", "''") & "%' AND "
        End If

        If Filter <> "" Then Filter = Filter.Substring(0, Filter.Length - 4)
        DT.DefaultView.RowFilter = Filter

        rptData.DataSource = DT
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim trRow As HtmlTableRow = e.Item.FindControl("trRow")
        Dim cb As CheckBox = e.Item.FindControl("cb")
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblDate As Label = e.Item.FindControl("lblDate")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        lblJobNo.Attributes.Add("JobID", e.Item.DataItem("JOB_ID"))
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        If IsDate(e.Item.DataItem("CLOSE_DATE")) Then
            lblDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("CLOSE_DATE"))
        End If
        lblDocType.Text = e.Item.DataItem("DOC_TYPE_NAME").ToString

        'If InStr(JobID, "|" & e.Item.DataItem("JOB_ID")) > 0 Then
        '    cb.Checked = True
        'Else
        '    cb.Checked = False
        'End If
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
        '----------- Checking Current Status-----------
        Dim IsNotCheck As Boolean = False
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            If Not cb.Checked Then
                IsNotCheck = True '------- มีอย่างน้อย 1 อันที่ยังไม่ได้เช็ค --------
                Exit For
            End If
        Next
        '---------- Set New Status-------
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            cb.Checked = IsNotCheck
        Next
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim Check As Int32 = 0
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            If cb.Checked = True Then
                Check = Check + 1
            End If
        Next

        If Check = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please choose Job No');", True)
            Exit Sub
        End If

        If Check > 10 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Maximun 10 Job No');", True)
            Exit Sub
        End If
        Dim Script As String = "returnSelectedValue('" & GetJobCheck().Replace("'", "\'") & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseDialog", Script, True)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim Script As String = "window.close();"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
    End Sub

    Protected Sub txtJobNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtJobNo.TextChanged, txtCustomer.TextChanged
        BindData()
    End Sub

    Function GetJobCheck() As String

        Dim DT As New DataTable
        DT.Columns.Add("JOB_ID")

        If rptData.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptData.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim cb As CheckBox = ri.FindControl("cb")
                Dim lblJobNo As Label = ri.FindControl("lblJobNo")
                If cb.Checked = True Then
                    Dim DR As DataRow = DT.NewRow
                    DR("JOB_ID") = lblJobNo.Attributes("JobID")
                    DT.Rows.Add(DR)
                End If
            Next
            Dim RetJobID As String = ""
            For i As Int32 = 0 To DT.Rows.Count - 1
                RetJobID = RetJobID & DT.Rows(i).Item("JOB_ID").ToString & ","
            Next
            RetJobID = RetJobID.Substring(0, RetJobID.Length - 1)
            Return RetJobID
        End If
        Return ""
    End Function
End Class

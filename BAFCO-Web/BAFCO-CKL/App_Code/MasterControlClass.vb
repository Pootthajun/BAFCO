﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient


Public Class MasterControlClass

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Public Enum Job_History
        Wait = 0
        Confirmation = 1
        Incomplete = 2
        Delete = 3
        FOC = 4
        Hold = 5
        CreateApprove = 6
        SubmitApprove = 7
        Revert = 8
    End Enum

    Public Enum Job_Status
        Open = 0
        Close = 1
        Submit = 2
        OnHand = 3
        Checked = 4
        Priced = 5
        Approving = 6
        Billed = 7
        Completed = 8
        EndProcess = 9
        FOC = 10
        Delete = 11
    End Enum

    Public Enum Ref_Status
        None = 0
        Submitted = 1
        Approve = 2
        OnHand = 3
    End Enum

    Public Enum DocumentType
        IM_AIR = 1
        IM_SEA = 2
        IM_TRUCK = 3
        EX_AIR = 4
        EX_SEA = 5
        EX_TRUCK = 6
        LOGIS = 7
        VESSEL = 8
        OTHER = 9
    End Enum

    Public Function Get_DocumentTypeName(ByVal DocumentType As DocumentType) As String
        Select Case DocumentType
            Case MasterControlClass.DocumentType.IM_AIR
                Return "IMPORT AIR"
            Case MasterControlClass.DocumentType.IM_SEA
                Return "IMPORT SEA"
            Case MasterControlClass.DocumentType.IM_TRUCK
                Return "IMPORT TRUCK"
            Case MasterControlClass.DocumentType.EX_AIR
                Return "EXPORT AIR"
            Case MasterControlClass.DocumentType.EX_SEA
                Return "EXPORT SEA"
            Case MasterControlClass.DocumentType.EX_TRUCK
                Return "EXPORT TRUCK"
            Case MasterControlClass.DocumentType.LOGIS
                Return "LOGISTICS"
            Case MasterControlClass.DocumentType.OTHER
                Return "OTHERS"
            Case MasterControlClass.DocumentType.VESSEL
                Return "VESSEL CLEARANCE & SIGN ON-OFF"
            Case Else
                Return ""
        End Select
    End Function

    Function Get_DocumentCondition(ByVal DocumentType As DocumentType) As String
        Select Case DocumentType
            Case MasterControlClass.DocumentType.IM_AIR
                Return "IM_AIR = 1"
            Case MasterControlClass.DocumentType.IM_SEA
                Return "IM_SEA = 1"
            Case MasterControlClass.DocumentType.IM_TRUCK
                Return "IM_TRUCK = 1"
            Case MasterControlClass.DocumentType.EX_AIR
                Return "EX_AIR = 1"
            Case MasterControlClass.DocumentType.EX_SEA
                Return "EX_SEA = 1"
            Case MasterControlClass.DocumentType.EX_TRUCK
                Return "EX_TRUCK = 1"
            Case MasterControlClass.DocumentType.LOGIS
                Return "LOGIS = 1"
            Case MasterControlClass.DocumentType.VESSEL
                Return "VESSEL = 1"
            Case MasterControlClass.DocumentType.OTHER
                Return "OTHER = 1"
            Case Else
                Return ""
        End Select
    End Function

    Function Get_DocumentSort(ByVal DocumentType As DocumentType) As String
        Select Case DocumentType
            Case MasterControlClass.DocumentType.IM_AIR
                Return "IM_AIR_SORT"
            Case MasterControlClass.DocumentType.IM_SEA
                Return "IM_SEA_SORT"
            Case MasterControlClass.DocumentType.IM_TRUCK
                Return "IM_TRUCK_SORT"
            Case MasterControlClass.DocumentType.EX_AIR
                Return "EX_AIR_SORT"
            Case MasterControlClass.DocumentType.EX_SEA
                Return "EX_SEA_SORT"
            Case MasterControlClass.DocumentType.EX_TRUCK
                Return "EX_TRUCK_SORT"
            Case MasterControlClass.DocumentType.LOGIS
                Return "LOGIS_SORT"
            Case MasterControlClass.DocumentType.VESSEL
                Return "VESSEL_SORT"
            Case MasterControlClass.DocumentType.OTHER
                Return "OTHER_SORT"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_RefStatus(ByVal Ref_Status As Ref_Status) As String
        Select Case Ref_Status
            Case MasterControlClass.Ref_Status.None
                Return "None"
            Case MasterControlClass.Ref_Status.Submitted
                Return "Submitted"
            Case MasterControlClass.Ref_Status.Approve
                Return "Approved"
            Case MasterControlClass.Ref_Status.OnHand
                Return "On Hand"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_JobStatus(ByVal Job_Status As Job_Status) As String
        Select Case Job_Status
            Case MasterControlClass.Job_Status.Open
                Return "Opened"
            Case MasterControlClass.Job_Status.Close
                Return "Closed"
            Case MasterControlClass.Job_Status.Submit
                Return "Submited"
            Case MasterControlClass.Job_Status.OnHand
                Return "On Hand"
            Case MasterControlClass.Job_Status.Checked
                Return "Checked"
            Case MasterControlClass.Job_Status.Priced
                Return "Priced"
            Case MasterControlClass.Job_Status.Approving
                Return "Approved"
            Case MasterControlClass.Job_Status.Billed
                Return "Billed"
            Case MasterControlClass.Job_Status.Completed
                Return "Completed"
            Case MasterControlClass.Job_Status.EndProcess
                Return "Check Completed"
            Case MasterControlClass.Job_Status.FOC
                Return "FOC"
            Case MasterControlClass.Job_Status.Delete
                Return "Delete"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_JobDescription(ByVal Desc As Job_Status) As String
        Select Case Desc
            Case Job_Status.Checked
                Return "การตรวจสอบเอกสาร"
            Case Job_Status.Priced
                Return "การตรวจสอบต้นทุน"
            Case Job_Status.Approving
                Return "การออกบิล"
            Case Job_Status.Billed
                Return "การวางบิล"
            Case Job_Status.Completed
                Return "Completed"
            Case Else
                Return ""
        End Select
    End Function

    Public Function Get_ReceiveDate(ByVal RefID As Int32, ByVal JobID As Int32) As String
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable

        SQL = "SELECT MAX(CLOSE_DATE) AS CLOSE_DATE FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows(0).Item("CLOSE_DATE").ToString <> "" Then
            Return DT.Rows(0).Item("CLOSE_DATE").ToString
        Else
            SQL = "SELECT RECEIVE_DATE RECEIVE_DATE FROM REF_DETAIL WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            Return DT.Rows(0).Item("RECEIVE_DATE").ToString
        End If
    End Function

    Public Function GetReferenceNo(ByVal DepartmentID As Int32) As String
        Dim ID As String = ""
        Dim CODE As String = ""
        Dim SQL As String = ""
        SQL = "SELECT DEPARTMENT_CODE FROM MS_DEPARTMENT WHERE DEPARTMENT_ID = " & DepartmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            CODE = DT.Rows(0).Item("DEPARTMENT_CODE")
        End If

        SQL = "SELECT CONVERT(INT,RIGHT(MAX(REF_NO),5)) + 1 AS ID FROM REF_HEADER WHERE REF_NO LIKE '%" & CODE & "%'"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows(0).Item("ID").ToString <> "" Then
            ID = CODE & DT.Rows(0).Item("ID").ToString.PadLeft(5, "0")
        Else
            ID = CODE & "00001"
        End If

        Return ID
    End Function

#Region "BindCheckboxList"
    Public Sub BindCBLEntryType(ByRef cbl As CheckBoxList, ByVal DocumentType As DocumentType)
        Dim SQL As String = "SELECT ENTRY_TYPE_ID,ENTRY_TYPE_NAME FROM MS_ENTRY_TYPE WHERE ACTIVE_STATUS = 1" & vbLf
        Select Case DocumentType
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND IM_AIR = " & MasterControlClass.DocumentType.IM_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_SEA
                SQL &= "AND IM_SEA = " & MasterControlClass.DocumentType.IM_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_TRUCK
                SQL &= "AND IM_TRUCK = " & MasterControlClass.DocumentType.IM_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.EX_AIR
                SQL &= "AND EX_AIR = " & MasterControlClass.DocumentType.EX_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_SEA = " & MasterControlClass.DocumentType.EX_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_TRUCK = " & MasterControlClass.DocumentType.EX_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND LOGIS = " & MasterControlClass.DocumentType.LOGIS & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND VESSEL = " & MasterControlClass.DocumentType.VESSEL & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND OTHER = " & MasterControlClass.DocumentType.OTHER & vbNewLine
        End Select
        SQL &= "ORDER BY SORT,ENTRY_TYPE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbl.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            cbl.DataSource = DT
            cbl.DataTextField = "ENTRY_TYPE_NAME"
            cbl.DataValueField = "ENTRY_TYPE_ID"
            cbl.DataBind()
        Next
    End Sub

    Public Sub BindCBLFee(ByRef cbl As CheckBoxList, ByVal DocumentType As DocumentType)
        Dim SQL As String = "SELECT FEE_ID,FEE_NAME FROM MS_FEE WHERE ACTIVE_STATUS = 1" & vbLf
        Select Case DocumentType
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND IM_AIR = " & MasterControlClass.DocumentType.IM_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_SEA
                SQL &= "AND IM_SEA = " & MasterControlClass.DocumentType.IM_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_TRUCK
                SQL &= "AND IM_TRUCK = " & MasterControlClass.DocumentType.IM_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.EX_AIR
                SQL &= "AND EX_AIR = " & MasterControlClass.DocumentType.EX_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_SEA = " & MasterControlClass.DocumentType.EX_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_TRUCK = " & MasterControlClass.DocumentType.EX_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND LOGIS = " & MasterControlClass.DocumentType.LOGIS & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND VESSEL = " & MasterControlClass.DocumentType.VESSEL & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND OTHER = " & MasterControlClass.DocumentType.OTHER & vbNewLine
        End Select
        SQL &= "ORDER BY SORT,FEE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbl.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            cbl.DataSource = DT
            cbl.DataTextField = "FEE_NAME"
            cbl.DataValueField = "FEE_ID"
            cbl.DataBind()
        Next
    End Sub

    Public Sub BindCBLJobDoc(ByRef cbl As CheckBoxList, ByVal DocumentType As DocumentType)
        Dim SQL As String = "SELECT JOB_DOC_ID,JOB_DOC_NAME FROM MS_JOB_DOC WHERE ACTIVE_STATUS = 1" & vbLf
        Select Case DocumentType
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND IM_AIR = " & MasterControlClass.DocumentType.IM_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_SEA
                SQL &= "AND IM_SEA = " & MasterControlClass.DocumentType.IM_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_TRUCK
                SQL &= "AND IM_TRUCK = " & MasterControlClass.DocumentType.IM_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.EX_AIR
                SQL &= "AND EX_AIR = " & MasterControlClass.DocumentType.EX_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_SEA = " & MasterControlClass.DocumentType.EX_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_TRUCK = " & MasterControlClass.DocumentType.EX_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND LOGIS = " & MasterControlClass.DocumentType.LOGIS & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND VESSEL = " & MasterControlClass.DocumentType.VESSEL & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND OTHER = " & MasterControlClass.DocumentType.OTHER & vbNewLine
        End Select
        SQL &= "ORDER BY SORT,JOB_DOC_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbl.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            cbl.DataSource = DT
            cbl.DataTextField = "JOB_DOC_NAME"
            cbl.DataValueField = "JOB_DOC_ID"
            cbl.DataBind()
        Next
    End Sub

    Public Sub BindCBLAccounting(ByRef cbl As CheckBoxList, ByVal DocumentType As DocumentType)
        Dim SQL As String = "SELECT ACCOUNTING_ID,ACCOUNTING_NAME FROM MS_ACCOUNTING WHERE ACTIVE_STATUS = 1" & vbLf
        Select Case DocumentType
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND IM_AIR = " & MasterControlClass.DocumentType.IM_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_SEA
                SQL &= "AND IM_SEA = " & MasterControlClass.DocumentType.IM_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_TRUCK
                SQL &= "AND IM_TRUCK = " & MasterControlClass.DocumentType.IM_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.EX_AIR
                SQL &= "AND EX_AIR = " & MasterControlClass.DocumentType.EX_AIR & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_SEA = " & MasterControlClass.DocumentType.EX_SEA & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND EX_TRUCK = " & MasterControlClass.DocumentType.EX_TRUCK & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND LOGIS = " & MasterControlClass.DocumentType.LOGIS & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND VESSEL = " & MasterControlClass.DocumentType.VESSEL & vbNewLine
            Case MasterControlClass.DocumentType.IM_AIR
                SQL &= "AND OTHER = " & MasterControlClass.DocumentType.OTHER & vbNewLine
        End Select
        SQL &= "ORDER BY SORT,ACCOUNTING_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbl.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            cbl.DataSource = DT
            cbl.DataTextField = "ACCOUNTING_NAME"
            cbl.DataValueField = "ACCOUNTING_ID"
            cbl.DataBind()
        Next
    End Sub
#End Region

#Region "BindDropDownList"

    Public Sub BindDDlDocType(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DOC_TYPE_ID,DOC_TYPE_NAME FROM MS_DOC_TYPE" & vbNewLine
        SQL &= "ORDER BY SORT ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("DOC_TYPE_NAME"), DT.Rows(i).Item("DOC_TYPE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlUserType(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT USER_TYPE_ID,USER_TYPE_NAME FROM MS_USER_TYPE" & vbNewLine
        SQL &= "ORDER BY SORT,USER_TYPE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("USER_TYPE_NAME"), DT.Rows(i).Item("USER_TYPE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlDepartment(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DEPARTMENT_ID,DEPARTMENT_NAME FROM MS_DEPARTMENT" & vbNewLine
        SQL &= "ORDER BY SORT,DEPARTMENT_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("DEPARTMENT_NAME"), DT.Rows(i).Item("DEPARTMENT_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlCustomerFromUpload(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DISTINCT CUSTOMER FROM JOB_HEADER" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("CUSTOMER"), DT.Rows(i).Item("CUSTOMER"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlJobTypeFromUpload(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DISTINCT JOB_TYPE FROM JOB_HEADER" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("JOB_TYPE"), DT.Rows(i).Item("JOB_TYPE"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlDepartmentFromUpload(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DISTINCT DEPARTMENT FROM JOB_HEADER" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("DEPARTMENT"), DT.Rows(i).Item("DEPARTMENT"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlStatus(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        ddl.Items.Clear()
        Dim Item As New ListItem
        If ShowTextIndex <> "" Then
            Item = New ListItem(ShowTextIndex, 99)
            ddl.Items.Add(Item)
        End If
        Item = New ListItem("Opened", 0)
        ddl.Items.Add(Item)
        Item = New ListItem("Closed", 1)
        ddl.Items.Add(Item)
        Item = New ListItem("Submited", 2)
        ddl.Items.Add(Item)
        Item = New ListItem("On Hand", 3)
        ddl.Items.Add(Item)
        Item = New ListItem("Checked", 4)
        ddl.Items.Add(Item)
        Item = New ListItem("Priced", 5)
        ddl.Items.Add(Item)
        Item = New ListItem("Approved", 6)
        ddl.Items.Add(Item)
        Item = New ListItem("Billed", 7)
        ddl.Items.Add(Item)
        Item = New ListItem("Completed", 8)
        ddl.Items.Add(Item)
        Item = New ListItem("FOC", 10)
        ddl.Items.Add(Item)
        Item = New ListItem("Delete", 11)
        ddl.Items.Add(Item)
        Item = New ListItem("Check Completed", 9)
        ddl.Items.Add(Item)
        ddl.SelectedIndex = 0
    End Sub
#End Region

#Region "JavascriptButton"

    Public Sub ImplementConfirmButton(ByVal MarkButton As HtmlAnchor, ByVal ActionButton As Button, ByVal RequireTextBox As TextBox, ByVal ConfirmText As String, ByVal InvalidText As String)
        Dim Script As String = ""
        '-------------- Check Require Text-----------
        If Not IsNothing(RequireTextBox) Then
            Script &= "if(document.getElementById('" & RequireTextBox.ClientID & "').value==''){alert('" & InvalidText & "'); return;}"
        End If
        Script &= "if(confirm('" & ConfirmText & "'))document.getElementById('" & ActionButton.ClientID & "').click();"
        MarkButton.Attributes("onClick") = Script
    End Sub

#End Region

    Public Function UpdateJobStatus(ByVal JobID As Integer, ByVal Job_Status As Job_Status, ByVal UserID As Integer) As String
        Dim SQL As String = ""
        Select Case Job_Status
            Case MasterControlClass.Job_Status.Open
                Return "Opened"
            Case MasterControlClass.Job_Status.Close
                Return "Closed"
                SQL &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & vbCrLf
                SQL &= ",CLOSE_BY = " & UserID & " ,CLOSE_DATE = GETDATE()" & vbCrLf
                SQL &= "WHERE JOB_ID = " & JobID
            Case MasterControlClass.Job_Status.Submit
                Return "Submited"


            Case MasterControlClass.Job_Status.OnHand
                Return "On Hand"
            Case MasterControlClass.Job_Status.Checked
                Return "Checked"
            Case MasterControlClass.Job_Status.Priced
                Return "Priced"
            Case MasterControlClass.Job_Status.Approving
                Return "Approved"
            Case MasterControlClass.Job_Status.Billed
                Return "Billed"
            Case MasterControlClass.Job_Status.Completed
                Return "Completed"
            Case MasterControlClass.Job_Status.EndProcess
                Return "Check Completed"
            Case MasterControlClass.Job_Status.FOC
                Return "FOC"
            Case MasterControlClass.Job_Status.Delete
                Return "Delete"
            Case Else
                Return ""
        End Select

        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()
    End Function

End Class

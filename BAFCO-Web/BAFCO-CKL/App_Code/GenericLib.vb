﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Globalization
Imports System.Data
Imports System.Data.SqlClient
Imports Join.LeftJoin

Public Class GenericLib

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

#Region "Nested Type"

    Public Structure AttachmentFileStructure
        Public ServerPath As String
        Public AbsoluteName As String
        Public URL As String
    End Structure

#End Region

    Public Function ReportProgrammingDate(ByVal Input As DateTime) As String
        If IsDate(Input) Then
            Return Input.Year & "-" & Input.Month.ToString.PadLeft(2, "0") & "-" & Input.Day.ToString.PadLeft(2, "0")
        End If
        Return ""
    End Function

    Public Function IsProgrammingDate(ByVal Input As String) As Boolean
        Try
            Dim Temp As Date = DateTime.Parse(Input)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function IsFormatFileName(ByVal FileName As String) As Boolean
        Dim ExceptChars As String = "/\:*?""<>|;"
        For i As Integer = 1 To Len(ExceptChars)
            If InStr(FileName, Mid(ExceptChars, i)) > 0 Then
                Return False
            End If
        Next
        Return True
    End Function

    Public Function IsValidEmailFormat(ByVal input As String) As Boolean
        Return Regex.IsMatch(input, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")
    End Function

    Public Function OriginalFileName(ByVal FullPath As String) As String
        Return FullPath.Substring(FullPath.LastIndexOf("\") + 1)
    End Function

    Public Function OriginalFileType(ByVal FullPath As String) As String
        Return FullPath.Substring(FullPath.LastIndexOf(".") + 1)
    End Function

    Public Function ReportMonthThai(ByVal MonthID As Integer) As String
        Select Case MonthID
            Case 1
                Return "มกราคม"
            Case 2
                Return "กุมภาพันธ์"
            Case 3
                Return "มีนาคม"
            Case 4
                Return "เมษายน"
            Case 5
                Return "พฤษภาคม"
            Case 6
                Return "มิถุนายน"
            Case 7
                Return "กรกฎาคม"
            Case 8
                Return "สิงหาคม"
            Case 9
                Return "กันยายน"
            Case 10
                Return "ตุลาคม"
            Case 11
                Return "พฤศจิกายน"
            Case 12
                Return "ธันวาคม"
            Case Else
                Return ""
        End Select
    End Function

    Public Function ReportMonthEnglish(ByVal MonthID As Integer) As String
        Select Case MonthID
            Case 1
                Return "January"
            Case 2
                Return "February"
            Case 3
                Return "March"
            Case 4
                Return "April"
            Case 5
                Return "May"
            Case 6
                Return "June"
            Case 7
                Return "July"
            Case 8
                Return "August"
            Case 9
                Return "September"
            Case 10
                Return "October"
            Case 11
                Return "November"
            Case 12
                Return "December"
            Case Else
                Return ""
        End Select
    End Function


    Private Function SendEmail(ByVal MailFrom As String, ByVal MailTo() As String, ByVal CC As String(), ByVal AttachFileName As String(), ByVal Subject As String, ByVal Body As String, ByVal IsHTML As Boolean, ByVal Host As String, Optional ByVal Port As Integer = 0, Optional ByVal UserName As String = "", Optional ByVal Password As String = "") As Boolean
        Try

            Dim smtp As SmtpClient = New SmtpClient(Host)

            Dim message As New MailMessage()
            message.From = New MailAddress(MailFrom)
            For i As Integer = 0 To MailTo.Length - 1
                message.To.Add(MailTo(i))
            Next
            For i As Integer = 0 To CC.Length - 1
                message.CC.Add(CC(i))
            Next
            For i As Integer = 0 To AttachFileName.Length - 1
                Dim Attach As New Net.Mail.Attachment(AttachFileName(i))
                message.Attachments.Add(Attach)
            Next

            message.Subject = Subject
            message.IsBodyHtml = IsHTML
            message.Body = Body


            smtp.Host = Host
            If Port <> 0 Then smtp.Port = Port
            If UserName <> "" Then smtp.Credentials = New System.Net.NetworkCredential(UserName, Password)

            smtp.Send(message)
            Return True

        Catch ex As Exception
            Return False

        End Try
    End Function

    Public Function GetImageContentType(ByVal SrcImg As System.Drawing.Image) As String
        Select Case SrcImg.RawFormat.Guid
            Case Drawing.Imaging.ImageFormat.Bmp.Guid
                Return "image/tiff"
            Case Drawing.Imaging.ImageFormat.Jpeg.Guid
                Return "image/jpeg"
            Case Drawing.Imaging.ImageFormat.Gif.Guid
                Return "image/gif"
            Case Drawing.Imaging.ImageFormat.Png.Guid
                Return "image/png"
            Case Else '------------- Default Format ---------------
                Return ""
        End Select
    End Function

    Public Function CompareDataTableNew(ByVal Dt1 As System.Data.DataTable, ByVal Dt2 As System.Data.DataTable, ByVal argKey1 As String, ByVal argKey2 As String) As System.Data.DataTable
        Dim joinDt As New DataTable
        If Dt1.Rows.Count > 0 And Dt2.Rows.Count > 0 Then
            joinDt = Join.LeftJoin.Join(Dt1, Dt2, Dt1.Columns(argKey1), Dt2.Columns(argKey2))
        End If
        Return joinDt
    End Function

    'หา rowID ของ Tabel
    Public Function FindID(ByVal TableName As String, ByVal ColName As String) As String
        Dim id As String = ""
        Dim SQL As String = ""
        SQL = "select isnull(MAX(" & ColName & " + 1),1) as id from " & TableName.Replace("'", "''")
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            id = DT.Rows(0).Item("id").ToString
        End If
        Return id
    End Function

    Public Function FindBeginDateNow() As String
        Dim retDate As String = ""
        Dim SQL As String = ""
        SQL = "SELECT CONVERT(VARCHAR(8),GETDATE(),120) + '01' AS DD"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            retDate = DT.Rows(0).Item("DD").ToString
        End If
        Return retDate
    End Function

    Public Function FindEndDateNow() As String
        Dim retDate As String = ""
        Dim SQL As String = ""
        SQL = "SELECT CONVERT(VARCHAR(10),DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE())+1,0)),120) AS DD"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            retDate = DT.Rows(0).Item("DD").ToString
        End If
        Return retDate
    End Function

    Public Function JoinDataTable(ByVal Dt1 As DataTable, ByVal Dt2 As DataTable, ByVal argKey1 As String, ByVal argKey2 As String) As DataTable
        Dim joinDt As New DataTable
        If Dt1.Rows.Count > 0 And Dt2.Rows.Count > 0 Then
            joinDt = Join.LeftJoin.Join(Dt1, Dt2, Dt1.Columns(argKey1), Dt2.Columns(argKey2))
        End If
        Return joinDt
    End Function

    Function ReplaceText(ByVal txt As String) As String
        Return txt.Replace(vbLf, "<br>").Replace(" ", "&nbsp;")
    End Function

    Function UndoReplaceText(ByVal txt As String) As String
        Return txt.Replace("<br>", vbLf).Replace("&nbsp;", " ")
    End Function

End Class

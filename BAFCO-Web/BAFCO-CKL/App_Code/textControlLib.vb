﻿Imports Microsoft.VisualBasic

Public Class textControlLib

    Dim GL As New GenericLib

    Public Enum Text_Align
        Right = 0
        Left = 1
        Center = 2
    End Enum

    Public Sub ImplementJavaMoneyText(ByRef Obj As TextBox, ByVal Text_Align As Text_Align)
        Obj.Attributes("OnChange") = "this.value=formatmoney(this.value,'0','999999999999');"
        'Obj.Style.Item("Text-Align") = "Center"
        Select Case Text_Align
            Case textControlLib.Text_Align.Right
                Obj.Style.Item("Text-Align") = "Right"
            Case textControlLib.Text_Align.Left
                Obj.Style.Item("Text-Align") = "Left"
            Case textControlLib.Text_Align.Center
                Obj.Style.Item("Text-Align") = "Center"
        End Select
    End Sub

    Public Sub ImplementJavaIntegerText(ByRef Obj As TextBox, ByVal Text_Align As Text_Align)
        Obj.Attributes("OnChange") = "this.value=formatinteger(this.value,'0','999999999999');"
        Select Case Text_Align
            Case textControlLib.Text_Align.Right
                Obj.Style.Item("Text-Align") = "Right"
            Case textControlLib.Text_Align.Left
                Obj.Style.Item("Text-Align") = "Left"
            Case textControlLib.Text_Align.Center
                Obj.Style.Item("Text-Align") = "Center"
        End Select
    End Sub

    Public Sub ImplementJavaOnlyNumberText(ByRef Obj As TextBox)
        Obj.Attributes("OnChange") = "this.value=formatonlynumber(this.value);"
    End Sub

    Public Sub ImplementPreventCalendarMinDate(ByVal mindate As DateTime, ByVal TextBox As TextBox, ByVal captionText As String)
        Dim tmpDate As String = GL.ReportProgrammingDate(mindate)
        TextBox.Attributes("onChange") = "preventCalendarMinDate('" & tmpDate & "','" & TextBox.ClientID & "','" & captionText & "');"
    End Sub

    Public Sub ImplementPreventCalendarMinDateNotClearText(ByVal mindate As DateTime, ByVal TextBox As TextBox, ByVal captionText As String, ByVal oldValue As String)
        Dim tmpDate As String = GL.ReportProgrammingDate(mindate)
        TextBox.Attributes("onChange") = "preventCalendarMinDateNotClearText('" & tmpDate & "','" & TextBox.ClientID & "','" & captionText & "','" & oldValue & "');"
    End Sub

End Class

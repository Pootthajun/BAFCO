﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Hold
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass

    Private Property DepartmentID() As Integer
        Get
            If IsNumeric(ViewState("DepartmentID")) Then
                Return ViewState("DepartmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DepartmentID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            DepartmentID = Session("Department_ID")
            MC.BindDDlDepartment(ddlDepartment, "--------- Select ---------")
            BindData()
        End If
    End Sub

    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT HD.REF_ID,REF_NO,DT.JOB_ID,JOB_NO,OPEN_DATE,DEPARTMENT_NAME,DT.UPDATE_DATE,USER_FULLNAME UPDATE_BY,REMARK" & vbCrLf
        SQL &= "FROM REF_HEADER HD " & vbCrLf
        SQL &= "LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_USER ON DT.UPDATE_BY = MS_USER.USER_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_DEPARTMENT DPT ON HD.REF_FROM = DPT.DEPARTMENT_ID" & vbCrLf
        SQL &= "LEFT JOIN" & vbCrLf
        SQL &= "(" & vbCrLf
        SQL &= "SELECT REF_ID,JOB_ID,REMARK FROM " & vbCrLf
        SQL &= "(SELECT ROW_NUMBER() OVER(PARTITION BY REF_ID,JOB_ID ORDER BY DETAIL_ID DESC ) AS Row,* FROM" & vbCrLf
        SQL &= "(SELECT DETAIL_ID,REF_ID,JOB_ID,REMARK FROM JOB_HISTORY GROUP BY DETAIL_ID,REF_ID,JOB_ID,REMARK) TB1) TB2" & vbCrLf
        SQL &= "WHERE Row = 1" & vbCrLf
        SQL &= ") REMARK ON DT.REF_ID = REMARK.REF_ID AND DT.JOB_ID = REMARK.JOB_ID" & vbCrLf
        SQL &= "WHERE" & vbCrLf

        If Session("User_Type_Name") <> "Administrator" And Session("User_Type_Name") <> "Accounting" Then
            SQL &= "HD.REF_FROM = " & DepartmentID & " AND " & vbCrLf
        End If

        SQL &= "(" & vbCrLf
        SQL &= " DT.STATUS_CHECK = " & MasterControlClass.Job_History.Hold & vbCrLf
        SQL &= "OR DT.STATUS_PRICE = " & MasterControlClass.Job_History.Hold & vbCrLf
        SQL &= "OR DT.STATUS_APP = " & MasterControlClass.Job_History.Hold & vbCrLf
        SQL &= "OR DT.STATUS_BILL = " & MasterControlClass.Job_History.Hold & vbCrLf
        SQL &= "OR DT.STATUS_COMPLETE = " & MasterControlClass.Job_History.Hold & vbCrLf
        SQL &= ") AND "
        If txtRefNoSort.Text <> "" Then
            SQL &= "REF_NO like '%" & txtRefNoSort.Text.Replace("'", "''") & "%' AND "
        End If
        If txtJobNo.Text <> "" Then
            SQL &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtDateFrom.Text.Trim <> "" Then
            If IsDate(txtDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),DT.UPDATE_DATE,112) >= '" & txtDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtDateTo.Text.Trim <> "" Then
            If IsDate(txtDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),DT.UPDATE_DATE,112) <= '" & txtDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If
        If ddlDepartment.SelectedIndex > 0 Then
            SQL &= "DPT.DEPARTMENT_ID = " & ddlDepartment.SelectedValue & " AND "
        End If
        SQL = SQL.Substring(0, SQL.Length - 4)
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("Reference") = DT
        Navigation.SesssionSourceName = "Reference"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptDataRef
    End Sub

    Protected Sub rptDataRef_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDataRef.ItemCommand
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim RefID As Int32 = lblRefNo.Attributes("REF_ID")
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim JobID As Int32 = lblJobNo.Attributes("JOB_ID")
        Select Case e.CommandName
            Case "Return"
                Dim SQL As String = ""
                SQL &= "UPDATE REF_DETAIL SET STATUS_CHECK = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= "WHERE STATUS_CHECK = " & MasterControlClass.Job_History.Hold & " AND REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS_PRICE = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= "WHERE STATUS_PRICE = " & MasterControlClass.Job_History.Hold & " AND REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= "WHERE STATUS_APP = " & MasterControlClass.Job_History.Hold & " AND REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS_BILL = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= "WHERE STATUS_BILL = " & MasterControlClass.Job_History.Hold & " AND REF_ID = " & RefID & " And JOB_ID = " & JobID & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS_COMPLETE = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= "WHERE STATUS_COMPLETE = " & MasterControlClass.Job_History.Hold & " AND REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                'BindData()
                Response.Redirect("Hold.aspx")
            Case "View"
                Dim btnView As ImageButton = e.Item.FindControl("btnView")
                Dim PostData As Int32 = 0

                Dim Script As String = "requireTextboxFeatured('ViewJobReference.aspx?RefID=" & RefID & "&JobID=" & JobID & "&RefStatus=" & MasterControlClass.Job_Status.Checked & "&UserID=" & Session("User_ID") & "&PostData=" & PostData & "','height=600px,width=1020px,center','','');"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Protected Sub rptDataRef_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDataRef.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblDepartment As Label = e.Item.FindControl("lblDepartment")
        Dim lblOpenDate As Label = e.Item.FindControl("lblOpenDate")
        Dim lblIncompleteDate As Label = e.Item.FindControl("lblIncompleteDate")
        Dim lblBy As Label = e.Item.FindControl("lblBy")
        Dim lblRemark As Label = e.Item.FindControl("lblRemark")

        lblRefNo.Attributes.Add("REF_ID", e.Item.DataItem("REF_ID"))
        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        lblJobNo.Attributes.Add("JOB_ID", e.Item.DataItem("JOB_ID"))
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblDepartment.Text = e.Item.DataItem("DEPARTMENT_NAME").ToString
        If IsDate(e.Item.DataItem("OPEN_DATE")) Then
            lblOpenDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("OPEN_DATE"))
        End If
        If IsDate(e.Item.DataItem("UPDATE_DATE")) Then
            lblIncompleteDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("UPDATE_DATE"))
        End If
        lblBy.Text = e.Item.DataItem("UPDATE_BY").ToString
        lblRemark.Text = e.Item.DataItem("REMARK").ToString
    End Sub

    Protected Sub SortData_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefNoSort.TextChanged, txtDateFrom.TextChanged, txtDateTo.TextChanged, txtJobNo.TextChanged, ddlDepartment.SelectedIndexChanged
        BindData()
    End Sub
End Class

﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Configuration

Partial Class ImportData
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim CV As New Converter

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        If Not FileUpload1.HasFile Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select file to upload');", True)
            Exit Sub
        End If

        Dim Extension As String = ""
        If FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("VND.MS-EXCEL") > 0 Then
            Extension = ".xls"
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid File Type');", True)
            PanelData.Visible = False
            Exit Sub
        End If

        Dim FileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
        Dim FolderPath As String = "Temp/"
        If (Directory.Exists(Server.MapPath(FolderPath)) = False) Then
            Directory.CreateDirectory(Server.MapPath(FolderPath))
        End If

        FileName = GetNewFileName()
        Dim FilePath As String = Server.MapPath(FolderPath + FileName)
        FileUpload1.SaveAs(FilePath)
        Try
            Import_To_DT(FilePath, Extension)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('" & ex.Message & "');", True)
        End Try
        If File.Exists(FilePath) Then
            File.Delete(FilePath)
        End If
    End Sub

    Private Sub Import_To_DT(ByVal FilePath As String, ByVal Extension As String)
        Dim SQL As String = "SELECT JOB_NO FROM JOB_HEADER"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_JOB As New DataTable
        DA.Fill(DT_JOB)

        Dim conStr As String = ""
        conStr = ConfigurationManager.ConnectionStrings("ExcelConString").ConnectionString
        conStr = String.Format(conStr, FilePath, "Yes")

        Dim connExcel As New OleDbConnection(conStr)
        Dim cmdExcel As New OleDbCommand()
        Dim oda As New OleDbDataAdapter()
        Dim dt As New DataTable()

        cmdExcel.Connection = connExcel

        'Get the name of First Sheet 
        connExcel.Open()
        Dim dtExcelSchema As DataTable
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        Dim SheetName As String = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
        connExcel.Close()

        'Read Data from First Sheet 
        connExcel.Open()
        cmdExcel.CommandText = "SELECT * From [" & SheetName & "]"
        oda.SelectCommand = cmdExcel
        oda.Fill(dt)
        connExcel.Close()
        dt.Columns.Add("DataStatus")

        dt.DefaultView.RowFilter = "[Job Reference] <> ''"
        dt = dt.DefaultView.ToTable
        dt.DefaultView.RowFilter = "Customer <> ''"
        dt = dt.DefaultView.ToTable
        dt.DefaultView.RowFilter = "[Job Open Date] IS NOT NULL"
        dt = dt.DefaultView.ToTable

        For i As Int32 = 0 To dt.Rows.Count - 1
            DT_JOB.DefaultView.RowFilter = "JOB_NO = '" & dt.Rows(i).Item("Job Reference").ToString.Trim & "'"
            If DT_JOB.DefaultView.Count > 0 Then
                dt.Rows(i).Item("DataStatus") = "This Job Reference is already exists."
            End If
            DT_JOB.DefaultView.RowFilter = ""
        Next

        rptData.DataSource = dt
        rptData.DataBind()
        PanelData.Visible = True
    End Sub

    Function GetNewFileName() As String
        Return Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
    End Function

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim cb As CheckBox = e.Item.FindControl("cb")
        Dim lblJobRef As Label = e.Item.FindControl("lblJobRef")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim lblJobTYpe As Label = e.Item.FindControl("lblJobTYpe")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblJobDate As Label = e.Item.FindControl("lblJobDate")
        Dim lblDepartment As Label = e.Item.FindControl("lblDepartment")
        Dim lblDataStatus As Label = e.Item.FindControl("lblDataStatus")

        lblJobRef.Text = e.Item.DataItem("Job Reference").ToString
        lblStatus.Text = e.Item.DataItem("Status").ToString
        lblJobTYpe.Text = e.Item.DataItem("Job Type").ToString
        lblCustomer.Text = e.Item.DataItem("Customer").ToString
        If e.Item.DataItem("Job Open Date").ToString <> "" Then
            If IsDate(e.Item.DataItem("Job Open Date")) Then
                lblJobDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("Job Open Date"))
            Else
                lblJobDate.Text = "Invalid Format"
                lblDataStatus.Text = "Job Open Date is invalid format."
            End If
        End If
        lblDepartment.Text = e.Item.DataItem("Department").ToString

        If lblDataStatus.Text <> "Job Open Date is invalid format." Then
            lblDataStatus.Text = e.Item.DataItem("DataStatus").ToString
        End If

        Select Case lblDataStatus.Text.Trim
            Case ""
                cb.Checked = True
                For i As Int32 = 1 To 8
                    Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
                    td.Style.Item("background-color") = "#33CC33"
                    td.Style.Item("color") = "#FFFFFF"
                Next
            Case Is <> ""
                For i As Int32 = 1 To 8
                    Dim td As HtmlTableCell = e.Item.FindControl("td" & i)
                    td.Style.Item("background-color") = "#FF3333"
                    td.Style.Item("color") = "#FFFFFF"
                Next
        End Select
    End Sub

    Protected Sub btnOpenJob_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenJob.Click
        Dim DT As New DataTable
        Dim DT_JOB As New DataTable
        Dim DA As New SqlDataAdapter
        Dim DR As DataRow
        DT_JOB = GetDataDetail()
        If DT_JOB.Rows.Count > 0 Then
            Dim CheckCount As Int32 = 0
            For i As Int32 = 0 To DT_JOB.Rows.Count - 1
                If DT_JOB.Rows(i).Item("cb") = True And DT_JOB.Rows(i).Item("DataStatus").ToString = "Job Open Date is invalid format." Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Can't close Job Reference " & DT_JOB.Rows(i).Item("JobRef").ToString & "');", True)
                    Exit Sub
                ElseIf DT_JOB.Rows(i).Item("cb") = True And DT_JOB.Rows(i).Item("DataStatus").ToString = "This Job Reference is already exists." Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Can't close Job Reference " & DT_JOB.Rows(i).Item("JobRef").ToString & "');", True)
                    Exit Sub
                ElseIf DT_JOB.Rows(i).Item("cb") = True Then
                    CheckCount = CheckCount + 1
                End If
            Next
            If CheckCount = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please selected job(s)');", True)
                Exit Sub
            End If

            DT_JOB.DefaultView.RowFilter = "cb = 1"
            DT_JOB = DT_JOB.DefaultView.ToTable

            For i As Int32 = 0 To DT_JOB.Rows.Count - 1
                If DT_JOB.Rows(i).Item("DataStatus").ToString = "" Then
                    Dim SQL As String = "SELECT * FROM JOB_HEADER"
                    DA = New SqlDataAdapter(SQL, ConnStr)
                    DT = New DataTable
                    DA.Fill(DT)

                    DT.Rows.Clear()
                    DR = DT.NewRow
                    DR("JOB_ID") = GL.FindID("JOB_HEADER", "JOB_ID")
                    DR("JOB_NO") = DT_JOB.Rows(i).Item("JobRef").ToString
                    DR("CUSTOMER") = DT_JOB.Rows(i).Item("Customer").ToString
                    DR("STATUS") = MasterControlClass.Job_Status.Open
                    DR("JOB_TYPE") = DT_JOB.Rows(i).Item("JobType").ToString
                    DR("DEPARTMENT") = DT_JOB.Rows(i).Item("Department").ToString
                    DR("OPEN_BY") = Session("USER_ID")
                    DR("OPEN_DATE") = CV.StringToDate(DT_JOB.Rows(i).Item("JobDate"), "yyyy-MM-dd")
                    DT.Rows.Add(DR)
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                Else
                    Dim SQL As String = "SELECT * FROM JOB_HEADER WHERE JOB_NO = '" & DT_JOB.Rows(i).Item("JobRef").ToString & "'"
                    DA = New SqlDataAdapter(SQL, ConnStr)
                    DT = New DataTable
                    DA.Fill(DT)

                    DR = DT.Rows(0)
                    DR("CUSTOMER") = DT_JOB.Rows(i).Item("Customer").ToString
                    DR("JOB_TYPE") = DT_JOB.Rows(i).Item("JobType").ToString
                    DR("DEPARTMENT") = DT_JOB.Rows(i).Item("Department").ToString
                    DR("OPEN_BY") = Session("USER_ID")
                    DR("OPEN_DATE") = CV.StringToDate(DT_JOB.Rows(i).Item("JobDate"), "yyyy-MM-dd")
                    Dim cmd As New SqlCommandBuilder(DA)
                    DA.Update(DT)
                End If
            Next
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Open Job(s) success');", True)
        PanelData.Visible = False
    End Sub

    Function GetDataDetail() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("cb", GetType(Boolean))
        DT.Columns.Add("JobRef")
        DT.Columns.Add("Status")
        DT.Columns.Add("JobType")
        DT.Columns.Add("Customer")
        DT.Columns.Add("JobDate")
        DT.Columns.Add("Department")
        DT.Columns.Add("DataStatus")

        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            Dim lblJobRef As Label = ri.FindControl("lblJobRef")
            Dim lblStatus As Label = ri.FindControl("lblStatus")
            Dim lblJobType As Label = ri.FindControl("lblJobTYpe")
            Dim lblCustomer As Label = ri.FindControl("lblCustomer")
            Dim lblJobDate As Label = ri.FindControl("lblJobDate")
            Dim lblDepartment As Label = ri.FindControl("lblDepartment")
            Dim lblDataStatus As Label = ri.FindControl("lblDataStatus")

            Dim DR As DataRow = DT.NewRow
            DR = DT.NewRow
            DR("cb") = cb.Checked
            DR("JobRef") = lblJobRef.Text
            DR("Status") = lblStatus.Text
            DR("JobType") = lblJobType.Text
            DR("Customer") = lblCustomer.Text
            DR("JobDate") = lblJobDate.Text
            DR("Department") = lblDepartment.Text
            DR("DataStatus") = lblDataStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FileUpload1.Attributes("onchange") = "document.getElementById('" & btnUpload.ClientID & "').click();"
        End If
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
        '----------- Checking Current Status-----------
        Dim IsNotCheck As Boolean = False
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            If Not cb.Checked Then
                IsNotCheck = True '------- มีอย่างน้อย 1 อันที่ยังไม่ได้เช็ค --------
                Exit For
            End If
        Next
        '---------- Set New Status-------
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            cb.Checked = IsNotCheck
        Next
    End Sub
End Class

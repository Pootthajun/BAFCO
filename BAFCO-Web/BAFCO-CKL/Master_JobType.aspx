﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_JobType.aspx.vb" Inherits="Master_JobType" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="40" colspan="4" class="Top_Menu_Bar">
                <ul class="shortcut-buttons-set">
                    <li>
                        <asp:LinkButton ID="btnNew" runat="server" CssClass="shortcut-button">
                            <span><img src="images/icon/41.png"/><br />New</span>
                        </asp:LinkButton>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="900" valign="top">
                <asp:UpdatePanel ID="udp1" runat="server" >
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="1" style="margin:10px  0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                            <tr>
                                <td width="40" class="Grid_Header" align="center">
                                    No                    
                                </td>
                                <td width="300" class="Grid_Header">                    
                                    Name
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    IM-AIR
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    IM-SEA
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    IM-TRU
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    EX-AIR
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    EX-SEA
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    EX-TRU
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    LOGIS
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    VESSEL
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    OTHER
                                </td>
                                <td width="70" class="Grid_Header">                  
                                    Status
                                </td>
                                <td width="80" class="Grid_Header">                  
                                    Action
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef; vertical-align:middle;">
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail">
                                            <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbIA" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbIS" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbIT" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbEA" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbES" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbET" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbLOGIS" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbVESSEL" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbOTHER" runat="server" Enabled="false"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Edit" ImageUrl="images/icon/61.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                    <tr id="trEdit" runat="server" style="border-bottom:solid 1px #efefef; vertical-align:middle;">
                                        <td class="Grid_Detail" align="center" valign="middle">
                                            <asp:Image ImageUrl="images/arrow_blue_right.png" ID="imgEdit" runat="server"></asp:Image>
                                        </td>
                                        <td class="Grid_Detail">
                                            <asp:TextBox ID="txtId" runat="server" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtName" runat="server" MaxLength="500" CssClass="Textbox_Form_Required" Width="300px" TextMode="MultiLine" Height="100px"></asp:TextBox>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbIA_Edit" runat="server" />
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbIS_Edit" runat="server" />
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbIT_Edit" runat="server" />
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbEA_Edit" runat="server" />
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbES_Edit" runat="server" />
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbET_Edit" runat="server" />
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbLOGIS_Edit" runat="server"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbVESSEL_Edit" runat="server"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:CheckBox ID="cbOTHER_Edit" runat="server"/>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:DropDownList ID="ddlStatus" runat="server" 
                                            CssClass="Dropdown_Form_Required" width="70px">
                                            <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                            <asp:ListItem Text="Inactive" Value="Inactive"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:ImageButton ID="btnSave" CommandName="Save" runat="server" ToolTip="Save" ImageUrl="images/icon/59.png" Height="25px" />
                                            <asp:ImageButton ID="btnCancel" CommandName="Cancel" runat="server" ToolTip="Cancel" ImageUrl="images/icon/21.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <br>
                    </br>
                        <br></br>
                    </ContentTemplate>
                </asp:UpdatePanel>           
            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>


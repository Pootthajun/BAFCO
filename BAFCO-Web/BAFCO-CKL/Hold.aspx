﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Hold.aspx.vb" Inherits="Hold" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table cellpadding=0 cellspacing=0 width="100%">
        <tr>
            <td style="padding-left:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">    
                    <tr>
                        <td valign="top">
                             <table align="left" cellpadding="0" cellspacing="1">
                                <tr>
                                    <td>
                                        <div class="Fieldset_Container" >
                                            <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                            <table width="600px" align="center" cellspacing="5px;" style="margin-left:10px;">
                                                <tr>
                                                    <td height="25" class="NormalTextBlack">
                                                        Reference No
                                                    </td>
                                                    <td height="25">
                                                        <asp:TextBox ID="txtRefNoSort" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25"></td>
                                                    <td height="25" class="NormalTextBlack">
                                                        Job No
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtJobNo" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25"></td>
                                                    <td height="25" class="NormalTextBlack">
                                                        Department
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="Textbox_Form_White" width="147px" Height="25px" style="text-align:center;" AutoPostBack="true"></asp:DropDownList>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Incompleted <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgDateFrom"
				                                        TargetControlID="txtDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="80" height="25" class="NormalTextBlack">
                                                        Incompleted <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgDateTo"
				                                        TargetControlID="txtDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td></td>
                                                </tr> 
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>       
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">    
                                <tr>
                                    <td style="padding-left:10px;">
                                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                                            <tr>
                                                <td width="40" class="Grid_Header">
                                                    No
                                                </td>
                                                <td width="80" class="Grid_Header">
                                                    Reference No
                                                </td>
                                                <td width="100" class="Grid_Header">
                                                    Job No
                                                </td>
                                                <td width="120" class="Grid_Header">
                                                    Department
                                                </td>
                                                <td width="100" class="Grid_Header">
                                                    Open Date
                                                </td>
                                                <td width="100" class="Grid_Header">
                                                    Incomplete Date
                                                </td>
                                                <td width="200" class="Grid_Header">
                                                    Incomplete By
                                                </td>
                                                <td width="200" class="Grid_Header">
                                                    Remark
                                                </td>
                                                <td width="70" class="Grid_Header">                  
                                                    Action
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7">
                                                    <asp:Repeater ID="rptDataRef" runat="server">
                                                        <ItemTemplate>
                                                            <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                                    <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                                    <asp:Label ID="lblRefNo" runat="server" ></asp:Label>
                                                                </td> 
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                                    <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                                    <asp:Label ID="lblDepartment" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                                    <asp:Label ID="lblOpenDate" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                                    <asp:Label ID="lblIncompleteDate" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:left; padding-left:10px;">
                                                                    <asp:Label ID="lblBy" runat="server" ></asp:Label>
                                                                </td>
                                                                <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:left; padding-left:10px;">
                                                                    <asp:Label ID="lblRemark" runat="server" ></asp:Label>
                                                                </td>
                                                                <td class="Grid_Detail" align="center" valign="middle">
                                                                    <asp:ImageButton ID="btnReturn" CommandName="Return" runat="server" ToolTip="Return Document" ImageUrl="images/icon/14.png" Height="25px" />
                                                                    <Ajax:ConfirmButtonExtender ID="btnReturn_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to return document?" TargetControlID="btnReturn">
                                                                    </Ajax:ConfirmButtonExtender>
                                                                    <asp:Imagebutton ID="btnView" runat="server" CommandName="View" ToolTip="View" ImageUrl="images/icon/70.png" Height="25px"/>
                                                                </td>                                 
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </table>
                                        <div style="margin-left:10px">
                                            <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="15"/>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table> 
</asp:Content>


﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data

Partial Class Print_Approve
    Inherits System.Web.UI.Page

    Dim Cov As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim UID As String = Request.QueryString("Print_ID")
        Dim DT As DataTable = Session("Print_" & UID)
        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../Report/Approve.rpt"))
        cc.SetDataSource(DT)
        CReport.ReportSource = cc
        Response.AddHeader("Content-Type", "application/pdf")
        Response.AppendHeader("Content-Disposition", "filename=" & UID & ".pdf")
        Dim B As Byte() = Cov.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
        Response.BinaryWrite(B)
    End Sub
End Class

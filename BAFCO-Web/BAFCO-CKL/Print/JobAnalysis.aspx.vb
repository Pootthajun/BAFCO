﻿Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data

Partial Class Print_JobAnalysis
    Inherits System.Web.UI.Page

    Dim Cov As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim UID As String = Request.QueryString("Print_ID")
        'Dim DT As DataTable = Session("Print_" & UID)
        'Dim cc As New ReportDocument()
        'cc.Load(Server.MapPath("../Report/JobAnalysis.rpt"))
        'cc.SetDataSource(DT)
        'CReport.ReportSource = cc
        'Response.AddHeader("Content-Type", "application/pdf")
        'Response.AppendHeader("Content-Disposition", "filename=" & UID & ".pdf")
        'Dim B As Byte() = Cov.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
        'Response.BinaryWrite(B)

        Dim UID As String = Request.QueryString("Print_ID")
        Dim DocType As String = Request.QueryString("DocType")
        Dim DT As DataTable = Session("Print_" & UID)
        Dim cc As New ReportDocument()
        cc.Load(Server.MapPath("../Report/JobAnalysis.rpt"))
        cc.SetDataSource(DT)
        CReport.ReportSource = cc
        Select Case DocType
            Case "PDF"
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=" & UID & ".pdf")
                Dim B As Byte() = Cov.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)
            Case "EXCEL"
                Response.AddHeader("Content-Type", "application/excel")
                Response.AppendHeader("Content-Disposition", "filename=" & UID & ".xls")
                Dim B As Byte() = Cov.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.Excel))
                Response.BinaryWrite(B)
        End Select
    End Sub
End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Approve.aspx.vb" Inherits="Print_Approve" %>

<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <CR:CrystalReportViewer ID="CReport" runat="server" AutoDataBind="true" 
            HasToggleGroupTreeButton="False" 
            HasCrystalLogo="False" HasDrillUpButton="False" />
    </form>
</body>
</html>

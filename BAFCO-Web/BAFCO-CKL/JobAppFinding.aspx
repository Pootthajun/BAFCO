﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JobAppFinding.aspx.vb" Inherits="JobAppFinding" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head id="Head1" runat="server">
    <title>Job Finding</title>
    <link href="style/css.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="Script/script.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManagerMain" runat="Server"></asp:ScriptManager>
        <table width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
            <tr>
                
                <td height="40" align="center" class="Master_Header_Text">
                    Job Finding
                </td>                
            </tr>
        </table>
            <asp:UpdatePanel ID="udp1" runat="server" >
                <ContentTemplate>
                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
                    <div style="width:750px; height:500px; position:absolute; top:50px; overflow-y:auto; overflow-x:auto;">
                    <table width="740" cellpadding="0" cellspacing="1" style="margin: 10px 0px 0px 0px;" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="30" class="Grid_Header">
                                <asp:Button ID="btnAll" runat="server" Text="All" CssClass="Button_White"/>
                            </td>
                            <td width="100" class="Grid_Header">
                                Job No
                            </td>
                            <td width="100" class="Grid_Header">
                                Reference No
                            </td>
                            <td width="350" class="Grid_Header">
                                Customer
                            </td>
                            <td width="90" class="Grid_Header">
                                Close Date
                            </td>
                            <td width="120" class="Grid_Header">
                                Document Type
                            </td>
                        </tr>
                        <tr>
                            <td height="25px">
                                <asp:Button ID="btnSearch" runat="server" style="display:none;"/>
                            </td>
                            <td height="25px">
                                <asp:TextBox ID="txtJobNo" runat="server" Width="100%" CssClass="Textbox_Form_White" AutoPostBack="true"></asp:TextBox>
                            </td>
                            <td height="25px">
                                <asp:TextBox ID="txtRefNo" runat="server" Width="100%" CssClass="Textbox_Form_White" AutoPostBack="true"></asp:TextBox>
                            </td>
                            <td height="25px">
                                <asp:TextBox ID="txtCustomer" runat="server" Width="100%" CssClass="Textbox_Form_White" AutoPostBack="true"></asp:TextBox>
                            </td>
                            <td height="25px"></td>
                            <td height="25px">
                                <asp:DropDownList ID="ddlDocType" runat="server" Width="100%" CssClass="Dropdown_Form_White" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trRow" runat="server" style="border-bottom:solid 1px #efefef;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td class="Grid_Detail" align="center">
                                        <asp:CheckBox ID="cb" runat="server" />
                                    </td>
                                    <td class="Grid_Detail" style="text-align:center;">
                                        <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" style="text-align:center;">
                                        <asp:Label ID="lblRefNo" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" style="text-align:center;">
                                        <asp:Label ID="lblDate" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" style="text-align:center;">
                                        <asp:Label ID="lblDocType" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    </div>
                    </asp:Panel>
                    <div style="text-align:left; position:fixed; width:750px; bottom:5px; background-color:White; overflow-x:hidden;">
                    <table width="100%" cellpadding="0" cellspacing="0" style="margin: 10px 0px 0px 0px; background-color: #FFFFFF;">
                        <tr>
                            <td width="40" style="text-align:right;">
                                <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="Button_Red" Width="100px" Height="30px"/>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                        </tr>
                    </table>  
                    </div>
                        
                </ContentTemplate>
            </asp:UpdatePanel>
        
    </form>
</body>

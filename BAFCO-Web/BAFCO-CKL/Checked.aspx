﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Checked.aspx.vb" Inherits="Checked" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="divJobList" runat="server" style="position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1;" visible="False">                    
        <div style="width:100%; height:100%; position:absolute; background-color:Gray; filter:alpha(opacity=80); opacity: 0.80;" ></div>
        <div style="left:15%; top:5%; position:absolute; background-color:#F6F3F3; padding: 20px 20px 20px 20px; width:880px; height:530px;">
            <table width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
                <tr>
                    <td height="40" align="center" class="Master_Header_Text">
                        <asp:Label ID="lblHead" runat="server" Text="-"></asp:Label>
                    </td>                
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="1" style="padding-top:10px; padding-bottom:10px;">
                <tr>
                    <td height="25" class="NormalTextBlack" width="100px">
                        Reference No
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtRefNo" runat="server" CssClass="Textbox_Form_Disable" Enabled="false" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td height="25" width="40px"></td>
                </tr>
            </table>    
            <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                <tr>
                    <td width="40" class="Grid_Header">
                        <asp:Button ID="btnAll" runat="server" Text="All" CssClass="Button_White"/>
                    </td>
                    <td width="40px" class="Grid_Header">
                        Status
                    </td>
                    <td width="100" class="Grid_Header">
                        Job Reference No
                    </td>
                    <td width="400px" class="Grid_Header">
                        Customer
                    </td>
                    <td width="90px" class="Grid_Header">
                        Received Date
                    </td>
                    <td width="90px" class="Grid_Header">
                        Document Type
                    </td>
                    <td width="90px" class="Grid_Header">
                        Payment
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <asp:Panel ID="PanelData" runat="server">
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                    <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                        <asp:CheckBox ID="cb" runat="server" />
                                    </td>
                                    <td id="td2" runat="server" class="Grid_Detail" align="center" valign="middle">
                                        <asp:Imagebutton ID="btnView" runat="server" CommandName="View"  ImageUrl="images/icon/70.png" Height="25px"/>
                                    </td>
                                    <td id="td3" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px; text-align:center;">
                                        <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                    </td> 
                                    <td id="td4" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px;">
                                        <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                    </td>
                                    <td id="td5" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px; text-align:center;">
                                        <asp:Label ID="lblJobReceiveDate" runat="server" ></asp:Label>
                                    </td>
                                    <td id="td6" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px; text-align:center;">
                                        <asp:Label ID="lblDocType" runat="server" ></asp:Label>
                                    </td>
                                    <td id="td7" runat="server" bgcolor="#DDDDDD" style="height:25px; text-align:center;">
                                        <asp:Label ID="lblPayment" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        </asp:Panel>
                    </td>
                </tr>
            </table>      
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td height="40" align="right" valign="bottom">
                        <asp:Button ID="btnConfirmation" runat="server" Text="Confirm Ticked Job(s) Without Remark" CssClass="Button_Red" Width="240px" Height="30px"/>
                        <Ajax:ConfirmButtonExtender ID="btnConfirm_" runat="server" Enabled="true" ConfirmText="Are you sure you want to confirm?" TargetControlID="btnConfirmation">
                        </Ajax:ConfirmButtonExtender>
                        <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="Button_Red" Width="120px" Height="30px"/>
                    </td>              
                </tr>
            </table>
            <table cellpadding="0" cellspacing="1" width="100%" style="position:absolute; top:530px;">
                <tr>
                    <td>
                        <asp:Image ID="Image1" runat="server" ImageUrl="images/icon/24.png" Height="30px" Width ="30px"/>
                    </td>    
                    <td>
                        = Wait
                    </td> 
                    <td>
                        <asp:Image ID="Image3" runat="server" ImageUrl="images/icon/21.png" Height="25px" Width ="25px"/>
                    </td>    
                    <td>
                        = Incomplete
                    </td> 
                    <td>
                        <asp:Image ID="Image4" runat="server" ImageUrl="images/icon/03.png" Height="30px" Width ="30px"/>
                    </td>    
                    <td>
                        = Hold
                    </td> 
                    <td>
                        <asp:Image ID="Image2" runat="server" ImageUrl="images/icon/57.png" Height="30px" Width ="30px"/>
                    </td>    
                    <td>
                        = Delete
                    </td> 
                    <td style="width:60%">
                    </td>      
                </tr>
            </table>
        </div>
    </div>
        
    <table cellpadding=0 cellspacing=0 width="100%">
        <tr>
            <td style="padding-left:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">    
                    <tr>
                        <td valign="top">
                             <table align="left" cellpadding="0" cellspacing="1">
                                <tr>
                                    <td>
                                        <div class="Fieldset_Container" >
                                            <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                            <table width="600px" align="center" cellspacing="5px;" style="margin-left:10px;">
                                                <tr>
                                                    <td height="25" class="NormalTextBlack">
                                                        Reference No
                                                    </td>
                                                    <td height="25">
                                                        <asp:TextBox ID="txtRefNoSort" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25"></td>
                                                    <td height="25" class="NormalTextBlack">
                                                        Job No
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtJobNo" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Received <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgDateFrom"
				                                        TargetControlID="txtDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="80" height="25" class="NormalTextBlack">
                                                        Received <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgDateTo"
				                                        TargetControlID="txtDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td></td>
                                                </tr> 
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>       
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px;">
                            <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                                <tr>
                                    <td width="40" class="Grid_Header" rowspan ="2">
                                        No
                                    </td>
                                    <td width="120" class="Grid_Header" rowspan ="2">
                                        Reference No
                                    </td>
                                    <td width="120" class="Grid_Header"rowspan ="2">
                                        From
                                    </td>
                                    <td class="Grid_Header" colspan="8">
                                        Number of Job(s)
                                    </td>
                                    <td width="70" class="Grid_Header" rowspan ="2">                  
                                        Action
                                    </td>
                                </tr>
                                <tr>
                                    <td width="80" class="Grid_Header">                  
                                        Checked
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Priced
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Approving
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Billed
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Completed
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        FOC
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Delete
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Total
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:Repeater ID="rptDataRef" runat="server">
                                            <ItemTemplate>
                                                <tr style="border-bottom:solid 1px #efefef;" >
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblRefNo" runat="server" ></asp:Label>
                                                    </td>  
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblRefFrom" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblCheck" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblPrice" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblApp" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblBill" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblComplete" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblFOC" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblDelete" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblTotal" runat="server" ></asp:Label>
                                                    </td>
                                                    <td class="Grid_Detail" align="center" valign="middle">
                                                        <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Checked" ImageUrl="images/icon/70.png" Height="25px"/>
                                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                                        <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete">
                                                        </Ajax:ConfirmButtonExtender>
                                                    </td>                                 
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                            <div style="margin-left:10px">
                                <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="15"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtRefresh" runat="server" style="display:none;"></asp:TextBox>
    <asp:Button ID="btnRefresh"  runat="server" style="display:none;"/>
    
</asp:Content>


﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CloseJob.aspx.vb" Inherits="Filled" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
        <ContentTemplate>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">    
            <tr>
                <td valign="top">
                     <table align="left" cellpadding="0" cellspacing="1">
                        <tr>
                            <td>
                                <div class="Fieldset_Container" >
                                    <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                    <table width="810px" align="center" cellspacing="5px;" style="margin-left:10px; ">
                                        <tr>
                                            <td height="25" class="NormalTextBlack">
                                                Job No
                                            </td>
                                            <td height="25">
                                                <asp:TextBox ID="txtJobNo" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                            </td>
                                            <td height="25"></td>
                                            <td height="25" class="NormalTextBlack">
                                                Customer
                                            </td>
                                            <td height="25" colspan="3">
                                                <asp:DropDownList ID="ddlCustomer" runat="server" CssClass="Textbox_Form_White" width="300px" Height="25px" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" class="NormalTextBlack">
                                                Job Type
                                            </td>
                                            <td height="25">
                                                <asp:DropDownList ID="ddlJobType" runat="server" CssClass="Textbox_Form_White" width="147px" Height="25px" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                            <td height="25"></td>
                                            <td height="25" class="NormalTextBlack">
                                                Department
                                            </td>
                                            <td height="25" colspan="3">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="Textbox_Form_White" width="147px" Height="25px" AutoPostBack="true"></asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="100" height="25" class="NormalTextBlack">
                                                Job Date From
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtJobDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgJobDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgJobDateFrom_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgJobDateFrom"
				                                TargetControlID="txtJobDateFrom"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="100" height="25" class="NormalTextBlack">
                                                Job Date To
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtJobDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgJobDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgJobDateTo_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgJobDateTo"
				                                TargetControlID="txtJobDateTo"></Ajax:CalendarExtender>
                                            </td>
                                            <td></td>
                                        </tr>  
                                </div>
                            </td>
                        </tr>
                    </table>       
                </td>
            </tr>
            <tr>
                <td style="padding-left:10px;">
                    <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="40" class="Grid_Header">
                                No
                            </td>
                            <td width="100" class="Grid_Header">
                                Job No
                            </td>
                            <td class="Grid_Header">
                                Customer
                            </td>
                            <td width="100" class="Grid_Header">
                                Open Date
                            </td>
                            <td width="80" class="Grid_Header">
                                Job Type
                            </td>
                            <td width="100" class="Grid_Header">
                                Department
                            </td>
                            <td width="100" class="Grid_Header">
                                Document Type
                            </td>
                            <td width="80" class="Grid_Header">
                                Action
                            </td>
                            
                        </tr>
                        <tr>
                            <td colspan="7">
                                <asp:Repeater ID="rptData" runat="server">
                                    <ItemTemplate>
                                        <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                            </td>
                                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; padding:0 0 0 10px;">
                                                <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                            </td> 
                                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; padding:0 10px 0 10px;">
                                                <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                            </td>
                                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblOpenDate" runat="server" ></asp:Label>
                                            </td>
                                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblJobType" runat="server" ></asp:Label>
                                            </td>
                                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblDepartment" runat="server" ></asp:Label>
                                            </td>
                                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:DropDownList ID="ddlDocType" runat="server" CssClass="Dropdown_Form_White" width="100%" style="text-align:center;"></asp:DropDownList>
                                            </td>
                                            <td class="Grid_Detail" align="center" valign="middle">
                                                <asp:ImageButton ID="btnCloseJob" CommandName="CloseJob" runat="server" ToolTip="Close Job" ImageUrl="images/icon/70.png" Height="25px"/>
                                                <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete Job" ImageUrl="images/icon/57.png" Height="25px" />
                                                <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete">
                                                </Ajax:ConfirmButtonExtender>
                                            </td>                                 
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <div style="margin-left:10px">
                        <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="15"/>
                    </div>
                    <br />
                </td>
            </tr>
        </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


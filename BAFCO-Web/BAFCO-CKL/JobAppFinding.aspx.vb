﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class JobAppFinding
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib

    Private Property JobID() As String
        Get
            Return ViewState("JobID")
        End Get
        Set(ByVal value As String)
            ViewState("JobID") = value
        End Set
    End Property

    Private Property AppNo() As String
        Get
            Return ViewState("AppNo")
        End Get
        Set(ByVal value As String)
            ViewState("AppNo") = value
        End Set
    End Property

    Private Property Mode() As String
        Get
            Return ViewState("Mode")
        End Get
        Set(ByVal value As String)
            ViewState("Mode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            JobID = Request.QueryString("JobID").Replace("?", "")
            AppNo = Request.QueryString("AppNo").Replace("?", "")
            Mode = Request.QueryString("Mode").Replace("?", "")
            MC.BindDDlDocType(ddlDocType, "-------- All --------", "")
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT REF_NO,JOB_ID,JOB_NO,CUSTOMER,PRICE_DATE,REF_DETAIL.DOC_TYPE_ID,DOC_TYPE_NAME" & vbCrLf
        SQL &= "FROM REF_DETAIL LEFT JOIN MS_DOC_TYPE " & vbCrLf
        SQL &= "ON REF_DETAIL.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "LEFT JOIN REF_HEADER ON REF_DETAIL.REF_ID = REF_HEADER.REF_ID" & vbCrLf
        SQL &= "WHERE REF_DETAIL.STATUS = " & MasterControlClass.Job_Status.Approving & vbCrLf
        If JobID <> "" Then
            SQL &= "AND JOB_ID NOT IN (" & JobID & " )" & vbCrLf
        End If
        If Mode = "New" Then
            SQL &= "AND STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
        Else
            SQL &= "AND (STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
            SQL &= "     OR APP_NO = '" & AppNo & "')" & vbCrLf
        End If

        SQL &= "ORDER BY JOB_NO"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim Filter As String = ""
        If txtRefNo.Text <> "" Then
            Filter &= "REF_NO like '%" & txtRefNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtJobNo.Text <> "" Then
            Filter &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtCustomer.Text <> "" Then
            Filter &= "CUSTOMER like '%" & txtCustomer.Text.Replace("'", "''") & "%' AND "
        End If
        If ddlDocType.SelectedIndex > 0 Then
            Filter &= "DOC_TYPE_ID = " & ddlDocType.SelectedValue.ToString & " AND "
        End If
        If Filter <> "" Then Filter = Filter.Substring(0, Filter.Length - 4)
        DT.DefaultView.RowFilter = Filter

        rptData.DataSource = DT
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim trRow As HtmlTableRow = e.Item.FindControl("trRow")
        Dim cb As CheckBox = e.Item.FindControl("cb")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblDate As Label = e.Item.FindControl("lblDate")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        lblJobNo.Attributes.Add("JobID", e.Item.DataItem("JOB_ID"))
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        If IsDate(e.Item.DataItem("PRICE_DATE")) Then
            lblDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("PRICE_DATE"))
        End If
        lblDocType.Text = e.Item.DataItem("DOC_TYPE_NAME").ToString

        'If InStr(JobID, "|" & e.Item.DataItem("JOB_ID")) > 0 Then
        '    cb.Checked = True
        'Else
        '    cb.Checked = False
        'End If
    End Sub

    Protected Sub btnAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAll.Click
        '----------- Checking Current Status-----------
        Dim IsNotCheck As Boolean = False
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            If Not cb.Checked Then
                IsNotCheck = True '------- มีอย่างน้อย 1 อันที่ยังไม่ได้เช็ค --------
                Exit For
            End If
        Next
        '---------- Set New Status-------
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            cb.Checked = IsNotCheck
        Next
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim Check As Int32 = 0
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim cb As CheckBox = ri.FindControl("cb")
            If cb.Checked = True Then
                Check = Check + 1
            End If
        Next

        If Check = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please choose Job No');", True)
            Exit Sub
        End If

        Dim Script As String = "returnSelectedValue('" & GetJobCheck().Replace("'", "\'") & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseDialog", Script, True)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim Script As String = "window.close();"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
    End Sub

    Protected Sub txtJobNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefNo.TextChanged, txtJobNo.TextChanged, txtCustomer.TextChanged, ddlDocType.SelectedIndexChanged
        BindData()
    End Sub

    Function GetJobCheck() As String

        Dim DT As New DataTable
        DT.Columns.Add("JOB_ID")

        If rptData.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptData.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim cb As CheckBox = ri.FindControl("cb")
                Dim lblJobNo As Label = ri.FindControl("lblJobNo")
                If cb.Checked = True Then
                    Dim DR As DataRow = DT.NewRow
                    DR("JOB_ID") = lblJobNo.Attributes("JobID")
                    DT.Rows.Add(DR)
                End If
            Next
            Dim RetJobID As String = ""
            For i As Int32 = 0 To DT.Rows.Count - 1
                RetJobID = RetJobID & DT.Rows(i).Item("JOB_ID").ToString & ","
            Next
            RetJobID = RetJobID.Substring(0, RetJobID.Length - 1)
            Return RetJobID
        End If
        Return ""
    End Function
End Class

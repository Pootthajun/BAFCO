﻿function requireTextboxDialog(url,width,height,TextboxClientID,HiddenButtonClientID)
{
   if(url.toString().indexOf('?')!=url.toString().length-1)url+="?";
   window.open(url + "&txt=" + TextboxClientID + "&btn=" + HiddenButtonClientID,"","height="+height+",width="+width+",center=true,scrollbars,resizable");
}

function requireTextboxFeatured(url, featured, TextboxClientID, HiddenButtonClientID) {
    if (url.toString().indexOf('?') != url.toString().length - 1) url += "?";
    window.open(url + "&txt=" + TextboxClientID + "&btn=" + HiddenButtonClientID, "", featured);
}

function returnSelectedValue(value,TextboxClientID,HiddenButtonClientID)
{
    if((!opener)){window.close(); return; } 
    if(TextboxClientID != "")
    {
        var txt=opener.document.getElementById(TextboxClientID);
        if(!txt){ } else{txt.value=value;}
    }
    if(HiddenButtonClientID !='')
    {
        var btn=opener.document.getElementById(HiddenButtonClientID);
        if(!btn){ } else{btn.click();}
    }
    window.close(); opener.focus();
}

function openPrintWindow(url,width,height) {
    var randomnumber = Math.floor((Math.random() * 100) + 1);
    window.open(url,"","height="+height+",width="+width+",center=true,scrollbars,resizable");
}

    function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
} 

function formatmoney(input,minvalue,maxvalue)
{
   var firstch = '';
   if(input!='')
   {
        switch(input.substr(0,1))
        {
            case "+":
            case "-":firstch=input.substr(0,1);
                    break;
        }
   }
  
   var tmp = parseFloat(replaceComma(input).replace('-','').replace('+',''));     
   if( tmp.toString().toUpperCase() == 'NAN' ) return ''; 
  
    //Check Min Max
    if((minvalue != '') && (tmp <minvalue)) tmp=minvalue;
    if((maxvalue != '') && (tmp >maxvalue)) tmp=maxvalue;
    
    return firstch + addCommas(Number(tmp).toFixed(2)); 
  }

function formatinteger(input,minvalue,maxvalue)
{
   var firstch = '';
   if(input!='')
   {
        switch(input.substr(0,1))
        {
            case "+":
            case "-":firstch=input.substr(0,1);
                    break;
        }
   }
   
   var tmp = parseInt(replaceComma(input).replace('-','').replace('+',''));
   
   if( tmp.toString().toUpperCase() == 'NAN' ) return ''; 
    
    //Check Min Max
    if((minvalue != '') && (tmp <minvalue)) tmp=minvalue;
    if((maxvalue != '') && (tmp >maxvalue)) tmp=maxvalue;
    
    return firstch + addCommas(Number(tmp).toFixed(0)); 
}

function formatonlynumber(input)
{
   var result='';
   for(i=0;i<input.length;i++)
   {
        switch(input.toString().substr(i,1))
        {
            case "0" :
            case "1" :
            case "2" :
            case "3" :
            case "4" :
            case "5" :
            case "6" :
            case "7" :
            case "8" :
            case "9" : result += input.toString().substr(i,1);
                      break;
        }       
   }   
    return result; 
}

function replaceComma(input)
{
    while(input.toString().indexOf(",")>-1) input=input.replace(",","");
    return input    
}

function preventCalendarMinDate(mindate,txtClientId,captionText)
{
    if(txtClientId == '' || mindate=='') return;
    
    var txt=document.getElementById(txtClientId);
    if(!txt) return;
    if(txt.value<mindate)
    {
        alert(captionText + " must be " + mindate + " or later");
        txt.value='';
        return;
    }
    var date_regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/ ;
    if(!(date_regex.test(txt.value)))
    {
        alert(captionText + " must be " + mindate + " or later");
        txt.value='';
    }

}

function preventCalendarMinDateNotClearText(mindate, txtClientId, captionText, oldValue) {
    if (txtClientId == '' || mindate == '') return;

    var txt = document.getElementById(txtClientId);
    if (!txt) return;
    if (txt.value < mindate) {
        alert(captionText + " must be " + mindate + " or later");
        txt.value = oldValue;
        return;
    }
    var date_regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/ ;
    if(!(date_regex.test(txt.value)))
    {
        alert(captionText + " must be " + mindate + " or later");
        txt.value = oldValue;
    }
}

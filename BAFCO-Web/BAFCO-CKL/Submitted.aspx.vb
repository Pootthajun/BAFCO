﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Submitted
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property UserID() As Integer
        Get
            Return ViewState("UserID")
        End Get
        Set(ByVal value As Integer)
            ViewState("UserID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            UserID = Session("User_ID")
            AddLineItem()
            Dim Item As New ListItem
            Item = New ListItem(MC.Get_RefStatus(MasterControlClass.Ref_Status.Submitted), MasterControlClass.Ref_Status.Submitted)
            ddlStatus.Items.Add(Item)
            Item = New ListItem(MC.Get_RefStatus(MasterControlClass.Ref_Status.Approve), MasterControlClass.Ref_Status.Approve)
            ddlStatus.Items.Add(Item)
            BindData()
        End If
    End Sub

#Region "Reference"
    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT HD.REF_ID,REF_NO,REF_DATE,HD.STATUS" & vbCrLf
        SQL &= ",DPT.DEPARTMENT_NAME REF_FROM_NAME,COUNT(1) AS JOB" & vbCrLf
        SQL &= "FROM REF_HEADER HD" & vbCrLf
        SQL &= "LEFT JOIN MS_DEPARTMENT DPT ON HD.REF_FROM = DPT.DEPARTMENT_ID" & vbCrLf
        SQL &= "LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
        If ddlStatus.SelectedValue = MasterControlClass.Ref_Status.Submitted Then
            SQL &= "WHERE HD.STATUS = " & MasterControlClass.Ref_Status.Submitted & " AND "
        ElseIf ddlStatus.SelectedValue = MasterControlClass.Ref_Status.Approve Then
            SQL &= "WHERE HD.STATUS = " & MasterControlClass.Ref_Status.Approve & " AND "
        End If
        If txtRefNoSort.Text <> "" Then
            SQL &= "REF_NO like '%" & txtRefNoSort.Text.Replace("'", "''") & "%' AND "
        End If

        If txtDateFrom.Text.Trim <> "" Then
            If IsDate(txtDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),REF_DATE,112) >= '" & txtDateFrom.Text.Replace("-", "") & "' AND "
            End If
        End If
        If txtDateTo.Text.Trim <> "" Then
            If IsDate(txtDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),REF_DATE,112) <= '" & txtDateTo.Text.Replace("-", "") & "' AND "
            End If
        End If

        If Session("User_Type_Name") <> "Administrator" And Not Session("User_ID") Is Nothing Then
            SQL &= "HD.SUBMIT_BY = " & Session("User_ID") & " AND "
        End If

        SQL = SQL.Substring(0, SQL.Length - 4)
        SQL &= vbCrLf & "AND DT.STATUS <> " & MasterControlClass.Job_Status.Delete
        SQL &= vbCrLf & "GROUP BY HD.REF_ID,REF_NO,REF_DATE,HD.STATUS,DPT.DEPARTMENT_NAME"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("Reference") = DT
        Navigation.SesssionSourceName = "Reference"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptDataRef
    End Sub

    Protected Sub rptDataRef_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDataRef.ItemCommand
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim RefID As Int32 = lblRefNo.Attributes("REF_ID")
        Dim lblRefDate As Label = e.Item.FindControl("lblRefDate")
        Select Case e.CommandName
            Case "View"
                divNewRef.Visible = True
                lblHead.Text = "View Reference List"
                txtRefNo.Text = lblRefNo.Text
                txtRefNo.Enabled = False
                txtRefNo.CssClass = "Textbox_Form_Disable"
                txtDate.Text = lblRefDate.Text

                btnSave.Visible = False
                btnCreate.Visible = False
                BindJob(RefID)

            Case "Edit"
                divNewRef.Visible = True
                lblHead.Text = "Edit Reference List"

                txtRefNo.Text = lblRefNo.Text
                txtRefNo.Attributes.Add("REF_ID", RefID)
                txtRefNo.Enabled = True
                txtRefNo.CssClass = "Textbox_Form_Disable"
                txtDate.Text = lblRefDate.Text

                btnSave.Visible = True
                btnCreate.Visible = False
                BindJob(RefID)

                PanelData.Enabled = True
            Case "Approve"
                txtDialogPassword.Attributes.Add("REF_ID", RefID)
                txtDialogPassword.Text = ""
                divConfirmApprove.Visible = True
                btnDialogApp.Visible = True
                btnDialogRevert.Visible = False
                txtDialogPassword.Focus()
            Case "Print"
                Print(RefID)
            Case "Delete"
                Dim SQL As String = ""
                SQL = "DELETE FROM JOB_HEADER WHERE JOB_ID IN " & vbCrLf
                SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
                SQL &= "WHERE REF_ID = " & RefID

                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
            Case "RevertCloseJob"
                Dim Sql As String = ""
                Sql &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & "  WHERE JOB_ID IN (SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
                Sql &= "DELETE FROM REF_DETAIL WHERE REF_ID = " & RefID & vbCrLf
                Sql &= "DELETE FROM REF_HEADER WHERE REF_ID = " & RefID & vbCrLf
                Dim conn As New SqlConnection(ConnStr)
                Dim SqlCmd As SqlCommand
                conn.Open()
                SqlCmd = New SqlCommand(Sql, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
            Case "RevertSubmit"
                txtDialogPassword.Attributes.Add("REF_ID", RefID)
                txtDialogPassword.Text = ""
                divConfirmApprove.Visible = True
                btnDialogApp.Visible = False
                btnDialogRevert.Visible = True
                txtDialogPassword.Focus()
        End Select
    End Sub

    Protected Sub rptDataRef_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDataRef.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblRefDate As Label = e.Item.FindControl("lblRefDate")
        Dim lblRefFrom As Label = e.Item.FindControl("lblRefFrom")
        Dim lblRefJob As Label = e.Item.FindControl("lblRefJob")
        Dim btnPrint As ImageButton = e.Item.FindControl("btnPrint")
        Dim btnView As ImageButton = e.Item.FindControl("btnView")
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnApprove As ImageButton = e.Item.FindControl("btnApprove")
        Dim btnRevertCloseJob As ImageButton = e.Item.FindControl("btnRevertCloseJob")
        Dim btnRevertSubmit As ImageButton = e.Item.FindControl("btnRevertSubmit")

        lblRefNo.Attributes.Add("REF_ID", e.Item.DataItem("REF_ID"))
        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        lblRefJob.Text = e.Item.DataItem("JOB").ToString
        If IsDate(e.Item.DataItem("REF_DATE")) Then
            lblRefDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("REF_DATE"))
        End If
        lblRefFrom.Text = e.Item.DataItem("REF_FROM_NAME").ToString
        If e.Item.DataItem("STATUS") >= MasterControlClass.Ref_Status.Approve Then
            btnPrint.Visible = True
            btnEdit.Visible = False
            btnView.Visible = False
            btnDelete.Visible = False
            btnApprove.Visible = False
            btnRevertCloseJob.Visible = False
            btnRevertSubmit.Visible = True
        Else
            btnPrint.Visible = False
            btnEdit.Visible = True
            btnView.Visible = False
            btnDelete.Visible = True
            btnApprove.Visible = True
            btnRevertCloseJob.Visible = True
            btnRevertSubmit.Visible = False
        End If

        If Session("User_Type_Name") = "Administrator" Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If
    End Sub

    Protected Sub SortData_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefNoSort.TextChanged, txtDateFrom.TextChanged, txtDateTo.TextChanged, ddlStatus.SelectedIndexChanged
       If ddlStatus.SelectedValue = MasterControlClass.Ref_Status.Approve Then
            btnNew.Visible = False
        Else
            btnNew.Visible = True
        End If
        BindData()
    End Sub
#End Region

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        divNewRef.Visible = True
        lblHead.Text = "Create New Reference List"
        txtRefNo.Text = "Auto Number"
        txtRefNo.ReadOnly = True
        txtRefNo.CssClass = "Textbox_Form_Disable"
        txtRefNo.Attributes.Add("REF_ID", "")
        txtDate.Text = GL.ReportProgrammingDate(Date.Now)
        btnSave.Visible = False
        btnCreate.Visible = True
        ClearData()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        divNewRef.Visible = False
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Dialog"
                Dim txtTempJobNo As TextBox = e.Item.FindControl("txtTempJobNo")
                Dim btnJobNoDialog As Button = e.Item.FindControl("btnJobNoDialog")
                Dim RefID As String = ""
                If txtRefNo.Attributes("REF_ID").ToString <> "" Then
                    RefID = txtRefNo.Attributes("REF_ID")
                End If

                Dim JobID As String = ""
                Dim DT As New DataTable
                DT = GetDataDetail()
                DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
                DT = DT.DefaultView.ToTable
                If DT.Rows.Count > 0 Then
                    For i As Int32 = 0 To DT.Rows.Count - 1
                        JobID = JobID & DT.Rows(i).Item("JOB_ID").ToString & ","
                    Next
                    JobID = JobID.Substring(0, JobID.Length - 1)
                End If
                Dim Script As String = "requireTextboxDialog('JobFinding.aspx?RefID=" & RefID & "&JobID=" & JobID & "',766,580,'" & txtTempJobNo.ClientID & "','" & btnJobNoDialog.ClientID & "');"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
            Case "Select"
                Dim txtTempJobNo As TextBox = e.Item.FindControl("txtTempJobNo")
                Dim SQL As String = ""
                SQL = "SELECT JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),CLOSE_DATE,126) CLOSE_DATE,JOB_HEADER.DOC_TYPE_ID,DOC_TYPE_NAME,'' AS PAYMENT" & vbCrLf
                SQL &= "FROM JOB_HEADER LEFT JOIN MS_DOC_TYPE " & vbCrLf
                SQL &= "ON JOB_HEADER.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
                SQL &= "WHERE JOB_ID IN (" & txtTempJobNo.Text & ")"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim TEMP As New DataTable
                DA.Fill(TEMP)

                Dim DT As New DataTable
                DT = GetDataDetail()
                For i As Int32 = 0 To TEMP.Rows.Count - 1
                    Dim DR As DataRow
                    DR = DT.NewRow
                    DR("JOB_ID") = TEMP.Rows(i).Item("JOB_ID").ToString
                    DR("JOB_NO") = TEMP.Rows(i).Item("JOB_NO").ToString
                    DR("CUSTOMER") = TEMP.Rows(i).Item("CUSTOMER").ToString
                    DR("CLOSE_DATE") = TEMP.Rows(i).Item("CLOSE_DATE").ToString
                    DR("DOC_TYPE_ID") = TEMP.Rows(i).Item("DOC_TYPE_ID").ToString
                    DR("DOC_TYPE_NAME") = TEMP.Rows(i).Item("DOC_TYPE_NAME").ToString
                    DR("PAYMENT") = ""
                    DT.Rows.Add(DR)
                Next
                
                DT.DefaultView.Sort = "JOB_NO ASC"
                DT = DT.DefaultView.ToTable

                rptData.DataSource = DT
                rptData.DataBind()
                If DT.Rows.Count < 10 Then
                    AddLineItem()
                End If
            Case "Delete"
                Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
                If lblJobNo.Text = "" Then
                    Exit Sub
                End If
                Dim DT As New DataTable
                DT = GetDataDetail()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
                AddLineItem()

            Case "Document"
                Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
                Dim lblDocType As Label = e.Item.FindControl("lblDocType")
                If lblJobNo.Text.Trim = "" Then
                    Exit Sub
                End If
                Dim Link As String = ""
                Dim UID As String = Now.ToOADate().ToString.Replace(".", "")
                Link = "window.open('EditDocument.aspx?JobID=" & lblJobNo.Attributes("JOB_ID") & "&DocumentType=" & lblDocType.Attributes("DOC_TYPE_ID") & "','" & UID & "','fullscreen, scrollbars,resizable,center');"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", Link, True)
        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblCloseDate As Label = e.Item.FindControl("lblCloseDate")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        Dim txtPayment As TextBox = e.Item.FindControl("txtPayment")
        Dim td As HtmlTableCell = e.Item.FindControl("td2")
        CL.ImplementJavaMoneyText(txtPayment, textControlLib.Text_Align.Right)
        txtPayment.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
        lblJobNo.Attributes.Add("JOB_ID", e.Item.DataItem("JOB_ID").ToString)
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        If e.Item.DataItem("CLOSE_DATE").ToString <> "" Then
            lblCloseDate.Text = e.Item.DataItem("CLOSE_DATE").ToString
        End If
        lblDocType.Attributes.Add("DOC_TYPE_ID", e.Item.DataItem("DOC_TYPE_ID").ToString)
        lblDocType.Text = e.Item.DataItem("DOC_TYPE_NAME").ToString
        If e.Item.DataItem("PAYMENT").ToString <> "" Then
            txtPayment.Text = FormatNumber(e.Item.DataItem("PAYMENT").ToString, 2)
        End If

        Dim btnAdd As ImageButton = e.Item.FindControl("btnAdd")
        If lblJobNo.Text = "" Then
            btnAdd.ImageUrl = "images/Icon/01.png"
            btnAdd.Visible = True
            td.BgColor = "#FFFFFF"
        Else
            'btnAdd.ImageUrl = "images/Icon/65.png"
            btnAdd.Visible = False
            td.BgColor = "#666666"
        End If

        If btnSave.Visible = False And btnCreate.Visible = False Then
            Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
            btnAdd.Visible = False
            btnDelete.Visible = False
            txtPayment.Enabled = False
        End If
    End Sub

    Function GetDataDetail() As DataTable
        
        Dim DT As New DataTable
        DT.Columns.Add("JOB_ID")
        DT.Columns.Add("JOB_NO")
        DT.Columns.Add("CUSTOMER")
        DT.Columns.Add("CLOSE_DATE")
        DT.Columns.Add("DOC_TYPE_ID")
        DT.Columns.Add("DOC_TYPE_NAME")
        DT.Columns.Add("PAYMENT")

        If rptData.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptData.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                
                Dim lblJobNo As Label = ri.FindControl("lblJobNo")
                Dim lblCustomer As Label = ri.FindControl("lblCustomer")
                Dim lblCloseDate As Label = ri.FindControl("lblCloseDate")
                Dim lblDocType As Label = ri.FindControl("lblDocType")
                Dim txtPayment As TextBox = ri.FindControl("txtPayment")

                If lblJobNo.Text <> "" Then
                    Dim DR As DataRow = DT.NewRow
                    DR("JOB_ID") = lblJobNo.Attributes("JOB_ID")
                    DR("JOB_NO") = lblJobNo.Text
                    DR("CUSTOMER") = lblCustomer.Text
                    DR("CLOSE_DATE") = lblCloseDate.Text
                    DR("DOC_TYPE_ID") = lblDocType.Attributes("DOC_TYPE_ID")
                    DR("DOC_TYPE_NAME") = lblDocType.Text
                    DR("PAYMENT") = txtPayment.Text
                    DT.Rows.Add(DR)
                End If

            Next
        End If
        Return DT

    End Function

    Sub AddLineItem()
        Dim DT As New DataTable
        DT = GetDataDetail()
        DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
        DT = DT.DefaultView.ToTable

        Dim DR As DataRow
        DR = DT.NewRow
        DR("JOB_ID") = ""
        DR("JOB_NO") = ""
        DR("CUSTOMER") = ""
        DR("CLOSE_DATE") = ""
        DR("DOC_TYPE_ID") = ""
        DR("DOC_TYPE_NAME") = ""
        DR("PAYMENT") = ""
        DT.Rows.Add(DR)

        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Sub ClearData()
        rptData.DataSource = ""
        rptData.DataBind()
        AddLineItem()
    End Sub

    Sub BindJob(ByVal RefID As Int32)
        Dim SQL As String = ""
        SQL &= "SELECT DETAIL_ID,REF_DETAIL.JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),CLOSE_DATE,126) CLOSE_DATE,REF_DETAIL.DOC_TYPE_ID,DOC_TYPE_NAME,PAYMENT" & vbCrLf
        SQL &= "FROM REF_DETAIL LEFT JOIN MS_DOC_TYPE ON REF_DETAIL.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & " ORDER BY DETAIL_ID" & vbCrLf

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
        If DT.Rows.Count = 0 Then
            AddLineItem()
        ElseIf DT.Rows.Count < 10 Then
            SQL = "SELECT * FROM REF_HEADER WHERE REF_ID = " & RefID
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows(0).Item("STATUS") <> MasterControlClass.Ref_Status.Approve Then
                AddLineItem()
            End If
        End If
    End Sub

    Protected Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Dim DT As New DataTable
        DT = GetDataDetail()
        DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
        DT = DT.DefaultView.ToTable
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select job(s)');", True)
            Exit Sub
        End If

        SaveData()
        divNewRef.Visible = False
        BindData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SaveData() = False Then
            Exit Sub
        End If
        divNewRef.Visible = False
        BindData()
    End Sub

    Function Validation() As Boolean
        If txtDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select date');", True)
            Return False
        End If
        Dim JobID As String = ""
        Dim DT As New DataTable
        DT = GetDataDetail()
        DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
        DT = DT.DefaultView.ToTable
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select job(s)');", True)
            Return False
        Else
            For i As Int32 = 0 To DT.Rows.Count - 1
                JobID &= DT.Rows(i).Item("JOB_ID").ToString & ","
            Next
            JobID = JobID.Substring(0, JobID.Length - 1)
        End If

        Dim SQL As String = ""
        SQL &= "SELECT JOB_HEADER.JOB_ID,JOB_HEADER.JOB_NO FROM JOB_HEADER LEFT JOIN REF_DETAIL ON JOB_HEADER.JOB_ID = REF_DETAIL.JOB_ID" & vbCrLf
        SQL &= "WHERE JOB_HEADER.STATUS = " & MasterControlClass.Job_Status.Submit & " AND JOB_HEADER.JOB_ID IN (" & JobID & ")" & vbCrLf
        If txtRefNo.Attributes("REF_ID").ToString <> "" Then
            SQL &= "AND REF_ID <> " & txtRefNo.Attributes("REF_ID")
        End If
        Dim DT_ As New DataTable
        Dim DA_ As New SqlDataAdapter(SQL, ConnStr)
        DA_.Fill(DT_)
        If DT_.Rows.Count > 0 Then
            Dim JobNo As String = ""
            For i As Int32 = 0 To DT_.Rows.Count - 1
                JobNo = JobNo + DT_.Rows(i).Item("JOB_NO").ToString & ","
            Next
            JobNo = JobNo.Substring(0, JobNo.Length - 1)
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Job No " & JobNo & " has updated');", True)
            Return False
        End If
        Return True
    End Function

    Function SaveData() As Boolean
        If Validation() = False Then
            Return False
        End If

        Dim SQL As String = ""
        Dim DT_HD As New DataTable
        Dim DT_DT As New DataTable
        Dim DR As DataRow
        Dim cmd As New SqlCommandBuilder

        Dim RefID As String = ""
        RefID = txtRefNo.Attributes("REF_ID").ToString

        '--------- HEADER ----------
        If RefID = "" Then
            SQL = "SELECT * FROM REF_HEADER WHERE 0=1"
        Else
            SQL = "SELECT * FROM REF_HEADER WHERE REF_ID = " & RefID
        End If

        Dim DA_HD As New SqlDataAdapter(SQL, ConnStr)
        DA_HD.Fill(DT_HD)
        If RefID = "" Then
            '------- Add -------
            DR = DT_HD.NewRow
            RefID = GL.FindID("REF_HEADER", "REF_ID")
            DR("REF_ID") = RefID
            DR("REF_NO") = MC.GetReferenceNo(Session("Department_ID"))
            DR("REF_DATE") = CV.StringToDate(txtDate.Text, "yyyy-MM-dd")
            DR("REF_FROM") = Session("Department_ID")
            DR("STATUS") = MasterControlClass.Ref_Status.Submitted
            DR("SUBMIT_BY") = Session("User_ID")
            DR("SUBMIT_DATE") = Now
            DT_HD.Rows.Add(DR)
        Else
            '------- Edit -------
            DR = DT_HD.Rows(0)
            DR("SUBMIT_BY") = Session("User_ID")
            DR("SUBMIT_DATE") = Now
        End If
        cmd = New SqlCommandBuilder(DA_HD)
        DA_HD.Update(DT_HD)

        '--------- DETAIL ---------- 
        SQL = ""
        If RefID <> "" Then
            SQL &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & " WHERE JOB_ID IN (SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
            SQL &= "DELETE FROM REF_DETAIL WHERE REF_ID = " & RefID
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            Dim SqlCmd As SqlCommand
            SqlCmd = New SqlCommand(SQL, conn)
            SqlCmd.ExecuteNonQuery()
            conn.Close()
        End If

        Dim DT As New DataTable
        DT = GetDataDetail()
        DT.DefaultView.RowFilter = "JOB_ID IS NOT NULL AND JOB_ID <> ''"
        DT = DT.DefaultView.ToTable

        SQL = "SELECT * FROM REF_DETAIL WHERE 0=1"
        Dim DA_DT As New SqlDataAdapter(SQL, ConnStr)
        DA_DT.Fill(DT_DT)
        For i As Int32 = 0 To DT.Rows.Count - 1

            SQL &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Submit & " WHERE JOB_ID = " & DT.Rows(i).Item("JOB_ID")
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            Dim SqlCmd As SqlCommand
            SqlCmd = New SqlCommand(SQL, conn)
            SqlCmd.ExecuteNonQuery()
            conn.Close()

            SQL = "SELECT OPEN_BY,CONVERT(VARCHAR(10),OPEN_DATE,126) OPEN_DATE,CLOSE_BY,CONVERT(VARCHAR(10),CLOSE_DATE,126) CLOSE_DATE FROM JOB_HEADER WHERE JOB_ID = " & DT.Rows(i).Item("JOB_ID")
            Dim DT_JOB As New DataTable
            Dim DA_JOB As New SqlDataAdapter(SQL, ConnStr)
            DA_JOB.Fill(DT_JOB)

            DR = DT_DT.NewRow
            DR("DETAIL_ID") = GL.FindID("REF_DETAIL", "DETAIL_ID")
            DR("REF_ID") = RefID
            DR("JOB_ID") = DT.Rows(i).Item("JOB_ID")
            DR("JOB_NO") = DT.Rows(i).Item("JOB_NO")
            DR("CUSTOMER") = DT.Rows(i).Item("CUSTOMER")
            DR("DOC_TYPE_ID") = DT.Rows(i).Item("DOC_TYPE_ID")
            If DT.Rows(i).Item("PAYMENT").ToString <> "" Then
                DR("PAYMENT") = DT.Rows(i).Item("PAYMENT")
            Else
                DR("PAYMENT") = DBNull.Value
            End If
            DR("STATUS") = MasterControlClass.Job_History.Wait
            DR("OPEN_BY") = DT_JOB.Rows(0).Item("OPEN_BY")
            If DT_JOB.Rows(0).Item("OPEN_DATE").ToString <> "" Then
                DR("OPEN_DATE") = CV.StringToDate(DT_JOB.Rows(0).Item("OPEN_DATE"), "yyyy-MM-dd")
            End If
            DR("CLOSE_BY") = DT_JOB.Rows(0).Item("CLOSE_BY")
            If DT_JOB.Rows(0).Item("CLOSE_DATE").ToString <> "" Then
                DR("CLOSE_DATE") = CV.StringToDate(DT_JOB.Rows(0).Item("CLOSE_DATE"), "yyyy-MM-dd")
            End If
            DR("SUBMIT_BY") = Session("User_ID")
            DR("SUBMIT_DATE") = Now
            DT_DT.Rows.Add(DR)
            cmd = New SqlCommandBuilder(DA_DT)
            DA_DT.Update(DT_DT)
        Next
        Return True
    End Function

    Sub Print(ByVal RefID As String)
        Dim SQL As String = ""
        SQL &= "SELECT REF_NO,REF_DATE,JOB_NO,CUSTOMER,OPEN_DATE,DATEDIFF(D,OPEN_DATE,REF_DATE) COUNT_DAY,PAYMENT" & vbCrLf
        SQL &= "FROM REF_HEADER LEFT JOIN REF_DETAIL ON REF_HEADER.REF_ID = REF_DETAIL.REF_ID" & vbCrLf
        SQL &= "WHERE REF_HEADER.REF_ID = " & RefID
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        Dim UID As String = Now.ToOADate().ToString.Replace(".", "")
        Session("Print_" & UID) = DT
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "openPrintWindow('Print/Submit.aspx?Print_ID=" & UID & "',800,500);", True)
    End Sub
    
    Protected Sub btnDialogApp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDialogApp.Click
        If txtDialogPassword.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter password');", True)
            txtDialogPassword.Focus()
            Exit Sub
        End If
        Dim RefID As Int32 = txtDialogPassword.Attributes("REF_ID").ToString

        Dim SQL As String = ""
        SQL = "SELECT User_ID FROM MS_USER WHERE ISNULL(APP_PASSWORD,'') <> '' AND APP_PASSWORD = '" & txtDialogPassword.Text.Trim.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid approve password');", True)
            txtDialogPassword.Focus()
            Exit Sub
        End If

        SQL = ""
        SQL &= "UPDATE REF_HEADER SET STATUS = " & MasterControlClass.Ref_Status.Approve & vbCrLf
        SQL &= ",SUBMIT_APPROVE_BY = " & Session("User_ID") & vbCrLf
        SQL &= ",SUBMIT_APPROVE_UPDATE_BY = " & Session("User_ID") & vbCrLf
        SQL &= ",SUBMIT_APPROVE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & vbCrLf

        SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Submit & vbCrLf
        SQL &= ",SUBMIT_APPROVE_BY = " & Session("User_ID") & vbCrLf
        SQL &= ",SUBMIT_APPROVE_UPDATE_BY = " & Session("User_ID") & vbCrLf
        SQL &= ",SUBMIT_APPROVE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & vbCrLf

        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()

        divConfirmApprove.Visible = False
        BindData()
        Print(RefID)
    End Sub

    Protected Sub btnDialogCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDialogCAncel.Click
        divConfirmApprove.Visible = False
    End Sub

    Protected Sub btnDialogRevert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDialogRevert.Click
        If txtDialogPassword.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter password');", True)
            txtDialogPassword.Focus()
            Exit Sub
        End If
        Dim RefID As Int32 = txtDialogPassword.Attributes("REF_ID").ToString

        Dim SQL As String = ""
        SQL = "SELECT User_ID FROM MS_USER WHERE ISNULL(APP_PASSWORD,'') <> '' AND APP_PASSWORD = '" & txtDialogPassword.Text.Trim.Replace("'", "''") & "'"
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid approve password');", True)
            Exit Sub
        End If

        SQL = ""
        SQL &= "UPDATE REF_HEADER SET STATUS = " & MasterControlClass.Ref_Status.Submitted & vbCrLf
        SQL &= ",SUBMIT_APPROVE_BY = NULL" & vbCrLf
        SQL &= ",SUBMIT_APPROVE_DATE = NULL" & vbCrLf
        SQL &= ",SUBMIT_APPROVE_UPDATE_BY = NULL" & vbCrLf
        SQL &= ",RECEIVE_BY = NULL" & vbCrLf
        SQL &= ",RECEIVE_DATE = NULL" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & vbCrLf

        SQL &= "UPDATE REF_DETAIL SET SUBMIT_BY = NULL" & vbCrLf
        SQL &= ",SUBMIT_DATE = NULL" & vbCrLf
        SQL &= ",SUBMIT_APPROVE_BY = NULL" & vbCrLf
        SQL &= ",SUBMIT_APPROVE_DATE = NULL" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & vbCrLf

        SQL &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Submit & "  WHERE JOB_ID IN " & vbCrLf
        SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf

        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()
        divConfirmApprove.Visible = False
        BindData()
    End Sub

    Protected Sub btnDefault_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDefault.Click
        If divConfirmApprove.Visible = True Then
            If btnDialogApp.Visible = True Then
                btnDialogApp_Click(Nothing, Nothing)
            ElseIf btnDialogRevert.Visible = True Then
                btnDialogRevert_Click(Nothing, Nothing)
            End If
        End If
    End Sub
End Class

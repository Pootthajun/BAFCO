﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_User_Permission
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass

    Protected Sub imgEmployeeDialog_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgUserDialog.Click
        Dim Script As String = "requireTextboxDialog('UserFinding.aspx',875,540,'" & txtTempUserID.ClientID & "','" & btnUserDialog.ClientID & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
    End Sub

    Protected Sub btnEmployeeDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUserDialog.Click
        Dim SQL As String = "SELECT * FROM MS_USER" & vbNewLine
        SQL &= "WHERE MS_USER.USER_ID = " & txtTempUserID.Text
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('User has not found');", True)
            Exit Sub
        End If

        txttxtUserCode.Text = DT.Rows(0).Item("USER_CODE").ToString
        txtFullName.Text = DT.Rows(0).Item("USER_FULLNAME").ToString

        Dim DT_Menu As New DataTable
        DT_Menu = GetMenu()

        SQL = "SELECT * FROM MS_USER_MENU WHERE USER_ID = " & txtTempUserID.Text
        DA = New SqlDataAdapter(sql, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            For i As Int32 = 0 To DT.Rows.Count - 1
                DT_Menu.DefaultView.RowFilter = "MenuID = " & DT.Rows(i).Item("MENU_ID").ToString
                If DT_Menu.DefaultView.Count > 0 Then
                    Dim dt_temp As New DataTable
                    dt_temp = DT_Menu.DefaultView.ToTable
                    DT_Menu.Rows(dt_temp.Rows(0).Item("No") - 1).Item("Visible") = True
                End If
                DT_Menu.DefaultView.RowFilter = ""
            Next
        End If

        rptData.DataSource = DT_Menu
        rptData.DataBind()

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblMenuID As Label = e.Item.FindControl("lblMenuID")
        Dim lblMenu As Label = e.Item.FindControl("lblMenu")
        Dim cbCheck As CheckBox = e.Item.FindControl("cbCheck")

        lblNo.Text = e.Item.DataItem("No").ToString
        lblMenuID.Text = e.Item.DataItem("MenuID").ToString
        lblMenu.Text = e.Item.DataItem("Menu").ToString
        If e.Item.DataItem("Visible").ToString.ToUpper = "TRUE" Then
            cbCheck.Checked = True
        Else
            cbCheck.Checked = False
        End If
    End Sub

    Function GetMenu() As DataTable
        Dim DT As New DataTable
        Dim DR As DataRow
        DT.Columns.Add("No")
        DT.Columns.Add("MenuID")
        DT.Columns.Add("MenuName")
        DT.Columns.Add("Visible", GetType(Boolean))

        Dim SQL As String = "SELECT * FROM MS_MENU ORDER BY SORT"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_ As New DataTable
        DA.Fill(DT_)

        For i As Int32 = 0 To DT_.Rows.Count - 1
            DR = DT.NewRow
            DR("No") = i + 1
            DR("MenuID") = DT_.Rows(i).Item("MENU_ID").ToString
            DR("Menu") = DT_.Rows(i).Item("MENU_NAME").ToString
            DR("Visible") = False
            DT.Rows.Add(DR)
        Next

        Return DT
    End Function

    Function GetDataDetail() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("No")
        DT.Columns.Add("MenuID")
        DT.Columns.Add("Menu")
        DT.Columns.Add("Visible", GetType(Boolean))

        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblNo As Label = ri.FindControl("lblNo")
            Dim lblMenuID As Label = ri.FindControl("lblMenuID")
            Dim lblMenu As Label = ri.FindControl("lblMenu")
            Dim cbCheck As CheckBox = ri.FindControl("cbCheck")

            Dim DR As DataRow = DT.NewRow
            DR("No") = lblNo.Text
            DR("MenuID") = lblMenuID.Text
            DR("Menu") = lblMenu.Text
            DR("Visible") = cbCheck.Checked
            DT.Rows.Add(DR)
        Next

        Return DT

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As New DataTable
        Dim conn As New SqlConnection(ConnStr)
        Dim cmd As New SqlCommand
        Dim sql As String = ""
        DT = GetDataDetail()
        Dim chk As Boolean = False
        conn.Open()
        Try
            For i As Int32 = 0 To DT.Rows.Count - 1
                If DT.Rows(i).Item("Visible").ToString.ToUpper = "TRUE" Then
                    If chk = False Then
                        sql = "DELETE FROM MS_USER_MENU WHERE USER_ID = " & txtTempUserID.Text
                        cmd.CommandType = CommandType.Text
                        cmd.CommandText = sql
                        cmd.Connection = conn
                        cmd.ExecuteNonQuery()
                    End If

                    sql = "INSERT INTO MS_USER_MENU(USER_ID,MENU_ID) VALUES(" & txtTempUserID.Text & "," & DT.Rows(i).Item("MenuID").ToString & ")"
                    cmd = New SqlCommand
                    cmd.CommandType = CommandType.Text
                    cmd.CommandText = sql
                    cmd.Connection = conn
                    cmd.ExecuteNonQuery()
                    chk = True
                End If
            Next
            conn.Close()
            If chk = False Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select menu');", True)
                Exit Sub
            End If

            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success!!');", True)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert(""" & ex.Message & """);", True)
            conn.Close()
        End Try

    End Sub
End Class

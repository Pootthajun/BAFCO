﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewJobReference.aspx.vb" Inherits="ViewJobReference" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="Document.ascx" tagname="Document" tagprefix="uc1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript" src="Script/script.js"></script>    
    <link href="style/css.css" rel="stylesheet" type="text/css" />
    <link href="style/menustyles.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="style/logoffscreen.css"/>
</head>
<body>
    <form id="form1" runat="server">
    <Ajax:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />
    <asp:UpdatePanel ID="udp1" runat="server" >
        <ContentTemplate>
            <center>
            <div style="width:1000px; position:fixed; top:0px;"> 
                <div style="text-align:left;">
                <table width="1000px" height="40" border="0" bgcolor="red" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="Master_Header_Text" align="center" >
                            Ckeck List Form
                        </td>      
                    </tr>
                </table>
                </div>

                <asp:Panel ID="Panel" runat="server" Enabled="false" style="width:1000px; height:440px; position:absolute; top:40px; overflow-y:auto; overflow-x:hidden;">
                    <uc1:Document ID="Document" runat="server" />
                    <table id="tbData1" runat="server" width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
                        <tr>
                            <td height="40" align="center" class="Master_Header_Text">
                                Action
                            </td>                
                        </tr>
                    </table>
                    <div style="height:10px;"></div>
                    <table id="tbData2" runat="server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">
                                <table cellpadding="0" cellspacing="5" style="text-align:left;" width="980px">
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptData" runat="server">
                                                <ItemTemplate>
                                                    <table cellpadding="0" cellspacing="5" style="border:1px solid black; width:100%; padding: 5px 0 5px 0;">
                                                        <tr>
                                                            <td style="font-family: Tahoma; font-size: 13px; font-weight:bold; color:Navy; text-align:left; padding-left:10px;" width="300px">
                                                                <table cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Label ID="lblDesc" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="padding-left:5px; vertical-align:middle;">
                                                                            <asp:Image ID="Img" runat="server" Width="15px" Height="15px"/>
                                                                        </td>
                                                                    </tr>
                                                                </table> 
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" style="padding-left:10px;">
                                                                    <tr>
                                                                        <td style="font-family: Tahoma; font-size: 13px; font-weight:bold; color:Black; width:105px;">
                                                                            Receive Date
                                                                        </td>
                                                                        <td style="font-family: Tahoma; font-size: 13px; color:Black; width:150px;">
                                                                            <asp:Label ID="lblReceiveDate" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="font-family: Tahoma; font-size: 13px; font-weight:bold; color:Black; width:90px;">
                                                                            Close Date
                                                                        </td>
                                                                        <td style="font-family: Tahoma; font-size: 13px; color:Black; width:150px;">
                                                                            <asp:Label ID="lblCloseDate" runat="server"></asp:Label>
                                                                        </td>
                                                                        <td style="font-family: Tahoma; font-size: 13px; font-weight:bold; color:Black; width:80px;">
                                                                            Close By
                                                                        </td>
                                                                        <td style="font-family: Tahoma; font-size: 13px; color:Black;">
                                                                            <asp:Label ID="lblCloseBy" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <table cellpadding="0" cellspacing="0" style="padding-left:10px;">
                                                                    <tr>
                                                                        <td style="font-family: Tahoma; font-size: 13px; font-weight:bold; color:Black; width:105px; vertical-align:top;">
                                                                            Remark
                                                                        </td>
                                                                        <td style="font-family: Tahoma; font-size: 13px; color:Black; width:150px; vertical-align:top;">
                                                                            <asp:Label ID="lblRemark" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div style="height:10px;"></div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            </by>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

                <div id="divComment" runat="server" style="text-align:left; position:fixed; width:1000px; bottom:5px; background-color:White;">
                    <asp:Panel ID="PanelRemark" runat="server" BorderWidth="1" BorderColor="#CCCCCC" style="border-radius:7px;" Width="1000px" Height="105px">
                    <table cellpadding="0" cellspacing="5">
                        <tr>
                            <td align="center" class="NormalTextBlack" valign="top" width="80px">
                                Remark
                            </td>
                            <td width="400px">
                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Height="50px" Width="500px" style="resize:none;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <asp:Button ID="btnConfirmation" runat="server" Text="Confirm" CssClass="Button_Red" Height="30px" style="background-image:url('images/icon/Confirm.png'); background-position:left; background-repeat:no-repeat;  padding:  5px 10px 5px 40px;"/>
                                <Ajax:ConfirmButtonExtender ID="ConfirmButtonTrue" runat="server" Enabled="true" ConfirmText="Are you sure you want to confirm this job?" TargetControlID="btnConfirmation">
                                </Ajax:ConfirmButtonExtender>
                                <asp:Button ID="btnIncomplete" runat="server" Text="Incomplete" CssClass="Button_Red" Height="30px" style="background-image:url('images/icon/Incomplete.png'); background-position:left; background-repeat:no-repeat;  padding:  5px 10px 5px 40px;"/>
                                <asp:Button ID="btnHold" runat="server" Text="Hold" CssClass="Button_Red" Height="30px" style="background-image:url('images/icon/Hold.png'); background-position:left; background-repeat:no-repeat;  padding:  5px 10px 5px 40px;"/>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="Button_Red" Height="30px" style="background-image:url('images/icon/Delete.png'); background-position:left; background-repeat:no-repeat;  padding:  5px 10px 5px 40px;"/>
                                <Ajax:ConfirmButtonExtender ID="ConfirmButtonDelete" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete this job?" TargetControlID="btnDelete">
                                </Ajax:ConfirmButtonExtender>
                                <asp:Button ID="btnFOC" runat="server" Text="FOC" CssClass="Button_Red" Height="30px" style="background-image:url('images/icon/FOC.png'); background-position:left; background-repeat:no-repeat;  padding:  5px 10px 5px 40px;"/>
                                <Ajax:ConfirmButtonExtender ID="ConfirmButtonFOC" runat="server" Enabled="true" ConfirmText="Are you sure you want to complete this job?" TargetControlID="btnFOC">
                                </Ajax:ConfirmButtonExtender>
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                </div>
                    
                
            </div>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>

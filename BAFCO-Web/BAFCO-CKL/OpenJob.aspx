<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="OpenJob.aspx.vb" Inherits="ImportData" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
    <ContentTemplate>
        <table cellspacing="5px;" style="margin: 5px 0 100px 10px;">
            <tr>
                <td width="160px" height="25" class="NormalTextBlack">
                    Upload Excel Document
                </td>
                <td width="350px" height="25"> 
                    <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" BackColor="#CCCCCC" onchange="clearGrid();" /> 
                </td>
               
                <td width="50px" height="25" style="display:none;">
                    <asp:ImageButton ID="btnUpload" runat="server" ImageUrl="images/upload.png" Height="25px" Width="75px" />
                </td>
                <td>
                </td>
            </tr> 
            <tr>
                <td colspan="4"> 
                    <asp:Panel ID="PanelData" runat="server" Visible="false">
                    <div style="padding-bottom:10px;"> 
                        
                    </div>
                    <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="40" class="Grid_Header">
                                <asp:Button ID="btnAll" runat="server" Text="All" CssClass="Button_White"/>
                            </td>
                            <td width="100" class="Grid_Header">
                                Job Reference
                            </td>
                            <td width="40" class="Grid_Header">
                                Status
                            </td>
                            <td width="100" class="Grid_Header">
                                Job Type
                            </td>
                            <td width="300" class="Grid_Header">
                                Customer
                            </td>
                            <td width="100" class="Grid_Header">
                                Job Open Date
                            </td>
                            <td width="100" class="Grid_Header">
                                Department
                            </td>
                            <td width="200" class="Grid_Header">
                                Data Status
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <asp:Repeater ID="rptData" runat="server">
                                    <ItemTemplate>
                                        <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                            <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:CheckBox ID="cb" runat="server" />
                                            </td>
                                            <td id="td2" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; padding-left:5px;">
                                                <asp:Label ID="lblJobRef" runat="server" ></asp:Label>
                                            </td> 
                                            <td id="td3" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                            </td> 
                                            <td id="td4" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblJobTYpe" runat="server" ></asp:Label>
                                            </td>
                                            <td id="td5" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; padding-left:5px;">
                                                <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                            </td>
                                            <td id="td6" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblJobDate" runat="server" ></asp:Label>
                                            </td>
                                            <td id="td7" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblDepartment" runat="server" ></asp:Label>
                                            </td>
                                            <td id="td8" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; padding-left:5px;">
                                                <asp:Label ID="lblDataStatus" runat="server" ></asp:Label>
                                            </td>                                 
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <div style="text-align:right; padding-top:10px;"> 
                        <asp:Button ID="btnOpenJob" runat="server" Text="Open Selected Job(s)" CssClass="Button_Red" Width="150px" Height="30px"/>
                        <Ajax:ConfirmButtonExtender ID="btnOpenJob_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to open selected job(s)?" TargetControlID="btnOpenJob"></Ajax:ConfirmButtonExtender>
                    </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

<script language="javascript">
    function clearGrid() {
        document.getElementById("ctl00_ContentPlaceHolder1_PanelACC").innerHTML = '';
        document.getElementById("ctl00_ContentPlaceHolder1_PanelCCF").innerHTML = '';
        document.getElementById("ctl00_ContentPlaceHolder1_PanelImport").innerHTML = '';
    }
</script>
</asp:Content>


﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewDocument.aspx.vb" Inherits="ViewDocument" %>
<%@ Register src="Document.ascx" tagname="Document" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style/css.css" rel="stylesheet" type="text/css" />
</head>
<body>
<form id="form1" runat="server">
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" />
    <asp:Panel id="Panel" runat="server">
        <table cellpadding="0" cellspacing="0" width="1000px">
            <tr>
                <td style="height:20px;">
                
                </td>
            </tr>
            <tr>
                <td style="padding-left:10px;">
                    <uc1:Document ID="Document" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="height:20px; text-align:right; vertical-align:middle;">
                </td>
            </tr>
        </table>
    </asp:Panel>
</form>
    
</body>
</html>

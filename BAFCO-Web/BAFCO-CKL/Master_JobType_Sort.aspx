﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_JobType_Sort.aspx.vb" Inherits="Master_JobType_Sort" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="40" style="padding-left:10px;" valign="middle">
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            Document Type&nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="Dropdown_Form_White" width="150px" style="text-align:center;" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="900" valign="top" colspan="2">
                <asp:UpdatePanel ID="udp1" runat="server" >
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="1" style="margin:10px  0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                            <tr>
                                <td width="40" class="Grid_Header" align="center">
                                    No                    
                                </td>
                                <td width="300" class="Grid_Header">                    
                                    Name
                                </td>
                                <td width="60" class="Grid_Header">                  
                                    Sort
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr style="border-bottom:solid 1px #efefef; vertical-align:middle;">
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail">
                                            <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:TextBox ID="txtSort" runat="server" MaxLength="2" CssClass="Textbox_Form_White" Width="60px" style="text-align:center;"></asp:TextBox>
                                            <asp:Button ID="btnSort" runat="server" CommandName="Sort" style="display:none;"></asp:Button>
                                        </td>       
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>           
            </td>
        </tr>
    </table>
    <br />
</asp:Content>


﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Approving
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

#Region "Reference"
    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT HD.REF_ID,REF_NO,HD.STATUS" & vbCrLf
        SQL &= ",DPT.DEPARTMENT_NAME REF_FROM_NAME,COUNT(1) AS JOB" & vbCrLf
        SQL &= ",[CHECK],PRICE,APP,BILL,COMPLETE,[END],TOTAL" & vbCrLf
        SQL &= "FROM REF_HEADER HD" & vbCrLf
        SQL &= "LEFT JOIN MS_DEPARTMENT DPT ON HD.REF_FROM = DPT.DEPARTMENT_ID" & vbCrLf
        SQL &= "LEFT JOIN REF_DETAIL DT ON HD.REF_ID = DT.REF_ID" & vbCrLf
        SQL &= "LEFT JOIN" & vbCrLf
        SQL &= "(" & vbCrLf
        SQL &= "SELECT REF_ID" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Checked & " THEN 1 ELSE 0 END) [CHECK]" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Priced & " THEN 1 ELSE 0 END) PRICE" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Approving & " THEN 1 ELSE 0 END) APP" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Billed & " THEN 1 ELSE 0 END) BILL" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.Completed & " THEN 1 ELSE 0 END) COMPLETE" & vbCrLf
        SQL &= ",SUM(CASE WHEN [STATUS] = " & MasterControlClass.Job_Status.EndProcess & " THEN 1 ELSE 0 END) [END]" & vbCrLf
        SQL &= ",COUNT(1) TOTAL FROM REF_DETAIL GROUP BY REF_ID" & vbCrLf
        SQL &= ") JOB ON HD.REF_ID = JOB.REF_ID" & vbCrLf
        SQL &= "WHERE DT.STATUS = " & MasterControlClass.Job_Status.Approving & " AND " & vbCrLf
        SQL &= "DT.STATUS_APP IN (" & MasterControlClass.Job_History.Wait & "," & MasterControlClass.Job_History.Incomplete & "," & MasterControlClass.Job_History.Hold & ")" & " AND "
        If txtRefNoSort.Text <> "" Then
            SQL &= "REF_NO like '%" & txtRefNoSort.Text.Replace("'", "''") & "%' AND "
        End If
        If txtJobNo.Text <> "" Then
            SQL &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtDateFrom.Text.Trim <> "" Then
            If IsDate(txtDateFrom.Text) Then
                SQL &= "CONVERT(VARCHAR(8),PRICE_DATE,112) >= '" & txtDateFrom.Text & "' AND "
            End If
        End If
        If txtDateTo.Text.Trim <> "" Then
            If IsDate(txtDateTo.Text) Then
                SQL &= "CONVERT(VARCHAR(8),PRICE_DATE,112) <= '" & txtDateTo.Text & "' AND "
            End If
        End If

        SQL = SQL.Substring(0, SQL.Length - 4)
        SQL &= vbCrLf & "GROUP BY HD.REF_ID,REF_NO,HD.STATUS,DPT.DEPARTMENT_NAME"
        SQL &= vbCrLf & ",[CHECK],PRICE,APP,BILL,COMPLETE,[END],TOTAL"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If
        Session("Reference") = DT
        Navigation.SesssionSourceName = "Reference"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptDataRef
    End Sub

    Protected Sub rptDataRef_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptDataRef.ItemCommand
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim RefID As Int32 = lblRefNo.Attributes("REF_ID")
        Dim lblRefReceiveDate As Label = e.Item.FindControl("lblRefReceiveDate")

        Select Case e.CommandName
            Case "Edit"
                divJobList.Visible = True
                lblHead.Text = "Check List"
                txtRefNo.Text = lblRefNo.Text
                txtRefNo.Attributes.Add("REF_ID", RefID)
                BindJob(RefID)
            Case "Delete"
                Dim SQL As String = ""
                SQL = "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & " WHERE JOB_ID IN " & vbCrLf
                SQL &= "(SELECT JOB_ID FROM REF_DETAIL WHERE REF_ID = " & RefID & ")" & vbCrLf
                SQL &= "DELETE FROM REF_DETAIL WHERE REF_ID = " & RefID & vbCrLf
                SQL &= "DELETE FROM REF_HEADER WHERE REF_ID = " & RefID & vbCrLf
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
                BindData()
        End Select
    End Sub

    Protected Sub rptDataRef_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptDataRef.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefNo As Label = e.Item.FindControl("lblRefNo")
        Dim lblRefFrom As Label = e.Item.FindControl("lblRefFrom")
        Dim lblCheck As Label = e.Item.FindControl("lblCheck")
        Dim lblPrice As Label = e.Item.FindControl("lblPrice")
        Dim lblApp As Label = e.Item.FindControl("lblApp")
        Dim lblBill As Label = e.Item.FindControl("lblBill")
        Dim lblComplete As Label = e.Item.FindControl("lblComplete")
        Dim lblEnd As Label = e.Item.FindControl("lblEnd")
        Dim lblTotal As Label = e.Item.FindControl("lblTotal")

        lblRefNo.Attributes.Add("REF_ID", e.Item.DataItem("REF_ID"))
        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblRefNo.Text = e.Item.DataItem("REF_NO").ToString
        lblRefFrom.Text = e.Item.DataItem("REF_FROM_NAME").ToString
        lblCheck.Text = e.Item.DataItem("CHECK").ToString
        lblPrice.Text = e.Item.DataItem("PRICE").ToString
        lblApp.Text = e.Item.DataItem("APP").ToString
        lblBill.Text = e.Item.DataItem("BILL").ToString
        lblComplete.Text = e.Item.DataItem("COMPLETE").ToString
        lblEnd.Text = e.Item.DataItem("END").ToString
        lblTotal.Text = e.Item.DataItem("TOTAL").ToString
    End Sub

    Protected Sub SortData_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRefNoSort.TextChanged, txtDateFrom.TextChanged, txtDateTo.TextChanged, txtJobNo.TextChanged
        BindData()
    End Sub
#End Region

    Sub BindJob(ByVal RefID As Int32)
        Dim SQL As String = ""
        SQL &= "SELECT DETAIL_ID,REF_DETAIL.JOB_ID,JOB_NO,CUSTOMER,CONVERT(VARCHAR(10),REF_DETAIL.PRICE_DATE,126) PRICE_DATE," & vbCrLf
        SQL &= "REF_DETAIL.DOC_TYPE_ID,DOC_TYPE_NAME,PAYMENT,ISNULL(STATUS_APP," & MasterControlClass.Job_History.Wait & ") AS STATUS" & vbCrLf
        SQL &= "FROM REF_DETAIL LEFT JOIN REF_HEADER ON REF_DETAIL.REF_ID = REF_HEADER.REF_ID" & vbCrLf
        SQL &= "LEFT JOIN MS_DOC_TYPE ON REF_DETAIL.DOC_TYPE_ID = MS_DOC_TYPE.DOC_TYPE_ID" & vbCrLf
        SQL &= "WHERE REF_DETAIL.REF_ID = " & RefID & vbCrLf
        SQL &= "AND STATUS_APP IS NOT NULL" & vbCrLf
        SQL &= "AND STATUS_APP IN (" & MasterControlClass.Job_History.Wait & "," & MasterControlClass.Job_History.Incomplete & "," & MasterControlClass.Job_History.Hold & ")" & vbCrLf
        SQL &= "ORDER BY DETAIL_ID" & vbCrLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
        If DT.Rows.Count = 0 Then
            Response.Redirect("Approving.aspx")
        End If
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim DT As New DataTable
                DT = GetDataDetail()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
            Case "View"
                Dim RefID As Int32 = txtRefNo.Attributes("REF_ID")
                Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
                Dim JobID As Int32 = lblJobNo.Attributes("JOB_ID")
                Dim btnView As ImageButton = e.Item.FindControl("btnView")
                Dim PostData As Int32 = 0
                If btnView.ToolTip = "Wait" Then
                    PostData = 1
                End If

                Dim Script As String = "requireTextboxFeatured('ViewJobReference.aspx?RefID=" & RefID & "&JobID=" & JobID & "&RefStatus=" & MasterControlClass.Job_Status.Approving & "&UserID=" & Session("User_ID") & "&PostData=" & PostData & "','height=600px,width=1020px,center','" & txtRefresh.ClientID & "','" & btnRefresh.ClientID & "');"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblJobNo As Label = e.Item.FindControl("lblJobNo")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
        Dim lblJobReceiveDate As Label = e.Item.FindControl("lblJobReceiveDate")
        Dim lblDocType As Label = e.Item.FindControl("lblDocType")
        Dim lblPayment As Label = e.Item.FindControl("lblPayment")
        Dim btnView As ImageButton = e.Item.FindControl("btnView")
        Dim td As HtmlTableCell = e.Item.FindControl("td1")

        lblJobNo.Attributes.Add("JOB_ID", e.Item.DataItem("JOB_ID").ToString)
        lblJobNo.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString
        If e.Item.DataItem("PRICE_DATE").ToString <> "" Then
            lblJobReceiveDate.Text = e.Item.DataItem("PRICE_DATE").ToString
        End If
        lblDocType.Attributes.Add("DOC_TYPE_ID", e.Item.DataItem("DOC_TYPE_ID").ToString)
        lblDocType.Text = e.Item.DataItem("DOC_TYPE_NAME").ToString
        If e.Item.DataItem("PAYMENT").ToString <> "" Then
            lblPayment.Text = FormatNumber(e.Item.DataItem("PAYMENT").ToString, 2)
        End If

        Select Case e.Item.DataItem("STATUS")
            Case MasterControlClass.Job_History.Wait
                btnView.ImageUrl = "images/Icon/24.png"
                btnView.ToolTip = "Wait"
            Case MasterControlClass.Job_History.Confirmation
                btnView.ImageUrl = "images/Icon/63.png"
                btnView.ToolTip = "Confirmation"
            Case MasterControlClass.Job_History.Incomplete
                btnView.ImageUrl = "images/Icon/21.png"
                btnView.ToolTip = "Incomplete"
                btnView.Enabled = False
                td.Style.Item("background-color") = "#DDDDDD"
                btnView.Style.Item("cursor") = "default"
            Case MasterControlClass.Job_History.Hold
                btnView.ImageUrl = "images/Icon/03.png"
                btnView.ToolTip = "Hold"
                btnView.Enabled = False
                td.Style.Item("background-color") = "#DDDDDD"
                btnView.Style.Item("cursor") = "default"
            Case MasterControlClass.Job_History.Delete
                btnView.ImageUrl = "images/Icon/57.png"
                btnView.ToolTip = "Delete"
            Case MasterControlClass.Job_History.FOC
                btnView.ImageUrl = "images/Icon/36.png"
                btnView.ToolTip = "FOC"
        End Select

    End Sub

    Function GetDataDetail() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("JOB_ID")
        DT.Columns.Add("JOB_NO")
        DT.Columns.Add("CUSTOMER")
        DT.Columns.Add("PRICE_DATE")
        DT.Columns.Add("DOC_TYPE_ID")
        DT.Columns.Add("DOC_TYPE_NAME")
        DT.Columns.Add("PAYMENT")
        DT.Columns.Add("STATUS")

        If rptData.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptData.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                Dim lblJobNo As Label = ri.FindControl("lblJobNo")
                Dim lblCustomer As Label = ri.FindControl("lblCustomer")
                Dim lblJobDate As Label = ri.FindControl("lblJobDate")
                Dim lblDocType As Label = ri.FindControl("lblDocType")
                Dim lblPayment As Label = ri.FindControl("lblPayment")

                Dim DR As DataRow = DT.NewRow
                DR("JOB_ID") = lblJobNo.Attributes("JOB_ID")
                DR("JOB_NO") = lblJobNo.Text
                DR("CUSTOMER") = lblCustomer.Text
                DR("PRICE_DATE") = lblJobDate.Text
                DR("DOC_TYPE_ID") = lblDocType.Attributes("DOC_TYPE_ID")
                DR("DOC_TYPE_NAME") = lblDocType.Text
                DR("PAYMENT") = lblPayment.Text
                DT.Rows.Add(DR)
            Next
        End If
        Return DT

    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        divJobList.Visible = False
        BindData()
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If txtRefresh.Text.Trim <> "" Then
            BindJob(txtRefresh.Text)
        End If
    End Sub
End Class


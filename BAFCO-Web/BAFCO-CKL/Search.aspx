﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Search" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table cellpadding=0 cellspacing=0 width="100%">
        <tr>
            <td style="padding-left:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">    
                    <tr>
                        <td valign="top">
                             <table align="left" cellpadding="0" cellspacing="1">
                                <tr>
                                    <td>
                                        <div class="Fieldset_Container" >
                                            <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                            <table width="1100px" align="center" cellspacing="5px;" style="margin-left:10px;">
                                                <tr>
                                                    <td height="25" class="NormalTextBlack">
                                                        Reference No
                                                    </td>
                                                    <td height="25">
                                                        <asp:TextBox ID="txtRefNo" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25">&nbsp;</td>
                                                    <td height="25" class="NormalTextBlack">
                                                        Job No
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtJobNo" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25">&nbsp;</td>
                                                    <td height="25" class="NormalTextBlack">
                                                        Status
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="Dropdown_Form_White" AutoPostBack="true" style="text-align:center;" width="150px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td colspan="4" rowspan="2" align="center">
                                                        <asp:Panel ID="pnlReportType" runat="server" style="text-align:left;" BackColor="Navy" ForeColor="White" Width="180px" Height="55px">
                                                            <asp:RadioButtonList ID="rblReportType" runat="server" RepeatDirection="Vertical" RepeatLayout="Table" AutoPostBack="true">
                                                                <asp:ListItem Text="Job Status Report" Value="0" Selected="True" style="cursor:pointer;"></asp:ListItem>
                                                                <asp:ListItem Text="Job Analysis Report" Value="1" style="cursor:pointer;"></asp:ListItem>
                                                            </asp:RadioButtonList> 
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="25" class="NormalTextBlack">
                                                        Department
                                                    </td>
                                                    <td height="25">
                                                        <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="Dropdown_Form_White" AutoPostBack="true" style="text-align:center;" width="100%">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td width="40" height="25">&nbsp;</td>
                                                    <td height="25" class="NormalTextBlack">
                                                        Customer
                                                    </td>
                                                    <td colspan="4" height="25">
                                                        <asp:DropDownList ID="ddlCustomer" runat="server" CssClass="Dropdown_Form_White" AutoPostBack="true" style="text-align:center;" width="100%">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Open <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtOpenDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgOpenDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgOpenDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgOpenDateFrom"
				                                        TargetControlID="txtOpenDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Open <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtOpenDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgOpenDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgOpenDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgOpenDateTo"
				                                        TargetControlID="txtOpenDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Close <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtCloseDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgCloseDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgCloseDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgCloseDateFrom"
				                                        TargetControlID="txtCloseDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Close <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtCloseDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgCloseDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgCloseDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgCloseDateTo"
				                                        TargetControlID="txtCloseDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                </tr> 
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Submit <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtSubmitDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgSubmitDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgSubmitDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgSubmitDateFrom"
				                                        TargetControlID="txtSubmitDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Submit <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtSubmitDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgSubmitDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgSubmitDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgSubmitDateTo"
				                                        TargetControlID="txtSubmitDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Receive <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtReceiveDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgReceiveDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgReceiveDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgReceiveDateFrom"
				                                        TargetControlID="txtReceiveDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Receive <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtReceiveDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgReceiveDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgReceiveDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgReceiveDateTo"
				                                        TargetControlID="txtReceiveDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Check <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtCheckDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgCheckDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgCheckDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgCheckDateFrom"
				                                        TargetControlID="txtCheckDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Check <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtCheckDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgCheckDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgCheckDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgCheckDateTo"
				                                        TargetControlID="txtCheckDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Price <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtPriceDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgPriceDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgPriceDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgPriceDateFrom"
				                                        TargetControlID="txtPriceDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Price <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtPriceDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgPriceDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgPriceDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgPriceDateTo"
				                                        TargetControlID="txtPriceDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Approve <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtApproveDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgApproveDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgApproveDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgApproveDateFrom"
				                                        TargetControlID="txtApproveDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Approve <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtApproveDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgApproveDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgApproveDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgApproveDateTo"
				                                        TargetControlID="txtApproveDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Bill <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtBillDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgBillDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgBillDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgBillDateFrom"
				                                        TargetControlID="txtBillDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Bill <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtBillDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgBillDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgBillDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgBillDateTo"
				                                        TargetControlID="txtBillDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Complete <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtCompleteDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgCompleteDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgCompleteDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgCompleteDateFrom"
				                                        TargetControlID="txtCompleteDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Complete <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtCompleteDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgCompleteDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgCompleteDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgCompleteDateTo"
				                                        TargetControlID="txtCompleteDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td colspan="6">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>       
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:left; padding-left:10px; height:40px; vertical-align:top;">
                            <table cellpadding="0" cellspacing="2" style="padding-bottom:10px; width:200px; text-align:left;">
                                <tr>
                                    <td>
                                        <asp:Button ID="btnAll" runat="server" Text="All" CssClass="Button_Red" Width="100px" Height="30px"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="Button_Red" Width="100px" Height="30px"/>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="Button_Red" Width="100px" Height="30px"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr id="trJobStatus" runat="server" visible="false">
                        <td style="padding-left:10px;">
                            <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                                <tr>
                                    <td width="40" class="Grid_Header">
                                        No
                                    </td>
                                    <td width="120" class="Grid_Header">
                                        Reference No
                                    </td>
                                    <td width="120" class="Grid_Header">
                                        Job No
                                    </td>
                                    <td class="Grid_Header">
                                        Customer
                                    </td>
                                    <td width="100" class="Grid_Header">
                                        Department
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Open Date
                                    </td>
                                    <td width="80" class="Grid_Header">                  
                                        Receive Date
                                    </td>
                                    <td width="120" class="Grid_Header">                  
                                        Status
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7">
                                        <asp:Repeater ID="rptJobStatus" runat="server">
                                            <ItemTemplate>
                                                <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblRefNo" runat="server" ></asp:Label>
                                                    </td> 
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                                    </td> 
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:left; padding:0 10px 0 10px;">
                                                        <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblDepartment" runat="server" ></asp:Label>
                                                    </td> 
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblOpenDate" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblReceiveDate" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                                    </td>                              
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                            <div style="margin-left:10px">
                                <uc1:PageNavigation ID="NavigationJobStatus" runat="server" MaximunPageCount="10" PageSize="15"/>
                            </div>
                            <br />
                        </td>
                    </tr>
                    <tr id="trJobAnalysis" runat="server" visible="false">
                        <td style="padding-left:10px;">
                            <asp:Panel ID="Panel" runat="server" style="width:2500px; position:absolute; top:390px; overflow-y:hiden; overflow-x:auto;">
                                <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                                    <tr>
                                        <td width="40" class="Grid_Header">
                                            No
                                        </td>
                                        <td width="120" class="Grid_Header">
                                            Reference No
                                        </td>
                                        <td width="120" class="Grid_Header">
                                            Job No
                                        </td>
                                        <td class="Grid_Header">
                                            Customer
                                        </td>
                                        <td width="100" class="Grid_Header">
                                            Department
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Open Date
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Close Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Submit Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Receive Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Check Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Price Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Approve Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Bill Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Complete Date
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Incomplete
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Hold
                                        </td>
                                        <td width="40" class="Grid_Header">                  
                                            Days
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            FOC
                                        </td>
                                        <td width="80" class="Grid_Header">                  
                                            Sum Days
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="7">
                                            <asp:Repeater ID="rptJobAnalysis" runat="server">
                                                <ItemTemplate>
                                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblRefNo" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                                        </td> 
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:left; padding:0 10px 0 10px;">
                                                            <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblDepartment" runat="server" ></asp:Label>
                                                        </td> 
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblOpenDate" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblCloseDate" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblOpenClose" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblSubmitDate" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblCloseSubmit" runat="server" ></asp:Label>
                                                        </td>
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblReceiveDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblSubmitReceive" runat="server" ></asp:Label>
                                                        </td>  
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblCheckDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblReceiveCheck" runat="server" ></asp:Label>
                                                        </td>
							                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblPriceDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblCheckPrice" runat="server" ></asp:Label>
                                                        </td>
							                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblAppDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblPriceApp" runat="server" ></asp:Label>
                                                        </td>
							                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblBillDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblAppBill" runat="server" ></asp:Label>
                                                        </td>
							                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblCompleteDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblBillComplete" runat="server" ></asp:Label>
                                                        </td>
							                            <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblInCompleteDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblInComplete" runat="server" ></asp:Label>
                                                        </td><td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblHoldDate" runat="server" ></asp:Label>
                                                        </td>    
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblHold" runat="server" ></asp:Label>
                                                        </td>  
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblFocDate" runat="server" ></asp:Label>
                                                        </td>  
                                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                            <asp:Label ID="lblSum" runat="server" ></asp:Label>
                                                        </td>                     
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                                <div style="margin-left:10px">
                                    <uc1:PageNavigation ID="NavigationJobAnalysis" runat="server" MaximunPageCount="10" PageSize="15"/>
                                </div>
                                <br />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>


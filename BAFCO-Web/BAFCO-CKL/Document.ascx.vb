﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing

Partial Class Document
    Inherits System.Web.UI.UserControl

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass

    Private Property JobID() As Integer
        Get
            If IsNumeric(ViewState("JobID")) Then
                Return ViewState("JobID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("JobID") = value
        End Set
    End Property

    Private Property DocumentType() As Integer
        Get
            If IsNumeric(ViewState("DocumentType")) Then
                Return ViewState("DocumentType")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("DocumentType") = value
        End Set
    End Property

    Private Property CreateNew() As Integer
        Get
            If IsNumeric(ViewState("CreateNew")) Then
                Return ViewState("CreateNew")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("CreateNew") = value
        End Set
    End Property

    Sub BindData()
        lblHeader.Text = MC.Get_DocumentTypeName(DocumentType)
        lblJobID.Text = JobID
        Dim SQL As String = ""
        SQL = "SELECT JOB_NO,CUSTOMER,DOC_TYPE_ID FROM JOB_HEADER WHERE JOB_ID = " & JobID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblCustomer.Text = DT.Rows(0).Item("CUSTOMER").ToString
            lblJobNo.Text = DT.Rows(0).Item("JOB_NO").ToString
        End If

        BindEntryType(JobID)
        BindJobType(JobID)
        BindFee(JobID)
        BindJobDoc_Accounting(JobID)
        BindVehicle(JobID)
        BindRemark(JobID)
    End Sub

#Region "Entry Type"
    Sub BindEntryType(ByVal JobID As Int32)
        Dim SQL As String = ""
        If CreateNew = 1 Then
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.ENTRY_TYPE_ID,ENTRY_TYPE_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_ENTRY_TYPE HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT ENTRY_TYPE_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_ENTRY_TYPE WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.ENTRY_TYPE_ID = DF.ENTRY_TYPE_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,ENTRY_TYPE_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_ENTRY_TYPE WHERE JOB_ID = " & JobID & "  ORDER BY SORT,ENTRY_TYPE_NAME"
        End If

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            trEntryType1.Visible = False
            trEntryType2.Visible = False
            trEntryType3.Visible = False
        End If

        Dim DT_ENTYE As New DataTable
        For x As Int32 = 1 To 4
            DT_ENTYE.Columns.Add("DETAIL_ID_" & x)
            DT_ENTYE.Columns.Add("ENTRY_TYPE_ID_" & x)
            DT_ENTYE.Columns.Add("ENTRY_TYPE_NAME_" & x)
            DT_ENTYE.Columns.Add("CB_" & x)
            DT_ENTYE.Columns.Add("TB1_" & x)
            DT_ENTYE.Columns.Add("TB2_" & x)
            DT_ENTYE.Columns.Add("TB3_" & x)
            DT_ENTYE.Columns.Add("TB4_" & x)
            DT_ENTYE.Columns.Add("TB5_" & x)
            DT_ENTYE.Columns.Add("TB6_" & x)
            DT_ENTYE.Columns.Add("TB7_" & x)
            DT_ENTYE.Columns.Add("TB8_" & x)
            DT_ENTYE.Columns.Add("TB9_" & x)
            DT_ENTYE.Columns.Add("TB10_" & x)
            DT_ENTYE.Columns.Add("LB1_" & x)
            DT_ENTYE.Columns.Add("LB2_" & x)
            DT_ENTYE.Columns.Add("LB3_" & x)
            DT_ENTYE.Columns.Add("LB4_" & x)
            DT_ENTYE.Columns.Add("LB5_" & x)
            DT_ENTYE.Columns.Add("LB6_" & x)
            DT_ENTYE.Columns.Add("LB7_" & x)
            DT_ENTYE.Columns.Add("LB8_" & x)
            DT_ENTYE.Columns.Add("LB9_" & x)
            DT_ENTYE.Columns.Add("LB10_" & x)
            DT_ENTYE.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 4)
            If i * 4 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_ENTYE.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 4).Item("DETAIL_ID")
            DR("ENTRY_TYPE_ID_1") = DT.Rows(i * 4).Item("ENTRY_TYPE_ID")
            DR("ENTRY_TYPE_NAME_1") = DT.Rows(i * 4).Item("ENTRY_TYPE_NAME")
            DR("CB_1") = DT.Rows(i * 4).Item("CB")
            DR("LB1_1") = DT.Rows(i * 4).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 4).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 4).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 4).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 4).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 4).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 4).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 4).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 4).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 4).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 4).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 4).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 4).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 4).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 4).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 4).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 4).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 4).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 4).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 4).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 4).Item("TB10")

            If (i * 4) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 4) + 1).Item("DETAIL_ID")
                DR("ENTRY_TYPE_ID_2") = DT.Rows((i * 4) + 1).Item("ENTRY_TYPE_ID")
                DR("ENTRY_TYPE_NAME_2") = DT.Rows((i * 4) + 1).Item("ENTRY_TYPE_NAME")
                DR("CB_2") = DT.Rows((i * 4) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 4) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 4) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 4) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 4) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 4) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 4) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 4) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 4) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 4) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 4) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 4) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 4) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 4) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 4) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 4) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 4) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 4) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 4) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 4) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 4) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 4) + 1).Item("TB10")
            End If
            If (i * 4) + 2 < DT.Rows.Count Then
                DR("DETAIL_ID_3") = DT.Rows((i * 4) + 2).Item("DETAIL_ID")
                DR("ENTRY_TYPE_ID_3") = DT.Rows((i * 4) + 2).Item("ENTRY_TYPE_ID")
                DR("ENTRY_TYPE_NAME_3") = DT.Rows((i * 4) + 2).Item("ENTRY_TYPE_NAME")
                DR("CB_3") = DT.Rows((i * 4) + 2).Item("CB")
                DR("LB1_3") = DT.Rows((i * 4) + 2).Item("LB1")
                DR("LB2_3") = DT.Rows((i * 4) + 2).Item("LB2")
                DR("LB3_3") = DT.Rows((i * 4) + 2).Item("LB3")
                DR("LB4_3") = DT.Rows((i * 4) + 2).Item("LB4")
                DR("LB5_3") = DT.Rows((i * 4) + 2).Item("LB5")
                DR("LB6_3") = DT.Rows((i * 4) + 2).Item("LB6")
                DR("LB7_3") = DT.Rows((i * 4) + 2).Item("LB7")
                DR("LB8_3") = DT.Rows((i * 4) + 2).Item("LB8")
                DR("LB9_3") = DT.Rows((i * 4) + 2).Item("LB9")
                DR("LB10_3") = DT.Rows((i * 4) + 2).Item("LB10")
                DR("LB11_3") = DT.Rows((i * 4) + 2).Item("LB11")
                DR("TB1_3") = DT.Rows((i * 4) + 2).Item("TB1")
                DR("TB2_3") = DT.Rows((i * 4) + 2).Item("TB2")
                DR("TB3_3") = DT.Rows((i * 4) + 2).Item("TB3")
                DR("TB4_3") = DT.Rows((i * 4) + 2).Item("TB4")
                DR("TB5_3") = DT.Rows((i * 4) + 2).Item("TB5")
                DR("TB6_3") = DT.Rows((i * 4) + 2).Item("TB6")
                DR("TB7_3") = DT.Rows((i * 4) + 2).Item("TB7")
                DR("TB8_3") = DT.Rows((i * 4) + 2).Item("TB8")
                DR("TB9_3") = DT.Rows((i * 4) + 2).Item("TB9")
                DR("TB10_3") = DT.Rows((i * 4) + 2).Item("TB10")
            End If
            If (i * 4) + 3 < DT.Rows.Count Then
                DR("DETAIL_ID_4") = DT.Rows((i * 4) + 3).Item("DETAIL_ID")
                DR("ENTRY_TYPE_ID_4") = DT.Rows((i * 4) + 3).Item("ENTRY_TYPE_ID")
                DR("ENTRY_TYPE_NAME_4") = DT.Rows((i * 4) + 3).Item("ENTRY_TYPE_NAME")
                DR("CB_4") = DT.Rows((i * 4) + 3).Item("CB")
                DR("LB1_4") = DT.Rows((i * 4) + 3).Item("LB1")
                DR("LB2_4") = DT.Rows((i * 4) + 3).Item("LB2")
                DR("LB3_4") = DT.Rows((i * 4) + 3).Item("LB3")
                DR("LB4_4") = DT.Rows((i * 4) + 3).Item("LB4")
                DR("LB5_4") = DT.Rows((i * 4) + 3).Item("LB5")
                DR("LB6_4") = DT.Rows((i * 4) + 3).Item("LB6")
                DR("LB7_4") = DT.Rows((i * 4) + 3).Item("LB7")
                DR("LB8_4") = DT.Rows((i * 4) + 3).Item("LB8")
                DR("LB9_4") = DT.Rows((i * 4) + 3).Item("LB9")
                DR("LB10_4") = DT.Rows((i * 4) + 3).Item("LB10")
                DR("LB11_4") = DT.Rows((i * 4) + 3).Item("LB11")
                DR("TB1_4") = DT.Rows((i * 4) + 3).Item("TB1")
                DR("TB2_4") = DT.Rows((i * 4) + 3).Item("TB2")
                DR("TB3_4") = DT.Rows((i * 4) + 3).Item("TB3")
                DR("TB4_4") = DT.Rows((i * 4) + 3).Item("TB4")
                DR("TB5_4") = DT.Rows((i * 4) + 3).Item("TB5")
                DR("TB6_4") = DT.Rows((i * 4) + 3).Item("TB6")
                DR("TB7_4") = DT.Rows((i * 4) + 3).Item("TB7")
                DR("TB8_4") = DT.Rows((i * 4) + 3).Item("TB8")
                DR("TB9_4") = DT.Rows((i * 4) + 3).Item("TB9")
                DR("TB10_4") = DT.Rows((i * 4) + 3).Item("TB10")

            End If
            DT_ENTYE.Rows.Add(DR)
        Next

        rptEntryType.DataSource = DT_ENTYE
        rptEntryType.DataBind()
    End Sub

    Protected Sub rptEntryType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptEntryType.ItemDataBound
        For x As Int32 = 1 To 4
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            If e.Item.DataItem("ENTRY_TYPE_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataEntryType() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptEntryType.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptEntryType.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 4
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "Job Type"
    Sub BindJobType(ByVal JobID As Int32)
        Dim SQL As String = ""
        If CreateNew = 1 Then
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.JOB_TYPE_ID,JOB_TYPE_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_JOB_TYPE HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT JOB_TYPE_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_JOB_TYPE WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.JOB_TYPE_ID = DF.JOB_TYPE_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,JOB_TYPE_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_JOB_TYPE WHERE JOB_ID = " & JobID & "  ORDER BY SORT,JOB_TYPE_NAME"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            trJobType1.Visible = False
            trJobType2.Visible = False
            trJobType3.Visible = False
        End If

        Dim DT_JOB As New DataTable
        For x As Int32 = 1 To 2
            DT_JOB.Columns.Add("DETAIL_ID_" & x)
            DT_JOB.Columns.Add("JOB_TYPE_ID_" & x)
            DT_JOB.Columns.Add("JOB_TYPE_NAME_" & x)
            DT_JOB.Columns.Add("CB_" & x)
            DT_JOB.Columns.Add("TB1_" & x)
            DT_JOB.Columns.Add("TB2_" & x)
            DT_JOB.Columns.Add("TB3_" & x)
            DT_JOB.Columns.Add("TB4_" & x)
            DT_JOB.Columns.Add("TB5_" & x)
            DT_JOB.Columns.Add("TB6_" & x)
            DT_JOB.Columns.Add("TB7_" & x)
            DT_JOB.Columns.Add("TB8_" & x)
            DT_JOB.Columns.Add("TB9_" & x)
            DT_JOB.Columns.Add("TB10_" & x)
            DT_JOB.Columns.Add("LB1_" & x)
            DT_JOB.Columns.Add("LB2_" & x)
            DT_JOB.Columns.Add("LB3_" & x)
            DT_JOB.Columns.Add("LB4_" & x)
            DT_JOB.Columns.Add("LB5_" & x)
            DT_JOB.Columns.Add("LB6_" & x)
            DT_JOB.Columns.Add("LB7_" & x)
            DT_JOB.Columns.Add("LB8_" & x)
            DT_JOB.Columns.Add("LB9_" & x)
            DT_JOB.Columns.Add("LB10_" & x)
            DT_JOB.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 2)
            If i * 2 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_JOB.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 2).Item("DETAIL_ID")
            DR("JOB_TYPE_ID_1") = DT.Rows(i * 2).Item("JOB_TYPE_ID")
            DR("JOB_TYPE_NAME_1") = DT.Rows(i * 2).Item("JOB_TYPE_NAME")
            DR("CB_1") = DT.Rows(i * 2).Item("CB")
            DR("LB1_1") = DT.Rows(i * 2).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 2).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 2).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 2).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 2).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 2).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 2).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 2).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 2).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 2).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 2).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 2).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 2).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 2).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 2).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 2).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 2).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 2).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 2).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 2).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 2).Item("TB10")

            If (i * 2) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 2) + 1).Item("DETAIL_ID")
                DR("JOB_TYPE_ID_2") = DT.Rows((i * 2) + 1).Item("JOB_TYPE_ID")
                DR("JOB_TYPE_NAME_2") = DT.Rows((i * 2) + 1).Item("JOB_TYPE_NAME")
                DR("CB_2") = DT.Rows((i * 2) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 2) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 2) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 2) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 2) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 2) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 2) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 2) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 2) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 2) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 2) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 2) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 2) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 2) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 2) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 2) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 2) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 2) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 2) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 2) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 2) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 2) + 1).Item("TB10")
            End If
            DT_JOB.Rows.Add(DR)
        Next

        rptJobType.DataSource = DT_JOB
        rptJobType.DataBind()
    End Sub

    Protected Sub rptJobType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptJobType.ItemDataBound
        For x As Int32 = 1 To 2
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            Dim btn As Button = e.Item.FindControl("btnCB" & x)
            'cb.Attributes("onclick") = "document.getElementById('" & btn.ClientID & "').click();"
            If e.Item.DataItem("JOB_TYPE_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataJobType() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptJobType.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptJobType.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 2
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "Entry Fee"
    Sub BindFee(ByVal JobID As Int32)
        Dim SQL As String = ""
        If CreateNew = 1 Then
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.FEE_ID,FEE_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_FEE HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT FEE_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_FEE WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.FEE_ID = DF.FEE_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,FEE_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_FEE WHERE JOB_ID = " & JobID & "  ORDER BY SORT,FEE_NAME"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            trFee1.Visible = False
            trFee2.Visible = False
            trFee3.Visible = False
        End If

        Dim DT_FEE As New DataTable
        For x As Int32 = 1 To 4
            DT_FEE.Columns.Add("DETAIL_ID_" & x)
            DT_FEE.Columns.Add("FEE_ID_" & x)
            DT_FEE.Columns.Add("FEE_NAME_" & x)
            DT_FEE.Columns.Add("CB_" & x)
            DT_FEE.Columns.Add("TB1_" & x)
            DT_FEE.Columns.Add("TB2_" & x)
            DT_FEE.Columns.Add("TB3_" & x)
            DT_FEE.Columns.Add("TB4_" & x)
            DT_FEE.Columns.Add("TB5_" & x)
            DT_FEE.Columns.Add("TB6_" & x)
            DT_FEE.Columns.Add("TB7_" & x)
            DT_FEE.Columns.Add("TB8_" & x)
            DT_FEE.Columns.Add("TB9_" & x)
            DT_FEE.Columns.Add("TB10_" & x)
            DT_FEE.Columns.Add("LB1_" & x)
            DT_FEE.Columns.Add("LB2_" & x)
            DT_FEE.Columns.Add("LB3_" & x)
            DT_FEE.Columns.Add("LB4_" & x)
            DT_FEE.Columns.Add("LB5_" & x)
            DT_FEE.Columns.Add("LB6_" & x)
            DT_FEE.Columns.Add("LB7_" & x)
            DT_FEE.Columns.Add("LB8_" & x)
            DT_FEE.Columns.Add("LB9_" & x)
            DT_FEE.Columns.Add("LB10_" & x)
            DT_FEE.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 4)
            If i * 4 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_FEE.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 4).Item("DETAIL_ID")
            DR("FEE_ID_1") = DT.Rows(i * 4).Item("FEE_ID")
            DR("FEE_NAME_1") = DT.Rows(i * 4).Item("FEE_NAME")
            DR("CB_1") = DT.Rows(i * 4).Item("CB")
            DR("LB1_1") = DT.Rows(i * 4).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 4).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 4).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 4).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 4).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 4).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 4).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 4).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 4).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 4).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 4).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 4).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 4).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 4).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 4).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 4).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 4).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 4).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 4).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 4).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 4).Item("TB10")

            If (i * 4) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 4) + 1).Item("DETAIL_ID")
                DR("FEE_ID_2") = DT.Rows((i * 4) + 1).Item("FEE_ID")
                DR("FEE_NAME_2") = DT.Rows((i * 4) + 1).Item("FEE_NAME")
                DR("CB_2") = DT.Rows((i * 4) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 4) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 4) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 4) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 4) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 4) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 4) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 4) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 4) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 4) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 4) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 4) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 4) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 4) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 4) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 4) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 4) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 4) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 4) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 4) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 4) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 4) + 1).Item("TB10")
            End If
            If (i * 4) + 2 < DT.Rows.Count Then
                DR("DETAIL_ID_3") = DT.Rows((i * 4) + 2).Item("DETAIL_ID")
                DR("FEE_ID_3") = DT.Rows((i * 4) + 2).Item("FEE_ID")
                DR("FEE_NAME_3") = DT.Rows((i * 4) + 2).Item("FEE_NAME")
                DR("CB_3") = DT.Rows((i * 4) + 2).Item("CB")
                DR("LB1_3") = DT.Rows((i * 4) + 2).Item("LB1")
                DR("LB2_3") = DT.Rows((i * 4) + 2).Item("LB2")
                DR("LB3_3") = DT.Rows((i * 4) + 2).Item("LB3")
                DR("LB4_3") = DT.Rows((i * 4) + 2).Item("LB4")
                DR("LB5_3") = DT.Rows((i * 4) + 2).Item("LB5")
                DR("LB6_3") = DT.Rows((i * 4) + 2).Item("LB6")
                DR("LB7_3") = DT.Rows((i * 4) + 2).Item("LB7")
                DR("LB8_3") = DT.Rows((i * 4) + 2).Item("LB8")
                DR("LB9_3") = DT.Rows((i * 4) + 2).Item("LB9")
                DR("LB10_3") = DT.Rows((i * 4) + 2).Item("LB10")
                DR("LB11_3") = DT.Rows((i * 4) + 2).Item("LB11")
                DR("TB1_3") = DT.Rows((i * 4) + 2).Item("TB1")
                DR("TB2_3") = DT.Rows((i * 4) + 2).Item("TB2")
                DR("TB3_3") = DT.Rows((i * 4) + 2).Item("TB3")
                DR("TB4_3") = DT.Rows((i * 4) + 2).Item("TB4")
                DR("TB5_3") = DT.Rows((i * 4) + 2).Item("TB5")
                DR("TB6_3") = DT.Rows((i * 4) + 2).Item("TB6")
                DR("TB7_3") = DT.Rows((i * 4) + 2).Item("TB7")
                DR("TB8_3") = DT.Rows((i * 4) + 2).Item("TB8")
                DR("TB9_3") = DT.Rows((i * 4) + 2).Item("TB9")
                DR("TB10_3") = DT.Rows((i * 4) + 2).Item("TB10")
            End If
            If (i * 4) + 3 < DT.Rows.Count Then
                DR("DETAIL_ID_4") = DT.Rows((i * 4) + 3).Item("DETAIL_ID")
                DR("FEE_ID_4") = DT.Rows((i * 4) + 3).Item("FEE_ID")
                DR("FEE_NAME_4") = DT.Rows((i * 4) + 3).Item("FEE_NAME")
                DR("CB_4") = DT.Rows((i * 4) + 3).Item("CB")
                DR("LB1_4") = DT.Rows((i * 4) + 3).Item("LB1")
                DR("LB2_4") = DT.Rows((i * 4) + 3).Item("LB2")
                DR("LB3_4") = DT.Rows((i * 4) + 3).Item("LB3")
                DR("LB4_4") = DT.Rows((i * 4) + 3).Item("LB4")
                DR("LB5_4") = DT.Rows((i * 4) + 3).Item("LB5")
                DR("LB6_4") = DT.Rows((i * 4) + 3).Item("LB6")
                DR("LB7_4") = DT.Rows((i * 4) + 3).Item("LB7")
                DR("LB8_4") = DT.Rows((i * 4) + 3).Item("LB8")
                DR("LB9_4") = DT.Rows((i * 4) + 3).Item("LB9")
                DR("LB10_4") = DT.Rows((i * 4) + 3).Item("LB10")
                DR("LB11_4") = DT.Rows((i * 4) + 3).Item("LB11")
                DR("TB1_4") = DT.Rows((i * 4) + 3).Item("TB1")
                DR("TB2_4") = DT.Rows((i * 4) + 3).Item("TB2")
                DR("TB3_4") = DT.Rows((i * 4) + 3).Item("TB3")
                DR("TB4_4") = DT.Rows((i * 4) + 3).Item("TB4")
                DR("TB5_4") = DT.Rows((i * 4) + 3).Item("TB5")
                DR("TB6_4") = DT.Rows((i * 4) + 3).Item("TB6")
                DR("TB7_4") = DT.Rows((i * 4) + 3).Item("TB7")
                DR("TB8_4") = DT.Rows((i * 4) + 3).Item("TB8")
                DR("TB9_4") = DT.Rows((i * 4) + 3).Item("TB9")
                DR("TB10_4") = DT.Rows((i * 4) + 3).Item("TB1")

            End If
            DT_FEE.Rows.Add(DR)
        Next

        rptFee.DataSource = DT_FEE
        rptFee.DataBind()
    End Sub

    Protected Sub rptFee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFee.ItemDataBound
        For x As Int32 = 1 To 4
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            If e.Item.DataItem("FEE_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataFee() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptFee.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptFee.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 4
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    If cb Is Nothing Then
                        Exit For
                    End If
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "JobDoc & Accounting"
    Sub BindJobDoc_Accounting(ByVal JobID As Int32)
        Dim SQL As String = ""
        If CreateNew = 1 Then
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.JOB_DOC_ID,JOB_DOC_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_JOB_DOC HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT JOB_DOC_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_JOB_DOC WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.JOB_DOC_ID = DF.JOB_DOC_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,JOB_DOC_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_JOB_DOC WHERE JOB_ID = " & JobID & "  ORDER BY SORT,JOB_DOC_NAME"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_JobDoc As New DataTable
        DA.Fill(DT_JobDoc)
        rptJobDoc.DataSource = DT_JobDoc
        rptJobDoc.DataBind()

        If CreateNew = 1 Then
            SQL = ""
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.ACCOUNTING_ID,ACCOUNTING_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_ACCOUNTING HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT ACCOUNTING_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_ACCOUNTING WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.ACCOUNTING_ID = DF.ACCOUNTING_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,ACCOUNTING_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_ACCOUNTING WHERE JOB_ID = " & JobID & "  ORDER BY SORT,ACCOUNTING_NAME"
        End If
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Acc As New DataTable
        DA.Fill(DT_Acc)
        rptAcc.DataSource = DT_Acc
        rptAcc.DataBind()

        If DT_JobDoc.Rows.Count = 0 And DT_Acc.Rows.Count = 0 Then
            trJob1.Visible = False
            trJob2.Visible = False
        End If
    End Sub

    Protected Sub rptJobDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptJobDoc.ItemDataBound
        Dim cb As CheckBox = e.Item.FindControl("cbC1")
        If e.Item.DataItem("JOB_DOC_NAME").ToString <> "" Then
            cb.Visible = True
            cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
            If e.Item.DataItem("CB").ToString = "True" Then
                cb.Checked = True
            End If
            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Protected Sub rptAcc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAcc.ItemDataBound
        If e.Item.DataItem("ACCOUNTING_NAME").ToString <> "" Then
            Dim lblNo As Label = e.Item.FindControl("lblNo")
            lblNo.Text = e.Item.ItemIndex + 1

            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Function GetDataJobDoc() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptJobDoc.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptJobDoc.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

    Function GetDataAccounting() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptAcc.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptAcc.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    DR("CB") = DBNull.Value
                    Dim txtDETAIL_ID As TextBox = ri.FindControl("txt_C1_1")
                    Dim DetailID As Int32 = txtDETAIL_ID.Attributes("DETAIL_ID")
                    DR("DETAIL_ID") = DetailID
                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

#End Region

#Region "Vehicle"
    Sub BindVehicle(ByVal JobID As Int32)
        Dim SQL As String = ""
        If CreateNew = 1 Then
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.VEHICLE_BAFCO_ID,VEHICLE_BAFCO_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_VEHICLE_BAFCO HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT VEHICLE_BAFCO_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_VEHICLE_BAFCO WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.VEHICLE_BAFCO_ID = DF.VEHICLE_BAFCO_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,VEHICLE_BAFCO_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_BAFCO WHERE JOB_ID = " & JobID & "  ORDER BY SORT,VEHICLE_BAFCO_NAME"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Bafco As New DataTable
        DA.Fill(DT_Bafco)
        rptBafco.DataSource = DT_Bafco
        rptBafco.DataBind()

        If CreateNew = 1 Then
            SQL = ""
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.VEHICLE_EMPLOY_ID,VEHICLE_EMPLOY_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_VEHICLE_EMPLOY HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT VEHICLE_EMPLOY_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_VEHICLE_EMPLOY WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.VEHICLE_EMPLOY_ID = DF.VEHICLE_EMPLOY_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,VEHICLE_EMPLOY_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_EMPLOY WHERE JOB_ID = " & JobID & "  ORDER BY SORT,VEHICLE_EMPLOY_NAME"
        End If
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Employ As New DataTable
        DA.Fill(DT_Employ)
        rptEmploy.DataSource = DT_Employ
        rptEmploy.DataBind()

        If CreateNew = 1 Then
            SQL = ""
            SQL &= "SELECT DETAIL_ID,JOB_ID,HD.NAME_VEHICLE_EMPLOY_ID,NAME_VEHICLE_EMPLOY_NAME," & vbCrLf
            SQL &= "SORT,LB1,LB2,LB3,LB4,LB5,LB6,LB7,LB8,LB9,LB10,LB11," & vbCrLf
            SQL &= "DF.CB,ISNULL(DF.TB1,HD.TB1) TB1,ISNULL(DF.TB2,HD.TB2) TB2" & vbCrLf
            SQL &= ",ISNULL(DF.TB3,HD.TB3) TB3,ISNULL(DF.TB4,HD.TB4) TB4" & vbCrLf
            SQL &= ",ISNULL(DF.TB5,HD.TB5) TB5,ISNULL(DF.TB6,HD.TB6) TB6" & vbCrLf
            SQL &= ",ISNULL(DF.TB7,HD.TB7) TB7,ISNULL(DF.TB8,HD.TB8) TB8" & vbCrLf
            SQL &= ",ISNULL(DF.TB9,HD.TB9) TB9,ISNULL(DF.TB10 ,HD.TB10) TB10" & vbCrLf
            SQL &= "FROM JOB_DETAIL_NAME_VEHICLE_EMPLOY HD LEFT JOIN" & vbCrLf
            SQL &= "(" & vbCrLf
            SQL &= "SELECT NAME_VEHICLE_EMPLOY_ID,CB,TB1,TB2,TB3,TB4,TB5,TB6,TB7,TB8,TB9,TB10 " & vbCrLf
            SQL &= "FROM JOB_DETAIL_NAME_VEHICLE_EMPLOY WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' " & vbCrLf
            SQL &= "OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= ") DF ON HD.NAME_VEHICLE_EMPLOY_ID = DF.NAME_VEHICLE_EMPLOY_ID" & vbCrLf
            SQL &= "WHERE JOB_ID =  " & JobID & " ORDER BY SORT,NAME_VEHICLE_EMPLOY_NAME" & vbCrLf
        Else
            SQL = "SELECT * FROM JOB_DETAIL_NAME_VEHICLE_EMPLOY WHERE JOB_ID = " & JobID & "  ORDER BY SORT,NAME_VEHICLE_EMPLOY_NAME"
        End If
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DT_NameEmploy As New DataTable
        For x As Int32 = 1 To 2
            DT_NameEmploy.Columns.Add("DETAIL_ID_" & x)
            DT_NameEmploy.Columns.Add("NAME_VEHICLE_EMPLOY_ID_" & x)
            DT_NameEmploy.Columns.Add("NAME_VEHICLE_EMPLOY_NAME_" & x)
            DT_NameEmploy.Columns.Add("CB_" & x)
            DT_NameEmploy.Columns.Add("TB1_" & x)
            DT_NameEmploy.Columns.Add("TB2_" & x)
            DT_NameEmploy.Columns.Add("TB3_" & x)
            DT_NameEmploy.Columns.Add("TB4_" & x)
            DT_NameEmploy.Columns.Add("TB5_" & x)
            DT_NameEmploy.Columns.Add("TB6_" & x)
            DT_NameEmploy.Columns.Add("TB7_" & x)
            DT_NameEmploy.Columns.Add("TB8_" & x)
            DT_NameEmploy.Columns.Add("TB9_" & x)
            DT_NameEmploy.Columns.Add("TB10_" & x)
            DT_NameEmploy.Columns.Add("LB1_" & x)
            DT_NameEmploy.Columns.Add("LB2_" & x)
            DT_NameEmploy.Columns.Add("LB3_" & x)
            DT_NameEmploy.Columns.Add("LB4_" & x)
            DT_NameEmploy.Columns.Add("LB5_" & x)
            DT_NameEmploy.Columns.Add("LB6_" & x)
            DT_NameEmploy.Columns.Add("LB7_" & x)
            DT_NameEmploy.Columns.Add("LB8_" & x)
            DT_NameEmploy.Columns.Add("LB9_" & x)
            DT_NameEmploy.Columns.Add("LB10_" & x)
            DT_NameEmploy.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 2)
            If i * 2 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_NameEmploy.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 2).Item("DETAIL_ID")
            DR("NAME_VEHICLE_EMPLOY_ID_1") = DT.Rows(i * 2).Item("NAME_VEHICLE_EMPLOY_ID")
            DR("NAME_VEHICLE_EMPLOY_NAME_1") = DT.Rows(i * 2).Item("NAME_VEHICLE_EMPLOY_NAME")
            DR("CB_1") = DT.Rows(i * 2).Item("CB")
            DR("LB1_1") = DT.Rows(i * 2).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 2).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 2).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 2).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 2).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 2).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 2).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 2).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 2).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 2).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 2).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 2).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 2).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 2).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 2).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 2).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 2).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 2).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 2).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 2).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 2).Item("TB10")

            If (i * 2) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 2) + 1).Item("DETAIL_ID")
                DR("NAME_VEHICLE_EMPLOY_ID_2") = DT.Rows((i * 2) + 1).Item("NAME_VEHICLE_EMPLOY_ID")
                DR("NAME_VEHICLE_EMPLOY_NAME_2") = DT.Rows((i * 2) + 1).Item("NAME_VEHICLE_EMPLOY_NAME")
                DR("CB_2") = DT.Rows((i * 2) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 2) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 2) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 2) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 2) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 2) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 2) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 2) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 2) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 2) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 2) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 2) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 2) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 2) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 2) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 2) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 2) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 2) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 2) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 2) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 2) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 2) + 1).Item("TB10")
            End If
            DT_NameEmploy.Rows.Add(DR)
        Next

        rptNameEmploy.DataSource = DT_NameEmploy
        rptNameEmploy.DataBind()

        If DT_Bafco.Rows.Count = 0 And DT_Employ.Rows.Count = 0 And DT_NameEmploy.Rows.Count = 0 Then
            trVehicle1.Visible = False
            trVehicle2.Visible = False
        End If
    End Sub

    Protected Sub rptBafco_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptBafco.ItemDataBound
        Dim cb As CheckBox = e.Item.FindControl("cbC1")
        If e.Item.DataItem("VEHICLE_BAFCO_NAME").ToString <> "" Then
            cb.Visible = True
            cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
            If e.Item.DataItem("CB").ToString = "True" Then
                cb.Checked = True
            End If
            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Function GetDataBafco() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptBafco.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptBafco.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

    Protected Sub rptEmploy_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptEmploy.ItemDataBound
        Dim cb As CheckBox = e.Item.FindControl("cbC1")
        If e.Item.DataItem("VEHICLE_EMPLOY_NAME").ToString <> "" Then
            cb.Visible = True
            cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
            If e.Item.DataItem("CB").ToString = "True" Then
                cb.Checked = True
            End If
            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Function GetDataEmploy() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptEmploy.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptEmploy.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

    Protected Sub rptNameEmploy_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNameEmploy.ItemDataBound
        For x As Int32 = 1 To 2
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            If e.Item.DataItem("NAME_VEHICLE_EMPLOY_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataNameEmploy() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptNameEmploy.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptNameEmploy.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 2
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    If cb Is Nothing Then
                        Exit For
                    End If
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "Remark"
    Sub BindRemark(ByVal JobID As Int32)
        Dim Sql As String = ""
        If CreateNew = 1 Then
            Sql = "SELECT * FROM JOB_DETAIL_REMARK WHERE JOB_ID = 0 AND DF_DOC_TYPE = " & DocumentType
        Else
            Sql = "SELECT * FROM JOB_DETAIL_REMARK WHERE JOB_ID = " & JobID
        End If
        Dim DA As New SqlDataAdapter(Sql, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            txtRemark1.Text = Trim(DT.Rows(0).Item("REMARK1").ToString & vbCrLf & DT.Rows(0).Item("REMARK2").ToString & vbCrLf & DT.Rows(0).Item("REMARK3").ToString)
        End If
    End Sub

#End Region

    Private Sub SaveData(ByVal Table As String, ByVal Field As String, ByVal Value As Object, ByVal DETAIL_ID As String)
        Dim Sql As String = ""
        Sql = "SELECT * FROM " & Table & " WHERE Detail_ID = " & DETAIL_ID
        Dim DA As New SqlDataAdapter(Sql, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If Value.ToString <> "" Then
                DT.Rows(0).Item(Field) = Value
            Else
                DT.Rows(0).Item(Field) = DBNull.Value
            End If
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
    End Sub

    Private Sub SaveDataRemark(ByVal Table As String, ByVal Field As String, ByVal Value As Object)
        Dim Sql As String = ""
        Sql = "SELECT * FROM " & Table & " WHERE JOB_ID = " & lblJobID.Text
        Dim DA As New SqlDataAdapter(Sql, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If Value.ToString <> "" Then
                DT.Rows(0).Item(Field) = Value
            Else
                DT.Rows(0).Item(Field) = DBNull.Value
            End If
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
    End Sub

    Sub ViewDocument()
        trSave.Visible = True
        btnSave.Visible = True
        btnCloseJob.Visible = True
        btnCancel.Visible = True
        btnPrintView.Visible = True
        btnPrintEdit.Visible = False
    End Sub

    Sub EditDocument()
        trSave.Visible = True
        btnSave.Visible = True
        btnCloseJob.Visible = False
        btnCancel.Visible = False
        btnPrintEdit.Visible = True
        btnPrintView.Visible = False
    End Sub

    Protected Sub btnCloseJob_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseJob.Click
        SaveData()
        Dim SQL As String = ""
        SQL &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & vbCrLf
        SQL &= ",CLOSE_BY = " & Session("User_ID") & " ,CLOSE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE JOB_ID = " & JobID
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()

        Dim Link As String = ""
        Link = "window.location.href='CloseJob.aspx';"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", Link, True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            JobID = Request.QueryString("JobID")
            DocumentType = Request.QueryString("DocumentType")

            Dim SQL As String = ""
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_ACCOUNTING WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_ENTRY_TYPE WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_FEE WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_JOB_DOC WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_JOB_TYPE WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_NAME_VEHICLE_EMPLOY WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_VEHICLE_BAFCO WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_VEHICLE_EMPLOY WHERE JOB_ID = " & JobID & vbCrLf
            SQL &= "AND (CB = 1 OR TB1 <> '' OR TB2 <> '' OR TB3 <> '' OR TB4 <> '' OR TB5 <> '' OR TB6 <> '' OR TB7 <> '' OR TB8 <> '' OR TB9 <> '' OR TB10 <> '')" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
            SQL &= "SELECT DETAIL_ID FROM JOB_DETAIL_REMARK WHERE (REMARK1 <> '' OR REMARK2 <> '' OR REMARK3 <> '' OR REMARK4 <> '') AND JOB_ID = " & JobID & vbCrLf
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count = 0 Then
                CreateNew = 1
            Else
                CreateNew = 0
            End If
            BindData()
        End If

        Dim g As System.Drawing.Graphics = Graphics.FromImage(New Bitmap(1, 1))
        Dim s As SizeF = g.MeasureString(txtRemark1.Text, New Font("Tahoma", 9))
        txtRemark1.Height = s.Height + 10
        If txtRemark1.Height.Value < 220 Then
            txtRemark1.Height = 220
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

    Private Sub SaveData()
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable
        Dim SQL As String = ""

        Dim DT_DATA As New DataTable
        For x As Int32 = 1 To 8
            Select Case x
                Case 1
                    DT_DATA = GetDataEntryType()
                    SQL = "SELECT * FROM JOB_DETAIL_ENTRY_TYPE" & vbCrLf
                Case 2
                    DT_DATA = GetDataJobType()
                    SQL = "SELECT * FROM JOB_DETAIL_JOB_TYPE" & vbCrLf
                Case 3
                    DT_DATA = GetDataFee()
                    SQL = "SELECT * FROM JOB_DETAIL_FEE" & vbCrLf
                Case 4
                    DT_DATA = GetDataJobDoc()
                    SQL = "SELECT * FROM JOB_DETAIL_JOB_DOC" & vbCrLf
                Case 5
                    DT_DATA = GetDataAccounting()
                    SQL = "SELECT * FROM JOB_DETAIL_ACCOUNTING" & vbCrLf
                Case 6
                    DT_DATA = GetDataBafco()
                    SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_BAFCO" & vbCrLf
                Case 7
                    DT_DATA = GetDataEmploy()
                    SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_EMPLOY" & vbCrLf
                Case 8
                    DT_DATA = GetDataNameEmploy()
                    SQL = "SELECT * FROM JOB_DETAIL_NAME_VEHICLE_EMPLOY" & vbCrLf
            End Select

            For i As Int32 = 0 To DT_DATA.Rows.Count - 1
                Dim SQL_Str As String = ""
                SQL_Str = SQL & "WHERE DETAIL_ID = " & DT_DATA.Rows(i).Item("DETAIL_ID")
                DA = New SqlDataAdapter(SQL_Str, ConnStr)
                DT = New DataTable
                DA.Fill(DT)
                Dim DR As DataRow = DT.Rows(0)
                DR("CB") = DT_DATA.Rows(i).Item("CB")
                DR("TB1") = DT_DATA.Rows(i).Item("TB1")
                DR("TB2") = DT_DATA.Rows(i).Item("TB2")
                DR("TB3") = DT_DATA.Rows(i).Item("TB3")
                DR("TB4") = DT_DATA.Rows(i).Item("TB4")
                DR("TB5") = DT_DATA.Rows(i).Item("TB5")
                DR("TB6") = DT_DATA.Rows(i).Item("TB6")
                DR("TB7") = DT_DATA.Rows(i).Item("TB7")
                DR("TB8") = DT_DATA.Rows(i).Item("TB8")
                DR("TB9") = DT_DATA.Rows(i).Item("TB9")
                DR("TB10") = DT_DATA.Rows(i).Item("TB10")
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)
            Next

        Next

        SQL = "SELECT * FROM JOB_DETAIL_REMARK WHERE JOB_ID = " & JobID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If txtRemark1.Text.Trim <> "" Then
                DT.Rows(0).Item("REMARK1") = txtRemark1.Text
            Else
                DT.Rows(0).Item("REMARK1") = DBNull.Value
            End If
            
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim Link As String = ""
        Link = "window.location.href='CloseJob.aspx';"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", Link, True)
    End Sub

    Protected Sub btnPrintView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintView.Click
        SaveData()
        Dim url As String = ""
        url = "DocumentPrint.aspx?JobID=" & JobID & "&DocumentType=" & DocumentType
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "openPrintWindow('" & url & "',1000,700);", True)
    End Sub

    Protected Sub btnSaveDataButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveData.Click
        SaveData()
    End Sub
End Class

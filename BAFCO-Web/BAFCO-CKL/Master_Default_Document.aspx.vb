﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_Default_Document
    Inherits System.Web.UI.Page

    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Private Property JobID() As Integer
        Get
            If IsNumeric(ViewState("JobID")) Then
                Return ViewState("JobID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("JobID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            JobID = 0
            MC.BindDDlDocType(ddlDocType, "", "")
            BindData()
        End If
    End Sub

    Sub BindData()
        lblHeader.Text = MC.Get_DocumentTypeName(ddlDocType.SelectedValue)

        Dim FieldKey(8) As String
        FieldKey(0) = "ENTRY_TYPE"
        FieldKey(1) = "JOB_TYPE"
        FieldKey(2) = "FEE"
        FieldKey(3) = "JOB_DOC"
        FieldKey(4) = "ACCOUNTING"
        FieldKey(5) = "VEHICLE_BAFCO"
        FieldKey(6) = "VEHICLE_EMPLOY"
        FieldKey(7) = "NAME_VEHICLE_EMPLOY"
        FieldKey(8) = "REMARK"

        Dim SQL As String = ""
        For i As Int32 = 0 To FieldKey.Length - 2
            SQL = "SELECT * FROM MS_" & FieldKey(i) & " WHERE " & MC.Get_DocumentCondition(ddlDocType.SelectedValue)
            Dim DA_MASTER As New SqlDataAdapter(SQL, ConnStr)
            Dim DT_MASTER As New DataTable
            DA_MASTER = New SqlDataAdapter(SQL, ConnStr)
            DT_MASTER = New DataTable
            DA_MASTER.Fill(DT_MASTER)
            If DT_MASTER.Rows.Count > 0 Then
                SQL = "DELETE FROM JOB_DETAIL_" & FieldKey(i) & " WHERE DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " AND " & FieldKey(i) & "_ID NOT IN (SELECT " & FieldKey(i) & "_ID FROM MS_" & FieldKey(i) & " WHERE " & MC.Get_DocumentCondition(ddlDocType.SelectedValue) & ")"
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()

                SQL = "SELECT * FROM JOB_DETAIL_" & FieldKey(i) & " WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString
                Dim DT_DETAIL As New DataTable
                Dim DA_DETAIL As New SqlDataAdapter(SQL, ConnStr)
                DA_DETAIL.Fill(DT_DETAIL)
                For x As Int32 = 0 To DT_MASTER.Rows.Count - 1
                    DT_DETAIL.DefaultView.RowFilter = FieldKey(i) & "_ID = " & DT_MASTER.Rows(x).Item(FieldKey(i) & "_ID")
                    Dim Temp As New DataTable
                    Temp = DT_DETAIL.DefaultView.ToTable
                    DT_DETAIL.DefaultView.RowFilter = ""
                    If Temp.Rows.Count > 0 Then
                        If (Temp.Rows(0).Item(FieldKey(i) & "_NAME").ToString <> DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME").ToString) Or (Temp.Rows(0).Item("SORT").ToString <> DT_MASTER.Rows(x).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue)).ToString) Then
                            Dim row As Int32 = 0
                            For y As Int32 = 0 To DT_DETAIL.Rows.Count - 1
                                If DT_DETAIL.Rows(y).Item(FieldKey(i) & "_ID") = Temp.Rows(0).Item(FieldKey(i) & "_ID") Then
                                    row = y
                                End If
                            Next
                            DT_DETAIL.Rows(row).Item(FieldKey(i) & "_NAME") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                            DT_DETAIL.Rows(row).Item("SORT") = DT_MASTER.Rows(x).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue))
                            If Temp.Rows(0).Item("CB").ToString = "True" Then
                                DT_DETAIL.Rows(row).Item("CB") = True
                            Else
                                DT_DETAIL.Rows(row).Item("CB") = False
                            End If
                            DT_DETAIL.Rows(row).Item("LB1") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB2") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB3") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB4") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB5") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB6") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB7") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB8") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB9") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB10") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("LB11") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB1") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB2") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB3") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB4") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB5") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB6") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB7") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB8") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB9") = DBNull.Value
                            DT_DETAIL.Rows(row).Item("TB10") = DBNull.Value
                            If InStr(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"), "[values]") > 0 Then
                                Dim txtValue As String = ""
                                txtValue = GL.ReplaceText(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"))
                                Dim Name() As String
                                Name = Split(txtValue, "[values]")

                                For j As Integer = 0 To Name.Length - 1
                                    If j <> Name.Length - 1 Then
                                        DT_DETAIL.Rows(row).Item("TB" & j + 1) = ""
                                    End If
                                    DT_DETAIL.Rows(row).Item("LB" & j + 1) = Name(j)
                                Next
                            Else
                                DT_DETAIL.Rows(row).Item("LB1") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                            End If
                            Dim cmd As New SqlCommandBuilder(DA_DETAIL)
                            DA_DETAIL.Update(DT_DETAIL)
                        End If
                    Else
                        'ADD ถ้ายังไม่เคยมีข้อมูล
                        Dim DR As DataRow
                        DR = DT_DETAIL.NewRow
                        DR("DETAIL_ID") = GL.FindID("JOB_DETAIL_" & FieldKey(i), "DETAIL_ID")
                        DR("JOB_ID") = JobID
                        DR(FieldKey(i) & "_ID") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_ID")
                        DR(FieldKey(i) & "_NAME") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                        DR("SORT") = DT_MASTER.Rows(x).Item(MC.Get_DocumentSort(ddlDocType.SelectedValue).ToString)
                        DR("DF_DOC_TYPE") = ddlDocType.SelectedValue
                        If InStr(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"), "[values]") > 0 Then
                            Dim txtValue As String = ""
                            txtValue = GL.ReplaceText(DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME"))
                            Dim Name() As String
                            Name = Split(txtValue, "[values]")

                            For j As Integer = 0 To Name.Length - 1
                                If j <> Name.Length - 1 Then
                                    DR("TB" & j + 1) = ""
                                End If
                                DR("LB" & j + 1) = Name(j)
                            Next
                        Else
                            DR("LB1") = DT_MASTER.Rows(x).Item(FieldKey(i) & "_NAME")
                        End If

                        DT_DETAIL.Rows.Add(DR)
                        Dim cmd As New SqlCommandBuilder(DA_DETAIL)
                        DA_DETAIL.Update(DT_DETAIL)
                    End If
                Next

            Else
                SQL &= "DELETE FROM JOB_DETAIL_" & FieldKey(i) & " WHERE JOB_ID = " & JobID & vbCrLf & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim SqlCmd As SqlCommand
                SqlCmd = New SqlCommand(SQL, conn)
                SqlCmd.ExecuteNonQuery()
                conn.Close()
            End If
        Next

        SQL = "SELECT * FROM JOB_DETAIL_REMARK WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Dim DR As DataRow
            DR = DT.NewRow
            DR("DETAIL_ID") = GL.FindID("JOB_DETAIL_REMARK", "DETAIL_ID")
            DR("JOB_ID") = JobID
            DR("REMARK1") = DBNull.Value
            DR("REMARK2") = DBNull.Value
            DR("REMARK3") = DBNull.Value
            DR("REMARK4") = DBNull.Value
            DR("DF_DOC_TYPE") = ddlDocType.SelectedValue
            DT.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If

        BindEntryType(JobID)
        BindJobType(JobID)
        BindFee(JobID)
        BindJobDoc_Accounting(JobID)
        BindVehicle(JobID)
        BindRemark(JobID)
    End Sub

#Region "Entry Type"
    Sub BindEntryType(ByVal JobID As Int32)
        Dim SQL As String = ""
        SQL = "SELECT * FROM JOB_DETAIL_ENTRY_TYPE WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,ENTRY_TYPE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            trEntryType1.Visible = False
            trEntryType2.Visible = False
            trEntryType3.Visible = False
        Else
            trEntryType1.Visible = True
            trEntryType2.Visible = True
            trEntryType3.Visible = True
        End If

        Dim DT_ENTYE As New DataTable
        For x As Int32 = 1 To 4
            DT_ENTYE.Columns.Add("DETAIL_ID_" & x)
            DT_ENTYE.Columns.Add("ENTRY_TYPE_ID_" & x)
            DT_ENTYE.Columns.Add("ENTRY_TYPE_NAME_" & x)
            DT_ENTYE.Columns.Add("CB_" & x)
            DT_ENTYE.Columns.Add("TB1_" & x)
            DT_ENTYE.Columns.Add("TB2_" & x)
            DT_ENTYE.Columns.Add("TB3_" & x)
            DT_ENTYE.Columns.Add("TB4_" & x)
            DT_ENTYE.Columns.Add("TB5_" & x)
            DT_ENTYE.Columns.Add("TB6_" & x)
            DT_ENTYE.Columns.Add("TB7_" & x)
            DT_ENTYE.Columns.Add("TB8_" & x)
            DT_ENTYE.Columns.Add("TB9_" & x)
            DT_ENTYE.Columns.Add("TB10_" & x)
            DT_ENTYE.Columns.Add("LB1_" & x)
            DT_ENTYE.Columns.Add("LB2_" & x)
            DT_ENTYE.Columns.Add("LB3_" & x)
            DT_ENTYE.Columns.Add("LB4_" & x)
            DT_ENTYE.Columns.Add("LB5_" & x)
            DT_ENTYE.Columns.Add("LB6_" & x)
            DT_ENTYE.Columns.Add("LB7_" & x)
            DT_ENTYE.Columns.Add("LB8_" & x)
            DT_ENTYE.Columns.Add("LB9_" & x)
            DT_ENTYE.Columns.Add("LB10_" & x)
            DT_ENTYE.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 4)
            If i * 4 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_ENTYE.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 4).Item("DETAIL_ID")
            DR("ENTRY_TYPE_ID_1") = DT.Rows(i * 4).Item("ENTRY_TYPE_ID")
            DR("ENTRY_TYPE_NAME_1") = DT.Rows(i * 4).Item("ENTRY_TYPE_NAME")
            DR("CB_1") = DT.Rows(i * 4).Item("CB")
            DR("LB1_1") = DT.Rows(i * 4).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 4).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 4).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 4).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 4).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 4).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 4).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 4).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 4).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 4).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 4).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 4).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 4).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 4).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 4).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 4).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 4).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 4).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 4).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 4).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 4).Item("TB10")

            If (i * 4) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 4) + 1).Item("DETAIL_ID")
                DR("ENTRY_TYPE_ID_2") = DT.Rows((i * 4) + 1).Item("ENTRY_TYPE_ID")
                DR("ENTRY_TYPE_NAME_2") = DT.Rows((i * 4) + 1).Item("ENTRY_TYPE_NAME")
                DR("CB_2") = DT.Rows((i * 4) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 4) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 4) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 4) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 4) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 4) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 4) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 4) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 4) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 4) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 4) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 4) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 4) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 4) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 4) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 4) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 4) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 4) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 4) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 4) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 4) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 4) + 1).Item("TB10")
            End If
            If (i * 4) + 2 < DT.Rows.Count Then
                DR("DETAIL_ID_3") = DT.Rows((i * 4) + 2).Item("DETAIL_ID")
                DR("ENTRY_TYPE_ID_3") = DT.Rows((i * 4) + 2).Item("ENTRY_TYPE_ID")
                DR("ENTRY_TYPE_NAME_3") = DT.Rows((i * 4) + 2).Item("ENTRY_TYPE_NAME")
                DR("CB_3") = DT.Rows((i * 4) + 2).Item("CB")
                DR("LB1_3") = DT.Rows((i * 4) + 2).Item("LB1")
                DR("LB2_3") = DT.Rows((i * 4) + 2).Item("LB2")
                DR("LB3_3") = DT.Rows((i * 4) + 2).Item("LB3")
                DR("LB4_3") = DT.Rows((i * 4) + 2).Item("LB4")
                DR("LB5_3") = DT.Rows((i * 4) + 2).Item("LB5")
                DR("LB6_3") = DT.Rows((i * 4) + 2).Item("LB6")
                DR("LB7_3") = DT.Rows((i * 4) + 2).Item("LB7")
                DR("LB8_3") = DT.Rows((i * 4) + 2).Item("LB8")
                DR("LB9_3") = DT.Rows((i * 4) + 2).Item("LB9")
                DR("LB10_3") = DT.Rows((i * 4) + 2).Item("LB10")
                DR("LB11_3") = DT.Rows((i * 4) + 2).Item("LB11")
                DR("TB1_3") = DT.Rows((i * 4) + 2).Item("TB1")
                DR("TB2_3") = DT.Rows((i * 4) + 2).Item("TB2")
                DR("TB3_3") = DT.Rows((i * 4) + 2).Item("TB3")
                DR("TB4_3") = DT.Rows((i * 4) + 2).Item("TB4")
                DR("TB5_3") = DT.Rows((i * 4) + 2).Item("TB5")
                DR("TB6_3") = DT.Rows((i * 4) + 2).Item("TB6")
                DR("TB7_3") = DT.Rows((i * 4) + 2).Item("TB7")
                DR("TB8_3") = DT.Rows((i * 4) + 2).Item("TB8")
                DR("TB9_3") = DT.Rows((i * 4) + 2).Item("TB9")
                DR("TB10_3") = DT.Rows((i * 4) + 2).Item("TB10")
            End If
            If (i * 4) + 3 < DT.Rows.Count Then
                DR("DETAIL_ID_4") = DT.Rows((i * 4) + 3).Item("DETAIL_ID")
                DR("ENTRY_TYPE_ID_4") = DT.Rows((i * 4) + 3).Item("ENTRY_TYPE_ID")
                DR("ENTRY_TYPE_NAME_4") = DT.Rows((i * 4) + 3).Item("ENTRY_TYPE_NAME")
                DR("CB_4") = DT.Rows((i * 4) + 3).Item("CB")
                DR("LB1_4") = DT.Rows((i * 4) + 3).Item("LB1")
                DR("LB2_4") = DT.Rows((i * 4) + 3).Item("LB2")
                DR("LB3_4") = DT.Rows((i * 4) + 3).Item("LB3")
                DR("LB4_4") = DT.Rows((i * 4) + 3).Item("LB4")
                DR("LB5_4") = DT.Rows((i * 4) + 3).Item("LB5")
                DR("LB6_4") = DT.Rows((i * 4) + 3).Item("LB6")
                DR("LB7_4") = DT.Rows((i * 4) + 3).Item("LB7")
                DR("LB8_4") = DT.Rows((i * 4) + 3).Item("LB8")
                DR("LB9_4") = DT.Rows((i * 4) + 3).Item("LB9")
                DR("LB10_4") = DT.Rows((i * 4) + 3).Item("LB10")
                DR("LB11_4") = DT.Rows((i * 4) + 3).Item("LB11")
                DR("TB1_4") = DT.Rows((i * 4) + 3).Item("TB1")
                DR("TB2_4") = DT.Rows((i * 4) + 3).Item("TB2")
                DR("TB3_4") = DT.Rows((i * 4) + 3).Item("TB3")
                DR("TB4_4") = DT.Rows((i * 4) + 3).Item("TB4")
                DR("TB5_4") = DT.Rows((i * 4) + 3).Item("TB5")
                DR("TB6_4") = DT.Rows((i * 4) + 3).Item("TB6")
                DR("TB7_4") = DT.Rows((i * 4) + 3).Item("TB7")
                DR("TB8_4") = DT.Rows((i * 4) + 3).Item("TB8")
                DR("TB9_4") = DT.Rows((i * 4) + 3).Item("TB9")
                DR("TB10_4") = DT.Rows((i * 4) + 3).Item("TB10")

            End If
            DT_ENTYE.Rows.Add(DR)
        Next

        rptEntryType.DataSource = DT_ENTYE
        rptEntryType.DataBind()
    End Sub

    Protected Sub rptEntryType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptEntryType.ItemDataBound
        For x As Int32 = 1 To 4
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            If e.Item.DataItem("ENTRY_TYPE_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                Else
                    cb.Checked = False
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataEntryType() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptEntryType.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptEntryType.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 4
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "Job Type"
    Sub BindJobType(ByVal JobID As Int32)
        Dim SQL As String = ""
        SQL = "SELECT * FROM JOB_DETAIL_JOB_TYPE WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,JOB_TYPE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            trJobType1.Visible = False
            trJobType2.Visible = False
            trJobType3.Visible = False
        Else
            trJobType1.Visible = True
            trJobType2.Visible = True
            trJobType3.Visible = True
        End If

        Dim DT_JOB As New DataTable
        For x As Int32 = 1 To 2
            DT_JOB.Columns.Add("DETAIL_ID_" & x)
            DT_JOB.Columns.Add("JOB_TYPE_ID_" & x)
            DT_JOB.Columns.Add("JOB_TYPE_NAME_" & x)
            DT_JOB.Columns.Add("CB_" & x)
            DT_JOB.Columns.Add("TB1_" & x)
            DT_JOB.Columns.Add("TB2_" & x)
            DT_JOB.Columns.Add("TB3_" & x)
            DT_JOB.Columns.Add("TB4_" & x)
            DT_JOB.Columns.Add("TB5_" & x)
            DT_JOB.Columns.Add("TB6_" & x)
            DT_JOB.Columns.Add("TB7_" & x)
            DT_JOB.Columns.Add("TB8_" & x)
            DT_JOB.Columns.Add("TB9_" & x)
            DT_JOB.Columns.Add("TB10_" & x)
            DT_JOB.Columns.Add("LB1_" & x)
            DT_JOB.Columns.Add("LB2_" & x)
            DT_JOB.Columns.Add("LB3_" & x)
            DT_JOB.Columns.Add("LB4_" & x)
            DT_JOB.Columns.Add("LB5_" & x)
            DT_JOB.Columns.Add("LB6_" & x)
            DT_JOB.Columns.Add("LB7_" & x)
            DT_JOB.Columns.Add("LB8_" & x)
            DT_JOB.Columns.Add("LB9_" & x)
            DT_JOB.Columns.Add("LB10_" & x)
            DT_JOB.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 2)
            If i * 2 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_JOB.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 2).Item("DETAIL_ID")
            DR("JOB_TYPE_ID_1") = DT.Rows(i * 2).Item("JOB_TYPE_ID")
            DR("JOB_TYPE_NAME_1") = DT.Rows(i * 2).Item("JOB_TYPE_NAME")
            DR("CB_1") = DT.Rows(i * 2).Item("CB")
            DR("LB1_1") = DT.Rows(i * 2).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 2).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 2).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 2).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 2).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 2).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 2).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 2).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 2).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 2).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 2).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 2).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 2).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 2).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 2).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 2).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 2).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 2).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 2).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 2).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 2).Item("TB10")

            If (i * 2) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 2) + 1).Item("DETAIL_ID")
                DR("JOB_TYPE_ID_2") = DT.Rows((i * 2) + 1).Item("JOB_TYPE_ID")
                DR("JOB_TYPE_NAME_2") = DT.Rows((i * 2) + 1).Item("JOB_TYPE_NAME")
                DR("CB_2") = DT.Rows((i * 2) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 2) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 2) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 2) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 2) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 2) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 2) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 2) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 2) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 2) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 2) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 2) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 2) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 2) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 2) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 2) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 2) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 2) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 2) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 2) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 2) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 2) + 1).Item("TB10")
            End If
            DT_JOB.Rows.Add(DR)
        Next

        rptJobType.DataSource = DT_JOB
        rptJobType.DataBind()
    End Sub

    Protected Sub rptJobType_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptJobType.ItemDataBound
        For x As Int32 = 1 To 2
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            Dim btn As Button = e.Item.FindControl("btnCB" & x)
            If e.Item.DataItem("JOB_TYPE_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                Else
                    cb.Checked = False
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataJobType() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptJobType.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptJobType.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 2
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "Entry Fee"
    Sub BindFee(ByVal JobID As Int32)
        Dim SQL As String = ""
        SQL = "SELECT * FROM JOB_DETAIL_FEE WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,FEE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            trFee1.Visible = False
            trFee2.Visible = False
            trFee3.Visible = False
        Else
            trFee1.Visible = True
            trFee2.Visible = True
            trFee3.Visible = True
        End If

        Dim DT_FEE As New DataTable
        For x As Int32 = 1 To 4
            DT_FEE.Columns.Add("DETAIL_ID_" & x)
            DT_FEE.Columns.Add("FEE_ID_" & x)
            DT_FEE.Columns.Add("FEE_NAME_" & x)
            DT_FEE.Columns.Add("CB_" & x)
            DT_FEE.Columns.Add("TB1_" & x)
            DT_FEE.Columns.Add("TB2_" & x)
            DT_FEE.Columns.Add("TB3_" & x)
            DT_FEE.Columns.Add("TB4_" & x)
            DT_FEE.Columns.Add("TB5_" & x)
            DT_FEE.Columns.Add("TB6_" & x)
            DT_FEE.Columns.Add("TB7_" & x)
            DT_FEE.Columns.Add("TB8_" & x)
            DT_FEE.Columns.Add("TB9_" & x)
            DT_FEE.Columns.Add("TB10_" & x)
            DT_FEE.Columns.Add("LB1_" & x)
            DT_FEE.Columns.Add("LB2_" & x)
            DT_FEE.Columns.Add("LB3_" & x)
            DT_FEE.Columns.Add("LB4_" & x)
            DT_FEE.Columns.Add("LB5_" & x)
            DT_FEE.Columns.Add("LB6_" & x)
            DT_FEE.Columns.Add("LB7_" & x)
            DT_FEE.Columns.Add("LB8_" & x)
            DT_FEE.Columns.Add("LB9_" & x)
            DT_FEE.Columns.Add("LB10_" & x)
            DT_FEE.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 4)
            If i * 4 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_FEE.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 4).Item("DETAIL_ID")
            DR("FEE_ID_1") = DT.Rows(i * 4).Item("FEE_ID")
            DR("FEE_NAME_1") = DT.Rows(i * 4).Item("FEE_NAME")
            DR("CB_1") = DT.Rows(i * 4).Item("CB")
            DR("LB1_1") = DT.Rows(i * 4).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 4).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 4).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 4).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 4).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 4).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 4).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 4).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 4).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 4).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 4).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 4).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 4).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 4).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 4).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 4).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 4).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 4).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 4).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 4).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 4).Item("TB10")

            If (i * 4) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 4) + 1).Item("DETAIL_ID")
                DR("FEE_ID_2") = DT.Rows((i * 4) + 1).Item("FEE_ID")
                DR("FEE_NAME_2") = DT.Rows((i * 4) + 1).Item("FEE_NAME")
                DR("CB_2") = DT.Rows((i * 4) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 4) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 4) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 4) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 4) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 4) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 4) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 4) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 4) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 4) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 4) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 4) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 4) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 4) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 4) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 4) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 4) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 4) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 4) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 4) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 4) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 4) + 1).Item("TB10")
            End If
            If (i * 4) + 2 < DT.Rows.Count Then
                DR("DETAIL_ID_3") = DT.Rows((i * 4) + 2).Item("DETAIL_ID")
                DR("FEE_ID_3") = DT.Rows((i * 4) + 2).Item("FEE_ID")
                DR("FEE_NAME_3") = DT.Rows((i * 4) + 2).Item("FEE_NAME")
                DR("CB_3") = DT.Rows((i * 4) + 2).Item("CB")
                DR("LB1_3") = DT.Rows((i * 4) + 2).Item("LB1")
                DR("LB2_3") = DT.Rows((i * 4) + 2).Item("LB2")
                DR("LB3_3") = DT.Rows((i * 4) + 2).Item("LB3")
                DR("LB4_3") = DT.Rows((i * 4) + 2).Item("LB4")
                DR("LB5_3") = DT.Rows((i * 4) + 2).Item("LB5")
                DR("LB6_3") = DT.Rows((i * 4) + 2).Item("LB6")
                DR("LB7_3") = DT.Rows((i * 4) + 2).Item("LB7")
                DR("LB8_3") = DT.Rows((i * 4) + 2).Item("LB8")
                DR("LB9_3") = DT.Rows((i * 4) + 2).Item("LB9")
                DR("LB10_3") = DT.Rows((i * 4) + 2).Item("LB10")
                DR("LB11_3") = DT.Rows((i * 4) + 2).Item("LB11")
                DR("TB1_3") = DT.Rows((i * 4) + 2).Item("TB1")
                DR("TB2_3") = DT.Rows((i * 4) + 2).Item("TB2")
                DR("TB3_3") = DT.Rows((i * 4) + 2).Item("TB3")
                DR("TB4_3") = DT.Rows((i * 4) + 2).Item("TB4")
                DR("TB5_3") = DT.Rows((i * 4) + 2).Item("TB5")
                DR("TB6_3") = DT.Rows((i * 4) + 2).Item("TB6")
                DR("TB7_3") = DT.Rows((i * 4) + 2).Item("TB7")
                DR("TB8_3") = DT.Rows((i * 4) + 2).Item("TB8")
                DR("TB9_3") = DT.Rows((i * 4) + 2).Item("TB9")
                DR("TB10_3") = DT.Rows((i * 4) + 2).Item("TB10")
            End If
            If (i * 4) + 3 < DT.Rows.Count Then
                DR("DETAIL_ID_4") = DT.Rows((i * 4) + 3).Item("DETAIL_ID")
                DR("FEE_ID_4") = DT.Rows((i * 4) + 3).Item("FEE_ID")
                DR("FEE_NAME_4") = DT.Rows((i * 4) + 3).Item("FEE_NAME")
                DR("CB_4") = DT.Rows((i * 4) + 3).Item("CB")
                DR("LB1_4") = DT.Rows((i * 4) + 3).Item("LB1")
                DR("LB2_4") = DT.Rows((i * 4) + 3).Item("LB2")
                DR("LB3_4") = DT.Rows((i * 4) + 3).Item("LB3")
                DR("LB4_4") = DT.Rows((i * 4) + 3).Item("LB4")
                DR("LB5_4") = DT.Rows((i * 4) + 3).Item("LB5")
                DR("LB6_4") = DT.Rows((i * 4) + 3).Item("LB6")
                DR("LB7_4") = DT.Rows((i * 4) + 3).Item("LB7")
                DR("LB8_4") = DT.Rows((i * 4) + 3).Item("LB8")
                DR("LB9_4") = DT.Rows((i * 4) + 3).Item("LB9")
                DR("LB10_4") = DT.Rows((i * 4) + 3).Item("LB10")
                DR("LB11_4") = DT.Rows((i * 4) + 3).Item("LB11")
                DR("TB1_4") = DT.Rows((i * 4) + 3).Item("TB1")
                DR("TB2_4") = DT.Rows((i * 4) + 3).Item("TB2")
                DR("TB3_4") = DT.Rows((i * 4) + 3).Item("TB3")
                DR("TB4_4") = DT.Rows((i * 4) + 3).Item("TB4")
                DR("TB5_4") = DT.Rows((i * 4) + 3).Item("TB5")
                DR("TB6_4") = DT.Rows((i * 4) + 3).Item("TB6")
                DR("TB7_4") = DT.Rows((i * 4) + 3).Item("TB7")
                DR("TB8_4") = DT.Rows((i * 4) + 3).Item("TB8")
                DR("TB9_4") = DT.Rows((i * 4) + 3).Item("TB9")
                DR("TB10_4") = DT.Rows((i * 4) + 3).Item("TB1")

            End If
            DT_FEE.Rows.Add(DR)
        Next

        rptFee.DataSource = DT_FEE
        rptFee.DataBind()
    End Sub

    Protected Sub rptFee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFee.ItemDataBound
        For x As Int32 = 1 To 4
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            If e.Item.DataItem("FEE_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                Else
                    cb.Checked = False
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataFee() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptFee.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptFee.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 4
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    If cb Is Nothing Then
                        Exit For
                    End If
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "JobDoc & Accounting"
    Sub BindJobDoc_Accounting(ByVal JobID As Int32)
        Dim SQL As String = ""
        SQL = "SELECT * FROM JOB_DETAIL_JOB_DOC WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,JOB_DOC_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_JobDoc As New DataTable
        DA.Fill(DT_JobDoc)
        rptJobDoc.DataSource = DT_JobDoc
        rptJobDoc.DataBind()

        SQL = "SELECT * FROM JOB_DETAIL_ACCOUNTING WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,ACCOUNTING_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Acc As New DataTable
        DA.Fill(DT_Acc)
        rptAcc.DataSource = DT_Acc
        rptAcc.DataBind()

        If DT_JobDoc.Rows.Count = 0 And DT_Acc.Rows.Count = 0 Then
            trJob1.Visible = False
            trJob2.Visible = False
        Else
            trJob1.Visible = True
            trJob2.Visible = True
        End If
    End Sub

    Protected Sub rptJobDoc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptJobDoc.ItemDataBound
        Dim cb As CheckBox = e.Item.FindControl("cbC1")
        If e.Item.DataItem("JOB_DOC_NAME").ToString <> "" Then
            cb.Visible = True
            cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
            If e.Item.DataItem("CB").ToString = "True" Then
                cb.Checked = True
            Else
                cb.Checked = False
            End If
            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Protected Sub rptAcc_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptAcc.ItemDataBound
        If e.Item.DataItem("ACCOUNTING_NAME").ToString <> "" Then
            Dim lblNo As Label = e.Item.FindControl("lblNo")
            lblNo.Text = e.Item.ItemIndex + 1

            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Function GetDataJobDoc() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptJobDoc.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptJobDoc.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

    Function GetDataAccounting() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptAcc.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptAcc.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    DR("CB") = DBNull.Value
                    Dim txtDETAIL_ID As TextBox = ri.FindControl("txt_C1_1")
                    Dim DetailID As Int32 = txtDETAIL_ID.Attributes("DETAIL_ID")
                    DR("DETAIL_ID") = DetailID
                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

#End Region

#Region "Vehicle"
    Sub BindVehicle(ByVal JobID As Int32)
        Dim SQL As String = ""
        SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_BAFCO WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,VEHICLE_BAFCO_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Bafco As New DataTable
        DA.Fill(DT_Bafco)
        rptBafco.DataSource = DT_Bafco
        rptBafco.DataBind()

        SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_EMPLOY WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,VEHICLE_EMPLOY_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT_Employ As New DataTable
        DA.Fill(DT_Employ)
        rptEmploy.DataSource = DT_Employ
        rptEmploy.DataBind()

        SQL = "SELECT * FROM JOB_DETAIL_NAME_VEHICLE_EMPLOY WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString & " ORDER BY SORT,NAME_VEHICLE_EMPLOY_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DT_NameEmploy As New DataTable
        For x As Int32 = 1 To 2
            DT_NameEmploy.Columns.Add("DETAIL_ID_" & x)
            DT_NameEmploy.Columns.Add("NAME_VEHICLE_EMPLOY_ID_" & x)
            DT_NameEmploy.Columns.Add("NAME_VEHICLE_EMPLOY_NAME_" & x)
            DT_NameEmploy.Columns.Add("CB_" & x)
            DT_NameEmploy.Columns.Add("TB1_" & x)
            DT_NameEmploy.Columns.Add("TB2_" & x)
            DT_NameEmploy.Columns.Add("TB3_" & x)
            DT_NameEmploy.Columns.Add("TB4_" & x)
            DT_NameEmploy.Columns.Add("TB5_" & x)
            DT_NameEmploy.Columns.Add("TB6_" & x)
            DT_NameEmploy.Columns.Add("TB7_" & x)
            DT_NameEmploy.Columns.Add("TB8_" & x)
            DT_NameEmploy.Columns.Add("TB9_" & x)
            DT_NameEmploy.Columns.Add("TB10_" & x)
            DT_NameEmploy.Columns.Add("LB1_" & x)
            DT_NameEmploy.Columns.Add("LB2_" & x)
            DT_NameEmploy.Columns.Add("LB3_" & x)
            DT_NameEmploy.Columns.Add("LB4_" & x)
            DT_NameEmploy.Columns.Add("LB5_" & x)
            DT_NameEmploy.Columns.Add("LB6_" & x)
            DT_NameEmploy.Columns.Add("LB7_" & x)
            DT_NameEmploy.Columns.Add("LB8_" & x)
            DT_NameEmploy.Columns.Add("LB9_" & x)
            DT_NameEmploy.Columns.Add("LB10_" & x)
            DT_NameEmploy.Columns.Add("LB11_" & x)
        Next


        For i As Int32 = 0 To (DT.Rows.Count / 2)
            If i * 2 >= DT.Rows.Count Then
                Exit For
            End If
            Dim Col As Int32 = 0
            Dim DR As DataRow
            DR = DT_NameEmploy.NewRow
            DR("DETAIL_ID_1") = DT.Rows(i * 2).Item("DETAIL_ID")
            DR("NAME_VEHICLE_EMPLOY_ID_1") = DT.Rows(i * 2).Item("NAME_VEHICLE_EMPLOY_ID")
            DR("NAME_VEHICLE_EMPLOY_NAME_1") = DT.Rows(i * 2).Item("NAME_VEHICLE_EMPLOY_NAME")
            DR("CB_1") = DT.Rows(i * 2).Item("CB")
            DR("LB1_1") = DT.Rows(i * 2).Item("LB1")
            DR("LB2_1") = DT.Rows(i * 2).Item("LB2")
            DR("LB3_1") = DT.Rows(i * 2).Item("LB3")
            DR("LB4_1") = DT.Rows(i * 2).Item("LB4")
            DR("LB5_1") = DT.Rows(i * 2).Item("LB5")
            DR("LB6_1") = DT.Rows(i * 2).Item("LB6")
            DR("LB7_1") = DT.Rows(i * 2).Item("LB7")
            DR("LB8_1") = DT.Rows(i * 2).Item("LB8")
            DR("LB9_1") = DT.Rows(i * 2).Item("LB9")
            DR("LB10_1") = DT.Rows(i * 2).Item("LB10")
            DR("LB11_1") = DT.Rows(i * 2).Item("LB11")
            DR("TB1_1") = DT.Rows(i * 2).Item("TB1")
            DR("TB2_1") = DT.Rows(i * 2).Item("TB2")
            DR("TB3_1") = DT.Rows(i * 2).Item("TB3")
            DR("TB4_1") = DT.Rows(i * 2).Item("TB4")
            DR("TB5_1") = DT.Rows(i * 2).Item("TB5")
            DR("TB6_1") = DT.Rows(i * 2).Item("TB6")
            DR("TB7_1") = DT.Rows(i * 2).Item("TB7")
            DR("TB8_1") = DT.Rows(i * 2).Item("TB8")
            DR("TB9_1") = DT.Rows(i * 2).Item("TB9")
            DR("TB10_1") = DT.Rows(i * 2).Item("TB10")

            If (i * 2) + 1 < DT.Rows.Count Then
                DR("DETAIL_ID_2") = DT.Rows((i * 2) + 1).Item("DETAIL_ID")
                DR("NAME_VEHICLE_EMPLOY_ID_2") = DT.Rows((i * 2) + 1).Item("NAME_VEHICLE_EMPLOY_ID")
                DR("NAME_VEHICLE_EMPLOY_NAME_2") = DT.Rows((i * 2) + 1).Item("NAME_VEHICLE_EMPLOY_NAME")
                DR("CB_2") = DT.Rows((i * 2) + 1).Item("CB")
                DR("LB1_2") = DT.Rows((i * 2) + 1).Item("LB1")
                DR("LB2_2") = DT.Rows((i * 2) + 1).Item("LB2")
                DR("LB3_2") = DT.Rows((i * 2) + 1).Item("LB3")
                DR("LB4_2") = DT.Rows((i * 2) + 1).Item("LB4")
                DR("LB5_2") = DT.Rows((i * 2) + 1).Item("LB5")
                DR("LB6_2") = DT.Rows((i * 2) + 1).Item("LB6")
                DR("LB7_2") = DT.Rows((i * 2) + 1).Item("LB7")
                DR("LB8_2") = DT.Rows((i * 2) + 1).Item("LB8")
                DR("LB9_2") = DT.Rows((i * 2) + 1).Item("LB9")
                DR("LB10_2") = DT.Rows((i * 2) + 1).Item("LB10")
                DR("LB11_2") = DT.Rows((i * 2) + 1).Item("LB11")
                DR("TB1_2") = DT.Rows((i * 2) + 1).Item("TB1")
                DR("TB2_2") = DT.Rows((i * 2) + 1).Item("TB2")
                DR("TB3_2") = DT.Rows((i * 2) + 1).Item("TB3")
                DR("TB4_2") = DT.Rows((i * 2) + 1).Item("TB4")
                DR("TB5_2") = DT.Rows((i * 2) + 1).Item("TB5")
                DR("TB6_2") = DT.Rows((i * 2) + 1).Item("TB6")
                DR("TB7_2") = DT.Rows((i * 2) + 1).Item("TB7")
                DR("TB8_2") = DT.Rows((i * 2) + 1).Item("TB8")
                DR("TB9_2") = DT.Rows((i * 2) + 1).Item("TB9")
                DR("TB10_2") = DT.Rows((i * 2) + 1).Item("TB10")
            End If
            DT_NameEmploy.Rows.Add(DR)
        Next

        rptNameEmploy.DataSource = DT_NameEmploy
        rptNameEmploy.DataBind()

        If DT_Bafco.Rows.Count = 0 And DT_Employ.Rows.Count = 0 And DT_NameEmploy.Rows.Count = 0 Then
            trVehicle1.Visible = False
            trVehicle2.Visible = False
        Else
            trVehicle1.Visible = True
            trVehicle2.Visible = True
        End If
    End Sub

    Protected Sub rptBafco_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptBafco.ItemDataBound
        Dim cb As CheckBox = e.Item.FindControl("cbC1")
        If e.Item.DataItem("VEHICLE_BAFCO_NAME").ToString <> "" Then
            cb.Visible = True
            cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
            If e.Item.DataItem("CB").ToString = "True" Then
                cb.Checked = True
            Else
                cb.Checked = False
            End If
            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Function GetDataBafco() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptBafco.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptBafco.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

    Protected Sub rptEmploy_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptEmploy.ItemDataBound
        Dim cb As CheckBox = e.Item.FindControl("cbC1")
        If e.Item.DataItem("VEHICLE_EMPLOY_NAME").ToString <> "" Then
            cb.Visible = True
            cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
            If e.Item.DataItem("CB").ToString = "True" Then
                cb.Checked = True
            Else
                cb.Checked = False
            End If
            For i As Int32 = 1 To 11
                Dim lbl As Label = e.Item.FindControl("lbl_C1" & "_" & i)
                If e.Item.DataItem("lb" & i).ToString <> "" Then
                    lbl.Text = e.Item.DataItem("lb" & i)
                    lbl.Visible = True
                End If
            Next

            For i As Int32 = 1 To 10
                Dim txt As TextBox = e.Item.FindControl("txt_C1" & "_" & i)
                txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID").ToString)
                If Not (e.Item.DataItem("tb" & i) Is DBNull.Value) Then
                    txt.Text = e.Item.DataItem("tb" & i)
                    txt.Visible = True
                End If
            Next
        End If
    End Sub

    Function GetDataEmploy() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptEmploy.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptEmploy.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 1
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function

    Protected Sub rptNameEmploy_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptNameEmploy.ItemDataBound
        For x As Int32 = 1 To 2
            Dim cb As CheckBox = e.Item.FindControl("cbC" & x)
            If e.Item.DataItem("NAME_VEHICLE_EMPLOY_NAME_" & x).ToString <> "" Then
                cb.Visible = True
                cb.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                If e.Item.DataItem("CB_" & x).ToString = "True" Then
                    cb.Checked = True
                Else
                    cb.Checked = False
                End If
                For i As Int32 = 1 To 11
                    Dim lbl As Label = e.Item.FindControl("lbl_C" & x & "_" & i)
                    If e.Item.DataItem("lb" & i & "_" & x).ToString <> "" Then
                        lbl.Text = e.Item.DataItem("lb" & i & "_" & x)
                        lbl.Visible = True
                    End If
                Next

                For i As Int32 = 1 To 10
                    Dim txt As TextBox = e.Item.FindControl("txt_C" & x & "_" & i)
                    txt.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
                    txt.Attributes.Add("DETAIL_ID", e.Item.DataItem("DETAIL_ID_" & x).ToString)
                    If Not (e.Item.DataItem("tb" & i & "_" & x) Is DBNull.Value) Then
                        txt.Text = e.Item.DataItem("tb" & i & "_" & x)
                        txt.Visible = True
                    End If
                Next
            End If
        Next
    End Sub

    Function GetDataNameEmploy() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("DETAIL_ID")
        DT.Columns.Add("CB")
        DT.Columns.Add("TB1")
        DT.Columns.Add("TB2")
        DT.Columns.Add("TB3")
        DT.Columns.Add("TB4")
        DT.Columns.Add("TB5")
        DT.Columns.Add("TB6")
        DT.Columns.Add("TB7")
        DT.Columns.Add("TB8")
        DT.Columns.Add("TB9")
        DT.Columns.Add("TB10")

        If rptNameEmploy.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptNameEmploy.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
                For x As Int32 = 1 To 2
                    Dim DR As DataRow = DT.NewRow
                    Dim cb As CheckBox = ri.FindControl("cbC" & x)
                    If cb Is Nothing Then
                        Exit For
                    End If
                    Dim DetailID As Int32 = cb.Attributes("DETAIL_ID")
                    If DetailID = 0 Then
                        Exit For
                    End If
                    DR("DETAIL_ID") = DetailID
                    If cb.Checked = True Then
                        DR("CB") = cb.Checked
                    Else
                        DR("CB") = DBNull.Value
                    End If

                    For i As Int32 = 1 To 10
                        Dim txt As TextBox = ri.FindControl("txt_C" & x & "_" & i)
                        If txt.Visible = True Then
                            DR("TB" & i) = txt.Text
                        Else
                            DR("TB" & i) = DBNull.Value
                        End If
                    Next
                    DT.Rows.Add(DR)
                Next
            Next
        End If
        Return DT
    End Function
#End Region

#Region "Remark"
    Sub BindRemark(ByVal JobID As Int32)
        Dim Sql As String = ""
        Sql = "SELECT * FROM JOB_DETAIL_REMARK WHERE JOB_ID = " & JobID & " AND DF_DOC_TYPE = " & ddlDocType.SelectedValue.ToString
        Dim DA As New SqlDataAdapter(Sql, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            txtRemark1.Text = DT.Rows(0).Item("REMARK1").ToString
            txtRemark2.Text = DT.Rows(0).Item("REMARK2").ToString
            txtRemark3.Text = DT.Rows(0).Item("REMARK3").ToString
            txtRemark4.Text = DT.Rows(0).Item("REMARK4").ToString
        End If
    End Sub

#End Region

    Protected Sub ddlDocType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocType.SelectedIndexChanged
        BindData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SaveData()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

    Private Sub SaveData()
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable
        Dim SQL As String = ""

        Dim DT_DATA As New DataTable
        For x As Int32 = 1 To 8
            Select Case x
                Case 1
                    DT_DATA = GetDataEntryType()
                    SQL = "SELECT * FROM JOB_DETAIL_ENTRY_TYPE" & vbCrLf
                Case 2
                    DT_DATA = GetDataJobType()
                    SQL = "SELECT * FROM JOB_DETAIL_JOB_TYPE" & vbCrLf
                Case 3
                    DT_DATA = GetDataFee()
                    SQL = "SELECT * FROM JOB_DETAIL_FEE" & vbCrLf
                Case 4
                    DT_DATA = GetDataJobDoc()
                    SQL = "SELECT * FROM JOB_DETAIL_JOB_DOC" & vbCrLf
                Case 5
                    DT_DATA = GetDataAccounting()
                    SQL = "SELECT * FROM JOB_DETAIL_ACCOUNTING" & vbCrLf
                Case 6
                    DT_DATA = GetDataBafco()
                    SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_BAFCO" & vbCrLf
                Case 7
                    DT_DATA = GetDataEmploy()
                    SQL = "SELECT * FROM JOB_DETAIL_VEHICLE_EMPLOY" & vbCrLf
                Case 8
                    DT_DATA = GetDataNameEmploy()
                    SQL = "SELECT * FROM JOB_DETAIL_NAME_VEHICLE_EMPLOY" & vbCrLf
            End Select

            For i As Int32 = 0 To DT_DATA.Rows.Count - 1
                Dim SQL_Str As String = ""
                SQL_Str = SQL & "WHERE DETAIL_ID = " & DT_DATA.Rows(i).Item("DETAIL_ID")
                DA = New SqlDataAdapter(SQL_Str, ConnStr)
                DT = New DataTable
                DA.Fill(DT)
                Dim DR As DataRow = DT.Rows(0)
                DR("CB") = DT_DATA.Rows(i).Item("CB")
                DR("TB1") = DT_DATA.Rows(i).Item("TB1")
                DR("TB2") = DT_DATA.Rows(i).Item("TB2")
                DR("TB3") = DT_DATA.Rows(i).Item("TB3")
                DR("TB4") = DT_DATA.Rows(i).Item("TB4")
                DR("TB5") = DT_DATA.Rows(i).Item("TB5")
                DR("TB6") = DT_DATA.Rows(i).Item("TB6")
                DR("TB7") = DT_DATA.Rows(i).Item("TB7")
                DR("TB8") = DT_DATA.Rows(i).Item("TB8")
                DR("TB9") = DT_DATA.Rows(i).Item("TB9")
                DR("TB10") = DT_DATA.Rows(i).Item("TB10")
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)
            Next

        Next

        SQL = "SELECT * FROM JOB_DETAIL_REMARK WHERE JOB_ID = " & JobID & " AND  DF_DOC_TYPE = " & ddlDocType.SelectedValue
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If txtRemark1.Text.Trim <> "" Then
                DT.Rows(0).Item("REMARK1") = txtRemark1.Text
            Else
                DT.Rows(0).Item("REMARK1") = DBNull.Value
            End If
            If txtRemark2.Text.Trim <> "" Then
                DT.Rows(0).Item("REMARK2") = txtRemark2.Text
            Else
                DT.Rows(0).Item("REMARK2") = DBNull.Value
            End If
            If txtRemark3.Text.Trim <> "" Then
                DT.Rows(0).Item("REMARK3") = txtRemark3.Text
            Else
                DT.Rows(0).Item("REMARK3") = DBNull.Value
            End If
            If txtRemark4.Text.Trim <> "" Then
                DT.Rows(0).Item("REMARK4") = txtRemark4.Text
            Else
                DT.Rows(0).Item("REMARK4") = DBNull.Value
            End If
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)
        End If
        
    End Sub
End Class

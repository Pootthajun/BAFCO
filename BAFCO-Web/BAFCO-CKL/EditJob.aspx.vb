﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class EditJob
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim MC As New MasterControlClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlCustomerFromUpload(ddlFilterCustomer, "------------ Select ------------")
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT DT.REF_ID,REF_NO,HD.JOB_ID,HD.JOB_NO,HD.CUSTOMER" & vbCrLf
        SQL &= "FROM JOB_HEADER HD" & vbCrLf
        SQL &= "LEFT JOIN REF_DETAIL DT ON HD.JOB_ID = DT.JOB_ID" & vbCrLf
        SQL &= "LEFT JOIN REF_HEADER REF ON DT.REF_ID = REF.REF_ID" & vbCrLf
        SQL &= "WHERE 1=1 AND "

        If txtFilterRefNo.Text <> "" Then
            SQL &= "REF_NO like '%" & txtFilterRefNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txtFilterJobNo.Text <> "" Then
            SQL &= "HD.JOB_NO like '%" & txtFilterJobNo.Text.Replace("'", "''") & "%' AND "
        End If
        If ddlFilterCustomer.SelectedIndex > 0 Then
            SQL &= "HD.CUSTOMER = '" & ddlFilterCustomer.SelectedValue.Replace("'", "''") & "' AND "
        End If
        SQL = SQL.Substring(0, SQL.Length - 4)

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Columns.Add("edit_mode", GetType(String), "'Edit'")
        DT.Columns.Add("view_mode", GetType(String), "'View'")
        '--------------- Filter ----------------------
        'rptData.DataSource = DT
        'rptData.DataBind()

        If DT.Rows.Count <= 15 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("FilterCustomer") = DT
        Navigation.SesssionSourceName = "FilterCustomer"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblRefId As Label = e.Item.FindControl("lblRefId")
        Dim lblRefName As Label = e.Item.FindControl("lblRefName")
        Dim lblJobId As Label = e.Item.FindControl("lblJobId")
        Dim lblJobName As Label = e.Item.FindControl("lblJobName")
        Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")

        Dim lblEditRefId As Label = e.Item.FindControl("lblEditRefId")
        Dim lblEditRefName As Label = e.Item.FindControl("lblEditRefName")
        Dim lblEditJobId As Label = e.Item.FindControl("lblEditJobId")
        Dim lblEditJobName As Label = e.Item.FindControl("lblEditJobName")
        Dim ddlCustomer As DropDownList = e.Item.FindControl("ddlCustomer")
       
        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim btnSave As ImageButton = e.Item.FindControl("btnSave")
        Dim btnCancel As ImageButton = e.Item.FindControl("btnCancel")

        Dim trView As HtmlTableRow = e.Item.FindControl("trView")
        Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblRefId.Text = e.Item.DataItem("REF_ID").ToString
        lblRefName.Text = e.Item.DataItem("REF_NO").ToString
        lblJobId.Text = e.Item.DataItem("JOB_ID").ToString
        lblJobName.Text = e.Item.DataItem("JOB_NO").ToString
        lblCustomer.Text = e.Item.DataItem("CUSTOMER").ToString

        lblEditRefId.Text = e.Item.DataItem("REF_ID").ToString
        lblEditRefName.Text = e.Item.DataItem("REF_NO").ToString
        lblEditJobId.Text = e.Item.DataItem("JOB_ID").ToString
        lblEditJobName.Text = e.Item.DataItem("JOB_NO").ToString
        MC.BindDDlCustomerFromUpload(ddlCustomer, "", e.Item.DataItem("CUSTOMER").ToString)

        '--------------- เปิด ปิด ฟังก์ชั่น Edit -----------
        trView.Visible = e.Item.DataItem("view_mode") = "View"
        trEdit.Visible = e.Item.DataItem("view_mode") = "Edit"

        '---------------- เก็บ Mode Edit,Add----------------
        Dim imgEdit As Image = e.Item.FindControl("imgEdit")
        imgEdit.Attributes("Mode") = e.Item.DataItem("edit_mode")

    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim ddlCustomer As DropDownList = e.Item.FindControl("ddlCustomer")
                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                trView.Visible = False
                trEdit.Visible = True
                ddlCustomer.Focus()
            Case "Save"
                Dim lblEditRefId As Label = e.Item.FindControl("lblEditRefId")
                Dim lblEditRefName As Label = e.Item.FindControl("lblEditRefName")
                Dim lblEditJobId As Label = e.Item.FindControl("lblEditJobId")
                Dim lblEditJobName As Label = e.Item.FindControl("lblEditJobName")
                Dim ddlCustomer As DropDownList = e.Item.FindControl("ddlCustomer")

                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")

                Dim imgEdit As Image = e.Item.FindControl("imgEdit")
                Select Case imgEdit.Attributes("Mode")
                    Case "Edit"
                        Dim SQL As String = ""
                        SQL &= "UPDATE JOB_HEADER SET CUSTOMER = '" & ddlCustomer.Text.Replace("'", "''") & "' WHERE JOB_ID = " & lblEditJobId.Text & vbCrLf
                        SQL &= "UPDATE REF_DETAIL SET CUSTOMER = '" & ddlCustomer.Text.Replace("'", "''") & "' WHERE REF_ID = " & lblEditRefId.Text & " AND JOB_ID = " & lblEditJobId.Text & vbCrLf
                        Dim conn As New SqlConnection(ConnStr)
                        conn.Open()
                        Dim SqlCmd As SqlCommand
                        SqlCmd = New SqlCommand(SQL, conn)
                        SqlCmd.ExecuteNonQuery()
                        conn.Close()
                End Select

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
                BindData()

                Select Case imgEdit.Attributes("Mode")
                    Case "Edit"
                        trView.Visible = True
                        trEdit.Visible = False
                End Select
            Case "Cancel"
                Dim lblCustomer As Label = e.Item.FindControl("lblCustomer")
                '----------------- คืนค่า ----------------
                Dim ddlCustomer As DropDownList = e.Item.FindControl("ddlCustomer")
                MC.BindDDlCustomerFromUpload(ddlCustomer, "", lblCustomer.Text)

                '---------------- กลับไป ตารางเดิม --------
                Dim imgEdit As Image = e.Item.FindControl("imgEdit")
                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                Select Case imgEdit.Attributes("Mode")
                    Case "Edit"
                        trView.Visible = True
                        trEdit.Visible = False
                End Select
        End Select
    End Sub

    Protected Sub txtJobNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFilterRefNo.TextChanged, txtFilterJobNo.TextChanged, ddlFilterCustomer.SelectedIndexChanged
        BindData()
    End Sub

End Class

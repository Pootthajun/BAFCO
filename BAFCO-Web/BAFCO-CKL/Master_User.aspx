﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_User.aspx.vb" Inherits="Master_User"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="40" colspan="2" class="Top_Menu_Bar">
                <ul class="shortcut-buttons-set">
                    <li>
                        <asp:LinkButton ID="btnClear" runat="server" CssClass="shortcut-button">
                            <span><img src="images/icon/41.png"/><br />Clear</span>
                        </asp:LinkButton>
                    </li>
                    <li>
                        <asp:LinkButton ID="btnSave" runat="server" CssClass="shortcut-button">
                            <span><img src="images/icon/59.png"/><br />Save</span>
                        </asp:LinkButton>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="1000" valign="top">
                <asp:UpdatePanel ID="udp1" runat="server" >
                    <ContentTemplate>
                        <br />
                        <table width="800" cellspacing="5px" style="margin-left:10px;">
                            <tr>
                                <td colspan="3" class="NormalTextBlack" style="height:30px; background-color:Navy; text-align:center;">
                                     <asp:Label ID="lblHead" runat="server" ForeColor="White" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="height:10px;">
                                
                                </td>
                            </tr>
                            <tr>
                                <td width="120" height="25" class="NormalTextBlack">
                                   User Code / Login
                                </td>
                                <td width="200" height="25">
                                    <asp:TextBox ID="txtCode" runat="server" CssClass="Textbox_Form_Required" width="200px" MaxLength ="30"></asp:TextBox>
                                    <asp:TextBox ID="txtTmpCode" runat="server" style="position:absolute; display:none;"></asp:TextBox>
                                    <asp:Label ID="lblId" runat="server" style="position:absolute; display:none;"></asp:Label>
                                </td>
                                <td width="25" height="25">
                                    <asp:ImageButton ID="imgCodeDialog" runat="server" ImageUrl="images/Search.png" />
                                    <asp:Button ID="btnCodeDialog"  runat="server" style="display:none;"/>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="120" height="25" class="NormalTextBlack">
                                   Password
                                </td>
                                <td width="200" height="25">
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="Textbox_Form_Required" width="200px" MaxLength ="15"></asp:TextBox>
                                </td>
                                <td width="25" height="25"> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="120" height="25" class="NormalTextBlack">
                                   Fullname
                                </td>
                                <td width="200" height="25">
                                    <asp:TextBox ID="txtFullname" runat="server" CssClass="Textbox_Form_Required" width="200px" MaxLength ="100"></asp:TextBox>
                                </td>
                                <td width="25" height="25"> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="100" height="25" class="NormalTextBlack">
                                   Department
                                </td>
                                <td width="200" height="25">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" 
                                    CssClass="Dropdown_Form_Required" width="205px">
                                    </asp:DropDownList>
                                </td>
                                <td width="25" height="25"> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="100" height="25" class="NormalTextBlack">
                                   User Type
                                </td>
                                <td width="200" height="25">
                                    <asp:DropDownList ID="ddlType" runat="server" 
                                    CssClass="Dropdown_Form_Required" width="205px">
                                    </asp:DropDownList>
                                </td>
                                <td width="25" height="25"> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="120" height="25" class="NormalTextBlack">
                                   Approve Password
                                </td>
                                <td width="200" height="25">
                                    <asp:TextBox ID="txtAppPassword" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength ="15"></asp:TextBox>
                                </td>
                                <td width="25" height="25"> 
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="100" height="25" class="NormalTextBlack">
                                   Status
                                </td>
                                <td width="200" height="25">
                                    <asp:DropDownList ID="ddlStatus" runat="server" 
                                    CssClass="Dropdown_Form_Required" width="205px">
                                    <asp:ListItem Text="Active" Value="True"></asp:ListItem>
                                    <asp:ListItem Text="Inactive" Value="False"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td width="25" height="25"> 
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>           
            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>


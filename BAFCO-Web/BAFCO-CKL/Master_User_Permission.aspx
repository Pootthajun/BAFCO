<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_User_Permission.aspx.vb" Inherits="Master_User_Permission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" border="0" cellspacing="0" cellpadding="0" cellspacing="5">
    <tr>
        <td height="40" colspan="4" class="Top_Menu_Bar">
            <ul class="shortcut-buttons-set">
			      <li>
			        <asp:LinkButton ID="btnSave" runat="server" CssClass="shortcut-button">
			            <span>
				            <img src="images/icon/59.png"/><br />
			             Save
			            </span>
			        </asp:LinkButton>
			      </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td width="450" valign="top">
            <div class="Fieldset_Container" >
                <table width="500" cellspacing="5px;" style="margin-left:15px; margin-bottom:10px; margin-top:10px; margin-right:15px;">
                    <tr>
                        <td width="100" height="25">
                            <asp:TextBox ID="txttxtUserCode" runat="server" CssClass="Textbox_Form_Disable"></asp:TextBox>
                            <asp:TextBox ID="txtTempUserID" runat="server" style="position:absolute; display:none;"></asp:TextBox>
                        </td>
                        <td width="50" height="25" align="left">
                            <asp:ImageButton ID="imgUserDialog" runat="server" ImageUrl="images/Search.png" style="cursor:pointer;"/>
                            <asp:Button ID="btnUserDialog"  runat="server" style="display:none;"/>
                        </td>
                        <td width="250" height="25" >
                            <asp:TextBox ID="txtFullName" runat="server" CssClass="Textbox_Form_Disable" width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <table width="500" align="left" cellpadding="0" cellspacing="1" style="margin:10px 0px 0px 0px;" bgColor="#CCCCCC" class="NormalText">
                                <tr>
                                    <td  width="80" class="Grid_Header">
                                        No.
                                    </td>
                                    <td width="350" class="Grid_Header">
                                        Menu
                                    </td>
                                    <td  width="80" class="Grid_Header">
                                       Visible
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="tbTag" runat="server" style="cursor: pointer; border-bottom: solid 1px #efefef">
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblNo" runat="server" Width="60"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="left">
                                            <asp:Label ID="lblMenuID" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblMenu" runat="server" Width="300"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center" style="background-color:White">
                                            <asp:CheckBox ID="cbCheck" runat="server" Text="" ></asp:CheckBox>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table> 
            </div>
        </td>
        <td></td>
    </tr>
</table>
</asp:Content>


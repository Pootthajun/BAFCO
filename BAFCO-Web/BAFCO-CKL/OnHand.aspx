﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="OnHand.aspx.vb" Inherits="OnHand" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="divNewRef" runat="server" style="position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1;" visible="False">                    
        <div style="width:100%; height:100%; position:absolute; background-color:Gray; filter:alpha(opacity=80); opacity: 0.80;" ></div>
        <div style="left:15%; top:5%; position:absolute; background-color:#F6F3F3; padding: 20px 20px 20px 20px; width:875px; height:530px;">
            <table width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
                <tr>
                    <td height="40" align="center" class="Master_Header_Text">
                        <asp:Label ID="lblHead" runat="server" Text="-"></asp:Label>
                    </td>                
                </tr>
            </table>
            <table border="0" cellpadding="0" cellspacing="1" style="padding-top:10px; padding-bottom:10px;">
                <tr>
                    <td height="25" class="NormalTextBlack" width="100px">
                        Reference No
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtRefNo" runat="server" CssClass="Textbox_Form_Disable" Enabled="false" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td width="40" height="25" align="left">
                    </td>
                </tr>
            </table>    
            <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                <tr>
                    <td width="40" class="Grid_Header">
                        Revert
                    </td>
                    <td width="40" class="Grid_Header">
                        View
                    </td>
                    <td width="113" class="Grid_Header">
                        Job Reference No
                    </td>
                    <td width="300px" class="Grid_Header">
                        Customer
                    </td>
                    <td width="100px" class="Grid_Header">
                        Submitted Date
                    </td>
                    <td width="100px" class="Grid_Header">
                        Document Type
                    </td>
                    <td width="100px" class="Grid_Header">
                        Payment
                    </td>
                </tr>
                <tr>
                    <td colspan="7">
                        <asp:Panel ID="PanelData" runat="server">
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                    <td id="td1" runat="server" class="Grid_Detail" align="center" valign="middle">
                                        <asp:ImageButton ID="btnRevert" runat="server" ToolTip="Reverse to submit" ImageUrl="images/icon/77.png" Height="25px" CommandName="Revert"/>
                                        <Ajax:ConfirmButtonExtender ID="btnRevert_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to reverse to submit?" TargetControlID="btnRevert">
                                        </Ajax:ConfirmButtonExtender>
                                    </td>
                                    <td id="td2" runat="server" class="Grid_Detail" align="center" valign="middle">
                                        <asp:ImageButton ID="btnView" runat="server" ToolTip="View Document" ImageUrl="images/icon/70.png" Height="25px"/>
                                    </td>
                                    <td id="td3" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px; text-align:center;">
                                        <asp:Label ID="lblJobNo" runat="server" ></asp:Label>
                                    </td> 
                                    <td id="td4" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px;">
                                        <asp:Label ID="lblCustomer" runat="server" ></asp:Label>
                                    </td>
                                    <td id="td5" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px; text-align:center;">
                                        <asp:Label ID="lblJobDate" runat="server" ></asp:Label>
                                    </td>
                                    <td id="td6" runat="server" bgcolor="#DDDDDD" style="height:25px; padding:0 0 0 10px; text-align:center;">
                                        <asp:Label ID="lblDocType" runat="server" ></asp:Label>
                                    </td>
                                    <td id="td7" runat="server" bgcolor="#DDDDDD" style="height:25px; text-align:center;">
                                        <asp:Label ID="lblPayment" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        </asp:Panel>
                    </td>
                </tr>
            </table>      
            <table cellpadding="0" cellspacing="1" width="100%">
                <tr>
                    <td height="40" align="right" valign="bottom">
                        <asp:Button ID="btnReceive" runat="server" Text="Receive" CssClass="Button_Red" Width="100px" Height="30px"/>
                        <Ajax:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" Enabled="true" ConfirmText="Are you sure you want to Receive?" TargetControlID="btnReceive">
                        </Ajax:ConfirmButtonExtender>
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="Button_Red" Width="100px" Height="30px"/>
                    </td>              
                </tr>
            </table>
        </div>
    </div>
    <table cellpadding=0 cellspacing=0 width="100%">
        <tr>
            <td style="padding-left:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">    
                    <tr>
                        <td valign="top">
                             <table align="left" cellpadding="0" cellspacing="1">
                                <tr>
                                    <td>
                                        <div class="Fieldset_Container" >
                                            <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                            <table width="605px" align="center" cellspacing="5px;" style="margin-left:10px;">
                                                <tr>
                                                    <td height="25" class="NormalTextBlack">
                                                        Reference No
                                                    </td>
                                                    <td height="25">
                                                        <asp:TextBox ID="txtRefNoSort" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25"></td>
                                                    <td height="25" class="NormalTextBlack">
                                                        Job No
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtJobNo" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25"></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Submitted <br> Date From
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtDateFrom" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgDateFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgDateFrom_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgDateFrom"
				                                        TargetControlID="txtDateFrom"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td width="100" height="25" class="NormalTextBlack">
                                                        Submitted <br> Date To
                                                    </td>
                                                    <td width="145" height="25">
                                                        <asp:TextBox ID="txtDateTo" runat="server" AutoPostBack="true" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD" style="text-align:center;"></asp:TextBox>
                                                    </td>
                                                    <td width="40" height="25" align="left">
                                                        <asp:ImageButton ID="imgDateTo" runat="server" ImageUrl="images/Calendar.png" />
                                                        <Ajax:CalendarExtender ID="imgDateTo_CDE" runat="server" 
                                                        Format="yyyy-MM-dd"  PopupButtonID="imgDateTo"
				                                        TargetControlID="txtDateTo"></Ajax:CalendarExtender>
                                                    </td>
                                                    <td></td>
                                                </tr> 
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>       
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px;">
                            <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                                <tr>
                                    <td width="40" class="Grid_Header">
                                        No
                                    </td>
                                    <td width="120" class="Grid_Header">
                                        Reference No
                                    </td>
                                    <td width="120" class="Grid_Header">
                                        Submitted Date
                                    </td>
                                    <td width="120" class="Grid_Header">
                                        From
                                    </td>
                                    <td width="80" class="Grid_Header">
                                        Job(s)
                                    </td>
                                    <td width="100" class="Grid_Header">                  
                                        Action
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7">
                                        <asp:Repeater ID="rptDataRef" runat="server">
                                            <ItemTemplate>
                                                <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblRefNo" runat="server" ></asp:Label>
                                                    </td> 
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblRefDate" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblRefFrom" runat="server" ></asp:Label>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                        <asp:Label ID="lblRefJob" runat="server" ></asp:Label>
                                                    </td>
                                                    <td class="Grid_Detail" align="center" valign="middle">
                                                        <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Receive" ImageUrl="images/icon/70.png" Height="25px"/>
                                                        <asp:ImageButton ID="btnRevertCloseJob" CommandName="RevertCloseJob" runat="server" ToolTip="Reverse to Close Job" ImageUrl="images/icon/77.png" Height="25px" />
                                                        <Ajax:ConfirmButtonExtender ID="btnRevertCloseJob_Conform" runat="server" Enabled="true" ConfirmText="Are you sure you want to reverse to Close Job?" TargetControlID="btnRevertCloseJob">
                                                        </Ajax:ConfirmButtonExtender>
                                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                                        <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete">
                                                        </Ajax:ConfirmButtonExtender>
                                                    </td>                                 
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>
                            <div style="margin-left:10px">
                                <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="15"/>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>


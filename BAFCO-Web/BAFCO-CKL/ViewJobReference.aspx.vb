﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class ViewJobReference
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass
    Dim CV As New Converter

    Private Property JobID() As Integer
        Get
            If IsNumeric(ViewState("JobID")) Then
                Return ViewState("JobID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("JobID") = value
        End Set
    End Property

    Private Property RefID() As Integer
        Get
            If IsNumeric(ViewState("RefID")) Then
                Return ViewState("RefID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("RefID") = value
        End Set
    End Property

    Private Property RefStatus() As Integer
        Get
            Return ViewState("RefStatus")
        End Get
        Set(ByVal value As Integer)
            ViewState("RefStatus") = value
        End Set
    End Property

    Private Property UserID() As Integer
        Get
            Return ViewState("UserID")
        End Get
        Set(ByVal value As Integer)
            ViewState("UserID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            JobID = Request.QueryString("JobID").Replace("?", "")
            RefID = Request.QueryString("RefID").Replace("?", "")
            RefStatus = Request.QueryString("RefStatus").Replace("?", "")
            UserID = Request.QueryString("UserID").Replace("?", "")
            BindData()

            If Request.QueryString("PostData").Replace("?", "") = 0 Then
                divComment.Visible = False
            End If

            If Session("User_Type_Name") = "Administrator" Then
                btnDelete.Visible = True
            Else
                btnDelete.Visible = False
            End If

            If RefStatus >= MasterControlClass.Job_Status.Billed Then
                btnFOC.Visible = False
            End If
        End If
    End Sub

    Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT DISTINCT DESCRIPTION,REMARK,JOB_STATUS,USER_FULLNAME AS CLOSE_BY" & vbCrLf
        SQL &= ",CONVERT(VARCHAR(10),RECEIVE_DATE,20) + '   ' + CONVERT(VARCHAR(5),RECEIVE_DATE,108) RECEIVE_DATE" & vbCrLf
        SQL &= ",CONVERT(VARCHAR(10),CLOSE_DATE,20) + '   ' + CONVERT(VARCHAR(5),CLOSE_DATE,108) CLOSE_DATE" & vbCrLf
        SQL &= "FROM JOB_HISTORY LEFT JOIN MS_USER ON JOB_HISTORY.UPDATE_BY = MS_USER.USER_ID" & vbCrLf
        SQL &= "WHERE JOB_ID = " & JobID & " ORDER BY RECEIVE_DATE,CLOSE_DATE" & vbCrLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            tbData1.Visible = False
            tbData2.Visible = False
        End If
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblDesc As Label = e.Item.FindControl("lblDesc")
        Dim Img As Image = e.Item.FindControl("Img")
        Dim lblCloseBy As Label = e.Item.FindControl("lblCloseBy")
        Dim lblReceiveDate As Label = e.Item.FindControl("lblReceiveDate")
        Dim lblCloseDate As Label = e.Item.FindControl("lblCloseDate")
        Dim lblRemark As Label = e.Item.FindControl("lblRemark")

        lblDesc.Text = e.Item.DataItem("DESCRIPTION").ToString
        lblCloseBy.Text = e.Item.DataItem("CLOSE_BY").ToString
        Select Case e.Item.DataItem("JOB_STATUS")
            Case MasterControlClass.Job_History.Confirmation
                lblDesc.Text = lblDesc.Text & "  ( " & "Confirmation" & " )"
                Img.ImageUrl = "images/Icon/63.png"
            Case MasterControlClass.Job_History.Incomplete
                lblDesc.Text = lblDesc.Text & "  ( " & "Incomplete" & " )"
                Img.ImageUrl = "images/Icon/21.png"
            Case MasterControlClass.Job_History.Hold
                lblDesc.Text = lblDesc.Text & "  ( " & "Hold" & " )"
                Img.ImageUrl = "images/Icon/03.png"
            Case MasterControlClass.Job_History.Delete
                lblDesc.Text = lblDesc.Text & "  ( " & "Delete" & " )"
                Img.ImageUrl = "images/Icon/57.png"
            Case MasterControlClass.Job_History.FOC
                lblDesc.Text = lblDesc.Text & "  ( " & "FOC" & " )"
                Img.ImageUrl = "images/Icon/36.png"
        End Select
        lblReceiveDate.Text = e.Item.DataItem("RECEIVE_DATE")
        lblCloseDate.Text = e.Item.DataItem("CLOSE_DATE")
        lblRemark.Text = GL.ReplaceText(e.Item.DataItem("REMARK").ToString)

    End Sub

    Protected Sub btnConfirmation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConfirmation.Click
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable

        SQL = "SELECT * FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & " ORDER BY DETAIL_ID"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        DR = DT.NewRow
        DR("DETAIL_ID") = GL.FindID("JOB_HISTORY", "DETAIL_ID")
        DR("REF_ID") = RefID
        DR("JOB_ID") = JobID
        DR("DESCRIPTION") = MC.Get_JobDescription(RefStatus)
        DR("JOB_STATUS") = MasterControlClass.Job_History.Confirmation
        DR("REMARK") = txtRemark.Text
        Dim ReceiveDate As String = ""
        ReceiveDate = MC.Get_ReceiveDate(RefID, JobID)
        DR("RECEIVE_DATE") = ReceiveDate
        DR("CLOSE_DATE") = Now
        DR("UPDATE_BY") = UserID
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        SQL = ""
        Select Case RefStatus
            Case MasterControlClass.Job_Status.Checked
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Priced & vbCrLf
                SQL &= ",STATUS_CHECK = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                SQL &= ",STATUS_PRICE = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= ",CHECK_BY = " & UserID & vbCrLf
                SQL &= ",CHECK_DATE = GETDATE()" & vbCrLf
            Case MasterControlClass.Job_Status.Priced
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Approving & vbCrLf
                SQL &= ",STATUS_PRICE = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                SQL &= ",STATUS_APP = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= ",PRICE_BY = " & UserID & vbCrLf
                SQL &= ",PRICE_DATE = GETDATE()" & vbCrLf
            Case MasterControlClass.Job_Status.Approving
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Billed & vbCrLf
                SQL &= ",STATUS_APP = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                SQL &= ",STATUS_BILL = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= ",APP_BY = " & UserID & vbCrLf
                SQL &= ",APP_DATE = GETDATE()" & vbCrLf
            Case MasterControlClass.Job_Status.Billed
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.Completed & vbCrLf
                SQL &= ",STATUS_BILL = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Wait & vbCrLf
                SQL &= ",BILL_BY = " & UserID & vbCrLf
                SQL &= ",BILL_DATE = GETDATE()" & vbCrLf
            Case MasterControlClass.Job_Status.Completed
                SQL &= "UPDATE REF_DETAIL SET STATUS = " & MasterControlClass.Job_Status.EndProcess & vbCrLf
                SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Confirmation & vbCrLf
                SQL &= ",COMPLETE_BY = " & UserID & vbCrLf
                SQL &= ",COMPLETE_DATE = GETDATE()" & vbCrLf
        End Select
        SQL &= ",UPDATE_BY = " & UserID & " ,UPDATE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()

        Dim Script As String = ""
        Script = "returnSelectedValue('" & RefID & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseDialog", Script, True)
    End Sub

    Protected Sub btnIncomplete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIncomplete.Click
        If txtRemark.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter remark');", True)
            txtRemark.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable

        SQL = "SELECT * FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & " ORDER BY DETAIL_ID"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        DR = DT.NewRow
        DR("DETAIL_ID") = GL.FindID("JOB_HISTORY", "DETAIL_ID")
        DR("REF_ID") = RefID
        DR("JOB_ID") = JobID
        DR("DESCRIPTION") = MC.Get_JobDescription(RefStatus)
        DR("JOB_STATUS") = MasterControlClass.Job_History.Incomplete
        DR("REMARK") = txtRemark.Text
        Dim ReceiveDate As String = ""
        ReceiveDate = MC.Get_ReceiveDate(RefID, JobID)
        DR("RECEIVE_DATE") = ReceiveDate
        DR("CLOSE_DATE") = Now
        DR("UPDATE_BY") = UserID
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        SQL = ""
        Select Case RefStatus
            Case MasterControlClass.Job_Status.Checked
                SQL &= "UPDATE REF_DETAIL SET STATUS_CHECK = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            Case MasterControlClass.Job_Status.Priced
                SQL &= "UPDATE REF_DETAIL SET STATUS_PRICE = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            Case MasterControlClass.Job_Status.Approving
                SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            Case MasterControlClass.Job_Status.Billed
                SQL &= "UPDATE REF_DETAIL SET STATUS_BILL = " & MasterControlClass.Job_History.Incomplete & vbCrLf
            Case MasterControlClass.Job_Status.Completed
                SQL &= "UPDATE REF_DETAIL SET STATUS_COMPLETE = " & MasterControlClass.Job_History.Incomplete & vbCrLf
        End Select
        SQL &= ",UPDATE_BY = " & UserID & " ,UPDATE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf

        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()

        Dim Script As String = ""
        Script = "returnSelectedValue('" & RefID & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseDialog", Script, True)
    End Sub

    Protected Sub btnHold_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHold.Click
        If txtRemark.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter remark');", True)
            txtRemark.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable

        SQL = "SELECT * FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & " ORDER BY DETAIL_ID"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        DR = DT.NewRow
        DR("DETAIL_ID") = GL.FindID("JOB_HISTORY", "DETAIL_ID")
        DR("REF_ID") = RefID
        DR("JOB_ID") = JobID
        DR("DESCRIPTION") = MC.Get_JobDescription(RefStatus)
        DR("JOB_STATUS") = MasterControlClass.Job_History.Hold
        DR("REMARK") = txtRemark.Text
        Dim ReceiveDate As String = ""
        ReceiveDate = MC.Get_ReceiveDate(RefID, JobID)
        DR("RECEIVE_DATE") = ReceiveDate
        DR("CLOSE_DATE") = Now
        DR("UPDATE_BY") = UserID
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        SQL = ""
        Select Case RefStatus
            Case MasterControlClass.Job_Status.Checked
                SQL &= "UPDATE REF_DETAIL SET STATUS_CHECK = " & MasterControlClass.Job_History.Hold & vbCrLf
            Case MasterControlClass.Job_Status.Priced
                SQL &= "UPDATE REF_DETAIL SET STATUS_PRICE = " & MasterControlClass.Job_History.Hold & vbCrLf
            Case MasterControlClass.Job_Status.Approving
                SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.Hold & vbCrLf
            Case MasterControlClass.Job_Status.Billed
                SQL &= "UPDATE REF_DETAIL SET STATUS_BILL = " & MasterControlClass.Job_History.Hold & vbCrLf
            Case MasterControlClass.Job_Status.Completed
                SQL &= "UPDATE REF_DETAIL SET STATUS_COMPLETE = " & MasterControlClass.Job_History.Hold & vbCrLf
        End Select
        SQL &= ",UPDATE_BY = " & UserID & " ,UPDATE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()

        Dim Script As String = ""
        Script = "returnSelectedValue('" & RefID & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseDialog", Script, True)
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If txtRemark.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter remark');", True)
            txtRemark.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable

        SQL = "SELECT * FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & " ORDER BY DETAIL_ID"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        DR = DT.NewRow
        DR("DETAIL_ID") = GL.FindID("JOB_HISTORY", "DETAIL_ID")
        DR("REF_ID") = RefID
        DR("JOB_ID") = JobID
        DR("DESCRIPTION") = MC.Get_JobDescription(RefStatus)
        DR("JOB_STATUS") = MasterControlClass.Job_History.Delete
        DR("REMARK") = txtRemark.Text
        Dim ReceiveDate As String = ""
        ReceiveDate = MC.Get_ReceiveDate(RefID, JobID)
        DR("RECEIVE_DATE") = ReceiveDate
        DR("CLOSE_DATE") = Now
        DR("UPDATE_BY") = UserID
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        SQL = ""
        'SQL &= "DELETE FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
        'SQL &= "DELETE FROM REF_DETAIL WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
        'SQL &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Close & " WHERE JOB_ID =" & JobID & vbCrLf
        'SQL &= "UPDATE JOB_HEADER SET STATUS = " & MasterControlClass.Job_Status.Delete & " WHERE JOB_ID =" & JobID & vbCrLf
        SQL = "DELETE FROM JOB_HEADER WHERE JOB_ID =" & JobID & vbCrLf
        Select Case RefStatus
            Case MasterControlClass.Job_Status.Checked
                SQL &= "UPDATE REF_DETAIL SET STATUS_CHECK = " & MasterControlClass.Job_History.Delete & vbCrLf
                SQL &= ",CHECK_BY = " & UserID & vbCrLf
                SQL &= ",CHECK_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
            Case MasterControlClass.Job_Status.Priced
                SQL &= "UPDATE REF_DETAIL SET STATUS_PRICE = " & MasterControlClass.Job_History.Delete & vbCrLf
                SQL &= ",PRICE_BY = " & UserID & vbCrLf
                SQL &= ",PRICE_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
            Case MasterControlClass.Job_Status.Approving
                SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.Delete & vbCrLf
                SQL &= ",APP_BY = " & UserID & vbCrLf
                SQL &= ",APP_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
            Case MasterControlClass.Job_Status.Billed
                SQL &= "UPDATE REF_DETAIL SET STATUS_BILL = " & MasterControlClass.Job_History.Delete & vbCrLf
                SQL &= ",BILL_BY = " & UserID & vbCrLf
                SQL &= ",BILL_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS = " & MasterControlClass.Job_Status.Delete & vbCrLf
        End Select
        SQL &= "WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf

        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()
        Dim Script As String = ""
        Script = "returnSelectedValue('" & RefID & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseDialog", Script, True)
    End Sub

    Protected Sub btnFOC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFOC.Click
        If txtRemark.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter remark');", True)
            txtRemark.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter
        Dim DT As New DataTable

        SQL = "SELECT * FROM JOB_HISTORY WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & " ORDER BY DETAIL_ID"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        DR = DT.NewRow
        DR("DETAIL_ID") = GL.FindID("JOB_HISTORY", "DETAIL_ID")
        DR("REF_ID") = RefID
        DR("JOB_ID") = JobID
        DR("DESCRIPTION") = MC.Get_JobDescription(RefStatus)
        DR("JOB_STATUS") = MasterControlClass.Job_History.FOC
        DR("REMARK") = txtRemark.Text
        Dim ReceiveDate As String = ""
        ReceiveDate = MC.Get_ReceiveDate(RefID, JobID)
        DR("RECEIVE_DATE") = ReceiveDate
        DR("CLOSE_DATE") = Now
        DR("UPDATE_BY") = UserID
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        SQL = ""
        Select Case RefStatus
            Case MasterControlClass.Job_Status.Checked
                SQL &= "UPDATE REF_DETAIL SET STATUS_CHECK = " & MasterControlClass.Job_History.FOC & vbCrLf
                SQL &= ",CHECK_BY = " & UserID & vbCrLf
                SQL &= ",CHECK_DATE = GETDATE()" & vbCrLf
                SQL &= ",COMPLETE_BY = " & UserID & vbCrLf
                SQL &= ",COMPLETE_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Wait & vbCrLf
            Case MasterControlClass.Job_Status.Priced
                SQL &= "UPDATE REF_DETAIL SET STATUS_PRICE = " & MasterControlClass.Job_History.FOC & vbCrLf
                SQL &= ",PRICE_BY = " & UserID & vbCrLf
                SQL &= ",PRICE_DATE = GETDATE()" & vbCrLf
                SQL &= ",COMPLETE_BY = " & UserID & vbCrLf
                SQL &= ",COMPLETE_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Wait & vbCrLf
            Case MasterControlClass.Job_Status.Approving
                SQL &= "UPDATE REF_DETAIL SET STATUS_APP = " & MasterControlClass.Job_History.FOC & vbCrLf
                SQL &= ",APP_BY = " & UserID & vbCrLf
                SQL &= ",APP_DATE = GETDATE()" & vbCrLf
                SQL &= ",COMPLETE_BY = " & UserID & vbCrLf
                SQL &= ",COMPLETE_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Wait & vbCrLf
            Case MasterControlClass.Job_Status.Billed
                SQL &= "UPDATE REF_DETAIL SET STATUS_BILL = " & MasterControlClass.Job_History.FOC & vbCrLf
                SQL &= ",BILL_BY = " & UserID & vbCrLf
                SQL &= ",BILL_DATE = GETDATE()" & vbCrLf
                SQL &= ",COMPLETE_BY = " & UserID & vbCrLf
                SQL &= ",COMPLETE_DATE = GETDATE()" & vbCrLf
                SQL &= ",STATUS_COMPLETE = " & MasterControlClass.Job_History.Wait & vbCrLf
            Case MasterControlClass.Job_Status.Completed
                SQL &= "UPDATE REF_DETAIL SET STATUS_COMPLETE = " & MasterControlClass.Job_History.FOC & vbCrLf
                SQL &= ",COMPLETE_BY = " & UserID & vbCrLf
                SQL &= ",COMPLETE_DATE = GETDATE()" & vbCrLf
        End Select
        SQL &= ",STATUS = " & MasterControlClass.Job_Status.FOC & vbCrLf
        SQL &= ",UPDATE_BY = " & UserID & " ,UPDATE_DATE = GETDATE()" & vbCrLf
        SQL &= "WHERE REF_ID = " & RefID & " AND JOB_ID = " & JobID & vbCrLf
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim SqlCmd As SqlCommand
        SqlCmd = New SqlCommand(SQL, conn)
        SqlCmd.ExecuteNonQuery()
        conn.Close()
        Dim Script As String = ""
        Script = "returnSelectedValue('" & RefID & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CloseDialog", Script, True)
    End Sub

End Class

﻿
Partial Class RenderFile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.QueryString("S")) OrElse IsNothing(Session(Request.QueryString("S"))) Then
            Dim Script As String = "alert('File is not found'); window.close();" & vbLf
            Script &= "parent.location.href=parent.location.href;"
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Alert", Script, True)
            Exit Sub
        End If

        Dim F As GenericLib.FileStructure = Session(Request.QueryString("S"))
        Response.Clear()
        Select Case F.FileType
            Case GenericLib.AllowFileType.JPG, GenericLib.AllowFileType.PNG
                Response.AddHeader("Content-Type", "image/jpeg")
                Response.AddHeader("Content-Type", "image/png")
            Case GenericLib.AllowFileType.PDF
                Response.AddHeader("Content-Type", "application/pdf")
        End Select
        Response.AppendHeader("Content-Disposition", "filename=" & F.AbsoluteName)
        Response.BinaryWrite(F.Content)
    End Sub
End Class

﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class MasterEditor_MultiLine
    Inherits System.Web.UI.UserControl

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib

    Public Property MasterName() As String
        Get
            Return lblMasterName.Text
        End Get
        Set(ByVal value As String)
            lblMasterName.Text = value
        End Set
    End Property

    Public Property TableName() As String
        Get
            Return lblTableName.Text
        End Get
        Set(ByVal value As String)
            lblTableName.Text = value
        End Set
    End Property

    Public Property RelateTransaction() As String
        Get
            Return lblTranscationField.Text
        End Get
        Set(ByVal value As String)
            lblTranscationField.Text = value
        End Set
    End Property

    Public ReadOnly Property RelateTable() As String
        Get
            Try
                Return RelateTransaction.Split(".")(0)
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public ReadOnly Property RelateField() As String
        Get
            Try
                Return RelateTransaction.Split(".")(1)
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public Property HasCode() As Boolean
        Get
            If Not IsNothing(codeHeader.Style("display")) AndAlso codeHeader.Style("display") = "none" Then
                Return False
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            Select Case value
                Case True
                    codeHeader.Style("display") = ""
                Case False
                    codeHeader.Style("display") = "none"
            End Select
        End Set
    End Property

    Public Event ManageFinished(ByRef Editor As MasterEditor_MultiLine)
    Public Event AddFinished(ByRef Editor As MasterEditor_MultiLine, ByVal LastValue As String)
    Public Event Selected(ByRef Editor As MasterEditor_MultiLine, ByVal Value As String)

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Visible = False
        RaiseEvent ManageFinished(Me)
    End Sub

    Public Sub BindData()
        Dim SQL As String = "select * from " & TableName & " ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Columns.Add("edit_mode", GetType(String), "'Edit'")
        DT.Columns.Add("view_mode", GetType(String), "'View'")

        HasCode = DT.Columns.IndexOf("Code") > -1

        rptMaster.DataSource = DT
        rptMaster.DataBind()
    End Sub


    Protected Sub rptMaster_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptMaster.ItemCommand
        Select Case e.CommandName
            Case "Selected"
                Dim lblName As LinkButton = e.Item.FindControl("lblName")
                RaiseEvent Selected(Me, lblName.Text)
            Case "Edit"
                Dim txtName As TextBox = e.Item.FindControl("txtName")
                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                trView.Visible = False
                trEdit.Visible = True
                txtName.Focus()
            Case "Delete"
                Dim lblID As Label = e.Item.FindControl("lblID")
                Dim Comm As New SqlCommand
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = "DELETE FROM " & TableName & " WHERE ID=" & lblID.Text
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()
                Dim DT As DataTable = CurrentDatasource()
                DT.Rows.RemoveAt(e.Item.ItemIndex)
                rptMaster.DataSource = DT
                rptMaster.DataBind()

            Case "Save"
                Dim lblID As Label = e.Item.FindControl("lblID")
                Dim lblName As LinkButton = e.Item.FindControl("lblName")
                Dim lblCode As LinkButton = e.Item.FindControl("lblCode")

                Dim txtName As TextBox = e.Item.FindControl("txtName")
                Dim txtCode As TextBox = e.Item.FindControl("txtCode")

                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")

                If txtName.Text = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Name');", True)
                    txtName.Focus()
                    Exit Sub
                End If

                If HasCode And txtCode.Text = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Code');", True)
                    txtCode.Focus()
                    Exit Sub
                End If

                Dim SQL As String = "SELECT * FROM " & TableName & " WHERE Name='" & txtName.Text.Replace("'", "''") & "' AND ID<>" & lblID.Text
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This Name is already exists');", True)
                    txtName.Focus()
                    Exit Sub
                End If

                If HasCode Then
                    SQL = "SELECT * FROM " & TableName & " WHERE Code='" & txtCode.Text.Replace("'", "''") & "' AND ID<>" & lblID.Text
                    DA = New SqlDataAdapter(SQL, ConnStr)
                    DT = New DataTable
                    DA.Fill(DT)
                    If DT.Rows.Count > 0 Then
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This Code is already exists');", True)
                        txtCode.Focus()
                        Exit Sub
                    End If
                End If

                SQL = "SELECT * FROM " & TableName & " WHERE ID=" & lblID.Text
                DA = New SqlDataAdapter(SQL, ConnStr)
                DT = New DataTable
                DA.Fill(DT)

                Dim DR As DataRow
                If DT.Rows.Count = 0 Then
                    DR = DT.NewRow
                    DR("ID") = GetNewID()
                    lblID.Text = DR("ID")
                Else
                    DR = DT.Rows(0)
                End If
                DR("Name") = txtName.Text
                If HasCode Then DR("Code") = txtCode.Text
                If DT.Rows.Count = 0 Then DT.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)

                'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
                '--------------- Do After That -----------
                lblName.Text = txtName.Text.Replace(vbLf, "<br>")
                lblCode.Text = txtCode.Text
                Dim btnEdit As Button = e.Item.FindControl("btnEdit")
                trView.Visible = True
                trEdit.Visible = False
                If btnEdit.Attributes("Mode") = "Add" Then RaiseEvent AddFinished(Me, txtName.Text)

                btnEdit.Attributes("Mode") = "Edit"
            Case "Cancel"
                Dim btnEdit As Button = e.Item.FindControl("btnEdit")
                Select Case btnEdit.Attributes("Mode")
                    Case "Edit"
                        Dim lblName As LinkButton = e.Item.FindControl("lblName")
                        Dim lblCode As LinkButton = e.Item.FindControl("lblCode")
                        Dim txtName As TextBox = e.Item.FindControl("txtName")
                        Dim txtCode As TextBox = e.Item.FindControl("txtCode")
                        Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                        Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                        '-------------- Reset Value-----------
                        txtName.Text = lblName.Text.Replace("<br>", vbLf)
                        txtCode.Text = lblCode.Text
                        trView.Visible = True
                        trEdit.Visible = False
                    Case "Add"
                        Dim DT As DataTable = CurrentDatasource()
                        DT.Rows(e.Item.ItemIndex).Delete()
                        DT.AcceptChanges()
                        rptMaster.DataSource = DT
                        rptMaster.DataBind()
                End Select
            Case "Default"

                Dim TheID As Integer = CType(e.Item.FindControl("lblID"), Label).Text
                Dim btnDefault As ImageButton = e.Item.FindControl("btnDefault")

                Dim SetToDefault As Boolean = btnDefault.ImageUrl = "images/none.png"

                Dim Comm As New SqlCommand
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    If SetToDefault Then
                        .CommandText = "UPDATE " & TableName & " SET IsDefault=CASE ID WHEN " & TheID & " THEN 1 ELSE 0 END "
                    Else
                        .CommandText = "UPDATE " & TableName & " SET IsDefault=0 "
                    End If
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()

                For Each Item As RepeaterItem In rptMaster.Items
                    If Item.ItemType <> ListItemType.Item And Item.ItemType <> ListItemType.AlternatingItem Then Continue For
                    Dim lblID As Label = Item.FindControl("lblID")
                    Dim _btnDefault As ImageButton = Item.FindControl("btnDefault")

                    If SetToDefault And lblID.Text = TheID Then
                        _btnDefault.ImageUrl = "images/arrow_blue_left.png"
                    Else
                        _btnDefault.ImageUrl = "images/none.png"
                    End If
                Next

        End Select
    End Sub


    Protected Sub rptMaster_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMaster.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblID As Label = e.Item.FindControl("lblID")
        Dim lblName As LinkButton = e.Item.FindControl("lblName")
        Dim lblCode As LinkButton = e.Item.FindControl("lblCode")

        Dim txtName As TextBox = e.Item.FindControl("txtName")
        Dim txtCode As TextBox = e.Item.FindControl("txtCode")

        Dim btnDefault As ImageButton = e.Item.FindControl("btnDefault")

        Dim btnEdit As Button = e.Item.FindControl("btnEdit")
        Dim btnDelete As Button = e.Item.FindControl("btnDelete")
        Dim btnSave As Button = e.Item.FindControl("btnSave")
        Dim btnCancel As Button = e.Item.FindControl("btnCancel")

        Dim trView As HtmlTableRow = e.Item.FindControl("trView")
        Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")

        Dim codeDetail1 As HtmlTableCell = e.Item.FindControl("codeDetail1")
        Dim codeDetail2 As HtmlTableCell = e.Item.FindControl("codeDetail2")

        lblID.Text = e.Item.DataItem("ID")
        lblName.Text = e.Item.DataItem("Name").ToString.Replace(vbLf, "<br>")
        txtName.Text = e.Item.DataItem("Name").ToString.Replace("<br>", vbLf)
        If HasCode Then
            lblCode.Text = e.Item.DataItem("Code").ToString
            txtCode.Text = e.Item.DataItem("Code").ToString
        End If
        If HasCode Then
            codeDetail1.Style("display") = ""
            codeDetail2.Style("display") = ""
        Else
            codeDetail1.Style("display") = "none"
            codeDetail2.Style("display") = "none"
        End If

        If Not IsDBNull(e.Item.DataItem("IsDefault")) AndAlso e.Item.DataItem("IsDefault") Then
            btnDefault.ImageUrl = "images/arrow_blue_left.png"
        Else
            btnDefault.ImageUrl = "images/none.png"
        End If

        '--------------- เปิด ปิด ฟังก์ชั่น Edit -----------
        trView.Visible = e.Item.DataItem("view_mode") = "View"
        trEdit.Visible = e.Item.DataItem("view_mode") = "Edit"

        btnEdit.Attributes("Mode") = e.Item.DataItem("edit_mode")

    End Sub

    Private Function CurrentDatasource() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("ID", GetType(Integer))
        DT.Columns.Add("Name", GetType(String))
        If HasCode Then
            DT.Columns.Add("Code", GetType(String))
        End If
        DT.Columns.Add("IsDefault", GetType(Boolean))
        DT.Columns.Add("edit_mode", GetType(String))
        DT.Columns.Add("view_mode", GetType(String))

        For Each Item As RepeaterItem In rptMaster.Items
            If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For

            Dim lblID As Label = Item.FindControl("lblID")
            Dim lblName As LinkButton = Item.FindControl("lblName")
            Dim lblCode As LinkButton = Item.FindControl("lblCode")
            Dim txtName As TextBox = Item.FindControl("txtName")
            Dim txtCode As TextBox = Item.FindControl("txtCode")
            Dim btnDefault As ImageButton = Item.FindControl("btnDefault")

            Dim DR As DataRow = DT.NewRow
            DR("ID") = lblID.Text
            DR("Name") = txtName.Text
            If HasCode Then
                DR("Code") = txtCode.Text
            End If
            DR("IsDefault") = btnDefault.ImageUrl = "images/arrow_blue_left.png"

            Dim trView As HtmlTableRow = Item.FindControl("trView")
            Dim trEdit As HtmlTableRow = Item.FindControl("trEdit")
            If trView.Visible Then
                DR("view_mode") = "View"
            Else
                DR("view_mode") = "Edit"
            End If

            Dim btnEdit As Button = Item.FindControl("btnEdit")
            DR("edit_mode") = btnEdit.Attributes("Mode")
            DT.Rows.Add(DR)
        Next

        Return DT
    End Function

    Private Function GetNewID() As Integer
        Dim SQL As String = "SELECT ISNULL(MAX(ID),0)+1 FROM " & TableName
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = CurrentDatasource()
        Dim DR As DataRow = DT.NewRow
        DR("ID") = 0
        DR("Name") = ""
        If HasCode Then DR("Code") = ""
        DR("edit_mode") = "Add"
        DR("view_mode") = "Edit"
        DT.Rows.Add(DR)

        rptMaster.DataSource = DT
        rptMaster.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'btnClose
            Mask.Attributes("onclick") = "document.getElementById('" & btnClose.ClientID & "').click();"
        End If
    End Sub

    Public Function GetMasterCode(ByVal TableName As String, ByVal Name As String) As String
        Dim SQL As String = "SELECT Code FROM " & TableName & " WHERE Name='" & Name.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Return ""
        Else
            Return DT.Rows(0).Item("Code").ToString
        End If
    End Function

    Public Function GetMasterName(ByVal TableName As String, ByVal Code As String) As String
        Dim SQL As String = "SELECT Name FROM " & TableName & " WHERE Code='" & Code.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            Return ""
        Else
            Return DT.Rows(0).Item("Name").ToString
        End If
    End Function

End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="JDA_Upload.aspx.vb" Inherits="JDA_Upload" %>

<head runat="server">
<link href="style/css.css" rel="stylesheet" type="text/css" />
<script src="script/jquery-1.10.1.min.js" type="text/javascript"></script>
</head>
<body style="">
    <form id="form1" runat="server" style="margin:0px 0px 0px 0px;">
        <input type="button" id="btnFake" value="Upload file" class="Button_Red" />
        <asp:FileUpload ID="ful" runat="server"  style="display:none;" />
        <asp:Button ID="btnUpload" runat="server" style="display:none;" />
        <asp:TextBox ID="txtP" runat="server" style="display:none;"></asp:TextBox>
        <asp:TextBox ID="txtB" runat="server" style="display:none;"></asp:TextBox>
    </form>
    <script language="javascript" type="text/javascript">
        $("#btnFake").click(function(){
         $("#ful").click();
        });
    </script>
</body>
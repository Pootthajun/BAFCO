﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Search_Detail
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib

    Public Property JDAType() As String
        Get
            Try
                Return ViewState("JDAType")
            Catch ex As Exception
                Return ""
            End Try
        End Get
        Set(ByVal value As String)
            ViewState("JDAType") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtOpenerText1.Text = Request.QueryString("txt1")
            txtOpenerText2.Text = Request.QueryString("txt2")
            JDAType = Request.QueryString("JDAType")
            BindData()
        End If
    End Sub

    Private Sub BindData()

        Dim SQL As String = ""
        Select Case JDAType
            Case "1", "3"
                SQL = " SELECT DISTINCT top 1000 Description,CodeNo" & vbLf
                SQL &= " FROM " & vbLf
                SQL &= " (SELECT DISTINCT JDA1_33Description_Goods Description" & vbLf
                SQL &= " ,JDA1_34CodeNo CodeNo " & vbLf
                SQL &= " FROM tb_JDA_1_Detail " & vbLf
                SQL &= " WHERE ISNULL(JDA1_33Description_Goods,'')<>'' AND ISNULL(JDA1_34CodeNo,'')<>''  " & vbCrLf
                SQL &= " and(ISNULL(JDA1_33Description_Goods,'') LIKE '%" & txtSearch.Text.Trim().Replace("'", "''") & "%' or ISNULL(JDA1_34CodeNo,'') LIKE '%" & txtSearch.Text.Trim().Replace("'", "''") & "%') " & vbLf
                SQL &= " UNION ALL" & vbLf
                SQL &= "SELECT DISTINCT JDA3_33Description_Goods Description" & vbLf
                SQL &= " ,JDA3_34CodeNo CodeNo " & vbLf
                SQL &= " FROM tb_JDA_3_Detail" & vbLf
                SQL &= " WHERE ISNULL(JDA3_33Description_Goods,'')<>'' AND ISNULL(JDA3_34CodeNo,'')<>''" & vbLf
                SQL &= " and(ISNULL(JDA3_33Description_Goods,'') LIKE '%" & txtSearch.Text.Trim().Replace("'", "''") & "%' or ISNULL(JDA3_34CodeNo,'') LIKE '%" & txtSearch.Text.Trim().Replace("'", "''") & "%') " & vbLf
                SQL &= " ) JDA " & vbLf
                SQL &= " ORDER BY Description,CodeNo" & vbLf
                'SQL = " select * from ("
                'SQL &= " SELECT  DISTINCT top 2000 ID,Description,CodeNo"
                'SQL &= " FROM"
                'SQL &= " (SELECT DISTINCT JDA1_33Description_Goods Description"
                'SQL &= " ,JDA1_34CodeNo CodeNo,JDA1_Detail_ID ID"
                'SQL &= "  FROM tb_JDA_1_Detail"
                'SQL &= " WHERE ISNULL(JDA1_33Description_Goods,'')<>'' AND ISNULL(JDA1_34CodeNo,'')<>''"
                'SQL &= "  UNION ALL"
                'SQL &= " SELECT DISTINCT JDA3_33Description_Goods Description"
                'SQL &= " ,JDA3_34CodeNo CodeNo ,JDA3_Detail_ID ID"
                'SQL &= "  FROM tb_JDA_3_Detail "
                'SQL &= " WHERE ISNULL(JDA3_33Description_Goods,'')<>'' AND ISNULL(JDA3_34CodeNo,'')<>''"
                'SQL &= " ) JDA "
                'SQL &= " ORDER BY ID desc"
                'SQL &= " ) T order by Description,CodeNo"
            Case Else
                Exit Sub
        End Select

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim Result As String = ""
        For i As Integer = 0 To DT.Rows.Count - 1
            Result &= "<tr><td id='cell_" & (i + 1) & "_1' onClick=""sendBack('cell_" & (i + 1) & "_1','cell_" & (i + 1) & "_2');"">" & DT.Rows(i).Item("Description").ToString & "</td>"
            Result &= "<td  id='cell_" & (i + 1) & "_2' onClick=""sendBack('cell_" & (i + 1) & "_1','cell_" & (i + 1) & "_2');"">" & DT.Rows(i).Item("CodeNo").ToString & "</td></tr>" & vbLf
        Next
        tbodyItemList.InnerHtml = Result

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSearch.Click
        BindData()
    End Sub
End Class

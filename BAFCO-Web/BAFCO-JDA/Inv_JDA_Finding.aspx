﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Inv_JDA_Finding.aspx.vb" Inherits="Inv_JDA_Finding" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Invoice Finding</title>
    <link href="style/css.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="Script/script.js"></script>
    <style type="text/css">
            .NormalText .itemrow td
            {
            	cursor:pointer; background-color:#FFFFFF;
            	}
            	
             .NormalText .itemrow:hover td
             {
             	background-color:#DAE7FC;
             	}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManagerMain" runat="Server"></asp:ScriptManager>
        <table width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
            <tr>
                
                <td height="40" align="center" class="Master_Header_Text">
                    Invoice Finding
                </td>                
            </tr>
        </table>

        <asp:Panel ID="pnlSearch" runat="server" style="height:100%; width:100%;">
            <asp:UpdatePanel ID="udp1" runat="server" >
                <ContentTemplate>
                    <table width="430" cellpadding="0" cellspacing="1" style="margin: 10px 0px 0px 0px;" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="30" class="Grid_Header">
                                No
                            </td>
                            <td width="350" class="Grid_Header">
                                Invoice
                            </td>
                            <td width="50" class="Grid_Header">
                                Item(s)
                            </td>
                        </tr>
                        <tr>
                            <td height="25px"></td>
                            <td height="25px">
                                <asp:TextBox ID="txtInv" runat="server" Width="100%" CssClass="Textbox_Form_White" AutoPostBack="true"></asp:TextBox>
                            </td>
                            <td height="25px"></td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trRow" class="itemrow" runat="server" style="border-bottom:solid 1px #efefef;">
                                    <td class="Grid_Detail" align="center" >
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblInv" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" align="center" >
                                        <asp:Label ID="lblItem" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>     
                    <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="5" PageSize="10"/>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </form>
</body>
</html>
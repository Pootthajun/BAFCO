﻿
Imports System.Data

Partial Class Report_EXCEL_JDA1
    Inherits System.Web.UI.Page

    Dim C As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '---------------- Check Permission-------------
        If IsNothing(Session("IS_JDA_1")) OrElse Not Session("IS_JDA_1") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        End If

        If IsNothing(Session("Export_JDA1")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Timout.\nPlease click to export again'); window.location.href='Export_JDA_1.aspx';", True)
            Exit Sub
        End If

        Dim DT As DataTable = CType(Session("Export_JDA1"), DataTable).Copy
        For i As Integer = 0 To DT.Columns.Count - 1
            DT.Columns(i).ColumnName = Replace(DT.Columns(i).ColumnName, "JDA1_", "")
            DT.Columns(i).ColumnName = Replace(DT.Columns(i).ColumnName, "DA1_42", "42")
            If DT.Columns.Count = 70 Then
                Select Case i
                    Case 1
                        DT.Columns(i).ColumnName = "BAFCO Job No."
                    Case 2
                        DT.Columns(i).ColumnName = "Client Reference No."
                    Case 3
                        DT.Columns(i).ColumnName = "1.Consignor/Export (Name and Address)"
                    Case 4
                        DT.Columns(i).ColumnName = "2.Consignee/Importer (Code)"
                    Case 5
                        DT.Columns(i).ColumnName = "2.Consignee/Importer (Name and Address)"
                    Case 6
                        DT.Columns(i).ColumnName = "3.Code of Authorised Agent"
                    Case 7
                        DT.Columns(i).ColumnName = "3.Name and Address of Authorised Agent"
                    Case 8
                        DT.Columns(i).ColumnName = "4.Mode of Transport"
                    Case 9
                        DT.Columns(i).ColumnName = "5.Date of Import"
                    Case 10
                        DT.Columns(i).ColumnName = "6.No./Name of Vessel/Flight/Conveyance"
                    Case 11
                        DT.Columns(i).ColumnName = "7.Port/Place of Import Code"
                    Case 12
                        DT.Columns(i).ColumnName = "7.Port/Place of Import"
                    Case 13
                        DT.Columns(i).ColumnName = "8.Port/Place of Loading Code"
                    Case 14
                        DT.Columns(i).ColumnName = "8.Port/Place of Loading"
                    Case 15
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only) Code"
                    Case 16
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only)"
                    Case 17
                        DT.Columns(i).ColumnName = "10.Others (Specify)"
                    Case 18
                        DT.Columns(i).ColumnName = "10.Date and Time of Receipt"
                    Case 19
                        DT.Columns(i).ColumnName = "11.Documents Attached Invoice"
                    Case 20
                        DT.Columns(i).ColumnName = "11.Documents Attached Bill of Lading"
                    Case 21
                        DT.Columns(i).ColumnName = "11.Documents Attached Insurance Certificate"
                    Case 22
                        DT.Columns(i).ColumnName = "11.Documents Attached Letter of Credit"
                    Case 23
                        DT.Columns(i).ColumnName = "11.Documents Attached Others"
                    Case 24
                        DT.Columns(i).ColumnName = "12.Registration Number"
                    Case 25
                        DT.Columns(i).ColumnName = "13.Name of Customs Office"
                    Case 26
                        DT.Columns(i).ColumnName = "13.Name of Customs Office Code"
                    Case 27
                        DT.Columns(i).ColumnName = "14.Manifest Registration No."
                    Case 28
                        DT.Columns(i).ColumnName = "15.Receipt of Duty/Tax as levied authorised by"
                    Case 29
                        DT.Columns(i).ColumnName = "15.Receipt of Duty/Tax as levied authorised date"
                    Case 30
                        DT.Columns(i).ColumnName = "16.Import Permit No."
                    Case 31
                        DT.Columns(i).ColumnName = "17.Exchange Control Ref."
                    Case 32
                        DT.Columns(i).ColumnName = "18.Special Treatment"
                    Case 33
                        DT.Columns(i).ColumnName = "19.Bill of Landing or Consignment Note No."
                    Case 34
                        DT.Columns(i).ColumnName = "20.Terms of Delivery and Payment"
                    Case 35
                        DT.Columns(i).ColumnName = "21.Country to which payment of Goods to be made"
                    Case 36
                        DT.Columns(i).ColumnName = "21.Country to which payment of Goods to be made Code"
                    Case 37
                        DT.Columns(i).ColumnName = "22.Exchange Rate Currency"
                    Case 38
                        DT.Columns(i).ColumnName = "22.Exchange Rate"
                    Case 39
                        DT.Columns(i).ColumnName = "23.FOB Value"
                    Case 40
                        DT.Columns(i).ColumnName = "24.Insurance"
                    Case 41
                        DT.Columns(i).ColumnName = "25.Freight"
                    Case 42
                        DT.Columns(i).ColumnName = "26.CIF Value"
                    Case 43
                        DT.Columns(i).ColumnName = "27.Gross Wt.(Kg.) Unit"
                    Case 44
                        DT.Columns(i).ColumnName = "27.Gross Wt.(Kg.)"
                    Case 45
                        DT.Columns(i).ColumnName = "28.Measurement (m3)"
                    Case 46
                        DT.Columns(i).ColumnName = "29.Other Charges Currency"
                    Case 47
                        DT.Columns(i).ColumnName = "29.Other Charges"
                    Case 48
                        DT.Columns(i).ColumnName = "30.Marks and Numbers"
                    Case 49
                        DT.Columns(i).ColumnName = "48.Name of Declarant"
                    Case 50
                        DT.Columns(i).ColumnName = "49.Identity Card/Passport No."
                    Case 51
                        DT.Columns(i).ColumnName = "50.Status"
                    Case 52
                        DT.Columns(i).ColumnName = "51.I certify that this declaration is true and complete date"
                    Case 53
                        DT.Columns(i).ColumnName = "51.I certify that this declaration is true and complete"
                    Case 54
                        DT.Columns(i).ColumnName = "52.Removal from Customs control authorised date"
                    Case 55
                        DT.Columns(i).ColumnName = "52.Removal from Customs control authorised by"
                    Case 56
                        DT.Columns(i).ColumnName = "53.Total Duty/Tax Payable"
                    Case 57
                        DT.Columns(i).ColumnName = "54.Other Charges"
                    Case 58
                        DT.Columns(i).ColumnName = "55.Total Amount Payable"
                    Case 59
                        DT.Columns(i).ColumnName = "56.Manuscript Reccipt No.(if applicable)"
                    Case 60
                        DT.Columns(i).ColumnName = "56.Manuscript Reccipt No.(if applicable) date"
                    Case 61
                        DT.Columns(i).ColumnName = "45.Currency"
                    Case 62
                        DT.Columns(i).ColumnName = "45.Absolute Total Value"
                    Case 63
                        DT.Columns(i).ColumnName = "45.Absolute Total Value "
                    Case 64
                        DT.Columns(i).ColumnName = "46.Absolute Total Import Duty"
                    Case 65
                        DT.Columns(i).ColumnName = "47.Absolute Total Tax Payable"
                    Case 66
                        DT.Columns(i).ColumnName = "Total Set"
                    Case 67
                        DT.Columns(i).ColumnName = "Invoice No."
                    Case 68
                        DT.Columns(i).ColumnName = "Create Date"
                    Case 69
                        DT.Columns(i).ColumnName = "System ID"
                End Select
            Else
                Select Case i
                    Case 1
                        DT.Columns(i).ColumnName = "BAFCO Job No."
                    Case 2
                        DT.Columns(i).ColumnName = "Client Reference No."
                    Case 3
                        DT.Columns(i).ColumnName = "1.Consignor/Export (Name and Address)"
                    Case 4
                        DT.Columns(i).ColumnName = "2.Consignee/Importer (Code)"
                    Case 5
                        DT.Columns(i).ColumnName = "2.Consignee/Importer (Name and Address)"
                    Case 6
                        DT.Columns(i).ColumnName = "3.Code of Authorised Agent"
                    Case 7
                        DT.Columns(i).ColumnName = "3.Name and Address of Authorised Agent"
                    Case 8
                        DT.Columns(i).ColumnName = "4.Mode of Transport"
                    Case 9
                        DT.Columns(i).ColumnName = "5.Date of Import"
                    Case 10
                        DT.Columns(i).ColumnName = "6.No./Name of Vessel/Flight/Conveyance"
                    Case 11
                        DT.Columns(i).ColumnName = "7.Port/Place of Import Code"
                    Case 12
                        DT.Columns(i).ColumnName = "7.Port/Place of Import"
                    Case 13
                        DT.Columns(i).ColumnName = "8.Port/Place of Loading Code"
                    Case 14
                        DT.Columns(i).ColumnName = "8.Port/Place of Loading"
                    Case 15
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only) Code"
                    Case 16
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only)"
                    Case 17
                        DT.Columns(i).ColumnName = "12.Registration Number"
                    Case 18
                        DT.Columns(i).ColumnName = "16.Import Permit No."
                    Case 19
                        DT.Columns(i).ColumnName = "17.Exchange Control Ref."
                    Case 20
                        DT.Columns(i).ColumnName = "18.Special Treatment"
                    Case 21
                        DT.Columns(i).ColumnName = "19.Bill of Landing or Consignment Note No."
                    Case 22
                        DT.Columns(i).ColumnName = "20.Terms of Delivery and Payment"
                    Case 23
                        DT.Columns(i).ColumnName = "21.Country to which payment of Goods to be made"
                    Case 24
                        DT.Columns(i).ColumnName = "21.Country to which payment of Goods to be made Code"
                    Case 25
                        DT.Columns(i).ColumnName = "22.Exchange Rate Currency"
                    Case 26
                        DT.Columns(i).ColumnName = "22.Exchange Rate"
                    Case 27
                        DT.Columns(i).ColumnName = "23.FOB Value"
                    Case 28
                        DT.Columns(i).ColumnName = "24.Insurance"
                    Case 29
                        DT.Columns(i).ColumnName = "25.Freight"
                    Case 30
                        DT.Columns(i).ColumnName = "26.CIF Value"
                    Case 31
                        DT.Columns(i).ColumnName = "27.Gross Wt.(Kg.) Unit"
                    Case 32
                        DT.Columns(i).ColumnName = "27.Gross Wt.(Kg.)"
                    Case 33
                        DT.Columns(i).ColumnName = "28.Measurement (m3)"
                    Case 34
                        DT.Columns(i).ColumnName = "29.Other Charges Currency"
                    Case 35
                        DT.Columns(i).ColumnName = "29.Other Charges"
                    Case 36
                        DT.Columns(i).ColumnName = "30.Marks and Numbers"
                    Case 37
                        DT.Columns(i).ColumnName = "48.Name of Declarant"
                    Case 38
                        DT.Columns(i).ColumnName = "49.Identity Card/Passport No."
                    Case 39
                        DT.Columns(i).ColumnName = "50.Status"
                    Case 40
                        DT.Columns(i).ColumnName = "51.I certify that this declaration is true and complete date"
                    Case 41
                        DT.Columns(i).ColumnName = "51.I certify that this declaration is true and complete"
                    Case 42
                        DT.Columns(i).ColumnName = "45.Currency"
                    Case 43
                        DT.Columns(i).ColumnName = "45.Absolute Total Value"
                    Case 44
                        DT.Columns(i).ColumnName = "45.Absolute Total Value "
                    Case 45
                        DT.Columns(i).ColumnName = "46.Absolute Total Import Duty"
                    Case 46
                        DT.Columns(i).ColumnName = "47.Absolute Total Tax Payable"
                    Case 47
                        DT.Columns(i).ColumnName = "Total Set"
                    Case 48
                        DT.Columns(i).ColumnName = "Invoice No."
                    Case 49
                        DT.Columns(i).ColumnName = "Create Date"
                    Case 50
                        DT.Columns(i).ColumnName = "31.Item No."
                    Case 51
                        DT.Columns(i).ColumnName = "33.Description of Goods"
                    Case 52
                        DT.Columns(i).ColumnName = "34.Code No."
                    Case 53
                        DT.Columns(i).ColumnName = "35.Unit"
                    Case 54
                        DT.Columns(i).ColumnName = "36.Country of Origin Code"
                    Case 55
                        DT.Columns(i).ColumnName = "37.Qty based on Customs Tariff Unit"
                    Case 56
                        DT.Columns(i).ColumnName = "38.Per Unit"
                    Case 57
                        DT.Columns(i).ColumnName = "39.Total Value"
                    Case 58
                        DT.Columns(i).ColumnName = "39.Total Value "
                    Case 59
                        DT.Columns(i).ColumnName = "40.Rate"
                    Case 60
                        DT.Columns(i).ColumnName = "41.Total Import Duty"
                    Case 61
                        DT.Columns(i).ColumnName = "42.Type"
                    Case 62
                        DT.Columns(i).ColumnName = "43.Rate"
                    Case 63
                        DT.Columns(i).ColumnName = "44.Total Tax Payable"
                    Case 64
                        DT.Columns(i).ColumnName = "System ID"
                End Select
            End If
           
        Next
        '---------------ปรับรูปแบบข้อมูล --------------------
        Dim NewDT As New DataTable
        For j As Integer = 0 To DT.Columns.Count - 1
            NewDT.Columns.Add(DT.Columns(j).ColumnName)
            'Response.Write(DT.Columns(j).DataType.ToString & "<br>")
        Next

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim DR As DataRow = NewDT.NewRow

            For j As Integer = 0 To DT.Columns.Count - 1
                Select Case DT.Columns(j).DataType.ToString
                    Case "System.Int32", "System.String"
                        If IsNumeric(DT.Rows(i).Item(j).ToString) Then
                            DR(j) = "'" & DT.Rows(i).Item(j).ToString
                        Else
                            DR(j) = DT.Rows(i).Item(j).ToString
                        End If

                    Case "System.DateTime"
                        If Not IsDBNull(DT.Rows(i).Item(j)) Then
                            DR(j) = " " & C.DateToString(DT.Rows(i).Item(j), "dd-MMM-yyyy")
                        End If
                    Case "System.Boolean"
                        If Not IsDBNull(DT.Rows(i).Item(j)) Then
                            If DT.Rows(i).Item(j) Then
                                DR(j) = "yes"
                            Else
                                DR(j) = "no"
                            End If
                        Else
                            DR(j) = ""
                        End If
                    Case "System.Decimal"
                        If Not IsDBNull(DT.Rows(i).Item(j)) Then
                            DR(j) = " " & FormatNumber(DT.Rows(i).Item(j))
                        End If
                    Case Else

                End Select
            Next

            NewDT.Rows.Add(DR)
        Next
        DT = NewDT

        Dim Filename As String = Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & ".xls"
        Dim gv As New DataGrid
        gv.DataSource = DT
        gv.DataBind()

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & Filename)
        Response.ContentType = "application/ms-excel"
        Response.ContentEncoding = System.Text.Encoding.Unicode
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())
        Dim sw As New System.IO.StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        gv.RenderControl(hw)
        Response.Write(sw.ToString())
        Response.End()
    End Sub

End Class

﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Export_JDA_3.aspx.vb" Inherits="Export_JDA_3" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="udp1" runat="server" >
     <ContentTemplate>
              
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">    
            <tr>
                <td valign="top">
                     <asp:Panel ID="pnlFilter" runat="server" DefaultButton="btnSearch">  
                         <table align="left" cellpadding="0" cellspacing="1">
                            <tr>
                                <td>
                                    <div class="Fieldset_Container" >
                                        <div class="Fieldset_Header"> <span style="background-color:white; padding-left:10px; padding-right:10px; font-weight:bold;">Filter</span> </div>
                                        <table width="950px" align="center" cellspacing="5px;" style="margin-left:10px; ">
                                          <tr>
                                              <td width="125" height="25" class="NormalTextBlack">
                                                  Invoice No</td>
                                              <td width="100" height="25">
                                                 <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                              </td>
                                              <td height="25" width="200" align="right" class="NormalTextBlack">Registration Number </td>
                                              <td height="25" ><asp:TextBox ID="txtRegistrationNumber" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox></td>
                                              <td width="150" height="25" align="center" class="NormalTextBlack">&nbsp;</td>
                                             <td height="25">&nbsp;</td>
                                              <td>&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td width="125" height="25" class="NormalTextBlack">Date of Despatch</td>
                                            <td width="150" height="25">
                                        	    <asp:TextBox ID="txtDateOfDespatch_From" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                      <Ajax:CalendarExtender ID="clDateOfImport_From" runat="server" 
                                                        Format="dd-MMM-yyyy"  BehaviorID="txtDateOfDespatch_From"
                                                        TargetControlID="txtDateOfDespatch_From"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="100" height="25" align="center">To</td>
                                            <td width="150" height="25" class="NormalTextBlack">
                                        	    <asp:TextBox ID="txtDateOfDespatch_To" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                      <Ajax:CalendarExtender ID="clDateOfImport_To" runat="server" 
                                                        Format="dd-MMM-yyyy"  BehaviorID="txtDateOfDespatch_To"
                                                        TargetControlID="txtDateOfDespatch_To"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="150" height="25" align="center">&nbsp;</td>
                                            <td height="25">&nbsp;</td>
                                            <td height="25">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td width="125" height="25" class="NormalTextBlack">Create Date</td>
                                            <td width="150" height="25">
                                        	    <asp:TextBox ID="txtCreateDate_From" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                      <Ajax:CalendarExtender ID="clCreateDate_From" runat="server" 
                                                        Format="dd-MMM-yyyy"  BehaviorID="txtCreateDate_From"
                                                        TargetControlID="txtCreateDate_From"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="100" height="25" align="center">To</td>
                                            <td width="150" height="25" class="NormalTextBlack">
                                        	    <asp:TextBox ID="txtCreateDate_To" runat="server" CssClass="Textbox_Form_White" width="145px" AutoPostBack="true" style="text-align:center;"></asp:TextBox>
                                                      <Ajax:CalendarExtender ID="clCreateDate_To" runat="server" 
                                                        Format="dd-MMM-yyyy"  BehaviorID="txtCreateDate_To"
                                                        TargetControlID="txtCreateDate_To"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="150" height="25" align="center"><asp:CheckBox ID="chkDetail" runat="server" Text="Export with item-detail" AutoPostBack="True" /></td>
                                            <td height="25">&nbsp;</td>
                                            <td height="25">&nbsp;</td>
                                          </tr>
                                      </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>  
                </td>
            </tr>
            <tr>
                <td height="20" valign="middle">
                    <table cellpadding="0" cellspacing="2" style="margin: 0px 0px 10px 10px; width:970px;">
                        <tr>
                            <td style="width:100px;">
                                <asp:Button ID="btnClear" runat="server" Text="All" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td style="width:100px;">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td style="width:100px;">
                                <input type="Button" onclick="window.location.href='Report_EXCEL_JDA3.aspx';" Value="Export to Excel" class="Button_Red" style="width:100px; height:30px;"/>
                            </td>
                            <td align="right" style="font-weight:bold;">
                              <asp:Label ID="lblFound" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="1" style="margin: 0px 0px 0px 10px; position:absolute; width:auto !important;" class="NormalTextBlack" bgColor="#cccccc">
                        <tr>
                            <td class="Grid_Header" width="30">
                                No
                            </td>
                            <td class="Grid_Header" width="250">
                                Consignor/Tax Payer
                            </td>
                            <td class="Grid_Header" width="250">
                                 Consignee/Importer
                            </td>
                            <td class="Grid_Header" width="100">
                                Date of Despatch
                            </td>
                            <td class="Grid_Header" width="200">
                                Registration Number
                            </td>
                            <td class="Grid_Header" width="200">
                                Invoice No
                            </td>
                            <td class="Grid_Header" width="100">
                                Create Date
                            </td>
                            
                            <td class="Grid_Header" id="tdItem" runat="server" width="30">
                                ItemNo.
                            </td>
                            <td class="Grid_Header" id="tdDesc" runat="server" width="200">
                                Description Of Goods
                            </td>
                            <td class="Grid_Header" id="tdCode" runat="server" width="150">
                                Code No
                            </td>
                            <td class="Grid_Header" id="tdUnit1" runat="server" width="50">
                                Per Unit
                            </td>
                            <td class="Grid_Header" id="tdTotal1" runat="server" width="100">
                                Total
                            </td>
                            <td class="Grid_Header" id="tdUnit2" runat="server" width="50">
                                Per Unit
                            </td>
                            <td class="Grid_Header" id="tdTotal2" runat="server" width="100">
                                Total
                            </td>
                            
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr style="border-bottom:solid 1px #efefef;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td class="Grid_Detail" align="center"  style=" vertical-align:top; ">
                                        <asp:LinkButton ID="lblNo" runat="server" CssClass="Grid_Link"></asp:LinkButton>
                                        <asp:LinkButton ID="lblJDA_ID" runat="server" visible="false" CssClass="Grid_Link"></asp:LinkButton>
                                    </td>
                                    <td class="Grid_Detail"  style="vertical-align:top; ">
                                        <asp:LinkButton ID="lblConsignorTax" runat="server" CssClass="Grid_Link"></asp:LinkButton>
                                    </td>
                                    <td class="Grid_Detail"  style="vertical-align:top;">
                                        <asp:LinkButton ID="lblConsigneeImporter" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                    </td>
                                    <td class="Grid_Detail" style=" vertical-align:top; text-align:center;">
                                        <asp:LinkButton ID="lblDate_Despatch" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                    </td>
                                    <td class="Grid_Detail"  style="vertical-align:top; text-align:center;">
                                        <asp:LinkButton ID="lblReg_Number" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                    </td>
                                    <td class="Grid_Detail" align="center"  style="vertical-align:top; text-align:center; ">
                                        <asp:LinkButton ID="lblInvoice_No" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                    </td> 
                                    <td class="Grid_Detail" align="center"  style="vertical-align:top; text-align:center; ">
                                        <asp:LinkButton ID="lblCreateDate" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                    </td>
                                    
                                        <td class="Grid_Detail" style="vertical-align:top; text-align:center;" id="tdItem" runat="server" visible="false">
                                            <asp:LinkButton ID="lblItemNo" runat="server" CssClass="Grid_Link" ></asp:LinkButton>
                                        </td>  
                                        <td class="Grid_Detail" style="vertical-align:top;" id="tdDesc" runat="server" visible="false">
                                            <asp:LinkButton ID="lblItemDesc" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                        </td>
                                         <td class="Grid_Detail" style="vertical-align:top;" id="tdCode" runat="server" visible="false">
                                            <asp:LinkButton ID="lblItemCode" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                        </td>
                                        <td class="Grid_Detail" style="vertical-align:top; text-align:center;"  id="tdUnit1" runat="server" visible="false">
                                            <asp:LinkButton ID="lblUnit1" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                        </td> 
                                        <td class="Grid_Detail" style="vertical-align:top; text-align:right;" id="tdTotal1" runat="server" visible="false">
                                            <asp:LinkButton ID="lblTotal1" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                        </td>
                                        <td class="Grid_Detail" style="vertical-align:top; text-align:center;" id="tdUnit2" runat="server" visible="false">
                                            <asp:LinkButton ID="lblUnit2" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                        </td> 
                                        <td class="Grid_Detail" style="vertical-align:top; text-align:right;" id="tdTotal2" runat="server" visible="false">
                                            <asp:LinkButton ID="lblTotal2" runat="server"  CssClass="Grid_Link"></asp:LinkButton>
                                        </td>
                                                                                                    
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div style="margin-left:10px">
                        <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="10" Visible="false"/>
                    </div>
                    <br />
                    
                </td>
            </tr>
        </table>
        
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


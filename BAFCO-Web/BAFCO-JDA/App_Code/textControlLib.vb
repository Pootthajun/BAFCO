﻿Imports Microsoft.VisualBasic
Imports System.Web.UI.WebControls
Imports System

Public Class textControlLib

    Dim GL As New GenericLib

    Public Sub ImplementJavaMoneyText(ByRef Obj As TextBox, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") = "this.value=formatmoney(this.value,'0','999999999999');"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaIntegerText(ByRef Obj As TextBox, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") = "this.value=formatinteger(this.value,'0','999999999999');"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaOnlyNumberText(ByRef Obj As TextBox, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") = "this.value=formatonlynumber(this.value);"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaFloatText(ByRef Obj As TextBox, ByVal DecimalPlace As UInteger, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") = "this.value=formatfloat(this.value,'0','999999999999'," & DecimalPlace & ");"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaFloatManualText(ByRef Obj As TextBox, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") = "this.value=formatfloatManualPlace(this.value,'0','999999999999');"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Function CalculateManualFloatText(ByVal Input As Double) As String
        Dim Result As String = ""
        If Input <> Math.Floor(Input) Then
            '----------- มีเศษ -------------
            Dim Sed As String = (Input - Math.Floor(Input).ToString.Replace("0.", "")).ToString.Replace(".", "")
            Dim DecPlace As Integer = Sed.Length
            Result = FormatNumber(Input, DecPlace)
            While Result.IndexOf(".") > -1 And (Right(Result, 1) = "0" Or Right(Result, 1) = ".")
                Result = Result.Substring(0, Result.Length - 1)
            End While
        Else
            Result = FormatNumber(Input, 0)
        End If
        Return Result
    End Function

End Class

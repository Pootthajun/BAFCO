﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Globalization
Imports System.Data
Imports System.Data.SqlClient
Imports System
Imports System.Text.RegularExpressions
Imports System.Configuration
Imports System.IO

Public Class GenericLib

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

#Region "Nested Type"

    Public Enum AllowFileType
        PDF = 1
        JPG = 2
        PNG = 3
    End Enum

    Public Structure FileStructure
        Public FileIndex As Integer
        Public AbsoluteName As String
        Public FileType As AllowFileType
        Public Content As Byte()
    End Structure

#End Region

    Public Function ReportProgrammingDate(ByVal Input As DateTime) As String
        If IsDate(Input) Then
            Return Input.Year & "-" & Input.Month.ToString.PadLeft(2, "0") & "-" & Input.Day.ToString.PadLeft(2, "0")
        End If
        Return ""
    End Function

    Public Function ReportProgrammingDate(ByVal Input As DateTime, ByVal Format As String) As String
        Dim Provider As CultureInfo = CultureInfo.GetCultureInfo("en-US")
        Return Input.ToString(Format, Provider)
    End Function

    Public Function ReportProgrammingDotDate(ByVal Input As DateTime) As String
        If IsDate(Input) Then
            Return Input.Day.ToString.PadLeft(2, "0") & "." & Input.Month.ToString.PadLeft(2, "0") & "." & Input.Year
        End If
        Return ""
    End Function

    Public Function IsProgrammingDate(ByVal Input As String) As Boolean
        Try
            Dim Temp As Date = DateTime.Parse(Input)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function IsProgrammingDate(ByVal Input As String, ByVal Format As String) As Boolean
        Try
            Dim Provider As CultureInfo = CultureInfo.GetCultureInfo("en-US")
            Dim Temp As DateTime = DateTime.ParseExact(Input, Format, Provider)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function


    Public Function IsFormatFileName(ByVal FileName As String) As Boolean
        Dim ExceptChars As String = "/\:*?""<>|;"
        For i As Integer = 1 To Len(ExceptChars)
            If InStr(FileName, Mid(ExceptChars, i)) > 0 Then
                Return False
            End If
        Next
        Return True
    End Function

    Public Function IsValidEmailFormat(ByVal input As String) As Boolean
        Return Regex.IsMatch(input, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$")
    End Function

    Public Function OriginalFileName(ByVal FullPath As String) As String
        Return FullPath.Substring(FullPath.LastIndexOf("\") + 1)
    End Function

    Public Function ReportMonthThai(ByVal MonthID As Integer) As String
        Select Case MonthID
            Case 1
                Return "มกราคม"
            Case 2
                Return "กุมภาพันธ์"
            Case 3
                Return "มีนาคม"
            Case 4
                Return "เมษายน"
            Case 5
                Return "พฤษภาคม"
            Case 6
                Return "มิถุนายน"
            Case 7
                Return "กรกฎาคม"
            Case 8
                Return "สิงหาคม"
            Case 9
                Return "กันยายน"
            Case 10
                Return "ตุลาคม"
            Case 11
                Return "พฤศจิกายน"
            Case 12
                Return "ธันวาคม"
            Case Else
                Return ""
        End Select
    End Function

    Public Function ReportMonthEnglish(ByVal MonthID As Integer) As String
        Select Case MonthID
            Case 1
                Return "January"
            Case 2
                Return "February"
            Case 3
                Return "March"
            Case 4
                Return "April"
            Case 5
                Return "May"
            Case 6
                Return "June"
            Case 7
                Return "July"
            Case 8
                Return "August"
            Case 9
                Return "September"
            Case 10
                Return "October"
            Case 11
                Return "November"
            Case 12
                Return "December"
            Case Else
                Return ""
        End Select
    End Function

    Public Function ReportShotMonthEnglish(ByVal MonthID As Integer) As String
        Select Case MonthID
            Case 1
                Return "Jan"
            Case 2
                Return "Feb"
            Case 3
                Return "Mar"
            Case 4
                Return "Apr"
            Case 5
                Return "May"
            Case 6
                Return "Jun"
            Case 7
                Return "Jul"
            Case 8
                Return "Aug"
            Case 9
                Return "Sep"
            Case 10
                Return "Oct"
            Case 11
                Return "Nov"
            Case 12
                Return "Dec"
            Case Else
                Return ""
        End Select
    End Function


    Public Function ReportMonthEnglishToNumber(ByVal DateFullName As Integer) As String


        Select Case DateFullName
            Case "January"
                Return 1
            Case "February"
                Return 2
            Case "March"
                Return 3
            Case "April"
                Return 4
            Case "May"
                Return 5
            Case "June"
                Return 6
            Case "July"
                Return 7
            Case "August"
                Return 8
            Case "September"
                Return 9
            Case "October"
                Return 10
            Case "November"
                Return 11
            Case "December"
                Return 12
            Case Else
                Return ""
        End Select
    End Function

    Private Function SendEmail(ByVal MailFrom As String, ByVal MailTo() As String, ByVal CC As String(), ByVal AttachFileName As String(), ByVal Subject As String, ByVal Body As String, ByVal IsHTML As Boolean, ByVal Host As String, Optional ByVal Port As Integer = 0, Optional ByVal UserName As String = "", Optional ByVal Password As String = "") As Boolean
        Try

            Dim smtp As SmtpClient = New SmtpClient(Host)

            Dim message As New MailMessage()
            message.From = New MailAddress(MailFrom)
            For i As Integer = 0 To MailTo.Length - 1
                message.To.Add(MailTo(i))
            Next
            For i As Integer = 0 To CC.Length - 1
                message.CC.Add(CC(i))
            Next
            For i As Integer = 0 To AttachFileName.Length - 1
                Dim Attach As New Net.Mail.Attachment(AttachFileName(i))
                message.Attachments.Add(Attach)
            Next

            message.Subject = Subject
            message.IsBodyHtml = IsHTML
            message.Body = Body


            smtp.Host = Host
            If Port <> 0 Then smtp.Port = Port
            If UserName <> "" Then smtp.Credentials = New System.Net.NetworkCredential(UserName, Password)

            smtp.Send(message)
            Return True

        Catch ex As Exception
            Return False

        End Try
    End Function

    Public Function GetImageContentType(ByVal SrcImg As System.Drawing.Image) As String
        Select Case SrcImg.RawFormat.Guid
            Case Drawing.Imaging.ImageFormat.Bmp.Guid
                Return "image/tiff"
            Case Drawing.Imaging.ImageFormat.Jpeg.Guid
                Return "image/jpeg"
            Case Drawing.Imaging.ImageFormat.Gif.Guid
                Return "image/gif"
            Case Drawing.Imaging.ImageFormat.Png.Guid
                Return "image/png"
            Case Else '------------- Default Format ---------------
                Return ""
        End Select
    End Function

    Public Sub ForceDeleteFolder(ByVal Path As String)
        On Error Resume Next
        ForceDeleteFileInFolder(Path)
        Directory.Delete(Path, True)
    End Sub

    Public Sub ForceDeleteFileInFolder(ByVal Path As String)
        On Error Resume Next
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(Path, FileIO.SearchOption.SearchAllSubDirectories)
            File.Delete(foundFile)
        Next
    End Sub

End Class

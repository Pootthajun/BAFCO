Imports System.IO
Imports System.Text
Imports System.Globalization
Imports System

Public Class Converter

    Public Enum EncodeType
        _DEFAULT = 0
        _ASCII = 1
        _UNICODE = 2
        _UTF32 = 3
        _UTF7 = 4
        _UTF8 = 5
    End Enum

    Public Function StreamToByte(ByVal Stream As System.IO.Stream) As Byte() ' Convert Specify Stream To Byte
        Dim Result() As Byte
        ReDim Result(Stream.Length - 1)
        Stream.Read(Result, 0, Stream.Length)
        StreamToByte = Result.Clone
    End Function

    Public Function ByteToStream(ByVal Buffer() As Byte) As System.IO.MemoryStream ' Convert Byte To Stream
        ByteToStream = New System.IO.MemoryStream(Buffer)
    End Function

    Public Function StringToByte(ByVal Str As String, Optional ByVal Encoding As Integer = EncodeType._DEFAULT) As Byte()
        Select Case Encoding
            Case EncodeType._ASCII
                Return System.Text.Encoding.ASCII.GetBytes(Str)
            Case EncodeType._UNICODE
                Return System.Text.Encoding.Unicode.GetBytes(Str)
            Case EncodeType._UTF32
                Return System.Text.Encoding.UTF32.GetBytes(Str)
            Case EncodeType._UTF7
                Return System.Text.Encoding.UTF7.GetBytes(Str)
            Case EncodeType._UTF8
                Return System.Text.Encoding.UTF8.GetBytes(Str)
            Case Else
                Return System.Text.Encoding.Default.GetBytes(Str)
        End Select
    End Function

    Public Function ByteToString(ByVal Buffer() As Byte, Optional ByVal Encoding As Integer = EncodeType._DEFAULT) As String
        System.Text.Encoding.Default.GetString(Buffer, 0, Buffer.Length)
        Select Case Encoding
            Case EncodeType._ASCII
                Return System.Text.Encoding.ASCII.GetString(Buffer, 0, Buffer.Length)
            Case EncodeType._UNICODE
                Return System.Text.Encoding.Unicode.GetString(Buffer, 0, Buffer.Length)
            Case EncodeType._UTF32
                Return System.Text.Encoding.UTF32.GetString(Buffer, 0, Buffer.Length)
            Case EncodeType._UTF7
                Return System.Text.Encoding.UTF7.GetString(Buffer, 0, Buffer.Length)
            Case EncodeType._UTF8
                Return System.Text.Encoding.UTF8.GetString(Buffer, 0, Buffer.Length)
            Case Else
                Return System.Text.Encoding.Default.GetString(Buffer, 0, Buffer.Length)
        End Select
    End Function

    Public Function ImageToByte(ByVal SrcImg As System.Drawing.Image) As Byte()
        Dim ms As New MemoryStream()
        Select Case SrcImg.RawFormat.Guid
            Case Drawing.Imaging.ImageFormat.Bmp.Guid
                SrcImg.Save(ms, Drawing.Imaging.ImageFormat.Bmp)
            Case Drawing.Imaging.ImageFormat.Jpeg.Guid
                SrcImg.Save(ms, Drawing.Imaging.ImageFormat.Jpeg)
            Case Drawing.Imaging.ImageFormat.Gif.Guid
                SrcImg.Save(ms, Drawing.Imaging.ImageFormat.Gif)
            Case Drawing.Imaging.ImageFormat.Png.Guid
                SrcImg.Save(ms, Drawing.Imaging.ImageFormat.Png)
            Case Else '------------- Default Format ---------------
                SrcImg.Save(ms, Drawing.Imaging.ImageFormat.Jpeg)
        End Select
        Return ms.ToArray()
    End Function

    Public Function StringToDate(ByVal InputString As String, ByVal Format As String) As DateTime
        Dim Provider As CultureInfo = CultureInfo.GetCultureInfo("en-US")
        Return DateTime.ParseExact(InputString, Format, Provider)
    End Function

    Public Function DateToString(ByVal InputDate As DateTime, ByVal Format As String) As String
        Return InputDate.ToString(Format)
    End Function
End Class

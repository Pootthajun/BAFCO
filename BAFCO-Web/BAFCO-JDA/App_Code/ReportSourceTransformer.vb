﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class ReportSourceTransformer

    Dim TC As New textControlLib

    Public Function TransformJDA1(ByVal HeaderTable As DataTable,ByVal DetailTable As DataTable) As DataTable
        'Dim Result As DataTable = HeaderTable.Copy
        Dim H As DataTable = HeaderTable.Copy '-------------------- HeaderTable ต้องส่งมา Record เดียว -------------
        Dim D As DataTable = DetailTable.Copy
        'D.DefaultView.Sort = "JDA1_ID,JDA1_Detail_ID"
        D = D.DefaultView.ToTable
        '-------------- Remove UnUsed Column-----------
        H.Columns.Remove("CREATE_BY")
        H.Columns.Remove("CREATE_DATE")
        H.Columns.Remove("UPDATE_BY")
        H.Columns.Remove("UPDATE_DATE")
        D.Columns.Remove("CREATE_BY")
        D.Columns.Remove("CREATE_DATE")
        D.Columns.Remove("UPDATE_BY")
        D.Columns.Remove("UPDATE_DATE")
        '----------------- Remove Duplicate Column Name -----------
        For i As Integer = D.Columns.Count - 1 To 0
            If H.Columns.IndexOf(D.Columns(i).ColumnName) > -1 Then
                D.Columns.RemoveAt(i)
            End If
        Next

        Dim Result As DataTable = H.Copy

        Result.Columns.Add("SUMTForPage_Unit", GetType(Double))
        Result.Columns.Add("SUMTForPage_THB", GetType(Double))
        'Result.Columns.Add("SUMTForPage_Amount_41", GetType(Double))
        'Result.Columns.Add("SUMTForPage_Amount_44", GetType(Double))
        Result.Columns.Add("SUMTForPage_Amount_41_New")
        Result.Columns.Add("SUMTForPage_Amount_44_New")

        Result.Rows.Clear() '----------------- Create Structure------------
        For i As Integer = 0 To D.Columns.Count - 1
            For j As Integer = 1 To 7
                Result.Columns.Add(D.Columns(i).ColumnName & "_" & j, D.Columns(i).DataType)
            Next
        Next
        '---------------- Calculate Page ------------
        Dim TotalPage As Integer = Math.Ceiling(D.Rows.Count / 7)
        If TotalPage = 0 Then TotalPage = 1

        For p As Integer = 1 To TotalPage
            Dim DR As DataRow = Result.NewRow
            '-------------- Insert Header Part ------------
            For i As Integer = 0 To H.Columns.Count - 1

                Dim FieldName As String = H.Columns(i).ColumnName
                If Not IsDBNull(H.Rows(0).Item(FieldName)) Then
                    DR(FieldName) = H.Rows(0).Item(FieldName)
                End If
            Next
            '------------- Insert Detail Part -------------
            For i As Integer = 1 To 7
                If D.Rows.Count = 0 Then Exit For
                For F As Integer = 0 To D.Columns.Count - 1
                    Dim FieldName As String = D.Columns(F).ColumnName
                    If Not IsDBNull(D.Rows(0).Item(FieldName)) Then
                        DR(FieldName & "_" & i) = D.Rows(0).Item(FieldName)
                    End If

                Next
                D.Rows(0).Delete()
                D.AcceptChanges()
            Next
            Result.Rows.Add(DR)
        Next


        Dim DT As New DataTable
        DT = Result
        '------ผลรวมท้ายตารางแต่ละ Page-----
        Dim SumForPageUnit As Double = 0
        Dim SumForPageTHB As Double = 0
        Dim SumForPageAmount_41 As Double = 0
        Dim SumForPageAmount_44 As Double = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            For j As Integer = 1 To 7

                If IsNumeric(DT.Rows(i).Item("JDA1_39Total_" & j)) Then
                    SumForPageUnit = FormatNumber(DT.Rows(i).Item("JDA1_39Total_" & j) + SumForPageUnit)
                End If
                If IsNumeric(DT.Rows(i).Item("JDA1_39Total_" & j)) Then
                    SumForPageTHB = FormatNumber((DT.Rows(i).Item("JDA1_39Total_THB_" & j)) + SumForPageTHB)
                End If
                If IsNumeric(DT.Rows(i).Item("JDA1_41Amount_" & j)) Then
                    SumForPageAmount_41 = FormatNumber((DT.Rows(i).Item("JDA1_41Amount_" & j)) + SumForPageAmount_41)
                End If
                If IsNumeric(DT.Rows(i).Item("JDA1_44Amount_" & j)) Then
                    SumForPageAmount_44 = FormatNumber((DT.Rows(i).Item("JDA1_44Amount_" & j)) + SumForPageAmount_44)
                End If
            Next
            DT.Rows(i).Item("SUMTForPage_Unit") = FormatNumber(SumForPageUnit)
            DT.Rows(i).Item("SUMTForPage_THB") = FormatNumber(SumForPageTHB)
            If i <> DT.Rows.Count - 1 Then
                DT.Rows(i).Item("SUMTForPage_Amount_41_New") = TC.CalculateManualFloatText(SumForPageAmount_41)
                DT.Rows(i).Item("SUMTForPage_Amount_44_New") = TC.CalculateManualFloatText(SumForPageAmount_44)
            Else
                If Not IsDBNull(DT.Rows(i).Item("JDA1_41_Amount")) Then
                    DT.Rows(i).Item("SUMTForPage_Amount_41_New") = TC.CalculateManualFloatText(DT.Rows(i).Item("JDA1_41_Amount"))
                End If
                If Not IsDBNull(DT.Rows(i).Item("JDA1_44_Amount")) Then
                    DT.Rows(i).Item("SUMTForPage_Amount_44_New") = TC.CalculateManualFloatText(DT.Rows(i).Item("JDA1_44_Amount"))
                End If
            End If
        Next

        '--------------- Change Column For Manual Key Import Duty-------------------
        For i As Integer = 1 To 7
            DT.Columns.Add("JDA1_41Amount_" & i & "_New")
            DT.Columns.Add("JDA1_43Rate_" & i & "_New")
            DT.Columns.Add("JDA1_44Amount_" & i & "_New")
        Next

        For r As Integer = 0 To DT.Rows.Count - 1
            For c As Integer = 1 To 7
                If Not IsDBNull(DT.Rows(r).Item("JDA1_41Amount_" & c)) Then
                    DT.Rows(r).Item("JDA1_41Amount_" & c & "_New") = TC.CalculateManualFloatText(DT.Rows(r).Item("JDA1_41Amount_" & c))
                End If
                If Not IsDBNull(DT.Rows(r).Item("JDA1_43Rate_" & c)) Then
                    DT.Rows(r).Item("JDA1_43Rate_" & c & "_New") = TC.CalculateManualFloatText(DT.Rows(r).Item("JDA1_43Rate_" & c))
                End If
                If Not IsDBNull(DT.Rows(r).Item("JDA1_44Amount_" & c)) Then
                    DT.Rows(r).Item("JDA1_44Amount_" & c & "_New") = TC.CalculateManualFloatText(DT.Rows(r).Item("JDA1_44Amount_" & c))
                End If
            Next
        Next


        Return Result
    End Function

    Public Function TransformJDA3(ByVal HeaderTable As DataTable, ByVal DetailTable As DataTable) As DataTable
        Dim H As DataTable = HeaderTable.Copy '-------------------- HeaderTable ต้องส่งมา Record เดียว -------------
        Dim D As DataTable = DetailTable.Copy
        'D.DefaultView.Sort = "JDA3_ID,JDA3_31Item_No"
        D = D.DefaultView.ToTable
        '-------------- Remove UnUsed Column-----------
        H.Columns.Remove("CREATE_BY")
        H.Columns.Remove("CREATE_DATE")
        H.Columns.Remove("UPDATE_BY")
        H.Columns.Remove("UPDATE_DATE")
        D.Columns.Remove("CREATE_BY")
        D.Columns.Remove("CREATE_DATE")
        D.Columns.Remove("UPDATE_BY")
        D.Columns.Remove("UPDATE_DATE")
        '----------------- Remove Duplicate Column Name -----------
        For i As Integer = D.Columns.Count - 1 To 0
            If H.Columns.IndexOf(D.Columns(i).ColumnName) > -1 Then
                D.Columns.RemoveAt(i)
            End If
        Next

        Dim Result As DataTable = H.Copy
        Result.Columns.Add("SUMTForPage_Unit", GetType(Double))
        Result.Columns.Add("SUMTForPage_THB", GetType(Double))

        Result.Rows.Clear() '----------------- Create Structure------------
        For i As Integer = 0 To D.Columns.Count - 1
            For j As Integer = 1 To 7
                Result.Columns.Add(D.Columns(i).ColumnName & "_" & j, D.Columns(i).DataType)
            Next
        Next

        '---------------- Calculate Page ------------
        Dim TotalPage As Integer = Math.Ceiling(D.Rows.Count / 7)
        If TotalPage = 0 Then TotalPage = 1

        For p As Integer = 1 To TotalPage
            Dim DR As DataRow = Result.NewRow
            '-------------- Insert Header Part ------------
            For i As Integer = 0 To H.Columns.Count - 1
                Dim FieldName As String = H.Columns(i).ColumnName
                If Not IsDBNull(H.Rows(0).Item(FieldName)) Then
                    DR(FieldName) = H.Rows(0).Item(FieldName)
                End If
            Next
            '------------- Insert Detail Part -------------
            For i As Integer = 1 To 7
                If D.Rows.Count = 0 Then Exit For
                For F As Integer = 0 To D.Columns.Count - 1
                    Dim FieldName As String = D.Columns(F).ColumnName
                    If Not IsDBNull(D.Rows(0).Item(FieldName)) Then
                        DR(FieldName & "_" & i) = D.Rows(0).Item(FieldName)
                    End If
                Next
                D.Rows(0).Delete()
                D.AcceptChanges()
            Next

            Result.Rows.Add(DR)
        Next

        Dim DT As New DataTable
        DT = Result
        '------ผลรวมท้ายตารางแต่ละ Page-----
        Dim SumForPageUnit As Double = 0
        Dim SumForPageTHB As Double = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            For j As Integer = 1 To 7
                If IsNumeric(DT.Rows(i).Item("JDA3_39Total_" & j)) Then
                    SumForPageUnit = FormatNumber(DT.Rows(i).Item("JDA3_39Total_" & j) + SumForPageUnit)
                End If
                If IsNumeric(DT.Rows(i).Item("JDA3_39Total_" & j)) Then
                    SumForPageTHB = FormatNumber((DT.Rows(i).Item("JDA3_39Total_" & j) * (DT.Rows(i).Item("JDA3_18AmountReceived"))) + SumForPageTHB)
                End If
            Next
            DT.Rows(i).Item("SUMTForPage_Unit") = FormatNumber(SumForPageUnit)
            DT.Rows(i).Item("SUMTForPage_THB") = FormatNumber(SumForPageTHB)
        Next


        Return Result
    End Function
End Class

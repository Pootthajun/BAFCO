﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="JDA_3.aspx.vb" Inherits="JDA_3"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="MasterEditor.ascx" tagname="MasterEditor" tagprefix="uc1" %>
<%@ Register src="MasterEditor_MultiLine.ascx" tagname="MasterEditor_MultiLine" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">   
 <style type="text/css">
    .MainTable{
        border-right: 1px solid #f96;
		border-bottom: 1px solid #f96;
    }
    .MainTable td{
		border-left: 1px solid #f96;
		border-top: 1px solid #f96;
		}
 </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >

    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		     <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; ">
		        <tr>
		            <td colspan="2" style="font-size: 16px; font-weight:bold; height:30px; color:White; padding-left:10px;" align="center" valign="middle">
		                CUSTOMS DECLARATION FROM FOR INTERNAL MOVEMENT OF GOODS (JDA3)
		                <asp:Label ID="lblJDA3_ID" runat="server"  style="display:none;"></asp:Label> 
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 14px; height:40px; color:White; padding-left:10px;" align="left" valign="middle">		            
		                BAFCO Job No. <asp:TextBox ID="txt_BAFCO_Job_No" runat="server" MaxLength="100" style="text-align:center; font-weight:bold;"></asp:TextBox>
		            </td>
		            <td style="font-size: 14px; height:40px; color:White;  padding-right:10px;" align="right" valign="middle">		            
		                Client Reference No. <asp:TextBox ID="txt_Client_Reference_No" runat="server" MaxLength="100" style="text-align:center; font-weight:bold;"></asp:TextBox>
		            </td>
		        </tr>
		    </table>
		   
		   <asp:Panel ID="pnlForm" runat="server" DefaultButton="btnCalculate">
            <asp:Button ID="btnCalculate" runat="server" style="display:none;" />
        
            <table width="1000" border="0" cellpadding="2" cellspacing="0" class="MainTable">
              <tr>
                <td colspan="2" valign="top">1. Consignor (Name and Address) </td>
                <td colspan="2" valign="top" style="border-bottom:1px solid #F96;">
                <asp:TextBox ID="txtJDA3_1Consignor_Code" runat="server" CssClass="StrechControl"></asp:TextBox>                                         
                </td>
                <td colspan="3" align="center" style=" font-weight:bold; font-size:14px; border-top-width:2px; border-left-width:3px; border-right:2px solid #F96;">FOR OFFICIAL USE</td>
              </tr>
              <tr>
                <td colspan="4" rowspan="3" valign="top" style="border-top:none;">
                
                            <div class="wrapper" style="height:110px; width:440px; z-index:20;">
		                        <div class="accordionButton StrechControl">
    		                    	<asp:TextBox ID="txtJDA3_1Consignor" runat="server" CssClass="StrechControl" 
    		                    	TextMode="MultiLine" Height="125px" ></asp:TextBox>
    		                    </div>
		                        <div class="accordionContent">		                        
		                            <asp:Repeater ID="lst01" runat="server" >
		                                <ItemTemplate>
		                                    <div ID="divItem" runat="server">    			                                
		                                    </div>
		                                </ItemTemplate>
		                            </asp:Repeater>		                            
		                        </div>
	                        </div>
                </td>
                <td height="25" colspan="2" style="border-left-width:3px;">10. Date and Time of Receipt </td>
                <td style="border-right:2px solid #F96;">11. Documents Attached </td>
              </tr>
              <tr>
                <td height="25" colspan="2"style="border-top:none; border-left-width:3px;">
                	<asp:TextBox ID="txtJDA3_10DateTime_Receipt" runat="server" CssClass="StrechControl" Height="100px"></asp:TextBox>
                    <Ajax:CalendarExtender ID="clJDA3_10DateTime_Receipt" runat="server" 
                    Format="dd-MMM-yyyy"  BehaviorID="txtJDA3_10DateTime_Receipt"
                    TargetControlID="txtJDA3_10DateTime_Receipt"></Ajax:CalendarExtender>
                </td>
                <td style="border-top:none; border-right:2px solid #F96;">
                	<asp:CheckBox runat="server" id="ckJDA3_11Attached_Invoice" /> Invoice <br>
                    <asp:CheckBox runat="server" id="ckJDA3_11Attached_Bill" /> Bill of Landing<br>
                    <asp:CheckBox runat="server" id="ckJDA3_11Attached_Letter"/> Letter of Credit<br>
                    <asp:CheckBox runat="server" id="ckJDA3_11Attached_Insurance" /> Insurance Certificate <br>
                    <asp:CheckBox runat="server" id="ckJDA3_11Attached_Others"/> Others
                </td>
              </tr>
              <tr>
                <td colspan="3" rowspan="2" style="border-right:2px solid #F96; border-left-width:3px;">12. Registration Number</td>
              </tr>
              <tr>
                <td colspan="2">2. Consignee (Name and Address) </td>
                <td colspan="2"  style="border-bottom:1px solid #F96;">
                <asp:TextBox ID="txtJDA3_2Consignee_Importer_Code" runat="server" CssClass="StrechControl" ></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="4" rowspan="3" style="border-top:none; text-align:left; vertical-align:top;">
                        
                            <div class="wrapper" style="height:50px; width:440px; z-index:18;">
		                        <div class="accordionButton StrechControl">
    		                    	<asp:TextBox ID="txtJDA3_2Consignee_Importer" runat="server" CssClass="StrechControl" 
    		                    	TextMode="MultiLine" Height="80px" ></asp:TextBox>
    		                    </div>
		                        <div class="accordionContent">		                        
		                            <asp:Repeater ID="lst02" runat="server" >
		                                <ItemTemplate>
		                                    <div ID="divItem" runat="server">    			                                
		                                    </div>
		                                </ItemTemplate>
		                            </asp:Repeater>		                            
		                        </div>
	                        </div>	                        
                
                </td>
                <td colspan="3" style="border-top:none; border-right:2px solid #F96; border-left-width:3px;">
                <asp:TextBox ID="txtJDA3_12Registration_Number" runat="server" CssClass="StrechControl"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="2" style="border-left-width:3px;">13. Name of Customs Office</td>
                <td style="border-left:none; border-right:2px solid #F96;">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3" style="border-top:none; border-right:2px solid #F96; border-left-width:3px;">
                <asp:TextBox ID="txtJDA3_13Name_CustomsOffice" runat="server" CssClass="StrechControl" TextMode="MultiLine"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="2" style="border-bottom:none;">3. Name and Address of Authorised Agent </td>
                <td colspan="2"  style="border-bottom:1px solid #F96;">
                <asp:TextBox ID="txtJDA3_3Authorised_Agent_Code" runat="server" CssClass="StrechControl"></asp:TextBox>
                </td>
                <td colspan="2" align="right" style="border-top:none; border-left-width:3px;"><span style="border-top:none; border-left-width:3px;">Code</span></td>
                <td align="right" style="border-right:2px solid #F96;">
                  <asp:TextBox ID="txtJDA3_13Name_CustomsOffice_Code" runat="server" CssClass="StrechControl"></asp:TextBox>                </td>
              </tr>
              <tr>
                <td colspan="4" rowspan="4" style="border-top:none; text-align:left; vertical-align:top;">
                
                            <div class="wrapper" style="height:75px; width:440px; z-index:15;">
		                        <div class="accordionButton StrechControl">
    		                    	<asp:TextBox ID="txtJDA3_3Authorised_Agent" runat="server" CssClass="StrechControl" 
    		                    	TextMode="MultiLine" Height="75px"></asp:TextBox>
    		                    </div>
		                        <div class="accordionContent">		                        
		                            <asp:Repeater ID="lst03" runat="server" >
		                                <ItemTemplate>
		                                    <div ID="divItem" runat="server">    			                                
		                                    </div>
		                                </ItemTemplate>
		                            </asp:Repeater>		                            
		                        </div>
	                        </div>	                 
                </td>
                <td colspan="2" style="border-top-width:3px;">14. Movement Permit No. (if Applicable)</td>
                <td style="border-top-width:3px;">15. Date of Expiry</td>
              </tr>
              <tr>
                <td colspan="2" style="border-top:none;">
                <asp:TextBox ID="txtJDA3_14MovementPermit_No" runat="server" CssClass="StrechControl"></asp:TextBox>
                </td>
                <td style="border-top:none;">
                <asp:TextBox ID="txtJDA3_15Date_Expiry" runat="server" CssClass="StrechControl"></asp:TextBox>
                 <Ajax:CalendarExtender ID="clJDA3_15Date_Expiry" runat="server" 
                     Format="dd-MMM-yyyy" BehaviorID="txtJDA3_15Date_Expiry" 
                     TargetControlID="txtJDA3_15Date_Expiry">
                 </Ajax:CalendarExtender>
                </td>
              </tr>
              <tr>
                <td colspan="2" >16. Security Ref. No. (if applicable)</td>
                <td >17 . Amount of Security</td>
              </tr>
              <tr>
                <td colspan="2" style="border-top:none;">
                <asp:TextBox ID="txtJDA3_16SecurityRef_No" runat="server" CssClass="StrechControl"></asp:TextBox>
                </td>
                <td style="border-top:none;">
                <asp:TextBox ID="txtJDA3_17Amount_Security" runat="server" CssClass="StrechControl"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="3">4. Mode of Transport </td>
                <td>5. Date of Despatch</td>
                <td colspan="2">18. Amount Received/To Be Received</td>
                <td>19. Bill of Landing or Consignment </td>
              </tr>
              <tr>
                <td rowspan="3" style="border-top:none;">
                  <asp:DropDownList ID="ddlJDA3_4Mode_Transport" runat="server" AutoPostBack="True" CssClass="StrechControl" Height="50px">
                    <asp:ListItem Value="" Text=""></asp:ListItem>
                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="border-top:none; border-left:none;"><span style="border-left:none;">1. Sea</span></td>
                <td style="border-top:none; border-left:none;"><span style="border-left:none;">2.Rail</span></td>
                <td rowspan="3" style="border-top:none;">
                  <asp:TextBox ID="txtJDA3_5Date_Despatch" runat="server" CssClass="StrechControl" Height="50px"></asp:TextBox>
                  <Ajax:CalendarExtender ID="ClJDA3_5Date_Despatch" runat="server" 
                Format="dd.MM.yyyy"  BehaviorID="txtJDA3_5Date_Despatch"
                TargetControlID="txtJDA3_5Date_Despatch"></Ajax:CalendarExtender>
                </td>
                <td rowspan="3" style="border-top:none;">
                <asp:DropDownList ID="ddlJDA3_18Currency" runat="server" CssClass="StrechControl" AutoPostBack="true" style="height:50px;">
                  <asp:ListItem Value="" Text=""></asp:ListItem>                  
                </asp:DropDownList></td>
                <td rowspan="3" style="border-top:none;"><asp:TextBox ID="txtJDA3_18AmountReceived" runat="server" CssClass="StrechControl" Height="50px"></asp:TextBox></td>
                <td width="25%" rowspan="3" style="border-top:none;">
                  <asp:TextBox ID="txtJDA3_19Bil_Landing" runat="server" CssClass="StrechControl" Height="50px"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td style="border-top:none; border-left:none;">3. Road</td>
                <td style="border-top:none; border-left:none;">4.Air</td>
              </tr>
              <tr>
                <td style="border-top:none; border-left:none;">10. Others</td>
                <td style="border-top:none; border-left:none;">
                <asp:TextBox ID="txtJDA3_10Others_Specify" runat="server" CssClass="StrechControl"  
               style="border-top: 1px solid #F96; border-left: 1px solid #F96;" PlaceHolder="Specify" ></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="2">6. No./Name of Vessel/Flight/Conveyance</td>
                <td colspan="2">7. Port/Place of Loading </td>
                <td>20. FOB Value</td>
                <td>21. Insurance </td>
                <td>22. Freight</td>
              </tr>
              <tr>
                <td colspan="2" style="border-top:none;">
                <asp:DropDownList ID="ddlJDA3_6Vessel_Flight_Conveyance" runat="server" CssClass="StrechControl" AutoPostBack="true">
                    <asp:ListItem Value="" Text=""></asp:ListItem>
                    <asp:ListItem Value="CB. UNI EXPRESS 10" Text="CB. UNI EXPRESS 10"></asp:ListItem>
                </asp:DropDownList>
                </td>
                <td align="right" style="border-top:none;"><asp:DropDownList ID="ddlJDA3_7Place_Loading" runat="server" CssClass="StrechControl" AutoPostBack="true">
                  <asp:ListItem Value="" Text=""></asp:ListItem>
                  <asp:ListItem Value="SONGKLA" Text="SONGKLA"></asp:ListItem>
                </asp:DropDownList></td>
                <td align="right" style="width:120px;"> <!---ปรับขนาด Column---->
                    <asp:TextBox ID="txtJDA3_7Place_Loading_Code" runat="server" CssClass="StrechControl" PlaceHolder="Code"></asp:TextBox>
                </td>
                <td style="border-top:none; width:150px;"> <!---ปรับขนาด Column---->
                   <input type="text" id="txtCurrency1" runat="server" class="StrechControl" style="width:35px;"/>
                   <asp:TextBox ID="txtJDA3_20FOB_Value" runat="server" CssClass="StrechControl" Width="110px"></asp:TextBox>
                </td>
                <td style="border-top:none; width:150px;"> <!---ปรับขนาด Column---->
                  <input type="text" id="txtCurrency2" runat="server" class="StrechControl" style="width:35px;"/>
                  <asp:TextBox ID="txtJDA3_21Insurance" runat="server" CssClass="StrechControl" Width="110px"></asp:TextBox>
                </td>
                <td style="border-top:none; width:230px; "> <!---ปรับขนาด Column---->
                  <input type="text" id="txtCurrency3" runat="server" class="StrechControl" style="width:35px;"/>
                  <asp:TextBox ID="txtJDA3_22_Freight" runat="server" CssClass="StrechControl" Width="120px"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="2">8. Port/Place of Discharge</td>
                <td colspan="2">9. Via(transhipment cargo only)</td>
                <td>23. Gross Wt. (Kg.) </td>
                <td>24. Measurement (m3)</td>
                <td>25. CIF Value</td>
              </tr>
              <tr>
                <td align="right" style="border-top:none; width:120px;"><!---ปรับขนาด Column---->
                <asp:DropDownList ID="ddlJDA3_8Place_Discharge" runat="server"  CssClass="StrechControl" AutoPostBack="true">
                <asp:ListItem Value="" Text=""></asp:ListItem>
                <asp:ListItem Value="SINGAPORE" Text="SINGAPORE"></asp:ListItem>
              </asp:DropDownList>
                </td>
                <td align="right" >
                <asp:TextBox ID="txtJDA3_8Place_Discharge_Code" runat="server" PlaceHolder="Code" CssClass="StrechControl"></asp:TextBox>
                </td>
                <td align="right" style="border-top:none;">
                <asp:TextBox ID="txtJDA3_9Via" runat="server" CssClass="StrechControl" ></asp:TextBox>
                </td>
                <td align="right">
                <asp:TextBox ID="txtJDA3_9Via_Code" runat="server" PlaceHolder="Code" CssClass="StrechControl"></asp:TextBox>
                </td>
                <td style="border-top:none; width:150px;">
                    <asp:TextBox ID="txtJDA3_23Unit" runat="server" CssClass="StrechControl" Width="35px"></asp:TextBox>
                    <asp:TextBox ID="txtJDA3_23Gross_Wt" runat="server" CssClass="StrechControl" Width="110px"></asp:TextBox>
                </td>
                <td style="border-top:none;  width:150px;">
                <asp:TextBox ID="txtJDA3_24Measurement" runat="server" CssClass="StrechControl"></asp:TextBox>
                </td>
                <td style="border-top:none; width:230px;">
                        <input type="text" id="txtCurrency4" runat="server" class="StrechControl" style="width:35px;"/>
                        <asp:TextBox ID="txtJDA3_25CIF_Value" runat="server" CssClass="StrechControl" Width="120px"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="7">
                <table width="1000" border="0" cellspacing="0" cellpadding="2" class="MainTable">
                  <tr>
                    <td width="30" rowspan="2" align="center">Item <br />
                      No. </td>
                    <td width="135" rowspan="2" align="center">No. and Type <br />
                      of Packages</td>
                    <td width="200" rowspan="2" align="center">Description of Goods</td>
                    <td colspan="2" align="center">Customs Tariff </td>
                    <td width="60" rowspan="2" align="center">Country of <br />
                      Origin Code</td>
                    <td width="80" rowspan="2" align="center">Qty based on <br />
                      Customs Tariff Unit</td>
                    <td colspan="2" align="center">Value </td>
                    <td width="20" rowspan="2" align="center">Ins</td>
                    <td width="20" rowspan="2" align="center">Del</td>                    
                  </tr>
                  <tr>
                    <td width="180" align="center">Code No. </td>
                    <td width="60" align="center">Unit</td>
                    <td width="50" align="center">Per Unit </td>
                    <td width="120" align="center">Total</td>
                  </tr>
                  <asp:Repeater ID="rptData" runat="server">
       				<ItemTemplate>
                  <tr>
                    <td width="30" rowspan="2" style="text-align:center;">
                   		 <asp:TextBox ID="txtRow" runat ="server" style="width:30px; text-align:center; border:none;"></asp:TextBox>
                         <asp:Label ID="lblJDA3_Detail_ID" runat="server"  style="display:none;"></asp:Label>
                         <asp:Label ID="lblINV" runat="server"  style="display:none;"></asp:Label>
                    </td>
                    <td width="350" rowspan="2" colspan="2">
                    	<asp:TextBox ID="txtJDA3_33Description_Goods" runat="server" height="50px" CssClass="StrechControl" style="text-align:left; cursor:pointer;" title="Double Click to search archive description and code."></asp:TextBox>
                    </td>
                    <td width="180" rowspan="2">
                    	<asp:TextBox ID="txtJDA3_34CodeNo" runat="server" height="50px" CssClass="StrechControl" Style="cursor:pointer;" title="Double Click to search archive description and code."></asp:TextBox>
                    </td>
                    <td width="60" rowspan="2">
                    	<asp:TextBox ID="txtJDA3_35Unit" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td width="60" rowspan="2">
                    	<asp:TextBox ID="txtJDA3_36CountryCode" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td width="80" rowspan="2">
                    	<asp:TextBox ID="txtJDA3_37Qtybased" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td width="50">
                    	<asp:TextBox ID="txtJDA3_38PerUnit" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td width="120">
                    	<asp:TextBox ID="txtJDA3_39Total" runat="server" CssClass="StrechControl" style="text-align:right;"></asp:TextBox>
                    </td>
                    <td width="20" rowspan="2">
                    	<asp:ImageButton ID="btnInsert" CommandName="Insert" runat="server" ToolTip="Click to insert record" ImageUrl="images/insert.png" Width="20px" />
                    </td>
                    <td width="20" rowspan="2">
                    	<asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Click to delete" ImageUrl="images/icon/57.png" Width="20px" />
                    </td>
                  </tr>
                  
                  <tr>
                    <td width="50" align="center" valign="middle"><input type="Text" class="StrechControl" value="THB" ReadOnly="True" /></td>
                    <td width="120"><asp:TextBox ID="txtJDA3_39Total_THB" runat="server" CssClass="StrechControl" style="text-align:right;"></asp:TextBox></td>
                  </tr>
                  </ItemTemplate>
                  
     			 </asp:Repeater>
                  <tr>
                    <td colspan="6" rowspan="2" align="left" style="border-left:none;">
                      <asp:Panel ID="pnlAdd" runat="server" DefaultButton="btnAddLine">
                          <asp:Button ID="btnAdd" runat="server" CssClass="Button_Red" Text="Add" style="display:none;"/>
                          Total :&nbsp;
                          <asp:TextBox ID="txt_Total_Line" runat="server" Text="0" style="text-align:center; width:60px;"></asp:TextBox>
                          &nbsp;Lines 
                          &nbsp;&nbsp;&nbsp;<asp:Button ID="btnAddLine" runat="server" CssClass="Button_Red" Text="Ok"/>
                          </asp:Panel>
                    </td>
                    <td rowspan="2" align="center" style="border-left:none;">Total</td>
                    <td align="right"><asp:TextBox ID="txtJDA3_Currency" runat="server" CssClass="StrechControl"></asp:TextBox></td>
                    <td align="right"><asp:TextBox ID="txtJDA3_PerUnit" runat="server"  style="text-align:right;" CssClass="StrechControl" ></asp:TextBox></td>
                    <td width="20" rowspan="2" colspan="2">&nbsp;</td>
                    
                  </tr>
                  <tr>
                    <td><input type="text" class="StrechControl" value="THB" readonly="readonly" /></td>
                    <td><asp:TextBox ID="txtJDA3_Total" runat="server"  style="text-align:right;" CssClass="StrechControl" ></asp:TextBox></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td rowspan="2" >30. Marks and Numbers</td>
                <td colspan="2" rowspan="2" style="border-left:none;" ><asp:TextBox ID="txtJDA3_26Marks_Numbers" runat="server" CssClass="StrechControl"  style="font-size:14px; height:50px; text-align:center !important;" TextMode="MultiLine"></asp:TextBox></td>
                <td style="font-weight:bold; font-size:14px; color:Red; border-right:1px solid #F96;">&nbsp;&nbsp;&nbsp;INV.NO.</td>
                <td colspan="4" style="border-left:none">
                  <table width="100%">
                    <tr>
                        <td colspan="2" style="border:none; height:25px;">
                          <asp:TextBox ID="txtINV_NO" runat="server" CssClass="StrechControl" style="font-size:14px; color:red ; text-align:left;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="border:none; border-top:1px solid #F96; height:25px; width:30px;">
                          <asp:Button ID="btnAddInv" runat="server" CssClass="Button_Red" Text="Add"/>
                          <asp:TextBox ID="txtTmpInv" runat="server" style="display:none;"></asp:TextBox>
                          <asp:Button ID="btnDialog"  runat="server" style="display:none;"/>
                        </td>
                        <td>
                          <asp:Repeater ID="rptINV" runat="server">
                            <ItemTemplate>
                            <li class="file_list" runat="server" id="liINV">
                                <a href="javascript:;" target="_blank" id="INVlink" runat="server">xxxxxxx.png</a> <asp:ImageButton ID="btnINVDelete" runat="server" ImageUrl="images/delete.png" CommandName="Delete" />
                            </li>
                            </ItemTemplate>
                          </asp:Repeater>
                        </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td style="font-weight:bold; font-size:14px; color:Red; border-right:1px solid #3c3;">&nbsp;&nbsp;&nbsp;TOTAL SET</td>
                <td colspan="4"  style="border-left:none;"><asp:TextBox ID="txtTOTAL_SET" runat="server" CssClass="StrechControl" style="font-size:14px; color:red ; text-align:left"></asp:TextBox></td>
              </tr>
              <tr>
                <td colspan="3">36. Name of Declarant </td>
                <td colspan="2" style="">39. To Purper Officer of Customs at.</td>
                <td colspan="2" align="center" 
                style=" font-weight:bold; font-size:14px; border-left-width:3px; border-top-width:3px; border-right:2px solid #F96;">FOR OFFICIAL USE</td>
              </tr>
              <tr>
                <td colspan="3" rowspan="2" style="border-top:none;">
                <asp:TextBox ID="txtJDA3_36Name_Declarant" runat="server" CssClass="StrechControl" Height="35px"></asp:TextBox>
                </td>
                <td colspan="2" rowspan="7" style="border-top:none; ">
                <asp:TextBox ID="txtJDA3_39To_Purper" runat="server" CssClass="StrechControl" TextMode="MultiLine" Height="120px"></asp:TextBox>
                </td>
                <td colspan="2" style="border-left-width:3px; border-right:2px solid #F96;">40. Request Approved</td>
              </tr>
              <tr>
                <td colspan="2" rowspan="3" style="border-top:none; border-left-width:3px; border-right:2px solid #F96;">
                <asp:TextBox ID="txtJDA3_40RequestApproved" runat="server" CssClass="StrechControl" TextMode="MultiLine"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td colspan="3">37. Identity Card/Passport No. </td>
              </tr>
              <tr>
                <td colspan="3" rowspan="2" style="border-top:none;">
                <asp:TextBox ID="txtJDA3_37IDCard_Passport" runat="server" CssClass="StrechControl" Height="35px"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td align="right" style="border-top:none; border-left-width:3px;">Date</td>
                <td style="border-right:2px solid #F96;">
                <asp:TextBox ID="txtJDA3_40RequestApproved_Date" runat="server" CssClass="StrechControl" ></asp:TextBox>
                 <Ajax:CalendarExtender ID="ClJDA3_40RequestApproved_Date" runat="server" 
                     Format="dd-MMM-yyyy" BehaviorID="txtJDA3_40RequestApproved_Date" PopupPosition="TopLeft"
                     TargetControlID="txtJDA3_40RequestApproved_Date">
                 </Ajax:CalendarExtender>
                </td>
              </tr>
              <tr>
                <td colspan="3" rowspan="2">38. Status </td>
                <td colspan="2" style="border-left-width:3px; border-right:2px solid #F96;">41. Certified that the above goods have been duly moved. </td>
              </tr>
              <tr>
                <td colspan="2" style="border-top:none;border-left-width:3px; border-right:2px solid #F96;">Removal from Customs control is authorised.</td>
              </tr>
              <tr>
                <td colspan="3" rowspan="2" style="border-top:none;">
                <asp:TextBox ID="txtJDA3_38Status" runat="server" CssClass="StrechControl" Height="70px" TextMode="MultiLine" style="text-align:center  !important;"></asp:TextBox>
                </td>
                <td colspan="2" style="border-top:none;">Please permit the above goods to be moved into the Joint Development Area. I certify that this declaration is true</td>
                <td colspan="2" style="border-top:none;border-left-width:3px; border-right:2px solid #F96;">
                <asp:TextBox ID="txtJDA3_41Certified" runat="server" CssClass="StrechControl" TextMode="MultiLine"></asp:TextBox>
                </td>
              </tr>
              <tr>
                <td align="right" style="border-top:none;">Date</td>
                <td style="">
                <asp:TextBox ID="txtJDA3_39To_Purper_Date" runat="server" CssClass="StrechControl" ></asp:TextBox>
                <Ajax:CalendarExtender ID="clJDA3_39To_Purper_Date" runat="server" 
                Format="dd-MMM-yyyy"  BehaviorID="txtJDA3_39To_Purper_Date" PopupPosition="TopLeft"
                TargetControlID="txtJDA3_39To_Purper_Date"></Ajax:CalendarExtender>
                </td>
                <td align="right" style="border-top:none; border-left-width:3px; border-bottom:2px solid #F96;">Date</td>
                <td style="border-right:2px solid #F96; border-bottom:2px solid #F96;">
                <asp:TextBox ID="txtJDA3_41Certified_Date" runat="server" CssClass="StrechControl" ></asp:TextBox>
                <Ajax:CalendarExtender ID="ClJDA3_41Certified_Date" runat="server" 
                     Format="dd-MMM-yyyy" BehaviorID="txtJDA3_41Certified_Date" PopupPosition="TopLeft"
                     TargetControlID="txtJDA3_41Certified_Date">
                 </Ajax:CalendarExtender>
                </td>
              </tr>
            </table>            
  </asp:Panel>
            
        </div>
        
        <div style="height:80px;"></div>
        <div id="footerMenu" style="bottom:0px; right:10px; min-height:40px; padding-top:5px; width:100%; background-color:White; text-align:right; padding-right:20px;  position:fixed;" class="NormalTextBlack">
            <table style="width:100%; height:0px;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="text-align:left;">
                        <ul style="list-style: none; padding-left:30px;">
                        
                            <asp:Repeater ID="rptFile" runat="server">
                            <ItemTemplate>
                            <li class="file_list" runat="server" id="liType">
                                <a href="javascript:;" target="_blank" id="alink" runat="server">xxxxxxx.png</a> <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="images/delete.png" CommandName="Delete" />
                            </li>
                            </ItemTemplate>
                            </asp:Repeater>
                            <li style="display:block;padding:3px 5px 3px 5px;">
                                <iframe width="90" height="40" id="windowUpload" runat="server" border="0" marginwidth="0" marginheight="0" style="border:none; margin-left:10px;" scrolling="no"></iframe>
                                <asp:Button ID="btnUpload" runat="server" style="display:none;" />
                            </li>
                        </ul>
                    </td>
                    <td style=" width:420px; vertical-align:top; text-align:center;">
                        <input type="button" class="Button_Disable" style="height:30px; width:130px; color:White; cursor:pointer; " value="Back to Report List" onclick="window.location.href='Filter_JDA_1_3.aspx';" />
                        <asp:Button ID="btnPrint" runat="server" CssClass="Button_Red" Height="30px" Text="Print Preview" Width="130px" />
                        <asp:Button ID="btnSave" runat="server" CssClass="Button_Red" Height="30px" Text="Save" Width="130px" /> 
                    </td>
                </tr>
            </table>
                      
        </div>
        
        <uc1:MasterEditor ID="MasterEditor1" runat="server" Visible="false" />
        <uc1:MasterEditor_MultiLine ID="MasterEditor_MultiLine1" runat="server" Visible="False" />
        <asp:TextBox ID="txtFieldID" runat="Server" style="display:none"></asp:TextBox>
        <asp:Button ID="btnMasterMultilineDialog" runat="Server" style="display:none" />
        
    </ContentTemplate>
</asp:UpdatePanel>
<script language="javascript" type="text/javascript" src="Script/JDA3.js"></script>
</asp:Content>


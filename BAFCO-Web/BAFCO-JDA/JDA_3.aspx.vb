﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class JDA_3
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim TC As New textControlLib
    Dim CV As New Converter

    Public Property PageUniqueID() As String
        Get
            Return ViewState("PageUniqueID").ToString
        End Get
        Set(ByVal value As String)
            ViewState("PageUniqueID") = value
        End Set
    End Property

    Public ReadOnly Property AttachedFileList() As List(Of GenericLib.FileStructure)
        Get
            Dim Result As New List(Of GenericLib.FileStructure)
            For i As Integer = 1 To 100 '-----Set Maximum 100 File-------
                Dim S_Name As String = "File_" & PageUniqueID & "_" & i
                If Not IsNothing(Session(S_Name)) Then
                    Dim F As GenericLib.FileStructure = Session(S_Name)
                    Result.Add(F)
                End If
            Next
            Return Result
        End Get
    End Property

    Public Property JDA_ID() As Integer
        Get
            Try
                Return lblJDA3_ID.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            lblJDA3_ID.Text = value
        End Set
    End Property

    Private WriteOnly Property GridDatasource() As DataTable
        Set(ByVal value As DataTable)
            rptData.DataSource = value
            rptData.DataBind()
            txt_Total_Line.Text = rptData.Items.Count
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("USER_ID")) Then Exit Sub

        If IsNothing(Session("IS_JDA_3")) OrElse Not Session("IS_JDA_3") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        End If

        If Not IsPostBack Then
            SetHeader()
            BindData()
            BindFile()
            TC.ImplementJavaIntegerText(txt_Total_Line, "center")
            BindINV()
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Accordion", "init_Accordion();", True)

    End Sub

    Private Sub SetHeader()
        JDA_ID = Val(Request.QueryString("JDA_ID"))
        PageUniqueID = Now.ToOADate.ToString.Replace(".", "")
    End Sub

#Region "BindTextAreaDLL"

    Private Sub Bind_JDA3_1Consignor(ByVal DisplayDefault As Boolean)
        lst01.DataSource = Nothing
        lst01.DataBind()

        Dim SQL As String = "SELECT * FROM tb_JDA3_01 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DR_Blank As DataRow = DT.NewRow
        DR_Blank.Item("Name") = "....."
        DR_Blank.Item("Code") = ""
        DT.Rows.InsertAt(DR_Blank, 0)
        Dim DR_Master As DataRow = DT.NewRow
        DR_Master.Item("Name") = "*** Manage ***"
        DT.Rows.Add(DR_Master)
        lst01.DataSource = DT
        lst01.DataBind()

        If DisplayDefault Then
            DT.DefaultView.RowFilter = "IsDefault=True"
            If DT.DefaultView.Count > 0 Then
                txtJDA3_1Consignor.Text = DT.DefaultView(0).Item("Name").ToString
                txtJDA3_1Consignor_Code.Text = DT.DefaultView(0).Item("Code").ToString
            Else
                txtJDA3_1Consignor.Text = ""
                txtJDA3_1Consignor_Code.Text = ""
            End If
        End If
    End Sub

    Private Sub Bind_JDA3_2Consignee_Importer(ByVal DisplayDefault As Boolean)
        lst02.DataSource = Nothing
        lst02.DataBind()

        Dim SQL As String = "SELECT * FROM tb_JDA3_02 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DR_Blank As DataRow = DT.NewRow
        DR_Blank.Item("Name") = "....."
        DR_Blank.Item("Code") = ""
        DT.Rows.InsertAt(DR_Blank, 0)
        Dim DR_Master As DataRow = DT.NewRow
        DR_Master.Item("Name") = "*** Manage ***"
        DT.Rows.Add(DR_Master)
        lst02.DataSource = DT
        lst02.DataBind()

        If DisplayDefault Then
            DT.DefaultView.RowFilter = "IsDefault=True"
            If DT.DefaultView.Count > 0 Then
                txtJDA3_2Consignee_Importer.Text = DT.DefaultView(0).Item("Name").ToString
                txtJDA3_2Consignee_Importer_Code.Text = DT.DefaultView(0).Item("Code").ToString
            Else
                txtJDA3_2Consignee_Importer.Text = ""
                txtJDA3_2Consignee_Importer_Code.Text = ""
            End If
        End If

    End Sub

    Private Sub Bind_JDA3_3Authorised_Agent(ByVal DisplayDefault As Boolean)
        lst03.DataSource = Nothing
        lst03.DataBind()

        Dim SQL As String = "SELECT * FROM tb_JDA3_03 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DR_Blank As DataRow = DT.NewRow
        DR_Blank.Item("Name") = "....."
        DR_Blank.Item("Code") = ""
        DT.Rows.InsertAt(DR_Blank, 0)
        Dim DR_Master As DataRow = DT.NewRow
        DR_Master.Item("Name") = "*** Manage ***"
        DT.Rows.Add(DR_Master)
        lst03.DataSource = DT
        lst03.DataBind()

        If DisplayDefault Then
            DT.DefaultView.RowFilter = "IsDefault=True"
            If DT.DefaultView.Count > 0 Then
                txtJDA3_3Authorised_Agent.Text = DT.DefaultView(0).Item("Name").ToString
                txtJDA3_3Authorised_Agent_Code.Text = DT.DefaultView(0).Item("Code").ToString
            Else
                txtJDA3_3Authorised_Agent.Text = ""
                txtJDA3_3Authorised_Agent_Code.Text = ""
            End If
        End If
    End Sub

    Protected Sub lst01_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles lst01.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divItem As HtmlGenericControl = e.Item.FindControl("divItem")
        divItem.InnerHtml = e.Item.DataItem("Name").ToString.Replace(vbLf, "<br>")

        If e.Item.ItemIndex = CType(lst01.DataSource, DataTable).Rows.Count - 1 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtFieldID.ClientID & "').value=1; document.getElementById('" & btnMasterMultilineDialog.ClientID & "').click();"
            divItem.Style("text-align") = "center"
        ElseIf e.Item.ItemIndex = 0 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA3_1Consignor.ClientID & "').value='';"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA3_1Consignor_Code.ClientID & "').value='';"
            divItem.Style("text-align") = "center"
        Else
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA3_1Consignor.ClientID & "').value=replaceAll(this.innerHTML,'<br>','\n');"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA3_1Consignor_Code.ClientID & "').value='" & e.Item.DataItem("Code").ToString.Replace("'", "\'") & "';"
        End If
    End Sub

    Protected Sub lst02_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles lst02.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divItem As HtmlGenericControl = e.Item.FindControl("divItem")
        divItem.InnerHtml = e.Item.DataItem("Name").ToString.Replace(vbLf, "<br>")

        If e.Item.ItemIndex = CType(lst02.DataSource, DataTable).Rows.Count - 1 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtFieldID.ClientID & "').value=2; document.getElementById('" & btnMasterMultilineDialog.ClientID & "').click();"
            divItem.Style("text-align") = "center"
        ElseIf e.Item.ItemIndex = 0 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA3_2Consignee_Importer.ClientID & "').value='';"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA3_2Consignee_Importer_Code.ClientID & "').value='';"
            divItem.Style("text-align") = "center"
        Else
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA3_2Consignee_Importer.ClientID & "').value=replaceAll(this.innerHTML,'<br>','\n');"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA3_2Consignee_Importer_Code.ClientID & "').value='" & e.Item.DataItem("Code").ToString.Replace("'", "\'") & "';"
        End If
    End Sub

    Protected Sub lst03_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles lst03.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divItem As HtmlGenericControl = e.Item.FindControl("divItem")
        divItem.InnerHtml = e.Item.DataItem("Name").ToString.Replace(vbLf, "<br>")

        If e.Item.ItemIndex = CType(lst03.DataSource, DataTable).Rows.Count - 1 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtFieldID.ClientID & "').value=3; document.getElementById('" & btnMasterMultilineDialog.ClientID & "').click();"
            divItem.Style("text-align") = "center"
        ElseIf e.Item.ItemIndex = 0 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA3_3Authorised_Agent.ClientID & "').value='';"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA3_3Authorised_Agent_Code.ClientID & "').value='';"
            divItem.Style("text-align") = "center"
        Else
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA3_3Authorised_Agent.ClientID & "').value=replaceAll(this.innerHTML,'<br>','\n');"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA3_3Authorised_Agent_Code.ClientID & "').value='" & e.Item.DataItem("Code").ToString.Replace("'", "\'") & "';"
        End If
    End Sub

    Protected Sub btnMasterMultilineDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMasterMultilineDialog.Click
        Select Case txtFieldID.Text
            Case "1"
                MasterEditor_MultiLine1.MasterName = "Consignor (Name and Address)"
                MasterEditor_MultiLine1.TableName = "tb_JDA3_01"
                MasterEditor_MultiLine1.RelateTransaction = "tb_JDA_3.JDA3_1Consignor"
            Case "2"
                MasterEditor_MultiLine1.MasterName = "Consignee (Name and Address) "
                MasterEditor_MultiLine1.TableName = "tb_JDA3_02"
                MasterEditor_MultiLine1.RelateTransaction = "tb_JDA_3.JDA3_2Consignee_Importer"
            Case "3"
                MasterEditor_MultiLine1.MasterName = "Name and Address of Authorised Agent"
                MasterEditor_MultiLine1.TableName = "tb_JDA3_03"
                MasterEditor_MultiLine1.RelateTransaction = "tb_JDA_3.JDA3_3Authorised_Agent"
            Case Else
                Exit Sub
        End Select
        MasterEditor_MultiLine1.BindData()
        MasterEditor_MultiLine1.Visible = True
    End Sub

    Protected Sub MasterEditor_MultiLine1_ManageFinished(ByRef Editor As MasterEditor_MultiLine) Handles MasterEditor_MultiLine1.ManageFinished
        Select Case txtFieldID.Text
            Case "1"
                Bind_JDA3_1Consignor(False)
            Case "2"
                Bind_JDA3_2Consignee_Importer(False)
            Case "3"
                Bind_JDA3_3Authorised_Agent(False)
            Case Else

        End Select
        MasterEditor_MultiLine1.Visible = False
    End Sub

    Protected Sub MasterEditor_MultiLine1_Selected(ByRef Editor As MasterEditor_MultiLine, ByVal Value As String) Handles MasterEditor_MultiLine1.Selected
        Select Case txtFieldID.Text
            Case "1"
                Bind_JDA3_1Consignor(False)
                txtJDA3_1Consignor.Text = Value.Replace("<br>", vbLf)
                txtJDA3_1Consignor_Code.Text = MasterEditor_MultiLine1.GetMasterCode(MasterEditor_MultiLine1.TableName, Value.Replace("<br>", vbLf))
            Case "2"
                Bind_JDA3_2Consignee_Importer(False)
                txtJDA3_2Consignee_Importer.Text = Value.Replace("<br>", vbLf)
                txtJDA3_2Consignee_Importer_Code.Text = MasterEditor_MultiLine1.GetMasterCode(MasterEditor_MultiLine1.TableName, Value.Replace("<br>", vbLf))
            Case "3"
                Bind_JDA3_3Authorised_Agent(False)
                txtJDA3_3Authorised_Agent.Text = Value.Replace("<br>", vbLf)
                txtJDA3_3Authorised_Agent_Code.Text = MasterEditor_MultiLine1.GetMasterCode(MasterEditor_MultiLine1.TableName, Value.Replace("<br>", vbLf))
            Case Else

        End Select
        MasterEditor_MultiLine1.Visible = False
    End Sub

#End Region

#Region "Binding DLL"
    Private Sub Bind_JDA3_6Vessel_Flight_Conveyance(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA3_6Vessel_Flight_Conveyance.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA3_6Vessel_Flight_Conveyance.Items.Add(BlankItem)
        ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA3_06 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA3_6Vessel_Flight_Conveyance.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndex = i + 1
            End If
        Next
        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA3_6Vessel_Flight_Conveyance.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA3_6Vessel_Flight_Conveyance_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA3_7Place_Loading(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA3_7Place_Loading.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA3_7Place_Loading.Items.Add(BlankItem)
        ddlJDA3_7Place_Loading.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA3_07 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA3_7Place_Loading.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA3_7Place_Loading.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA3_7Place_Loading.SelectedIndex = i + 1
            End If
        Next

        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA3_7Place_Loading.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA3_7Place_Loading_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA3_8Place_Discharge(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA3_8Place_Discharge.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA3_8Place_Discharge.Items.Add(BlankItem)
        ddlJDA3_8Place_Discharge.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA3_08 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA3_8Place_Discharge.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA3_8Place_Discharge.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA3_8Place_Discharge.SelectedIndex = i + 1
            End If
        Next

        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA3_8Place_Discharge.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA3_8Place_Discharge_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA3_18Currency(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA3_18Currency.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA3_18Currency.Items.Add(BlankItem)
        ddlJDA3_18Currency.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA3_18 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA3_18Currency.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA3_18Currency.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA3_18Currency.SelectedIndex = i + 1
            End If
        Next
        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA3_18Currency.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA3_18Currency_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub ddlJDA3_6Vessel_Flight_Conveyance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndexChanged
        If ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndex = ddlJDA3_6Vessel_Flight_Conveyance.Items.Count - 1 Then
            MasterEditor1.MasterName = "Name of Vessel/Flight/Conveyance"
            MasterEditor1.TableName = "tb_JDA3_06"
            MasterEditor1.RelateTransaction = "tb_JDA_3.JDA3_6Vessel_Flight_Conveyance"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else
            '--------------- Keep Last Value---------------
            ddlJDA3_6Vessel_Flight_Conveyance.Attributes("LastName") = ddlJDA3_6Vessel_Flight_Conveyance.Items(ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndex).Value
        End If
    End Sub

    Protected Sub ddlJDA3_7Place_Loading_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA3_7Place_Loading.SelectedIndexChanged
        If ddlJDA3_7Place_Loading.SelectedIndex = ddlJDA3_7Place_Loading.Items.Count - 1 Then
            MasterEditor1.MasterName = "Port/Place of Loading"
            MasterEditor1.TableName = "tb_JDA3_07"
            MasterEditor1.RelateTransaction = "tb_JDA_3.JDA3_7Place_Loading"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else '----------- Apply Code ----------------
            txtJDA3_7Place_Loading_Code.Text = MasterEditor1.GetMasterCode("tb_JDA3_07", ddlJDA3_7Place_Loading.Items(ddlJDA3_7Place_Loading.SelectedIndex).Value)
            '--------------- Keep Last Value---------------
            ddlJDA3_7Place_Loading.Attributes("LastName") = ddlJDA3_7Place_Loading.Items(ddlJDA3_7Place_Loading.SelectedIndex).Value
        End If
    End Sub

    Protected Sub ddlJDA3_8Place_Discharge_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA3_8Place_Discharge.SelectedIndexChanged
        If ddlJDA3_8Place_Discharge.SelectedIndex = ddlJDA3_8Place_Discharge.Items.Count - 1 Then
            MasterEditor1.MasterName = "Port/Place of Discharge"
            MasterEditor1.TableName = "tb_JDA3_08"
            MasterEditor1.RelateTransaction = "tb_JDA_3.JDA3_8Place_Discharge"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else '----------- Apply Code ----------------
            txtJDA3_8Place_Discharge_Code.Text = MasterEditor1.GetMasterCode("tb_JDA3_08", ddlJDA3_8Place_Discharge.Items(ddlJDA3_8Place_Discharge.SelectedIndex).Value)
            '--------------- Keep Last Value---------------
            ddlJDA3_8Place_Discharge.Attributes("LastName") = ddlJDA3_8Place_Discharge.Items(ddlJDA3_8Place_Discharge.SelectedIndex).Value
        End If
    End Sub

    Protected Sub ddlJDA3_18Currency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA3_18Currency.SelectedIndexChanged
        '------------- Set Calculate Currency -------------
        If ddlJDA3_18Currency.SelectedIndex = ddlJDA3_18Currency.Items.Count - 1 Then
            MasterEditor1.MasterName = "Exchange Rate"
            MasterEditor1.TableName = "tb_JDA3_18"
            MasterEditor1.RelateTransaction = "tb_JDA_3.JDA3_18Currency"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else
            '--------------- Keep Last Value---------------
            ddlJDA3_18Currency.Attributes("LastName") = ddlJDA3_18Currency.Items(ddlJDA3_18Currency.SelectedIndex).Value
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
        End If
    End Sub

    Protected Sub MasterEditor1_Selected(ByRef Editor As MasterEditor, ByVal LastValue As String) Handles MasterEditor1.Selected ',MasterEditor1.AddFinished, 
        Select Case MasterEditor1.TableName
            Case "tb_JDA3_06"
                Bind_JDA3_6Vessel_Flight_Conveyance(False, LastValue)
            Case "tb_JDA3_07"
                Bind_JDA3_7Place_Loading(False, LastValue)
            Case "tb_JDA3_08"
                Bind_JDA3_8Place_Discharge(False, LastValue)
            Case "tb_JDA3_18"
                Bind_JDA3_18Currency(False, LastValue)
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
        End Select
        MasterEditor1.Visible = False
    End Sub

    Protected Sub MasterEditor1_ManageFinished(ByRef Editor As MasterEditor) Handles MasterEditor1.ManageFinished
        Select Case MasterEditor1.TableName
            Case "tb_JDA3_06"
                Bind_JDA3_6Vessel_Flight_Conveyance(False, ddlJDA3_6Vessel_Flight_Conveyance.Attributes("LastName"))
            Case "tb_JDA3_07"
                Bind_JDA3_7Place_Loading(False, ddlJDA3_7Place_Loading.Attributes("LastName"))
            Case "tb_JDA3_08"
                Bind_JDA3_8Place_Discharge(False, ddlJDA3_8Place_Discharge.Attributes("LastName"))
            Case "tb_JDA3_18"
                Bind_JDA3_18Currency(False, ddlJDA3_18Currency.Attributes("LastName"))
        End Select
        MasterEditor1.Visible = False
    End Sub

#End Region

    Private Sub BindData()
        If IsNothing(Session("User_ID")) Then Exit Sub

        ClearForm()

        Dim SQL As String = "SELECT * FROM tb_JDA_3 WHERE JDA3_ID = " & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            JDA_ID = 0
            Exit Sub
        End If

        txt_BAFCO_Job_No.Text = DT.Rows(0).Item("BAFCO_Job_No").ToString
        txt_Client_Reference_No.Text = DT.Rows(0).Item("Client_Reference_No").ToString

        txtJDA3_1Consignor_Code.Text = DT.Rows(0).Item("JDA3_1Consignor_Code").ToString
        txtJDA3_1Consignor.Text = DT.Rows(0).Item("JDA3_1Consignor").ToString

        txtJDA3_2Consignee_Importer_Code.Text = DT.Rows(0).Item("JDA3_2Consignee_Importer_Code").ToString
        txtJDA3_2Consignee_Importer.Text = DT.Rows(0).Item("JDA3_2Consignee_Importer").ToString

        txtJDA3_3Authorised_Agent_Code.Text = DT.Rows(0).Item("JDA3_3Authorised_Agent_Code").ToString
        txtJDA3_3Authorised_Agent.Text = DT.Rows(0).Item("JDA3_3Authorised_Agent").ToString

        If Not IsDBNull(DT.Rows(0).Item("JDA3_4Mode_Transport")) Then
            For i As Integer = 0 To ddlJDA3_4Mode_Transport.Items.Count - 1
                If ddlJDA3_4Mode_Transport.Items(i).Value = DT.Rows(0).Item("JDA3_4Mode_Transport") Then
                    ddlJDA3_4Mode_Transport.SelectedIndex = i : Exit For
                End If
            Next
        End If

        If ddlJDA3_4Mode_Transport.SelectedIndex = 5 Then
            txtJDA3_10Others_Specify.Visible = True
            txtJDA3_10Others_Specify.Text = DT.Rows(0).Item("JDA3_10Others_Specify").ToString
        End If

        If Not IsDBNull(DT.Rows(0).Item("JDA3_5Date_Despatch")) Then
            txtJDA3_5Date_Despatch.Text = GL.ReportProgrammingDotDate(DT.Rows(0).Item("JDA3_5Date_Despatch"))
        End If

        Bind_JDA3_6Vessel_Flight_Conveyance(False, DT.Rows(0).Item("JDA3_6Vessel_Flight_Conveyance").ToString)


        Bind_JDA3_7Place_Loading(False, DT.Rows(0).Item("JDA3_7Place_Loading").ToString)
        txtJDA3_7Place_Loading_Code.Text = DT.Rows(0).Item("JDA3_7Place_Loading_Code").ToString

        Bind_JDA3_8Place_Discharge(False, DT.Rows(0).Item("JDA3_8Place_Discharge").ToString)
        txtJDA3_8Place_Discharge_Code.Text = DT.Rows(0).Item("JDA3_8Place_Discharge_Code").ToString

        txtJDA3_9Via_Code.Text = DT.Rows(0).Item("JDA3_9Via_Code").ToString
        txtJDA3_9Via.Text = DT.Rows(0).Item("JDA3_9Via").ToString

        If Not IsDBNull(DT.Rows(0).Item("JDA3_10DateTime_Receipt")) Then
            txtJDA3_10DateTime_Receipt.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA3_10DateTime_Receipt"), "dd-MMM-yyyy")
        End If

        txtJDA3_12Registration_Number.Text = DT.Rows(0).Item("JDA3_12Registration_Number").ToString
        txtJDA3_13Name_CustomsOffice_Code.Text = DT.Rows(0).Item("JDA3_13Name_CustomsOffice_Code").ToString
        txtJDA3_13Name_CustomsOffice.Text = DT.Rows(0).Item("JDA3_13Name_CustomsOffice").ToString
        txtJDA3_14MovementPermit_No.Text = DT.Rows(0).Item("JDA3_14MovementPermit_No").ToString

        If Not IsDBNull(DT.Rows(0).Item("JDA3_15Date_Expiry")) Then
            txtJDA3_15Date_Expiry.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA3_15Date_Expiry"), "dd-MMM-yyyy")
        End If

        txtJDA3_16SecurityRef_No.Text = DT.Rows(0).Item("JDA3_16SecurityRef_No").ToString
        txtJDA3_17Amount_Security.Text = DT.Rows(0).Item("JDA3_17Amount_Security").ToString

        Bind_JDA3_18Currency(False, DT.Rows(0).Item("JDA3_18Currency").ToString)
        If IsNumeric(DT.Rows(0).Item("JDA3_18AmountReceived")) Then
            txtJDA3_18AmountReceived.Text = FormatNumber(DT.Rows(0).Item("JDA3_18AmountReceived"), 4)
        End If

        txtJDA3_19Bil_Landing.Text = DT.Rows(0).Item("JDA3_19Bil_Landing").ToString

        If IsNumeric(DT.Rows(0).Item("JDA3_20FOB_Value")) Then
            txtJDA3_20FOB_Value.Text = FormatNumber(DT.Rows(0).Item("JDA3_20FOB_Value"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA3_21Insurance")) Then
            txtJDA3_21Insurance.Text = FormatNumber(DT.Rows(0).Item("JDA3_21Insurance"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA3_22_Freight")) Then
            txtJDA3_22_Freight.Text = FormatNumber(DT.Rows(0).Item("JDA3_22_Freight"))
        End If

        txtJDA3_23Unit.Text = DT.Rows(0).Item("JDA3_23Unit").ToString
        If IsNumeric(DT.Rows(0).Item("JDA3_23Gross_Wt")) Then
            txtJDA3_23Gross_Wt.Text = FormatNumber(DT.Rows(0).Item("JDA3_23Gross_Wt"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA3_24Measurement")) Then
            txtJDA3_24Measurement.Text = FormatNumber(DT.Rows(0).Item("JDA3_24Measurement"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA3_25CIF_Value")) Then
            txtJDA3_25CIF_Value.Text = FormatNumber(DT.Rows(0).Item("JDA3_25CIF_Value"))
        End If
        txtJDA3_26Marks_Numbers.Text = DT.Rows(0).Item("JDA3_26Marks_Numbers").ToString

        txtJDA3_Currency.Text = DT.Rows(0).Item("JDA3_Currency").ToString
        If IsNumeric(DT.Rows(0).Item("JDA3_PerUnit")) Then
            txtJDA3_PerUnit.Text = FormatNumber(DT.Rows(0).Item("JDA3_PerUnit"))
        Else
            txtJDA3_PerUnit.Text = ""
        End If
        If IsNumeric(DT.Rows(0).Item("JDA3_Total")) Then
            txtJDA3_Total.Text = FormatNumber(DT.Rows(0).Item("JDA3_Total"))
        Else
            txtJDA3_Total.Text = ""
        End If


        txtJDA3_36Name_Declarant.Text = DT.Rows(0).Item("JDA3_36Name_Declarant").ToString
        txtJDA3_37IDCard_Passport.Text = DT.Rows(0).Item("JDA3_37IDCard_Passport").ToString
        txtJDA3_38Status.Text = DT.Rows(0).Item("JDA3_38Status").ToString
        txtJDA3_39To_Purper.Text = DT.Rows(0).Item("JDA3_39To_Purper").ToString

        If Not IsDBNull(DT.Rows(0).Item("JDA3_39To_Purper_Date")) Then
            txtJDA3_39To_Purper_Date.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA3_39To_Purper_Date"), "dd-MMM-yyyy")
        End If

        txtJDA3_40RequestApproved.Text = DT.Rows(0).Item("JDA3_40RequestApproved").ToString
        If Not IsDBNull(DT.Rows(0).Item("JDA3_40RequestApproved_Date")) Then
            txtJDA3_40RequestApproved_Date.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA3_40RequestApproved_Date"), "dd-MMM-yyyy")
        End If

        txtJDA3_41Certified.Text = DT.Rows(0).Item("JDA3_41Certified").ToString
        If Not IsDBNull(DT.Rows(0).Item("JDA3_41Certified_Date")) Then
            txtJDA3_41Certified_Date.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA3_41Certified_Date"), "dd-MMM-yyyy")
        End If

        If Not IsDBNull(DT.Rows(0).Item("JDA3_11Attached_Invoice")) Then
            ckJDA3_11Attached_Invoice.Checked = DT.Rows(0).Item("JDA3_11Attached_Invoice")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA3_11Attached_Bill")) Then
            ckJDA3_11Attached_Bill.Checked = DT.Rows(0).Item("JDA3_11Attached_Bill")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA3_11Attached_Insurance")) Then
            ckJDA3_11Attached_Insurance.Checked = DT.Rows(0).Item("JDA3_11Attached_Insurance")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA3_11Attached_Letter")) Then
            ckJDA3_11Attached_Letter.Checked = DT.Rows(0).Item("JDA3_11Attached_Letter")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA3_11Attached_Others")) Then
            ckJDA3_11Attached_Others.Checked = DT.Rows(0).Item("JDA3_11Attached_Others")
        End If

        txtINV_NO.Text = DT.Rows(0).Item("JDA3_INV_NO").ToString
        txtTOTAL_SET.Text = DT.Rows(0).Item("JDA3_TOTAL_SET").ToString
        If IsNumeric(DT.Rows(0).Item("JDA3_Total")) Then
            txtJDA3_Total.Text = FormatNumber(DT.Rows(0).Item("JDA3_Total"))
        End If
        '------------------- Bind Detail --------------------
        '----------------Bind Detail -----------------
        SQL = "SELECT * FROM tb_JDA_3_Detail WHERE JDA3_ID = " & JDA_ID & " ORDER BY JDA3_Detail_ID"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        GridDatasource = DT

    End Sub

    Private Sub ClearForm()

        txt_BAFCO_Job_No.Text = ""
        txt_Client_Reference_No.Text = ""

        TC.ImplementJavaOnlyNumberText(txtJDA3_1Consignor_Code, "center")
        Bind_JDA3_1Consignor(True)

        TC.ImplementJavaOnlyNumberText(txtJDA3_2Consignee_Importer_Code, "center")
        Bind_JDA3_2Consignee_Importer(True)
     
        TC.ImplementJavaOnlyNumberText(txtJDA3_3Authorised_Agent_Code, "center")
        Bind_JDA3_3Authorised_Agent(True)

        ddlJDA3_4Mode_Transport.SelectedIndex = 0

        txtJDA3_10Others_Specify.Visible = False
        txtJDA3_10Others_Specify.Text = ""

        txtJDA3_5Date_Despatch.Text = ""
        ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndex = 0

        Bind_JDA3_6Vessel_Flight_Conveyance(True)
        Bind_JDA3_7Place_Loading(True)
        Bind_JDA3_8Place_Discharge(True)

        txtJDA3_9Via_Code.Text = ""
        txtJDA3_9Via.Text = ""

        txtJDA3_10DateTime_Receipt.Text = ""
        ckJDA3_11Attached_Invoice.Checked = False
        ckJDA3_11Attached_Bill.Checked = False
        ckJDA3_11Attached_Insurance.Checked = False
        ckJDA3_11Attached_Letter.Checked = False
        ckJDA3_11Attached_Others.Checked = False

        txtJDA3_12Registration_Number.Text = ""
        txtJDA3_13Name_CustomsOffice_Code.Text = ""
        txtJDA3_13Name_CustomsOffice.Text = "SONGKHLA CUSTOMS HOUSE"
        txtJDA3_14MovementPermit_No.Text = ""
        txtJDA3_15Date_Expiry.Text = ""
        txtJDA3_16SecurityRef_No.Text = ""
        txtJDA3_17Amount_Security.Text = ""

        Bind_JDA3_18Currency(True)
        txtJDA3_18AmountReceived.Text = "1.0000"
        TC.ImplementJavaFloatText(txtJDA3_18AmountReceived, 4, "center")
        txtJDA3_18AmountReceived.Attributes("onchange") &= " calculateLineTotal();"

        txtJDA3_19Bil_Landing.Text = ""
        txtJDA3_20FOB_Value.Text = ""
        TC.ImplementJavaMoneyText(txtJDA3_20FOB_Value, "center")
        txtJDA3_20FOB_Value.Attributes("onchange") &= " calculateFOB();"

        txtJDA3_21Insurance.Text = ""
        TC.ImplementJavaMoneyText(txtJDA3_21Insurance, "center")

        txtJDA3_22_Freight.Text = ""
        TC.ImplementJavaMoneyText(txtJDA3_22_Freight, "center")

        txtJDA3_23Unit.Text = "KGS"
        txtJDA3_23Gross_Wt.Text = ""
        TC.ImplementJavaMoneyText(txtJDA3_23Gross_Wt, "center")

        txtJDA3_24Measurement.Text = ""
        TC.ImplementJavaMoneyText(txtJDA3_24Measurement, "center")

        txtJDA3_25CIF_Value.Text = ""
        TC.ImplementJavaMoneyText(txtJDA3_25CIF_Value, "center")

        txtJDA3_26Marks_Numbers.Text = "AS PER ATTACHED LIST" & vbLf & "(REF.No: xxxxxxxxx)"
       
        txtJDA3_36Name_Declarant.Text = ""
        txtJDA3_37IDCard_Passport.Text = ""
        txtJDA3_38Status.Text = ""
        txtJDA3_39To_Purper.Text = ""
        txtJDA3_39To_Purper_Date.Text = ""
        txtJDA3_40RequestApproved.Text = ""
        txtJDA3_40RequestApproved_Date.Text = ""
        txtJDA3_41Certified.Text = ""
        txtJDA3_41Certified_Date.Text = ""

        txtJDA3_Currency.Text = ""
        txtJDA3_PerUnit.Text = ""
        txtJDA3_Total.Text = ""
        txtJDA3_PerUnit.Attributes("ReadOnly") = "True"
        txtJDA3_Total.Attributes("ReadOnly") = "True"

        txtTOTAL_SET.Text = ""
        txtINV_NO.Text = ""
        txtJDA3_Total.Text = ""

        GridDatasource = Nothing

        '------------------ Set MasterEditor -----------
        MasterEditor1.Visible = False

        '------------------ Set File upload ------------
        windowUpload.Attributes("src") = "JDA_Upload.aspx?P=" & PageUniqueID & "&B=" & btnUpload.ClientID & "&t=" & Now.ToOADate.ToString.Replace(".", "")
        '------------------ Empty Buffer--------------
        For i As Integer = 1 To 100
            Dim SNAME As String = "File_" & PageUniqueID & "_" & i
            Session(SNAME) = Nothing
        Next
        DisplayFile()
    End Sub

    Protected Sub ddlJDA3_4Mode_Transport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA3_4Mode_Transport.SelectedIndexChanged
        txtJDA3_10Others_Specify.Visible = ddlJDA3_4Mode_Transport.SelectedIndex = 5
    End Sub

    Public Function CurrentHeader() As DataTable
        Dim SQL As String = ""
        SQL = "SELECT * FROM tb_JDA_3 WHERE 1=0"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow

        DR("JDA3_ID") = JDA_ID
        DR("JDA3_1Consignor_Code") = txtJDA3_1Consignor_Code.Text
        DR("JDA3_1Consignor") = txtJDA3_1Consignor.Text
        DR("JDA3_2Consignee_Importer_Code") = txtJDA3_2Consignee_Importer_Code.Text
        DR("JDA3_2Consignee_Importer") = txtJDA3_2Consignee_Importer.Text
        DR("JDA3_3Authorised_Agent_Code") = txtJDA3_3Authorised_Agent_Code.Text
        DR("JDA3_3Authorised_Agent") = txtJDA3_3Authorised_Agent.Text
        DR("JDA3_4Mode_Transport") = ddlJDA3_4Mode_Transport.Items(ddlJDA3_4Mode_Transport.SelectedIndex).Value

        If GL.IsProgrammingDate(txtJDA3_5Date_Despatch.Text, "dd.MM.yyyy") Then
            DR("JDA3_5Date_Despatch") = CV.StringToDate(txtJDA3_5Date_Despatch.Text, "dd.MM.yyyy")
        Else
            DR("JDA3_5Date_Despatch") = DBNull.Value
        End If
        DR("JDA3_6Vessel_Flight_Conveyance") = ddlJDA3_6Vessel_Flight_Conveyance.Items(ddlJDA3_6Vessel_Flight_Conveyance.SelectedIndex).Value
        DR("JDA3_7Place_Loading_Code") = txtJDA3_7Place_Loading_Code.Text
        DR("JDA3_7Place_Loading") = ddlJDA3_7Place_Loading.Items(ddlJDA3_7Place_Loading.SelectedIndex).Value
        DR("JDA3_8Place_Discharge_Code") = txtJDA3_8Place_Discharge_Code.Text
        DR("JDA3_8Place_Discharge") = ddlJDA3_8Place_Discharge.Items(ddlJDA3_8Place_Discharge.SelectedIndex).Value
        DR("JDA3_9Via_Code") = txtJDA3_9Via_Code.Text
        DR("JDA3_9Via") = txtJDA3_9Via.Text
        DR("JDA3_10Others_Specify") = txtJDA3_10Others_Specify.Text
        If GL.IsProgrammingDate(txtJDA3_10DateTime_Receipt.Text, "dd-MMM-yyyy") Then
            DR("JDA3_10DateTime_Receipt") = CV.StringToDate(txtJDA3_10DateTime_Receipt.Text, "dd-MMM-yyyy")
        Else
            DR("JDA3_10DateTime_Receipt") = DBNull.Value
        End If

        DR("JDA3_11Attached_Invoice") = ckJDA3_11Attached_Invoice.Checked
        DR("JDA3_11Attached_Bill") = ckJDA3_11Attached_Bill.Checked
        DR("JDA3_11Attached_Insurance") = ckJDA3_11Attached_Insurance.Checked
        DR("JDA3_11Attached_Letter") = ckJDA3_11Attached_Letter.Checked
        DR("JDA3_11Attached_Others") = ckJDA3_11Attached_Others.Checked

        DR("JDA3_12Registration_Number") = txtJDA3_12Registration_Number.Text
        DR("JDA3_13Name_CustomsOffice_Code") = txtJDA3_13Name_CustomsOffice_Code.Text
        DR("JDA3_13Name_CustomsOffice") = txtJDA3_13Name_CustomsOffice.Text
        DR("JDA3_14MovementPermit_No") = txtJDA3_14MovementPermit_No.Text

        If GL.IsProgrammingDate(txtJDA3_15Date_Expiry.Text, "dd-MMM-yyyy") Then
            DR("JDA3_15Date_Expiry") = CV.StringToDate(txtJDA3_15Date_Expiry.Text, "dd-MMM-yyyy")
        Else
            DR("JDA3_15Date_Expiry") = DBNull.Value
        End If
        DR("JDA3_16SecurityRef_No") = txtJDA3_16SecurityRef_No.Text
        DR("JDA3_17Amount_Security") = txtJDA3_17Amount_Security.Text

        DR("JDA3_18Currency") = ddlJDA3_18Currency.Items(ddlJDA3_18Currency.SelectedIndex).Value
        If IsNumeric(txtJDA3_18AmountReceived.Text.Replace(",", "")) Then
            DR("JDA3_18AmountReceived") = txtJDA3_18AmountReceived.Text.Replace(",", "")
        Else
            DR("JDA3_18AmountReceived") = DBNull.Value
        End If

        DR("JDA3_19Bil_Landing") = txtJDA3_19Bil_Landing.Text

        If IsNumeric(txtJDA3_20FOB_Value.Text.Replace(",", "")) Then
            DR("JDA3_20FOB_Value") = txtJDA3_20FOB_Value.Text.Replace(",", "")
        Else
            DR("JDA3_20FOB_Value") = DBNull.Value
        End If
        If IsNumeric(txtJDA3_21Insurance.Text.Replace(",", "")) Then
            DR("JDA3_21Insurance") = txtJDA3_21Insurance.Text.Replace(",", "")
        Else
            DR("JDA3_21Insurance") = DBNull.Value
        End If
        If IsNumeric(txtJDA3_22_Freight.Text.Replace(",", "")) Then
            DR("JDA3_22_Freight") = txtJDA3_22_Freight.Text.Replace(",", "")
        Else
            DR("JDA3_22_Freight") = DBNull.Value
        End If

        DR("JDA3_23Unit") = txtJDA3_23Unit.Text
        If IsNumeric(txtJDA3_23Gross_Wt.Text.Replace(",", "")) Then
            DR("JDA3_23Gross_Wt") = txtJDA3_23Gross_Wt.Text.Replace(",", "")
        Else
            DR("JDA3_23Gross_Wt") = DBNull.Value
        End If
        If IsNumeric(txtJDA3_24Measurement.Text.Replace(",", "")) Then
            DR("JDA3_24Measurement") = txtJDA3_24Measurement.Text.Replace(",", "")
        Else
            DR("JDA3_24Measurement") = DBNull.Value
        End If
        If IsNumeric(txtJDA3_25CIF_Value.Text.Replace(",", "")) Then
            DR("JDA3_25CIF_Value") = txtJDA3_25CIF_Value.Text.Replace(",", "")
        Else
            DR("JDA3_25CIF_Value") = DBNull.Value
        End If

        DR("JDA3_Currency") = txtJDA3_Currency.Text
        If IsNumeric(txtJDA3_PerUnit.Text.Replace(",", "")) Then
            DR("JDA3_PerUnit") = txtJDA3_PerUnit.Text.Replace(",", "")
        Else
            DR("JDA3_PerUnit") = DBNull.Value
        End If
        If IsNumeric(txtJDA3_Total.Text.Replace(",", "")) Then
            DR("JDA3_Total") = txtJDA3_Total.Text.Replace(",", "")
        Else
            DR("JDA3_Total") = DBNull.Value
        End If

        DR("JDA3_26Marks_Numbers") = txtJDA3_26Marks_Numbers.Text
        DR("JDA3_36Name_Declarant") = txtJDA3_36Name_Declarant.Text
        DR("JDA3_37IDCard_Passport") = txtJDA3_37IDCard_Passport.Text
        DR("JDA3_38Status") = txtJDA3_38Status.Text
        DR("JDA3_39To_Purper") = txtJDA3_39To_Purper.Text

        If GL.IsProgrammingDate(txtJDA3_39To_Purper_Date.Text, "dd-MMM-yyyy") Then
            DR("JDA3_39To_Purper_Date") = CV.StringToDate(txtJDA3_39To_Purper_Date.Text, "dd-MMM-yyyy")
        Else
            DR("JDA3_39To_Purper_Date") = DBNull.Value
        End If

        DR("JDA3_40RequestApproved") = txtJDA3_40RequestApproved.Text
        If GL.IsProgrammingDate(txtJDA3_40RequestApproved_Date.Text, "dd-MMM-yyyy") Then
            DR("JDA3_40RequestApproved_Date") = CV.StringToDate(txtJDA3_40RequestApproved_Date.Text, "dd-MMM-yyyy")
        Else
            DR("JDA3_40RequestApproved_Date") = DBNull.Value
        End If
        DR("JDA3_41Certified") = txtJDA3_41Certified.Text
        If GL.IsProgrammingDate(txtJDA3_41Certified_Date.Text, "dd-MMM-yyyy") Then
            DR("JDA3_41Certified_Date") = CV.StringToDate(txtJDA3_41Certified_Date.Text, "dd-MMM-yyyy")
        Else
            DR("JDA3_41Certified_Date") = DBNull.Value
        End If

        DR("JDA3_INV_NO") = txtINV_NO.Text
        DR("JDA3_TOTAL_SET") = txtTOTAL_SET.Text
        DT.Rows.Add(DR)
        Return DT
    End Function

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Insert"
                Dim DT As DataTable = CurrentDetail()
                Dim NewID As Object = DT.Compute("MAX(JDA3_Detail_ID)", "")
                If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
                Dim DR As DataRow = DT.NewRow
                DR("JDA3_Detail_ID") = NewID
                DT.Rows.InsertAt(DR, e.Item.ItemIndex)
                GridDatasource = DT
            Case "Delete"
                Dim DT As DataTable = CurrentDetail()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                    DT.AcceptChanges()
                Catch ex As Exception
                End Try
                GridDatasource = DT
                DisplayINV()

        End Select


    End Sub


    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound

        Dim txtRow As TextBox = e.Item.FindControl("txtRow")
        Dim lblJDA3_Detail_ID As Label = e.Item.FindControl("lblJDA3_Detail_ID")
        Dim txtJDA3_31Item_No As TextBox = e.Item.FindControl("txtJDA3_31Item_No")
        Dim txtJDA3_33Description_Goods As TextBox = e.Item.FindControl("txtJDA3_33Description_Goods")
        Dim txtJDA3_34CodeNo As TextBox = e.Item.FindControl("txtJDA3_34CodeNo")
        Dim txtJDA3_35Unit As TextBox = e.Item.FindControl("txtJDA3_35Unit")
        Dim txtJDA3_36CountryCode As TextBox = e.Item.FindControl("txtJDA3_36CountryCode")
        Dim txtJDA3_37Qtybased As TextBox = e.Item.FindControl("txtJDA3_37Qtybased")
        Dim txtJDA3_38PerUnit As TextBox = e.Item.FindControl("txtJDA3_38PerUnit")
        Dim txtJDA3_39Total As TextBox = e.Item.FindControl("txtJDA3_39Total")
        Dim txtJDA3_39Total_THB As TextBox = e.Item.FindControl("txtJDA3_39Total_THB")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim lblINV As Label = e.Item.FindControl("lblINV")

        TC.ImplementJavaMoneyText(txtJDA3_39Total)
        If Not IsDBNull(e.Item.DataItem("JDA3_Detail_ID")) Then
            lblJDA3_Detail_ID.Attributes("Detail_ID") = e.Item.DataItem("JDA3_Detail_ID")
        End If

        If Not IsDBNull(e.Item.DataItem("JDA3_31Item_No")) Then
            txtRow.Text = e.Item.DataItem("JDA3_31Item_No")
        End If

        If Not IsDBNull(e.Item.DataItem("JDA3_33Description_Goods")) Then
            txtJDA3_33Description_Goods.Text = (e.Item.DataItem("JDA3_33Description_Goods"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA3_34CodeNo")) Then
            txtJDA3_34CodeNo.Text = (e.Item.DataItem("JDA3_34CodeNo"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA3_35Unit")) Then
            txtJDA3_35Unit.Text = (e.Item.DataItem("JDA3_35Unit"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA3_36CountryCode")) Then
            txtJDA3_36CountryCode.Text = (e.Item.DataItem("JDA3_36CountryCode"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA3_37Qtybased")) Then
            txtJDA3_37Qtybased.Text = (e.Item.DataItem("JDA3_37Qtybased"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA3_38PerUnit")) Then
            txtJDA3_38PerUnit.Text = (e.Item.DataItem("JDA3_38PerUnit"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA3_39Total")) Then
            txtJDA3_39Total.Text = FormatNumber((e.Item.DataItem("JDA3_39Total")))
        End If
        'calculateLineTotal
        txtJDA3_39Total.Attributes("onchange") &= "calculateLineTotal();"
        If Not IsDBNull(e.Item.DataItem("JDA3_39Total_THB")) Then
            txtJDA3_39Total_THB.Text = FormatNumber((e.Item.DataItem("JDA3_39Total_THB")))
        End If
        txtJDA3_39Total_THB.Attributes("ReadOnly") = "True"
        lblINV.Text = e.Item.DataItem("JDA3_Invoice").ToString
        btnDelete.CommandArgument = e.Item.DataItem("JDA3_Detail_ID")
        txtRow.Attributes("ReadOnly") = "True"

        '----------- Implement Search Javascript----------------------
        Dim SearchScript As String = "requireTextboxDialog('Search_Detail.aspx?JDAType=3&txt1=" & txtJDA3_33Description_Goods.ClientID & "&txt2=" & txtJDA3_34CodeNo.ClientID & "&t=1',700,500,'','');"
        txtJDA3_33Description_Goods.Attributes("ondblclick") = SearchScript
        txtJDA3_34CodeNo.Attributes("ondblclick") = SearchScript
    End Sub

    Private Function CurrentDetail() As DataTable
        Dim SQL As String = ""
        SQL = "SELECT * FROM tb_JDA_3_Detail WHERE 0 =1 "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        For Each Item As RepeaterItem In rptData.Items
            If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For
            Dim txtRow As TextBox = Item.FindControl("txtRow")
            Dim lblJDA3_Detail_ID As Label = Item.FindControl("lblJDA3_Detail_ID")
            Dim txtJDA3_31Item_No As TextBox = Item.FindControl("txtJDA3_31Item_No")
            Dim txtJDA3_33Description_Goods As TextBox = Item.FindControl("txtJDA3_33Description_Goods")
            Dim txtJDA3_34CodeNo As TextBox = Item.FindControl("txtJDA3_34CodeNo")
            Dim txtJDA3_35Unit As TextBox = Item.FindControl("txtJDA3_35Unit")
            Dim txtJDA3_36CountryCode As TextBox = Item.FindControl("txtJDA3_36CountryCode")
            Dim txtJDA3_37Qtybased As TextBox = Item.FindControl("txtJDA3_37Qtybased")
            Dim txtJDA3_38PerUnit As TextBox = Item.FindControl("txtJDA3_38PerUnit")
            Dim txtJDA3_39Total As TextBox = Item.FindControl("txtJDA3_39Total")
            Dim txtJDA3_39Total_THB As TextBox = Item.FindControl("txtJDA3_39Total_THB")
            Dim txtJDA3_40Rate As TextBox = Item.FindControl("txtJDA3_40Rate")
            Dim txtJDA3_41Amount As TextBox = Item.FindControl("txtJDA3_41Amount")
            Dim txtJDA3_42Type As TextBox = Item.FindControl("txtJDA3_42Type")
            Dim txtJDA3_43Rate As TextBox = Item.FindControl("txtJDA3_43Rate")
            Dim txtJDA3_44Amount As TextBox = Item.FindControl("txtJDA3_44Amount")
            Dim btnDelete As ImageButton = Item.FindControl("btnDelete")
            Dim lblINV As Label = Item.FindControl("lblINV")

            Dim DR As DataRow = DT.NewRow

            DR("JDA3_Detail_ID") = lblJDA3_Detail_ID.Attributes("Detail_ID")
            If IsNumeric(txtRow.Text) Then
                DR("JDA3_31Item_No") = txtRow.Text
            End If
            DR("JDA3_33Description_Goods") = txtJDA3_33Description_Goods.Text
            DR("JDA3_34CodeNo") = txtJDA3_34CodeNo.Text
            DR("JDA3_35Unit") = txtJDA3_35Unit.Text
            DR("JDA3_36CountryCode") = txtJDA3_36CountryCode.Text
            DR("JDA3_37Qtybased") = txtJDA3_37Qtybased.Text
            DR("JDA3_38PerUnit") = txtJDA3_38PerUnit.Text

            If IsNumeric(txtJDA3_39Total.Text) Then
                DR("JDA3_39Total") = txtJDA3_39Total.Text.Replace(",", "")
            End If
            If IsNumeric(txtJDA3_39Total_THB.Text) Then
                DR("JDA3_39Total_THB") = txtJDA3_39Total_THB.Text.Replace(",", "")
            End If
            DR("JDA3_Invoice") = lblINV.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = CurrentDetail()
        Dim NewID As Object = DT.Compute("MAX(JDA3_Detail_ID)", "")
        If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
        Dim DR As DataRow = DT.NewRow
        DR("JDA3_Detail_ID") = NewID
        DT.Rows.Add(DR)
        GridDatasource = DT
    End Sub


    Private Function GetNew_JDA3_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(JDA3_ID),0)+1 FROM tb_JDA_3"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Function GetNew_JDA3_Detail_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(JDA3_Detail_ID),0)+1 FROM tb_JDA_3_Detail "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Function GetNew_JDA3_File_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(F_ID),0)+1 FROM tb_JDA_3_File"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function


    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim RS As New ReportSourceTransformer

        Dim ReportSource As DataTable = RS.TransformJDA3(CurrentHeader, CurrentDetail)
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        Session(UNIQUE_ID) = ReportSource
        Dim Script As String = "requireTextboxDialog('Print/ReportJDA_3.aspx?Mode=PDF&S=" & UNIQUE_ID & "',900,800,'','')"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", Script, True)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If IsNothing(Session("USER_ID")) Then Exit Sub
        '------------------- Save Header-----------------
        Dim SQL As String = ""
        SQL = "SELECT * FROM tb_JDA_3 WHERE JDA3_ID =" & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        Dim _id As Integer = JDA_ID
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR.ItemArray = CurrentHeader.Rows(0).ItemArray
            _id = GetNew_JDA3_ID()
            DR("JDA3_ID") = _id
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
            Dim Header As DataTable = CurrentHeader()
            For i As Integer = 1 To Header.Columns.Count - 5
                Dim Obj As Object = Header.Rows(0).Item(i)
                DR(i) = Obj
            Next
        End If

        DR("BAFCO_Job_No") = txt_BAFCO_Job_No.Text
        DR("Client_Reference_No") = txt_Client_Reference_No.Text
        DR("UPDATE_BY") = Session("User_ID")
        DR("UPDATE_DATE") = Now

        Dim CMD As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('" & ex.Message.Replace("'", "\'").Replace(vbLf, "\n") & "');", True)
            Exit Sub
        End Try


        JDA_ID = _id
        '--------------- Save Detail-----------------
        SQL = "DELETE FROM tb_JDA_3_Detail where JDA3_ID=" & JDA_ID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        SQL = "SELECT * FROM tb_JDA_3_Detail where 0=1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim Detail As DataTable = CurrentDetail()
        For i As Integer = 0 To Detail.Rows.Count - 1
            DR = DT.NewRow
            DR.ItemArray = Detail.Rows(i).ItemArray
            DR("JDA3_Detail_ID") = GetNew_JDA3_Detail_ID()
            DR("JDA3_ID") = JDA_ID
            'DR("JDA3_31Item_No") = i + 1
            DR("JDA3_Invoice") = Detail.Rows(i).Item("JDA3_Invoice").ToString
            DR("CREATE_BY") = Session("USER_ID")
            DR("CREATE_DATE") = Now
            DR("UPDATE_BY") = Session("USER_ID")
            DR("UPDATE_DATE") = Now
            DT.Rows.Add(DR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)
        Next
        '---------------- Save Invoice --------------
        SQL = "DELETE FROM tb_JDA_3_Invoice where JDA3_ID=" & JDA_ID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        SQL = "SELECT * FROM tb_JDA_3_Invoice where 0=1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim Invoice As DataTable = CurrentINV()
        For i As Integer = 0 To Invoice.Rows.Count - 1
            DR = DT.NewRow
            DR("JDA3_Row") = i + 1
            DR("JDA3_ID") = JDA_ID
            DR("JDA3_Invoice") = Invoice.Rows(i).Item("JDA3_Invoice").ToString
            DT.Rows.Add(DR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)
            'DT.AcceptChanges()
        Next
        '---------------- Save File--------------
        SQL = "DELETE FROM tb_JDA_3_File where JDA3_ID=" & JDA_ID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        SQL = "SELECT * FROM tb_JDA_3_File where 0=1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim FT As DataTable = CurrentFile()
        Dim FileList As List(Of GenericLib.FileStructure) = AttachedFileList
        '------------ Clear Folder------------
        Dim Folder As String = Server.MapPath("File/JDA3")
        If Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        End If
        Folder &= "\" & JDA_ID
        If FT.Rows.Count > 0 And Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        ElseIf FT.Rows.Count = 0 And Directory.Exists(Folder) Then '-------- Clear unused folder ----------
            GL.ForceDeleteFolder(Folder)
        End If
        '-------- Clear All File-----------
        GL.ForceDeleteFileInFolder(Folder)

        For i As Integer = 0 To FT.Rows.Count - 1
            '--------------- Check Session is Alive -----------
            Dim F As GenericLib.FileStructure = FileList(i)
            If IsNothing(F) Then Continue For

            Dim FR As DataRow = DT.NewRow
            Dim F_ID As Integer = GetNew_JDA3_File_ID()
            FR("F_ID") = F_ID
            FR("JDA3_ID") = JDA_ID
            FR("AbsoluteName") = F.AbsoluteName
            Select Case F.FileType
                Case GenericLib.AllowFileType.JPG
                    FR("FileType") = "image/jpeg"
                Case GenericLib.AllowFileType.PDF
                    FR("FileType") = "application/pdf"
                Case GenericLib.AllowFileType.PNG
                    FR("FileType") = "image/png"
                Case Else
                    FR("FileType") = ""
            End Select
            DT.Rows.Add(FR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)

            '------------ Save File To Folder--------------
            Dim FS As FileStream = File.Open(Folder & "\" & F_ID, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)
            FS.Write(F.Content, 0, F.Content.Length)
            FS.Close()
        Next

        ClearForm()
        BindData()
        BindFile()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Save successfully');", True)
    End Sub

    Protected Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        Dim Script As String = "calculateFOB(); calculateLineTotal();"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", Script, True)
    End Sub

    Private Sub BindFile()

        Dim DT As New DataTable
        Dim SQL As String = "SELECT * FROM tb_JDA_3_File WHERE JDA3_ID=" & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        Dim Folder As String = Server.MapPath("File/JDA3")
        If Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        End If
        Folder &= "\" & JDA_ID
        If Not Directory.Exists(Folder) Then Exit Sub

        '------------ Get File To Session ---------
        Dim LastIndex As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Path As String = Folder & "\" & DT.Rows(i).Item("F_ID")
            If Not File.Exists(Path) Then Continue For
            LastIndex += 1
            Dim F As New GenericLib.FileStructure
            Dim FS As FileStream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            F.Content = CV.StreamToByte(FS)
            FS.Close()
            F.AbsoluteName = DT.Rows(i).Item("AbsoluteName").ToString
            F.FileIndex = LastIndex
            Select Case DT.Rows(i).Item("FileType").ToString
                Case "image/jpeg"
                    F.FileType = GenericLib.AllowFileType.JPG
                Case "image/png"
                    F.FileType = GenericLib.AllowFileType.PNG
                Case Else
                    F.FileType = GenericLib.AllowFileType.PDF
            End Select

            Dim SName As String = "File_" & PageUniqueID & "_" & F.FileIndex
            Session(SName) = F
        Next

        DisplayFile()
    End Sub

    Private Sub DisplayFile()
        Dim DT As DataTable = CurrentFile()
        rptFile.DataSource = DT
        rptFile.DataBind()
    End Sub

    Private Function CurrentFile() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("AbsoluteName")
        DT.Columns.Add("FileType", GetType(Integer))
        DT.Columns.Add("FileIndex", GetType(Integer))

        Dim FileList As List(Of GenericLib.FileStructure) = AttachedFileList
        For Each F In FileList
            Dim DR As DataRow = DT.NewRow
            DR("AbsoluteName") = F.AbsoluteName
            DR("FileType") = F.FileType
            DR("FileIndex") = F.FileIndex
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Private Sub RemoveFile(ByVal FileName As String)
        For i As Integer = 1 To 100
            If Not Session("File_" & PageUniqueID & "_" & i) Then
                Dim F As GenericLib.FileStructure = Session("File_" & PageUniqueID & "_" & i)
                If F.AbsoluteName = FileName Then
                    Session("File_" & PageUniqueID & "_" & i) = Nothing
                End If
            End If
        Next
        DisplayFile()
    End Sub

    Private Sub RemoveFile(ByVal FileIndex As Integer)
        Session("File_" & PageUniqueID & "_" & FileIndex) = Nothing
        DisplayFile()
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        DisplayFile()
    End Sub

    Protected Sub rptFile_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptFile.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim alink As HtmlAnchor = e.Item.FindControl("alink")
                Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

                '-------------- Clear Session -------------
                Dim SName As String = "File_" & PageUniqueID & "_" & e.CommandArgument
                RemoveFile(CInt(e.CommandArgument))
                '-------------- Display File --------------
                DisplayFile()
        End Select
    End Sub

    Protected Sub rptFile_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFile.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim liType As HtmlGenericControl = e.Item.FindControl("liType")
        Dim alink As HtmlAnchor = e.Item.FindControl("alink")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Select Case e.Item.DataItem("FileType")
            Case GenericLib.AllowFileType.JPG, GenericLib.AllowFileType.PNG
                liType.Attributes("class") = "file_list picture"
            Case GenericLib.AllowFileType.PDF
                liType.Attributes("class") = "file_list pdf"
        End Select
        alink.InnerHtml = e.Item.DataItem("AbsoluteName")
        btnDelete.CommandArgument = e.Item.DataItem("FileIndex")
        Dim SName As String = "File_" & PageUniqueID & "_" & e.Item.DataItem("FileIndex")
        alink.HRef = "RenderFile.aspx?S=" & SName & "&"
    End Sub

#Region "Sung"
    'Add Line
    Protected Sub btnAddLine_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddLine.Click
        If txt_Total_Line.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Please enter total lines.');", True)
            Exit Sub
        End If
        Dim DT As DataTable = CurrentDetail()
        Dim TotalLine As Integer = 0
        If DT.Rows.Count > CInt(txt_Total_Line.Text) Then
            For i As Int32 = 1 To DT.Rows.Count - CInt(txt_Total_Line.Text)
                DT.Rows.RemoveAt(DT.Rows.Count - 1)
            Next
        Else
            TotalLine = CInt(txt_Total_Line.Text) - DT.Rows.Count
            Dim NewID As Object = DT.Compute("MAX(JDA3_Detail_ID)", "")
            If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
            For i As Int32 = 0 To TotalLine - 1
                Dim DR As DataRow = DT.NewRow
                DR("JDA3_Detail_ID") = NewID + i
                DT.Rows.Add(DR)
            Next
        End If

        GridDatasource = DT
    End Sub

    'Invoice
    Private Sub DisplayINV()
        Dim DT As New DataTable
        DT = CurrentDetail()
        DT.DefaultView.RowFilter = ""

        Dim col() As String = {"JDA3_Invoice"}
        Dim INV As DataTable = DT.DefaultView.ToTable(True, col)
        INV.DefaultView.RowFilter = "JDA3_Invoice <> ''"
        INV = INV.DefaultView.ToTable
        rptINV.DataSource = INV
        rptINV.DataBind()
    End Sub

    Private Sub BindINV()
        Dim SQL As String = ""
        SQL &= "SELECT JDA3_Invoice FROM tb_JDA_3_Invoice WHERE JDA3_ID = " & JDA_ID & vbNewLine
        SQL &= "ORDER BY JDA3_Row" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptINV.DataSource = DT
        rptINV.DataBind()
    End Sub

    Private Sub AddINV(ByVal INV As String)
        Dim DT As DataTable = CurrentINV()
        Dim DR As DataRow
        DR = DT.NewRow
        DR("JDA3_Invoice") = INV
        DT.Rows.Add(DR)
        rptINV.DataSource = DT
        rptINV.DataBind()
    End Sub

    Private Function CurrentINV() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("JDA3_Invoice")
        If rptINV.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptINV.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim INVlink As HtmlAnchor = ri.FindControl("INVlink")
                Dim DR As DataRow = DT.NewRow
                DR("JDA3_Invoice") = INVlink.InnerHtml.ToString
                DT.Rows.Add(DR)
            Next
        End If
        Return DT
    End Function
    Protected Sub btnAddInv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddInv.Click
        Dim DT As DataTable = CurrentINV()
        Dim INV As String = ""
        For i As Int32 = 0 To DT.DefaultView.Count - 1
            INV = INV & "'" & DT.Rows(i).Item("JDA3_Invoice").ToString & "',"
        Next
        If INV <> "" Then
            INV = INV.Substring(0, INV.Length - 1)
        End If
        Session("Inv") = INV
        Dim Script As String = "requireTextboxDialog('Inv_JDA_Finding.aspx',445,540,'" & txtTmpInv.ClientID & "','" & btnDialog.ClientID & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
    End Sub

    Protected Sub rptINV_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptINV.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim INVlink As HtmlAnchor = e.Item.FindControl("INVlink")
                Dim btnINVDelete As ImageButton = e.Item.FindControl("btnINVDelete")

                '------ INV ------
                Dim DT As New DataTable
                DT = CurrentINV()
                DT.DefaultView.RowFilter = "JDA3_Invoice <> '" & INVlink.InnerHtml.Replace("'", "''") & "'"
                DT = DT.DefaultView.ToTable
                rptINV.DataSource = DT
                rptINV.DataBind()
                '------ Item -------
                DT = New DataTable
                DT = CurrentDetail()
                DT.DefaultView.RowFilter = "JDA3_Invoice <> '" & INVlink.InnerHtml.Replace("'", "''") & "'"
                DT = DT.DefaultView.ToTable
                GridDatasource = DT

        End Select
    End Sub

    Protected Sub rptINV_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptINV.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim liINV As HtmlGenericControl = e.Item.FindControl("liINV")
        Dim INVlink As HtmlAnchor = e.Item.FindControl("INVlink")
        Dim btnINVDelete As ImageButton = e.Item.FindControl("btnINVDelete")
        liINV.Attributes("class") = "file_list invoice"
        INVlink.InnerHtml = e.Item.DataItem("JDA3_Invoice")

        'Dim SQL As String = "SELECT * FROM tb_JDA_3 WHERE JDA3_INV_NO='" & e.Item.DataItem("JDA3_Invoice").ToString.Replace("'", "''") & "'"
        'Dim Header As New DataTable
        'Dim DA As New SqlDataAdapter(SQL, ConnStr)
        'DA.Fill(Header)
        'SQL = "SELECT * FROM tb_JDA_3_Detail  WHERE JDA3_ID = (SELECT Top 1 JDA3_ID FROM tb_JDA_3 WHERE JDA3_INV_NO='" & e.Item.DataItem("JDA3_Invoice").ToString.Replace("'", "''") & "')" & vbCrLf
        'SQL &= "ORDER BY JDA3_Detail_ID"
        'Dim Detail As New DataTable
        'DA = New SqlDataAdapter(SQL, ConnStr)
        'DA.Fill(Detail)
        'Dim RS As New ReportSourceTransformer
        'Dim ReportSource As DataTable = RS.TransformJDA3(Header, Detail)
        'Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        'Session(UNIQUE_ID) = ReportSource
        'Dim Script As String = "requireTextboxDialog('Print/ReportJDA_3.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,500,'','')"
        'INVlink.Attributes("onclick") = Script

        Dim SQL As String = "SELECT * FROM tb_JDA_1 WHERE JDA1_INV_NO='" & e.Item.DataItem("JDA3_Invoice").ToString.Replace("'", "''") & "'"
        Dim Header As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Header)
        If Header.Rows.Count > 0 Then
            SQL = "SELECT * FROM tb_JDA_1_Detail  WHERE JDA1_ID = (SELECT Top 1 JDA1_ID FROM tb_JDA_1 WHERE JDA1_INV_NO='" & e.Item.DataItem("JDA3_Invoice").ToString.Replace("'", "''") & "')" & vbCrLf
            SQL &= "ORDER BY JDA1_Detail_ID"
            Dim Detail As New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(Detail)
            Dim RS As New ReportSourceTransformer
            Dim ReportSource As DataTable = RS.TransformJDA1(Header, Detail)
            Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
            Session(UNIQUE_ID) = ReportSource
            Dim Script As String = "requireTextboxDialog('Print/ReportJDA_1.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,500,'','')"
            INVlink.Attributes("onclick") = Script
        Else
            SQL = "SELECT * FROM tb_JDA_3 WHERE JDA3_INV_NO='" & e.Item.DataItem("JDA3_Invoice").ToString.Replace("'", "''") & "'"
            Header = New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(Header)
            SQL = "SELECT * FROM tb_JDA_3_Detail  WHERE JDA3_ID = (SELECT Top 1 JDA3_ID FROM tb_JDA_3 WHERE JDA3_INV_NO='" & e.Item.DataItem("JDA3_Invoice").ToString.Replace("'", "''") & "')" & vbCrLf
            SQL &= "ORDER BY JDA3_Detail_ID"
            Dim Detail As New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(Detail)
            Dim RS As New ReportSourceTransformer
            Dim ReportSource As DataTable = RS.TransformJDA3(Header, Detail)
            Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
            Session(UNIQUE_ID) = ReportSource
            Dim Script As String = "requireTextboxDialog('Print/ReportJDA_3.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,500,'','')"
            INVlink.Attributes("onclick") = Script
        End If
    End Sub

    Protected Sub btnDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDialog.Click
        'Dim DT As New DataTable
        'DT = CurrentDetail()

        'Dim SQL As String = ""
        'SQL &= "SELECT DT.*,'" & txtTmpInv.Text.Trim.Replace("'", "''") & "' as INV" & vbNewLine
        'SQL &= "FROM tb_JDA_3 HD LEFT JOIN tb_JDA_3_Detail DT ON HD.JDA3_ID = DT.JDA3_ID" & vbNewLine
        'SQL &= "WHERE RTRIM(LTRIM(HD.JDA3_INV_NO)) = '" & txtTmpInv.Text.Trim.Replace("'", "''") & "'" & vbNewLine
        'Dim DA As New SqlDataAdapter(SQL, ConnStr)
        'Dim DT_INV As New DataTable
        'DA.Fill(DT_INV)

        'For i As Int32 = 0 To DT_INV.Rows.Count - 1
        '    'DT.DefaultView.RowFilter = "JDA3_33Description_Goods = '" & DT_INV.Rows(i).Item("JDA3_33Description_Goods").ToString.Replace("'", "''") & "'"
        '    'If DT.DefaultView.Count = 0 Then
        '    Dim DR As DataRow
        '    DR = DT.NewRow

        '    Dim NewID As Object = DT.Compute("MAX(JDA3_Detail_ID)", "")
        '    If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
        '    DR("JDA3_31Item_No") = DBNull.Value
        '    DR("JDA3_Detail_ID") = NewID + i
        '    DR("JDA3_33Description_Goods") = DT_INV.Rows(i).Item("JDA3_33Description_Goods")
        '    DR("JDA3_34CodeNo") = DT_INV.Rows(i).Item("JDA3_34CodeNo")
        '    DR("JDA3_35Unit") = DT_INV.Rows(i).Item("JDA3_35Unit")
        '    DR("JDA3_36CountryCode") = DT_INV.Rows(i).Item("JDA3_36CountryCode")
        '    DR("JDA3_37Qtybased") = DBNull.Value
        '    DR("JDA3_38PerUnit") = DBNull.Value
        '    DR("JDA3_39Total") = DBNull.Value
        '    DR("JDA3_39Total_THB") = DBNull.Value
        '    DR("JDA3_Invoice") = txtTmpInv.Text
        '    DT.Rows.Add(DR)
        '    'End If
        '    'DT.DefaultView.RowFilter = ""
        'Next
        'GridDatasource = DT
        'AddINV(txtTmpInv.Text)

        Dim DT As New DataTable
        DT = CurrentDetail()

        Dim SQL As String = ""
        SQL &= "SELECT DT.*,'" & txtTmpInv.Text.Trim.Replace("'", "''") & "' as INV" & vbNewLine
        SQL &= "FROM tb_JDA_1 HD LEFT JOIN tb_JDA_1_Detail DT ON HD.JDA1_ID = DT.JDA1_ID" & vbNewLine
        SQL &= "WHERE RTRIM(LTRIM(HD.JDA1_INV_NO)) = '" & txtTmpInv.Text.Trim.Replace("'", "''") & "'" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_INV As New DataTable
        DA.Fill(DT_INV)
        If DT_INV.Rows.Count > 0 Then
            For i As Int32 = 0 To DT_INV.Rows.Count - 1

                Dim DR As DataRow
                DR = DT.NewRow

                Dim NewID As Object = DT.Compute("MAX(JDA3_Detail_ID)", "")
                If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
                DR("JDA3_31Item_No") = DBNull.Value
                DR("JDA3_Detail_ID") = NewID + i
                DR("JDA3_33Description_Goods") = DT_INV.Rows(i).Item("JDA1_33Description_Goods")
                DR("JDA3_34CodeNo") = DT_INV.Rows(i).Item("JDA1_34CodeNo")
                DR("JDA3_35Unit") = DT_INV.Rows(i).Item("JDA1_35Unit")
                DR("JDA3_36CountryCode") = DT_INV.Rows(i).Item("JDA1_36CountryCode")
                DR("JDA3_37Qtybased") = DBNull.Value
                DR("JDA3_38PerUnit") = DBNull.Value
                DR("JDA3_39Total") = DBNull.Value
                DR("JDA3_39Total_THB") = DBNull.Value
                DR("JDA3_Invoice") = txtTmpInv.Text
                DT.Rows.Add(DR)

            Next
            GridDatasource = DT
            AddINV(txtTmpInv.Text)
        Else
            SQL = ""
            SQL &= "SELECT DT.*,'" & txtTmpInv.Text.Trim.Replace("'", "''") & "' as INV" & vbNewLine
            SQL &= "FROM tb_JDA_3 HD LEFT JOIN tb_JDA_3_Detail DT ON HD.JDA3_ID = DT.JDA3_ID" & vbNewLine
            SQL &= "WHERE RTRIM(LTRIM(HD.JDA3_INV_NO)) = '" & txtTmpInv.Text.Trim.Replace("'", "''") & "'" & vbNewLine
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT_INV = New DataTable
            DA.Fill(DT_INV)

            For i As Int32 = 0 To DT_INV.Rows.Count - 1
                Dim DR As DataRow
                DR = DT.NewRow
                Dim NewID As Object = DT.Compute("MAX(JDA3_Detail_ID)", "")
                If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
                DR("JDA3_31Item_No") = DBNull.Value
                DR("JDA3_Detail_ID") = NewID + i
                DR("JDA3_33Description_Goods") = DT_INV.Rows(i).Item("JDA3_33Description_Goods")
                DR("JDA3_34CodeNo") = DT_INV.Rows(i).Item("JDA3_34CodeNo")
                DR("JDA3_35Unit") = DT_INV.Rows(i).Item("JDA3_35Unit")
                DR("JDA3_36CountryCode") = DT_INV.Rows(i).Item("JDA3_36CountryCode")
                DR("JDA3_37Qtybased") = DBNull.Value
                DR("JDA3_38PerUnit") = DBNull.Value
                DR("JDA3_39Total") = DBNull.Value
                DR("JDA3_39Total_THB") = DBNull.Value
                DR("JDA3_Invoice") = txtTmpInv.Text
                DT.Rows.Add(DR)
            Next
            GridDatasource = DT
            AddINV(txtTmpInv.Text)
        End If
    End Sub
#End Region
End Class

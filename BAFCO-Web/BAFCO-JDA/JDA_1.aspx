<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="JDA_1.aspx.vb" Inherits="JDA_1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="MasterEditor.ascx" tagname="MasterEditor" tagprefix="uc1" %>
<%@ Register src="MasterEditor_MultiLine.ascx" tagname="MasterEditor_MultiLine" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >

    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellspacing="0" style="background: gray; width: 100%; ">
		        <tr>
		            <td colspan="2" style="font-size: 16px; height:30px; font-weight:bold; color:White; padding-left:10px;" align="center" valign="middle">
		                CUSTOMS DECLARATION FROM FOR IMPORTATION OF GOODS INTO JOINT DEVELOPMENT AREA (JDA1)
		            <asp:Label ID="lblJDA1_ID" runat="server"  style="display:none;"></asp:Label>
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 14px; height:40px; color:White; padding-left:10px;" align="left" valign="middle">		            
		                BAFCO Job No. <asp:TextBox ID="txt_BAFCO_Job_No" runat="server" MaxLength="100" style="text-align:center; font-weight:bold;"></asp:TextBox>
		            </td>
		            <td style="font-size: 14px; height:40px; color:White;  padding-right:10px;" align="right" valign="middle">		            
		                Client Reference No. <asp:TextBox ID="txt_Client_Reference_No" runat="server" MaxLength="100" style="text-align:center; font-weight:bold;"></asp:TextBox>
		            </td>
		        </tr>
            </table>
        
        <asp:Panel ID="pnlForm" runat="server" DefaultButton="btnCalculate">
        <asp:Button ID="btnCalculate" runat="server" style="display:none;" />
                

               <table width="1000" border="0" cellpadding="2" cellspacing="0" class="MainTable">
                  <tr>
                    <td colspan="4" valign="top">1. Consignor Export (Name and Address)</td>
                    <td colspan="4" align="center" style="border-top-width:2px; border-left-width:3px; border-right:2px solid #3c3; font-weight:bold; font-size:14px;">FOR OFFICIAL USE</td>
                  </tr>
                  <tr>
                    <td colspan="4" rowspan="3" valign="top" style="border-top:none;">    	                
    	                    <div class="wrapper" style="height:100px; width:470px; z-index:20;">
		                        <div class="accordionButton StrechControl">
    		                    	<asp:TextBox ID="txtJDA1_1Consignor_Export" runat="server" TextMode="MultiLine"
		                            CssClass="StrechControl"></asp:TextBox>
    		                    </div>
		                        <div class="accordionContent">		                        
		                            <asp:Repeater ID="lst01" runat="server" >
		                                <ItemTemplate>
		                                    <div ID="divItem" runat="server">    			                                
		                                    </div>
		                                </ItemTemplate>
		                            </asp:Repeater>		                            
		                        </div>
	                        </div>	                    
                    </td>
                    <td colspan="2" style="border-left-width:2px;">10. Date and Time of Receipt </td>
                    <td colspan="2" style="border-right:2px solid #3c3;">11. Documents Attached</td>
                  </tr>
                  <tr>
                    <td colspan="2" style="border-top:none; border-left-width:2px;">
                    <asp:TextBox ID="txtJDA1_10DateTime_Receipt" runat="server" CssClass="StrechControl" style="height:90px;"></asp:TextBox>
                    <Ajax:CalendarExtender ID="clJDA1_10DateTime_Receipt" runat="server" 
                    Format="dd-MMM-yyyy" BehaviorID="txtJDA1_10DateTime_Receipt" TargetControlID="txtJDA1_10DateTime_Receipt"></Ajax:CalendarExtender>
                    </td>
                    <td colspan="2" style="border-top:none; border-top-width:2px; border-right:2px solid #3c3;">
                    
                        <asp:CheckBox id="ckJDA1_11Attached_Invoice" runat="server" />Invoice<br>
                        <asp:CheckBox id="ckJDA1_11Attached_Bill" runat="server" /> Bill of Landing<br>
                        <asp:CheckBox id="ckJDA1_11Attached_Letter" runat="server" />  Letter of Credit<br>
                        <asp:CheckBox id="ckJDA1_11Attached_Insurance" runat="server" /> Insurance Certificate <br>
                        <asp:CheckBox id="ckJDA1_11Attached_Others" runat="server" /> Others
                    
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" rowspan="2" style="border-left:2px solid #3c3; vertical-align:top;">12. Registration Number </td>
                    <td colspan="2" rowspan="2" style="border-right:2px solid #3c3; vertical-align:top;">13. Name of Customs Office </td>
                  </tr>
                  <tr>
                    <td colspan="2">2. Consignee/Importer (Name and Address) </td>
                    <td colspan="2" style="border-bottom:1px solid #3c3;"><asp:TextBox ID="txtJDA1_2Consignee_Importer_Code" runat="server" CssClass="StrechControl" ></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td colspan="4" rowspan="5" style="border-top:none; text-align:left; vertical-align:top; ">
                        <div>
                        <div class="wrapper" style="height:130px; width:470px; z-index:15;">
		                        <div class="accordionButton StrechControl">
    		                    	<asp:TextBox ID="txtJDA1_2Consignee_Importer" runat="server" CssClass="StrechControl" 
    		                    	TextMode="MultiLine" style="height:140px;"></asp:TextBox>
    		                    </div>
		                        <div class="accordionContent" style="z-index:18;">  	                        
		                            <asp:Repeater ID="lst02" runat="server" >
		                                <ItemTemplate>
		                                    <div ID="divItem" runat="server">
		                                    </div>
		                                </ItemTemplate>
		                            </asp:Repeater>		                            
		                        </div>
	                        </div>
	                       </div>
                    </td>
                    <td colspan="2" rowspan="2" style="border-top:none; border-left:2px solid #3c3;">
                    <asp:TextBox ID="txtJDA1_12Registration_Number" runat="server" CssClass="StrechControl" style="height:80px; vertical-align:middle !important; text-align:center !important;" ></asp:TextBox>
                    </td>
                    <td colspan="2" style="border-top:none; border-right:2px solid #3c3;">
                      <asp:TextBox ID="txtJDA1_13Name_CustomsOffice" runat="server" CssClass="StrechControl" style="height:50px;" ></asp:TextBox></td>
                    </tr>
                  <tr>
                    <td align="right" style="border-top:none;">Code</td>
                    <td style="border-right:2px solid #3c3;"><asp:TextBox ID="txtJDA1_13Name_CustomsOffice_Code" runat="server" CssClass="StrechControl" ></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td colspan="2" style="border-left:2px solid #3c3;">14. Manifest Registration No. </td>
                    <td colspan="2" style="border-right:2px solid #3c3;">15. Receipt of Duty/Tax as levied authorised by :</td>
                  </tr>
                  <tr>
                    <td colspan="2" style="border-top:none; border-left:2px solid #3c3;">
                    <asp:TextBox ID="txtJDA1_14Manifest_Regist_No" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td colspan="2" rowspan="2" style="border-top:none; border-right:2px solid #3c3; border-bottom:2px solid #3c3;">
                    
                    <asp:TextBox ID="txtJDA1_15Receipt_Duty" runat="server" CssClass="StrechControl" TextMode="MultiLine" style="height:50px; vertical-align:middle !important; text-align:center !important;"></asp:TextBox>
                    
                    </td>
                  </tr>
                  <tr>
                    <td align="right" style="border-top:none; border-left:2px solid #3c3; border-bottom:2px solid #3c3;">
                    <Ajax:CalendarExtender ID="clJDA1_15Receipt_Duty_Date" runat="server" 
                        Format="dd-MMM-yyyy"  BehaviorID="txtJDA1_15Receipt_Duty_Date"
                        TargetControlID="txtJDA1_15Receipt_Duty_Date"></Ajax:CalendarExtender>
                    Date</td>
                    <td style="border-bottom:2px solid #3c3;">
                    <asp:TextBox ID="txtJDA1_15Receipt_Duty_Date" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="border-bottom:none;">3. Name and Address of Authorised Agent</td>
                    <td colspan="2" style="border-bottom:1px solid #3c3;">
    	                <asp:TextBox ID="txtJDA1_3Authorised_Agent_Code" runat="server" CssClass="StrechControl" ></asp:TextBox>
                    </td>
                    <td colspan="2">16. Import Permit No.</td>
                    <td colspan="2">17.Exchange Control Ref. </td>
                  </tr>
                  <tr>
                    <td colspan="4" rowspan="6" style="border-top:none; text-align:left; vertical-align:top;">
                        <div>
                         <div class="wrapper" style="height:110px; width:470px; z-index:10;">
		                        <div class="accordionButton StrechControl">
    		                    	<asp:TextBox ID="txtJDA1_3Authorised_Agent" runat="server" 
    		                    	CssClass="StrechControl" TextMode="MultiLine" style="height:140px;" ></asp:TextBox>
    		                    </div>
		                        <div class="accordionContent" style="z-index:13;">  	                        
		                            <asp:Repeater ID="lst03" runat="server" >
		                                <ItemTemplate>
		                                    <div ID="divItem" runat="server">
		                                    </div>
		                                </ItemTemplate>
		                            </asp:Repeater>		                            
		                        </div>
	                        </div>
                        </div>
                    </td>
                    <td colspan="2" style="border-top:none;">
                    <asp:TextBox ID="txtJDA1_16ImportPermit_No" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td colspan="2" style="border-top:none;">
                    <asp:TextBox ID="txtJDA1_17ExchangeControl_Ref" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">18. Special Treatment </td>
                    <td colspan="2">19. Bill of Landing or Consignment </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="border-top:none;">
                    <asp:TextBox ID="txtJDA1_18Special_Treatment" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td colspan="2" style="border-top:none;">
                    <asp:TextBox ID="txtJDA1_19Bil_Landing" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">20. Terms of Delivery and Payment </td>
                    <td colspan="2">21. Country to which payment of Goods to be made </td>
                  </tr>
                  <tr>
                    <td colspan="2" rowspan="2" style="border-top:none;">
                    <asp:TextBox ID="txtJDA1_20Terms_Delivery" runat="server" CssClass="StrechControl" TextMode="MultiLine" style="height:60px; vertical-align:middle !important; text-align:center !important;" ></asp:TextBox>
                    </td>
                    <td colspan="2" style="border-top:none;">
                    <asp:DropDownList ID="ddlJDA1_21Country_payment" runat="server" CssClass="StrechControl" AutoPostBack="true">
                    	 <asp:ListItem Value="" Text=""></asp:ListItem>
                    </asp:DropDownList>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" style="border-top:none;">Code</td>
                    <td ><asp:TextBox ID="txtJDA1_21Country_payment_Code" runat="server" CssClass="StrechControl"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td >4. Mode of Transport </td>
                    <td style="border-left:none;">1. Sea </td>
                    <td style="border-left:none;">2. Rail</td>
                    <td>5. Date of Import </td>
                    <td colspan="2">22. Exchange Rate</td>
                    <td colspan="2">23. FOB Value </td>
                  </tr>
                  <tr>
                    <td rowspan="2" style="border-top:none;">
                            <asp:DropDownList ID="ddlJDA1_4Mode_Transport" runat="server" AutoPostBack="True" CssClass="StrechControl" style="height:50px;" >
                                <asp:ListItem Value="" Text=""></asp:ListItem>
                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            </asp:DropDownList>
                    </td>
                    <td style="border-top:none; border-left:none;">3. Road</td>
                    <td style="border-top:none; border-left:none;">4. Air</td>
                    <td rowspan="2" style="border-top:none;">
    	                <asp:TextBox ID="txtJDA1_5Date_Import" runat="server" CssClass="StrechControl" style="height:50px;"></asp:TextBox>
                         <Ajax:CalendarExtender ID="clJDA1_5Date_Import_Date" runat="server" 
                        Format="dd.MM.yyyy"  BehaviorID="txtJDA1_5Date_Import"
                        TargetControlID="txtJDA1_5Date_Import"></Ajax:CalendarExtender>
                    </td>
                    <td rowspan="2" style="border-top:none; ">
                        <asp:DropDownList ID="ddlJDA1_22Currency" runat="server" CssClass="StrechControl" style="height:50px;" AutoPostBack="true">
                        <asp:ListItem Value="" Text=""></asp:ListItem>                        
                    </asp:DropDownList>
                    </td>
                    <td rowspan="2" style="border-top:none; border-left:none;"><asp:TextBox ID="txtJDA1_22Exchange_Rate" runat="server" CssClass="StrechControl" style="height:50px;" ></asp:TextBox></td>
                    <td colspan="2" rowspan="2" style="border-top:none;">
                        <input type="text" id="txtCurrency1" runat="server" class="StrechControl" style="width:25%; height:50px;"/>
                        <asp:TextBox ID="txtJDA1_23FOB_Value" runat="server" CssClass="StrechControl" style="height:50px; width:70%;"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td style="border-top:none; border-left:none;">10. Others</td>
                    <td style="border-top:none; border-left:none;">
                    <asp:TextBox ID="txtJDA1_10Others_Specify" runat="server" CssClass="StrechControl"  PlaceHolder="Specify"
                    style="border-left: 1px solid #3C3; border-top: 1px solid #3C3;" ></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">6. No./Name of Vessel/Flight/Conveyance</td>
                    <td colspan="2">7. Port/Place of Import</td>
                    <td>24. Insurance </td>
                    <td>25. Freight </td>
                    <td colspan="2">26. CIF Value </td>
                  </tr>
                  <tr>
                    <td colspan="2" style="border-top:none;">
                    <asp:DropDownList ID="ddlJDA1_6Vessel_Flight_Conveyance" runat="server" CssClass="StrechControl" AutoPostBack="true">
                    	<asp:ListItem Value="" Text=""></asp:ListItem>
                    </asp:DropDownList>
                    </td>
                    <td style="border-top:none;">
                     <asp:DropDownList ID="ddlJDA1_7Place_Import" runat="server" CssClass="StrechControl" AutoPostBack="true">
                            <asp:ListItem Value="" Text=""></asp:ListItem>
                            <asp:ListItem Value="SONGKLA" Text="SONGKLA"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td style=""><asp:TextBox ID="txtJDA1_7Place_Import_Code" runat="server" CssClass="StrechControl" PlaceHolder="Code"></asp:TextBox></td>
                    <td style="border-top:none; width:120px;"><!---��Ѻ��Ҵ Column---->
                      <input type="text" id="txtCurrency2" runat="server" class="StrechControl" style="width:30px;"/>
                      <asp:TextBox ID="txtJDA1_24Insurance" runat="server" CssClass="StrechControl" Width="85px"></asp:TextBox>                                              
                    </td>
                    <td style="border-top:none; width:140px;"><!---��Ѻ��Ҵ Column---->
    	                <input type="text" id="txtCurrency3" runat="server" class="StrechControl" style="width:40px;"/>
    	                <asp:TextBox ID="txtJDA1_25_Freight" runat="server" CssClass="StrechControl" Width="90px"></asp:TextBox>
                    </td>
                    <td colspan="2" style="border-top:none;">
    	                <input type="text" id="txtCurrency4" runat="server" class="StrechControl" style="width:25%"/>
    	                <asp:TextBox ID="txtJDA1_26CIF_Value" runat="server" CssClass="StrechControl" Width="70%"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">8. Port/Place of Loading </td>
                    <td colspan="2">9. Via(transhipment cargo only)</td>
                    <td>27. Gross Wt. (Kg.) </td>
                    <td>28. Measurement (m3) </td>
                    <td colspan="2">29. Other charges </td>
                  </tr>
                  <tr>
                    <td style="border-top:none;">
                     <asp:DropDownList ID="ddlJDA1_8Place_Loading" runat="server" CssClass="StrechControl" AutoPostBack="true">
                        <asp:ListItem Value="" Text=""></asp:ListItem>
                        <asp:ListItem Value="SINGAPORE" Text="SINGAPORE"></asp:ListItem>
                      </asp:DropDownList></td>
                    <td style=""><asp:TextBox ID="txtJDA1_8Place_Loading_Code" runat="server" PlaceHolder="Code" CssClass="StrechControl"></asp:TextBox></td>
                    <td style="border-top:none;">
                    <asp:TextBox ID="txtJDA1_9Via" runat="server" CssClass="StrechControl" style="height:30px;" AutoPostBack="true"></asp:TextBox></td>
                    <td style=""><asp:TextBox ID="txtJDA1_9Via_Code" runat="server" CssClass="StrechControl" PlaceHolder="Code"></asp:TextBox></td>
                    <td style="border-top:none; width:120px;">
                        
                        <asp:TextBox ID="txtJDA1_27Unit" runat="server" CssClass="StrechControl" Width="30px"></asp:TextBox>
                        <asp:TextBox ID="txtJDA1_27Gross_Wt" runat="server" CssClass="StrechControl" Width="85px"></asp:TextBox>
                    </td>
                    <td style="border-top:none;">
                      <asp:TextBox ID="txtJDA1_28Measurement" runat="server" CssClass="StrechControl"></asp:TextBox></td>
                    <td style="border-top:none;">
                        <asp:DropDownList ID="ddlJDA1_29Currency" runat="server" CssClass="StrechControl" style="" AutoPostBack="true">
                          
                        </asp:DropDownList>
                    </td>
                    <td style="border-top:none; border-left:none;"><asp:TextBox ID="txtJDA1_29Other_charges" runat="server" CssClass="StrechControl"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td colspan="8">
                    <table width="1000" border="0" cellspacing="0" cellpadding="2" class="MainTable" style="border-bottom:none; border-right:none; border-left:none;">
                      <tr>
                        <td width="30" rowspan="2" align="center" style="border-left:none;">Item <br />
                          No. </td>
                        <td width="100" rowspan="2" align="center">No. and Type <br />
                          of Packages</td>
                        <td width="150" rowspan="2" align="center">Description of Goods</td>
                        <td colspan="2" align="center">Customs Tariff </td>
                        <td width="40" rowspan="2" align="center">Country of <br />
                          Origin Code</td>
                        <td width="50" rowspan="2" align="center">Qty based on <br />
                          Customs Tariff Unit</td>
                        <td colspan="2" align="center">Value </td>
                        <td colspan="2" align="center">Import Duty</td>
                        <td colspan="3" align="center">Other Taxes Payable</td>
                        <td width="20" rowspan="2" align="center">Ins</td>
                        <td width="20" rowspan="2" align="center">Del</td>                        
                      </tr>
                      <tr>
                        <td width="150" align="center">Code No. </td>
                        <td width="50" align="center">Unit</td>
                        <td width="30" align="center">Per Unit </td>
                        <td width="80" align="center">Total</td>
                        <td width="40" align="center">Rate %</td>
                        <td width="80" align="center">Amount</td>
                        <td width="30" align="center">Type</td>
                        <td width="40" align="center">Rate %</td>
                        <td width="40" align="center">Amount</td>
                        </tr>
                       <asp:Repeater ID="rptData" runat="server">
                       <ItemTemplate>
                      <tr>
                        <td rowspan="2" style="border-left:none; width:30px;" class="StrechControl">
        	                <asp:TextBox ID="txtRow" runat ="server" style="width:30px; text-align:center; border:none;" ></asp:TextBox>
                            <asp:Label ID="lblJDA1_Detail_ID" runat="server"  style="display:none;"></asp:Label>
                            <asp:Label ID="lblINV" runat="server"  style="display:none;"></asp:Label>
                        </td>
                        <td width="100" rowspan="2" colspan="2">
        	                <asp:TextBox ID="txtJDA1_33Description_Goods" runat="server" height="50px" CssClass="StrechControl" Style="text-align:left; cursor:pointer;" title="Double Click to search archive description and code."></asp:TextBox>
                        </td>
                        <td width="150" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_34CodeNo" runat="server" height="50px" CssClass="StrechControl" Style="cursor:pointer;" title="Double Click to search archive description and code."></asp:TextBox>
                        </td>
                        <td width="50" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_35Unit" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="40" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_36CountryCode" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="50" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_37Qtybased" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="30">
        	                <asp:TextBox ID="txtJDA1_38PerUnit" runat="server"  CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="80"><asp:TextBox ID="txtJDA1_39Total" runat="server"  CssClass="StrechControl"></asp:TextBox></td>
                        <td width="40" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_40Rate" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="80" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_41Amount" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="30" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_42Type" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="40" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_43Rate" runat="server" height="50px"  CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="40" rowspan="2">
        	                <asp:TextBox ID="txtJDA1_44Amount" runat="server" height="50px" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td width="20" rowspan="2">
        	                <asp:ImageButton ID="btnInsert" CommandName="Insert" runat="server" ToolTip="Click to insert record" ImageUrl="images/insert.png" Width="20px" />
                        </td>
                        <td width="20" rowspan="2">
        	                <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Click to delete" ImageUrl="images/icon/57.png" Width="20px" />
                        </td>
                        
                      </tr>
                      <tr>
                        <td width="30" style="width:auto;"><input type="Text" class="StrechControl" value="THB" ReadOnly="True" /></td>
                        <td width="80">
                        <%--<asp:TextBox ID="txtJDA1_39THB" runat="server"  CssClass="StrechControl" style="text-align:right;"></asp:TextBox>--%>
                        <asp:TextBox ID="txtJDA1_39THB" runat="server"  CssClass="StrechControl" style="text-align:right;"></asp:TextBox>
                        </td>
                      </tr>
                      </ItemTemplate>
                      </asp:Repeater>
                       <tr>
                        <td colspan="6" rowspan="2" align="left" style="border-left:none;">
                          <asp:Panel ID="pnlAdd" runat="server" DefaultButton="btnAddLine">
                          <asp:Button ID="btnAdd" runat="server" CssClass="Button_Red" Text="Add" style="display:none;"/>
                          Total :&nbsp;
                          <asp:TextBox ID="txt_Total_Line" runat="server" Text="0" style="text-align:center; width:60px;"></asp:TextBox>
                          &nbsp;Lines 
                          &nbsp;&nbsp;&nbsp;<asp:Button ID="btnAddLine" runat="server" CssClass="Button_Red" Text="Ok"/>
                          </asp:Panel>
                       </td>
                        <td align="center" rowspan="2" style="border-left:none;">Total</td>
                        <td align="right">
                        <asp:TextBox ID="txtJDA1_39_Currency" runat="server" CssClass="StrechControl"></asp:TextBox>
                        </td>
                        <td align="right"><asp:TextBox ID="txtJDA1_39_PerUnit" runat="server" CssClass="StrechControl" style="text-align:right;"></asp:TextBox></td>
                        <td width="30" rowspan="2" align="center">Total </td>
                        <td width="100" rowspan="2"><asp:TextBox ID="txtJDA1_41Amount" runat="server" CssClass="StrechControl" style="text-align:right; height:50px;"></asp:TextBox></td>
                        <td colspan="2" rowspan="2" align="center">Total </td>
                        <td width="40"  rowspan="2"><asp:TextBox ID="txtJDA1_44Amount" runat="server" CssClass="StrechControl" style="text-align:right; height:50px;"></asp:TextBox>
                        </td>
                        <td width="20" colspan="2" rowspan="2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td><input type="text" class="StrechControl" value="THB" /></td>
                        <td><asp:TextBox ID="txtJDA1_39Total" runat="server" CssClass="StrechControl" style="text-align:right;"></asp:TextBox></td>
                      </tr>
                </table>
                    
                    </td>
                    </tr>
                  <tr>
                    <td rowspan="2" >30. Marks and Numbers</td>
                    <td colspan="2" rowspan="2" style="border-left:none;" ><asp:TextBox ID="txtJDA1_30Marks_Numbers" runat="server" CssClass="StrechControl"  style="font-size:14px; height:50px; text-align:center !important;" TextMode="MultiLine"></asp:TextBox></td>
                    <td style="font-weight:bold; font-size:14px; color:Red; border-right:1px solid #3c3;">&nbsp;&nbsp;&nbsp;INV.NO.</td>
                    <td colspan="4" style="border-left:none">
                      <table width="100%">
                        <tr>
                            <td colspan="2" style="border:none; height:25px;">
                              <asp:TextBox ID="txtINV_NO" runat="server" CssClass="StrechControl" style="font-size:14px; color:red ; text-align:left;"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="border:none; border-top:1px solid #3c3; height:25px; width:30px;">
                              <asp:Button ID="btnAddInv" runat="server" CssClass="Button_Red" Text="Add"/>
                              <asp:TextBox ID="txtTmpInv" runat="server" style="display:none;"></asp:TextBox>
                              <asp:Button ID="btnDialog"  runat="server" style="display:none;"/>
                            </td>
                            <td>
                              <asp:Repeater ID="rptINV" runat="server">
                                <ItemTemplate>
                                <li class="file_list" runat="server" id="liINV">
                                    <a href="javascript:;" target="_blank" id="INVlink" runat="server" title="View this invoice">xxxxxxx.png</a> <asp:ImageButton ID="btnINVDelete" runat="server" ImageUrl="images/delete.png" CommandName="Delete" ToolTip="Remove all items from this invoice" />
                                </li>
                                </ItemTemplate>
                              </asp:Repeater>
                            </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td style="font-weight:bold; font-size:14px; color:Red; border-right:1px solid #3c3;">&nbsp;&nbsp;&nbsp;TOTAL SET</td>
                    <td colspan="4"  style="border-left:none;"><asp:TextBox ID="txtTOTAL_SET" runat="server" CssClass="StrechControl" style="font-size:14px; color:red ; text-align:left"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td colspan="3">48. Name of Declarant  </td>
                    <td colspan="2" align="center" style="border-left:3px solid #3c3; border-top:3px solid #3c3; font-weight:bold; font-size:14px;">FOR OFFICIAL USE</td>
                    <td style="border-top:3px solid #3c3;">53. Total Duty/Tax Payable</td>
                    <td colspan="2" style="border-right:2px solid #3c3; border-top:3px solid #3c3;"><asp:TextBox ID="txtJDA1_53TotalDuty" runat="server" CssClass="StrechControl"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td colspan="3" style="border-top:none;">
                    <asp:TextBox ID="txtJDA1_48Name_Declarant" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td colspan="2" align="left" style="border-left:3px solid #3c3; ">52. Removal from Customs control authorised by : </td>
                    <td >54. Other Charges </td>
                    <td colspan="2" style="border-right:2px solid #3c3;">
                      <asp:TextBox ID="txtJDA1_54Other_Charges" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3">49. Identity Card/Passport No. </td>
                    <td colspan="2" rowspan="6" align="center" style="border-left:3px solid #3c3; border-top:none;">
                    <asp:TextBox ID="txtJDA1_52" runat="server" CssClass="StrechControl" TextMode="MultiLine" style="height:160px;"></asp:TextBox>    </td>
                    <td>55. Total Amount Payable </td>
                    <td colspan="2" style="border-right:2px solid #3c3; ">
                      <asp:TextBox ID="txtJDA1_55TotalAmount_Payable" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" style="border-top:none;">
    			                <asp:TextBox ID="txtJDA1_49IDCard_Passport" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    <td colspan="3" style="border-right:2px solid #3c3;">56. Manuscript No. (if applicable)</td>
                    </tr>
                  <tr>
                    <td colspan="3">50. Status </td>
                    <td colspan="3" rowspan="4" style="border-top:none; border-right:2px solid #3c3;">
                      <asp:TextBox ID="txtJDA1_56Manuscript_No" runat="server" CssClass="StrechControl" TextMode="MultiLine" style="height:110px;" ></asp:TextBox>      </td>
                    </tr>
                  <tr>
                    <td colspan="3" style="border-top:none;">
                      <asp:TextBox ID="txtJDA1_50Status" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </td>
                    </tr>
                  <tr>
                    <td colspan="3">51. I certify that this declaration is true and complete.</td>
                    </tr>
                  <tr>
                    <td colspan="3" style="border-top:none;">
                      <asp:TextBox ID="txtJDA1_51" runat="server" CssClass="StrechControl" TextMode="MultiLine" style="height:50px;" ></asp:TextBox>    </td>
                    </tr>
                  <tr>
                    <td colspan="2" align="right" style="border-top:none;">Date</td>
                    <td>
                      
                      <asp:TextBox ID="txtJDA1_51_Date" runat="server" CssClass="StrechControl" ></asp:TextBox>
                      <Ajax:CalendarExtender ID="clJDA1_51_Date" runat="server"  PopupPosition="TopLeft"
                    Format="dd-MMM-yyyy"  BehaviorID="txtJDA1_51_Date"
                    TargetControlID="txtJDA1_51_Date"></Ajax:CalendarExtender>
                    </td>
                    <td align="right" style="border-top:none; border-left:3px solid #3c3; border-bottom:2px solid #3c3;">Date </td>
                    <td style=" border-bottom:2px solid #3c3;">
                    <asp:TextBox ID="txtJDA1_52_Date" runat="server" CssClass="StrechControl"></asp:TextBox>
                    <Ajax:CalendarExtender ID="clJDA1_52_Date" runat="server" 
                        Format="dd-MMM-yyyy"  BehaviorID="txtJDA1_52_Date" PopupPosition="TopLeft"
                        TargetControlID="txtJDA1_52_Date"></Ajax:CalendarExtender>
                        
                     <Ajax:CalendarExtender ID="clJDA1_56Date" runat="server" 
                        Format="dd-MMM-yyyy"  BehaviorID="txtJDA1_56Date" PopupPosition="TopLeft"
                        TargetControlID="txtJDA1_56Date"></Ajax:CalendarExtender>
                    </td>
                    <td align="right" style="border-top:none; border-bottom:2px solid #3c3;">Date</td>
                    <td colspan="2" style=" border-right:2px solid #3c3; border-bottom:2px solid #3c3;">
                      <asp:TextBox ID="txtJDA1_56Date" runat="server" CssClass="StrechControl"></asp:TextBox>
                    </span></td>
                  </tr>
                  
                </table>        
        </asp:Panel>                             
        <div style="height:80px;">
            
            </div>
        </div>
        
        <div id="footerMenu" style="bottom:0px; right:10px; min-height:40px; padding-top:5px; width:100%; background-color:White; text-align:right; padding-right:20px;  position:fixed;" class="NormalTextBlack">
            <table style="width:100%; height:0px;" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="text-align:left;">
                        <ul style="list-style: none; padding-left:30px;">
                        
                            <asp:Repeater ID="rptFile" runat="server">
                            <ItemTemplate>
                            <li class="file_list" runat="server" id="liType">
                                <a href="javascript:;" target="_blank" id="alink" runat="server">xxxxxxx.png</a> <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="images/delete.png" CommandName="Delete" />
                            </li>
                            </ItemTemplate>
                            </asp:Repeater>
                            <li style="display:block;padding:3px 5px 3px 5px;">
                                <iframe width="90" height="40" id="windowUpload" runat="server" border="0" marginwidth="0" marginheight="0" style="border:none; margin-left:10px;" scrolling="no"></iframe>
                                <asp:Button ID="btnUpload" runat="server" style="display:none;" />
                            </li>
                        </ul>
                    </td>
                    <td style=" width:420px; vertical-align:top; text-align:center;">
                        <input type="button" class="Button_Disable" style="height:30px; width:130px; color:White; cursor:pointer; " value="Back to Report List" onclick="window.location.href='Filter_JDA_1_3.aspx';" />
                        <asp:Button ID="btnPrint" runat="server" CssClass="Button_Red" Height="30px" Text="Print Preview" Width="130px" />
                        <asp:Button ID="btnSave" runat="server" CssClass="Button_Red" Height="30px" Text="Save" Width="130px" /> 
                    </td>
                </tr>
            </table>
                      
        </div>
        
        
        <uc1:MasterEditor ID="MasterEditor1" runat="server"  Visible="False"/>
        <uc1:MasterEditor_MultiLine ID="MasterEditor_MultiLine1" runat="server" Visible="False" />
        <asp:TextBox ID="txtFieldID" runat="Server" style="display:none"></asp:TextBox>
        <asp:Button ID="btnMasterMultilineDialog" runat="Server" style="display:none" />
        
    </ContentTemplate>
</asp:UpdatePanel>
<script language="javascript" type="text/javascript" src="Script/JDA1.js"></script>
</asp:Content>

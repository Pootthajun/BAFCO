﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search_Detail.aspx.vb" Inherits="Search_Detail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title></title>
     <link href="style/css.css" rel="stylesheet" type="text/css" />
     <style type="text/css">
        body {
	        margin-left: 10px;
	        margin-top: 10px;
	        margin-right: 10px;
	        margin-bottom: 10px;
	        background-color:#F6F3F3;
        }
        
        .Master_Table thead tr th{
               background-color:#ff3300;
        }
        
        .Master_Table tr td{
               border-bottom:1px solid #ccc;
        }
        
        .Master_Table tr:hover td
        {
            background-color:#ffffcc;
        }
        
        .Master_Table tbody tr{
                cursor:pointer;               
        }
        
        .filter-table{
            font-size:14px;
            font-weight:bold;
        }
        
        #col1{
            width:70%;
        }
        #col2{
            width:30%;
        }
        
        .filter-table input{
            width:300px;
        }
        </style>
</head>
<body class="NormalTextBlack">
    <form id="form1" runat="server">
    <table>
        <tr>
            <td>
            <b>Search</b>
            </td>
            <td>
            <asp:TextBox ID="txtSearch" runat="server" style="height:23px; width:200px;"></asp:TextBox>
            <asp:ImageButton ID="btnSearch" runat="server" src="images/Search.png" style="width:20px; height:20px" />
            </td>
        </tr>
    </table>
    <table cellpadding="5" class="Master_Table">
        <thead>
            <tr>
                <th scope="col" id="col1">No. and type of Packages/Decription of Goods</th>
                <th scope="col" id="col2">Customs Tariff Code No.</th>
            </tr>
        </thead>
        <tbody ID="tbodyItemList" runat="server"></tbody>        
    </table>
    <asp:TextBox ID="txtOpenerText1" runat="server" style="display:none;"></asp:TextBox>
    <asp:TextBox ID="txtOpenerText2" runat="server" style="display:none;"></asp:TextBox>
    
    <script src="Script/jquery-2.1.1.min.js"></script>
    <script src="Script/jquery.filtertable.min.js"></script>
    <script>
    $(document).ready(function() {
        //$('table').filterTable(); // apply filterTable to all tables on this page
    });
    
    $(window).resize(function() {
        resizeWindow();
    });
    
    function resizeWindow()
    {
        $('.Master_Table').css('width',($(window).width()-40)+'px');
        //$("#input:text:search:visible:first").focus();
    }resizeWindow();
    
    </script>
    <script language="javascript">
    
    function sendBack(myTxt1,myTxt2)
        {
            if((!opener)){window.close(); return; } 
            
            var openerTxt1=opener.document.getElementById(document.getElementById('txtOpenerText1').value);
            var openerTxt2=opener.document.getElementById(document.getElementById('txtOpenerText2').value);
            
            openerTxt1.value=document.getElementById(myTxt1).innerHTML;
            openerTxt2.value=document.getElementById(myTxt2).innerHTML;
            window.close(); opener.focus();
        }
    </script>
    </form>
    
</body>
</html>

﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Protected Sub lnkSignout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSignout.Click
        lblUName.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Default.aspx';", True)
        Session.Abandon()
    End Sub

    Protected Sub btnToggleLeftMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnToggleLeftMenu.Click
        LeftMenu.Visible = Not LeftMenu.Visible
        Select Case LeftMenu.Visible
            Case True
                btnToggleLeftMenu.ImageUrl = "images/dbLeft.png"
                pnlLeftMenu.Style.Item("width") = "200px !important"
            Case False
                btnToggleLeftMenu.ImageUrl = "images/dbRight.png"
                pnlLeftMenu.Style.Item("width") = "18px"
        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            LogoffScreen.Visible = True
            Exit Sub
        End If

        If Not IsPostBack Then
            WriteHeaderPage()
        End If

        SetPermission()
       
    End Sub

    Sub WriteHeaderPage()
        Select Case Me.Page.TemplateControl.ToString
            Case "ASP." & "Filter_JDA_1_3.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "JDA Report 1,3"
                LeftMenu.SelectedIndex = 0

            Case "ASP." & "JDA_1.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "JDA 1"
                LeftMenu.SelectedIndex = 0

            Case "ASP." & "JDA_2.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "JDA 2"
                LeftMenu.SelectedIndex = 1


            Case "ASP." & "JDA_3.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "JDA 3"
                LeftMenu.SelectedIndex = 0


            Case "ASP." & "JDA_4.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "JDA 4"
                LeftMenu.SelectedIndex = 1


            Case "ASP." & "Filter_JDA_2_4.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "JDA Report 2,4"
                LeftMenu.SelectedIndex = 1

            Case "ASP." & "Export_JDA_1.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Export JDA 1"
                LeftMenu.SelectedIndex = 2

            Case "ASP." & "Export_JDA_3.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Export JDA 3"
                LeftMenu.SelectedIndex = 2

            Case "ASP." & "Setting_User.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "User Setting"
                LeftMenu.SelectedIndex = 3

            Case Else
                LeftMenu.RequireOpenedPane = False
                LeftMenu.SelectedIndex = -1
        End Select
    End Sub

    Protected Sub LogoffScreen_Login() Handles LogoffScreen.Login
        Select Case Page.TemplateControl.ToString
            Case "ASP.default_aspx"

                If (Not IsNothing(Session("IS_JDA_1")) AndAlso Session("IS_JDA_1")) OrElse (Not IsNothing(Session("IS_JDA_3")) AndAlso Session("IS_JDA_3")) Then
                    Response.Redirect("Filter_JDA_1_3.aspx")
                ElseIf (Not IsNothing(Session("IS_JDA_2")) AndAlso Session("IS_JDA_2")) OrElse (Not IsNothing(Session("IS_JDA_4")) AndAlso Session("IS_JDA_4")) Then
                    Response.Redirect("Filter_JDA_2_4.aspx")
                ElseIf Not IsNothing(Session("IS_ADMIN")) AndAlso Session("IS_ADMIN") Then
                    Response.Redirect("Setting_User.aspx")
                Else

                End If

            Case Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Reload", "window.location.href=window.location.href;", True)
        End Select
    End Sub

    Private Sub SetPermission()

        lblUName.Text = Session("USER_FULLNAME")
        ADP_USER.Visible = Not IsNothing(Session("IS_ADMIN")) AndAlso Session("IS_ADMIN")
        ADP_JDA_1_3.Visible = (Not IsNothing(Session("IS_JDA_1")) AndAlso Session("IS_JDA_1")) OrElse (Not IsNothing(Session("IS_JDA_3")) AndAlso Session("IS_JDA_3"))
        ADP_JDA_2_4.Visible = (Not IsNothing(Session("IS_JDA_2")) AndAlso Session("IS_JDA_2")) OrElse (Not IsNothing(Session("IS_JDA_4")) AndAlso Session("IS_JDA_4"))
        ADP_Export.Visible = ADP_JDA_1_3.Visible

    End Sub
End Class


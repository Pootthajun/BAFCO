﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class JDA_2
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim C As New Converter

    Public Property PageUniqueID() As String
        Get
            Return ViewState("PageUniqueID").ToString
        End Get
        Set(ByVal value As String)
            ViewState("PageUniqueID") = value
        End Set
    End Property

    Public ReadOnly Property AttachedFileList() As List(Of GenericLib.FileStructure)
        Get
            Dim Result As New List(Of GenericLib.FileStructure)
            For i As Integer = 1 To 100 '-----Set Maximum 100 File-------
                Dim S_Name As String = "File_" & PageUniqueID & "_" & i
                If Not IsNothing(Session(S_Name)) Then
                    Dim F As GenericLib.FileStructure = Session(S_Name)
                    Result.Add(F)
                End If
            Next
            Return Result
        End Get
    End Property

    Public Property JDA_ID() As Integer
        Get
            Try
                Return lblJDA_ID.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            lblJDA_ID.Text = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("USER_ID")) Then Exit Sub

        If IsNothing(Session("IS_JDA_2")) OrElse Not Session("IS_JDA_2") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        End If

        If Not IsPostBack Then
            SetHeader()
            BindData()
            BindFile()
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Accordion", "init_Accordion();", True)
    End Sub

    Private Sub SetHeader()
        JDA_ID = Val(Request.QueryString("JDA_ID"))
        PageUniqueID = Now.ToOADate.ToString.Replace(".", "")
    End Sub

    Private Sub BindData()
        If IsNothing(Session("User_ID")) Then Exit Sub

        ClearForm()

        Dim SQL As String = "SELECT * FROM tb_JDA_2 WHERE JDA2_ID = " & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            JDA_ID = 0
            Exit Sub
        End If

        If Not IsDBNull(DT.Rows(0).Item("JDA2_Date_Import")) Then
            txtImportDate.Text = C.DateToString(DT.Rows(0).Item("JDA2_Date_Import"), "dd-MMM-yyyy")
        End If
        txtRegisNo.Text = DT.Rows(0).Item("JDA2_Registration_Number").ToString
        txtINVNo.Text = DT.Rows(0).Item("JDA2_INV_NO").ToString
        txtRemark.Text = DT.Rows(0).Item("Remark").ToString

    End Sub

    Private Sub ClearForm()
        txtImportDate.Text = ""
        txtRegisNo.Text = ""
        txtINVNo.Text = ""
        txtRemark.Text = ""

        '------------------ Set File upload ------------
        windowUpload.Attributes("src") = "JDA_Upload.aspx?P=" & PageUniqueID & "&B=" & btnUpload.ClientID & "&t=" & Now.ToOADate.ToString.Replace(".", "")
        '------------------ Empty Buffer--------------
        For i As Integer = 1 To 100
            Dim SNAME As String = "File_" & PageUniqueID & "_" & i
            Session(SNAME) = Nothing
        Next
        DisplayFile()
    End Sub

    Private Sub BindFile()

        Dim DT As New DataTable
        Dim SQL As String = "SELECT * FROM tb_JDA_2_File WHERE JDA2_ID=" & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        Dim Folder As String = Server.MapPath("File/JDA2")
        If Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        End If
        Folder &= "\" & JDA_ID
        If Not Directory.Exists(Folder) Then Exit Sub

        '------------ Get File To Session ---------
        Dim LastIndex As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Path As String = Folder & "\" & DT.Rows(i).Item("F_ID")
            If Not File.Exists(Path) Then Continue For
            LastIndex += 1
            Dim F As New GenericLib.FileStructure
            Dim FS As FileStream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            F.Content = C.StreamToByte(FS)
            FS.Close()
            F.AbsoluteName = DT.Rows(i).Item("AbsoluteName").ToString
            F.FileIndex = LastIndex
            Select Case DT.Rows(i).Item("FileType").ToString
                Case "image/jpeg"
                    F.FileType = GenericLib.AllowFileType.JPG
                Case "image/png"
                    F.FileType = GenericLib.AllowFileType.PNG
                Case Else
                    F.FileType = GenericLib.AllowFileType.PDF
            End Select

            Dim SName As String = "File_" & PageUniqueID & "_" & F.FileIndex
            Session(SName) = F
        Next

        DisplayFile()
    End Sub

    Private Sub DisplayFile()
        Dim DT As DataTable = CurrentFile()
        rptFile.DataSource = DT
        rptFile.DataBind()
    End Sub

    Private Function CurrentFile() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("AbsoluteName")
        DT.Columns.Add("FileType", GetType(Integer))
        DT.Columns.Add("FileIndex", GetType(Integer))

        Dim FileList As List(Of GenericLib.FileStructure) = AttachedFileList
        For Each F In FileList
            Dim DR As DataRow = DT.NewRow
            DR("AbsoluteName") = F.AbsoluteName
            DR("FileType") = F.FileType
            DR("FileIndex") = F.FileIndex
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Private Sub RemoveFile(ByVal FileName As String)
        For i As Integer = 1 To 100
            If Not Session("File_" & PageUniqueID & "_" & i) Then
                Dim F As GenericLib.FileStructure = Session("File_" & PageUniqueID & "_" & i)
                If F.AbsoluteName = FileName Then
                    Session("File_" & PageUniqueID & "_" & i) = Nothing
                End If
            End If
        Next
        DisplayFile()
    End Sub

    Private Sub RemoveFile(ByVal FileIndex As Integer)
        Session("File_" & PageUniqueID & "_" & FileIndex) = Nothing
        DisplayFile()
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        DisplayFile()
    End Sub

    Protected Sub rptFile_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptFile.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim alink As HtmlAnchor = e.Item.FindControl("alink")
                Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

                '-------------- Clear Session -------------
                Dim SName As String = "File_" & PageUniqueID & "_" & e.CommandArgument
                RemoveFile(CInt(e.CommandArgument))
                '-------------- Display File --------------
                DisplayFile()
        End Select
    End Sub

    Protected Sub rptFile_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFile.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim liType As HtmlGenericControl = e.Item.FindControl("liType")
        Dim alink As HtmlAnchor = e.Item.FindControl("alink")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Select Case e.Item.DataItem("FileType")
            Case GenericLib.AllowFileType.JPG, GenericLib.AllowFileType.PNG
                liType.Attributes("class") = "file_list picture"
            Case GenericLib.AllowFileType.PDF
                liType.Attributes("class") = "file_list pdf"
        End Select
        alink.InnerHtml = e.Item.DataItem("AbsoluteName")
        btnDelete.CommandArgument = e.Item.DataItem("FileIndex")
        Dim SName As String = "File_" & PageUniqueID & "_" & e.Item.DataItem("FileIndex")
        alink.HRef = "RenderFile.aspx?S=" & SName & "&"
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        BindData()
        BindFile()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If IsNothing(Session("USER_ID")) Then Exit Sub

        If Not GL.IsProgrammingDate(txtImportDate.Text, "dd-MMM-yyyy") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "validate", "alert('Please insert ""Date of Import""');", True)
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = "SELECT * FROM tb_JDA_2 WHERE JDA2_ID =" & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        Dim _id As Integer = JDA_ID
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow

            _id = GetNew_JDA2_ID()
            DR("JDA2_ID") = _id
            DR("JDA2_Date_Import") = C.StringToDate(txtImportDate.Text, "dd-MMM-yyyy")
            DR("JDA2_Registration_Number") = txtRegisNo.Text
            DR("JDA2_INV_NO") = txtINVNo.Text
            DR("Remark") = txtRemark.Text
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
            DR("JDA2_Date_Import") = C.StringToDate(txtImportDate.Text, "dd-MMM-yyyy")
            DR("JDA2_Registration_Number") = txtRegisNo.Text
            DR("JDA2_INV_NO") = txtINVNo.Text
            DR("Remark") = txtRemark.Text
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If

        Dim CMD As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('" & ex.Message.Replace("'", "\'").Replace(vbLf, "\n") & "');", True)
            Exit Sub
        End Try

        JDA_ID = _id
        '---------------- Save File--------------
        SQL = "DELETE FROM tb_JDA_2_File where JDA2_ID=" & JDA_ID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        SQL = "SELECT * FROM tb_JDA_2_File where 0=1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim FT As DataTable = CurrentFile()
        Dim FileList As List(Of GenericLib.FileStructure) = AttachedFileList

        '------------ Clear Folder------------
        Dim Folder As String = Server.MapPath("File/JDA2")
        If Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        End If
        Folder &= "\" & JDA_ID
        If FT.Rows.Count > 0 And Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        ElseIf FT.Rows.Count = 0 And Directory.Exists(Folder) Then '-------- Clear unused folder ----------
            GL.ForceDeleteFolder(Folder)
        End If
        '-------- Clear All File-----------
        GL.ForceDeleteFileInFolder(Folder)

        For i As Integer = 0 To FT.Rows.Count - 1
            '--------------- Check Session is Alive -----------
            Dim F As GenericLib.FileStructure = FileList(i)
            If IsNothing(F) Then Continue For

            Dim FR As DataRow = DT.NewRow
            Dim F_ID As Integer = GetNew_JDA2_File_ID()
            FR("F_ID") = F_ID
            FR("JDA2_ID") = JDA_ID
            FR("AbsoluteName") = F.AbsoluteName
            Select Case F.FileType
                Case GenericLib.AllowFileType.JPG
                    FR("FileType") = "image/jpeg"
                Case GenericLib.AllowFileType.PDF
                    FR("FileType") = "application/pdf"
                Case GenericLib.AllowFileType.PNG
                    FR("FileType") = "image/png"
                Case Else
                    FR("FileType") = ""
            End Select
            DT.Rows.Add(FR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)

            '------------ Save File To Folder--------------
            Dim FS As FileStream = File.Open(Folder & "\" & F_ID, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)
            FS.Write(F.Content, 0, F.Content.Length)
            FS.Close()
        Next

        ClearForm()
        BindData()
        BindFile()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Save successfully');", True)

    End Sub

    Private Function GetNew_JDA2_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(JDA2_ID),0)+1 FROM tb_JDA_2"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Function GetNew_JDA2_File_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(F_ID),0)+1 FROM tb_JDA_2_File"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function
End Class

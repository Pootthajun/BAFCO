﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Setting_User.aspx.vb" Inherits="Setting_User" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="udp1" runat="server" >
<ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" colspan="4" class="Top_Menu_Bar">
                                    <ul class="shortcut-buttons-set">
                                        <li>
                                            <asp:LinkButton ID="btnNew" runat="server" CssClass="shortcut-button">
                                                <span><img src="images/icon/41.png"/><br />New</span>
                                            </asp:LinkButton>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            
                        </table>
    
            
                        <table cellpadding="0" cellspacing="1" style="margin:20px  0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                            <tr>
                                <td width="50" class="Grid_Header" align="center">
                                    No                    
                                </td>
                                <td width="150" class="Grid_Header">                    
                                    User Code / Login
                                </td>
                                <td width="150" class="Grid_Header">                    
                                    Fullname
                                </td>
                                <td width="150" class="Grid_Header">                    
                                    Password
                                </td>
                                <td width="100" class="Grid_Header">                    
                                    Setting<br>Other User
                                </td>
                                <td width="50" class="Grid_Header">                    
                                    JDA 1
                                </td>
                                <td width="50" class="Grid_Header">                    
                                    JDA 2
                                </td>
                                <td width="50" class="Grid_Header">                    
                                    JDA 3
                                </td>
                                <td width="50" class="Grid_Header">                    
                                    JDA 4
                                </td>
                                <td width="50" class="Grid_Header">                  
                                    Status
                                </td>
                                <td width="100" class="Grid_Header">                  
                                    Action
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                            <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail">                                            
                                            <asp:Label ID="lblCode" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail">                                            
                                            <asp:Label ID="lblFullname" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail">                                            
                                            <asp:Label ID="lblPassword" runat="server"></asp:Label>
                                        </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnSetting_Yes_1" CommandName="Setting" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnSetting_No_1" CommandName="Setting" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA1_Yes_1" CommandName="JDA1" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA1_No_1" CommandName="JDA1" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA2_Yes_1" CommandName="JDA2" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA2_No_1" CommandName="JDA2" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA3_Yes_1" CommandName="JDA3" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA3_No_1" CommandName="JDA3" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA4_Yes_1" CommandName="JDA4" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA4_No_1" CommandName="JDA4" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                        <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnStatus_Yes_1" CommandName="Status" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnStatus_No_1" CommandName="Status" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Edit" ImageUrl="images/icon/61.png" Height="25px" />
                                            <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                            <Ajax:ConfirmButtonExtender ID="cfm" runat="server" ConfirmText="Confirm to delete this user permanently?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </td>                                   
                                    </tr>
                                    <tr id="trEdit" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center" valign="middle">
                                            <asp:Image ImageUrl="images/arrow_blue_right.png" ID="imgEdit" runat="server"></asp:Image>
                                        </td>
                                        <td class="Grid_Detail">                                           
                                            <asp:TextBox ID="txtCode" runat="server" MaxLength="50" CssClass="Textbox_Form_Required StrechControl" style="background-color:#ffffcc; text-align:left;"></asp:TextBox>
                                        </td> 
                                        <td class="Grid_Detail">                                            
                                            <asp:TextBox ID="txtFullname" runat="server" MaxLength="50" CssClass="Textbox_Form_Required StrechControl" style="background-color:#ffffcc; text-align:left;"></asp:TextBox>
                                        </td>
                                        <td class="Grid_Detail">                                            
                                            <asp:TextBox ID="txtPassword" runat="server" CssClass="Textbox_Form_Required StrechControl" style="background-color:#ffffcc; text-align:left;"></asp:TextBox>
                                        </td>
		                                <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnSetting_Yes_2" CommandName="Setting" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnSetting_No_2" CommandName="Setting" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA1_Yes_2" CommandName="JDA1" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA1_No_2" CommandName="JDA1" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA2_Yes_2"  CommandName="JDA2" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA2_No_2" CommandName="JDA2" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA3_Yes_2" CommandName="JDA3" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA3_No_2"  CommandName="JDA3" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                         <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnJDA4_Yes_2" CommandName="JDA4" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnJDA4_No_2" CommandName="JDA4" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                        <td class="Grid_Detail" style="text-align:center; vertical-align:middle;">
                                            <asp:ImageButton ID="btnStatus_Yes_2" CommandName="Status" CommandArgument="False" runat="server" ImageUrl="images/check.png" />
                                            <asp:ImageButton ID="btnStatus_No_2" CommandName="Status" CommandArgument="True" runat="server" ImageUrl="images/none.png" />
                                         </td>
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnSave" CommandName="Save" runat="server" ToolTip="Save" ImageUrl="images/icon/59.png" Height="25px" />
                                            <asp:ImageButton ID="btnCancel" CommandName="Cancel" runat="server" ToolTip="Cancel" ImageUrl="images/icon/21.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                
    </ContentTemplate>
</asp:UpdatePanel>
    
</asp:Content>


﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="JDA_4.aspx.vb" Inherits="JDA_4" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .file_list{
        display:table; 
        float:none; 
        margin-bottom:10px;
        padding:0px;
        }
        .file_list a{
            
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" >
   
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <asp:Label ID="lblJDA_ID" runat="server"  style="display:none;"></asp:Label>
            <table cellspacing="5px;" style="margin-left:10px;" >
                <tr>
                  <td width="180" style="font-weight:bold;">
                        Date of Import <font color="red">**</font>
                    </td>
                  <td>
                    <asp:TextBox ID="txtImportDate" runat="server" CssClass="Textbox_Form_White" width="200px" style="text-align:center;" Placeholder="dd-MMM-yyyy"></asp:TextBox>
                         <asp:ImageButton ID="imgImportDate" runat="server" ImageUrl="images/Calendar.png" style="position:relative; top:5px;" />
                        <Ajax:CalendarExtender ID="imgImportDate_CDE" runat="server" BehaviorID="txtImportDate"
                        Format="dd-MMM-yyyy"  PopupButtonID="imgImportDate" TargetControlID="txtImportDate"></Ajax:CalendarExtender>
                  </td>
                </tr>
                <tr>
                  <td width="180" style="font-weight:bold;">
                        Registration Number
                    </td>
                    <td>
                       <asp:TextBox ID="txtRegisNo" MaxLength="100" runat="server" CssClass="Textbox_Form_White" width="200px" style="text-align:center;"></asp:TextBox>
                    </td>
  				</tr>
                <tr>
                    <td width="180" style="font-weight:bold;">
                         	Invoice No.
                    </td>
                    <td width="466" height="25">
                      <asp:TextBox ID="txtINVNo" MaxLength="100" runat="server" CssClass="Textbox_Form_White" width="200px" style="text-align:center;"></asp:TextBox>
                  </td>
  				</tr>
               
                <tr>
                  <td width="180" height="25" style="font-weight:bold;">
                        Insert Attachment
                    </td>
                    <td style="padding-top:10px;"> 
                           <iframe width="90" height="25" id="windowUpload" runat="server" border="0" marginwidth="0" marginheight="0" style="border:none; margin-left:10px;" scrolling="no"></iframe> 
                           <span style="color:Gray; position:relative; top: -10px;">Only .pdf, .png, .jpg, .jpeg</span>
                           <asp:Button ID="btnUpload" runat="server" style="display:none;" /><br>
                           
                           <ul style="list-style: none; margin-left:-10px;">
                             <asp:Repeater ID="rptFile" runat="server">
                                <ItemTemplate>
                                <li class="file_list" runat="server" id="liType">
                                    <a href="javascript:;" target="_blank" id="alink" runat="server">xxxxxxx.png</a> <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="images/delete.png" CommandName="Delete" />
                                </li>
                                </ItemTemplate>
                             </asp:Repeater>                               
                           </ul>                         
                    </td>
  				</tr> 
                <tr valign="top">
                    <td width="180" style="font-weight:bold;">
                        Remark
                    </td>
                    <td> 
                      <asp:TextBox ID="txtRemark" runat="server" CssClass="Textbox_Form_White" width="100%" TextMode="MultiLine" MaxLength="500" Height="80px"></asp:TextBox>
                                         
                  </td>
  				</tr>   
  				<tr valign="top">
                    <td width="180">
                        &nbsp;
                    </td>
                    <td style="padding-top:30px; padding-bottom:30px;"> 
                      <input type="button" class="Button_Disable" style="height:30px; width:130px; color:White; cursor:pointer; " value="Back to Report List" onclick="window.location.href='Filter_JDA_2_4.aspx';" />
                      <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="Button_Red" Width="130px" Height="30px"/>                       
                      <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>                       
                        
                  </td>
  				</tr>   
			</table>
        </div>
        
    </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>


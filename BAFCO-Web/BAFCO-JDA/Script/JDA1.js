﻿
function calculateFOB()
{
    var txt_23_fob = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_23FOB_Value');
    var txt_24_insurance = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_24Insurance');
    var txt_25_Freight = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_25_Freight');
    var txt_26_cif = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_26CIF_Value');

    var fob=parseFloat(replaceComma(txt_23_fob.value));
    txt_24_insurance.value = formatmoney((fob/100).toString(),'0','999999999999');
    txt_25_Freight.value=formatmoney((fob/10).toString(),'0','999999999999');
    txt_26_cif.value=formatmoney((fob*111/100).toString(),'0','999999999999');
    txt_23_fob.value=formatmoney(txt_23_fob.value,'0','999999999999');
    
}

function calculateLineTotal()
{
    var totalLine = document.getElementById('ctl00_ContentPlaceHolder1_txt_Total_Line').value;
    var totalUS = 0;
    var totalTHB = 0;
    var totalIMP = 0;

//    -------------------------------
     var totalOtherTaxes = 0;
     var txtEXR=document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_22Exchange_Rate');
     var Ex_Rate = replaceComma(txtEXR.value);
     if(parseFloat(Ex_Rate) != 'NaN' && Ex_Rate !='')txtEXR.value=formatfloat(Ex_Rate,'0','999999999999',4);
     var currency = document.getElementById('ctl00_ContentPlaceHolder1_ddlJDA1_22Currency').value;
     document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_39_Currency').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency1').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency2').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency3').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency4').value=currency;
     
     var item_no=0;
     for(i=0; i<totalLine; i++)
     {
        var itemid = 'ctl00_ContentPlaceHolder1_rptData_ctl' + padLeft(i.toString(),2,'0') + '_';
        var US = replaceComma(document.getElementById(itemid +'txtJDA1_39Total').value);
        var txtRow = document.getElementById(itemid +'txtRow');
        var txt_TH = document.getElementById(itemid +'txtJDA1_39THB');
        //var imp_rate = replaceComma(document.getElementById(itemid +'txtJDA1_40Rate').value);
        //var txt_Imp_Amount=document.getElementById(itemid +'txtJDA1_41Amount');
//        ------------
        var itemidOther = 'ctl00_ContentPlaceHolder1_rptData_ctl' + padLeft(i.toString(), 2, '0') + '_';
        //var THBOther = replaceComma(document.getElementById(itemidOther + 'txtJDA1_44Amount').value);
        //var Other_Rate = replaceComma(document.getElementById(itemidOther +'txtJDA1_43Rate').value);
        //var txtJDA1_44Amount = document.getElementById(itemid +'txtJDA1_44Amount');
//        ------------ Currency --------------
        var txtCurrency=document.getElementById(itemid + 'txtJDA1_38PerUnit');
        txtCurrency.value=currency;
        /*Sum Raw Value*/
        if(parseFloat(US) != 'NaN' && US !='')
        {
             totalUS+=parseFloat(US);
             document.getElementById(itemid +'txtJDA1_39Total').value=formatmoney(US,'0','999999999999');
             item_no+=1;
             txtRow.value=item_no;
         }else{
            document.getElementById(itemid +'txtJDA1_39Total').value='';
            txtRow.value='';
         }
        
        
        if(parseFloat(Ex_Rate) != 'NaN' && Ex_Rate !='' && parseFloat(US) != 'NaN' && US !='')
        {
              var _value=parseFloat(Ex_Rate)*parseFloat(US);
              txt_TH.value=formatmoney(_value.toString(),'0','999999999999');
              totalTHB+=_value;
              // Calculate import duty
//              if(parseFloat(imp_rate) != 'NaN' && imp_rate !='')
//                {
//                    var duty = parseFloat(_value * parseFloat(imp_rate) / 100); 
//                    var cvInt =duty.toFixed(0);
//                    txt_Imp_Amount.value = formatfloat(cvInt.toString(), '0', '999999999999', 0);
//                    totalIMP += parseFloat(cvInt); 
//                }else{
//                        txt_Imp_Amount.value='';
//                }
//            if(parseFloat(Other_Rate) != 'NaN' && Other_Rate !='')
//                {
//                    var OtherTaxes = _value * parseFloat(Other_Rate) / 100;
//                    var cvInt_Other = OtherTaxes.toFixed(0);
//                    txtJDA1_44Amount.value = formatfloat(cvInt_Other.toString(), '0', '999999999999', 0);                   
//                    totalOtherTaxes += parseFloat(cvInt_Other);
//                }else{
//                        txtJDA1_44Amount.value='';
//                }                              
        }else{
                txt_TH.value='';
                //txt_Imp_Amount.value='';
        }
        }
   
     document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_39Total').value = formatmoney(totalTHB.toString(), '0', '999999999999');
     //document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_41Amount').value = formatfloat(totalIMP.toString(), '0', '999999999999', 0);
     
     document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_39_PerUnit').value = formatmoney(totalUS.toString(), '0', '999999999999', 0);
     //document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_44Amount').value = formatfloat(totalOtherTaxes.toString(), '0', '999999999999', 0);
}

function CalculateImportDuty(txtTotalTHB , txtImportRate , txtImportAmount)
{
    // Calculate import duty
      if(parseFloat(txtTotalTHB.value) != 'NaN' && txtTotalTHB.value !='')
      {
            var _value=parseFloat(replaceComma(txtTotalTHB.value));
            if(parseFloat(txtImportRate.value) != 'NaN' && txtImportRate.value !='')
            {
               var rate = parseFloat(replaceComma(txtImportRate.value));
               var duty = parseFloat(_value * rate / 100);
               txtImportAmount.value = formatfloat(duty.toString(), '0', '999999999999', 0);
            }else{ txtImportAmount.value=''; }
      }else{ txtImportAmount.value=''; }
}

function sumImportDuty()
{
    var totalIMP = 0;
    var totalLine = document.getElementById('ctl00_ContentPlaceHolder1_txt_Total_Line').value;
    
    for(i=0; i<totalLine; i++)
    {
        var itemid = 'ctl00_ContentPlaceHolder1_rptData_ctl' + padLeft(i.toString(),2,'0') + '_';
        var ImpAmount=replaceComma(document.getElementById(itemid +'txtJDA1_41Amount').value);
        if(parseFloat(ImpAmount) != 'NaN' && ImpAmount !='')
        {
            totalIMP += parseFloat(ImpAmount);
        }
    }
    if(totalIMP != 0) 
        document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_41Amount').value=formatfloatManualPlace(totalIMP.toString(),'0','999999999999');
    else   
        document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_41Amount').value='';
}

function sumOtherTax()
{
    var totalOther = 0;
    var totalLine = document.getElementById('ctl00_ContentPlaceHolder1_txt_Total_Line').value;
    for(i=0; i<totalLine; i++)
    {
        var itemid = 'ctl00_ContentPlaceHolder1_rptData_ctl' + padLeft(i.toString(),2,'0') + '_';
        var OtherAmount=replaceComma(document.getElementById(itemid +'txtJDA1_44Amount').value);
        if(parseFloat(OtherAmount) != 'NaN' && OtherAmount !='')
        {
            totalOther += parseFloat(OtherAmount);
        }
    }
    if(totalOther != 0) 
        document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_44Amount').value=formatfloatManualPlace(totalOther.toString(),'0','999999999999');
    else
        document.getElementById('ctl00_ContentPlaceHolder1_txtJDA1_44Amount').value='';
}
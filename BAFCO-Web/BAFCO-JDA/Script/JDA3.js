﻿

function calculateFOB()
{
    var txt_28_fob = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_20FOB_Value');
    var txt_21_insurance = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_21Insurance');
    var txt_22_Freight = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_22_Freight');
    var txt_25_cif = document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_25CIF_Value');
    
    var fob=parseFloat(replaceComma(txt_28_fob.value)); 
    txt_21_insurance.value = formatmoney((fob/100).toString(),'0','999999999999');
    txt_22_Freight.value=formatmoney((fob/10).toString(),'0','999999999999');
    txt_25_cif.value=formatmoney((fob*111/100).toString(),'0','999999999999');
    txt_28_fob.value=formatmoney(txt_28_fob.value,'0','999999999999');
}

function calculateLineTotal()
{
    var totalLine = document.getElementById('ctl00_ContentPlaceHolder1_txt_Total_Line').value;
    var totalUS = 0;
    var totalTHB = 0;
    
    var txtEXR=document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_18AmountReceived');
    var Ex_Rate = replaceComma(txtEXR.value);
    if(parseFloat(Ex_Rate) != 'NaN' && Ex_Rate !='')txtEXR.value=formatfloat(Ex_Rate,'0','999999999999',4);
    var currency = document.getElementById('ctl00_ContentPlaceHolder1_ddlJDA3_18Currency').value;
     document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_Currency').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency1').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency2').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency3').value=currency;
     document.getElementById('ctl00_ContentPlaceHolder1_txtCurrency4').value=currency;
   
    var item_no=0;
    for(i=0; i<totalLine; i++)
    {
        var itemid = 'ctl00_ContentPlaceHolder1_rptData_ctl' + padLeft(i.toString(),2,'0') + '_';
        var txt_US = document.getElementById(itemid +'txtJDA3_39Total');
        var txt_THB= document.getElementById(itemid +'txtJDA3_39Total_THB');
        var txtRow=document.getElementById(itemid +'txtRow');
        
        var txtCurrency=document.getElementById(itemid + 'txtJDA3_38PerUnit');
        txtCurrency.value=currency;
        
        var US = replaceComma(txt_US.value);
        if(parseFloat(US) != 'NaN' && US !='')
        {
             totalUS+=parseFloat(US);
             txt_US.value=formatmoney(US,'0','999999999999');
             item_no += 1;

             txtRow.value=item_no;
         }else{
            txt_US.value='';
            txtRow.value='';
         }
         
         if(parseFloat(Ex_Rate) != 'NaN' && Ex_Rate !='' && parseFloat(US) != 'NaN' && US !='')
         {

             var _value = parseFloat(Ex_Rate) * parseFloat(US);
             var Int_THB = _value.toFixed(2);
             txt_THB.value = formatfloat(Int_THB.toString(), '0', '999999999999',2);

             totalTHB += parseFloat(_value.toFixed(2));            
         }else{
                txt_THB.value='';
        }
    }
    //Report Summary
         


     document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_PerUnit').value=formatmoney(totalUS.toString(),'0','999999999999');
     document.getElementById('ctl00_ContentPlaceHolder1_txtJDA3_Total').value = formatfloat(totalTHB.toString(), '0', '999999999999',2);

}


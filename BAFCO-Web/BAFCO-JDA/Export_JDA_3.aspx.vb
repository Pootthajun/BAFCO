﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Export_JDA_3
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Dim GL As New GenericLib
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        If Not IsPostBack Then
            ClearForm()
            BindData()
        End If

    End Sub

    Private Sub ClearForm()
        txtRegistrationNumber.Text = ""
        txtInvoiceNo.Text = ""
        txtCreateDate_From.Text = ""
        txtCreateDate_To.Text = ""
        txtDateOfDespatch_From.Text = ""
        txtDateOfDespatch_To.Text = ""

        '---------------- Check Permission-------------
        If IsNothing(Session("IS_JDA_3")) OrElse Not Session("IS_JDA_3") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        End If
    End Sub

    Private Sub BindData()

        '------------------------------------------Remain---------------------------------
        Dim SQL As String = ""
        '------------JDA1----------------------
        SQL &= "  SELECT JDA_3.* " & vbLf

        If chkDetail.Checked Then
            SQL &= ",JDA3_Detail_ID,JDA_3.JDA3_ID,JDA3_31Item_No,JDA3_33Description_Goods,JDA3_34CodeNo,"
            SQL &= "JDA3_35Unit, JDA3_36CountryCode, JDA3_37Qtybased, JDA3_38PerUnit, JDA3_39Total, JDA3_39Total_THB" & vbLf
        End If

        SQL &= "  FROM tb_JDA_3 JDA_3" & vbLf

        If chkDetail.Checked Then
            SQL &= " LEFT JOIN tb_JDA_3_Detail Detail ON JDA_3.JDA3_ID=Detail.JDA3_ID" & vbLf
        End If

        Dim Filter As String = ""
        Select Case True
            Case GL.IsProgrammingDate(txtCreateDate_From.Text, "dd-MMM-yyyy") And GL.IsProgrammingDate(txtCreateDate_To.Text, "dd-MMM-yyyy")
                Dim Date_From As String = GL.ReportProgrammingDate(CV.StringToDate(txtCreateDate_From.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Dim Date_To As String = GL.ReportProgrammingDate(CV.StringToDate(txtCreateDate_To.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Filter &= " JDA_3.CREATE_DATE BETWEEN '" & Date_From & "' AND '" & Date_To & "' AND "
            Case GL.IsProgrammingDate(txtCreateDate_From.Text, "dd-MMM-yyyy")
                Dim Date_From As String = GL.ReportProgrammingDate(CV.StringToDate(txtCreateDate_From.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Filter &= " DATEDIFF(DD,JDA_3.CREATE_DATE,'" & Date_From & "')=0 AND "
            Case GL.IsProgrammingDate(txtCreateDate_To.Text, "dd-MMM-yyyy")
                Dim Date_To As String = GL.ReportProgrammingDate(CV.StringToDate(txtCreateDate_To.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Filter &= " DATEDIFF(DD,JDA_3.CREATE_DATE,'" & Date_To & "')=0 AND "
        End Select

        Select Case True
            Case GL.IsProgrammingDate(txtDateOfDespatch_From.Text, "dd-MMM-yyyy") And GL.IsProgrammingDate(txtDateOfDespatch_To.Text, "dd-MMM-yyyy")
                Dim Date_From As String = GL.ReportProgrammingDate(CV.StringToDate(txtDateOfDespatch_From.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Dim Date_To As String = GL.ReportProgrammingDate(CV.StringToDate(txtDateOfDespatch_To.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Filter &= " JDA3_5Date_Despatch BETWEEN '" & Date_From & "' AND '" & Date_To & "' AND "
            Case GL.IsProgrammingDate(txtDateOfDespatch_From.Text, "dd-MMM-yyyy")
                Dim Date_From As String = GL.ReportProgrammingDate(CV.StringToDate(txtDateOfDespatch_From.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Filter &= " DATEDIFF(DD,JDA3_5Date_Despatch,'" & Date_From & "')=0 AND "
            Case GL.IsProgrammingDate(txtDateOfDespatch_To.Text, "dd-MMM-yyyy")
                Dim Date_To As String = GL.ReportProgrammingDate(CV.StringToDate(txtDateOfDespatch_To.Text, "dd-MMM-yyyy"), "yyyy-MM-dd")
                Filter &= " DATEDIFF(DD,JDA3_5Date_Despatch,'" & Date_To & "')=0 AND "
        End Select

        If txtRegistrationNumber.Text <> "" Then
            Filter &= " JDA3_12Registration_Number Like '%" & txtRegistrationNumber.Text.Replace("'", "''") & "%' AND "
        End If
        If txtInvoiceNo.Text <> "" Then
            Filter &= " JDA3_INV_NO Like '%" & txtInvoiceNo.Text.Replace("'", "''") & "%' AND "
        End If
        If Filter <> "" Then
            SQL &= "  WHERE " & Filter.Substring(0, Filter.Length - 4)
        End If

        SQL &= "Order BY JDA_3.JDA3_ID"
        If chkDetail.Checked Then
            SQL &= ",JDA3_Detail_ID"
        End If

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        '----------------- Format DT------------------
        DT = TransformData(DT)

        Session("Export_JDA3") = DT
        Navigation.SesssionSourceName = "Export_JDA3"
        Navigation.RenderLayout()

        lblFound.Text = "Found " & FormatNumber(DT.Compute("COUNT(No)", "No IS NOT NULL"), 0) & " report(s)"
        If chkDetail.Checked Then
            lblFound.Text &= " total " & FormatNumber(DT.Rows.Count, 0) & " record(s)"
        End If

        '---------- Display Detail-----------
        tdItem.Visible = chkDetail.Checked
        tdDesc.Visible = chkDetail.Checked
        tdCode.Visible = chkDetail.Checked
        tdUnit1.Visible = chkDetail.Checked
        tdTotal1.Visible = chkDetail.Checked
        tdUnit2.Visible = chkDetail.Checked
        tdTotal2.Visible = chkDetail.Checked

    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Private Function TransformData(ByVal DT As DataTable) As DataTable
        '----------- Insert Item Number ---------
        DT.Columns.Add("SystemID", GetType(Integer))
        If Not chkDetail.Checked Then
            For i As Integer = 0 To DT.Rows.Count - 1
                DT.Rows(i).Item("SystemID") = DT.Rows(i).Item("JDA3_ID")
                DT.Rows(i).Item("JDA3_ID") = i + 1
            Next
            DT.Columns("JDA3_ID").ColumnName = "No"
            DT.Columns.Remove("CREATE_BY")
            DT.Columns.Remove("UPDATE_BY")
            DT.Columns.Remove("UPDATE_DATE")
            Return DT
        End If

        DT.Columns.Add("No", GetType(String))
        '---------------- Has Detail-------------
        Dim JDA_ID As Integer = 0
        Dim No As Integer = 0
        Dim ItemNo As Integer = 0
        DT.Columns.Add("Deleting")

        For i As Integer = 0 To DT.Rows.Count - 1
            DT.Rows(i).Item("SystemID") = DT.Rows(i).Item("JDA3_ID")

            If Not IsDBNull(DT.Rows(i).Item("JDA3_31Item_No")) Then
                ItemNo = DT.Rows(i).Item("JDA3_31Item_No")
            End If

            If DT.Rows(i).Item("JDA3_ID") <> JDA_ID Then
                JDA_ID = DT.Rows(i).Item("JDA3_ID")
                No += 1
                DT.Rows(i).Item("No") = No
            Else
                '------------Remove Duplicated Header ----------
                For j As Integer = 1 To 58
                    DT.Rows(i).Item(j) = DBNull.Value
                Next
                '------------------- MergeRow-------------------
                If i > 0 And IsDBNull(DT.Rows(i).Item("JDA3_31Item_No")) Then
                    Dim JDA3_33Description_Goods As String = DT.Rows(i).Item("JDA3_33Description_Goods").ToString
                    Dim JDA3_34CodeNo As String = DT.Rows(i).Item("JDA3_34CodeNo").ToString
                    Dim JDA3_35Unit As String = DT.Rows(i).Item("JDA3_35Unit").ToString
                    Dim JDA3_36CountryCode As String = DT.Rows(i).Item("JDA3_36CountryCode").ToString
                    Dim JDA3_37Qtybased As String = DT.Rows(i).Item("JDA3_37Qtybased").ToString

                    DT.DefaultView.RowFilter = "JDA3_31Item_No='" & ItemNo & "' AND JDA3_ID=" & JDA_ID
                    If DT.DefaultView.Count > 0 Then
                        DT.DefaultView(0).Item("JDA3_33Description_Goods") &= JDA3_33Description_Goods
                        DT.DefaultView(0).Item("JDA3_34CodeNo") &= JDA3_34CodeNo
                        DT.DefaultView(0).Item("JDA3_35Unit") &= JDA3_35Unit
                        DT.DefaultView(0).Item("JDA3_36CountryCode") &= JDA3_36CountryCode
                        DT.DefaultView(0).Item("JDA3_37Qtybased") &= JDA3_37Qtybased
                        DT.Rows(i).Item("Deleting") = "Y"
                    End If
                End If
            End If
        Next

        DT.DefaultView.RowFilter = ""

        'Delete Duplicated Row
        For i As Integer = DT.Rows.Count - 1 To 0 Step -1
            If Not IsDBNull(DT.Rows(i).Item("No")) Then
                DT.Rows(i).Item("JDA3_ID") = DT.Rows(i).Item("No")
            Else
                DT.Rows(i).Item("JDA3_ID") = DBNull.Value
            End If
            '----------- Move Column No To First Column ----------
            If DT.Rows(i).Item("Deleting").ToString = "Y" Then DT.Rows(i).Delete()
        Next
        DT.AcceptChanges()
        '------------- Remove UnUsed Column-------------
        DT.Columns.Remove("No")
        DT.Columns.Remove("Deleting")
        DT.Columns("JDA3_ID").ColumnName = "No"
        DT.Columns.Remove("CREATE_BY")
        DT.Columns.Remove("UPDATE_BY")
        DT.Columns.Remove("UPDATE_DATE")

        For i As Integer = DT.Columns.Count - 1 To 0 Step -1
            If DT.Columns(i).ColumnName.ToUpper Like "JDA1_10*" Or _
            DT.Columns(i).ColumnName.ToUpper Like "JDA1_11*" Or _
            DT.Columns(i).ColumnName.ToUpper Like "JDA1_13*" Or _
            DT.Columns(i).ColumnName.ToUpper Like "JDA1_40*" Or _
            DT.Columns(i).ColumnName.ToUpper Like "JDA1_41*" Then
                DT.Columns.RemoveAt(i)
            End If
        Next

        Return DT

    End Function

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearForm()
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click, txtDateOfDespatch_From.TextChanged, txtDateOfDespatch_To.TextChanged, txtCreateDate_From.TextChanged, txtCreateDate_To.TextChanged, txtRegistrationNumber.TextChanged, txtInvoiceNo.TextChanged, chkDetail.CheckedChanged
        BindData()
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As LinkButton = e.Item.FindControl("lblNo")
        Dim JDA_ID As Integer = lblNo.Attributes("SystemID")

        Dim SQL As String = "SELECT * FROM tb_JDA_3 WHERE JDA3_ID=" & JDA_ID
        Dim Header As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Header)
        SQL = "SELECT * FROM tb_JDA_3_Detail  WHERE JDA3_ID=" & JDA_ID & vbLf
        SQL &= "ORDER BY JDA3_Detail_ID"
        Dim Detail As New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Detail)
        Dim RS As New ReportSourceTransformer
        Dim ReportSource As DataTable = RS.TransformJDA1(Header, Detail)
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        Session(UNIQUE_ID) = ReportSource
        Dim Script As String = "requireTextboxDialog('Print/ReportJDA_3.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,800,'','')"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", Script, True)

    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As LinkButton = e.Item.FindControl("lblNo")
        Dim lblJDA_ID As LinkButton = e.Item.FindControl("lblJDA_ID")
        Dim lblConsignorTax As LinkButton = e.Item.FindControl("lblConsignorTax")
        Dim lblConsigneeImporter As LinkButton = e.Item.FindControl("lblConsigneeImporter")
        Dim lblDate_Despatch As LinkButton = e.Item.FindControl("lblDate_Despatch")
        Dim lblReg_Number As LinkButton = e.Item.FindControl("lblReg_Number")
        Dim lblInvoice_No As LinkButton = e.Item.FindControl("lblInvoice_No")
        Dim lblCreateDate As LinkButton = e.Item.FindControl("lblCreateDate")

        lblNo.Text = e.Item.DataItem("No").ToString
        lblNo.Attributes("SystemID") = e.Item.DataItem("SystemID")
        lblConsignorTax.Text = e.Item.DataItem("JDA3_1Consignor").ToString.Split(vbLf)(0)
        lblConsigneeImporter.Text = e.Item.DataItem("JDA3_2Consignee_Importer").ToString.Split(vbLf)(0)
        lblReg_Number.Text = e.Item.DataItem("JDA3_12Registration_Number").ToString

        If Not IsDBNull(e.Item.DataItem("JDA3_5Date_Despatch")) Then
            lblDate_Despatch.Text = GL.ReportProgrammingDate(e.Item.DataItem("JDA3_5Date_Despatch"), "dd-MMM-yyyy")
        End If

        lblInvoice_No.Text = e.Item.DataItem("JDA3_INV_NO").ToString

        If Not IsDBNull(e.Item.DataItem("CREATE_DATE")) Then
            lblCreateDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("CREATE_DATE"), "dd-MMM-yyyy")
        End If

        If chkDetail.Checked Then
            Dim tdItem As HtmlTableCell = e.Item.FindControl("tdItem")
            Dim tdDesc As HtmlTableCell = e.Item.FindControl("tdDesc")
            Dim tdCode As HtmlTableCell = e.Item.FindControl("tdCode")
            Dim tdUnit1 As HtmlTableCell = e.Item.FindControl("tdUnit1")
            Dim tdTotal1 As HtmlTableCell = e.Item.FindControl("tdTotal1")
            Dim tdUnit2 As HtmlTableCell = e.Item.FindControl("tdUnit2")
            Dim tdTotal2 As HtmlTableCell = e.Item.FindControl("tdTotal2")

            tdItem.Visible = True
            tdDesc.Visible = True
            tdCode.Visible = True
            tdUnit1.Visible = True
            tdTotal1.Visible = True
            tdUnit2.Visible = True
            tdTotal2.Visible = True

            Dim lblItemNo As LinkButton = e.Item.FindControl("lblItemNo")
            Dim lblItemDesc As LinkButton = e.Item.FindControl("lblItemDesc")
            Dim lblItemCode As LinkButton = e.Item.FindControl("lblItemCode")
            Dim lblUnit1 As LinkButton = e.Item.FindControl("lblUnit1")
            Dim lblTotal1 As LinkButton = e.Item.FindControl("lblTotal1")
            Dim lblUnit2 As LinkButton = e.Item.FindControl("lblUnit2")
            Dim lblTotal2 As LinkButton = e.Item.FindControl("lblTotal2")

            lblItemNo.Text = e.Item.DataItem("JDA3_31Item_No").ToString
            lblItemDesc.Text = e.Item.DataItem("JDA3_33Description_Goods").ToString
            lblItemCode.Text = e.Item.DataItem("JDA3_34CodeNo").ToString
            lblUnit1.Text = e.Item.DataItem("JDA3_38PerUnit").ToString
            If Not IsDBNull(e.Item.DataItem("JDA3_39Total")) Then
                lblTotal1.Text = FormatNumber(e.Item.DataItem("JDA3_39Total"))
            End If
            lblUnit2.Text = "THB"
            If Not IsDBNull(e.Item.DataItem("JDA3_39Total_THB")) Then
                lblTotal2.Text = FormatNumber(e.Item.DataItem("JDA3_39Total_THB"))
            End If

        End If

    End Sub
End Class

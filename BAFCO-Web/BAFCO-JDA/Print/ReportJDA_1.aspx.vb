﻿Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine

Partial Class ReportJDA_1
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then Exit Sub


        Dim cc As New ReportDocument
        Dim DT As DataTable = Session(Request.QueryString("S").Replace("?", ""))

        'DT.Columns.Add("THB_1", GetType(Double))


        cc.Load(Server.MapPath("../Report/Report_JDA1.rpt"))
        cc.SetDataSource(DT)



        CrystalReportViewer1.ReportSource = cc

        Select Case Request.QueryString("Mode").ToUpper
            Case "PDF"
               '------------- Show File----------
                Response.AddHeader("Content-Type", "application/pdf")
                Response.AppendHeader("Content-Disposition", "filename=JDA_1" & Now.Day & "-" & GL.ReportMonthThai(Now.Month) & "-" & Now.Year & ".pdf")
                Dim B As Byte() = C.StreamToByte(cc.ExportToStream(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat))
                Response.BinaryWrite(B)


                'Case "EXCEL"
                '    Dim FilePath As String = Server.MapPath("../Temp/" & Session.SessionID & Now.ToOADate.ToString.Replace(".", "") & ".xls")
                '    cc.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, FilePath)
                '    '------------- Show File----------
                '    Response.AddHeader("Content-Type", "application/x-msexcel")
                '    Response.AppendHeader("Content-Disposition", "filename=JDA_1" & Now.Day & "-" & GL.ReportMonthThai(Now.Month) & "-" & Now.Year & ".xls")
                '    Dim F As IO.FileStream = IO.File.Open(FilePath, IO.FileMode.Open, IO.FileAccess.Read, IO.FileShare.ReadWrite)
                '    Dim B As Byte() = C.StreamToByte(F)
                '    F.Close()
                '    Response.BinaryWrite(B)
        End Select


    End Sub

End Class

﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportJDA_1.aspx.vb" Inherits="ReportJDA_1" %>

<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>

    <form id="form2" runat="server">
        <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
            AutoDataBind="true" DisplayGroupTree="False" HasCrystalLogo="False" 
            HasDrillUpButton="False" HasToggleGroupTreeButton="False" />
    </form>

</body>
</html>
﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Filter_JDA_1_3
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Dim GL As New GenericLib
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        If Not IsPostBack Then
            ClearForm()
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim SQL As String = ""

        Dim SQL1 As String = ""
        Dim SQL3 As String = ""
        'If ddlDocumentType.SelectedValue = 1 Then
        '------------JDA1----------------------
        SQL1 &= "  SELECT 'JDA 1'  DocumentType,1 JDA_TYPE,JDA_1.JDA1_ID JDA_ID,JDA_1.JDA1_5Date_Import Date_Import" & vbLf
        SQL1 &= "  ,JDA_1.JDA1_12Registration_Number Registration_Number,JDA1_1Consignor_Export  ConsignorExport,JDA1_2Consignee_Importer  ConsignorImporter,JDA1_INV_NO Invoice_No" & vbLf
        SQL1 &= "  ,BAFCO_Job_No,Client_Reference_No,COUNT(F.JDA1_ID) TotalFile" & vbLf
        SQL1 &= "  FROM tb_JDA_1 JDA_1" & vbLf
        SQL1 &= "  LEFT JOIN tb_JDA_1_File F ON JDA_1.JDA1_ID=F.JDA1_ID" & vbLf
        Dim Filter1 As String = ""
        If GL.IsProgrammingDate(txtDateOfImport.Text, "dd-MMM-yyyy") Then
            Dim TheDate As DateTime = CV.StringToDate(txtDateOfImport.Text, "dd-MMM-yyyy")
            Filter1 &= " DATEDIFF(dd,JDA_1.JDA1_5Date_Import,'" & CV.DateToString(TheDate, "yyyy-MM-dd") & "')=0  AND"
        End If
        If txtRegistrationNumber.Text <> "" Then
            Filter1 &= " JDA_1.JDA1_12Registration_Number Like '%" & txtRegistrationNumber.Text.Replace("'", "''") & "%' AND "
        End If
        If txtInvoiceNo.Text <> "" Then
            Filter1 &= " JDA1_INV_NO Like '%" & txtInvoiceNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_BAFCO_Job_No.Text <> "" Then
            Filter1 &= " BAFCO_Job_No Like '%" & txt_BAFCO_Job_No.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_Client_Reference_No.Text <> "" Then
            Filter1 &= " Client_Reference_No Like '%" & txt_Client_Reference_No.Text.Replace("'", "''") & "%' AND "
        End If

        If Filter1 <> "" Then
            SQL1 &= "  WHERE " & Filter1.Substring(0, Filter1.Length - 4)
        End If
       

        SQL1 &= "  GROUP BY JDA_1.JDA1_ID,JDA_1.JDA1_5Date_Import,JDA_1.JDA1_12Registration_Number,JDA1_1Consignor_Export,JDA1_2Consignee_Importer,JDA1_INV_NO,BAFCO_Job_No,Client_Reference_No" & vbLf

        'ElseIf ddlDocumentType.SelectedValue = 3 Then
        '------------JDA3----------------------
        SQL3 &= " SELECT 'JDA 3' DocumentType,3 JDA_TYPE,JDA_3.JDA3_ID JDA_ID,JDA_3.JDA3_5Date_Despatch Date_Import" & vbLf
        SQL3 &= " ,JDA_3.JDA3_12Registration_Number Registration_Number, JDA3_1Consignor ConsignorExport, JDA3_2Consignee_Importer ConsignorImporter,JDA3_INV_NO Invoice_No" & vbLf
        SQL3 &= " ,BAFCO_Job_No,Client_Reference_No,COUNT(F.JDA3_ID) TotalFile" & vbLf
        SQL3 &= " FROM tb_JDA_3 JDA_3" & vbLf
        SQL3 &= " LEFT JOIN tb_JDA_3_File F ON JDA_3.JDA3_ID=F.JDA3_ID" & vbLf
        Dim Filter3 As String = ""
        If GL.IsProgrammingDate(txtDateOfImport.Text, "dd-MMM-yyyy") Then
            Dim TheDate As DateTime = CV.StringToDate(txtDateOfImport.Text, "dd-MMM-yyyy")
            Filter3 &= " DATEDIFF(dd,JDA_3.JDA3_5Date_Despatch,'" & CV.DateToString(TheDate, "yyyy-MM-dd") & "')=0  AND"
        End If
        If txtRegistrationNumber.Text <> "" Then
            Filter3 &= " JDA_3.JDA3_12Registration_Number Like '%" & txtRegistrationNumber.Text & "%'   AND"
        End If
        If txtInvoiceNo.Text <> "" Then
            Filter3 &= " JDA3_INV_NO Like '%" & txtInvoiceNo.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_BAFCO_Job_No.Text <> "" Then
            Filter3 &= " BAFCO_Job_No Like '%" & txt_BAFCO_Job_No.Text.Replace("'", "''") & "%' AND "
        End If
        If txt_Client_Reference_No.Text <> "" Then
            Filter3 &= " Client_Reference_No Like '%" & txt_Client_Reference_No.Text.Replace("'", "''") & "%' AND "
        End If

        
        If Filter3 <> "" Then
            SQL3 &= "  WHERE " & Filter3.Substring(0, Filter3.Length - 4)
        End If
        SQL3 &= " GROUP BY JDA_3.JDA3_ID,JDA_3.JDA3_5Date_Despatch,JDA_3.JDA3_12Registration_Number,JDA3_1Consignor,JDA3_2Consignee_Importer,JDA3_INV_NO,BAFCO_Job_No,Client_Reference_No" & vbLf
        Select Case ddlDocumentType.SelectedValue
            Case 1
                SQL = SQL1

            Case 3
                SQL = SQL3

            Case Else
                SQL = SQL1 & vbLf & " UNION ALL" & vbLf & SQL3

        End Select


        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.DefaultView.Sort = "Date_Import DESC,Registration_Number,Invoice_No"
        DT = DT.DefaultView.ToTable.Copy

        Session("Filter_JDA_1_3") = DT
        Navigation.SesssionSourceName = "Filter_JDA_1_3"
        Navigation.RenderLayout()

        If Not IsNothing(DT) AndAlso DT.Rows.Count > 0 Then
            lblSearchResult.Text = "Found " & FormatNumber(DT.Rows.Count, 0) & " Record(s)."
            lblSearchResult.ForeColor = Drawing.Color.Green
        Else
            lblSearchResult.ForeColor = Drawing.Color.Red
            lblSearchResult.Text = "Item not found."
        End If


    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub


    Private Sub ClearForm()
        If Not IsNothing(Session("Search_ddlDocumentType")) Then
            ddlDocumentType.SelectedValue = Session("Serach_ddlDocumentType")
        Else
            ddlDocumentType.SelectedValue = 0
        End If
        If Not IsNothing(Session("Search_txtDateOfImport")) Then
            txtDateOfImport.Text = Session("Search_txtDateOfImport")
        End If
        If Not IsNothing(Session("Search_txtRegistrationNumber")) Then
            txtRegistrationNumber.Text = Session("Search_txtRegistrationNumber")
        End If
        If Not IsNothing(Session("Search_txtInvoiceNo")) Then
            txtInvoiceNo.Text = Session("Search_txtInvoiceNo")
        End If

        '---------------- Check Permission-------------
        If (IsNothing(Session("IS_JDA_1")) OrElse Not Session("IS_JDA_1")) AndAlso (IsNothing(Session("IS_JDA_3")) OrElse Not Session("IS_JDA_3")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx';", True)
            Exit Sub
        ElseIf IsNothing(Session("IS_JDA_1")) OrElse Not Session("IS_JDA_1") Then
            ddlDocumentType.SelectedIndex = 2 '--------Select JDA1
            ddlDocumentType.Enabled = False
            btnJDA_1.Visible = False
        ElseIf IsNothing(Session("IS_JDA_3")) OrElse Not Session("IS_JDA_3") Then
            ddlDocumentType.SelectedIndex = 1 '---------Select JDA3
            ddlDocumentType.Enabled = False
            btnJDA_3.Visible = False
        Else

        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ddlDocumentType.SelectedValue = -1
        txtRegistrationNumber.Text = ""
        txtInvoiceNo.Text = ""
        txtDateOfImport.Text = ""
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub btnNewDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewDocument.Click
        If IsNothing(Session("User_ID")) Then Exit Sub
        divNewDocument.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        divNewDocument.Visible = False
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim JDA_ID As Integer = e.CommandArgument
                Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
                Dim JDA_TYPE As Integer = btnDelete.Attributes("JDA_TYPE")

                Dim SQL As String = "DELETE FROM tb_JDA_" & JDA_TYPE & "_Detail WHERE JDA" & JDA_TYPE & "_ID=" & Val(JDA_ID) & vbLf
                SQL &= "DELETE FROM tb_JDA_" & JDA_TYPE & " WHERE JDA" & JDA_TYPE & "_ID=" & Val(JDA_ID)
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = SQL
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()

                '---------------- Delete File -------------
                GL.ForceDeleteFolder(Server.MapPath("File/JDA" & JDA_TYPE & "/" & JDA_ID))

                BindData()

            Case "Print"
                Dim JDA_ID As Integer = e.CommandArgument
                Dim JDA_TYPE As Integer = CType(e.CommandSource, ImageButton).Attributes("JDA_TYPE")
                Select Case JDA_TYPE
                    Case 1 '------------- JDA1
                        Dim SQL As String = "SELECT * FROM tb_JDA_1 WHERE JDA1_ID=" & JDA_ID
                        Dim Header As New DataTable
                        Dim DA As New SqlDataAdapter(SQL, ConnStr)
                        DA.Fill(Header)
                        SQL = "SELECT * FROM tb_JDA_1_Detail  WHERE JDA1_ID=" & JDA_ID & vbLf
                        SQL &= "ORDER BY JDA1_Detail_ID"
                        Dim Detail As New DataTable
                        DA = New SqlDataAdapter(SQL, ConnStr)
                        DA.Fill(Detail)
                        Dim RS As New ReportSourceTransformer
                        Dim ReportSource As DataTable = RS.TransformJDA1(Header, Detail)
                        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                        Session(UNIQUE_ID) = ReportSource
                        Dim Script As String = "requireTextboxDialog('Print/ReportJDA_1.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,800,'','')"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", Script, True)
                    Case 3
                        Dim SQL As String = "SELECT * FROM tb_JDA_3 WHERE JDA3_ID=" & JDA_ID
                        Dim Header As New DataTable
                        Dim DA As New SqlDataAdapter(SQL, ConnStr)
                        DA.Fill(Header)
                        SQL = "SELECT * FROM tb_JDA_3_Detail  WHERE JDA3_ID=" & JDA_ID & vbLf
                        SQL &= "ORDER BY JDA3_Detail_ID"
                        Dim Detail As New DataTable
                        DA = New SqlDataAdapter(SQL, ConnStr)
                        DA.Fill(Detail)
                        Dim RS As New ReportSourceTransformer
                        Dim ReportSource As DataTable = RS.TransformJDA3(Header, Detail)
                        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
                        Session(UNIQUE_ID) = ReportSource
                        Dim Script As String = "requireTextboxDialog('Print/ReportJDA_3.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,800,'','')"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", Script, True)
                End Select
        End Select

    End Sub


    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblJDA_ID As Label = e.Item.FindControl("lblJDA_ID")
        Dim lblConsignorExport As Label = e.Item.FindControl("lblConsignorExport")
        Dim lblConsignorImporter As Label = e.Item.FindControl("lblConsignorImporter")
        Dim lblDate_Import As Label = e.Item.FindControl("lblDate_Import")
        Dim lblReg_Number As Label = e.Item.FindControl("lblReg_Number")
        Dim lblInvoice_No As Label = e.Item.FindControl("lblInvoice_No")
        Dim lblDocumentType As Label = e.Item.FindControl("lblDocumentType")
        Dim lblFile As Label = e.Item.FindControl("lblFile")
        Dim lbl_BAFCO_Job_No As Label = e.Item.FindControl("lbl_BAFCO_Job_No")
        Dim lbl_Client_Reference_No As Label = e.Item.FindControl("lbl_Client_Reference_No")
        Dim btnEdit As HtmlAnchor = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnView As ImageButton = e.Item.FindControl("btnView")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblJDA_ID.Text = Val(e.Item.DataItem("JDA_ID"))

        lblConsignorExport.Text = e.Item.DataItem("ConsignorExport").ToString.Split(vbLf)(0)
        lblConsignorImporter.Text = e.Item.DataItem("ConsignorImporter").ToString.Split(vbLf)(0)
        lblReg_Number.Text = e.Item.DataItem("Registration_Number").ToString

        If Not IsDBNull(e.Item.DataItem("Date_Import")) Then
            lblDate_Import.Text = GL.ReportProgrammingDate(e.Item.DataItem("Date_Import"), "dd-MMM-yyyy")
        End If
        If Not IsDBNull(e.Item.DataItem("TotalFile")) Then
            lblFile.Text = e.Item.DataItem("TotalFile")
        Else
            lblFile.Text = "-"
        End If

        lblDocumentType.Text = e.Item.DataItem("DocumentType").ToString
        lblInvoice_No.Text = e.Item.DataItem("Invoice_No").ToString

        lbl_BAFCO_Job_No.Text = e.Item.DataItem("BAFCO_Job_No").ToString
        lbl_Client_Reference_No.Text = e.Item.DataItem("Client_Reference_No").ToString

        btnEdit.HRef = "JDA_" & e.Item.DataItem("JDA_TYPE") & ".aspx?JDA_Type=" & e.Item.DataItem("DocumentType").ToString & "&JDA_ID=" & e.Item.DataItem("JDA_ID")
        btnDelete.CommandArgument = e.Item.DataItem("JDA_ID")
        btnView.CommandArgument = e.Item.DataItem("JDA_ID")

        btnEdit.Attributes("JDA_TYPE") = e.Item.DataItem("JDA_TYPE")
        btnDelete.Attributes("JDA_TYPE") = e.Item.DataItem("JDA_TYPE")
        btnView.Attributes("JDA_TYPE") = e.Item.DataItem("JDA_TYPE")
    End Sub

    Protected Sub Search_Change(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocumentType.SelectedIndexChanged, txtDateOfImport.TextChanged, txtRegistrationNumber.TextChanged, txtInvoiceNo.TextChanged, txt_BAFCO_Job_No.TextChanged, txt_Client_Reference_No.TextChanged
        Session("Serach_ddlDocumentType") = ddlDocumentType.SelectedIndex
        Session("Search_txtDateOfImport") = txtDateOfImport.Text
        Session("Search_txtRegistrationNumber") = txtRegistrationNumber.Text
        Session("Search_txtInvoiceNo") = txtInvoiceNo.Text
        BindData()
    End Sub

    Protected Sub btnJDA_1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJDA_1.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='JDA_1.aspx?JDA_Type=JDA_1&JDA_ID=0';", True)
    End Sub

    Protected Sub btnJDA_3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJDA_3.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='JDA_3.aspx?JDA_Type=JDA_3&JDA_ID=0';", True)
    End Sub

End Class

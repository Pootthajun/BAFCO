﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class JDA_1
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim TC As New textControlLib
    Dim CV As New Converter

    Public Property PageUniqueID() As String
        Get
            Return ViewState("PageUniqueID").ToString
        End Get
        Set(ByVal value As String)
            ViewState("PageUniqueID") = value
        End Set
    End Property

    Public ReadOnly Property AttachedFileList() As List(Of GenericLib.FileStructure)
        Get
            Dim Result As New List(Of GenericLib.FileStructure)
            For i As Integer = 1 To 100 '-----Set Maximum 100 File-------
                Dim S_Name As String = "File_" & PageUniqueID & "_" & i
                If Not IsNothing(Session(S_Name)) Then
                    Dim F As GenericLib.FileStructure = Session(S_Name)
                    Result.Add(F)
                End If
            Next
            Return Result
        End Get
    End Property

    Public Property JDA_ID() As Integer
        Get
            Try
                Return lblJDA1_ID.Text
            Catch ex As Exception
                Return 0
            End Try
        End Get
        Set(ByVal value As Integer)
            lblJDA1_ID.Text = value
        End Set
    End Property

    Private WriteOnly Property GridDatasource() As DataTable
        Set(ByVal value As DataTable)
            rptData.DataSource = value
            rptData.DataBind()
            txt_Total_Line.Text = rptData.Items.Count
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("USER_ID")) Then Exit Sub

        If IsNothing(Session("IS_JDA_1")) OrElse Not Session("IS_JDA_1") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        End If

        If Not IsPostBack Then
            SetHeader()
            BindData()
            BindFile()
            TC.ImplementJavaIntegerText(txt_Total_Line, "center")
            BindINV()
        End If

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Accordion", "init_Accordion();", True)
    End Sub

    Private Sub SetHeader()
        JDA_ID = Val(Request.QueryString("JDA_ID"))
        PageUniqueID = Now.ToOADate.ToString.Replace(".", "")
    End Sub

#Region "BindTextAreaDLL"
    Private Sub Bind_JDA1_1Consignor_Export(ByVal DisplayDefault As Boolean)
        lst01.DataSource = Nothing
        lst01.DataBind()

        Dim SQL As String = "SELECT * FROM tb_JDA1_01 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DR_Blank As DataRow = DT.NewRow
        DR_Blank.Item("Name") = "....."
        DT.Rows.InsertAt(DR_Blank, 0)
        Dim DR_Master As DataRow = DT.NewRow
        DR_Master.Item("Name") = "*** Manage ***"
        DT.Rows.Add(DR_Master)
        lst01.DataSource = DT
        lst01.DataBind()

        If DisplayDefault Then
            DT.DefaultView.RowFilter = "IsDefault=True"
            If DT.DefaultView.Count > 0 Then
                txtJDA1_1Consignor_Export.Text = DT.DefaultView(0).Item("Name").ToString
            Else
                txtJDA1_1Consignor_Export.Text = ""
            End If
        End If
    End Sub

    Private Sub Bind_JDA1_2Consignee_Importer(ByVal DisplayDefault As Boolean)
        lst02.DataSource = Nothing
        lst02.DataBind()

        Dim SQL As String = "SELECT * FROM tb_JDA1_02 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DR_Blank As DataRow = DT.NewRow
        DR_Blank.Item("Name") = "....."
        DR_Blank.Item("Code") = ""
        DT.Rows.InsertAt(DR_Blank, 0)
        Dim DR_Master As DataRow = DT.NewRow
        DR_Master.Item("Name") = "*** Manage ***"
        DT.Rows.Add(DR_Master)
        lst02.DataSource = DT
        lst02.DataBind()

        If DisplayDefault Then
            DT.DefaultView.RowFilter = "IsDefault=True"
            If DT.DefaultView.Count > 0 Then
                txtJDA1_2Consignee_Importer.Text = DT.DefaultView(0).Item("Name").ToString
                txtJDA1_2Consignee_Importer_Code.Text = DT.DefaultView(0).Item("Code").ToString
            Else
                txtJDA1_2Consignee_Importer.Text = ""
                txtJDA1_2Consignee_Importer_Code.Text = ""
            End If
        End If
    End Sub

    Private Sub Bind_JDA1_3Authorised_Agent(ByVal DisplayDefault As Boolean)
        lst03.DataSource = Nothing
        lst03.DataBind()

        Dim SQL As String = "SELECT * FROM tb_JDA1_03 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DR_Blank As DataRow = DT.NewRow
        DR_Blank.Item("Name") = "....."
        DR_Blank.Item("Code") = ""
        DT.Rows.InsertAt(DR_Blank, 0)
        Dim DR_Master As DataRow = DT.NewRow
        DR_Master.Item("Name") = "*** Manage ***"
        DT.Rows.Add(DR_Master)
        lst03.DataSource = DT
        lst03.DataBind()

        If DisplayDefault Then
            DT.DefaultView.RowFilter = "IsDefault=True"
            If DT.DefaultView.Count > 0 Then
                txtJDA1_3Authorised_Agent.Text = DT.DefaultView(0).Item("Name").ToString
                txtJDA1_3Authorised_Agent_Code.Text = DT.DefaultView(0).Item("Code").ToString
            Else
                txtJDA1_3Authorised_Agent.Text = ""
                txtJDA1_3Authorised_Agent_Code.Text = ""
            End If
        End If

    End Sub

    Protected Sub lst01_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles lst01.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divItem As HtmlGenericControl = e.Item.FindControl("divItem")
        divItem.InnerHtml = e.Item.DataItem("Name").ToString.Replace(vbLf, "<br>")

        If e.Item.ItemIndex = CType(lst01.DataSource, DataTable).Rows.Count - 1 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtFieldID.ClientID & "').value=1; document.getElementById('" & btnMasterMultilineDialog.ClientID & "').click();"
            divItem.Style("text-align") = "center"
        ElseIf e.Item.ItemIndex = 0 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA1_1Consignor_Export.ClientID & "').value='';"
            divItem.Style("text-align") = "center"
        Else
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA1_1Consignor_Export.ClientID & "').value=replaceAll(this.innerHTML,'<br>','\n');"
        End If
    End Sub

    Protected Sub lst02_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles lst02.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divItem As HtmlGenericControl = e.Item.FindControl("divItem")
        divItem.InnerHtml = e.Item.DataItem("Name").ToString.Replace(vbLf, "<br>")

        If e.Item.ItemIndex = CType(lst02.DataSource, DataTable).Rows.Count - 1 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtFieldID.ClientID & "').value=2; document.getElementById('" & btnMasterMultilineDialog.ClientID & "').click();"
            divItem.Style("text-align") = "center"
        ElseIf e.Item.ItemIndex = 0 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA1_2Consignee_Importer.ClientID & "').value='';"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA1_2Consignee_Importer_Code.ClientID & "').value='';"
            divItem.Style("text-align") = "center"
        Else
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA1_2Consignee_Importer.ClientID & "').value=replaceAll(this.innerHTML,'<br>','\n');"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA1_2Consignee_Importer_Code.ClientID & "').value='" & e.Item.DataItem("Code").ToString.Replace("'", "\'") & "';"
        End If

    End Sub

    Protected Sub lst03_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles lst03.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim divItem As HtmlGenericControl = e.Item.FindControl("divItem")
        divItem.InnerHtml = e.Item.DataItem("Name").ToString.Replace(vbLf, "<br>")

        If e.Item.ItemIndex = CType(lst03.DataSource, DataTable).Rows.Count - 1 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtFieldID.ClientID & "').value=3; document.getElementById('" & btnMasterMultilineDialog.ClientID & "').click();"
            divItem.Style("text-align") = "center"
        ElseIf e.Item.ItemIndex = 0 Then
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA1_3Authorised_Agent.ClientID & "').value='';"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA1_3Authorised_Agent_Code.ClientID & "').value='';"
            divItem.Style("text-align") = "center"
        Else
            divItem.Attributes("onClick") = "document.getElementById('" & txtJDA1_3Authorised_Agent.ClientID & "').value=replaceAll(this.innerHTML,'<br>','\n');"
            divItem.Attributes("onClick") &= "document.getElementById('" & txtJDA1_3Authorised_Agent_Code.ClientID & "').value='" & e.Item.DataItem("Code").ToString.Replace("'", "\'") & "';"
        End If

    End Sub

    Protected Sub btnMasterMultilineDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMasterMultilineDialog.Click
        Select Case txtFieldID.Text
            Case "1"
                MasterEditor_MultiLine1.MasterName = "Consignor Export (Name and Address)"
                MasterEditor_MultiLine1.TableName = "tb_JDA1_01"
                MasterEditor_MultiLine1.RelateTransaction = "tb_JDA_1.JDA1_1Consignor_Export"
            Case "2"
                MasterEditor_MultiLine1.MasterName = "Consignee/Importer (Name and Address) "
                MasterEditor_MultiLine1.TableName = "tb_JDA1_02"
                MasterEditor_MultiLine1.RelateTransaction = "tb_JDA_1.JDA1_2Consignee_Importer"
            Case "3"
                MasterEditor_MultiLine1.MasterName = "Name and Address of Authorised Agent"
                MasterEditor_MultiLine1.TableName = "tb_JDA1_03"
                MasterEditor_MultiLine1.RelateTransaction = "tb_JDA_1.JDA1_3Authorised_Agent"
            Case Else
                Exit Sub
        End Select
        MasterEditor_MultiLine1.BindData()
        MasterEditor_MultiLine1.Visible = True
    End Sub

    Protected Sub MasterEditor_MultiLine1_ManageFinished(ByRef Editor As MasterEditor_MultiLine) Handles MasterEditor_MultiLine1.ManageFinished
        Select Case txtFieldID.Text
            Case "1"
                Bind_JDA1_1Consignor_Export(False)
            Case "2"
                Bind_JDA1_2Consignee_Importer(False)
            Case "3"
                Bind_JDA1_3Authorised_Agent(False)
            Case Else

        End Select
        MasterEditor_MultiLine1.Visible = False
    End Sub

    Protected Sub MasterEditor_MultiLine1_Selected(ByRef Editor As MasterEditor_MultiLine, ByVal Value As String) Handles MasterEditor_MultiLine1.Selected
        Select Case txtFieldID.Text
            Case "1"
                Bind_JDA1_1Consignor_Export(False)
                txtJDA1_1Consignor_Export.Text = Value.Replace("<br>", vbLf)
            Case "2"
                Bind_JDA1_2Consignee_Importer(False)
                txtJDA1_2Consignee_Importer.Text = Value.Replace("<br>", vbLf)
                txtJDA1_2Consignee_Importer_Code.Text = MasterEditor_MultiLine1.GetMasterCode(MasterEditor_MultiLine1.TableName, Value.Replace("<br>", vbLf))
            Case "3"
                Bind_JDA1_3Authorised_Agent(False)
                txtJDA1_3Authorised_Agent.Text = Value.Replace("<br>", vbLf)
                txtJDA1_3Authorised_Agent_Code.Text = MasterEditor_MultiLine1.GetMasterCode(MasterEditor_MultiLine1.TableName, Value.Replace("<br>", vbLf))
            Case Else

        End Select
        MasterEditor_MultiLine1.Visible = False
    End Sub

#End Region

#Region "Binding DLL"

    Private Sub Bind_JDA1_6Vessel_Flight_Conveyance(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA1_6Vessel_Flight_Conveyance.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA1_6Vessel_Flight_Conveyance.Items.Add(BlankItem)
        ddlJDA1_6Vessel_Flight_Conveyance.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA1_06 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA1_6Vessel_Flight_Conveyance.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA1_6Vessel_Flight_Conveyance.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA1_6Vessel_Flight_Conveyance.SelectedIndex = i + 1
            End If
        Next

        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA1_6Vessel_Flight_Conveyance.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA1_6Vessel_Flight_Conveyance_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA1_7Place_Import(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA1_7Place_Import.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA1_7Place_Import.Items.Add(BlankItem)
        ddlJDA1_7Place_Import.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA1_07 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA1_7Place_Import.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA1_7Place_Import.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA1_7Place_Import.SelectedIndex = i + 1
            End If
        Next

        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA1_7Place_Import.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA1_7Place_Import_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA1_8Place_Loading(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA1_8Place_Loading.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA1_8Place_Loading.Items.Add(BlankItem)
        ddlJDA1_8Place_Loading.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA1_08 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA1_8Place_Loading.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA1_8Place_Loading.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA1_8Place_Loading.SelectedIndex = i + 1
            End If
        Next

        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA1_8Place_Loading.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA1_8Place_Loading_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA1_21Country_payment(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA1_21Country_payment.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA1_21Country_payment.Items.Add(BlankItem)
        ddlJDA1_21Country_payment.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA1_21 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA1_21Country_payment.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA1_21Country_payment.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA1_21Country_payment.SelectedIndex = i + 1
            End If
        Next
        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA1_21Country_payment.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA1_21Country_payment_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA1_22Exchange_Rate(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA1_22Currency.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA1_22Currency.Items.Add(BlankItem)
        ddlJDA1_22Currency.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA1_22 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA1_22Currency.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA1_22Currency.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA1_22Currency.SelectedIndex = i + 1
            End If
        Next
        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA1_22Currency.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA1_22Currency_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Private Sub Bind_JDA1_29Currency(ByVal DisplayDefault As Boolean, Optional ByVal SelectedName As String = "")
        ddlJDA1_29Currency.Items.Clear()

        '------------------Add Blank Item -------------------
        Dim BlankItem As New ListItem("", "")
        ddlJDA1_29Currency.Items.Add(BlankItem)
        ddlJDA1_29Currency.SelectedIndex = 0

        '-------------- Add Master Item ---------------------
        Dim SQL As String = "SELECT * FROM tb_JDA1_22 ORDER BY ID"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item.Text = DT.Rows(i).Item("Name").ToString
            Item.Value = DT.Rows(i).Item("Name").ToString
            ddlJDA1_29Currency.Items.Add(Item)
            If DisplayDefault And SelectedName = "" AndAlso Not IsDBNull(DT.Rows(i).Item("IsDefault")) AndAlso DT.Rows(i).Item("IsDefault") Then
                ddlJDA1_29Currency.SelectedIndex = i + 1
            ElseIf DT.Rows(i).Item("Name").ToString = SelectedName Then '---------- OR Logic ----------
                ddlJDA1_29Currency.SelectedIndex = i + 1
            End If
        Next
        '------------ Add Adding Choice ---------------
        Dim MasterItem As New ListItem("*** Manage ***", "*** Manage ***")
        ddlJDA1_29Currency.Items.Add(MasterItem)
        '------------ Force Adapt Code-----------------
        ddlJDA1_29Currency_SelectedIndexChanged(Nothing, Nothing)
    End Sub

    Protected Sub ddlJDA1_6Vessel_Flight_Conveyance_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA1_6Vessel_Flight_Conveyance.SelectedIndexChanged
        If ddlJDA1_6Vessel_Flight_Conveyance.SelectedIndex = ddlJDA1_6Vessel_Flight_Conveyance.Items.Count - 1 Then
            MasterEditor1.MasterName = "Vessel Flight Conveyance"
            MasterEditor1.TableName = "tb_JDA1_06"
            MasterEditor1.RelateTransaction = "tb_JDA_1.JDA1_6Vessel_Flight_Conveyance"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else
            '--------------- Keep Last Value---------------
            ddlJDA1_6Vessel_Flight_Conveyance.Attributes("LastName") = ddlJDA1_6Vessel_Flight_Conveyance.Items(ddlJDA1_6Vessel_Flight_Conveyance.SelectedIndex).Value
        End If
    End Sub

    Protected Sub ddlJDA1_7Place_Import_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA1_7Place_Import.SelectedIndexChanged
        If ddlJDA1_7Place_Import.SelectedIndex = ddlJDA1_7Place_Import.Items.Count - 1 Then
            MasterEditor1.MasterName = "Port/Place of Import"
            MasterEditor1.TableName = "tb_JDA1_07"
            MasterEditor1.RelateTransaction = "tb_JDA_1.JDA1_7Place_Import"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else '----------- Apply Code ----------------
            txtJDA1_7Place_Import_Code.Text = MasterEditor1.GetMasterCode("tb_JDA1_07", ddlJDA1_7Place_Import.Items(ddlJDA1_7Place_Import.SelectedIndex).Value)
            '--------------- Keep Last Value---------------
            ddlJDA1_7Place_Import.Attributes("LastName") = ddlJDA1_7Place_Import.Items(ddlJDA1_7Place_Import.SelectedIndex).Value
        End If
    End Sub

    Protected Sub ddlJDA1_8Place_Loading_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA1_8Place_Loading.SelectedIndexChanged
        If ddlJDA1_8Place_Loading.SelectedIndex = ddlJDA1_8Place_Loading.Items.Count - 1 Then
            MasterEditor1.MasterName = "Port/Place of Loading"
            MasterEditor1.TableName = "tb_JDA1_08"
            MasterEditor1.RelateTransaction = "tb_JDA_1.JDA1_8Place_Loading"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else '----------- Apply Code ----------------
            txtJDA1_8Place_Loading_Code.Text = MasterEditor1.GetMasterCode("tb_JDA1_08", ddlJDA1_8Place_Loading.Items(ddlJDA1_8Place_Loading.SelectedIndex).Value)
            '--------------- Keep Last Value---------------
            ddlJDA1_8Place_Loading.Attributes("LastName") = ddlJDA1_8Place_Loading.Items(ddlJDA1_8Place_Loading.SelectedIndex).Value
        End If
    End Sub

    Protected Sub ddlJDA1_21Country_payment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA1_21Country_payment.SelectedIndexChanged
        If ddlJDA1_21Country_payment.SelectedIndex = ddlJDA1_21Country_payment.Items.Count - 1 Then
            MasterEditor1.MasterName = "Country to which payment FOB  of Goods to be made "
            MasterEditor1.TableName = "tb_JDA1_21"
            MasterEditor1.RelateTransaction = "tb_JDA_1.JDA1_21Country_payment"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else '----------- Apply Code ----------------
            txtJDA1_21Country_payment_Code.Text = MasterEditor1.GetMasterCode("tb_JDA1_21", ddlJDA1_21Country_payment.Items(ddlJDA1_21Country_payment.SelectedIndex).Value)
            '--------------- Keep Last Value---------------
            ddlJDA1_21Country_payment.Attributes("LastName") = ddlJDA1_21Country_payment.Items(ddlJDA1_21Country_payment.SelectedIndex).Value
        End If

    End Sub

    Protected Sub ddlJDA1_22Currency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA1_22Currency.SelectedIndexChanged
        '------------- Set Calculate Currency -------------
        If ddlJDA1_22Currency.SelectedIndex = ddlJDA1_22Currency.Items.Count - 1 Then
            MasterEditor1.MasterName = "Exchange Rate"
            MasterEditor1.TableName = "tb_JDA1_22"
            MasterEditor1.RelateTransaction = "tb_JDA_1.JDA1_22Currency"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else
            '--------------- Keep Last Value---------------
            ddlJDA1_22Currency.Attributes("LastName") = ddlJDA1_22Currency.Items(ddlJDA1_22Currency.SelectedIndex).Value
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
        End If
    End Sub

    Protected Sub ddlJDA1_29Currency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA1_29Currency.SelectedIndexChanged
        '------------- Set Calculate Currency -------------
        If ddlJDA1_29Currency.SelectedIndex = ddlJDA1_29Currency.Items.Count - 1 Then
            MasterEditor1.MasterName = "Currency"
            MasterEditor1.TableName = "tb_JDA1_22"
            MasterEditor1.RelateTransaction = "tb_JDA_1.JDA1_22Currency"
            MasterEditor1.BindData()
            MasterEditor1.Visible = True
        Else
            '--------------- Keep Last Value---------------
            ddlJDA1_29Currency.Attributes("LastName") = ddlJDA1_29Currency.Items(ddlJDA1_29Currency.SelectedIndex).Value
            'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
        End If
    End Sub

    Protected Sub MasterEditor1_Selected(ByRef Editor As MasterEditor, ByVal LastValue As String) Handles MasterEditor1.Selected ',MasterEditor1.AddFinished, 
        Select Case MasterEditor1.TableName
            Case "tb_JDA1_06"
                Bind_JDA1_6Vessel_Flight_Conveyance(False, LastValue)
            Case "tb_JDA1_07"
                Bind_JDA1_7Place_Import(False, LastValue)
            Case "tb_JDA1_08"
                Bind_JDA1_8Place_Loading(False, LastValue)
            Case "tb_JDA1_21"
                Bind_JDA1_21Country_payment(False, LastValue)
            Case "tb_JDA1_22"
                Select Case MasterEditor1.MasterName
                    Case "Exchange Rate" 'ddl22
                        Bind_JDA1_22Exchange_Rate(False, LastValue)
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
                    Case "Currency" 'ddl 29
                        Bind_JDA1_29Currency(False, LastValue)
                        'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "CalculateTotalLine", "calculateLineTotal();", True)
                End Select
        End Select
        MasterEditor1.Visible = False
    End Sub

    Protected Sub MasterEditor1_ManageFinished(ByRef Editor As MasterEditor) Handles MasterEditor1.ManageFinished
        Select Case MasterEditor1.TableName
            Case "tb_JDA1_06"
                Bind_JDA1_6Vessel_Flight_Conveyance(False, ddlJDA1_6Vessel_Flight_Conveyance.Attributes("LastName"))
            Case "tb_JDA1_07"
                Bind_JDA1_7Place_Import(False, ddlJDA1_7Place_Import.Attributes("LastName"))
            Case "tb_JDA1_08"
                Bind_JDA1_8Place_Loading(False, ddlJDA1_8Place_Loading.Attributes("LastName"))
            Case "tb_JDA1_21"
                Bind_JDA1_21Country_payment(False, ddlJDA1_21Country_payment.Attributes("LastName"))
            Case "tb_JDA1_22"
                Bind_JDA1_22Exchange_Rate(False, ddlJDA1_22Currency.Attributes("LastName"))
                Bind_JDA1_29Currency(False, ddlJDA1_29Currency.Attributes("LastName"))
        End Select
        MasterEditor1.Visible = False
    End Sub

#End Region

    Private Sub BindData()
        If IsNothing(Session("User_ID")) Then Exit Sub

        ClearForm()

        Dim SQL As String = "SELECT * FROM tb_JDA_1 WHERE JDA1_ID = " & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            JDA_ID = 0
            Exit Sub
        End If

        txt_BAFCO_Job_No.Text = DT.Rows(0).Item("BAFCO_Job_No").ToString
        txt_Client_Reference_No.Text = DT.Rows(0).Item("Client_Reference_No").ToString

        txtJDA1_1Consignor_Export.Text = DT.Rows(0).Item("JDA1_1Consignor_Export").ToString
        txtJDA1_2Consignee_Importer_Code.Text = DT.Rows(0).Item("JDA1_2Consignee_Importer_Code").ToString
        txtJDA1_2Consignee_Importer.Text = DT.Rows(0).Item("JDA1_2Consignee_Importer").ToString
        txtJDA1_3Authorised_Agent.Text = DT.Rows(0).Item("JDA1_3Authorised_Agent").ToString
        txtJDA1_3Authorised_Agent_Code.Text = DT.Rows(0).Item("JDA1_3Authorised_Agent_Code").ToString
        If Not IsDBNull(DT.Rows(0).Item("JDA1_4Mode_Transport")) Then
            For i As Integer = 0 To ddlJDA1_4Mode_Transport.Items.Count - 1
                If ddlJDA1_4Mode_Transport.Items(i).Value = DT.Rows(0).Item("JDA1_4Mode_Transport") Then
                    ddlJDA1_4Mode_Transport.SelectedIndex = i : Exit For
                End If
            Next
        End If
        If ddlJDA1_4Mode_Transport.SelectedIndex = 5 Then
            txtJDA1_10Others_Specify.Visible = True
            txtJDA1_10Others_Specify.Text = DT.Rows(0).Item("JDA1_10Others_Specify").ToString
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA1_5Date_Import")) Then
            txtJDA1_5Date_Import.Text = GL.ReportProgrammingDotDate(DT.Rows(0).Item("JDA1_5Date_Import"))
        End If

        Bind_JDA1_6Vessel_Flight_Conveyance(False, DT.Rows(0).Item("JDA1_6Vessel_Flight_Conveyance").ToString)

        If Not IsDBNull(DT.Rows(0).Item("JDA1_7Place_Import")) Then
            For i As Integer = 0 To ddlJDA1_7Place_Import.Items.Count - 1
                If ddlJDA1_7Place_Import.Items(i).Value = DT.Rows(0).Item("JDA1_7Place_Import") Then
                    ddlJDA1_7Place_Import.SelectedIndex = i : Exit For
                End If
            Next
        End If
        Bind_JDA1_7Place_Import(False, DT.Rows(0).Item("JDA1_7Place_Import").ToString)
        txtJDA1_7Place_Import_Code.Text = DT.Rows(0).Item("JDA1_7Place_Import_Code").ToString

        Bind_JDA1_8Place_Loading(False, DT.Rows(0).Item("JDA1_8Place_Loading").ToString)
        txtJDA1_8Place_Loading_Code.Text = DT.Rows(0).Item("JDA1_8Place_Loading_Code").ToString

        txtJDA1_9Via.Text = DT.Rows(0).Item("JDA1_9Via").ToString
        txtJDA1_9Via_Code.Text = DT.Rows(0).Item("JDA1_9Via_Code").ToString

        If Not IsDBNull(DT.Rows(0).Item("JDA1_10DateTime_Receipt")) Then
            txtJDA1_10DateTime_Receipt.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA1_10DateTime_Receipt"), "dd-MMM-yyyy")
        End If

        If Not IsDBNull(DT.Rows(0).Item("JDA1_11Attached_Invoice")) Then
            ckJDA1_11Attached_Invoice.Checked = DT.Rows(0).Item("JDA1_11Attached_Invoice")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA1_11Attached_Bill")) Then
            ckJDA1_11Attached_Bill.Checked = DT.Rows(0).Item("JDA1_11Attached_Bill")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA1_11Attached_Insurance")) Then
            ckJDA1_11Attached_Insurance.Checked = DT.Rows(0).Item("JDA1_11Attached_Insurance")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA1_11Attached_Letter")) Then
            ckJDA1_11Attached_Letter.Checked = DT.Rows(0).Item("JDA1_11Attached_Letter")
        End If
        If Not IsDBNull(DT.Rows(0).Item("JDA1_11Attached_Others")) Then
            ckJDA1_11Attached_Others.Checked = DT.Rows(0).Item("JDA1_11Attached_Others")
        End If

        txtJDA1_12Registration_Number.Text = DT.Rows(0).Item("JDA1_12Registration_Number").ToString
        txtJDA1_13Name_CustomsOffice.Text = DT.Rows(0).Item("JDA1_13Name_CustomsOffice").ToString
        txtJDA1_13Name_CustomsOffice_Code.Text = DT.Rows(0).Item("JDA1_13Name_CustomsOffice_Code").ToString
        txtJDA1_14Manifest_Regist_No.Text = DT.Rows(0).Item("JDA1_14Manifest_Regist_No").ToString
        If Not IsDBNull(DT.Rows(0).Item("JDA1_15Receipt_Duty_Date")) Then
            txtJDA1_15Receipt_Duty_Date.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA1_15Receipt_Duty_Date"), "dd-MMM-yyyy")
        End If
        txtJDA1_15Receipt_Duty.Text = DT.Rows(0).Item("JDA1_15Receipt_Duty").ToString
        txtJDA1_16ImportPermit_No.Text = DT.Rows(0).Item("JDA1_16ImportPermit_No").ToString
        txtJDA1_17ExchangeControl_Ref.Text = DT.Rows(0).Item("JDA1_17ExchangeControl_Ref").ToString
        txtJDA1_18Special_Treatment.Text = DT.Rows(0).Item("JDA1_18Special_Treatment").ToString
        txtJDA1_19Bil_Landing.Text = DT.Rows(0).Item("JDA1_19Bil_Landing").ToString
        txtJDA1_20Terms_Delivery.Text = DT.Rows(0).Item("JDA1_20Terms_Delivery").ToString

        Bind_JDA1_21Country_payment(False, DT.Rows(0).Item("JDA1_21Country_payment").ToString)
        txtJDA1_21Country_payment_Code.Text = DT.Rows(0).Item("JDA1_21Country_payment_Code").ToString

        Bind_JDA1_22Exchange_Rate(False, DT.Rows(0).Item("JDA1_22Currency").ToString)
        If IsNumeric(DT.Rows(0).Item("JDA1_22Exchange_Rate")) Then
            txtJDA1_22Exchange_Rate.Text = FormatNumber(DT.Rows(0).Item("JDA1_22Exchange_Rate"), 4)
        End If
        If IsNumeric(DT.Rows(0).Item("JDA1_23FOB_Value")) Then
            txtJDA1_23FOB_Value.Text = FormatNumber(DT.Rows(0).Item("JDA1_23FOB_Value"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA1_24Insurance")) Then
            txtJDA1_24Insurance.Text = FormatNumber(DT.Rows(0).Item("JDA1_24Insurance"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA1_25_Freight")) Then
            txtJDA1_25_Freight.Text = FormatNumber(DT.Rows(0).Item("JDA1_25_Freight"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA1_26CIF_Value")) Then
            txtJDA1_26CIF_Value.Text = FormatNumber(DT.Rows(0).Item("JDA1_26CIF_Value"))
        End If

        txtJDA1_27Unit.Text = DT.Rows(0).Item("JDA1_27Unit").ToString

        txtJDA1_27Gross_Wt.Text = DT.Rows(0).Item("JDA1_27Gross_Wt").ToString
        txtJDA1_28Measurement.Text = DT.Rows(0).Item("JDA1_28Measurement").ToString
        Bind_JDA1_29Currency(False, DT.Rows(0).Item("JDA1_29Currency").ToString)
        txtJDA1_29Other_charges.Text = DT.Rows(0).Item("JDA1_29Other_charges").ToString
        txtJDA1_30Marks_Numbers.Text = DT.Rows(0).Item("JDA1_30Marks_Numbers").ToString

        txtJDA1_39_Currency.Text = DT.Rows(0).Item("JDA1_39_Currency").ToString
        If IsNumeric(DT.Rows(0).Item("JDA1_39_PerUnit")) Then
            txtJDA1_39_PerUnit.Text = FormatNumber(DT.Rows(0).Item("JDA1_39_PerUnit"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA1_39_Total")) Then
            txtJDA1_39Total.Text = FormatNumber(DT.Rows(0).Item("JDA1_39_Total"))
        End If
        If IsNumeric(DT.Rows(0).Item("JDA1_41_Amount")) Then
            txtJDA1_41Amount.Text = TC.CalculateManualFloatText(DT.Rows(0).Item("JDA1_41_Amount"))
        End If

        If IsNumeric(DT.Rows(0).Item("JDA1_44_Amount")) Then
            txtJDA1_44Amount.Text = TC.CalculateManualFloatText(DT.Rows(0).Item("JDA1_44_Amount"))
        End If

        txtJDA1_48Name_Declarant.Text = DT.Rows(0).Item("JDA1_48Name_Declarant").ToString
        txtJDA1_49IDCard_Passport.Text = DT.Rows(0).Item("JDA1_49IDCard_Passport").ToString
        txtJDA1_50Status.Text = DT.Rows(0).Item("JDA1_50Status").ToString
        txtJDA1_51.Text = DT.Rows(0).Item("JDA1_51").ToString
        If Not IsDBNull(DT.Rows(0).Item("JDA1_51_Date")) Then
            txtJDA1_51_Date.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA1_51_Date"), "dd-MMM-yyyy")
        End If
        txtJDA1_52.Text = DT.Rows(0).Item("JDA1_52").ToString
        If Not IsDBNull(DT.Rows(0).Item("JDA1_52_Date")) Then
            txtJDA1_52_Date.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA1_52_Date"), "dd-MMM-yyyy")
        End If

        txtJDA1_53TotalDuty.Text = DT.Rows(0).Item("JDA1_53TotalDuty").ToString
        txtJDA1_54Other_Charges.Text = DT.Rows(0).Item("JDA1_54Other_Charges").ToString
        txtJDA1_55TotalAmount_Payable.Text = DT.Rows(0).Item("JDA1_55TotalAmount_Payable").ToString
        txtJDA1_56Manuscript_No.Text = DT.Rows(0).Item("JDA1_56Manuscript_No").ToString
        If Not IsDBNull(DT.Rows(0).Item("JDA1_56Manuscript_Date")) Then
            txtJDA1_56Date.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("JDA1_56Manuscript_Date"), "dd-MMM-yyyy")
        End If

        txtINV_NO.Text = DT.Rows(0).Item("JDA1_INV_NO").ToString
        txtTOTAL_SET.Text = DT.Rows(0).Item("JDA1_TOTAL_SET").ToString

        '----------------Bind Detail -----------------
        SQL = "SELECT * FROM tb_JDA_1_Detail WHERE JDA1_ID = " & JDA_ID & " ORDER BY JDA1_Detail_ID"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        GridDatasource = DT

    End Sub

    Private Sub ClearForm()

        txt_BAFCO_Job_No.Text = ""
        txt_Client_Reference_No.Text = ""

        Bind_JDA1_1Consignor_Export(True)

        TC.ImplementJavaOnlyNumberText(txtJDA1_2Consignee_Importer_Code, "center")
        Bind_JDA1_2Consignee_Importer(True)

        TC.ImplementJavaOnlyNumberText(txtJDA1_3Authorised_Agent_Code, "center")
        Bind_JDA1_3Authorised_Agent(True)

        ddlJDA1_4Mode_Transport.SelectedIndex = 0
        txtJDA1_10Others_Specify.Visible = False
        txtJDA1_10Others_Specify.Text = ""

        txtJDA1_5Date_Import.Text = ""

        Bind_JDA1_6Vessel_Flight_Conveyance(True)

        Bind_JDA1_7Place_Import(True)
        'TC.ImplementJavaIntegerText(txtJDA1_7Place_Import_Code) : txtJDA1_7Place_Import_Code.Style("text-align") = "center"

        Bind_JDA1_8Place_Loading(True)
        'TC.ImplementJavaIntegerText(txtJDA1_8Place_Loading_Code) : txtJDA1_8Place_Loading_Code.Style("text-align") = "center"

        txtJDA1_9Via.Text = ""
        txtJDA1_9Via_Code.Text = ""
        'TC.ImplementJavaIntegerText(txtJDA1_9Via_Code) : txtJDA1_9Via_Code.Style("text-align") = "center"

        txtJDA1_10DateTime_Receipt.Text = ""
        ckJDA1_11Attached_Invoice.Checked = False
        ckJDA1_11Attached_Bill.Checked = False
        ckJDA1_11Attached_Letter.Checked = False
        ckJDA1_11Attached_Insurance.Checked = False
        ckJDA1_11Attached_Others.Checked = False
        txtJDA1_12Registration_Number.Text = ""
        txtJDA1_13Name_CustomsOffice.Text = ""
        txtJDA1_13Name_CustomsOffice_Code.Text = ""
        txtJDA1_13Name_CustomsOffice_Code.Style("text-align") = "center"
        '------------------ ขาด Code ----------------

        txtJDA1_14Manifest_Regist_No.Text = ""
        txtJDA1_15Receipt_Duty_Date.Text = ""
        txtJDA1_15Receipt_Duty.Text = ""
        txtJDA1_16ImportPermit_No.Text = ""
        txtJDA1_17ExchangeControl_Ref.Text = ""
        txtJDA1_18Special_Treatment.Text = ""
        txtJDA1_19Bil_Landing.Text = ""
        txtJDA1_20Terms_Delivery.Text = "FOB"

        Bind_JDA1_21Country_payment(True)
        txtJDA1_21Country_payment_Code.Style("text-align") = "center"

        Bind_JDA1_22Exchange_Rate(True)
        txtJDA1_22Exchange_Rate.Text = "31.6354"
        TC.ImplementJavaFloatText(txtJDA1_22Exchange_Rate, 4, "center")
        txtJDA1_22Exchange_Rate.Attributes("onchange") &= " calculateLineTotal();"

        txtJDA1_23FOB_Value.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_23FOB_Value, "center")
        txtJDA1_23FOB_Value.Attributes("onchange") &= " calculateFOB();"


        txtJDA1_24Insurance.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_24Insurance, "center")

        txtJDA1_25_Freight.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_25_Freight, "center")

        txtJDA1_26CIF_Value.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_26CIF_Value, "center")

        txtJDA1_27Unit.Text = "KGS"

        txtJDA1_27Gross_Wt.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_27Gross_Wt, "center")

        txtJDA1_28Measurement.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_28Measurement, "center")

        txtCurrency1.Attributes("ReadOnly") = "True"
        txtCurrency2.Attributes("ReadOnly") = "True"
        txtCurrency3.Attributes("ReadOnly") = "True"
        txtCurrency4.Attributes("ReadOnly") = "True"

        Bind_JDA1_29Currency(True)
        txtJDA1_29Other_charges.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_29Other_charges, "center")

        txtJDA1_30Marks_Numbers.Text = "AS PER ATTACHED LIST" & vbLf & "(REF.No: xxxxxxxxx)"

        txtJDA1_39_Currency.Text = ""
        txtJDA1_39_PerUnit.Text = ""
        'txtJDA1_39_PerUnit.Attributes("ReadOnly") = "True"

        txtJDA1_39Total.Text = ""
        'txtJDA1_39Total.Attributes("ReadOnly") = "True"

        txtJDA1_41Amount.Text = ""
        'txtJDA1_41Amount.Attributes("ReadOnly") = "True"
        'TC.ImplementJavaFloatManualText(txtJDA1_41Amount)

        txtJDA1_44Amount.Text = ""
        'txtJDA1_44Amount.Attributes("ReadOnly") = "True"
        'TC.ImplementJavaFloatManualText(txtJDA1_44Amount)

        txtJDA1_48Name_Declarant.Text = ""
        txtJDA1_49IDCard_Passport.Text = ""
        'TC.ImplementJavaIntegerText(txtJDA1_49IDCard_Passport) : txtJDA1_49IDCard_Passport.Style("text-align") = "center"

        txtJDA1_50Status.Text = ""
        txtJDA1_51.Text = ""
        txtJDA1_51_Date.Text = ""
        txtJDA1_52.Text = ""
        txtJDA1_52_Date.Text = ""
        txtJDA1_56Manuscript_No.Text = ""
        txtJDA1_56Date.Text = ""
        txtJDA1_53TotalDuty.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_53TotalDuty, "center")

        txtJDA1_54Other_Charges.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_54Other_Charges, "center")

        txtJDA1_55TotalAmount_Payable.Text = ""
        TC.ImplementJavaMoneyText(txtJDA1_55TotalAmount_Payable, "center")

        txtTOTAL_SET.Text = ""
        txtINV_NO.Text = ""

        GridDatasource = Nothing

        '------------------ Set MasterEditor -----------
        MasterEditor1.Visible = False

        '------------------ Set File upload ------------
        windowUpload.Attributes("src") = "JDA_Upload.aspx?P=" & PageUniqueID & "&B=" & btnUpload.ClientID & "&t=" & Now.ToOADate.ToString.Replace(".", "")
        '------------------ Empty Buffer--------------
        For i As Integer = 1 To 100
            Dim SNAME As String = "File_" & PageUniqueID & "_" & i
            Session(SNAME) = Nothing
        Next
        DisplayFile()
    End Sub


    Protected Sub ddlJDA1_4Mode_Transport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJDA1_4Mode_Transport.SelectedIndexChanged
        txtJDA1_10Others_Specify.Visible = ddlJDA1_4Mode_Transport.SelectedIndex = 5
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If IsNothing(Session("USER_ID")) Then Exit Sub
        '------------------- Save Header-----------------
        Dim SQL As String = ""
        SQL = "SELECT * FROM tb_JDA_1 WHERE JDA1_ID =" & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow
        Dim _id As Integer = JDA_ID

        Dim Header As DataTable = CurrentHeader()
        If DT.Rows.Count = 0 Then
            DR = DT.NewRow
            DR.ItemArray = Header.Rows(0).ItemArray
            _id = GetNew_JDA1_ID()
            DR("JDA1_ID") = _id
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
            DT.Rows.Add(DR)
        Else
            DR = DT.Rows(0)
            For i As Integer = 1 To Header.Columns.Count - 5
                Dim Obj As Object = Header.Rows(0).Item(i)
                DR(i) = Obj
            Next
        End If

        DR("BAFCO_Job_No") = txt_BAFCO_Job_No.Text
        DR("Client_Reference_No") = txt_Client_Reference_No.Text
        DR("UPDATE_BY") = Session("User_ID")
        DR("UPDATE_DATE") = Now

        Dim CMD As New SqlCommandBuilder(DA)
        Try
            DA.Update(DT)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('" & ex.Message.Replace("'", "\'").Replace(vbLf, "\n") & "');", True)
            Exit Sub
        End Try

        JDA_ID = _id
        '--------------- Save Detail-----------------
        SQL = "DELETE FROM tb_JDA_1_Detail where JDA1_ID=" & JDA_ID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        SQL = "SELECT *,'' as INV FROM tb_JDA_1_Detail where 0=1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim Detail As DataTable = CurrentDetail()
        For i As Integer = 0 To Detail.Rows.Count - 1
            DR = DT.NewRow
            DR.ItemArray = Detail.Rows(i).ItemArray
            DR("JDA1_Detail_ID") = GetNew_JDA1_Detail_ID()
            DR("JDA1_ID") = JDA_ID
            DR("JDA1_31Item_No") = i + 1
            DR("JDA1_Invoice") = Detail.Rows(i).Item("JDA1_Invoice").ToString
            DR("CREATE_BY") = Session("USER_ID")
            DR("CREATE_DATE") = Now
            DR("UPDATE_BY") = Session("USER_ID")
            DR("UPDATE_DATE") = Now
            DT.Rows.Add(DR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)
            'DT.AcceptChanges()
        Next
        '---------------- Save Invoice --------------
        SQL = "DELETE FROM tb_JDA_1_Invoice where JDA1_ID=" & JDA_ID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        SQL = "SELECT * FROM tb_JDA_1_Invoice where 0=1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim Invoice As DataTable = CurrentINV()
        For i As Integer = 0 To Invoice.Rows.Count - 1
            DR = DT.NewRow
            DR("JDA1_Row") = i + 1
            DR("JDA1_ID") = JDA_ID
            DR("JDA1_Invoice") = Invoice.Rows(i).Item("JDA1_Invoice").ToString
            DT.Rows.Add(DR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)
            'DT.AcceptChanges()
        Next
        '---------------- Save File--------------
        SQL = "DELETE FROM tb_JDA_1_File where JDA1_ID=" & JDA_ID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        SQL = "SELECT * FROM tb_JDA_1_File where 0=1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Dim FT As DataTable = CurrentFile()
        Dim FileList As List(Of GenericLib.FileStructure) = AttachedFileList

        '------------ Clear Folder------------
        Dim Folder As String = Server.MapPath("File/JDA1")
        If Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        End If
        Folder &= "\" & JDA_ID
        If FT.Rows.Count > 0 And Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        ElseIf FT.Rows.Count = 0 And Directory.Exists(Folder) Then '-------- Clear unused folder ----------
            GL.ForceDeleteFolder(Folder)
        End If
        '-------- Clear All File-----------
        GL.ForceDeleteFileInFolder(Folder)

        For i As Integer = 0 To FT.Rows.Count - 1
            '--------------- Check Session is Alive -----------
            Dim F As GenericLib.FileStructure = FileList(i)
            If IsNothing(F) Then Continue For

            Dim FR As DataRow = DT.NewRow
            Dim F_ID As Integer = GetNew_JDA1_File_ID()
            FR("F_ID") = F_ID
            FR("JDA1_ID") = JDA_ID
            FR("AbsoluteName") = F.AbsoluteName
            Select Case F.FileType
                Case GenericLib.AllowFileType.JPG
                    FR("FileType") = "image/jpeg"
                Case GenericLib.AllowFileType.PDF
                    FR("FileType") = "application/pdf"
                Case GenericLib.AllowFileType.PNG
                    FR("FileType") = "image/png"
                Case Else
                    FR("FileType") = ""
            End Select
            DT.Rows.Add(FR)
            CMD = New SqlCommandBuilder(DA)
            DA.Update(DT)

            '------------ Save File To Folder--------------
            Dim FS As FileStream = File.Open(Folder & "\" & F_ID, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)
            FS.Write(F.Content, 0, F.Content.Length)
            FS.Close()
        Next

        ClearForm()
        BindData()
        BindFile()

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Save successfully');", True)
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        DisplayFile()
    End Sub

    Public Function CurrentHeader() As DataTable

        Dim SQL As String = ""
        SQL = "SELECT * FROM tb_JDA_1 WHERE 1=0"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Dim DR As DataRow = DT.NewRow

        DR("JDA1_ID") = JDA_ID
        DR("JDA1_1Consignor_Export") = txtJDA1_1Consignor_Export.Text
        DR("JDA1_2Consignee_Importer_Code") = txtJDA1_2Consignee_Importer_Code.Text
        DR("JDA1_2Consignee_Importer") = txtJDA1_2Consignee_Importer.Text
        DR("JDA1_3Authorised_Agent_Code") = txtJDA1_3Authorised_Agent_Code.Text
        DR("JDA1_3Authorised_Agent") = txtJDA1_3Authorised_Agent.Text
        DR("JDA1_4Mode_Transport") = ddlJDA1_4Mode_Transport.Items(ddlJDA1_4Mode_Transport.SelectedIndex).Value
        If GL.IsProgrammingDate(txtJDA1_5Date_Import.Text, "dd.MM.yyyy") Then
            DR("JDA1_5Date_Import") = CV.StringToDate(txtJDA1_5Date_Import.Text, "dd.MM.yyyy")
        Else
            DR("JDA1_5Date_Import") = DBNull.Value
        End If

        DR("JDA1_6Vessel_Flight_Conveyance") = ddlJDA1_6Vessel_Flight_Conveyance.Items(ddlJDA1_6Vessel_Flight_Conveyance.SelectedIndex).Value
        DR("JDA1_7Place_Import_Code") = txtJDA1_7Place_Import_Code.Text
        DR("JDA1_7Place_Import") = ddlJDA1_7Place_Import.Items(ddlJDA1_7Place_Import.SelectedIndex).Value
        DR("JDA1_8Place_Loading_Code") = txtJDA1_8Place_Loading_Code.Text
        DR("JDA1_8Place_Loading") = ddlJDA1_8Place_Loading.Items(ddlJDA1_8Place_Loading.SelectedIndex).Value
        DR("JDA1_9Via_Code") = txtJDA1_9Via_Code.Text
        DR("JDA1_9Via") = txtJDA1_9Via.Text

        If ddlJDA1_4Mode_Transport.SelectedIndex = 5 Then
            DR("JDA1_10Others_Specify") = txtJDA1_10Others_Specify.Text
        Else
            DR("JDA1_10Others_Specify") = ""
        End If
        If GL.IsProgrammingDate(txtJDA1_10DateTime_Receipt.Text, "dd-MMM-yyyy") Then
            DR("JDA1_10DateTime_Receipt") = CV.StringToDate(txtJDA1_10DateTime_Receipt.Text, "dd-MMM-yyyy")
        Else
            DR("JDA1_10DateTime_Receipt") = DBNull.Value
        End If

        DR("JDA1_11Attached_Invoice") = ckJDA1_11Attached_Invoice.Checked
        DR("JDA1_11Attached_Bill") = ckJDA1_11Attached_Bill.Checked
        DR("JDA1_11Attached_Insurance") = ckJDA1_11Attached_Insurance.Checked
        DR("JDA1_11Attached_Letter") = ckJDA1_11Attached_Letter.Checked
        DR("JDA1_11Attached_Others") = ckJDA1_11Attached_Others.Checked
        DR("JDA1_12Registration_Number") = txtJDA1_12Registration_Number.Text
        DR("JDA1_13Name_CustomsOffice") = txtJDA1_13Name_CustomsOffice.Text
        DR("JDA1_13Name_CustomsOffice_Code") = txtJDA1_13Name_CustomsOffice_Code.Text
        DR("JDA1_14Manifest_Regist_No") = txtJDA1_14Manifest_Regist_No.Text
        DR("JDA1_15Receipt_Duty") = txtJDA1_15Receipt_Duty.Text
        If GL.IsProgrammingDate(txtJDA1_15Receipt_Duty_Date.Text, "dd-MMM-yyyy") Then
            DR("JDA1_15Receipt_Duty_Date") = CV.StringToDate(txtJDA1_15Receipt_Duty_Date.Text, "dd-MMM-yyyy")
        Else
            DR("JDA1_15Receipt_Duty_Date") = DBNull.Value
        End If
        DR("JDA1_16ImportPermit_No") = txtJDA1_16ImportPermit_No.Text
        DR("JDA1_17ExchangeControl_Ref") = txtJDA1_17ExchangeControl_Ref.Text
        DR("JDA1_18Special_Treatment") = txtJDA1_18Special_Treatment.Text
        DR("JDA1_19Bil_Landing") = txtJDA1_19Bil_Landing.Text
        DR("JDA1_20Terms_Delivery") = txtJDA1_20Terms_Delivery.Text
        DR("JDA1_21Country_payment") = ddlJDA1_21Country_payment.Items(ddlJDA1_21Country_payment.SelectedIndex).Value
        DR("JDA1_21Country_payment_Code") = txtJDA1_21Country_payment_Code.Text
        DR("JDA1_22Currency") = ddlJDA1_22Currency.Items(ddlJDA1_22Currency.SelectedIndex).Value
        DR("JDA1_22Exchange_Rate") = txtJDA1_22Exchange_Rate.Text
        If IsNumeric(txtJDA1_23FOB_Value.Text.Replace(",", "")) Then
            DR("JDA1_23FOB_Value") = txtJDA1_23FOB_Value.Text.Replace(",", "")
        Else
            DR("JDA1_23FOB_Value") = DBNull.Value
        End If
        If IsNumeric(txtJDA1_24Insurance.Text.Replace(",", "")) Then
            DR("JDA1_24Insurance") = txtJDA1_24Insurance.Text.Replace(",", "")
        Else
            DR("JDA1_24Insurance") = DBNull.Value
        End If
        If IsNumeric(txtJDA1_25_Freight.Text.Replace(",", "")) Then
            DR("JDA1_25_Freight") = txtJDA1_25_Freight.Text.Replace(",", "")
        Else
            DR("JDA1_25_Freight") = DBNull.Value
        End If
        If IsNumeric(txtJDA1_26CIF_Value.Text.Replace(",", "")) Then
            DR("JDA1_26CIF_Value") = txtJDA1_26CIF_Value.Text.Replace(",", "")
        Else
            DR("JDA1_26CIF_Value") = DBNull.Value
        End If

        DR("JDA1_27Unit") = txtJDA1_27Unit.Text
        If IsNumeric(txtJDA1_27Gross_Wt.Text.Replace(",", "")) Then
            'DR("JDA1_27Gross_Wt") = txtJDA1_27Gross_Wt.Text.Replace(",", "")
            DR("JDA1_27Gross_Wt") = txtJDA1_27Gross_Wt.Text
        Else
            DR("JDA1_27Gross_Wt") = DBNull.Value
        End If
        DR("JDA1_28Measurement") = txtJDA1_28Measurement.Text
        DR("JDA1_29Currency") = ddlJDA1_29Currency.Items(ddlJDA1_29Currency.SelectedIndex).Value
        DR("JDA1_29Other_charges") = txtJDA1_29Other_charges.Text
        DR("JDA1_30Marks_Numbers") = txtJDA1_30Marks_Numbers.Text

        If IsNumeric(txtJDA1_39_PerUnit.Text.Replace(",", "")) Then
            DR("JDA1_39_PerUnit") = txtJDA1_39_PerUnit.Text.Replace(",", "")
        Else
            DR("JDA1_39_PerUnit") = DBNull.Value
        End If
        DR("JDA1_39_Currency") = txtJDA1_39_Currency.Text

        DR("JDA1_48Name_Declarant") = txtJDA1_48Name_Declarant.Text
        DR("JDA1_49IDCard_Passport") = txtJDA1_49IDCard_Passport.Text
        DR("JDA1_50Status") = txtJDA1_50Status.Text
        If GL.IsProgrammingDate(txtJDA1_51_Date.Text, "dd-MMM-yyyy") Then
            DR("JDA1_51_Date") = CV.StringToDate(txtJDA1_51_Date.Text, "dd-MMM-yyyy")
        Else
            DR("JDA1_51_Date") = DBNull.Value
        End If
        DR("JDA1_51") = txtJDA1_51.Text
        If GL.IsProgrammingDate(txtJDA1_52_Date.Text, "dd-MMM-yyyy") Then
            DR("JDA1_52_Date") = CV.StringToDate(txtJDA1_52_Date.Text, "dd-MMM-yyyy")
        Else
            DR("JDA1_52_Date") = DBNull.Value
        End If
        DR("JDA1_52") = txtJDA1_52.Text
        DR("JDA1_53TotalDuty") = txtJDA1_53TotalDuty.Text
        DR("JDA1_54Other_Charges") = txtJDA1_54Other_Charges.Text
        DR("JDA1_55TotalAmount_Payable") = txtJDA1_55TotalAmount_Payable.Text
        DR("JDA1_56Manuscript_No") = txtJDA1_56Manuscript_No.Text
        If GL.IsProgrammingDate(txtJDA1_52_Date.Text, "dd-MMM-yyyy") Then
            DR("JDA1_56Manuscript_Date") = CV.StringToDate(txtJDA1_52_Date.Text, "dd-MMM-yyyy")
        Else
            DR("JDA1_56Manuscript_Date") = DBNull.Value
        End If
        DR("JDA1_INV_NO") = txtINV_NO.Text
        DR("JDA1_TOTAL_SET") = txtTOTAL_SET.Text

        If IsNumeric(txtJDA1_39Total.Text.Replace(",", "")) Then
            DR("JDA1_39_Total") = txtJDA1_39Total.Text.Replace(",", "")
        Else
            DR("JDA1_39_Total") = DBNull.Value
        End If
        If IsNumeric(txtJDA1_41Amount.Text.Replace(",", "")) Then
            DR("JDA1_41_Amount") = txtJDA1_41Amount.Text.Replace(",", "")
        Else
            DR("JDA1_41_Amount") = DBNull.Value
        End If
        If IsNumeric(txtJDA1_44Amount.Text.Replace(",", "")) Then
            DR("JDA1_44_Amount") = txtJDA1_44Amount.Text.Replace(",", "")
        Else
            DR("JDA1_44_Amount") = DBNull.Value
        End If

        DT.Rows.Add(DR)
        Return DT
    End Function

    Private Function GetNew_JDA1_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(JDA1_ID),0)+1 FROM tb_JDA_1"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Function GetNew_JDA1_Detail_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(JDA1_Detail_ID),0)+1 FROM tb_JDA_1_Detail "
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Private Function GetNew_JDA1_File_ID() As Integer
        Dim SQL As String = "SELECT IsNull(MAX(F_ID),0)+1 FROM tb_JDA_1_File"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0).Item(0)
    End Function

    Protected Sub rptFile_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptFile.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim alink As HtmlAnchor = e.Item.FindControl("alink")
                Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

                '-------------- Clear Session -------------
                Dim SName As String = "File_" & PageUniqueID & "_" & e.CommandArgument
                RemoveFile(CInt(e.CommandArgument))
                '-------------- Display File --------------
                DisplayFile()
        End Select
    End Sub

    Protected Sub rptFile_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFile.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim liType As HtmlGenericControl = e.Item.FindControl("liType")
        Dim alink As HtmlAnchor = e.Item.FindControl("alink")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Select Case e.Item.DataItem("FileType")
            Case GenericLib.AllowFileType.JPG, GenericLib.AllowFileType.PNG
                liType.Attributes("class") = "file_list picture"
            Case GenericLib.AllowFileType.PDF
                liType.Attributes("class") = "file_list pdf"
        End Select
        alink.InnerHtml = e.Item.DataItem("AbsoluteName")
        btnDelete.CommandArgument = e.Item.DataItem("FileIndex")
        Dim SName As String = "File_" & PageUniqueID & "_" & e.Item.DataItem("FileIndex")
        alink.HRef = "RenderFile.aspx?S=" & SName & "&"
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Insert"
                Dim DT As DataTable = CurrentDetail()
                Dim NewID As Object = DT.Compute("MAX(JDA1_Detail_ID)", "")
                If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
                Dim DR As DataRow = DT.NewRow
                DR("JDA1_Detail_ID") = NewID
                DT.Rows.InsertAt(DR, e.Item.ItemIndex)
                GridDatasource = DT
            Case "Delete"
                Dim DT As DataTable = CurrentDetail()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch ex As Exception
                End Try
                GridDatasource = DT
                DisplayINV()
        End Select
    End Sub

    Dim LastRow As Integer = 0
    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim txtRow As TextBox = e.Item.FindControl("txtRow")
        Dim lblJDA1_Detail_ID As Label = e.Item.FindControl("lblJDA1_Detail_ID")
        'Dim txtJDA1_31Item_No As TextBox = e.Item.FindControl("txtJDA1_31Item_No")
        'Dim txtJDA1_32No_Type_Packages As TextBox = e.Item.FindControl("txtJDA1_32No_Type_Packages")
        Dim txtJDA1_33Description_Goods As TextBox = e.Item.FindControl("txtJDA1_33Description_Goods")
        Dim txtJDA1_34CodeNo As TextBox = e.Item.FindControl("txtJDA1_34CodeNo")
        Dim txtJDA1_35Unit As TextBox = e.Item.FindControl("txtJDA1_35Unit")
        Dim txtJDA1_36CountryCode As TextBox = e.Item.FindControl("txtJDA1_36CountryCode")
        Dim txtJDA1_37Qtybased As TextBox = e.Item.FindControl("txtJDA1_37Qtybased")
        Dim txtJDA1_38PerUnit As TextBox = e.Item.FindControl("txtJDA1_38PerUnit")
        Dim txtJDA1_39Total As TextBox = e.Item.FindControl("txtJDA1_39Total")
        Dim txtJDA1_40Rate As TextBox = e.Item.FindControl("txtJDA1_40Rate")
        Dim txtJDA1_41Amount As TextBox = e.Item.FindControl("txtJDA1_41Amount")
        Dim txtJDA1_42Type As TextBox = e.Item.FindControl("txtJDA1_42Type")
        Dim txtJDA1_43Rate As TextBox = e.Item.FindControl("txtJDA1_43Rate")
        Dim txtJDA1_44Amount As TextBox = e.Item.FindControl("txtJDA1_44Amount")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim lblINV As Label = e.Item.FindControl("lblINV")

        'TC.ImplementJavaMoneyText(txtJDA1_39Total)
        txtJDA1_39Total.Style.Item("text-align") = "right"
        'TC.ImplementJavaMoneyText(txtJDA1_40Rate, "center")
        'TC.ImplementJavaFloatManualText(txtJDA1_41Amount)
        txtJDA1_41Amount.Style.Item("text-align") = "right"
        'TC.ImplementJavaFloatManualText(txtJDA1_43Rate, "center")
        'TC.ImplementJavaFloatManualText(txtJDA1_44Amount)
        txtJDA1_44Amount.Style.Item("text-align") = "right"

        If Not IsDBNull(e.Item.DataItem("JDA1_Detail_ID")) Then
            lblJDA1_Detail_ID.Attributes("Detail_ID") = e.Item.DataItem("JDA1_Detail_ID")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_31Item_No")) Then
            txtRow.Text = e.Item.DataItem("JDA1_31Item_No")
        End If
        'If Not IsDBNull(e.Item.DataItem("JDA1_32No_Type_Packages")) Then
        '    txtJDA1_32No_Type_Packages.Text = e.Item.DataItem("JDA1_32No_Type_Packages")
        'End If
        If Not IsDBNull(e.Item.DataItem("JDA1_33Description_Goods")) Then
            txtJDA1_33Description_Goods.Text = e.Item.DataItem("JDA1_33Description_Goods")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_34CodeNo")) Then
            txtJDA1_34CodeNo.Text = e.Item.DataItem("JDA1_34CodeNo")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_35Unit")) Then
            txtJDA1_35Unit.Text = e.Item.DataItem("JDA1_35Unit")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_36CountryCode")) Then
            txtJDA1_36CountryCode.Text = e.Item.DataItem("JDA1_36CountryCode")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_37Qtybased")) Then
            txtJDA1_37Qtybased.Text = e.Item.DataItem("JDA1_37Qtybased")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_38PerUnit")) Then
            txtJDA1_38PerUnit.Text = e.Item.DataItem("JDA1_38PerUnit")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_39Total")) Then
            txtJDA1_39Total.Text = FormatNumber(e.Item.DataItem("JDA1_39Total"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_40Rate")) Then
            txtJDA1_40Rate.Text = FormatNumber(e.Item.DataItem("JDA1_40Rate"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_41Amount")) Then
             txtJDA1_41Amount.Text = TC.CalculateManualFloatText(e.Item.DataItem("JDA1_41Amount"))
        End If

        If Not IsDBNull(e.Item.DataItem("DA1_42Type")) Then
            txtJDA1_42Type.Text = e.Item.DataItem("DA1_42Type")
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_43Rate")) Then
            'txtJDA1_43Rate.Text = FormatNumber(e.Item.DataItem("JDA1_43Rate"))
            txtJDA1_43Rate.Text = TC.CalculateManualFloatText(e.Item.DataItem("JDA1_43Rate"))
        End If
        If Not IsDBNull(e.Item.DataItem("JDA1_44Amount")) Then
            txtJDA1_44Amount.Text = TC.CalculateManualFloatText(e.Item.DataItem("JDA1_44Amount"))
        End If
        lblINV.Text = e.Item.DataItem("JDA1_Invoice").ToString
        btnDelete.CommandArgument = e.Item.DataItem("JDA1_Detail_ID")

        '---------------- Calculate THB -----------------------
        Dim txtJDA1_39THB As TextBox = e.Item.FindControl("txtJDA1_39THB")
        'txtJDA1_39THB.Attributes("ReadOnly") = "True"
        Dim ExchangeRate As String = txtJDA1_22Exchange_Rate.Text.Replace(",", "")
        If IsNumeric(ExchangeRate) And IsNumeric(txtJDA1_39Total.Text.Replace(",", "")) Then
            Dim Total As Double = Val(ExchangeRate) * Val(txtJDA1_39Total.Text.Replace(",", ""))
            txtJDA1_39THB.Text = FormatNumber(Total)

            '------------------ Calculate Import Duty --------------

            'If IsNumeric(txtJDA1_43Rate.Text.Replace(",", "")) Then
            '    Dim Rate As Double = Val(txtJDA1_43Rate.Text.Replace(",", ""))
            '    Dim Amount As Double = Total * Rate / 100
            '    ' txtJDA1_41Amount.Text = FormatNumber(Amount, 0)

            '    txtJDA1_41Amount.Text = FormatNumber(Amount)
            'End If
        End If
        txtJDA1_39Total.Attributes("onchange") &= "calculateLineTotal();"
        TC.ImplementJavaFloatManualText(txtJDA1_39THB)
        'TC.ImplementJavaMoneyText()


        Dim Param As String = "document.getElementById('" & txtJDA1_39THB.ClientID & "'),document.getElementById('" & txtJDA1_40Rate.ClientID & "'),document.getElementById('" & txtJDA1_41Amount.ClientID & "')"
        Dim ImpScript As String = "CalculateImportDuty(" & Param & "); sumImportDuty();"
        

        Param = "document.getElementById('" & txtJDA1_39THB.ClientID & "'),document.getElementById('" & txtJDA1_43Rate.ClientID & "'),document.getElementById('" & txtJDA1_44Amount.ClientID & "')"
        Dim OtherScript As String = "CalculateImportDuty(" & Param & "); sumOtherTax();"


        txtJDA1_41Amount.Attributes("onchange") &= OtherScript & " sumImportDuty();"
        txtJDA1_43Rate.Attributes("onchange") &= OtherScript & " sumImportDuty();"

        txtJDA1_39Total.Attributes("onchange") &= ImpScript & OtherScript
        txtJDA1_39THB.Attributes("onchange") &= ImpScript & OtherScript
        txtJDA1_40Rate.Attributes("onchange") &= ImpScript & OtherScript

        txtJDA1_44Amount.Attributes("onchange") &= " sumOtherTax();"

        txtRow.Attributes("ReadOnly") = "True"
        '----------- Implement Search Javascript----------------------
        Dim SearchScript As String = "requireTextboxDialog('Search_Detail.aspx?JDAType=1&txt1=" & txtJDA1_33Description_Goods.ClientID & "&txt2=" & txtJDA1_34CodeNo.ClientID & "&t=1',700,500,'','');"
        txtJDA1_33Description_Goods.Attributes("ondblclick") = SearchScript
        txtJDA1_34CodeNo.Attributes("ondblclick") = SearchScript
    End Sub

    Private Function CurrentDetail() As DataTable
        Dim SQL As String = ""
        SQL = "SELECT * FROM tb_JDA_1_Detail WHERE 0=1"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        For Each Item As RepeaterItem In rptData.Items
            If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For
            Dim txtRow As TextBox = Item.FindControl("txtRow")
            Dim lblJDA1_Detail_ID As Label = Item.FindControl("lblJDA1_Detail_ID")
            Dim txtJDA1_32No_Type_Packages As TextBox = Item.FindControl("txtJDA1_32No_Type_Packages")
            Dim txtJDA1_33Description_Goods As TextBox = Item.FindControl("txtJDA1_33Description_Goods")
            Dim txtJDA1_34CodeNo As TextBox = Item.FindControl("txtJDA1_34CodeNo")
            Dim txtJDA1_35Unit As TextBox = Item.FindControl("txtJDA1_35Unit")
            Dim txtJDA1_36CountryCode As TextBox = Item.FindControl("txtJDA1_36CountryCode")
            Dim txtJDA1_37Qtybased As TextBox = Item.FindControl("txtJDA1_37Qtybased")
            Dim txtJDA1_38PerUnit As TextBox = Item.FindControl("txtJDA1_38PerUnit")
            Dim txtJDA1_39Total As TextBox = Item.FindControl("txtJDA1_39Total")
            Dim txtJDA1_39THB As TextBox = Item.FindControl("txtJDA1_39THB")
            Dim txtJDA1_40Rate As TextBox = Item.FindControl("txtJDA1_40Rate")
            Dim txtJDA1_41Amount As TextBox = Item.FindControl("txtJDA1_41Amount")
            Dim txtJDA1_42Type As TextBox = Item.FindControl("txtJDA1_42Type")
            Dim txtJDA1_43Rate As TextBox = Item.FindControl("txtJDA1_43Rate")
            Dim txtJDA1_44Amount As TextBox = Item.FindControl("txtJDA1_44Amount")
            Dim btnDelete As ImageButton = Item.FindControl("btnDelete")
            Dim lblINV As Label = Item.FindControl("lblINV")

            Dim DR As DataRow = DT.NewRow

            If IsNumeric(txtRow.Text) Then
                DR("JDA1_31Item_No") = txtRow.Text
            End If
            DR("JDA1_Detail_ID") = (lblJDA1_Detail_ID.Attributes("Detail_ID"))
            'DR("JDA1_32No_Type_Packages") = txtJDA1_32No_Type_Packages.Text
            DR("JDA1_33Description_Goods") = txtJDA1_33Description_Goods.Text
            DR("JDA1_34CodeNo") = txtJDA1_34CodeNo.Text
            DR("JDA1_35Unit") = txtJDA1_35Unit.Text
            DR("JDA1_36CountryCode") = txtJDA1_36CountryCode.Text
            DR("JDA1_37Qtybased") = txtJDA1_37Qtybased.Text
            DR("JDA1_38PerUnit") = txtJDA1_38PerUnit.Text

            If IsNumeric(txtJDA1_39Total.Text) Then
                DR("JDA1_39Total") = txtJDA1_39Total.Text.Replace(",", "")
            End If
            If IsNumeric(txtJDA1_40Rate.Text) Then
                DR("JDA1_40Rate") = txtJDA1_40Rate.Text.Replace(",", "")
            End If
            If IsNumeric(txtJDA1_41Amount.Text) Then
                DR("JDA1_41Amount") = txtJDA1_41Amount.Text.Replace(",", "")
            End If
            DR("DA1_42Type") = txtJDA1_42Type.Text.Replace(",", "")
            If IsNumeric(txtJDA1_43Rate.Text) Then
                DR("JDA1_43Rate") = txtJDA1_43Rate.Text.Replace(",", "")
            End If
            If IsNumeric(txtJDA1_44Amount.Text) Then
                DR("JDA1_44Amount") = txtJDA1_44Amount.Text.Replace(",", "")
            End If

            If IsNumeric(txtJDA1_39THB.Text) Then
                DR("JDA1_39Total_THB") = txtJDA1_39THB.Text.Replace(",", "")
            End If
            DR("JDA1_Invoice") = lblINV.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = CurrentDetail()
        Dim NewID As Object = DT.Compute("MAX(JDA1_Detail_ID)", "")
        If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
        Dim DR As DataRow = DT.NewRow
        DR("JDA1_Detail_ID") = NewID
        DT.Rows.Add(DR)
        GridDatasource = DT
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim RS As New ReportSourceTransformer
        Dim Detail As DataTable = CurrentDetail()
        Dim ReportSource As DataTable = RS.TransformJDA1(CurrentHeader, Detail)
        Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
        Session(UNIQUE_ID) = ReportSource
        Dim Script As String = "requireTextboxDialog('Print/ReportJDA_1.aspx?Mode=PDF&S=" & UNIQUE_ID & "',900,800,'','')"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", Script, True)

    End Sub

    Protected Sub btnCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalculate.Click
        Dim Script As String = "calculateFOB(); calculateLineTotal();"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", Script, True)
    End Sub

    Private Sub BindFile()

        Dim DT As New DataTable
        Dim SQL As String = "SELECT * FROM tb_JDA_1_File WHERE JDA1_ID=" & JDA_ID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT)

        Dim Folder As String = Server.MapPath("File/JDA1")
        If Not Directory.Exists(Folder) Then
            Directory.CreateDirectory(Folder)
        End If
        Folder &= "\" & JDA_ID
        If Not Directory.Exists(Folder) Then Exit Sub

        '------------ Get File To Session ---------
        Dim LastIndex As Integer = 0
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Path As String = Folder & "\" & DT.Rows(i).Item("F_ID")
            If Not File.Exists(Path) Then Continue For
            LastIndex += 1
            Dim F As New GenericLib.FileStructure
            Dim FS As FileStream = File.Open(Path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)
            F.Content = CV.StreamToByte(FS)
            FS.Close()
            F.AbsoluteName = DT.Rows(i).Item("AbsoluteName").ToString
            F.FileIndex = LastIndex
            Select Case DT.Rows(i).Item("FileType").ToString
                Case "image/jpeg"
                    F.FileType = GenericLib.AllowFileType.JPG
                Case "image/png"
                    F.FileType = GenericLib.AllowFileType.PNG
                Case Else
                    F.FileType = GenericLib.AllowFileType.PDF
            End Select

            Dim SName As String = "File_" & PageUniqueID & "_" & F.FileIndex
            Session(SName) = F
        Next

        DisplayFile()
    End Sub

    Private Sub DisplayFile()
        Dim DT As DataTable = CurrentFile()
        rptFile.DataSource = DT
        rptFile.DataBind()
    End Sub

    Private Function CurrentFile() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("AbsoluteName")
        DT.Columns.Add("FileType", GetType(Integer))
        DT.Columns.Add("FileIndex", GetType(Integer))

        Dim FileList As List(Of GenericLib.FileStructure) = AttachedFileList
        For Each F In FileList
            Dim DR As DataRow = DT.NewRow
            DR("AbsoluteName") = F.AbsoluteName
            DR("FileType") = F.FileType
            DR("FileIndex") = F.FileIndex
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Private Sub RemoveFile(ByVal FileName As String)
        For i As Integer = 1 To 100
            If Not Session("File_" & PageUniqueID & "_" & i) Then
                Dim F As GenericLib.FileStructure = Session("File_" & PageUniqueID & "_" & i)
                If F.AbsoluteName = FileName Then
                    Session("File_" & PageUniqueID & "_" & i) = Nothing
                End If
            End If
        Next
        DisplayFile()
    End Sub

    Private Sub RemoveFile(ByVal FileIndex As Integer)
        Session("File_" & PageUniqueID & "_" & FileIndex) = Nothing
        DisplayFile()
    End Sub


#Region "Sung"
    'Add Line
    Protected Sub btnAddLine_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddLine.Click
        If txt_Total_Line.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Please enter total lines.');", True)
            Exit Sub
        End If
        Dim DT As DataTable = CurrentDetail()
        Dim TotalLine As Integer = 0
        If DT.Rows.Count > CInt(txt_Total_Line.Text) Then
            For i As Int32 = 1 To DT.Rows.Count - CInt(txt_Total_Line.Text)
                DT.Rows.RemoveAt(DT.Rows.Count - 1)
            Next
        Else
            TotalLine = CInt(txt_Total_Line.Text) - DT.Rows.Count
            Dim NewID As Object = DT.Compute("MAX(JDA1_Detail_ID)", "")
            If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
            For i As Int32 = 0 To TotalLine - 1
                Dim DR As DataRow = DT.NewRow
                DR("JDA1_Detail_ID") = NewID + i
                DT.Rows.Add(DR)
            Next
        End If

        GridDatasource = DT
    End Sub

    'Invoice
    Private Sub DisplayINV()
        Dim DT As New DataTable
        DT = CurrentDetail()
        DT.DefaultView.RowFilter = ""

        Dim col() As String = {"JDA1_Invoice"}
        Dim INV As DataTable = DT.DefaultView.ToTable(True, col)
        INV.DefaultView.RowFilter = "JDA1_Invoice <> ''"
        INV = INV.DefaultView.ToTable
        rptINV.DataSource = INV
        rptINV.DataBind()
    End Sub

    Private Sub BindINV()
        Dim SQL As String = ""
        SQL &= "SELECT JDA1_Invoice FROM tb_JDA_1_Invoice WHERE JDA1_ID = " & JDA_ID & vbNewLine
        SQL &= "ORDER BY JDA1_Row" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptINV.DataSource = DT
        rptINV.DataBind()
    End Sub

    Private Sub AddINV(ByVal INV As String)
        Dim DT As DataTable = CurrentINV()
        Dim DR As DataRow
        DR = DT.NewRow
        DR("JDA1_Invoice") = INV
        DT.Rows.Add(DR)
        rptINV.DataSource = DT
        rptINV.DataBind()
    End Sub

    Private Function CurrentINV() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("JDA1_Invoice")
        If rptINV.Items.Count > 0 Then
            For Each ri As RepeaterItem In rptINV.Items
                If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For

                Dim INVlink As HtmlAnchor = ri.FindControl("INVlink")
                Dim DR As DataRow = DT.NewRow
                DR("JDA1_Invoice") = INVlink.InnerHtml.ToString
                DT.Rows.Add(DR)
            Next
        End If
        Return DT
    End Function
    Protected Sub btnAddInv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddInv.Click
        Dim DT As DataTable = CurrentINV()
        Dim INV As String = ""
        For i As Int32 = 0 To DT.DefaultView.Count - 1
            INV = INV & "'" & DT.Rows(i).Item("JDA1_Invoice").ToString & "',"
        Next
        If INV <> "" Then
            INV = INV.Substring(0, INV.Length - 1)
        End If
        Session("Inv") = INV
        Dim Script As String = "requireTextboxDialog('Inv_JDA_Finding.aspx',445,540,'" & txtTmpInv.ClientID & "','" & btnDialog.ClientID & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
    End Sub

    Protected Sub rptINV_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptINV.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim INVlink As HtmlAnchor = e.Item.FindControl("INVlink")
                Dim btnINVDelete As ImageButton = e.Item.FindControl("btnINVDelete")

                '------ INV ------
                Dim DT As New DataTable
                DT = CurrentINV()
                DT.DefaultView.RowFilter = "JDA1_Invoice <> '" & INVlink.InnerHtml.Replace("'", "''") & "'"
                DT = DT.DefaultView.ToTable
                rptINV.DataSource = DT
                rptINV.DataBind()
                '------ Item -------
                DT = New DataTable
                DT = CurrentDetail()
                DT.DefaultView.RowFilter = "JDA1_Invoice <> '" & INVlink.InnerHtml.Replace("'", "''") & "'"
                DT = DT.DefaultView.ToTable
                GridDatasource = DT

        End Select
    End Sub

    Protected Sub rptINV_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptINV.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item And e.Item.ItemType <> ListItemType.AlternatingItem Then Exit Sub

        Dim liINV As HtmlGenericControl = e.Item.FindControl("liINV")
        Dim INVlink As HtmlAnchor = e.Item.FindControl("INVlink")
        Dim btnINVDelete As ImageButton = e.Item.FindControl("btnINVDelete")
        liINV.Attributes("class") = "file_list invoice"
        INVlink.InnerHtml = e.Item.DataItem("JDA1_Invoice")

        Dim SQL As String = "SELECT * FROM tb_JDA_1 WHERE JDA1_INV_NO='" & e.Item.DataItem("JDA1_Invoice").ToString.Replace("'", "''") & "'"
        Dim Header As New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(Header)
        If Header.Rows.Count > 0 Then
            SQL = "SELECT * FROM tb_JDA_1_Detail  WHERE JDA1_ID = (SELECT Top 1 JDA1_ID FROM tb_JDA_1 WHERE JDA1_INV_NO='" & e.Item.DataItem("JDA1_Invoice").ToString.Replace("'", "''") & "')" & vbCrLf
            SQL &= "ORDER BY JDA1_Detail_ID"
            Dim Detail As New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(Detail)
            Dim RS As New ReportSourceTransformer
            Dim ReportSource As DataTable = RS.TransformJDA1(Header, Detail)
            Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
            Session(UNIQUE_ID) = ReportSource
            Dim Script As String = "requireTextboxDialog('Print/ReportJDA_1.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,500,'','')"
            INVlink.Attributes("onclick") = Script
        Else
            SQL = "SELECT * FROM tb_JDA_3 WHERE JDA3_INV_NO='" & e.Item.DataItem("JDA1_Invoice").ToString.Replace("'", "''") & "'"
            Header = New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(Header)
            SQL = "SELECT * FROM tb_JDA_3_Detail  WHERE JDA3_ID = (SELECT Top 1 JDA3_ID FROM tb_JDA_3 WHERE JDA3_INV_NO='" & e.Item.DataItem("JDA1_Invoice").ToString.Replace("'", "''") & "')" & vbCrLf
            SQL &= "ORDER BY JDA3_Detail_ID"
            Dim Detail As New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(Detail)
            Dim RS As New ReportSourceTransformer
            Dim ReportSource As DataTable = RS.TransformJDA3(Header, Detail)
            Dim UNIQUE_ID As String = Now.ToOADate.ToString.Replace(".", "")
            Session(UNIQUE_ID) = ReportSource
            Dim Script As String = "requireTextboxDialog('Print/ReportJDA_3.aspx?Mode=PDF&S=" & UNIQUE_ID & "&',900,500,'','')"
            INVlink.Attributes("onclick") = Script
        End If

    End Sub

    Protected Sub btnDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDialog.Click
        Dim DT As New DataTable
        DT = CurrentDetail()

        Dim SQL As String = ""
        SQL &= "SELECT DT.*,'" & txtTmpInv.Text.Trim.Replace("'", "''") & "' as INV" & vbNewLine
        SQL &= "FROM tb_JDA_1 HD LEFT JOIN tb_JDA_1_Detail DT ON HD.JDA1_ID = DT.JDA1_ID" & vbNewLine
        SQL &= "WHERE RTRIM(LTRIM(HD.JDA1_INV_NO)) = '" & txtTmpInv.Text.Trim.Replace("'", "''") & "'" & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_INV As New DataTable
        DA.Fill(DT_INV)
        If DT_INV.Rows.Count > 0 Then
            For i As Int32 = 0 To DT_INV.Rows.Count - 1
                'DT.DefaultView.RowFilter = "JDA1_33Description_Goods = '" & DT_INV.Rows(i).Item("JDA1_33Description_Goods").ToString.Replace("'", "''") & "'"
                'If DT.DefaultView.Count = 0 Then
                Dim DR As DataRow
                DR = DT.NewRow

                Dim NewID As Object = DT.Compute("MAX(JDA1_Detail_ID)", "")
                If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
                DR("JDA1_31Item_No") = DBNull.Value
                DR("JDA1_Detail_ID") = NewID + i
                DR("JDA1_33Description_Goods") = DT_INV.Rows(i).Item("JDA1_33Description_Goods")
                DR("JDA1_34CodeNo") = DT_INV.Rows(i).Item("JDA1_34CodeNo")
                DR("JDA1_35Unit") = DT_INV.Rows(i).Item("JDA1_35Unit")
                DR("JDA1_36CountryCode") = DT_INV.Rows(i).Item("JDA1_36CountryCode")
                DR("JDA1_37Qtybased") = DBNull.Value
                DR("JDA1_38PerUnit") = DBNull.Value
                DR("JDA1_39Total") = DBNull.Value
                DR("JDA1_40Rate") = DBNull.Value
                DR("JDA1_41Amount") = DBNull.Value
                DR("DA1_42Type") = DBNull.Value
                DR("JDA1_43Rate") = DBNull.Value
                DR("JDA1_44Amount") = DBNull.Value
                DR("JDA1_39Total_THB") = DBNull.Value
                DR("JDA1_Invoice") = txtTmpInv.Text
                DT.Rows.Add(DR)
                'End If
                'DT.DefaultView.RowFilter = ""
            Next
            GridDatasource = DT
            AddINV(txtTmpInv.Text)
        Else
            SQL = ""
            SQL &= "SELECT DT.*,'" & txtTmpInv.Text.Trim.Replace("'", "''") & "' as INV" & vbNewLine
            SQL &= "FROM tb_JDA_3 HD LEFT JOIN tb_JDA_3_Detail DT ON HD.JDA3_ID = DT.JDA3_ID" & vbNewLine
            SQL &= "WHERE RTRIM(LTRIM(HD.JDA3_INV_NO)) = '" & txtTmpInv.Text.Trim.Replace("'", "''") & "'" & vbNewLine
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT_INV = New DataTable
            DA.Fill(DT_INV)

            For i As Int32 = 0 To DT_INV.Rows.Count - 1
                'DT.DefaultView.RowFilter = "JDA1_33Description_Goods = '" & DT_INV.Rows(i).Item("JDA1_33Description_Goods").ToString.Replace("'", "''") & "'"
                'If DT.DefaultView.Count = 0 Then
                Dim DR As DataRow
                DR = DT.NewRow

                Dim NewID As Object = DT.Compute("MAX(JDA1_Detail_ID)", "")
                If IsDBNull(NewID) Then NewID = 1 Else NewID += 1
                DR("JDA1_31Item_No") = DBNull.Value
                DR("JDA1_Detail_ID") = NewID + i
                DR("JDA1_33Description_Goods") = DT_INV.Rows(i).Item("JDA3_33Description_Goods")
                DR("JDA1_34CodeNo") = DT_INV.Rows(i).Item("JDA3_34CodeNo")
                DR("JDA1_35Unit") = DT_INV.Rows(i).Item("JDA3_35Unit")
                DR("JDA1_36CountryCode") = DT_INV.Rows(i).Item("JDA3_36CountryCode")
                DR("JDA1_37Qtybased") = DBNull.Value
                DR("JDA1_38PerUnit") = DBNull.Value
                DR("JDA1_39Total") = DBNull.Value
                DR("JDA1_40Rate") = DBNull.Value
                DR("JDA1_41Amount") = DBNull.Value
                DR("DA1_42Type") = DBNull.Value
                DR("JDA1_43Rate") = DBNull.Value
                DR("JDA1_44Amount") = DBNull.Value
                DR("JDA1_39Total_THB") = DBNull.Value
                DR("JDA1_Invoice") = txtTmpInv.Text
                DT.Rows.Add(DR)
                'End If
                'DT.DefaultView.RowFilter = ""
            Next
            GridDatasource = DT
            AddINV(txtTmpInv.Text)
        End If
    End Sub
#End Region

    
End Class

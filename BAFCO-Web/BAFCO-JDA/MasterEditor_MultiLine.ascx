﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MasterEditor_MultiLine.ascx.vb" Inherits="MasterEditor_MultiLine" %>

       <div style="position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1000;" >
       
       
                <div id="Mask" runat="server" style="width:100%; z-index:400; height:100%; z-index:300; position:absolute; background-color:black; filter:alpha(opacity=80); opacity: 0.80;" Text="" ></div>
                 <div style="left:20%; top:20%; z-index:500; position:fixed; background-color:#F6F3F3; padding: 10px 15px 10px 15px;" >
                           <h3><asp:Label ID="lblMasterName" runat="Server" ></asp:Label></h3>
                            <asp:Label ID="lblTableName" runat="Server" style="display:none;" ></asp:Label>
                            <asp:Label ID="lblTranscationField" runat="Server" style="display:none;" ></asp:Label>
                             <table class="Master_Table" cellpadding="10">
                                <tr>
                                    <td align="center" style="background-color:#cccccc; width:400px;">                    
                                        Name
                                    </td>
                                    <td align="center" style="background-color:#cccccc; min-width:130px; display:none;" id="codeHeader" runat="server">                   
                                        Code
                                    </td>
                                    <td align="center" style="background-color:#cccccc; ">                   
                                        Default
                                    </td>
                                     <td align="center" style="background-color:#cccccc; min-width:100px;">                  
                                        Action
                                    </td>
                                </tr>
                                <asp:Repeater ID="rptMaster" runat="server">
                                    <ItemTemplate>
                                        <tr id="trView" runat="server" >
                                            <td style="min-height:35px; width:400px; border-bottom:1px solid #ccc;" >
                                                <asp:Label ID="lblID" runat="server" style="display:none;" CommandName="Selected"></asp:Label>
                                                <asp:LinkButton ID="lblName" runat="server" CommandName="Selected" style="text-decoration:none; color:gray;"></asp:LinkButton>
                                            </td>
                                            <td style="min-height:35px; min-Width:150px; overflow:hidden; border-bottom:1px solid #ccc; display:none; vertical-align:top;" id="codeDetail1" runat="server" >
                                                <asp:LinkButton ID="lblCode" runat="server" style="text-decoration:none; color:gray;" CommandName="Selected"></asp:LinkButton>
                                            </td >
                                            <td align="center" style="min-height:35px; border-bottom:1px solid #ccc;" valign="top" >
                                                <asp:ImageButton ID="btnDefault" runat="server" CommandName="Default" ToolTip="" ImageUrl="images/arrow_blue_left.png" Width="20px" />
                                            </td>   
                                            <td style="width:130px; text-align:left; min-height:35px; border-bottom:1px solid #ccc; vertical-align:top;">
                                                <asp:Button ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Edit" Text="Change" CssClass="Button_Disable" ForeColor="White" style="cursor:pointer; width:60px;" />
                                                <asp:Button ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" Text="Delete" CssClass="Button_Red" style="width:60px;"/>                                      
                                            </td>                                   
                                        </tr>
                                        
                                        <tr id="trEdit" runat="server">
                                            <td style="width:400px;">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="Master_Textbox" TextMode="MultiLine" Height="100px" PlaceHolder="Name" Width="400px"></asp:TextBox>
                                            </td>
                                            <td id="codeDetail2" runat="server" style="display:none;" valign="top">
                                                <asp:TextBox ID="txtCode" runat="server" CssClass="Master_Textbox" PlaceHolder="Code" Width="150px"></asp:TextBox>
                                            </td>
                                            <td align="center">
                                                &nbsp;
                                            </td>   
                                            <td align="center" style="width:125px; text-align:left; min-height:35px; vertical-align:top;">
                                                <asp:Button ID="btnSave" CommandName="Save" runat="server" ToolTip="Close" Text="Save" CssClass="Button_Red" style="width:60px;"/>
                                                <asp:Button ID="btnCancel" CommandName="Cancel" runat="server" Text="Cancel" CssClass="Button_Disable" ToolTip="Cancel" ForeColor="White" style="cursor:pointer;width:60px;"/>
                                            </td>                                   
                                        </tr>
                                        
                                        
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                           <div style="width:100%; text-align:right;">
                            <asp:Button ID="btnAdd" runat="server" ToolTip="Save" Text="Add" CssClass="Button_Red" style="margin-top:10px;" />
                            <asp:Button ID="btnClose" runat="server" ToolTip="Save" Text="Close" CssClass="Button_White" />
                           </div>
                            
                           
                    </div>     
        </div>
        
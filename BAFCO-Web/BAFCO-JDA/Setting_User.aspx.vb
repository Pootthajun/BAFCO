﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Setting_User
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("USER_ID")) Then Exit Sub

        '---------------- Check Permission-------------
        If IsNothing(Session("IS_ADMIN")) OrElse Not Session("IS_ADMIN") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        End If

        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Public Sub BindData()
        Dim SQL As String = "select * from MS_USER WHERE USER_ID<>-1 ORDER BY USER_CODE"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Columns.Add("edit_mode", GetType(String), "'Edit'")
        DT.Columns.Add("view_mode", GetType(String), "'View'")

        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblFullname As Label = e.Item.FindControl("lblFullname")
        Dim lblPassword As Label = e.Item.FindControl("lblPassword")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim txtCode As TextBox = e.Item.FindControl("txtCode")
        Dim txtFullname As TextBox = e.Item.FindControl("txtFullname")
        Dim txtPassword As TextBox = e.Item.FindControl("txtPassword")

        Dim btnSave As ImageButton = e.Item.FindControl("btnSave")
        Dim btnCancel As ImageButton = e.Item.FindControl("btnCancel")

        Dim btnSetting_Yes_1 As ImageButton = e.Item.FindControl("btnSetting_Yes_1")
        Dim btnSetting_No_1 As ImageButton = e.Item.FindControl("btnSetting_No_1")
        Dim btnJDA1_Yes_1 As ImageButton = e.Item.FindControl("btnJDA1_Yes_1")
        Dim btnJDA1_No_1 As ImageButton = e.Item.FindControl("btnJDA1_No_1")
        Dim btnJDA2_Yes_1 As ImageButton = e.Item.FindControl("btnJDA2_Yes_1")
        Dim btnJDA2_No_1 As ImageButton = e.Item.FindControl("btnJDA2_No_1")
        Dim btnJDA3_Yes_1 As ImageButton = e.Item.FindControl("btnJDA3_Yes_1")
        Dim btnJDA3_No_1 As ImageButton = e.Item.FindControl("btnJDA3_No_1")
        Dim btnJDA4_Yes_1 As ImageButton = e.Item.FindControl("btnJDA4_Yes_1")
        Dim btnJDA4_No_1 As ImageButton = e.Item.FindControl("btnJDA4_No_1")
        Dim btnStatus_Yes_1 As ImageButton = e.Item.FindControl("btnStatus_Yes_1")
        Dim btnStatus_No_1 As ImageButton = e.Item.FindControl("btnStatus_No_1")

        Dim btnSetting_Yes_2 As ImageButton = e.Item.FindControl("btnSetting_Yes_2")
        Dim btnSetting_No_2 As ImageButton = e.Item.FindControl("btnSetting_No_2")
        Dim btnJDA1_Yes_2 As ImageButton = e.Item.FindControl("btnJDA1_Yes_2")
        Dim btnJDA1_No_2 As ImageButton = e.Item.FindControl("btnJDA1_No_2")
        Dim btnJDA2_Yes_2 As ImageButton = e.Item.FindControl("btnJDA2_Yes_2")
        Dim btnJDA2_No_2 As ImageButton = e.Item.FindControl("btnJDA2_No_2")
        Dim btnJDA3_Yes_2 As ImageButton = e.Item.FindControl("btnJDA3_Yes_2")
        Dim btnJDA3_No_2 As ImageButton = e.Item.FindControl("btnJDA3_No_2")
        Dim btnJDA4_Yes_2 As ImageButton = e.Item.FindControl("btnJDA4_Yes_2")
        Dim btnJDA4_No_2 As ImageButton = e.Item.FindControl("btnJDA4_No_2")
        Dim btnStatus_Yes_2 As ImageButton = e.Item.FindControl("btnStatus_Yes_2")
        Dim btnStatus_No_2 As ImageButton = e.Item.FindControl("btnStatus_No_2")

        Dim trView As HtmlTableRow = e.Item.FindControl("trView")
        Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")

        Dim USER_ID As Integer = lblId.Text
        Select Case e.CommandName
            '------------------------ Common Command---------------
            Case "Edit"

                trView.Visible = False
                trEdit.Visible = True
                txtCode.Focus()

            Case "Delete"
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "DELETE FROM MS_USER WHERE USER_ID=" & USER_ID
                    .ExecuteNonQuery()
                End With
                Conn.Close()
                Dim DT As DataTable = CurrentDatasource()
                DT.Rows.RemoveAt(e.Item.ItemIndex)
                rptData.DataSource = DT
                rptData.DataBind()
            Case "Save"

                If txtCode.Text = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert User Code/Login'); document.getElementById('" & txtCode.ClientID & "').focus();", True)
                    Exit Sub
                End If

                If txtFullname.Text = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Fullname'); document.getElementById('" & txtFullname.ClientID & "').focus();", True)
                    Exit Sub
                End If

                '------------------ Check Duplicated--------------------
                Dim SQL As String = "SELECT * FROM MS_USER WHERE  USER_CODE='" & txtCode.Text.Replace("'", "''") & "' AND USER_ID<>" & USER_ID
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This User Code/Login is already exists'); document.getElementById('" & txtCode.ClientID & "').focus();", True)
                    Exit Sub
                End If

                SQL = "SELECT * FROM MS_USER WHERE  USER_FULLNAME='" & txtFullname.Text.Replace("'", "''") & "' AND USER_ID<>" & USER_ID
                DA = New SqlDataAdapter(SQL, ConnStr)
                DT = New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This user is already exists'); document.getElementById('" & txtFullname.ClientID & "').focus();", True)
                    Exit Sub
                End If

                '---------------- Save ------------------
                SQL = "SELECT * FROM MS_USER WHERE USER_ID=" & USER_ID
                DA = New SqlDataAdapter(SQL, ConnStr)
                DT = New DataTable
                DA.Fill(DT)

                Dim DR As DataRow
                If DT.Rows.Count = 0 Or USER_ID = 0 Then
                    DR = DT.NewRow
                    DR("USER_ID") = GetNewID()
                    DT.Rows.Add(DR)
                Else
                    DR = DT.Rows(0)
                End If

                DR("USER_CODE") = txtCode.Text
                DR("USER_FULLNAME") = txtFullname.Text
                DR("USER_PASSWORD") = txtPassword.Text

                DR("IS_ADMIN") = btnSetting_Yes_2.Visible
                DR("IS_JDA_1") = btnJDA1_Yes_2.Visible
                DR("IS_JDA_2") = btnJDA2_Yes_2.Visible
                DR("IS_JDA_3") = btnJDA3_Yes_2.Visible
                DR("IS_JDA_4") = btnJDA4_Yes_2.Visible
                DR("ACTIVE_STATUS") = btnStatus_Yes_2.Visible
                DR("UPDATE_BY") = Session("USER_ID")
                DR("UPDATE_DATE") = Now

                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)
                '---------------------- Update Data----------------
                lblId.Text = DR("USER_ID")
                lblNo.Text = e.Item.ItemIndex + 1
                lblCode.Text = DR("USER_CODE")
                lblFullname.Text = DR("USER_FULLNAME")
                lblPassword.Text = DR("USER_PASSWORD")

                txtCode.Text = DR("USER_CODE")
                txtFullname.Text = DR("USER_FULLNAME")
                txtPassword.Text = DR("USER_PASSWORD")

                btnSetting_Yes_1.Visible = btnSetting_Yes_2.Visible
                btnSetting_No_1.Visible = btnSetting_No_2.Visible
                btnJDA1_Yes_1.Visible = btnJDA1_Yes_2.Visible
                btnJDA1_No_1.Visible = btnJDA1_No_2.Visible
                btnJDA2_Yes_1.Visible = btnJDA2_Yes_2.Visible
                btnJDA2_No_1.Visible = btnJDA2_No_2.Visible
                btnJDA3_Yes_1.Visible = btnJDA3_Yes_2.Visible
                btnJDA3_No_1.Visible = btnJDA3_No_2.Visible
                btnJDA4_Yes_1.Visible = btnJDA4_Yes_2.Visible
                btnJDA4_No_1.Visible = btnJDA4_No_2.Visible
                btnStatus_Yes_1.Visible = btnStatus_Yes_2.Visible
                btnStatus_No_1.Visible = btnStatus_No_2.Visible

                trEdit.Visible = False
                trView.Visible = True
                btnEdit.Attributes("Mode") = "Edit"

            Case "Cancel"
                Select Case btnEdit.Attributes("Mode")
                    Case "Edit"
                        '-------------- Reset Value-----------
                        txtCode.Text = lblCode.Text
                        txtFullname.Text = lblFullname.Text
                        txtPassword.Text = lblPassword.Text

                        trView.Visible = True
                        trEdit.Visible = False
                    Case "Add"
                        Dim DT As DataTable = CurrentDatasource()
                        DT.Rows(e.Item.ItemIndex).Delete()
                        DT.AcceptChanges()
                        rptData.DataSource = DT
                        rptData.DataBind()
                End Select
                '------------------------- Permission -----------------------------
            Case "JDA1"
                '------------Save If Already Created Profile---------------
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "UPDATE MS_USER SET IS_JDA_1=" & CUInt(CBool(e.CommandArgument)) & " WHERE USER_ID=" & USER_ID
                    .ExecuteNonQuery()
                End With
                Conn.Close()
                '-------------Update Icon---------
                btnJDA1_Yes_1.Visible = CBool(e.CommandArgument)
                btnJDA1_No_1.Visible = Not CBool(e.CommandArgument)
                btnJDA1_Yes_2.Visible = CBool(e.CommandArgument)
                btnJDA1_No_2.Visible = Not CBool(e.CommandArgument)
                
            Case "JDA2"
                '------------Save If Already Created Profile---------------
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "UPDATE MS_USER SET IS_JDA_2=" & CUInt(CBool(e.CommandArgument)) & " WHERE USER_ID=" & USER_ID
                    .ExecuteNonQuery()
                End With
                Conn.Close()
                '-------------Update Icon---------
                btnJDA2_Yes_1.Visible = CBool(e.CommandArgument)
                btnJDA2_No_1.Visible = Not CBool(e.CommandArgument)
                btnJDA2_Yes_2.Visible = CBool(e.CommandArgument)
                btnJDA2_No_2.Visible = Not CBool(e.CommandArgument)

            Case "JDA3"
                '------------Save If Already Created Profile---------------
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "UPDATE MS_USER SET IS_JDA_3=" & CUInt(CBool(e.CommandArgument)) & " WHERE USER_ID=" & USER_ID
                    .ExecuteNonQuery()
                End With
                Conn.Close()
                '-------------Update Icon---------
                btnJDA3_Yes_1.Visible = CBool(e.CommandArgument)
                btnJDA3_No_1.Visible = Not CBool(e.CommandArgument)
                btnJDA3_Yes_2.Visible = CBool(e.CommandArgument)
                btnJDA3_No_2.Visible = Not CBool(e.CommandArgument)

            Case "JDA4"
                '------------Save If Already Created Profile---------------
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "UPDATE MS_USER SET IS_JDA_4=" & CUInt(CBool(e.CommandArgument)) & " WHERE USER_ID=" & USER_ID
                    .ExecuteNonQuery()
                End With
                Conn.Close()
                '-------------Update Icon---------
                btnJDA4_Yes_1.Visible = CBool(e.CommandArgument)
                btnJDA4_No_1.Visible = Not CBool(e.CommandArgument)
                btnJDA4_Yes_2.Visible = CBool(e.CommandArgument)
                btnJDA4_No_2.Visible = Not CBool(e.CommandArgument)

            Case "Setting"
                '------------Save If Already Created Profile---------------
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "UPDATE MS_USER SET IS_ADMIN=" & CUInt(CBool(e.CommandArgument)) & " WHERE USER_ID=" & USER_ID
                    .ExecuteNonQuery()
                End With
                Conn.Close()
                '-------------Update Icon---------
                btnSetting_Yes_1.Visible = CBool(e.CommandArgument)
                btnSetting_No_1.Visible = Not CBool(e.CommandArgument)
                btnSetting_Yes_2.Visible = CBool(e.CommandArgument)
                btnSetting_No_2.Visible = Not CBool(e.CommandArgument)

            Case "Status"
                '------------Save If Already Created Profile---------------
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .CommandType = CommandType.Text
                    .Connection = Conn
                    .CommandText = "UPDATE MS_USER SET ACTIVE_STATUS=" & CUInt(CBool(e.CommandArgument)) & " WHERE USER_ID=" & USER_ID
                    .ExecuteNonQuery()
                End With
                Conn.Close()
                '-------------Update Icon---------
                btnStatus_Yes_1.Visible = CBool(e.CommandArgument)
                btnStatus_No_1.Visible = Not CBool(e.CommandArgument)
                btnStatus_Yes_2.Visible = CBool(e.CommandArgument)
                btnStatus_No_2.Visible = Not CBool(e.CommandArgument)
        End Select

    End Sub

    
    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblFullname As Label = e.Item.FindControl("lblFullname")
        Dim lblPassword As Label = e.Item.FindControl("lblPassword")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim txtCode As TextBox = e.Item.FindControl("txtCode")
        Dim txtFullname As TextBox = e.Item.FindControl("txtFullname")
        Dim txtPassword As TextBox = e.Item.FindControl("txtPassword")

        Dim btnSave As ImageButton = e.Item.FindControl("btnSave")
        Dim btnCancel As ImageButton = e.Item.FindControl("btnCancel")

        Dim btnSetting_Yes_1 As ImageButton = e.Item.FindControl("btnSetting_Yes_1")
        Dim btnSetting_No_1 As ImageButton = e.Item.FindControl("btnSetting_No_1")
        Dim btnJDA1_Yes_1 As ImageButton = e.Item.FindControl("btnJDA1_Yes_1")
        Dim btnJDA1_No_1 As ImageButton = e.Item.FindControl("btnJDA1_No_1")
        Dim btnJDA2_Yes_1 As ImageButton = e.Item.FindControl("btnJDA2_Yes_1")
        Dim btnJDA2_No_1 As ImageButton = e.Item.FindControl("btnJDA2_No_1")
        Dim btnJDA3_Yes_1 As ImageButton = e.Item.FindControl("btnJDA3_Yes_1")
        Dim btnJDA3_No_1 As ImageButton = e.Item.FindControl("btnJDA3_No_1")
        Dim btnJDA4_Yes_1 As ImageButton = e.Item.FindControl("btnJDA4_Yes_1")
        Dim btnJDA4_No_1 As ImageButton = e.Item.FindControl("btnJDA4_No_1")
        Dim btnStatus_Yes_1 As ImageButton = e.Item.FindControl("btnStatus_Yes_1")
        Dim btnStatus_No_1 As ImageButton = e.Item.FindControl("btnStatus_No_1")

        Dim btnSetting_Yes_2 As ImageButton = e.Item.FindControl("btnSetting_Yes_2")
        Dim btnSetting_No_2 As ImageButton = e.Item.FindControl("btnSetting_No_2")
        Dim btnJDA1_Yes_2 As ImageButton = e.Item.FindControl("btnJDA1_Yes_2")
        Dim btnJDA1_No_2 As ImageButton = e.Item.FindControl("btnJDA1_No_2")
        Dim btnJDA2_Yes_2 As ImageButton = e.Item.FindControl("btnJDA2_Yes_2")
        Dim btnJDA2_No_2 As ImageButton = e.Item.FindControl("btnJDA2_No_2")
        Dim btnJDA3_Yes_2 As ImageButton = e.Item.FindControl("btnJDA3_Yes_2")
        Dim btnJDA3_No_2 As ImageButton = e.Item.FindControl("btnJDA3_No_2")
        Dim btnJDA4_Yes_2 As ImageButton = e.Item.FindControl("btnJDA4_Yes_2")
        Dim btnJDA4_No_2 As ImageButton = e.Item.FindControl("btnJDA4_No_2")
        Dim btnStatus_Yes_2 As ImageButton = e.Item.FindControl("btnStatus_Yes_2")
        Dim btnStatus_No_2 As ImageButton = e.Item.FindControl("btnStatus_No_2")

        lblNo.Text = e.Item.ItemIndex + 1
        lblId.Text = e.Item.DataItem("USER_ID")
        lblCode.Text = e.Item.DataItem("USER_CODE")
        lblFullname.Text = e.Item.DataItem("USER_FULLNAME")
        lblPassword.Text = e.Item.DataItem("USER_PASSWORD")

        txtCode.Text = e.Item.DataItem("USER_CODE")
        txtFullname.Text = e.Item.DataItem("USER_FULLNAME")
        txtPassword.Text = e.Item.DataItem("USER_PASSWORD")
        '----------------- Permission ----------------------
        btnSetting_Yes_1.Visible = Not IsDBNull(e.Item.DataItem("IS_ADMIN")) AndAlso e.Item.DataItem("IS_ADMIN")
        btnSetting_No_1.Visible = Not btnSetting_Yes_1.Visible
        btnJDA1_Yes_1.Visible = Not IsDBNull(e.Item.DataItem("IS_JDA_1")) AndAlso e.Item.DataItem("IS_JDA_1")
        btnJDA1_No_1.Visible = Not btnJDA1_Yes_1.Visible
        btnJDA2_Yes_1.Visible = Not IsDBNull(e.Item.DataItem("IS_JDA_2")) AndAlso e.Item.DataItem("IS_JDA_2")
        btnJDA2_No_1.Visible = Not btnJDA2_Yes_1.Visible
        btnJDA3_Yes_1.Visible = Not IsDBNull(e.Item.DataItem("IS_JDA_3")) AndAlso e.Item.DataItem("IS_JDA_3")
        btnJDA3_No_1.Visible = Not btnJDA3_Yes_1.Visible
        btnJDA4_Yes_1.Visible = Not IsDBNull(e.Item.DataItem("IS_JDA_4")) AndAlso e.Item.DataItem("IS_JDA_4")
        btnJDA4_No_1.Visible = Not btnJDA4_Yes_1.Visible
        btnStatus_Yes_1.Visible = Not IsDBNull(e.Item.DataItem("ACTIVE_STATUS")) AndAlso e.Item.DataItem("ACTIVE_STATUS")
        btnStatus_No_1.Visible = Not btnStatus_Yes_1.Visible
        '----------------- Same As Above---------------
        btnSetting_Yes_2.Visible = btnSetting_Yes_1.Visible
        btnSetting_No_2.Visible = btnSetting_No_1.Visible
        btnJDA1_Yes_2.Visible = btnJDA1_Yes_1.Visible
        btnJDA1_No_2.Visible = btnJDA1_No_1.Visible
        btnJDA2_Yes_2.Visible = btnJDA2_Yes_1.Visible
        btnJDA2_No_2.Visible = btnJDA2_No_1.Visible
        btnJDA3_Yes_2.Visible = btnJDA3_Yes_1.Visible
        btnJDA3_No_2.Visible = btnJDA3_No_1.Visible
        btnJDA4_Yes_2.Visible = btnJDA4_Yes_1.Visible
        btnJDA4_No_2.Visible = btnJDA4_No_1.Visible
        btnStatus_Yes_2.Visible = btnStatus_Yes_1.Visible
        btnStatus_No_2.Visible = btnStatus_No_1.Visible
        '--------------- เปิด ปิด ฟังก์ชั่น Edit -----------
        Dim trView As HtmlTableRow = e.Item.FindControl("trView")
        Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
        trView.Visible = e.Item.DataItem("view_mode") = "View"
        trEdit.Visible = e.Item.DataItem("view_mode") = "Edit"

        btnEdit.Attributes("Mode") = e.Item.DataItem("edit_mode")

    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim DT As DataTable = CurrentDatasource()
        Dim DR As DataRow = DT.NewRow
        DR("USER_ID") = 0
        DR("USER_CODE") = ""
        DR("USER_FULLNAME") = ""
        DR("USER_PASSWORD") = ""
        DR("IS_JDA_1") = False
        DR("IS_JDA_2") = False
        DR("IS_JDA_3") = False
        DR("IS_JDA_4") = False
        DR("ACTIVE_STATUS") = True
        DR("edit_mode") = "Add"
        DR("view_mode") = "Edit"
        DT.Rows.Add(DR)
        rptData.DataSource = DT
        rptData.DataBind()

        Dim Item As RepeaterItem = rptData.Items(rptData.Items.Count - 1)
        Dim txtCode As TextBox = Item.FindControl("txtCode")
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Focus", "document.getElementById('" & txtCode.ClientID & "').focus();", True)
    End Sub

    Private Function CurrentDatasource() As DataTable
        Dim DT As New DataTable

        DT.Columns.Add("USER_ID", GetType(Integer))
        DT.Columns.Add("USER_CODE")
        DT.Columns.Add("USER_FULLNAME")
        DT.Columns.Add("USER_PASSWORD")
        DT.Columns.Add("IS_ADMIN", GetType(Boolean))
        DT.Columns.Add("IS_JDA_1", GetType(Boolean))
        DT.Columns.Add("IS_JDA_2", GetType(Boolean))
        DT.Columns.Add("IS_JDA_3", GetType(Boolean))
        DT.Columns.Add("IS_JDA_4", GetType(Boolean))
        DT.Columns.Add("ACTIVE_STATUS", GetType(Boolean))
        DT.Columns.Add("edit_mode")
        DT.Columns.Add("view_mode")

        For Each Item As RepeaterItem In rptData.Items
            If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For

            Dim lblId As Label = Item.FindControl("lblId")
            Dim lblCode As Label = Item.FindControl("lblCode")
            Dim lblFullname As Label = Item.FindControl("lblFullname")
            Dim lblPassword As Label = Item.FindControl("lblPassword")

            Dim btnEdit As ImageButton = Item.FindControl("btnEdit")
            Dim btnDelete As ImageButton = Item.FindControl("btnDelete")

            Dim txtCode As TextBox = Item.FindControl("txtCode")
            Dim txtFullname As TextBox = Item.FindControl("txtFullname")
            Dim txtPassword As TextBox = Item.FindControl("txtPassword")

            Dim btnSave As ImageButton = Item.FindControl("btnSave")
            Dim btnCancel As ImageButton = Item.FindControl("btnCancel")
            Dim btnSetting_Yes_1 As ImageButton = Item.FindControl("btnSetting_Yes_1")
            Dim btnSetting_Yes_2 As ImageButton = Item.FindControl("btnSetting_Yes_2")
            Dim btnJDA1_Yes_1 As ImageButton = Item.FindControl("btnJDA1_Yes_1")
            Dim btnJDA1_Yes_2 As ImageButton = Item.FindControl("btnJDA1_Yes_2")
            Dim btnJDA2_Yes_1 As ImageButton = Item.FindControl("btnJDA2_Yes_1")
            Dim btnJDA2_Yes_2 As ImageButton = Item.FindControl("btnJDA2_Yes_2")
            Dim btnJDA3_Yes_1 As ImageButton = Item.FindControl("btnJDA3_Yes_1")
            Dim btnJDA3_Yes_2 As ImageButton = Item.FindControl("btnJDA3_Yes_2")
            Dim btnJDA4_Yes_1 As ImageButton = Item.FindControl("btnJDA4_Yes_1")
            Dim btnJDA4_Yes_2 As ImageButton = Item.FindControl("btnJDA4_Yes_2")
            Dim btnStatus_Yes_1 As ImageButton = Item.FindControl("btnStatus_Yes_1")
            Dim btnStatus_Yes_2 As ImageButton = Item.FindControl("btnStatus_Yes_2")

            Dim trView As HtmlTableRow = Item.FindControl("trView")
            Dim trEdit As HtmlTableRow = Item.FindControl("trEdit")

            Dim DR As DataRow = DT.NewRow
            DR("USER_ID") = lblId.Text
            DR("USER_CODE") = lblCode.Text
            DR("USER_FULLNAME") = lblFullname.Text
            DR("USER_PASSWORD") = lblPassword.Text
            
            If trView.Visible Then
                DR("view_mode") = "View"
                DR("IS_ADMIN") = btnSetting_Yes_1.Visible
                DR("IS_JDA_1") = btnJDA1_Yes_1.Visible
                DR("IS_JDA_2") = btnJDA2_Yes_1.Visible
                DR("IS_JDA_3") = btnJDA3_Yes_1.Visible
                DR("IS_JDA_4") = btnJDA4_Yes_1.Visible
                DR("ACTIVE_STATUS") = btnStatus_Yes_1.Visible
            Else
                DR("view_mode") = "Edit"
                DR("IS_ADMIN") = btnSetting_Yes_2.Visible
                DR("IS_JDA_1") = btnJDA1_Yes_2.Visible
                DR("IS_JDA_2") = btnJDA2_Yes_2.Visible
                DR("IS_JDA_3") = btnJDA3_Yes_2.Visible
                DR("IS_JDA_4") = btnJDA4_Yes_2.Visible
                DR("ACTIVE_STATUS") = btnStatus_Yes_2.Visible
            End If
            DR("edit_mode") = btnEdit.Attributes("Mode")
            DT.Rows.Add(DR)
        Next

        Return DT
    End Function

    Private Function GetNewID() As Integer
        Dim SQL As String = "SELECT ISNULL(MAX(USER_ID),0)+1 FROM MS_USER"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Return DT.Rows(0)(0)
    End Function


End Class

﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Filter_JDA_2_4
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Dim GL As New GenericLib
    Dim CV As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        If Not IsPostBack Then
            ClearForm()
            BindData()
        End If

    End Sub

    Private Sub BindData()
        Dim SQL As String = ""

        Dim SQL2 As String = ""
        Dim SQL4 As String = ""
        'If ddlDocumentType.SelectedValue = 1 Then
        '------------JDA1----------------------
        SQL2 &= "  SELECT 'JDA 2'  DocumentType,2 JDA_TYPE,JDA_2.JDA2_ID JDA_ID,JDA2_Date_Import Date_Import,JDA2_Registration_Number Registration_Number,JDA2_INV_NO Invoice_No" & vbLf
        SQL2 &= "  ,COUNT(F.JDA2_ID) TotalFile" & vbLf
        SQL2 &= "  FROM tb_JDA_2 JDA_2" & vbLf
        SQL2 &= "  LEFT JOIN tb_JDA_2_File F ON JDA_2.JDA2_ID=F.JDA2_ID" & vbLf
        Dim Filter2 As String = ""
        If GL.IsProgrammingDate(txtDateOfImport.Text, "dd-MMM-yyyy") Then
            Dim TheDate As DateTime = CV.StringToDate(txtDateOfImport.Text, "dd-MMM-yyyy")
            Filter2 &= " DATEDIFF(dd,JDA_2.JDA2_Date_Import,'" & CV.DateToString(TheDate, "yyyy-MM-dd") & "')=0  AND"
        End If
        If txtRegistrationNumber.Text <> "" Then
            Filter2 &= " JDA_2.JDA2_Registration_Number Like '%" & txtRegistrationNumber.Text.Replace("'", "''") & "%' AND "
        End If
        If txtInvoiceNo.Text <> "" Then
            Filter2 &= " JDA2_INV_NO Like '%" & txtInvoiceNo.Text.Replace("'", "''") & "%' AND "
        End If
        If Filter2 <> "" Then
            SQL2 &= "  WHERE " & Filter2.Substring(0, Filter2.Length - 4)
        End If
        SQL2 &= "  GROUP BY JDA_2.JDA2_ID,JDA2_Date_Import,JDA2_Registration_Number,JDA2_INV_NO" & vbLf

        '------------JDA4----------------------
        SQL4 &= "  SELECT 'JDA 4'  DocumentType,4 JDA_TYPE,JDA_4.JDA4_ID JDA_ID,JDA4_Date_Import Date_Import,JDA4_Registration_Number Registration_Number,JDA4_INV_NO Invoice_No" & vbLf
        SQL4 &= "  ,COUNT(F.JDA4_ID) TotalFile" & vbLf
        SQL4 &= "  FROM tb_JDA_4 JDA_4" & vbLf
        SQL4 &= "  LEFT JOIN tb_JDA_4_File F ON JDA_4.JDA4_ID=F.JDA4_ID" & vbLf
        Dim Filter4 As String = ""
        If GL.IsProgrammingDate(txtDateOfImport.Text, "dd-MMM-yyyy") Then
            Dim TheDate As DateTime = CV.StringToDate(txtDateOfImport.Text, "dd-MMM-yyyy")
            Filter4 &= " DATEDIFF(dd,JDA_4.JDA4_Date_Import,'" & CV.DateToString(TheDate, "yyyy-MM-dd") & "')=0  AND"
        End If
        If txtRegistrationNumber.Text <> "" Then
            Filter4 &= " JDA_4.JDA4_Registration_Number Like '%" & txtRegistrationNumber.Text.Replace("'", "''") & "%' AND "
        End If
        If txtInvoiceNo.Text <> "" Then
            Filter4 &= " JDA4_INV_NO Like '%" & txtInvoiceNo.Text.Replace("'", "''") & "%' AND "
        End If
        If Filter4 <> "" Then
            SQL4 &= "  WHERE " & Filter4.Substring(0, Filter4.Length - 4)
        End If
        SQL4 &= "  GROUP BY JDA4_Date_Import,JDA_4.JDA4_ID,JDA4_Registration_Number,JDA4_INV_NO" & vbLf

        Select Case ddlDocumentType.SelectedValue
            Case 2
                SQL = SQL2
            Case 4
                SQL = SQL4
            Case Else
                SQL = SQL2 & vbLf & " UNION ALL" & vbLf & SQL4
        End Select


        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        DT.DefaultView.Sort = "Date_Import DESC,Registration_Number,Invoice_No"
        DT = DT.DefaultView.ToTable.Copy

        Session("Filter_JDA_2_4") = DT
        Navigation.SesssionSourceName = "Filter_JDA_2_4"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub


    Private Sub ClearForm()
        'If Not IsNothing(Session("Search_ddlDocumentType")) Then
        '    ddlDocumentType.SelectedValue = Session("Serach_ddlDocumentType")
        'Else
        ddlDocumentType.SelectedValue = 0
        'End If
        'If Not IsNothing(Session("Search_txtDateOfImport")) Then
        txtDateOfImport.Text = Session("Search_txtDateOfImport")
        'End If
        'If Not IsNothing(Session("Search_txtRegistrationNumber")) Then
        txtRegistrationNumber.Text = Session("Search_txtRegistrationNumber")
        'End If
        'If Not IsNothing(Session("Search_txtInvoiceNo")) Then
        txtInvoiceNo.Text = Session("Search_txtInvoiceNo")
        'End If

        '---------------- Check Permission-------------
        If (IsNothing(Session("IS_JDA_2")) OrElse Not Session("IS_JDA_2")) AndAlso (IsNothing(Session("IS_JDA_4")) OrElse Not Session("IS_JDA_4")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        ElseIf IsNothing(Session("IS_JDA_2")) OrElse Not Session("IS_JDA_2") Then
            ddlDocumentType.SelectedIndex = 2 '--------Select JDA1
            ddlDocumentType.Enabled = False
            btnJDA_2.Visible = False
        ElseIf IsNothing(Session("IS_JDA_4")) OrElse Not Session("IS_JDA_4") Then
            ddlDocumentType.SelectedIndex = 1 '---------Select JDA3
            ddlDocumentType.Enabled = False
            btnJDA_4.Visible = False
        Else

        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ddlDocumentType.SelectedValue = -1
        txtRegistrationNumber.Text = ""
        txtInvoiceNo.Text = ""
        txtDateOfImport.Text = ""
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub btnNewDocument_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewDocument.Click
        If IsNothing(Session("User_ID")) Then Exit Sub
        divNewDocument.Visible = True
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        divNewDocument.Visible = False
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim JDA_ID As Integer = e.CommandArgument
                Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
                Dim JDA_TYPE As Integer = btnDelete.Attributes("JDA_TYPE")

                Dim SQL As String = "DELETE FROM tb_JDA_" & JDA_TYPE & "_File WHERE JDA" & JDA_TYPE & "_ID=" & Val(JDA_ID) & vbLf
                SQL &= "DELETE FROM tb_JDA_" & JDA_TYPE & " WHERE JDA" & JDA_TYPE & "_ID=" & Val(JDA_ID)
                Dim Conn As New SqlConnection(ConnStr)
                Conn.Open()
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .CommandType = CommandType.Text
                    .CommandText = SQL
                    .ExecuteNonQuery()
                    .Dispose()
                End With
                Conn.Close()

                '---------------- Delete File -------------
                Try
                    GL.ForceDeleteFileInFolder(Server.MapPath("File/JDA" & JDA_TYPE & "/" & JDA_ID))
                    GL.ForceDeleteFolder(Server.MapPath("File/JDA" & JDA_TYPE & "/" & JDA_ID))
                Catch ex As Exception
                End Try

                BindData()
        End Select

    End Sub


    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblJDA_ID As Label = e.Item.FindControl("lblJDA_ID")
        Dim lblDate_Import As Label = e.Item.FindControl("lblDate_Import")
        Dim lblReg_Number As Label = e.Item.FindControl("lblReg_Number")
        Dim lblInvoice_No As Label = e.Item.FindControl("lblInvoice_No")
        Dim lblDocumentType As Label = e.Item.FindControl("lblDocumentType")
        Dim lblFile As Label = e.Item.FindControl("lblFile")
        Dim btnEdit As HtmlAnchor = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblJDA_ID.Text = Val(e.Item.DataItem("JDA_ID"))

        lblReg_Number.Text = e.Item.DataItem("Registration_Number").ToString

        If Not IsDBNull(e.Item.DataItem("Date_Import")) Then
            lblDate_Import.Text = GL.ReportProgrammingDate(e.Item.DataItem("Date_Import"), "dd-MMM-yyyy")
        End If
        If Not IsDBNull(e.Item.DataItem("TotalFile")) Then
            lblFile.Text = e.Item.DataItem("TotalFile")
        Else
            lblFile.Text = "-"
        End If

        lblDocumentType.Text = e.Item.DataItem("DocumentType").ToString
        lblInvoice_No.Text = e.Item.DataItem("Invoice_No").ToString

        btnEdit.HRef = "JDA_" & e.Item.DataItem("JDA_TYPE") & ".aspx?JDA_Type=" & e.Item.DataItem("DocumentType").ToString & "&JDA_ID=" & e.Item.DataItem("JDA_ID")
        btnDelete.CommandArgument = e.Item.DataItem("JDA_ID")

        btnEdit.Attributes("JDA_TYPE") = e.Item.DataItem("JDA_TYPE")
        btnDelete.Attributes("JDA_TYPE") = e.Item.DataItem("JDA_TYPE")
    End Sub

    Protected Sub Search_Change(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocumentType.SelectedIndexChanged, txtDateOfImport.TextChanged, txtRegistrationNumber.TextChanged, txtInvoiceNo.TextChanged
        'Session("Serach_ddlDocumentType") = ddlDocumentType.SelectedIndex
        'Session("Search_txtDateOfImport") = txtDateOfImport.Text
        'Session("Search_txtRegistrationNumber") = txtRegistrationNumber.Text
        'Session("Search_txtInvoiceNo") = txtInvoiceNo.Text
        BindData()
    End Sub

    Protected Sub btnJDA_2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJDA_2.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='JDA_2.aspx?JDA_Type=JDA_2&JDA_ID=0';", True)
    End Sub

    Protected Sub btnJDA_4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnJDA_4.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='JDA_4.aspx?JDA_Type=JDA_4&JDA_ID=0';", True)
    End Sub

End Class

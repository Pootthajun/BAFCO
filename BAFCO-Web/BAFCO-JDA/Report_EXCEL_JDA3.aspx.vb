﻿
Imports System.Data

Partial Class Report_EXCEL_JDA3
    Inherits System.Web.UI.Page

    Dim C As New Converter

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '---------------- Check Permission-------------
        If IsNothing(Session("IS_JDA_3")) OrElse Not Session("IS_JDA_3") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "alert('You are not authorized to access this area'); window.location.href='Default.aspx'", True)
            Exit Sub
        End If

        If IsNothing(Session("Export_JDA3")) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Timout.\nPlease click to export again'); window.location.href='Export_JDA_3.aspx';", True)
            Exit Sub
        End If

        Dim DT As DataTable = CType(Session("Export_JDA3"), DataTable).Copy
        For i As Integer = 0 To DT.Columns.Count - 1
            'DT.Columns(i).ColumnName = Replace(DT.Columns(i).ColumnName, "JDA3_", "")
            If DT.Columns.Count = 57 Then
                Select Case i
                    Case 1
                        DT.Columns(i).ColumnName = "1.Consignor (Name and Address) Code"
                    Case 2
                        DT.Columns(i).ColumnName = "1.Consignor (Name and Address)"
                    Case 3
                        DT.Columns(i).ColumnName = "2.Consignee (Name and Address) Code"
                    Case 4
                        DT.Columns(i).ColumnName = "2.Consignee (Name and Address)"
                    Case 5
                        DT.Columns(i).ColumnName = "3.Name and Address of Authorised Agent Code"
                    Case 6
                        DT.Columns(i).ColumnName = "3.Name and Address of Authorised Agent"
                    Case 7
                        DT.Columns(i).ColumnName = "4.Mode of Transport"
                    Case 8
                        DT.Columns(i).ColumnName = "5.Date of Despatch"
                    Case 9
                        DT.Columns(i).ColumnName = "6.No./Name of Vessel/Flight/Conveyance"
                    Case 10
                        DT.Columns(i).ColumnName = "7.Port/Place of Loading Code"
                    Case 11
                        DT.Columns(i).ColumnName = "7.Port/Place of Loading"
                    Case 12
                        DT.Columns(i).ColumnName = "7.Port/Place of Discharge Code"
                    Case 13
                        DT.Columns(i).ColumnName = "7.Port/Place of Discharge"
                    Case 14
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only) Code"
                    Case 15
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only)"
                    Case 16
                        DT.Columns(i).ColumnName = "10.Others (Specify)"
                    Case 17
                        DT.Columns(i).ColumnName = "10.Date and Time of Receipt"
                    Case 18
                        DT.Columns(i).ColumnName = "11.Documents Attached Invoice"
                    Case 19
                        DT.Columns(i).ColumnName = "11.Documents Attached Bill of Lading"
                    Case 20
                        DT.Columns(i).ColumnName = "11.Documents Attached Insurance Certificate"
                    Case 21
                        DT.Columns(i).ColumnName = "11.Documents Attached Letter of Credit"
                    Case 22
                        DT.Columns(i).ColumnName = "11.Documents Attached Others"
                    Case 23
                        DT.Columns(i).ColumnName = "12.Registration Number"
                    Case 24
                        DT.Columns(i).ColumnName = "13.Name of Customs Office Code"
                    Case 25
                        DT.Columns(i).ColumnName = "13.Name of Customs Office"
                    Case 26
                        DT.Columns(i).ColumnName = "14.Movement Permit No.(if applicable)"
                    Case 27
                        DT.Columns(i).ColumnName = "15.Date Expiry"
                    Case 28
                        DT.Columns(i).ColumnName = "16.Security Ref.No.(if applicable)"
                    Case 29
                        DT.Columns(i).ColumnName = "17.Amount of Security"
                    Case 30
                        DT.Columns(i).ColumnName = "18.Amount Received/To Be Received Currency"
                    Case 31
                        DT.Columns(i).ColumnName = "18.Amount Received/To Be Received"
                    Case 32
                        DT.Columns(i).ColumnName = "19.Bill of Landing or Consignment Note No."
                    Case 33
                        DT.Columns(i).ColumnName = "20.FOB Value"
                    Case 34
                        DT.Columns(i).ColumnName = "21.Insurance"
                    Case 35
                        DT.Columns(i).ColumnName = "22.Freight"
                    Case 36
                        DT.Columns(i).ColumnName = "23.Gross Wt.(Kg.) Unit"
                    Case 37
                        DT.Columns(i).ColumnName = "23.Gross Wt.(Kg.)"
                    Case 38
                        DT.Columns(i).ColumnName = "24.Measurement (m3)"
                    Case 39
                        DT.Columns(i).ColumnName = "25.CIF Value"
                    Case 40
                        DT.Columns(i).ColumnName = "26.Marks and Numbers"
                    Case 41
                        DT.Columns(i).ColumnName = "36.Name of Declarant"
                    Case 42
                        DT.Columns(i).ColumnName = "37.Identity Card/Passport No."
                    Case 43
                        DT.Columns(i).ColumnName = "38.Status"
                    Case 44
                        DT.Columns(i).ColumnName = "39.To Proper Officer of Customs at"
                    Case 45
                        DT.Columns(i).ColumnName = "39.To Proper Officer of Customs date"
                    Case 46
                        DT.Columns(i).ColumnName = "40.Request Approved"
                    Case 47
                        DT.Columns(i).ColumnName = "40.Request Approved Date"
                    Case 48
                        DT.Columns(i).ColumnName = "41.Certified that the above goods have been duly moved. Removal from Customs control authorised by"
                    Case 49
                        DT.Columns(i).ColumnName = "41.Certified that the above goods have been duly moved. Removal from Customs control authorised date"
                    Case 50
                        DT.Columns(i).ColumnName = "Total Set"
                    Case 51
                        DT.Columns(i).ColumnName = "Invoice No."
                    Case 52
                        DT.Columns(i).ColumnName = "Currency"
                    Case 53
                        DT.Columns(i).ColumnName = "Absolute Total Value"
                    Case 54
                        DT.Columns(i).ColumnName = "Absolute Total Value "
                    Case 55
                        DT.Columns(i).ColumnName = "Create Date"
                    Case 56
                        DT.Columns(i).ColumnName = "System ID"
                End Select
            Else
                Select Case i
                    Case 1
                        DT.Columns(i).ColumnName = "1.Consignor (Name and Address) Code"
                    Case 2
                        DT.Columns(i).ColumnName = "1.Consignor (Name and Address)"
                    Case 3
                        DT.Columns(i).ColumnName = "2.Consignee (Name and Address) Code"
                    Case 4
                        DT.Columns(i).ColumnName = "2.Consignee (Name and Address)"
                    Case 5
                        DT.Columns(i).ColumnName = "3.Name and Address of Authorised Agent Code"
                    Case 6
                        DT.Columns(i).ColumnName = "3.Name and Address of Authorised Agent"
                    Case 7
                        DT.Columns(i).ColumnName = "4.Mode of Transport"
                    Case 8
                        DT.Columns(i).ColumnName = "5.Date of Despatch"
                    Case 9
                        DT.Columns(i).ColumnName = "6.No./Name of Vessel/Flight/Conveyance"
                    Case 10
                        DT.Columns(i).ColumnName = "7.Port/Place of Loading Code"
                    Case 11
                        DT.Columns(i).ColumnName = "7.Port/Place of Loading"
                    Case 12
                        DT.Columns(i).ColumnName = "7.Port/Place of Discharge Code"
                    Case 13
                        DT.Columns(i).ColumnName = "7.Port/Place of Discharge"
                    Case 14
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only) Code"
                    Case 15
                        DT.Columns(i).ColumnName = "9.Via (transhipment cargo only)"
                    Case 16
                        DT.Columns(i).ColumnName = "10.Others (Specify)"
                    Case 17
                        DT.Columns(i).ColumnName = "10.Date and Time of Receipt"
                    Case 18
                        DT.Columns(i).ColumnName = "11.Documents Attached Invoice"
                    Case 19
                        DT.Columns(i).ColumnName = "11.Documents Attached Bill of Lading"
                    Case 20
                        DT.Columns(i).ColumnName = "11.Documents Attached Insurance Certificate"
                    Case 21
                        DT.Columns(i).ColumnName = "11.Documents Attached Letter of Credit"
                    Case 22
                        DT.Columns(i).ColumnName = "11.Documents Attached Others"
                    Case 23
                        DT.Columns(i).ColumnName = "12.Registration Number"
                    Case 24
                        DT.Columns(i).ColumnName = "13.Name of Customs Office Code"
                    Case 25
                        DT.Columns(i).ColumnName = "13.Name of Customs Office"
                    Case 26
                        DT.Columns(i).ColumnName = "14.Movement Permit No.(if applicable)"
                    Case 27
                        DT.Columns(i).ColumnName = "15.Date Expiry"
                    Case 28
                        DT.Columns(i).ColumnName = "16.Security Ref.No.(if applicable)"
                    Case 29
                        DT.Columns(i).ColumnName = "17.Amount of Security"
                    Case 30
                        DT.Columns(i).ColumnName = "18.Amount Received/To Be Received Currency"
                    Case 31
                        DT.Columns(i).ColumnName = "18.Amount Received/To Be Received"
                    Case 32
                        DT.Columns(i).ColumnName = "19.Bill of Landing or Consignment Note No."
                    Case 33
                        DT.Columns(i).ColumnName = "20.FOB Value"
                    Case 34
                        DT.Columns(i).ColumnName = "21.Insurance"
                    Case 35
                        DT.Columns(i).ColumnName = "22.Freight"
                    Case 36
                        DT.Columns(i).ColumnName = "23.Gross Wt.(Kg.) Unit"
                    Case 37
                        DT.Columns(i).ColumnName = "23.Gross Wt.(Kg.)"
                    Case 38
                        DT.Columns(i).ColumnName = "24.Measurement (m3)"
                    Case 39
                        DT.Columns(i).ColumnName = "25.CIF Value"
                    Case 40
                        DT.Columns(i).ColumnName = "26.Marks and Numbers"
                    Case 41
                        DT.Columns(i).ColumnName = "36.Name of Declarant"
                    Case 42
                        DT.Columns(i).ColumnName = "37.Identity Card/Passport No."
                    Case 43
                        DT.Columns(i).ColumnName = "38.Status"
                    Case 44
                        DT.Columns(i).ColumnName = "39.To Proper Officer of Customs at"
                    Case 45
                        DT.Columns(i).ColumnName = "39.To Proper Officer of Customs date"
                    Case 46
                        DT.Columns(i).ColumnName = "40.Request Approved"
                    Case 47
                        DT.Columns(i).ColumnName = "40.Request Approved Date"
                    Case 48
                        DT.Columns(i).ColumnName = "41.Certified that the above goods have been duly moved. Removal from Customs control authorised by"
                    Case 49
                        DT.Columns(i).ColumnName = "41.Certified that the above goods have been duly moved. Removal from Customs control authorised date"
                    Case 50
                        DT.Columns(i).ColumnName = "Total Set"
                    Case 51
                        DT.Columns(i).ColumnName = "Invoice No."
                    Case 52
                        DT.Columns(i).ColumnName = "Currency"
                    Case 53
                        '-----------ปรับเป็น Absolute total Value---------
                        DT.Columns(i).ColumnName = "Absolute Total Value"
                    Case 54
                        '-----------ปรับเป็น Absolute total Value---------
                        DT.Columns(i).ColumnName = "Absolute Total Value "
                    Case 55
                        DT.Columns(i).ColumnName = "Create Date"
                    Case 56
                        DT.Columns(i).ColumnName = "Shipment ID"
                    Case 57
                        DT.Columns(i).ColumnName = "ID"
                    Case 58
                        DT.Columns(i).ColumnName = "27.Item No."
                    Case 59
                        DT.Columns(i).ColumnName = "29.Description of Goods"
                    Case 60
                        DT.Columns(i).ColumnName = "30.Code No."
                    Case 61
                        DT.Columns(i).ColumnName = "31.Unit"
                    Case 62
                        DT.Columns(i).ColumnName = "32.Country of Origin Code"
                    Case 63
                        DT.Columns(i).ColumnName = "33.Qty based on Customs Tariff Unit"
                    Case 64
                        DT.Columns(i).ColumnName = "34.Per Unit"
                    Case 65
                        DT.Columns(i).ColumnName = "35.Total Value"
                    Case 66
                        DT.Columns(i).ColumnName = "35.Total THB"
                    Case 67
                        DT.Columns(i).ColumnName = "System ID"
                End Select
            End If
            
        Next
        '---------------ปรับรูปแบบข้อมูล --------------------
        Dim NewDT As New DataTable
        For j As Integer = 0 To DT.Columns.Count - 1
            NewDT.Columns.Add(DT.Columns(j).ColumnName)
            'Response.Write(DT.Columns(j).DataType.ToString & "<br>")
        Next

        For i As Integer = 0 To DT.Rows.Count - 1
            Dim DR As DataRow = NewDT.NewRow

            For j As Integer = 0 To DT.Columns.Count - 1
                Select Case DT.Columns(j).DataType.ToString
                    Case "System.Int32", "System.String"
                        If IsNumeric(DT.Rows(i).Item(j).ToString) Then
                            DR(j) = "'" & DT.Rows(i).Item(j).ToString
                        Else
                            DR(j) = DT.Rows(i).Item(j).ToString
                        End If
                    Case "System.DateTime"
                        If Not IsDBNull(DT.Rows(i).Item(j)) Then
                            DR(j) = " " & C.DateToString(DT.Rows(i).Item(j), "dd-MMM-yyyy")
                        End If
                    Case "System.Boolean"
                        If Not IsDBNull(DT.Rows(i).Item(j)) Then
                            If DT.Rows(i).Item(j) Then
                                DR(j) = "yes"
                            Else
                                DR(j) = "no"
                            End If
                        Else
                            DR(j) = ""
                        End If
                    Case "System.Decimal"
                        If Not IsDBNull(DT.Rows(i).Item(j)) Then
                            DR(j) = " " & FormatNumber(DT.Rows(i).Item(j))
                        End If
                    Case Else

                End Select
            Next

            NewDT.Rows.Add(DR)
        Next
        DT = NewDT

        Dim Filename As String = Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & ".xls"
        Dim gv As New DataGrid
        gv.DataSource = DT
        gv.DataBind()

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & Filename)
        Response.ContentType = "application/ms-excel"
        Response.ContentEncoding = System.Text.Encoding.Unicode
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())
        Dim sw As New System.IO.StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        gv.RenderControl(hw)
        Response.Write(sw.ToString())
        Response.End()
    End Sub

End Class

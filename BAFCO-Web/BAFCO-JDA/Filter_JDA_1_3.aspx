﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Filter_JDA_1_3.aspx.vb" Inherits="Filter_JDA_1_3"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
        <ContentTemplate>
        
          <div id="divNewDocument" runat="server" style="position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1;" visible="False">                    
            <div style="width:100%; height:100%; position:absolute; background-color:Gray; filter:alpha(opacity=80); opacity: 0.80;" ></div>
            <div style="left:39.4%; top:20%; position:absolute; background-color:#F6F3F3; padding: 20px 20px 20px 20px;">
                <table width="100%" height="30" border="0" cellpadding="0" cellspacing="0" >
                 <tr>
                    <td align="center" class="Calibri_Head_Blue" >
                        New Document
                        
                        <div style="margin-top:20px;"><asp:Button ID="btnJDA_1" runat="server" CssClass="Button_Red" Height="30px" Text="Create JDA 1 " Width="90%" /></div>
                       
                        <div style="margin-top:20px;"><asp:Button ID="btnJDA_3" runat="server" CssClass="Button_Red" Text="Create JDA 3 " Height="30px"  Width="90%"/></div>
                        
                        <div style="margin-top:20px;"><asp:Button ID="btnCancel" runat="server" CssClass="Button_White" Text="Cancel" Height="30px"  Width="90%"/></div>
                    </td>                
                </tr>
            </table>
            
                
            </div> 
        </div>
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0">    
            <tr>
                <td valign="top">
                
                    <asp:Panel ID="pnlFilter" runat="server" DefaultButton="btnSearch">
                     <table align="left" cellpadding="0" cellspacing="1">
                        <tr>
                            <td>
                                <div class="Fieldset_Container" >
                                    <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                    <table width="950px" align="center" cellspacing="5px;" style="margin-left:10px; ">
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Document Type</td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlDocumentType" runat="server" AutoPostBack="true"
                                                CssClass="Dropdown_Form_White" width="150px" >
                                                    <asp:ListItem Value="-1" Text="-------- All --------" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="JDA 1"></asp:ListItem>
                                                    <asp:ListItem Value="3" Text="JDA 3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td width="40" height="25">&nbsp;</td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Date of Import                                                
                                            </td>
                                            <td width="145" height="25" colspan="6">
                                               <asp:TextBox ID="txtDateOfImport" runat="server" CssClass="Textbox_Form_White" width="145px"  style="text-align:center;"  AutoPostBack="true" placeholder="dd-MMM-yyyy"></asp:TextBox>
                                                    <Ajax:CalendarExtender ID="clDateOfImport" runat="server" 
                                                    Format="dd-MMM-yyyy"  BehaviorID="txtDateOfImport" TargetControlID="txtDateOfImport"></Ajax:CalendarExtender>
                                             </td>
                                        </tr>
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Registration Number
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtRegistrationNumber" runat="server" CssClass="Textbox_Form_White"  style="text-align:center;"  width="145px" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                           
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Invoice No</td>
                                            <td width="145" height="25">
                                                
                                                 <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="Textbox_Form_White"  style="text-align:center;" width="145px" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td></td>
                                        </tr>                                       
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                BAFCO Job No.
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txt_BAFCO_Job_No" runat="server" CssClass="Textbox_Form_White"  style="text-align:center;"  width="145px" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                           
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Client Reference No.   
                                            </td>
                                            <td height="25">
                                                 
                                                 <asp:TextBox ID="txt_Client_Reference_No" runat="server" CssClass="Textbox_Form_White"  style="text-align:center;" width="145px" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td></td>
                                        </tr>    
                                        
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>  
                    </asp:Panel>     
                </td>
            </tr>
            <tr>
                <td height="20" valign="middle">
                    <table cellpadding="0" cellspacing="2" style="margin: 0px 0px 10px 10px; width:970px;">
                        <tr>
                            <td style="width:100px;">
                                <asp:Button ID="btnClear" runat="server" Text="All" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td style="width:100px;">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td style="width:100px;">
                                <asp:Button ID="btnNewDocument" runat="server" Text="New Document" CssClass="Button_Red" Width="100px" Height="30px"/>
                                
                            </td>
                            <td align="right">
                               <asp:Label ID="lblSearchResult" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="1" style="margin: 0px 0px 0px 10px;" bgColor="#CCCCCC" class="NormalTextBlack">
                        <tr>
                            <td class="Grid_Header">
                                No
                            </td>
                            <td class="Grid_Header">
                                Consignor Export
                            </td>
                            <td class="Grid_Header">
                                 Consignee/Importer
                            </td>
                            <td class="Grid_Header">
                                Date of Import
                            </td>
                            <td class="Grid_Header">
                                Registration Number
                            </td>
                            <td class="Grid_Header">
                                Invoice No
                            </td>
                            <td class="Grid_Header">
                                Document Type
                            </td>
                            <td class="Grid_Header">
                                BAFCO Job No.
                            </td>
                            <td class="Grid_Header">
                               Client Reference No.
                            </td>
                            <td class="Grid_Header">
                                Attachment
                            </td>
                            <td width="100" class="Grid_Header">
                                Action
                            </td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trRow" runat="server" style="border-bottom:solid 1px #efefef;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td class="Grid_Detail" align="center"  style=" vertical-align:top; ">
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        <asp:Label ID="lblJDA_ID" runat="server" visible="false"></asp:Label>
                                    </td>
                                    <td class="Grid_Detail"  style=" vertical-align:top; ">
                                        <asp:Label ID="lblConsignorExport" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail"  style=" vertical-align:top; ">
                                        <asp:Label ID="lblConsignorImporter" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" align ="center "  style=" vertical-align:top; text-align:center;">
                                        <asp:Label ID="lblDate_Import" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail"  style=" vertical-align:top; text-align:center;">
                                        <asp:Label ID="lblReg_Number" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" align="center"  style=" vertical-align:top; text-align:center; ">
                                        <asp:Label ID="lblInvoice_No" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail"  style=" vertical-align:top; text-align:center; ">
                                        <asp:Label ID="lblDocumentType" runat="server" ></asp:Label>
                                    </td>
                                    
                                    <td class="Grid_Detail"  style=" vertical-align:top; text-align:center; ">
                                        <asp:Label ID="lbl_BAFCO_Job_No" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail"  style=" vertical-align:top; text-align:center; ">
                                        <asp:Label ID="lbl_Client_Reference_No" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail"  style=" vertical-align:top; text-align:center; ">
                                        <asp:Label ID="lblFile" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" align="center"   style=" vertical-align:top; ">
                                        <a ID="btnEdit" runat="server" title="Edit"><img src="images/icon/10.png" style="height:25px;" alt="Edit" /></a>                                        
                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                        <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete">
                                        </Ajax:ConfirmButtonExtender>
                                        <asp:ImageButton ID="btnView" CommandName="Print" runat="server" ToolTip="Print" ImageUrl="images/icon/64.png" Height="25px" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div style="margin-left:10px">
                        <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="10" Visible="false"/>
                    </div>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


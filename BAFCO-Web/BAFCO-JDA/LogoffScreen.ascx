﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LogoffScreen.ascx.vb" Inherits="LogoffScreen" %>
 
<link rel="stylesheet" href="style/logoffscreen.css"/>
    
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      
        <asp:Panel id="divImage" runat="server" style="position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1000;" DefaultButton="btnLogin">                    
            <div style="width:100%; height:100%; position:absolute; background-color:black; filter:alpha(opacity=80); opacity: 0.80;" ></div>
            <div style="left:40%; top:20%; position:absolute; background-color:#F6F3F3; padding: 20px 30px 20px 30px;" >
                <div class="footer" style="padding-bottom:0px; padding-top:0px;">
                    <footer>
                        <div class="subscribe_block" >
                                <h3><span>JDA Report SYSTEM</span></h3>
                                <p><asp:TextBox ID="txtUser" runat="server" placeholder="Enter Your User ID ..."></asp:TextBox></p>
                                <p><asp:TextBox ID="txtPass" runat="server" TextMode="Password" placeholder="Enter Your Password ..."></asp:TextBox></p>
                                <p><asp:Button type="submit" id="btnLogin" runat="server" value="SIGN IN" Text="SIGN IN" ></asp:Button></p>                               
                        </div>
                   </footer>
                </div>      
            </div>         
        </asp:Panel>               
    </ContentTemplate>
</asp:UpdatePanel>
﻿Imports CrystalDecisions.Web
Imports System.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO

Partial Class ReportViewer
    Inherits System.Web.UI.Page

    Dim C As New Converter
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim DT As DataTable = Session("ExportDataFilter")
        Dim Filename As String = ""
        Filename = Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
        Dim gv As New DataGrid
        gv.DataSource = DT
        gv.DataBind()

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=" & Filename & ".xls")
        Response.ContentType = "application/ms-excel"
        Response.ContentEncoding = System.Text.Encoding.Unicode
        Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble())
        Dim sw As New System.IO.StringWriter()
        Dim hw As New HtmlTextWriter(sw)
        gv.RenderControl(hw)
        Response.Write(sw.ToString())
        Response.End()
    End Sub
End Class

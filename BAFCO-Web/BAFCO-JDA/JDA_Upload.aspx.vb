﻿
Partial Class JDA_Upload
    Inherits System.Web.UI.Page

    Dim CV As New Converter

    Public Property PageUniqueID() As String
        Get
            Return txtP.Text
        End Get
        Set(ByVal value As String)
            txtP.Text = value
        End Set
    End Property

    Public Property parentButton() As String
        Get
            Return txtB.Text
        End Get
        Set(ByVal value As String)
            txtB.Text = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ful.Attributes("onchange") = "$('#" & btnUpload.ClientID & "').click();"
            PageUniqueID = Request.QueryString("P") '---------- Set onlyone Time
            parentButton = Request.QueryString("B")
        End If
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If Not ful.HasFile Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Please upload PDF, PNG or JPG file only');", True)
            Exit Sub
        End If

        Dim F As New GenericLib.FileStructure
        Select Case ful.PostedFile.ContentType.ToString.ToLower
            Case "image/png"
                F.FileType = GenericLib.AllowFileType.PNG
            Case "image/jpeg"
                F.FileType = GenericLib.AllowFileType.JPG
            Case "application/pdf"
                F.FileType = GenericLib.AllowFileType.PDF
            Case Else
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Please upload PDF, PNG or JPG file only');", True)
                Exit Sub
        End Select

        F.AbsoluteName = ful.PostedFile.FileName
        F.Content = CV.StreamToByte(ful.PostedFile.InputStream)
        '-------- Find Empy FileIndex--------
        For i As Integer = 1 To 100
            Dim S_Name As String = "File_" & PageUniqueID & "_" & i
            If IsNothing(Session(S_Name)) Then
                F.FileIndex = i
                Session(S_Name) = F
                DisplayFile()
                Exit Sub
            End If
        Next
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "alert('Limit exceed\n\nAllow not more than 100 files per report');", True)

    End Sub

    Private Sub DisplayFile()
        Dim Script As String = "$('#" & parentButton & "', window.parent.document).click();"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Update", Script, True)
    End Sub
End Class

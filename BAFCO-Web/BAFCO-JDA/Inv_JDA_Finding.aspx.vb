﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Inv_JDA_Finding
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Private Property Inv() As String
        Get
            Return ViewState("Inv")
        End Get
        Set(ByVal value As String)
            ViewState("Inv") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Inv = Session("Inv")
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = ""
        SQL &= "SELECT LTRIM(RTRIM(JDA1_INV_NO)) AS INV,ISNULL(MAX(CONVERT(INTEGER,JDA1_31Item_No)),0) AS ITEM" & vbNewLine
        SQL &= "FROM tb_JDA_1 HD LEFT JOIN tb_JDA_1_Detail DT ON HD.JDA1_ID = DT.JDA1_ID" & vbNewLine
        SQL &= "WHERE ISNULL(JDA1_INV_NO,'') <> ''" & vbNewLine
        If Inv <> "" Then
            SQL &= "AND JDA1_INV_NO NOT IN (" & Inv & ")" & vbNewLine
        End If
        If txtInv.Text <> "" Then
            SQL &= "AND JDA1_INV_NO LIKE '%" & txtInv.Text.Replace("'", "''") & "%'" & vbNewLine
        End If

        SQL &= "GROUP BY JDA1_INV_NO" & vbNewLine
        SQL &= "HAVING ISNULL(MAX(CONVERT(INTEGER,JDA1_31Item_No)),0) > 0 " & vbNewLine
        SQL &= "ORDER BY JDA1_INV_NO" & vbNewLine
        Dim DA_JDA1 As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_JDA1 As New DataTable
        DA_JDA1.Fill(DT_JDA1)

        SQL = ""
        SQL &= "SELECT LTRIM(RTRIM(JDA3_INV_NO)) AS INV,ISNULL(MAX(CONVERT(INTEGER,JDA3_31Item_No)),0) AS ITEM" & vbNewLine
        SQL &= "FROM tb_JDA_3 HD LEFT JOIN tb_JDA_3_Detail DT ON HD.JDA3_ID = DT.JDA3_ID" & vbNewLine
        SQL &= "WHERE ISNULL(JDA3_INV_NO,'') <> ''" & vbNewLine
        If Inv <> "" Then
            SQL &= "AND JDA3_INV_NO NOT IN (" & Inv & ")" & vbNewLine
        End If
        If txtInv.Text <> "" Then
            SQL &= "AND JDA3_INV_NO LIKE '%" & txtInv.Text.Replace("'", "''") & "%'" & vbNewLine
        End If

        SQL &= "GROUP BY JDA3_INV_NO" & vbNewLine
        SQL &= "HAVING ISNULL(MAX(CONVERT(INTEGER,JDA3_31Item_No)),0) > 0 " & vbNewLine
        SQL &= "ORDER BY JDA3_INV_NO" & vbNewLine
        Dim DA_JDA3 As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_JDA3 As New DataTable
        DA_JDA3.Fill(DT_JDA3)

        Dim DT As New DataTable
        DT_JDA1.Merge(DT_JDA3)
        DT_JDA1.DefaultView.Sort = "INV ASC"
        DT = DT_JDA1.DefaultView.ToTable

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("InvFinding") = DT
        Navigation.SesssionSourceName = "InvFinding"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblInv As Label = e.Item.FindControl("lblInv")
        Dim lblItem As Label = e.Item.FindControl("lblItem")
        Dim lblJDA As Label = e.Item.FindControl("lblJDA")
        Dim trRow As HtmlTableRow = e.Item.FindControl("trRow")

        lblNo.Text = e.Item.ItemIndex + 1
        lblInv.Text = e.Item.DataItem("INV")
        lblItem.Text = e.Item.DataItem("ITEM")
        trRow.Attributes("onClick") = "returnSelectedValue('" & CStr(e.Item.DataItem("INV")).Replace("'", "\'") & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
    End Sub

    Protected Sub txtInv_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInv.TextChanged
        BindData()
    End Sub
End Class

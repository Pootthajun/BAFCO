﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class LogoffScreen
    Inherits System.Web.UI.UserControl

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Public Event Login()


    Public Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Session("User_ID")) Then
            divImage.Visible = True
        Else
            divImage.Visible = False
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim SQL As String = "SELECT * FROM MS_USER WHERE USER_CODE='" & txtUser.Text.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid username');", True)
            txtUser.Focus()
            Exit Sub
        End If

        If DT.Rows(0).Item("User_Password") <> txtPass.Text Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid password');", True)
            txtPass.Focus()
            Exit Sub
        End If

        If IsDBNull(DT.Rows(0).Item("ACTIVE_STATUS")) OrElse Not DT.Rows(0).Item("ACTIVE_STATUS") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Your account is disable\nAny information, please contact administrator!!');", True)
            Exit Sub
        End If

        Session("USER_ID") = DT.Rows(0).Item("USER_ID")
        Session("USER_FULLNAME") = DT.Rows(0).Item("USER_FULLNAME")

        If Not IsDBNull(DT.Rows(0).Item("IS_ADMIN")) Then
            Session("IS_ADMIN") = DT.Rows(0).Item("IS_ADMIN")
        Else
            Session("IS_ADMIN") = False
        End If

        If Not IsDBNull(DT.Rows(0).Item("IS_JDA_1")) Then
            Session("IS_JDA_1") = DT.Rows(0).Item("IS_JDA_1")
        Else
            Session("IS_JDA_1") = False
        End If

        If Not IsDBNull(DT.Rows(0).Item("IS_JDA_2")) Then
            Session("IS_JDA_2") = DT.Rows(0).Item("IS_JDA_2")
        Else
            Session("IS_JDA_2") = False
        End If

        If Not IsDBNull(DT.Rows(0).Item("IS_JDA_3")) Then
            Session("IS_JDA_3") = DT.Rows(0).Item("IS_JDA_3")
        Else
            Session("IS_JDA_3") = False
        End If

        If Not IsDBNull(DT.Rows(0).Item("IS_JDA_4")) Then
            Session("IS_JDA_4") = DT.Rows(0).Item("IS_JDA_4")
        Else
            Session("IS_JDA_4") = False
        End If

        divImage.Visible = False
        RaiseEvent Login()
    End Sub
End Class

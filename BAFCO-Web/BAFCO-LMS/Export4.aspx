﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageNewWindows.master" AutoEventWireup="false" CodeFile="Export4.aspx.vb" Inherits="Export4" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Export
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;" >
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:119;">
		                            <input runat="server" value="Receive Pre-Alert" id="btnStatus1" class="Button_White" style="height:30px;width:119px;" type="button" />
		                        </td>
		                        <td style="width:190;">
		                            <input runat="server" value="Submit Document to AOT/PAT" id="btnStatus2" class="Button_White" style="height:30px;width:190px;" type="button" />
		                        </td>
		                        <td style="width:190;">
		                            <input runat="server" value="Store in Warehouse & Packing" id="btnStatus3" class="Button_White" style="height:30px;width:190px;" type="button" />
		                        </td>
		                        <td style="width:90;">
		                             <input runat="server" value="Booking" id="btnStatus4" class="Button_Red" style="height:30px;width:90px;" type="button" />
		                        </td>
		                        <td style="width:180;">
		                             <input runat="server" value="Prepare Export Entry Form" id="btnStatus5" class="Button_White" style="height:30px;width:180px;" type="button" />
		                        </td>
		                        <td style="width:130;">
		                             <input runat="server" value="Release Shipment" id="btnStatus6" class="Button_White" style="height:30px;width:130px;" type="button" />
		                        </td>
		                        <td style="width:90;">
		                             <input runat="server" value="Pre Alert" id="btnStatus7" class="Button_White" style="height:30px;width:90px;" type="button" />
		                        </td>
		                        <td style="width:120;">
		                             <input runat="server" value="Accounting Info" id="btnStatus8" class="Button_White" style="height:30px;width:120px;" type="button" />
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Booking Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtBookingDate" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgBookingDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgBookingDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgBookingDate"
                        TargetControlID="txtBookingDate"></Ajax:CalendarExtender>
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        MAWB/BL
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtMAWB" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        HAWB/BL
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtHAWB" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Flight No./Vessel
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtFlightNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        ETD Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtETDDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgETDDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgETDDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgETDDate"
                        TargetControlID="txtETDDate"></Ajax:CalendarExtender>
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        ETA Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtETADate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgETADate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgETADate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgETADate"
                        TargetControlID="txtETADate"></Ajax:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Port of Departure
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlPortOfDeparture" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Port of Deatination
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlPortOfDeatination" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


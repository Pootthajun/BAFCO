﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageNewWindows.master" AutoEventWireup="false" CodeFile="Local1.aspx.vb" Inherits="Local1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
        <div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Local
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table> 
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;">
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:130;">
		                            <input runat="server" value="Received Shipment" id="btnStatus1" class="Button_Red" style="height:30px;width:130px;" type="button" />
		                        </td>
		                        <td style="width:190;">
		                             <input runat="server" value="Store in Warehouse & Packing" id="btnStatus2" class="Button_White" style="height:30px;width:190px;" type="button" />
		                        </td>
		                        <td style="width:120;">
		                             <input runat="server" value="Delivery Info" id="btnStatus3" class="Button_White" style="height:30px;width:120px;" type="button" />
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>  
		    <table cellspacing="5px;" style="margin-left:10px;">
		        <tr>
                    <td width="120" class="NormalTextBlack" valign="top">
                        Received Date
                    </td>
                    <td width="200" class="NormalTextNoneBorder" valign="top">
                        <%--<asp:TextBox ID="txtReeiveDate" runat="server" CssClass="Textbox_Form_Disable" width="200px" ReadOnly="true" Text=""></asp:TextBox>--%>
                        <asp:Label ID="lblReeiveDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" ></td>
                   <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Reference Lot No
                    </td>
                    <td width="200" height="25" class="NormalTextNoneBorder" valign="top">
                        <%--<asp:TextBox ID="txtRefLotNo" runat="server" CssClass="Textbox_Form_Disable" width="200px" ReadOnly="true" Text=""></asp:TextBox>--%>
                        <asp:Label ID="lblRefLotNo" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" class="NormalTextBlack" valign="top">
                        Consignee
                    </td>
                    <td width="200" class="NormalTextNoneBorder">
                        <%--<asp:TextBox ID="txtConsignee" runat="server" CssClass="Textbox_Form_Disable" width="200px" ReadOnly="true" Text=""></asp:TextBox>--%>
                        <asp:Label ID="lblConsignee" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40"></td>
                    <td width="120" class="NormalTextBlack" valign="top">
                        Supplier
                    </td>
                    <td width="200" class="NormalTextNoneBorder" valign="top">
                        <%--<asp:TextBox ID="txtSupplier" runat="server" CssClass="Textbox_Form_Disable" width="200px" ReadOnly="true" Text=""></asp:TextBox>--%>
                        <asp:Label ID="lblSupplier" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" class="style2"></td>
                    <td class="style2"></td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        PO No
                    </td>
                    <td height="25" colspan="6">
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextNoneBorder">
                            <tr style="font-family: Tahoma; font-size: 12px; color:White; text-align:center; background-color:Gray; height:25px;">
                                <td width="200" align="center">
                                    PO No                    
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                            <asp:Label ID="lblPONo" runat="server"></asp:Label>
                                        </td>                                  
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        No. of Package
                    </td>
                    <td width="200" height="25">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80" height="25" class="NormalTextNoneBorder" align="center">
                                    <%--<asp:TextBox ID="txtQty" runat="server" CssClass="Textbox_Form_Disable" width="80px" ReadOnly="true" Text=""></asp:TextBox>--%>
                                    <asp:Label ID="lblQty" runat="server" Text="-"></asp:Label>
                                </td>
                                <td width="10px"></td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlUOM" runat="server" CssClass="Dropdown_Form_White" width="70px" ></asp:DropDownList>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Weight
                    </td>
                    <td width="200" height="25" class="NormalTextNoneBorder">
                        <%--<asp:TextBox ID="txtWeight" runat="server" CssClass="Textbox_Form_Disable" width="200px" ReadOnly="true" Text=""></asp:TextBox>--%>
                        <asp:Label ID="lblWeight" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Volume
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtVolume" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td colspan="4"></td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        Description of Goods
                    </td>
                    <td width="200" height="25" colspan="4">
                        <asp:TextBox ID="txtDescriptionOfGoods" runat="server" CssClass="Textbox_Form_White" width="600px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr><td colspan="7"></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="120px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


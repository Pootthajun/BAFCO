﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LogoffScreen.ascx.vb" Inherits="LogoffScreen" %>
 
<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
<link rel="stylesheet" href="style/logoffscreen.css"/>
    
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnLogin">
        <div id="divImage" runat="server" style="position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1000;">                    
            <div style="width:100%; height:100%; position:absolute; background-color:black; filter:alpha(opacity=80); opacity: 0.80;" ></div>
            <div style="left:40%; top:20%; position:absolute; background-color:#F6F3F3; padding: 20px 30px 20px 30px;" >
                <div class="footer" style="padding-bottom:0px; padding-top:0px;">
                    <footer>
                        <div class="subscribe_block" >
                                <h3><span>TRACKING SYSTEM</span></h3>
                                <p><input type="text" id="txtUsername" runat="server" value="" placeholder="Enter Your User ID ..."/></p>
                                <p><input type="password" id="txtPassword" runat="server" value="" placeholder="Enter Your Password ..."/></p>
                                <%--<p><input type="submit" id="btnLogin" runat="server" value="SIGN IN" /></p>--%>
                                <p><asp:Button type="submit" id="btnLogin" runat="server" value="SIGN IN" Text="SIGN IN" ></asp:Button></p>
                                <input name="action" value="login" type="hidden" />
                        </div>
                   </footer>
                </div>      
            </div> 
        </div> 
        </asp:Panel>               
    </ContentTemplate>
</asp:UpdatePanel>
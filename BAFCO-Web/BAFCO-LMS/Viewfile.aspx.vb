﻿Imports System.IO

Partial Class Viewfile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Extension As String = ""
        Dim ContentType As String = ""
        Extension = Path.GetExtension(Session("PathImage"))
        Dim B As Byte() = File.ReadAllBytes(Session("PathImage"))
        Response.Clear()
        Select Case Extension.ToString.ToUpper
            Case ".PDF"
                Response.ContentType = "application/pdf"
                ContentType = "application/pdf"
            Case ".PNG"
                Response.ContentType = "image/png"
                ContentType = "image/png"
            Case ".JPG"
                Response.ContentType = "image/jpg"
                ContentType = "image/jpg"
            Case ".JPAG"
                Response.ContentType = "image/jpag"
                ContentType = "image/jpag"
        End Select
        Response.AddHeader("Content-Type", ContentType)
        Response.BinaryWrite(B)
        Response.End()
    End Sub
End Class

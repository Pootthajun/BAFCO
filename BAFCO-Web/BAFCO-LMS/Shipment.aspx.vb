﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Shipment
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlBranch(ddlBranch, "-------- All --------", "")
            MC.BindDDlCompany(ddlCompany, "-------- All --------", "")
            MC.BindCBLShipmentType(cblShipmentType)
            MC.BindDDlStatus(ddlStatus, "-------- All --------", "")
            MC.BindDDlTransportationType(ddlTransportationType, "-------- All --------", "")
            MC.BindDDlPortOfDestination(ddlPortOfDestination, "-------- All --------", "")

            MC.BindDDlBranch(ddl_N_Branch, "", "")
            MC.BindDDlCompany(ddl_N_Company, "", "")
            MC.BindDDlShipmentType(ddl_N_ShipmentType, "", "")
            MC.BindDDlTransportationType(ddl_N_TransportationType, "", "")

            ClearForm()
            BindData()
        End If
        Dim lblUName As Label
        lblUName = CType(Master.FindControl("lblUName"), Label)
        If lblUName.Text = "" And IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Shipment.aspx';", True)
        End If
       
    End Sub

    Private Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = "SELECT * FROM VW_SHIPMENT ORDER BY CREATE_DATE DESC" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Session("Shipment") = DT
        FilterData()
    End Sub

    Sub FilterData()
        Dim DT As New DataTable
        DT = Session("Shipment")
        If Not (DT Is Nothing) AndAlso DT.Rows.Count > 0 Then
            '--------------- Filter ----------------------
            Dim Filter As String = ""

            If ddlBranch.SelectedIndex > 0 Then
                Filter &= "BRANCH_ID =" & ddlBranch.SelectedValue & " AND "
            End If
            If ddlCompany.SelectedIndex > 0 Then
                Filter &= "COMPANY_ID =" & ddlCompany.SelectedValue & " AND "
            End If
            Dim ShipmentType As String = ""
            For i As Int32 = 0 To cblShipmentType.Items.Count - 1
                If cblShipmentType.Items(i).Selected = True Then
                    ShipmentType = ShipmentType & cblShipmentType.Items(i).Value.ToString & ","
                End If
            Next
            If ShipmentType <> "" Then
                ShipmentType = ShipmentType.Substring(0, ShipmentType.Length - 1)
                Filter &= "SHIPMENT_TYPE_ID in (" & ShipmentType & ") AND "
            End If
            If ddlStatus.SelectedIndex > 0 Then
                Filter &= "STATUS_ID =" & ddlStatus.SelectedValue & " AND "
            End If
            If txtWorkOrderNo.Text.Trim <> "" Then
                Filter &= "WORK_ORDER_NO like '%" & txtWorkOrderNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtPONo.Text.Trim <> "" Then
                Filter &= "ALL_PO like '%" & txtPONo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtInvoiceNo.Text.Trim <> "" Then
                Filter &= "INVOICE_NO like '%" & txtInvoiceNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If ddlTransportationType.SelectedIndex > 0 Then
                Filter &= "TRANSPORT_TYPE_ID =" & ddlTransportationType.SelectedValue & " AND "
            End If
            If txtHAWB.Text.Trim <> "" Then
                Filter &= "HAWB like '%" & txtHAWB.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If ddlPortOfDestination.SelectedIndex > 0 Then
                Filter &= "DESTINATION_ID =" & ddlPortOfDestination.SelectedValue & " AND "
            End If
            If ddlPending.SelectedIndex > 0 Then
                Filter &= "PENDING =" & ddlPending.SelectedValue & " AND "
            End If
            If txtPreAlertFrom.Text.Trim <> "" Then
                If MC.CheckDate(txtPreAlertFrom.Text) Then
                    Filter &= "PRE_ALERT >=" & txtPreAlertFrom.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtPreAlertTo.Text.Trim <> "" Then
                If MC.CheckDate(txtPreAlertTo.Text) Then
                    Filter &= "PRE_ALERT <=" & txtPreAlertTo.Text.Replace("-", "") & " AND "
                End If
            End If

            If Session("User_Type") = "User" Then
                Filter &= "CREATE_BY =" & Session("User_ID") & " AND "
            ElseIf Session("User_Type") = "Customer" Then
                Filter &= "COMPANY_NAME ='" & Session("User_Fullname") & "' AND "
            End If

            If Filter <> "" Then Filter = Filter.Substring(0, Filter.Length - 4)
            DT.DefaultView.RowFilter = Filter
            DT = DT.DefaultView.ToTable
            If DT.Rows.Count <= 15 Then
                Navigation.Visible = False
            Else
                Navigation.Visible = True
            End If

            Session("ShipmentFilter") = DT
            Navigation.SesssionSourceName = "ShipmentFilter"
            Navigation.RenderLayout()
        End If
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblShipmentID As Label = e.Item.FindControl("lblShipmentID")
        Dim lblShipmentCode As Label = e.Item.FindControl("lblShipmentCode")
        Dim lblStatusID As Label = e.Item.FindControl("lblStatusID")
        Dim lblWorkOrder As Label = e.Item.FindControl("lblWorkOrder")
        Dim lblCompany As Label = e.Item.FindControl("lblCompany")
        Dim lblSupplier As Label = e.Item.FindControl("lblSupplier")
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        'Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnEdit As HtmlAnchor = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnView As ImageButton = e.Item.FindControl("btnView")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblShipmentID.Text = e.Item.DataItem("SHIPMENT_ID").ToString
        lblShipmentCode.Text = e.Item.DataItem("SHIPMENT_CODE").ToString
        lblStatusID.Text = e.Item.DataItem("STATUS_ID").ToString
        lblWorkOrder.Text = e.Item.DataItem("WORK_ORDER_NO").ToString
        lblCompany.Text = e.Item.DataItem("COMPANY_NAME").ToString
        lblSupplier.Text = e.Item.DataItem("SUPPLIER_NAME").ToString
        lblType.Text = e.Item.DataItem("SHIPMENT_TYPE").ToString
        lblStatus.Text = e.Item.DataItem("STATUS_NAME").ToString

        If Session("User_Type") = "Admin" Then
            btnEdit.Visible = True
            btnDelete.Visible = True
            btnView.Visible = True
        ElseIf Session("User_Type") = "User" Then
            btnEdit.Visible = True
            btnDelete.Visible = False
            btnView.Visible = True
        Else
            btnEdit.Visible = False
            btnDelete.Visible = False
            btnView.Visible = True
        End If

        If lblType.Text.ToUpper = "LOCAL" Then
            btnDelete.Visible = False
        End If
        '------------------
        Dim btnDelete_Confirm As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("btnDelete_Confirm")
        btnDelete_Confirm.ConfirmText = "Delete action will be affected for all related information !!" & vbLf & vbLf & "Are you sure you want to delete '" & e.Item.DataItem("WORK_ORDER_NO").ToString & "' permanently !?"

        '----------- Set btnEdit --------
        Dim URL As String = ""
        Select Case e.Item.DataItem("SHIPMENT_TYPE").ToString
            Case "Import"
                URL = "Import" & lblStatusID.Text & ".aspx?ShipmentCode=" & lblShipmentCode.Text
                'Select Case lblStatusID.Text
                '    Case "1"
                '        URL = "Import1.aspx?ShipmentCode=" & lblShipmentCode.Text
                '    Case "2"
                '        URL = "Import2.aspx?ShipmentCode=" & lblShipmentCode.Text
                '    Case "3"
                '        URL = "Import3.aspx?ShipmentCode=" & lblShipmentCode.Text
                '    Case "4"
                '        URL = "Import4.aspx?ShipmentCode=" & lblShipmentCode.Text
                '    Case "5"
                '        URL = "Import5.aspx?ShipmentCode=" & lblShipmentCode.Text
                '    Case "6"
                '        URL = "Import6.aspx?ShipmentCode=" & lblShipmentCode.Text
                'End Select
            Case "Export"

                Select Case lblStatusID.Text
                    Case "1"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export1.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "2"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export2.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "3"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export3.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "4"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export4.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "5"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export5.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "6"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export6.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "7"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export7.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "8"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export8.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                End Select
            Case "Local"
                Select Case lblStatusID.Text
                    Case "1"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local1.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "2"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local2.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "3"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local3.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                End Select
        End Select
        btnEdit.HRef = URL
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            'Case "Edit"
            '    Dim lblShipmentID As Label = e.Item.FindControl("lblShipmentID")
            '    Dim lblShipmentCode As Label = e.Item.FindControl("lblShipmentCode")
            '    Dim lblStatusID As Label = e.Item.FindControl("lblStatusID")
            '    Dim lblType As Label = e.Item.FindControl("lblType")
            '    Dim lblStatus As Label = e.Item.FindControl("lblStatus")

            'Select Case lblType.Text
            '    Case "Import"
            '        Select Case lblStatusID.Text
            '            Case "1"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import1.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "2"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import2.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "3"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import3.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "4"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import4.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "5"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import5.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "6"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import6.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '        End Select
            '    Case "Export"
            '        Select Case lblStatusID.Text
            '            Case "1"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export1.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "2"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export2.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "3"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export3.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "4"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export4.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "5"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export5.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "6"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export6.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "7"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export7.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "8"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export8.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '        End Select
            '    Case "Local"
            '        Select Case lblStatusID.Text
            '            Case "1"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local1.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "2"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local2.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '            Case "3"
            '                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local3.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
            '        End Select
            'End Select

            Case "Delete"
                Dim lblType As Label = e.Item.FindControl("lblType")
                Dim lblShipmentID As Label = e.Item.FindControl("lblShipmentID")
                Dim SQL As String = ""
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                Select Case lblType.Text
                    Case "Import"
                        SQL &= "SELECT FILE_NAME_NEW FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "UNION ALL" & vbNewLine
                        SQL &= "SELECT FILE_NAME_NEW FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "UNION ALL" & vbNewLine
                        SQL &= "SELECT FILE_NAME_NEW FROM IMPORT_COMPLETE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        DA = New SqlDataAdapter(SQL, ConnStr)
                        DT = New DataTable
                        DA.Fill(DT)
                        If DT.Rows.Count > 0 Then
                            For i As Int32 = 0 To DT.Rows.Count - 1
                                Dim Pathfile As String = Server.MapPath("~/Upload/" + DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                                If File.Exists(pathfile) Then
                                    File.Delete(pathfile)
                                End If
                            Next
                        End If
                        SQL = ""
                        SQL &= "DELETE FROM IMPORT_COMPLETE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_COMPLETE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_CUSTOMS WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_DELIVER WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_PO WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_PREPARE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_RECEIVE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_STORE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM IMPORT_VEHICLE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM SHIPMENT WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        Dim conn As New SqlConnection(ConnStr)
                        conn.Open()
                        Dim Cmd As SqlCommand
                        Cmd = New SqlCommand(SQL, conn)
                        Cmd.ExecuteNonQuery()
                        conn.Close()
                    Case "Export"
                        SQL &= "SELECT FILE_NAME_NEW FROM EXPORT_RELEASE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        DA = New SqlDataAdapter(SQL, ConnStr)
                        DT = New DataTable
                        DA.Fill(DT)
                        If DT.Rows.Count > 0 Then
                            For i As Int32 = 0 To DT.Rows.Count - 1
                                Dim Pathfile As String = Server.MapPath("~/Upload/" + DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                                If File.Exists(Pathfile) Then
                                    File.Delete(Pathfile)
                                End If
                            Next
                        End If

                        SQL = ""
                        SQL &= "DELETE FROM EXPORT_ACCOUNTING WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_ALERT WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_BOOKING WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_PO WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_PREPARE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_RECEIVE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_RELEASE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_RELEASE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_STORE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_SUBMIT WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM EXPORT_VEHICLE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM SHIPMENT WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        Dim conn As New SqlConnection(ConnStr)
                        conn.Open()
                        Dim Cmd As SqlCommand
                        Cmd = New SqlCommand(SQL, conn)
                        Cmd.ExecuteNonQuery()
                        conn.Close()
                    Case "Local"
                        SQL &= "SELECT FILE_NAME_NEW FROM LOCAL_RELEASE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "UNION ALL" & vbNewLine
                        SQL &= "SELECT FILE_NAME_NEW FROM LOCAL_STORE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        DA = New SqlDataAdapter(SQL, ConnStr)
                        DT = New DataTable
                        DA.Fill(DT)
                        If DT.Rows.Count > 0 Then
                            For i As Int32 = 0 To DT.Rows.Count - 1
                                Dim Pathfile As String = Server.MapPath("~/Upload/" + DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                                If File.Exists(Pathfile) Then
                                    File.Delete(Pathfile)
                                End If
                            Next
                        End If

                        SQL = ""
                        SQL &= "DELETE FROM LOCAL_PO WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM LOCAL_RECEIVE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM LOCAL_RELEASE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM LOCAL_RELEASE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM LOCAL_STORE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM LOCAL_STORE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM LOCAL_VEHICLE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        SQL &= "DELETE FROM SHIPMENT WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                        Dim conn As New SqlConnection(ConnStr)
                        conn.Open()
                        Dim Cmd As SqlCommand
                        Cmd = New SqlCommand(SQL, conn)
                        Cmd.ExecuteNonQuery()
                        conn.Close()
                End Select
                BindData()
            Case "View"
                Dim lblType As Label = e.Item.FindControl("lblType")
                Dim lblShipmentCode As Label = e.Item.FindControl("lblShipmentCode")
                Select Case lblType.Text
                    Case "Import"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import_View.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "Export"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export_View.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                    Case "Local"
                        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local_View.aspx?ShipmentCode=" & lblShipmentCode.Text & "';", True)
                End Select

        End Select
    End Sub

    Sub ClearForm()
        ddlBranch.SelectedIndex = 0
        ddlCompany.SelectedIndex = 0
        For i As Int32 = 0 To cblShipmentType.Items.Count - 1
            cblShipmentType.Items(i).Selected = True
        Next
        ddlStatus.SelectedIndex = 0
        ddlTransportationType.SelectedIndex = 0
        ddlPortOfDestination.SelectedIndex = 0
        ddlPending.SelectedIndex = 0
        txtWorkOrderNo.Text = ""
        txtPONo.Text = ""
        txtInvoiceNo.Text = ""
        txtHAWB.Text = ""
        txtPreAlertFrom.Text = ""
        txtPreAlertTo.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearForm()
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        FilterData()
    End Sub

    Protected Sub btn_N_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_N_Cancel.Click
        divNewShipment.Visible = False
        ddl_N_Branch.SelectedIndex = 0
        ddl_N_Company.SelectedIndex = 0
        ddl_N_ShipmentType.SelectedIndex = 0
        txt_N_Branch.Text = ""
        txt_N_ShipmentType.Text = ""
        txt_N_WorkOrderNo.Text = ""
    End Sub

    Protected Sub btnNewShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewShipment.Click
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = "SELECT MS_USER_BRANCH.BRANCH_ID,BRANCH_NAME,BRANCH_CODE FROM MS_USER_BRANCH LEFT JOIN MS_BRANCH ON MS_USER_BRANCH.BRANCH_ID = MS_BRANCH.BRANCH_ID WHERE USER_ID = " & Session("USER_ID") & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 1 Then
            ddl_N_Branch.Visible = True
            txt_N_Branch.Visible = False
            lblBranch.Text = DT.Rows(0).Item("BRANCH_CODE").ToString
        Else
            ddl_N_Branch.SelectedValue = DT.Rows(0).Item("BRANCH_ID").ToString
            ddl_N_Branch.Visible = False
            txt_N_Branch.Visible = True
            txt_N_Branch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblBranch.Text = DT.Rows(0).Item("BRANCH_CODE").ToString
        End If

        SQL = "SELECT MS_USER_SHIPMENT_TYPE.SHIPMENT_TYPE_ID,SHIPMENT_TYPE_CODE,SHIPMENT_TYPE_NAME FROM MS_USER_SHIPMENT_TYPE LEFT JOIN MS_SHIPMENT_TYPE ON MS_USER_SHIPMENT_TYPE.SHIPMENT_TYPE_ID = MS_SHIPMENT_TYPE.SHIPMENT_TYPE_ID WHERE USER_ID = " & Session("USER_ID") & vbNewLine
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 1 Then
            ddl_N_ShipmentType.Visible = True
            txt_N_ShipmentType.Visible = False
            lblShipmentType.Text = DT.Rows(0).Item("SHIPMENT_TYPE_CODE").ToString
        ElseIf DT.Rows.Count = 1 Then
            ddl_N_ShipmentType.SelectedValue = DT.Rows(0).Item("SHIPMENT_TYPE_ID").ToString
            ddl_N_ShipmentType.Visible = False
            txt_N_ShipmentType.Visible = True
            txt_N_ShipmentType.Text = DT.Rows(0).Item("SHIPMENT_TYPE_NAME").ToString
            lblShipmentType.Text = DT.Rows(0).Item("SHIPMENT_TYPE_CODE").ToString
        End If

        SQL = "SELECT TRANSPORT_TYPE_CODE FROM MS_TRANSPORT_TYPE WHERE TRANSPORT_TYPE_ID = " & ddl_N_TransportationType.SelectedValue.ToString
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        lblTransportationType.Text = DT.Rows(0).Item("TRANSPORT_TYPE_CODE").ToString
        txt_N_WorkOrderNo.Text = GetWorkOrderSample()
        divNewShipment.Visible = True
    End Sub

    Function GetShipmentCode() As String
        Dim ShipmentCode As String = ""
        Dim YY As Integer
        YY = Now.Year
        If YY > 2500 Then
            YY = YY - 543
        End If
        ShipmentCode = YY.ToString.Substring(2, 2) & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
        Return ShipmentCode
    End Function

    Protected Sub btn_N_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_N_OK.Click
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable

        Dim YY As Integer
        YY = Now.Year
        If YY > 2500 Then
            YY = YY - 543
        End If
        Dim AutoNumber As String = ""
        Dim WO As String = ""

        SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_TYPE_ID = " & ddl_N_ShipmentType.SelectedValue.ToString & " AND TRANSPORT_TYPE_ID = " & ddl_N_TransportationType.SelectedValue.ToString & " AND MONTH(CREATE_DATE) = " & Date.Now.Month & " AND YEAR(CREATE_DATE) = " & YY
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        AutoNumber = CStr(DT.Rows.Count + 1).ToString.PadLeft(5, "0")
        WO = GetWorkOrderSample.Replace("-----", "") & AutoNumber
        Dim ShipmentID As Integer = GL.FindID("SHIPMENT", "SHIPMENT_ID")
        Dim ShipmentCode As String = GetShipmentCode()
        Dim DR As DataRow = DT.NewRow
        DR("SHIPMENT_ID") = ShipmentID
        DR("SHIPMENT_CODE") = ShipmentCode
        DR("BRANCH_ID") = ddl_N_Branch.SelectedValue.ToString
        DR("BRANCH_CODE") = lblBranch.Text
        DR("SHIPMENT_TYPE_ID") = ddl_N_ShipmentType.SelectedValue.ToString
        DR("TRANSPORT_TYPE_ID") = ddl_N_TransportationType.SelectedValue.ToString
        DR("TRANSPORT_TYPE_CODE") = lblTransportationType.Text
        DR("COMPANY_ID") = ddl_N_Company.SelectedValue.ToString
        DR("WORK_ORDER_NO") = WO
        DR("STATUS_ID") = "1"
        DR("ACTIVE_STATUS") = 1
        DR("CREATE_BY") = Session("User_ID")
        DR("CREATE_DATE") = Now
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        SQL = "SELECT SHIPMENT_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        Session("SHIPMENT_ID") = DT.Rows(0).Item("SHIPMENT_ID").ToString
        Select Case ddl_N_ShipmentType.SelectedItem.Text
            Case "Import"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import1.aspx?ShipmentCode=" & ShipmentCode & "';", True)
            Case "Export"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Export1.aspx?ShipmentCode=" & ShipmentCode & "';", True)

        End Select
    End Sub

    Protected Sub ddl_N_TransportationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_N_TransportationType.SelectedIndexChanged
        Dim SQL As String = ""
        SQL = "SELECT TRANSPORT_TYPE_CODE FROM MS_TRANSPORT_TYPE WHERE TRANSPORT_TYPE_ID = " & ddl_N_TransportationType.SelectedValue.ToString
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        lblTransportationType.Text = DT.Rows(0).Item("TRANSPORT_TYPE_CODE").ToString
        txt_N_WorkOrderNo.Text = GetWorkOrderSample()
    End Sub
    Protected Sub ddl_N_Branch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_N_Branch.SelectedIndexChanged
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_CODE FROM MS_BRANCH WHERE BRANCH_ID = " & ddl_N_Branch.SelectedValue.ToString
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        lblBranch.Text = DT.Rows(0).Item("BRANCH_CODE").ToString
        txt_N_WorkOrderNo.Text = GetWorkOrderSample()
    End Sub
    Protected Sub ddl_N_ShipmentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_N_ShipmentType.SelectedIndexChanged
        Dim SQL As String = ""
        SQL = "SELECT SHIPMENT_TYPE_CODE FROM MS_SHIPMENT_TYPE WHERE SHIPMENT_TYPE_ID = " & ddl_N_ShipmentType.SelectedValue.ToString
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        lblShipmentType.Text = DT.Rows(0).Item("SHIPMENT_TYPE_CODE").ToString
        txt_N_WorkOrderNo.Text = GetWorkOrderSample()
    End Sub

    Protected Sub cblShipmentType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cblShipmentType.SelectedIndexChanged
        Dim ShipmentType As String = ""
        For i As Int32 = 0 To cblShipmentType.Items.Count - 1
            If cblShipmentType.Items(i).Selected = True Then
                ShipmentType = ShipmentType & cblShipmentType.Items(i).Value.ToString & ","
            End If
        Next
        If ShipmentType <> "" Then ShipmentType = ShipmentType.Substring(0, ShipmentType.Length - 1)
        MC.BindDDlStatus(ddlStatus, "-------- All --------", "", ShipmentType)
    End Sub

    Function GetWorkOrderSample() As String
        Dim YY As Integer
        YY = Now.Year
        If YY > 2500 Then
            YY = YY - 543
        End If
        Return lblBranch.Text & lblShipmentType.Text & lblTransportationType.Text & YY.ToString.Substring(2, 2) & Now.Month.ToString.PadLeft(2, "0") & "-----"
    End Function
    'Protected Sub btn_L_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_L_OK.Click
    '    If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
    '        Exit Sub
    '    End If

    '    Dim YY As Integer
    '    YY = Now.Year
    '    If YY > 2500 Then
    '        YY = YY - 543
    '    End If
    '    Dim AutoNumber As String = ""
    '    Dim WO As String = ""
    '    Dim SQL As String = ""
    '    SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_TYPE_ID = 3 AND MONTH(CREATE_DATE) = " & Date.Now.Month & " AND YEAR(CREATE_DATE) = " & YY
    '    Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '    Dim DT As New DataTable
    '    DA.Fill(DT)
    '    AutoNumber = CStr(DT.Rows.Count + 1).ToString.PadLeft(4, "0")
    '    WO = "BKLX" & YY.ToString.Substring(2, 2) & Now.Month.ToString.PadLeft(2, "0") & AutoNumber
    '    Dim ShipmentID As String = GL.FindID("SHIPMENT", "SHIPMENT_ID")
    '    Dim ShipmentCode As String = GetShipmentCode(ShipmentID)
    '    Dim DR As DataRow = DT.NewRow
    '    DR("SHIPMENT_ID") = ShipmentID
    '    DR("SHIPMENT_CODE") = ShipmentCode
    '    DR("SHIPMENT_TYPE_ID") = 3
    '    DR("COMPANY_ID") = ddl_N_Company.SelectedValue.ToString
    '    DR("WORK_ORDER_NO") = WO
    '    DR("REF_LOT_NO") = txt_L_RefWorkOrderNo.Text
    '    DR("STATUS_ID") = "1"
    '    DR("ACTIVE_STATUS") = 1
    '    DR("CREATE_BY") = Session("User_ID")
    '    DR("CREATE_DATE") = Now
    '    DT.Rows.Add(DR)
    '    Dim cmd As New SqlCommandBuilder(DA)
    '    DA.Update(DT)

    '    If txt_L_RefWorkOrderNo.Text <> "" Then
    '        Dim DT_WO As New DataTable
    '        SQL = "SELECT * FROM WMS_WO WHERE inv_Lot_No = '" & txt_L_RefWorkOrderNo.Text.Replace("'", "''") & "'"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DA.Fill(DT_WO)
    '        Dim Consignee As String = DT_WO.Rows(0).Item("Consignee").ToString.Replace("'", "''")
    '        Dim Supplier As String = DT_WO.Rows(0).Item("Supplier").ToString.Replace("'", "''")
    '        Dim Delivery As String = DT_WO.Rows(0).Item("Place_Of_Delivery").ToString.Replace("'", "''")
    '        Dim ConsigneeID As Int32 = 0
    '        Dim SupplierID As Int32 = 0
    '        Dim DeliveryID As Int32 = 0
    '        Dim FindID As Int32 = 0

    '        'Consignee
    '        SQL = "select * from MS_PROJECT_OWNER where PROJECT_OWNER_NAME = '" & Consignee & "'"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            DT.Rows.Clear()
    '            DR = DT.NewRow
    '            FindID = GL.FindID("MS_PROJECT_OWNER", "PROJECT_OWNER_ID")
    '            DR("PROJECT_OWNER_ID") = FindID
    '            DR("PROJECT_OWNER_NAME") = Consignee
    '            DR("ACTIVE_STATUS") = 1
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            cmd = New SqlCommandBuilder(DA)
    '            DA.Update(DT)
    '            ConsigneeID = FindID
    '        Else
    '            ConsigneeID = DT.Rows(0).Item("PROJECT_OWNER_ID").ToString
    '        End If

    '        'Supplier
    '        SQL = "select * from MS_SUPPLIER where SUPPLIER_NAME = '" & Supplier & "'"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            DT.Rows.Clear()
    '            DR = DT.NewRow
    '            FindID = GL.FindID("MS_SUPPLIER", "SUPPLIER_ID")
    '            DR("SUPPLIER_ID") = FindID
    '            DR("SUPPLIER_NAME") = Supplier
    '            DR("ACTIVE_STATUS") = 1
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            cmd = New SqlCommandBuilder(DA)
    '            DA.Update(DT)
    '            SupplierID = FindID
    '        Else
    '            SupplierID = DT.Rows(0).Item("SUPPLIER_ID").ToString
    '        End If

    '        'Place_Of_Delivery
    '        SQL = "select * from MS_PLACE_OF_DELIVERY where DELIVERY_NAME = '" & Delivery & "'"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            DT.Rows.Clear()
    '            DR = DT.NewRow
    '            FindID = GL.FindID("MS_PLACE_OF_DELIVERY", "DELIVERY_ID")
    '            DR("DELIVERY_ID") = FindID
    '            DR("DELIVERY_NAME") = Delivery
    '            DR("ACTIVE_STATUS") = 1
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            cmd = New SqlCommandBuilder(DA)
    '            DA.Update(DT)
    '            DeliveryID = ID
    '        Else
    '            DeliveryID = DT.Rows(0).Item("DELIVERY_ID").ToString
    '        End If

    '        'LOCAL_RECEIVE
    '        SQL = "select * from LOCAL_RECEIVE where SHIPMENT_ID = 0"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        DR = DT.NewRow
    '        DR("SHIPMENT_ID") = ShipmentID
    '        DR("RECEIVE_DATE") = DT_WO.Rows(0).Item("Receive_Date")
    '        DR("PROJECT_OWNER_ID") = ConsigneeID
    '        DR("SUPPLIER_ID") = SupplierID
    '        DR("QTY") = DT_WO.Rows(0).Item("No_Of_Package").ToString
    '        DR("WEIGHT") = DT_WO.Rows(0).Item("Weight").ToString
    '        DR("CREATE_DATE") = Now
    '        DT.Rows.Add(DR)
    '        cmd = New SqlCommandBuilder(DA)
    '        DA.Update(DT)

    '        'LOCAL_STORE
    '        SQL = "select * from LOCAL_STORE where SHIPMENT_ID = 0"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        DR = DT.NewRow
    '        DR("SHIPMENT_ID") = ShipmentID
    '        DR("PICK_DATE") = DT_WO.Rows(0).Item("Pickup_Date")
    '        DR("CREATE_DATE") = Now
    '        DT.Rows.Add(DR)
    '        cmd = New SqlCommandBuilder(DA)
    '        DA.Update(DT)

    '        'LOCAL_RELEASE
    '        SQL = "select * from LOCAL_RELEASE where SHIPMENT_ID = 0"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        DR = DT.NewRow
    '        DR("SHIPMENT_ID") = ShipmentID
    '        DR("DELIVERY_DATE") = DT_WO.Rows(0).Item("Delivery_Date")
    '        DR("DELIVERY_ID") = DeliveryID
    '        DR("REF_DOC") = DT_WO.Rows(0).Item("Reference_Doc").ToString
    '        DR("COMMENT") = DT_WO.Rows(0).Item("Remark").ToString
    '        DR("CREATE_DATE") = Now
    '        DT.Rows.Add(DR)
    '        cmd = New SqlCommandBuilder(DA)
    '        DA.Update(DT)

    '        'PO
    '        SQL = "select * from WMS_PO where inv_Lot_No = '" & txt_L_RefWorkOrderNo.Text.Replace("'", "''") & "'"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count > 0 Then
    '            For i As Int32 = 0 To DT.Rows.Count - 1
    '                SQL = "select * from LOCAL_PO where SHIPMENT_ID = 0"
    '                Dim DA_T As New SqlDataAdapter(SQL, ConnStr)
    '                Dim DT_T As New DataTable
    '                DA_T.Fill(DT_T)
    '                DR = DT_T.NewRow
    '                DR("SHIPMENT_ID") = ShipmentID
    '                DR("PO") = DT.Rows(i).Item("PO_NO").ToString
    '                DR("SORT") = i + 1
    '                DT_T.Rows.Add(DR)
    '                cmd = New SqlCommandBuilder(DA_T)
    '                DA_T.Update(DT_T)
    '            Next
    '        End If

    '        'VEHICLE
    '        SQL = "select * from WMS_VEHICLE where inv_Lot_No = '" & txt_L_RefWorkOrderNo.Text.Replace("'", "''") & "'"
    '        DA = New SqlDataAdapter(SQL, ConnStr)
    '        DT = New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count > 0 Then
    '            For i As Int32 = 0 To DT.Rows.Count - 1
    '                SQL = "select * from LOCAL_VEHICLE where SHIPMENT_ID = 0"
    '                Dim DA_T As New SqlDataAdapter(SQL, ConnStr)
    '                Dim DT_T As New DataTable
    '                DA_T.Fill(DT_T)
    '                DR = DT_T.NewRow
    '                DR("SHIPMENT_ID") = ShipmentID
    '                DR("TYPE_OF_VEHICLE_ID") = DT.Rows(i).Item("TYPE_OF_VEHICLE_ID").ToString
    '                DR("AMOUNT") = DT.Rows(i).Item("Amount").ToString
    '                DR("LICENSE_PLATE") = DT.Rows(i).Item("truck_license").ToString
    '                DT_T.Rows.Add(DR)
    '                cmd = New SqlCommandBuilder(DA_T)
    '                DA_T.Update(DT_T)
    '            Next
    '        End If
    '    End If

    '    Session("SHIPMENT_ID") = ShipmentID
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Local1.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    'End Sub
End Class

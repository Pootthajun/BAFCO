﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_Permission
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlUserType(ddlType, "----- Select -----")
            BindData()
        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        MC.BindDDlUserType(ddlType, "----- Select -----")
        BindData()
    End Sub

    Private Sub BindData()
        Dim DT As New DataTable
        Dim DR As DataRow
        DT.Columns.Add("No")
        DT.Columns.Add("MENU_ID")
        DT.Columns.Add("MENU_NAME")
        DT.Columns.Add("Active", GetType(Boolean))
        DT.Columns.Add("NewShipment", GetType(Boolean))
        DT.Columns.Add("ExportData", GetType(Boolean))
        DT.Columns.Add("Edit", GetType(Boolean))
        DT.Columns.Add("Delete", GetType(Boolean))
        DT.Columns.Add("EditAllShipment", GetType(Boolean))

        Dim SQL As String = "SELECT * FROM MS_MENU ORDER BY SORT"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_ As New DataTable
        DA.Fill(DT_)
        For i As Int32 = 0 To DT_.Rows.Count - 1
            DR = DT.NewRow
            DR("No") = i + 1
            DR("MENU_ID") = DT_.Rows(i).Item("MENU_ID").ToString
            DR("MENU_NAME") = DT_.Rows(i).Item("MENU_NAME").ToString
            DR("Active") = False
            DR("NewShipment") = False
            DR("ExportData") = False
            DR("Edit") = False
            DR("Delete") = False
            DR("EditAllShipment") = False
            DT.Rows.Add(DR)
        Next
        rptData.DataSource = DT
        rptData.DataBind()
        
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblMenuID As Label = e.Item.FindControl("lblMenuID")
        Dim lblMenuName As Label = e.Item.FindControl("lblMenuName")
        Dim cbActive As CheckBox = e.Item.FindControl("cbActive")
        Dim cbNewShipment As CheckBox = e.Item.FindControl("cbNewShipment")
        Dim cbExportData As CheckBox = e.Item.FindControl("cbExportData")
        Dim cbEdit As CheckBox = e.Item.FindControl("cbEdit")
        Dim cbDelete As CheckBox = e.Item.FindControl("cbDelete")
        Dim cbEditAllShipment As CheckBox = e.Item.FindControl("cbEditAllShipment")

        lblNo.Text = e.Item.DataItem("No").ToString
        lblMenuID.Text = e.Item.DataItem("MENU_ID").ToString
        lblMenuName.Text = e.Item.DataItem("MENU_NAME").ToString
        If e.Item.DataItem("Active").ToString.ToUpper = "TRUE" Then
            cbActive.Checked = True
        Else
            cbActive.Checked = False
        End If

        If e.Item.DataItem("MENU_ID") < 3 Then
            If e.Item.DataItem("NewShipment").ToString.ToUpper = "TRUE" Then
                cbNewShipment.Checked = True
            Else
                cbNewShipment.Checked = False
            End If
            If e.Item.DataItem("ExportData").ToString.ToUpper = "TRUE" Then
                cbExportData.Checked = True
            Else
                cbExportData.Checked = False
            End If
            If e.Item.DataItem("Edit").ToString.ToUpper = "TRUE" Then
                cbEdit.Checked = True
            Else
                cbEdit.Checked = False
            End If
            If e.Item.DataItem("Delete").ToString.ToUpper = "TRUE" Then
                cbDelete.Checked = True
            Else
                cbDelete.Checked = False
            End If
            If e.Item.DataItem("EditAllShipment").ToString.ToUpper = "TRUE" Then
                cbEditAllShipment.Checked = True
            Else
                cbEditAllShipment.Checked = False
            End If
        ElseIf e.Item.DataItem("MENU_ID") = 3 Then
            cbNewShipment.Visible = False
            cbDelete.Visible = False
            cbEditAllShipment.Visible = False
            If e.Item.DataItem("ExportData").ToString.ToUpper = "TRUE" Then
                cbExportData.Checked = True
            Else
                cbExportData.Checked = False
            End If
            If e.Item.DataItem("Edit").ToString.ToUpper = "TRUE" Then
                cbEdit.Checked = True
            Else
                cbEdit.Checked = False
            End If
        Else
            cbNewShipment.Visible = False
            cbExportData.Visible = False
            cbEdit.Visible = False
            cbDelete.Visible = False
            cbEditAllShipment.Visible = False
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ddlType.SelectedIndex = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select User Type');", True)
            Exit Sub
        End If

        Dim Sql As String = ""
        Sql = "DELETE FROM MS_USER_TYPE_MENU WHERE USER_TYPE_ID = " & ddlType.SelectedValue.ToString
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim Cmd As SqlCommand
        Cmd = New SqlCommand(Sql, conn)
        Cmd.ExecuteNonQuery()
        conn.Close()

        Sql = "SELECT * FROM MS_USER_TYPE_MENU WHERE 1=0"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        Dim DT_MENU As New DataTable
        DT_MENU = GetDataDetail()

        For i As Int32 = 0 To DT_MENU.Rows.Count - 1
            Dim DR As DataRow = DT.NewRow
            DR("USER_TYPE_ID") = ddlType.SelectedValue.ToString
            DR("MENU_ID") = DT_MENU.Rows(i).Item("MENU_ID").ToString
            DR("MENU_NAME") = DT_MENU.Rows(i).Item("MENU_NAME").ToString
            DR("Active") = DT_MENU.Rows(i).Item("Active").ToString
            DR("NewShipment") = DT_MENU.Rows(i).Item("NewShipment").ToString
            DR("ExportData") = DT_MENU.Rows(i).Item("ExportData").ToString
            DR("Edit") = DT_MENU.Rows(i).Item("Edit").ToString
            DR("Delete") = DT_MENU.Rows(i).Item("Delete").ToString
            DR("EditAllShipment") = DT_MENU.Rows(i).Item("EditAllShipment").ToString
            DT.Rows.Add(DR)
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)
        Next

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

    Function GetDataDetail() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("No")
        DT.Columns.Add("MENU_ID")
        DT.Columns.Add("MENU_NAME")
        DT.Columns.Add("Active", GetType(Boolean))
        DT.Columns.Add("NewShipment", GetType(Boolean))
        DT.Columns.Add("ExportData", GetType(Boolean))
        DT.Columns.Add("Edit", GetType(Boolean))
        DT.Columns.Add("Delete", GetType(Boolean))
        DT.Columns.Add("EditAllShipment", GetType(Boolean))

        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblNo As Label = ri.FindControl("lblNo")
            Dim lblMenuID As Label = ri.FindControl("lblMenuID")
            Dim lblMenuName As Label = ri.FindControl("lblMenuName")
            Dim cbActive As CheckBox = ri.FindControl("cbActive")
            Dim cbNewShipment As CheckBox = ri.FindControl("cbNewShipment")
            Dim cbExportData As CheckBox = ri.FindControl("cbExportData")
            Dim cbEdit As CheckBox = ri.FindControl("cbEdit")
            Dim cbDelete As CheckBox = ri.FindControl("cbDelete")
            Dim cbEditAllShipment As CheckBox = ri.FindControl("cbEditAllShipment")
            Dim DR As DataRow = DT.NewRow
            DR = DT.NewRow
            DR("No") = lblNo.Text
            DR("MENU_ID") = lblMenuID.Text
            DR("MENU_NAME") = lblMenuName.Text
            DR("Active") = cbActive.Checked
            DR("NewShipment") = cbNewShipment.Checked
            DR("ExportData") = cbExportData.Checked
            DR("Edit") = cbEdit.Checked
            DR("Delete") = cbDelete.Checked
            DR("EditAllShipment") = cbEditAllShipment.Checked
            DT.Rows.Add(DR)
        Next
        Return DT

    End Function

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedIndex > 0 Then
            Dim SQL As String = ""
            SQL = "SELECT 0 AS No,MS_MENU.MENU_ID,MS_MENU.MENU_NAME,Active,NewShipment,ExportData,Edit,[Delete],EditAllShipment FROM MS_MENU LEFT JOIN (SELECT * FROM MS_USER_TYPE_MENU WHERE USER_TYPE_ID = " & ddlType.SelectedValue.ToString & " ) US ON MS_MENU.MENU_ID = US.MENU_ID ORDER BY SORT"
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                For i As Int32 = 0 To DT.Rows.Count - 1
                    DT.Rows(i).Item("No") = i + 1
                Next
                rptData.DataSource = DT
                rptData.DataBind()
                Exit Sub
            End If
        End If
        BindData()
    End Sub
End Class

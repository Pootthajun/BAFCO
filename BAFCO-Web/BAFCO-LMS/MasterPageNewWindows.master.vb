﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class MasterPageNewWindows
    Inherits System.Web.UI.MasterPage

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsNothing(Session("User_ID")) AndAlso Session("User_ID") <> "" Then
            divImage.Visible = False
        Else
            divImage.Visible = True
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim SQL As String = ""
        SQL = "SELECT * FROM MS_USER WHERE USER_CODE='" & txtUsername.Value.ToString.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            SQL = "SELECT * FROM MS_COMPANY WHERE COMPANY_CODE='" & txtUsername.Value.ToString.Replace("'", "''") & "'"
            DA = New SqlDataAdapter(SQL, ConnStr)
            Dim DT_ As New DataTable
            DA.Fill(DT_)
            If DT_.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid username');", True)
                txtUsername.Focus()
                Exit Sub
            End If

            If DT_.Rows(0).Item("COMPANY_PASSWORD") <> txtPassword.Value.ToString Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid password');", True)
                txtPassword.Focus()
                Exit Sub
            End If

            If Not DT_.Rows(0).Item("ACTIVE_STATUS") Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This username is unavailable');", True)
                txtUsername.Focus()
                Exit Sub
            End If

            Session("User_Type_ID") = 0
            Session("User_ID") = DT_.Rows(0).Item("COMPANY_ID").ToString
            Session("User_Fullname") = DT_.Rows(0).Item("COMPANY_NAME").ToString
            divImage.Visible = False
            Exit Sub
        End If

        If DT.Rows(0).Item("USER_PASSWORD") <> txtPassword.Value.ToString Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid password');", True)
            txtPassword.Focus()
            Exit Sub
        End If

        If Not DT.Rows(0).Item("ACTIVE_STATUS") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This username is unavailable');", True)
            txtUsername.Focus()
            Exit Sub
        End If
        Session("User_Type_ID") = DT.Rows(0).Item("User_Type_ID").ToString
        Session("User_ID") = DT.Rows(0).Item("User_ID").ToString
        Session("User_Fullname") = DT.Rows(0).Item("User_FULLNAME").ToString
        divImage.Visible = False
    End Sub

End Class


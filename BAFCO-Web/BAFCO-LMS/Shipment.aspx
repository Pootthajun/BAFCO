<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Shipment.aspx.vb" Inherits="Shipment"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
        <ContentTemplate>
        <div id="divNewShipment" runat="server" style="position:fixed; top:0px; left:0px; width:100%; height:100%; z-index:1;" visible="False">                    
            <div style="width:100%; height:100%; position:absolute; background-color:Gray; filter:alpha(opacity=80); opacity: 0.80;" ></div>
            <div style="left:39.4%; top:20%; position:absolute; background-color:#F6F3F3; padding: 20px 20px 20px 20px; width:270px; height:220px; ">
                <table width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td height="40" align="center" class="Master_Header_Text">
                            New Shipment
                        </td>                
                    </tr>
                </table>
                
                <table width="100%" border="0" cellpadding="0" cellspacing="1" style="padding-top:10px; padding-bottom:10px;">
                    <tr>
                        <td width="140" height="25" class="NormalTextBlack">
                            Branch
                        </td>
                        <td width="145" height="25">
                            <asp:TextBox ID="txt_N_Branch" runat="server" CssClass="Textbox_Form_Disable" width="145px" ReadOnly ="true"></asp:TextBox>
                            <asp:DropDownList ID="ddl_N_Branch" runat="server" CssClass="Dropdown_Form_White" width="150px" AutoPostBack="true"></asp:DropDownList>
                            <asp:Label ID="lblBranch" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                        <td></td>               
                    </tr>
                    <tr>
                        <td width="140" height="25" class="NormalTextBlack">
                            Shipment Type
                        </td>
                        <td width="145" height="25">
                            <asp:TextBox ID="txt_N_ShipmentType" runat="server" CssClass="Textbox_Form_Disable" width="145px" ReadOnly ="true"></asp:TextBox>
                            <asp:DropDownList ID="ddl_N_ShipmentType" runat="server" CssClass="Dropdown_Form_White" width="150px" AutoPostBack="true"></asp:DropDownList>
                            <asp:Label ID="lblShipmentType" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                        <td></td>               
                    </tr>
                    <tr>
                        <td width="140" height="25" class="NormalTextBlack">
                            Company Name
                        </td>
                        <td width="145" height="25">
                            <asp:DropDownList ID="ddl_N_Company" runat="server" CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                            <asp:Label ID="lblCompany" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                        <td></td>               
                    </tr>
                    <tr>
                        <td width="140" height="25" class="NormalTextBlack">
                            Transportation Type
                        </td>
                        <td width="145" height="25">
                            <asp:DropDownList ID="ddl_N_TransportationType" runat="server" CssClass="Dropdown_Form_White" width="150px" AutoPostBack="true"></asp:DropDownList>
                            <asp:Label ID="lblTransportationType" runat="server" Text="Label" Visible="false"></asp:Label>
                        </td>
                        <td></td>               
                    </tr>
                    <tr>
                        <td width="140" height="25" class="NormalTextBlack">
                            Work Order No
                        </td>
                        <td width="145" height="25">
                            <asp:TextBox ID="txt_N_WorkOrderNo" runat="server" CssClass="Textbox_Form_Disable" width="145px" MaxLength="30" Text="" ReadOnly ="true"></asp:TextBox>
                        </td>
                        <td></td>               
                    </tr>
                    <tr>
                        <td width="140" height="25"></td>
                        <td width="145" height="40" align="right" valign="bottom">
                            <asp:Button ID="btn_N_OK" runat="server" Text="OK" CssClass="Button_Red" Width="60px" Height="30px"/>
                            <asp:Button ID="btn_N_Cancel" runat="server" Text="Cancel" CssClass="Button_Red" Width="60px" Height="30px"/>
                        </td>
                        <td></td>               
                    </tr>
                </table>          
            </div>
        </div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">    
            <tr>
                <td valign="top">
                     <table align="left" cellpadding="0" cellspacing="1">
                        <tr>
                            <td>
                                <div class="Fieldset_Container" >
                                    <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                    <table width="950px" align="center" cellspacing="5px;" style="margin-left:10px; ">
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Branch
                                            </td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlBranch" runat="server" AutoPostBack="true"
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Shipment Type
                                            </td>
                                            <td width="145" height="25" colspan="6">
                                                <asp:CheckBoxList ID="cblShipmentType" runat="server" RepeatColumns="5" AutoPostBack="true">
                                                </asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Company
                                            </td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="true"
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Status
                                            </td>
                                            <td width="280" height="25" colspan="3">
                                                <asp:DropDownList ID="ddlStatus" runat="server" AutoPostBack="true"
                                                CssClass="Dropdown_Form_White" width="280px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" height="25" colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Work Order No
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtWorkOrderNo" runat="server" CssClass="Textbox_Form_White" width="145px"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                PO No
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtPONo" runat="server" CssClass="Textbox_Form_White" 
                                                    width="145px" ></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Invoice No
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="Textbox_Form_White" 
                                                width="145px" ></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Transportation Type
                                            </td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlTransportationType" runat="server" AutoPostBack="true"
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                HAWB/BL
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtHAWB" runat="server" CssClass="Textbox_Form_White" 
                                                    width="145px" ></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Port of Destination
                                            </td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlPortOfDestination" runat="server" AutoPostBack="true"
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Pending
                                            </td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlPending" runat="server" AutoPostBack="true"
                                                CssClass="Dropdown_Form_White" width="150px" >
                                                    <asp:ListItem Value="-1" Text="-------- All --------" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Pre-alert date From
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtPreAlertFrom" runat="server" CssClass="Textbox_Form_White" width="145px"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgPreAlertFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgPreAlertFrom_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgPreAlertFrom"
				                                TargetControlID="txtPreAlertFrom"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="120" height="25" class="NormalTextBlack">
                                                Pre-alert date To
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtPreAlertTo" runat="server" CssClass="Textbox_Form_White" width="145px"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgPreAlertTo" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgPreAlertTo_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgPreAlertTo"
				                                TargetControlID="txtPreAlertTo"></Ajax:CalendarExtender>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>       
                </td>
            </tr>
            <tr>
                <td height="20" valign="middle">
                    <table cellpadding="0" cellspacing="2" style="margin: 0px 0px 10px 10px; width:970px;">
                        <tr>
                            <td style="width:100px;">
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td style="width:100px;">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td style="width:100px;">
                                <asp:Button ID="btnNewShipment" runat="server" Text="New Shipment" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td align="right">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="1" style="margin: 0px 0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="30" class="Grid_Header">
                                No
                            </td>
                            <td width="130" class="Grid_Header">
                                Work Order
                            </td>
                            <td width="100" class="Grid_Header">
                                Company
                            </td>
                            <td width="250" class="Grid_Header">
                                Supplier
                            </td>
                            <td width="80" class="Grid_Header">
                                Type
                            </td>
                            <td width="200" class="Grid_Header">
                                Status
                            </td>
                            <td width="100" class="Grid_Header">
                                Action
                            </td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trRow" runat="server" style="border-bottom:solid 1px #efefef; cursor:pointer;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td class="Grid_Detail" align="center">
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        <asp:Label ID="lblShipmentID" runat="server" visible="false"></asp:Label>
                                        <asp:Label ID="lblShipmentCode" runat="server" visible="false"></asp:Label>
                                        <asp:Label ID="lblStatusID" runat="server" visible="false"></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblWorkOrder" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblCompany" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblSupplier" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" align="center">
                                        <asp:Label ID="lblType" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" align="center" valign="middle">
                                        <a ID="btnEdit" runat="server" target="_blank" >
                                            <img src="images/icon/10.png" height="25" alt="Edit" title="Edit" border="0" />
                                        </a>
                                        <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                        <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete">
                                        </Ajax:ConfirmButtonExtender>
                                        <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/75.png" Height="25px" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div style="margin-left:10px">
                        <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="15" Visible="false"/>
                    </div>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


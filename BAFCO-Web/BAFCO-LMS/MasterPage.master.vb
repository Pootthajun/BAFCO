﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class MasterPage
    Inherits System.Web.UI.MasterPage

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Protected Sub lnkSignout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSignout.Click
        lblUName.Text = ""
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Default.aspx';", True)
        Session.Abandon()
        divImage.Visible = True
    End Sub

    Protected Sub btnToggleLeftMenu_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnToggleLeftMenu.Click
        LeftMenu.Visible = Not LeftMenu.Visible
        Select Case LeftMenu.Visible
            Case True
                btnToggleLeftMenu.ImageUrl = "images/dbLeft.png"
                pnlLeftMenu.Style.Item("width") = "200px !important"
            Case False
                btnToggleLeftMenu.ImageUrl = "images/dbRight.png"
                pnlLeftMenu.Style.Item("width") = "18px"
        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        '--------------------- For Login From www.bafcothai.com-----------------
        If Not IsNothing(Request.QueryString("From")) AndAlso Request.QueryString("From").ToUpper = "WEBBAFCO" Then
            Dim U As String = ""
            If Not IsNothing(Request.Form("username")) Then
                U = Request.Form("username")
                txtUsername.Value = U
            End If
            Dim P As String = ""
            If Not IsNothing(Request.Form("password")) Then
                P = Request.Form("password")
                txtPassword.Value = Request.Form("password")
            End If

            If U <> "" Or P <> "" Then
                divImage.Visible = True
                btnLogin_Click(Nothing, Nothing)
            End If
        End If

        If Not IsPostBack Then
            PerMissionMenu()
            WriteHeaderPage()
        End If

        If Not IsNothing(Session("User_ID")) AndAlso Session("User_ID") <> "" Then
            divImage.Visible = False
        Else
            divImage.Visible = True
        End If
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim SQL As String = ""
        SQL = "SELECT * FROM MS_USER WHERE USER_CODE='" & txtUsername.Value.ToString.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            SQL = "SELECT * FROM MS_COMPANY WHERE COMPANY_CODE='" & txtUsername.Value.ToString.Replace("'", "''") & "'"
            DA = New SqlDataAdapter(SQL, ConnStr)
            Dim DT_ As New DataTable
            DA.Fill(DT_)
            If DT_.Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid username');", True)
                txtUsername.Focus()
                Exit Sub
            End If

            If DT_.Rows(0).Item("COMPANY_PASSWORD") <> txtPassword.Value.ToString Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid password');", True)
                txtPassword.Focus()
                Exit Sub
            End If

            If Not DT_.Rows(0).Item("ACTIVE_STATUS") Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This username is unavailable');", True)
                txtUsername.Focus()
                Exit Sub
            End If

            Session("User_Type_ID") = 0
            Session("User_ID") = DT_.Rows(0).Item("COMPANY_ID").ToString
            Session("User_Fullname") = DT_.Rows(0).Item("COMPANY_NAME").ToString
            divImage.Visible = False
            PerMissionMenu()
            LeftMenu.SelectedIndex = 0
            Exit Sub
        End If

        If DT.Rows(0).Item("USER_PASSWORD") <> txtPassword.Value.ToString Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid password');", True)
            txtPassword.Focus()
            Exit Sub
        End If

        If Not DT.Rows(0).Item("ACTIVE_STATUS") Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This username is unavailable');", True)
            txtUsername.Focus()
            Exit Sub
        End If
        Session("User_Type_ID") = DT.Rows(0).Item("User_Type_ID").ToString
        Session("User_ID") = DT.Rows(0).Item("User_ID").ToString
        Session("User_Fullname") = DT.Rows(0).Item("User_FULLNAME").ToString
        divImage.Visible = False
        WriteHeaderPage()
        PerMissionMenu()
        If LeftMenu.Panes("ADP_Shipment").Visible = True Then
            LeftMenu.SelectedIndex = 0
        End If

    End Sub

    Sub WriteHeaderPage()
        Select Case Me.Page.TemplateControl.ToString
            '   ------------- Shipment -----------
            Case "ASP." & "Shipment_Import.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Import"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Shipment_Export.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Export"
                LeftMenu.SelectedIndex = 0
            Case "ASP." & "Shipment_Local.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Local"
                LeftMenu.SelectedIndex = 0
                '   ------------- Import/Export Data -----------
            Case "ASP." & "ImportData.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Import Data"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(1)
            Case "ASP." & "ExportData.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Export Data"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(1)
                '---------------- Master Data ---------------
            Case "ASP." & "Master_TransportType.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Transport Type"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_UserType.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "User Type"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_User.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "User"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Permission.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Permission"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Branch.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Branch"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Company.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Company"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_PortOfDestination.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Port Of Destination"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_PortOfDeparture.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Port Of Departure"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_ProjectOwner.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Consignee"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Term.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Term"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Contractor.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Contractor"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Supplier.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Supplier"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_SectionBillTo.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Section Bill To"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_Currency.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Currency"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_UOM.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "UOM"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_TypeOfVehicle.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Type Of Vehicle"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_PlaceOfDelivery.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Place of Delivery"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case "ASP." & "Master_PickUpPlace.aspx".ToLower.Replace(".", "_")
                lblPageHeader.Text = "Pick Up Place"
                LeftMenu.SelectedIndex = GetMenuSelectedIndex(2)
            Case Else
                LeftMenu.RequireOpenedPane = False
                LeftMenu.SelectedIndex = -1
        End Select
    End Sub

    Function GetMenuSelectedIndex(ByVal DefaultSelectedIndex As Integer) As Integer
        Select Case DefaultSelectedIndex
            Case 0
                Return 0
            Case 1
                If LeftMenu.Panes("ADP_Shipment").Visible = True Then
                    Return 1
                Else
                    Return 0
                End If
            Case Else
                Dim Ret As Integer = 2
                If LeftMenu.Panes("ADP_Shipment").Visible = False Then
                    Ret = Ret - 1
                End If
                If LeftMenu.Panes("ADP_Import").Visible = False Then
                    Ret = Ret - 1
                End If
                Return Ret
        End Select
    End Function

    Sub PerMissionMenu()
        If Not IsNothing(Session("User_Fullname")) AndAlso Session("User_Fullname") <> "" Then
            lblUName.Text = Session("User_Fullname")

            If Session("User_Type_ID") = 0 Then
                LeftMenu.Panes("ADP_Shipment").Visible = True
                LeftMenu.Panes("ADP_Import").Visible = False
                LeftMenu.Panes("ADP_Setting").Visible = False
                Exit Sub
            End If

            '------ Permission Menu -----
            Dim sql As String = ""
            Dim MenuList As String = ""
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter
            sql = "SELECT MENU_NAME FROM MS_USER_TYPE_MENU WHERE active = 1 and USER_TYPE_ID = " & Session("User_Type_ID").ToString
            da = New SqlDataAdapter(sql, ConnStr)
            da.Fill(dt)

            For i As Int32 = 0 To dt.Rows.Count - 1
                If dt.Rows(i).Item("MENU_NAME").ToString = "User" Then
                    MenuList = MenuList & "User_;"
                Else
                    MenuList = MenuList & dt.Rows(i).Item("MENU_NAME").ToString & ";"
                End If

            Next

            Dim CountMenu As Int32 = 0
            '   ------------- Shipment -----------
            If InStr(MenuList, "Shipment Import") > 0 Then
                mnImport.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnImport.Visible = False
            End If

            If InStr(MenuList, "Shipment Export") > 0 Then
                mnExport.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnExport.Visible = False
            End If

            If InStr(MenuList, "Shipment Local") > 0 Then
                mnLocal.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnLocal.Visible = False
            End If

            If CountMenu > 0 Then
                LeftMenu.Panes("ADP_Shipment").Visible = True
            Else
                LeftMenu.Panes("ADP_Shipment").Visible = False
            End If

            '   ------------- Import Data -----------
            If InStr(MenuList, "Import Data") > 0 Then
                LeftMenu.Panes("ADP_Import").Visible = True
            Else
                LeftMenu.Panes("ADP_Import").Visible = False
            End If

            '*************** Setting ****************
            CountMenu = 0
            If InStr(MenuList, "Transport Type") > 0 Then
                mnTransportType.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnTransportType.Visible = False
            End If
            If InStr(MenuList, "User_") > 0 Then
                mnUser.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnUser.Visible = False
            End If
            If InStr(MenuList, "User Type") > 0 Then
                mnUserType.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnUserType.Visible = False
            End If
            If InStr(MenuList, "Permission") > 0 Then
                mnUserPermission.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnUserPermission.Visible = False
            End If
            If InStr(MenuList, "Branch") > 0 Then
                mnBranch.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnBranch.Visible = False
            End If
            If InStr(MenuList, "Company") > 0 Then
                mnCompany.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnCompany.Visible = False
            End If
            If InStr(MenuList, "Port of Destination") > 0 Then
                mnPortOfDestination.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnPortOfDestination.Visible = False
            End If
            If InStr(MenuList, "Port of Departure") > 0 Then
                mnPortOfDeparture.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnPortOfDeparture.Visible = False
            End If
            If InStr(MenuList, "Consignee") > 0 Then
                mnProjectOwner.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnProjectOwner.Visible = False
            End If
            If InStr(MenuList, "Term") > 0 Then
                mnTerm.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnTerm.Visible = False
            End If
            If InStr(MenuList, "Contractor") > 0 Then
                mnContractor.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnContractor.Visible = False
            End If
            If InStr(MenuList, "Supplier") > 0 Then
                mnSupplier.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnSupplier.Visible = False
            End If
            If InStr(MenuList, "Section Bill To") > 0 Then
                mnSectionBillTo.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnSectionBillTo.Visible = False
            End If
            If InStr(MenuList, "Currency") > 0 Then
                mnCurrency.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnCurrency.Visible = False
            End If
            If InStr(MenuList, "UOM") > 0 Then
                mnUOM.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnUOM.Visible = False
            End If
            If InStr(MenuList, "Type of Vehicle") > 0 Then
                mnTypeOfVehicle.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnTypeOfVehicle.Visible = False
            End If
            If InStr(MenuList, "Place of Delivery") > 0 Then
                mnPlaceOfDelivery.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnPlaceOfDelivery.Visible = False
            End If
            If InStr(MenuList, "Pick Up Place") > 0 Then
                mnPickUpPlace.Visible = True
                CountMenu = CountMenu + 1
            Else
                mnPickUpPlace.Visible = False
            End If

            If CountMenu > 0 Then
                LeftMenu.Panes("ADP_Setting").Visible = True
            Else
                LeftMenu.Panes("ADP_Setting").Visible = False
            End If
        End If
    End Sub

    'Protected Sub test_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles test.ServerClick
    '    Session.Clear()
    '    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Shipment.aspx';", True)
    'End Sub
End Class


﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_Supplier
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = "SELECT * FROM MS_SUPPLIER" & vbLf
        SQL &= " ORDER BY ACTIVE_STATUS DESC ,SUPPLIER_NAME ASC" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DT.Columns.Add("edit_mode", GetType(String), "'Edit'")
        DT.Columns.Add("view_mode", GetType(String), "'View'")
        '--------------- Filter ----------------------
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblId As Label = e.Item.FindControl("lblId")
        Dim lblName As Label = e.Item.FindControl("lblName")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        Dim txtId As TextBox = e.Item.FindControl("txtId")
        Dim txtName As TextBox = e.Item.FindControl("txtName")
        Dim ddlStatus As DropDownList = e.Item.FindControl("ddlStatus")

        Dim btnEdit As ImageButton = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")

        Dim btnSave As ImageButton = e.Item.FindControl("btnSave")
        Dim btnCancel As ImageButton = e.Item.FindControl("btnCancel")

        Dim trView As HtmlTableRow = e.Item.FindControl("trView")
        Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")


        lblNo.Text = e.Item.ItemIndex + 1
        lblId.Text = e.Item.DataItem("SUPPLIER_ID").ToString
        lblName.Text = e.Item.DataItem("SUPPLIER_NAME").ToString

        txtId.Text = e.Item.DataItem("SUPPLIER_ID").ToString
        txtName.Text = e.Item.DataItem("SUPPLIER_NAME").ToString

        If CBool(e.Item.DataItem("ACTIVE_STATUS")) = True Then
            lblStatus.Text = "Active"
            ddlStatus.SelectedIndex = 0
        Else
            lblStatus.Text = "Inactive"
            ddlStatus.SelectedIndex = 1
        End If

        '--------------- เปิด ปิด ฟังก์ชั่น Edit -----------
        trView.Visible = e.Item.DataItem("view_mode") = "View"
        trEdit.Visible = e.Item.DataItem("view_mode") = "Edit"

        '---------------- เก็บ Mode Edit,Add----------------
        Dim imgEdit As Image = e.Item.FindControl("imgEdit")
        imgEdit.Attributes("Mode") = e.Item.DataItem("edit_mode")

    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Edit"
                Dim txtName As TextBox = e.Item.FindControl("txtName")
                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                trView.Visible = False
                trEdit.Visible = True
                txtName.Focus()
            Case "Save"
                Dim txtId As TextBox = e.Item.FindControl("txtId")
                Dim txtName As TextBox = e.Item.FindControl("txtName")
                Dim ddlStatus As DropDownList = e.Item.FindControl("ddlStatus")
                Dim ActiveStatus As Boolean = False
                If ddlStatus.SelectedIndex = 0 Then
                    ActiveStatus = True
                End If
                Dim lblId As Label = e.Item.FindControl("lblId")
                Dim lblName As Label = e.Item.FindControl("lblName")

                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")

                If txtName.Text = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Supplier');", True)
                    txtName.Focus()
                    Exit Sub
                End If
                Dim imgEdit As Image = e.Item.FindControl("imgEdit")
                Select Case imgEdit.Attributes("Mode")
                    Case "Edit"
                        Dim SQL As String = "SELECT * FROM MS_SUPPLIER WHERE SUPPLIER_NAME='" & txtName.Text.Replace("'", "''") & "'"
                        Dim DA As New SqlDataAdapter(SQL, ConnStr)
                        Dim DT As New DataTable
                        DA.Fill(DT)
                        If txtName.Text <> lblName.Text And DT.Rows.Count > 0 Then
                            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This Supplier is already exists');", True)
                            txtName.Focus()
                            Exit Sub
                        End If

                        SQL = "SELECT * FROM MS_SUPPLIER WHERE SUPPLIER_ID=" & txtId.Text
                        DA = New SqlDataAdapter(SQL, ConnStr)
                        DT = New DataTable
                        DA.Fill(DT)
                        Dim DR As DataRow = DT.Rows(0)
                        DR("SUPPLIER_ID") = txtId.Text
                        DR("SUPPLIER_NAME") = txtName.Text
                        DR("ACTIVE_STATUS") = ActiveStatus
                        DR("UPDATE_BY") = Session("User_ID")
                        DR("UPDATE_DATE") = Now
                        Dim cmd As New SqlCommandBuilder(DA)
                        DA.Update(DT)
                    Case "Add"
                        Dim SQL As String = "SELECT * FROM MS_SUPPLIER WHERE SUPPLIER_NAME='" & txtName.Text.Replace("'", "''") & "'"
                        Dim DA As New SqlDataAdapter(SQL, ConnStr)
                        Dim DT As New DataTable
                        DA.Fill(DT)
                        DT.Columns.Add("edit_mode", GetType(String), "'Edit'")
                        DT.Columns.Add("view_mode", GetType(String), "'View'")
                        If DT.Rows.Count > 0 Then
                            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This Supplier is already exists');", True)
                            txtName.Focus()
                            Exit Sub
                        End If
                        Dim DR As DataRow = DT.NewRow
                        DR("SUPPLIER_ID") = GL.FindID("MS_SUPPLIER", "SUPPLIER_ID")
                        DR("SUPPLIER_NAME") = txtName.Text
                        DR("ACTIVE_STATUS") = ActiveStatus
                        DR("UPDATE_BY") = Session("User_ID")
                        DR("UPDATE_DATE") = Now
                        DR("view_mode") = "View"
                        DR("edit_mode") = "Edit"
                        DT.Rows.Add(DR)
                        Dim cmd As New SqlCommandBuilder(DA)
                        DA.Update(DT)
                End Select

                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
                BindData()

                Select Case imgEdit.Attributes("Mode")
                    Case "Edit"
                        trView.Visible = True
                        trEdit.Visible = False
                    Case "Add"
                        trView.Visible = True
                        trEdit.Visible = False
                        '---------------- เก็บ Mode Edit,Add----------------
                        imgEdit.Attributes("Mode") = "Edit"
                End Select
            Case "Cancel"
                Dim lblName As Label = e.Item.FindControl("lblName")

                '----------------- คืนค่า ----------------
                Dim txtName As TextBox = e.Item.FindControl("txtName")
                txtName.Text = lblName.Text

                '---------------- กลับไป ตารางเดิม --------
                Dim imgEdit As Image = e.Item.FindControl("imgEdit")
                Dim trView As HtmlTableRow = e.Item.FindControl("trView")
                Dim trEdit As HtmlTableRow = e.Item.FindControl("trEdit")
                Select Case imgEdit.Attributes("Mode")
                    Case "Edit"
                        trView.Visible = True
                        trEdit.Visible = False
                    Case "Add"
                        Dim DT As DataTable = GetDataFromRepeater()
                        DT.Rows.RemoveAt(e.Item.ItemIndex)
                        rptData.DataSource = DT
                        rptData.DataBind()
                End Select
        End Select
    End Sub

    Private Function GetDataFromRepeater() As DataTable

        Dim DT As New DataTable
        DT.Columns.Add("SUPPLIER_ID")
        DT.Columns.Add("SUPPLIER_NAME")
        DT.Columns.Add("ACTIVE_STATUS")
        DT.Columns.Add("view_mode")
        DT.Columns.Add("edit_mode")

        For Each Item As RepeaterItem In rptData.Items
            If Item.ItemType <> ListItemType.AlternatingItem And Item.ItemType <> ListItemType.Item Then Continue For

            Dim lblId As Label = Item.FindControl("lblId")
            Dim lblName As Label = Item.FindControl("lblName")
            Dim ddlStatus As DropDownList = Item.FindControl("ddlStatus")
            Dim ActiveStatus As Boolean = False
            If ddlStatus.SelectedIndex = 0 Then
                ActiveStatus = True
            End If

            Dim DR As DataRow = DT.NewRow
            DR("SUPPLIER_ID") = lblId.Text
            DR("SUPPLIER_NAME") = lblName.Text
            DR("ACTIVE_STATUS") = ActiveStatus

            Dim trView As HtmlTableRow = Item.FindControl("trView")
            Dim trEdit As HtmlTableRow = Item.FindControl("trEdit")
            If trView.Visible Then
                DR("view_mode") = "View"
            Else
                DR("view_mode") = "Edit"
            End If

            Dim imgEdit As Image = Item.FindControl("imgEdit")
            DR("edit_mode") = imgEdit.Attributes("Mode")

            DT.Rows.Add(DR)
        Next

        Return DT
    End Function

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim DT As DataTable = GetDataFromRepeater()
        Dim DR As DataRow = DT.NewRow
        DR("SUPPLIER_ID") = ""
        DR("SUPPLIER_NAME") = ""
        DR("ACTIVE_STATUS") = True
        DR("edit_mode") = "Add"
        DR("view_mode") = "Edit"

        DT.Rows.Add(DR)
        rptData.DataSource = DT
        rptData.DataBind()

        '------------- Set Focus Last Item------
        Dim LastItem As RepeaterItem = rptData.Items(rptData.Items.Count - 1)
        Dim trView As HtmlTableRow = LastItem.FindControl("trView")
        Dim trEdit As HtmlTableRow = LastItem.FindControl("trEdit")
        Dim txtName As TextBox = LastItem.FindControl("txtName")
        trView.Visible = False
        trEdit.Visible = True
        txtName.Focus()
    End Sub

End Class

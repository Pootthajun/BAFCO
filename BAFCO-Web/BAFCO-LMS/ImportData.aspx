﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ImportData.aspx.vb" Inherits="ImportData" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
    <ContentTemplate>
        <table cellspacing="5px;" style="margin: 5px 0 100px 10px;">
            <tr>
                <td width="160px" height="25" class="NormalTextBlack">
                    Upload Excel Document
                </td>
                <td width="350px" height="25"> 
                    <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" BackColor="#CCCCCC" onchange="clearGrid();" /> 
                </td>
               
                <td width="50px" height="25" style="display:none;">
                    <asp:ImageButton ID="btnUpload" runat="server" ImageUrl="images/upload.png" 
                        Height="25px" Width="75px" />
                </td>
                <td>
                </td>
            </tr> 
            <tr>
                <td height="25px" colspan="4"> 
                    <asp:Panel ID="PanelACC" runat="server" Visible="false">
                    <div style="padding: 10px 0 10px 0;"> 
                        <asp:Button ID="btnUpdateACC" runat="server" Text="Update Data" CssClass="Button_Red" Width="200px" Height="30px"/>
                        <Ajax:ConfirmButtonExtender ID="btnUpdate_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to update data?" TargetControlID="btnUpdateACC"></Ajax:ConfirmButtonExtender>
                    </div>
                    <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText" style="position:absolute;">
                        <tr>
                            <td width="40" class="Grid_Header">
                                Item
                            </td>
                            <td width="150" class="Grid_Header">
                                Bafco Job No
                            </td>
                            <td width="150" class="Grid_Header">
                                Invoice No
                            </td>
                            <td width="100" class="Grid_Header">
                                Billing Date
                            </td>
                            <td width="150" class="Grid_Header">
                                CWT
                            </td>
                            <td width="150" class="Grid_Header">
                                Approved CWT
                            </td>
                            <td width="150" class="Grid_Header">
                                Work Ticket 
                            </td>
                            <td width="200" class="Grid_Header">
                                Status
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <asp:Repeater ID="rptACC" runat="server">
                                    <ItemTemplate>
                                        <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                            <td id="tdACC1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdACC2" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblJobNO" runat="server" ></asp:Label>
                                            </td> 
                                            <td id="tdACC3" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblInvoiceNo" runat="server" ></asp:Label>
                                            </td> 
                                            <td id="tdACC4" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblSD" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdACC5" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblCWT" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdACC6" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblApproveCWT" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdACC7" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblWorkTicket" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdACC8" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                            </td>                                 
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr id="trACC" runat="server" visible="false" style="position:relative; background-color:White; height:50px;" valign="top">
                            <td colspan="8">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width:10px">
                                        
                                        </td>
                                        <td>
                                            <uc1:PageNavigation ID="NavigationACC" runat="server" MaximunPageCount="10" PageSize="15"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                    <asp:Panel ID="PanelCCF" runat="server" Visible="false">
                    <div style="padding: 10px 0 10px 0;"> 
                        <asp:Button ID="btnUpdateCCF" runat="server" Text="Update Data" CssClass="Button_Red" Width="200px" Height="30px"/>
                        <Ajax:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" Enabled="true" ConfirmText="Are you sure you want to update data?" TargetControlID="btnUpdateCCF"></Ajax:ConfirmButtonExtender>
                    </div>
                    <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText" style="position:absolute; width:1600px;">
                        <tr>
                            <td width="40" class="Grid_Header">
                                Item
                            </td>
                            <td width="150" class="Grid_Header">
                                Import Entry No
                            </td>
                            <td width="150" class="Grid_Header">
                                Master List No
                            </td>
                            <td width="150" class="Grid_Header">
                                DMF's Letter No
                            </td>
                            <td width="150" class="Grid_Header">
                                Absolute Clearance Date
                            </td>
                            <td width="200" class="Grid_Header">
                                Duty Subjected by DMF
                            </td>
                            <td width="200" class="Grid_Header">
                                Number of Dutiable Item
                            </td>
                            <td width="150" class="Grid_Header">
                                Pack in Case
                            </td>
                            <td width="150" class="Grid_Header">
                                Expenses
                            </td>
                            <td width="250" class="Grid_Header">
                                Status
                            </td>
                        </tr>
                        <tr>
                            <td colspan="10">
                                <asp:Repeater ID="rptCCF" runat="server">
                                    <ItemTemplate>
                                        <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                            <td id="tdCCF1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; text-align:center;">
                                                <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdCCF2" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblJEntryNo" runat="server" ></asp:Label>
                                            </td> 
                                            <td id="tdCCF3" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblListNo" runat="server" ></asp:Label>
                                            </td> 
                                            <td id="tdCCF4" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblLetterNo" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdCCF5" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblClearanceDate" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdCCF6" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblDutySubject" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdCCF7" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblNumberOfDutiable" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdCCF8" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblPackInCase" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdCCF9" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblExpenses" runat="server" ></asp:Label>
                                            </td>
                                            <td id="tdCCF10" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                                <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                            </td>                                 
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr id="trCCF" runat="server" visible="false" style="position:relative; background-color:White; height:50px;" valign="top">
                            <td colspan="10">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width:10px">
                                        
                                        </td>
                                        <td>
                                            <uc1:PageNavigation ID="NavigationCCF" runat="server" MaximunPageCount="10" PageSize="15"/>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>

<script language="javascript">
    function clearGrid() {
        document.getElementById("ctl00_ContentPlaceHolder1_PanelACC").innerHTML = '';
        document.getElementById("ctl00_ContentPlaceHolder1_PanelCCF").innerHTML = '';
        document.getElementById("ctl00_ContentPlaceHolder1_PanelImport").innerHTML = '';
    }
</script>
</asp:Content>


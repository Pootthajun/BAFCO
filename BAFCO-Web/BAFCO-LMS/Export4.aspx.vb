﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Export4
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment_Export.aspx", True)
                Exit Sub
            Else
                Dim SQL As String = ""
                SQL = "SELECT SHIPMENT_ID,STATUS_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ShipmentID = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment_Export.aspx", True)
                    Exit Sub
                End If
            End If

            MC.BindDDlPortOfDeparture(ddlPortOfDeparture, "--------- Select ---------", "")
            MC.BindDDlPortOfDestination(ddlPortOfDeatination, "--------- Select ---------", "")

            SetMenu()
            BindData()

            txtBookingDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtETADate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtETDDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtFlightNo.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtHAWB.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtMAWB.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            btnStatus1.Attributes("onclick") = "window.location.href='Export1.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus2.Attributes("onclick") = "window.location.href='Export2.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus3.Attributes("onclick") = "window.location.href='Export3.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus5.Attributes("onclick") = "window.location.href='Export5.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus6.Attributes("onclick") = "window.location.href='Export6.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus7.Attributes("onclick") = "window.location.href='Export7.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus8.Attributes("onclick") = "window.location.href='Export8.aspx?ShipmentCode=" & ShipmentCode & "';"
        End If
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = "SELECT * FROM EXPORT_BOOKING WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            txtFlightNo.Text = DT.Rows(0).Item("FLIGHT_NO").ToString
            txtMAWB.Text = DT.Rows(0).Item("MAWB").ToString
            txtHAWB.Text = DT.Rows(0).Item("HAWB").ToString
            ddlPortOfDeparture.SelectedValue = DT.Rows(0).Item("DEPARTURE_ID").ToString
            ddlPortOfDeatination.SelectedValue = DT.Rows(0).Item("DESTINATION_ID").ToString
            If DT.Rows(0).Item("ETD_DATE").ToString <> "" Then
                txtETDDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("ETD_DATE"))
            Else
                txtETDDate.Text = ""
            End If
            If DT.Rows(0).Item("ETA_DATE").ToString <> "" Then
                txtETADate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("ETA_DATE"))
            Else
                txtETADate.Text = ""
            End If
            If DT.Rows(0).Item("BOOKING_DATE").ToString <> "" Then
                txtBookingDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("BOOKING_DATE"))
            Else
                txtBookingDate.Text = ""
            End If
        End If
    End Sub

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        If txtETDDate.Text <> "" Then
            If Not MC.CheckDate(txtETDDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('ETD Date is not valid');", True)
                Exit Sub
            End If
        End If

        If txtETADate.Text <> "" Then
            If Not MC.CheckDate(txtETADate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('ETA Date is not valid');", True)
                Exit Sub
            End If
        End If
        '************************************************
        Dim SQL As String = ""
        SQL = "SELECT * FROM EXPORT_BOOKING WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If
        If txtBookingDate.Text.Trim <> "" Then
            DR("BOOKING_DATE") = CV.StringToDate(txtBookingDate.Text, "yyyy-MM-dd")
        Else
            DR("BOOKING_DATE") = DBNull.Value
        End If
        DR("FLIGHT_NO") = txtFlightNo.Text
        DR("MAWB") = txtMAWB.Text
        DR("HAWB") = txtHAWB.Text
        DR("DEPARTURE_ID") = ddlPortOfDeparture.SelectedValue.ToString
        DR("DESTINATION_ID") = ddlPortOfDeatination.SelectedValue.ToString
        If txtETDDate.Text.Trim <> "" Then
            DR("ETD_DATE") = CV.StringToDate(txtETDDate.Text, "yyyy-MM-dd")
        Else
            DR("ETD_DATE") = DBNull.Value
        End If
        If txtETADate.Text.Trim <> "" Then
            DR("ETA_DATE") = CV.StringToDate(txtETADate.Text, "yyyy-MM-dd")
        Else
            DR("ETA_DATE") = DBNull.Value
        End If


        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        GL.UpdateStatusShipmentExport(ShipmentID)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

End Class

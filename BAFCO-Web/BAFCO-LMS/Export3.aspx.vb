﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Export3
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment_Export.aspx", True)
                Exit Sub
            Else
                Dim SQL As String = ""
                SQL = "SELECT SHIPMENT_ID,STATUS_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ShipmentID = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment_Export.aspx", True)
                    Exit Sub
                End If
            End If
            MC.BindDDlPickUpPlace(ddlPickUpPlace, "--------- Select ---------", "")
            SetMenu()
            GetTypeOfVehicle()
            BindData()

            txtFumigate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtPacking.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtPickupDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtRepacking.Attributes.Add("onkeydown", "return (event.keyCode!=13);")

            btnStatus1.Attributes("onclick") = "window.location.href='Export1.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus2.Attributes("onclick") = "window.location.href='Export2.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus4.Attributes("onclick") = "window.location.href='Export4.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus5.Attributes("onclick") = "window.location.href='Export5.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus6.Attributes("onclick") = "window.location.href='Export6.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus7.Attributes("onclick") = "window.location.href='Export7.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus8.Attributes("onclick") = "window.location.href='Export8.aspx?ShipmentCode=" & ShipmentCode & "';"
        End If
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = "SELECT * FROM EXPORT_STORE WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("PICK_DATE").ToString <> "" Then
                txtPickupDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("PICK_DATE"))
            Else
                txtPickupDate.Text = ""
            End If
            ddlPickUpPlace.SelectedValue = DT.Rows(0).Item("PICK_UP_ID").ToString
            txtPacking.Text = DT.Rows(0).Item("PACKING").ToString
            txtRepacking.Text = DT.Rows(0).Item("REPACKING").ToString
            txtFumigate.Text = DT.Rows(0).Item("FUMIGATE").ToString
        End If
    End Sub

#Region "Vehicle"
    Sub GetTypeOfVehicle()
        Dim SQL As String = ""
        SQL &= "SELECT MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID,TYPE_OF_VEHICLE_NAME,AMOUNT,LICENSE_PLATE FROM MS_TYPE_OF_VEHICLE" & vbNewLine
        SQL &= "LEFT JOIN (SELECT TYPE_OF_VEHICLE_ID,AMOUNT,LICENSE_PLATE FROM EXPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID & ") AMOUNT" & vbNewLine
        SQL &= "ON MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID = AMOUNT.TYPE_OF_VEHICLE_ID" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptTypeOfVehicle.DataSource = DT
        rptTypeOfVehicle.DataBind()
    End Sub

    Protected Sub rptTypeOfVehicle_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTypeOfVehicle.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim txtTypeOfVehicle As TextBox = e.Item.FindControl("txtTypeOfVehicle")
        Dim lblTypeOfVehicleID As Label = e.Item.FindControl("lblTypeOfVehicleID")
        Dim lblTypeOfVehicleName As Label = e.Item.FindControl("lblTypeOfVehicleName")
        Dim txtLicensePlate As TextBox = e.Item.FindControl("txtLicensePlate")
        lblTypeOfVehicleID.Text = e.Item.DataItem("TYPE_OF_VEHICLE_ID").ToString
        lblTypeOfVehicleName.Text = e.Item.DataItem("TYPE_OF_VEHICLE_NAME").ToString
        CL.ImplementJavaIntegerText(txtTypeOfVehicle, "center")
        txtTypeOfVehicle.Text = e.Item.DataItem("AMOUNT").ToString
        txtLicensePlate.Text = e.Item.DataItem("LICENSE_PLATE").ToString

        txtTypeOfVehicle.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
        txtLicensePlate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
    End Sub

    Function GetDataVehicle() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("TYPE_OF_VEHICLE_ID")
        DT.Columns.Add("AMOUNT")
        DT.Columns.Add("LICENSE_PLATE")
        For Each ri As RepeaterItem In rptTypeOfVehicle.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblTypeOfVehicleID As Label = ri.FindControl("lblTypeOfVehicleID")
            Dim txtTypeOfVehicle As TextBox = ri.FindControl("txtTypeOfVehicle")
            Dim lblStatus As Label = ri.FindControl("lblStatus")
            Dim txtLicensePlate As TextBox = ri.FindControl("txtLicensePlate")

            Dim DR As DataRow = DT.NewRow
            DR("TYPE_OF_VEHICLE_ID") = lblTypeOfVehicleID.Text
            DR("AMOUNT") = txtTypeOfVehicle.Text
            DR("LICENSE_PLATE") = txtLicensePlate.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function
#End Region
#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        If txtPickupDate.Text <> "" Then
            If Not MC.CheckDate(txtPickupDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Pick Up Date is not valid');", True)
                Exit Sub
            End If
        End If
        '************************************************
        Dim SQL As String = ""
        SQL = "SELECT * FROM EXPORT_STORE WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If

        If txtPickupDate.Text.Trim <> "" Then
            DR("PICK_DATE") = CV.StringToDate(txtPickupDate.Text, "yyyy-MM-dd")
        Else
            DR("PICK_DATE") = DBNull.Value
        End If
        If ddlPickUpPlace.SelectedIndex > 0 Then
            DR("PICK_UP_ID") = ddlPickUpPlace.SelectedValue.ToString
        Else
            DR("PICK_UP_ID") = DBNull.Value
        End If
        DR("PACKING") = txtPacking.Text
        DR("REPACKING") = txtRepacking.Text
        DR("FUMIGATE") = txtFumigate.Text

        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        '************* Vehicle ***********
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim strSQL As String = "DELETE FROM EXPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID
        Dim objCmd As SqlCommand
        objCmd = New SqlCommand(strSQL, conn)
        objCmd.ExecuteNonQuery()
        conn.Close()

        strSQL = "SELECT * FROM EXPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID
        DA = New SqlDataAdapter(strSQL, conn)
        Dim DT_VEHICLE As New DataTable
        DA.Fill(DT_VEHICLE)

        DT = New DataTable
        DT = GetDataVehicle()
        For i As Int32 = 0 To DT.Rows.Count - 1
            If DT.Rows(i).Item("AMOUNT").ToString <> "" Then
                DR = DT_VEHICLE.NewRow
                DR("SHIPMENT_ID") = ShipmentID
                DR("TYPE_OF_VEHICLE_ID") = DT.Rows(i).Item("TYPE_OF_VEHICLE_ID").ToString
                DR("AMOUNT") = DT.Rows(i).Item("AMOUNT").ToString
                DR("LICENSE_PLATE") = DT.Rows(i).Item("LICENSE_PLATE").ToString
                DT_VEHICLE.Rows.Add(DR)
                Dim cmb As New SqlCommandBuilder(DA)
                DA.Update(DT_VEHICLE)
            End If
        Next

        GL.UpdateStatusShipmentExport(ShipmentID)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

End Class

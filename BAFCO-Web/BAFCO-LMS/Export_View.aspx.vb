﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Export_View
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment_Export.aspx", True)
                Exit Sub
            Else
                Dim SQL_ As String = ""
                SQL_ = "SELECT SHIPMENT_ID,STATUS_ID,REF_WORK_ORDER_NO FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA_ As New SqlDataAdapter(SQL_, ConnStr)
                Dim DT_ As New DataTable
                DA_.Fill(DT_)
                If DT_.Rows.Count > 0 Then
                    ShipmentID = DT_.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment_Export.aspx", True)
                    Exit Sub
                End If
            End If

            Dim SQL As String = ""
            SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
                lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
                lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
            End If

            BindDataExport1()
            BindDataExport2()
            BindDataExport3()
            BindDataExport4()
            BindDataExport5()
            BindDataExport6()
            BindDataExport7()
            BindDataExport8()
            For Each ctrl As Control In PanelForm.Controls
                If TypeOf ctrl Is TextBox Then
                    CType(ctrl, TextBox).ReadOnly = True
                End If
            Next

        End If
    End Sub

#Region "Export1"
    Sub BindDataExport1()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT1 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("PRE_ALERT_DATE").ToString <> "" Then
                lblPreAlertDate.Text = DT.Rows(0).Item("PRE_ALERT_DATE").ToString
                lblImport1.Text = DT.Rows(0).Item("PRE_ALERT_DATE").ToString
            End If
            If DT.Rows(0).Item("JOB_NO").ToString <> "" Then
                lblJobNo.Text = DT.Rows(0).Item("JOB_NO").ToString
            End If
            If DT.Rows(0).Item("PROJECT_OWNER_NAME").ToString <> "" Then
                lblProjectOwner.Text = DT.Rows(0).Item("PROJECT_OWNER_NAME").ToString
            End If
            If DT.Rows(0).Item("CONTRACTOR_NAME").ToString <> "" Then
                lblContractor.Text = DT.Rows(0).Item("CONTRACTOR_NAME").ToString
            End If
            If DT.Rows(0).Item("SUPPLIER_NAME").ToString <> "" Then
                lblSupplier.Text = DT.Rows(0).Item("SUPPLIER_NAME").ToString
            End If
            If DT.Rows(0).Item("BILL_TO_NAME").ToString <> "" Then
                lblSectionBillTo.Text = DT.Rows(0).Item("BILL_TO_NAME").ToString
            End If
            If DT.Rows(0).Item("COST_CENTER").ToString <> "" Then
                lblCostCenter.Text = DT.Rows(0).Item("COST_CENTER").ToString
            End If
            If DT.Rows(0).Item("CARGO_VALUE").ToString <> "" Then
                lblCargoValue.Text = FormatNumber(DT.Rows(0).Item("CARGO_VALUE").ToString)
            End If
            If DT.Rows(0).Item("CURRENCY_NAME").ToString <> "" Then
                lblCurrency.Text = DT.Rows(0).Item("CURRENCY_NAME").ToString
            End If
            If DT.Rows(0).Item("TERM_NAME").ToString <> "" Then
                lblTerm.Text = DT.Rows(0).Item("TERM_NAME").ToString
            End If
            If DT.Rows(0).Item("QTY").ToString <> "" Then
                lblQty.Text = FormatNumber(DT.Rows(0).Item("QTY").ToString, 0)
            End If
            If DT.Rows(0).Item("UOM_NAME").ToString <> "" Then
                lblUOM.Text = DT.Rows(0).Item("UOM_NAME").ToString
            End If
            If DT.Rows(0).Item("WEIGHT").ToString <> "" Then
                lblWeight.Text = FormatNumber(DT.Rows(0).Item("WEIGHT").ToString)
            End If
            If DT.Rows(0).Item("VOLUME").ToString <> "" Then
                lblVolume.Text = FormatNumber(DT.Rows(0).Item("VOLUME").ToString, 0)
            End If
            If DT.Rows(0).Item("DESCRIPTION").ToString <> "" Then
                lblDescriptionOfGoods.Text = DT.Rows(0).Item("DESCRIPTION").ToString
            End If

            If DT.Rows(0).Item("INVOICE_NO").ToString <> "" Then
                'lblInvoiceNo.Text = DT.Rows(0).Item("INVOICE_NO").ToString
                Dim DT_INV As New DataTable
                DT_INV.Columns.Add("INV_NO")
                If InStr(DT.Rows(0).Item("INVOICE_NO").ToString, " ||||| ") > 0 Then
                    Dim inv() As String
                    inv = Split(DT.Rows(0).Item("INVOICE_NO").ToString, " ||||| ")
                    For i As Int32 = 0 To inv.Length - 1
                        Dim DR As DataRow = DT_INV.NewRow
                        DR("INV_NO") = inv(i)
                        DT_INV.Rows.Add(DR)
                    Next
                Else
                    Dim DR As DataRow = DT_INV.NewRow
                    DR("INV_NO") = DT.Rows(0).Item("INVOICE_NO").ToString
                    DT_INV.Rows.Add(DR)
                End If
                rptInvoice.DataSource = DT_INV
                rptInvoice.DataBind()
            End If
        End If

        SQL = "SELECT * FROM EXPORT_PO WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Dim DT_PO As New DataTable
            DT_PO.Columns.Add("PO_NO")
            For i As Int32 = 0 To DT.Rows.Count - 1
                Dim DR As DataRow = DT_PO.NewRow
                DR("PO_NO") = DT.Rows(i).Item("PO").ToString
                DT_PO.Rows.Add(DR)
            Next
            rptData_E1.DataSource = DT_PO
            rptData_E1.DataBind()
        Else
            PO_E1.Visible = False
        End If
    End Sub

    Protected Sub rptData_E1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_E1.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblPONo As Label = e.Item.FindControl("lblPONo")
        lblPONo.Text = e.Item.DataItem("PO_NO").ToString
    End Sub

    Protected Sub rptInvoice_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInvoice.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblInvoice As Label = e.Item.FindControl("lblInvoice")
        lblInvoice.Text = e.Item.DataItem("INV_NO").ToString
    End Sub
#End Region
#Region "Export2"
    Sub BindDataExport2()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT2 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("DOC_DATE").ToString <> "" Then
                lblDocDate.Text = DT.Rows(0).Item("DOC_DATE").ToString
                lblImport2.Text = DT.Rows(0).Item("DOC_DATE").ToString
            End If
            If DT.Rows(0).Item("PER_DATE").ToString <> "" Then
                lblPerDate.Text = DT.Rows(0).Item("PER_DATE").ToString
            End If
            If DT.Rows(0).Item("APP_DATE").ToString <> "" Then
                lblAppDate.Text = DT.Rows(0).Item("APP_DATE").ToString
            End If
            If DT.Rows(0).Item("LETTER_NO").ToString <> "" Then
                lblLetterNo.Text = DT.Rows(0).Item("LETTER_NO").ToString
            End If
        End If
    End Sub
#End Region
#Region "Export3"
    Sub BindDataExport3()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT3 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("PICK_DATE").ToString <> "" Then
                lblPickupDate.Text = DT.Rows(0).Item("PICK_DATE").ToString
                lblImport3.Text = DT.Rows(0).Item("PICK_DATE").ToString
            End If
            If DT.Rows(0).Item("PICK_UP_NAME").ToString <> "" Then
                lblPickUpPlace.Text = DT.Rows(0).Item("PICK_UP_NAME").ToString
            End If
            If DT.Rows(0).Item("PACKING").ToString <> "" Then
                lblPacking.Text = DT.Rows(0).Item("PACKING").ToString
            End If
            If DT.Rows(0).Item("REPACKING").ToString <> "" Then
                lblRepacking.Text = DT.Rows(0).Item("REPACKING").ToString
            End If
            If DT.Rows(0).Item("FUMIGATE").ToString <> "" Then
                lblFumigate.Text = DT.Rows(0).Item("FUMIGATE").ToString
            End If
        End If

        SQL = "SELECT MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID,TYPE_OF_VEHICLE_NAME,AMOUNT,LICENSE_PLATE FROM MS_TYPE_OF_VEHICLE" & vbNewLine
        SQL &= "LEFT JOIN (SELECT TYPE_OF_VEHICLE_ID,AMOUNT,LICENSE_PLATE FROM EXPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID & ") AMOUNT" & vbNewLine
        SQL &= "ON MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID = AMOUNT.TYPE_OF_VEHICLE_ID" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        rptTypeOfVehicle.DataSource = DT
        rptTypeOfVehicle.DataBind()
    End Sub

    Protected Sub rptTypeOfVehicle_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTypeOfVehicle.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim txtTypeOfVehicle As TextBox = e.Item.FindControl("txtTypeOfVehicle")
        Dim lblTypeOfVehicleID As Label = e.Item.FindControl("lblTypeOfVehicleID")
        Dim lblTypeOfVehicleName As Label = e.Item.FindControl("lblTypeOfVehicleName")
        Dim txtLicensePlate As TextBox = e.Item.FindControl("txtLicensePlate")
        lblTypeOfVehicleID.Text = e.Item.DataItem("TYPE_OF_VEHICLE_ID").ToString
        lblTypeOfVehicleName.Text = e.Item.DataItem("TYPE_OF_VEHICLE_NAME").ToString
        txtTypeOfVehicle.Text = e.Item.DataItem("AMOUNT").ToString
        txtLicensePlate.Text = e.Item.DataItem("LICENSE_PLATE").ToString
    End Sub
#End Region
#Region "Export4"
    Sub BindDataExport4()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT4 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("BOOKING_DATE").ToString <> "" Then
                lblBookingDate.Text = DT.Rows(0).Item("BOOKING_DATE").ToString
                lblImport4.Text = DT.Rows(0).Item("BOOKING_DATE").ToString
            End If
            If DT.Rows(0).Item("FLIGHT_NO").ToString <> "" Then
                lblFlightNo.Text = DT.Rows(0).Item("FLIGHT_NO").ToString
            End If
            If DT.Rows(0).Item("MAWB").ToString <> "" Then
                lblMAWB.Text = DT.Rows(0).Item("MAWB").ToString
            End If
            If DT.Rows(0).Item("HAWB").ToString <> "" Then
                lblHAWB.Text = DT.Rows(0).Item("HAWB").ToString
            End If
            If DT.Rows(0).Item("DEPARTURE_NAME").ToString <> "" Then
                lblPortOfDeparture.Text = DT.Rows(0).Item("DEPARTURE_NAME").ToString
            End If
            If DT.Rows(0).Item("DESTINATION_NAME").ToString <> "" Then
                lblPortOfDeatination.Text = DT.Rows(0).Item("DESTINATION_NAME").ToString
            End If
            If DT.Rows(0).Item("ETD_DATE").ToString <> "" Then
                lblETDDate.Text = DT.Rows(0).Item("ETD_DATE").ToString
            End If
            If DT.Rows(0).Item("ETA_DATE").ToString <> "" Then
                lblETADate.Text = DT.Rows(0).Item("ETA_DATE").ToString
            End If
        End If
    End Sub
#End Region
#Region "Export5"
    Sub BindDataExport5()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT5 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("SIGN_DATE").ToString <> "" Then
                lblSignDate.Text = DT.Rows(0).Item("SIGN_DATE").ToString
                lblImport5.Text = DT.Rows(0).Item("SIGN_DATE").ToString
            End If
            If DT.Rows(0).Item("EXPORT_ENTRY_NO").ToString <> "" Then
                lblExportEntryNo.Text = DT.Rows(0).Item("EXPORT_ENTRY_NO").ToString
            End If
        End If
    End Sub
#End Region
#Region "Export6"
    Sub BindDataExport6()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT6 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("CLEAR_DATE").ToString <> "" Then
                lblClearDate.Text = DT.Rows(0).Item("CLEAR_DATE").ToString
                lblImport6.Text = DT.Rows(0).Item("CLEAR_DATE").ToString
            End If
            If DT.Rows(0).Item("DELIVERY_DATE").ToString <> "" Then
                lblDeliveryDate.Text = DT.Rows(0).Item("DELIVERY_DATE").ToString
            End If
            If DT.Rows(0).Item("DELIVERY_NAME").ToString <> "" Then
                lblPlaceOfDelivery.Text = DT.Rows(0).Item("DELIVERY_NAME").ToString
            End If
            If DT.Rows(0).Item("PENDING").ToString <> "" Then
                If CBool(DT.Rows(0).Item("PENDING")) = True Then
                    lblPending.Text = "Yes"
                Else
                    lblPending.Text = "No"
                End If
            End If
            If DT.Rows(0).Item("COMMENT").ToString <> "" Then
                lblComment.Text = DT.Rows(0).Item("COMMENT").ToString
            End If
            If DT.Rows(0).Item("DURATION").ToString <> "" Then
                lblDuration.Text = DT.Rows(0).Item("DURATION").ToString
            End If
        End If

        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM EXPORT_RELEASE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            rptData_E6.DataSource = DT
            rptData_E6.DataBind()
        Else
            Upload_E6.Visible = False
        End If

    End Sub

    Protected Sub rptData_E6_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_E6.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
    End Sub

    Protected Sub rptData_E6_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData_E6.ItemCommand
        Select Case e.CommandName
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload_E6()
                Dim pathfile As String = ""
                pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetDataFileUpload_E6() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptData_E6.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function
#End Region
#Region "Export7"
    Sub BindDataExport7()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT7 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("ALERT_DATE").ToString <> "" Then
                lblAlertDate.Text = DT.Rows(0).Item("ALERT_DATE").ToString
                lblImport7.Text = DT.Rows(0).Item("ALERT_DATE").ToString
            End If
        End If
    End Sub
#End Region
#Region "Export8"
    Sub BindDataExport8()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_EXPORT8 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("STATUS_DELIVER").ToString <> "" Then
                lblStatusDeliver.Text = DT.Rows(0).Item("STATUS_DELIVER").ToString
                lblImport8.Text = DT.Rows(0).Item("STATUS_DELIVER").ToString
            End If
            If DT.Rows(0).Item("CWT").ToString <> "" Then
                lblCWT.Text = DT.Rows(0).Item("CWT").ToString
            End If
            If DT.Rows(0).Item("WORK_TICKET").ToString <> "" Then
                lblWorkTicket.Text = DT.Rows(0).Item("WORK_TICKET").ToString
            End If
            If DT.Rows(0).Item("APPROVE_CWT").ToString <> "" Then
                lblApprovedCWT.Text = DT.Rows(0).Item("APPROVE_CWT").ToString
            End If
            If DT.Rows(0).Item("ACC_INVOICE_NO").ToString <> "" Then
                lblInvNo.Text = DT.Rows(0).Item("ACC_INVOICE_NO").ToString
            End If
        End If
    End Sub
#End Region
End Class

﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class ExportData
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlBranch(ddlBranch, "-------- All --------", "")
            MC.BindDDlCompany(ddlCompany, "-------- All --------", "")
            MC.BindCBLShipmentType(cblShipmentType)
            MC.BindDDlTransportationType(ddlTransportationType, "-------- All --------", "")
            MC.BindDDlPortOfDestination(ddlPortOfDestination, "-------- All --------", "")
            ClearForm()
            BindData()
        End If
    End Sub

    Private Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = "SELECT * FROM VW_SHIPMENT ORDER BY CREATE_DATE DESC" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Session("Export") = DT
        FilterData()
    End Sub

    Sub FilterData()
        Dim DT As New DataTable
        DT = Session("Export")
        If Not (DT Is Nothing) AndAlso DT.Rows.Count > 0 Then
            '--------------- Filter ----------------------
            Dim Filter As String = ""

            If ddlBranch.SelectedIndex > 0 Then
                Filter &= "BRANCH_ID =" & ddlBranch.SelectedValue & " AND "
            End If
            If ddlCompany.SelectedIndex > 0 Then
                Filter &= "COMPANY_ID =" & ddlCompany.SelectedValue & " AND "
            End If
            Dim ShipmentType As String = ""
            For i As Int32 = 0 To cblShipmentType.Items.Count - 1
                If cblShipmentType.Items(i).Selected = True Then
                    ShipmentType = ShipmentType & cblShipmentType.Items(i).Value.ToString & ","
                End If
            Next
            If ShipmentType <> "" Then
                ShipmentType = ShipmentType.Substring(0, ShipmentType.Length - 1)
                Filter &= "SHIPMENT_TYPE_ID in (" & ShipmentType & ") AND "
            End If
            If txtWorkOrderNo.Text.Trim <> "" Then
                Filter &= "WORK_ORDER_NO like '%" & txtWorkOrderNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtPONo.Text.Trim <> "" Then
                Filter &= "ALL_PO like '%" & txtPONo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtInvoiceNo.Text.Trim <> "" Then
                Filter &= "INVOICE_NO like '%" & txtInvoiceNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If ddlTransportationType.SelectedIndex > 0 Then
                Filter &= "TRANSPORT_TYPE_ID =" & ddlTransportationType.SelectedValue & " AND "
            End If
            If ddlPortOfDestination.SelectedIndex > 0 Then
                Filter &= "DESTINATION_ID =" & ddlPortOfDestination.SelectedValue & " AND "
            End If
            If txtPreAlertFrom.Text.Trim <> "" Then
                If MC.CheckDate(txtPreAlertFrom.Text) Then
                    Filter &= "PRE_ALERT >=" & txtPreAlertFrom.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtPreAlertTo.Text.Trim <> "" Then
                If MC.CheckDate(txtPreAlertTo.Text) Then
                    Filter &= "PRE_ALERT <=" & txtPreAlertTo.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtReceivedFrom.Text.Trim <> "" Then
                If MC.CheckDate(txtReceivedFrom.Text) Then
                    Filter &= "RECEIVE_DATE >=" & txtReceivedFrom.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtReceivedTo.Text.Trim <> "" Then
                If MC.CheckDate(txtReceivedTo.Text) Then
                    Filter &= "RECEIVE_DATE <=" & txtReceivedTo.Text.Replace("-", "") & " AND "
                End If
            End If

            If Session("User_Type") = "User" Then
                Filter &= "CREATE_BY =" & Session("User_ID") & " AND "
            ElseIf Session("User_Type") = "Customer" Then
                Filter &= "COMPANY_NAME ='" & Session("User_Fullname") & "' AND "
            End If

            If Filter <> "" Then Filter = Filter.Substring(0, Filter.Length - 4)
            DT.DefaultView.RowFilter = Filter
            DT = DT.DefaultView.ToTable
            If DT.Rows.Count <= 15 Then
                Navigation.Visible = False
            Else
                Navigation.Visible = True
            End If

            Session("ExportDataFilter") = DT
            Navigation.SesssionSourceName = "ExportDataFilter"
            Navigation.RenderLayout()
        End If
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblShipmentID As Label = e.Item.FindControl("lblShipmentID")
        Dim lblShipmentCode As Label = e.Item.FindControl("lblShipmentCode")
        Dim lblStatusID As Label = e.Item.FindControl("lblStatusID")
        Dim lblWorkOrder As Label = e.Item.FindControl("lblWorkOrder")
        Dim lblCompany As Label = e.Item.FindControl("lblCompany")
        Dim lblSupplier As Label = e.Item.FindControl("lblSupplier")
        Dim lblType As Label = e.Item.FindControl("lblType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblShipmentID.Text = e.Item.DataItem("SHIPMENT_ID").ToString
        lblShipmentCode.Text = e.Item.DataItem("SHIPMENT_CODE").ToString
        lblStatusID.Text = e.Item.DataItem("STATUS_ID").ToString
        lblWorkOrder.Text = e.Item.DataItem("WORK_ORDER_NO").ToString
        lblCompany.Text = e.Item.DataItem("COMPANY_NAME").ToString
        lblSupplier.Text = e.Item.DataItem("SUPPLIER_NAME").ToString
        lblType.Text = e.Item.DataItem("SHIPMENT_TYPE").ToString
        lblStatus.Text = e.Item.DataItem("STATUS_NAME").ToString

    End Sub

    Sub ClearForm()
        ddlBranch.SelectedIndex = 0
        ddlCompany.SelectedIndex = 0
        For i As Int32 = 0 To cblShipmentType.Items.Count - 1
            cblShipmentType.Items(i).Selected = True
        Next
        ddlTransportationType.SelectedIndex = 0
        ddlPortOfDestination.SelectedIndex = 0
        txtWorkOrderNo.Text = ""
        txtPONo.Text = ""
        txtInvoiceNo.Text = ""
        txtPreAlertFrom.Text = ""
        txtPreAlertTo.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearForm()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData()
        FilterData()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim DT As New DataTable
        DT = Session("ExportDataFilter")
        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('None data for Export');", True)
            Exit Sub
        End If
        Dim ShipmentID As String = ""
        For i As Int32 = 0 To DT.Rows.Count - 1
            ShipmentID = ShipmentID & DT.Rows(i).Item("SHIPMENT_ID").ToString & ","
        Next
        ShipmentID = ShipmentID.Substring(0, ShipmentID.Length - 1)

        Dim SQL As String = "SELECT ROW_NUMBER() OVER(ORDER BY SHIPMENT_ID DESC) AS Item,* FROM VW_EXPORT_DATA WHERE SHIPMENT_ID IN (" & ShipmentID & ") ORDER BY SHIPMENT_ID DESC" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_ As New DataTable
        DA.Fill(DT_)
        DT_.Columns.RemoveAt(1)

        Session("ExportDataToExcel") = DT_
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('ReportViewer.aspx');", True)
    End Sub
End Class

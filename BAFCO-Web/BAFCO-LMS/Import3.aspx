﻿<%@ Page Language="VB" MasterPageFile="~/MasterPageNewWindows.master" AutoEventWireup="false" CodeFile="Import3.aspx.vb" Inherits="Import3"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
    <ContentTemplate>
        <div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Import
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>   
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;">
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:119;">
		                            <input runat="server" value="Receive Pre-Alert" id="btnStatus1" class="Button_White" style="height:30px;width:119px;" type="button" />
		                        </td>
		                        <td style="width:170;">
		                            <input runat="server" value="Prepare Import Entry Form" id="btnStatus2" class="Button_White" style="height:30px;width:170px;" type="button" />
		                        </td>
		                        <td style="width:130;">
		                            <input runat="server" value="Customs Clearance" id="btnStatus3" class="Button_Red" style="height:30px;width:130px;" type="button" />
		                        </td>
		                        <td style="width:190;">
		                             <input runat="server" value="Store in Warehouse & Packing" id="btnStatus4" class="Button_White" style="height:30px;width:190px;" type="button" />
		                        </td>
		                        <td style="width:120;">
		                             <input runat="server" value="Accounting Info" id="btnStatus5" class="Button_White" style="height:30px;width:120px;" type="button" />
		                        </td>
		                        <td style="width:180;">
		                             <input runat="server" value="Complete Customs Formality" id="btnStatus6" class="Button_White" style="height:30px;width:180px;" type="button" />
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Clear Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtClearDate" runat="server" CssClass="Textbox_Form_White" width="200px" AutoPostBack="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgClearDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgClearDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgClearDate"
                        TargetControlID="txtClearDate"></Ajax:CalendarExtender>
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Duration of Clear day
                    </td>
                    <td width="100" height="25">
                        <asp:TextBox ID="txtDuration" runat="server" CssClass="Textbox_Form_Disable" width="100px" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr >
                    <td width="150" height="25" class="NormalTextBlack">
                        Place of Delivery
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlPlaceOfDelivery" runat="server" CssClass="Dropdown_Form_White" width="205px"></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Status Pending
                    </td>
                    <td width="100" height="25">
                        <asp:DropDownList ID="ddlPending" runat="server" CssClass="Dropdown_Form_White" width="105px">
                                    <asp:ListItem Text="--- Select ---" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Only .pdf, .png, .jpg, .jpeg"></asp:Label>
                    </td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Insert Attachment
                    </td>
                    <td height="25" colspan="3"> 
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" BackColor="#CCCCCC" />
                    </td>
                    <td width="50" height="25" style="display:none;">
                        <asp:ImageButton ID="btnUpload" runat="server" ImageUrl="images/upload.png" 
                            Height="25px" Width="75px"/>
                    </td>
                    <td></td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                    </td>
                    <td width="200" height="25" colspan="2"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:170px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td2" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                            <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="150" height="25">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment" runat="server" CssClass="Textbox_Form_White" width="515px" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


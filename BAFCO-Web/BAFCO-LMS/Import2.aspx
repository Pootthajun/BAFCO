﻿<%@ Page Language="VB" MasterPageFile="~/MasterPageNewWindows.master" AutoEventWireup="false" CodeFile="Import2.aspx.vb" Inherits="Import2"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Import
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;">
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:119;">
		                            <input runat="server" value="Receive Pre-Alert" id="btnStatus1" class="Button_White" style="height:30px;width:119px;" type="button" />
		                        </td>
		                        <td style="width:170;">
		                            <input runat="server" value="Prepare Import Entry Form" id="btnStatus2" class="Button_Red" style="height:30px;width:170px;" type="button" />
		                        </td>
		                        <td style="width:130;">
		                            <input runat="server" value="Customs Clearance" id="btnStatus3" class="Button_White" style="height:30px;width:130px;" type="button" />
		                        </td>
		                        <td style="width:190;">
		                             <input runat="server" value="Store in Warehouse & Packing" id="btnStatus4" class="Button_White" style="height:30px;width:190px;" type="button" />
		                        </td>
		                        <td style="width:120;">
		                             <input runat="server" value="Accounting Info" id="btnStatus5" class="Button_White" style="height:30px;width:120px;" type="button" />
		                        </td>
		                        <td style="width:180;">
		                             <input runat="server" value="Complete Customs Formality" id="btnStatus6" class="Button_White" style="height:30px;width:180px;" type="button" />
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Job Issued Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtIssueDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgIssueDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgIssueDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgIssueDate"
                        TargetControlID="txtIssueDate"></Ajax:CalendarExtender>
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        BAFCO Job No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtJobNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Port of Destination
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlPortOfDestination" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        ETA Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtETADate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgETADate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgETADate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgETADate"
                        TargetControlID="txtETADate"></Ajax:CalendarExtender>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlETATime_H" runat="server" CssClass="Dropdown_Form_White" width="50px">
                                    <asp:ListItem Text="--" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="01" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="02" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="03" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="04" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="06" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="07" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="08" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="09" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>:</td>
                                <td>
                                    <asp:DropDownList ID="ddlETATime_M" runat="server" CssClass="Dropdown_Form_White" width="50px" >
                                    <asp:ListItem Text="--" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                    <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                    <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                    <asp:ListItem Text="55" Value="55"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        D/O Receive Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDODate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgDODate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgDODate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgDODate"
                        TargetControlID="txtDODate"></Ajax:CalendarExtender>
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Sign Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtSignDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgSignDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgSignDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgSignDate"
                        TargetControlID="txtSignDate"></Ajax:CalendarExtender>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlSignTime_H" runat="server" CssClass="Dropdown_Form_White" width="50px">
                                    <asp:ListItem Text="--" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="01" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="02" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="03" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="04" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="06" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="07" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="08" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="09" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>:</td>
                                <td>
                                    <asp:DropDownList ID="ddlSignTime_M" runat="server" CssClass="Dropdown_Form_White" width="50px" >
                                    <asp:ListItem Text="--" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                    <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                    <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                    <asp:ListItem Text="55" Value="55"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Import Entry No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtImportEntryNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Import Entry Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtImportEntryDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgImportEntryDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgImportEntryDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgImportEntryDate"
                        TargetControlID="txtImportEntryDate"></Ajax:CalendarExtender>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="ddlImportEntryTime_H" runat="server" CssClass="Dropdown_Form_White" width="50px">
                                   <asp:ListItem Text="--" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="01" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="02" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="03" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="04" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="06" Value="6"></asp:ListItem>
                                    <asp:ListItem Text="07" Value="7"></asp:ListItem>
                                    <asp:ListItem Text="08" Value="8"></asp:ListItem>
                                    <asp:ListItem Text="09" Value="9"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="21" Value="21"></asp:ListItem>
                                    <asp:ListItem Text="22" Value="22"></asp:ListItem>
                                    <asp:ListItem Text="23" Value="23"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>:</td>
                                <td>
                                    <asp:DropDownList ID="ddlImportEntryTime_M" runat="server" CssClass="Dropdown_Form_White" width="50px" >
                                    <asp:ListItem Text="--" Value="-1"></asp:ListItem>
                                    <asp:ListItem Text="00" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="05" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                    <asp:ListItem Text="25" Value="25"></asp:ListItem>
                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                    <asp:ListItem Text="35" Value="35"></asp:ListItem>
                                    <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                    <asp:ListItem Text="45" Value="45"></asp:ListItem>
                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                    <asp:ListItem Text="55" Value="55"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Duty Tax
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDutyTax" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Duty Tax Paid On
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDutyTaxDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgDutyTaxDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgDutyTaxDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgDutyTaxDate"
                        TargetControlID="txtDutyTaxDate"></Ajax:CalendarExtender>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Excises Tax
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtExcisesTax" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Excises Tax Paid On
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtExcisesTaxDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgExcisesTaxDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgExcisesTaxDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgExcisesTaxDate"
                        TargetControlID="txtExcisesTaxDate"></Ajax:CalendarExtender>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        M.O.I. Tax
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtMOI" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td colspan="5"></td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


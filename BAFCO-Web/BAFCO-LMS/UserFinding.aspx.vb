﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class UserFinding
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
            MC.BindDDlBranch(ddlBranch, "-------- All --------", "")
            MC.BindDDlUserType(ddlUserType, "-------- All --------", "")
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = "SELECT [USER_ID],USER_CODE,USER_FULLNAME,MS_USER.USER_TYPE_ID,USER_TYPE_NAME," & vbNewLine
        SQL &= "CASE WHEN MS_USER.ACTIVE_STATUS = 1 THEN 'Active' ELSE 'Inactive' END AS [STATUS]" & vbNewLine
        SQL &= "FROM MS_USER LEFT JOIN MS_USER_TYPE ON MS_USER.USER_TYPE_ID = MS_USER_TYPE.USER_TYPE_ID ORDER BY USER_FULLNAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If
        DT.Columns.Add("BRANCH")

        For i As Int32 = 0 To DT.Rows.Count - 1
            Dim DT_ As New DataTable
            Dim Branch As String = ""
            Dim ShipmentType As String = ""

            SQL = "SELECT BRANCH_NAME FROM MS_USER_BRANCH LEFT JOIN MS_BRANCH" & vbNewLine
            SQL &= "ON MS_USER_BRANCH.BRANCH_ID = MS_BRANCH.BRANCH_ID" & vbNewLine
            SQL &= "WHERE [USER_ID] = " & DT.Rows(i).Item("USER_ID") & " ORDER BY BRANCH_NAME"
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT_ = New DataTable
            DA.Fill(DT_)
            For x As Int32 = 0 To DT_.Rows.Count - 1
                Branch = Branch & DT_.Rows(x).Item("BRANCH_NAME").ToString & ","
            Next
            Branch = Branch.Substring(0, Branch.Length - 1)
            DT.Rows(i).Item("BRANCH") = Branch
        Next
        Session("UserFinding") = DT

        Navigation.SesssionSourceName = "UserFinding"
        Navigation.RenderLayout()
    End Sub

    Private Sub FilterData()
        Dim DT As New DataTable
        DT = Session("UserFinding")

        '--------------- Filter ----------------------
        Dim Filter As String = ""
        If txtCode.Text <> "" Then
            Filter &= "USER_CODE like '%" & txtCode.Text.Replace("'", "''") & "%' AND "
        End If
        If txtFullname.Text <> "" Then
            Filter &= "USER_FULLNAME like '%" & txtFullname.Text.Replace("'", "''") & "%' AND "
        End If
        If ddlBranch.SelectedIndex > 0 Then
            Filter &= "BRANCH like '%" & ddlBranch.SelectedItem.Text.Replace("'", "''") & "%' AND "
        End If
        If ddlUserType.SelectedIndex > 0 Then
            Filter &= "USER_TYPE_ID = " & ddlUserType.SelectedValue.ToString & " AND "
        End If
        If ddlStatus.SelectedIndex > 0 Then
            Filter &= "STATUS = '" & ddlStatus.Text.Replace("'", "''") & "' AND "
        End If

        If Filter <> "" Then Filter = Filter.Substring(0, Filter.Length - 4)
        DT.DefaultView.RowFilter = Filter

        DT = DT.DefaultView.ToTable
        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If
        Session("UserFinding_Filter") = DT

        Navigation.SesssionSourceName = "UserFinding_Filter"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblCode As Label = e.Item.FindControl("lblCode")
        Dim lblFullname As Label = e.Item.FindControl("lblFullname")
        Dim lblBranch As Label = e.Item.FindControl("lblBranch")
        Dim lblUserType As Label = e.Item.FindControl("lblUserType")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim trRow As HtmlTableRow = e.Item.FindControl("trRow")

        lblNo.Text = e.Item.ItemIndex + 1
        lblCode.Text = e.Item.DataItem("USER_CODE")
        lblFullname.Text = e.Item.DataItem("USER_FULLNAME")
        lblBranch.Text = e.Item.DataItem("BRANCH")
        lblUserType.Text = e.Item.DataItem("USER_TYPE_NAME")
        lblStatus.Text = e.Item.DataItem("STATUS")

        trRow.Attributes("onClick") = "returnSelectedValue('" & CStr(e.Item.DataItem("USER_ID")).Replace("'", "\'") & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click, ddlStatus.SelectedIndexChanged, ddlBranch.SelectedIndexChanged, ddlUserType.SelectedIndexChanged
        FilterData()
    End Sub
End Class

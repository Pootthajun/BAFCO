﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WMSFinding.aspx.vb" Inherits="WMSFinding" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reference Work Order No Finding</title>
    <link href="style/css.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="Script/script.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManagerMain" runat="Server"></asp:ScriptManager>
        <table width="100%" height="30" border="0" bgcolor="red" cellpadding="0" cellspacing="0" >
            <tr>
                
                <td height="40" align="center" class="Master_Header_Text">
                    Reference Work Order No Finding
                </td>                
            </tr>
        </table>

        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch" style="height:100%; width:100%;">
            <asp:UpdatePanel ID="udp1" runat="server" >
                <ContentTemplate>
                    <table width="250" cellpadding="0" cellspacing="1" style="margin: 10px 0px 0px 0px;" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="50" class="Grid_Header">
                                No
                            </td>
                            <td width="400" class="Grid_Header">
                                WMS Lot No
                            </td>
                        </tr>
                        <tr>
                            <td height="25px">
                                &nbsp;<asp:Button ID="btnSearch" runat="server" style="display:none;"/>
                            </td>
                            <td height="25px">
                                <asp:TextBox ID="txtWO" runat="server" Width="400px" CssClass="Textbox_Form_White"></asp:TextBox>
                            </td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trRow" runat="server" style="border-bottom:solid 1px #efefef; cursor:pointer;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td class="Grid_Detail" align="center">
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblWO" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="5" PageSize="10" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </form>
</body>
</html>

﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class WMSFinding
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()
        Dim SQL As String = "select inv_Lot_No,REF_LOT_NO from WMS_WO wo left join SHIPMENT sh" & vbNewLine
        SQL &= "on wo.inv_Lot_No = sh.REF_LOT_NO and sh.SHIPMENT_TYPE_ID = 3" & vbNewLine
        SQL &= "where REF_LOT_NO Is null"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If

        Session("WMSFinding") = DT

        Navigation.SesssionSourceName = "WMSFinding"
        Navigation.RenderLayout()
    End Sub

    Private Sub FilterData()
        Dim DT As New DataTable
        DT = Session("WMSFinding")

        '--------------- Filter ----------------------
        Dim Filter As String = ""
        If txtWO.Text <> "" Then
            Filter &= "inv_Lot_No like '%" & txtWO.Text.Replace("'", "''") & "%'"
        End If
       
        DT.DefaultView.RowFilter = Filter
        DT = DT.DefaultView.ToTable
        If DT.Rows.Count <= 20 Then
            Navigation.Visible = False
        Else
            Navigation.Visible = True
        End If
        Session("WMSFinding_Filter") = DT

        Navigation.SesssionSourceName = "WMSFinding_Filter"
        Navigation.RenderLayout()
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblWO As Label = e.Item.FindControl("lblWO")
        Dim trRow As HtmlTableRow = e.Item.FindControl("trRow")

        lblNo.Text = e.Item.ItemIndex + 1
        lblWO.Text = e.Item.DataItem("inv_Lot_No")

        trRow.Attributes("onClick") = "returnSelectedValue('" & CStr(e.Item.DataItem("inv_Lot_No")).Replace("'", "\'") & "','" & Request.QueryString("txt").Replace("'", "\'") & "','" & Request.QueryString("btn").Replace("'", "\'") & "');"
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        FilterData()
    End Sub
End Class

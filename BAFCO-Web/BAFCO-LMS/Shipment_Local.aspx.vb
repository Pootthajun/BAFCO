﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Shipment_Local
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim BL As New ExportExcel

    Private Property ShipmentEdit() As Boolean
        Get
            Return ViewState("ShipmentEdit")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShipmentEdit") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlCompany(ddlCompany, "-------- All --------", "")
            MC.BindDDlStatus(ddlStatus, "-------- All --------", "", 3)

            If Session("User_Type_ID") > 0 Then
                Dim Sql As String = ""
                Sql = "Select ExportData,Edit From MS_USER_TYPE_MENU Where USER_TYPE_ID = " & Session("User_Type_ID") & " And MENU_ID = 3"
                Dim DA As New SqlDataAdapter(Sql, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)

                If DT.Rows(0).Item("ExportData").ToString.ToUpper = "TRUE" Then
                    btnExport.Visible = True
                Else
                    btnExport.Visible = False
                End If
                If DT.Rows(0).Item("Edit").ToString.ToUpper = "TRUE" Then
                    ShipmentEdit = True
                Else
                    ShipmentEdit = False
                End If
            ElseIf Session("User_Type_ID") = 0 Then
                'ลูกค้า
                ShipmentEdit = False
            Else
                btnExport.Visible = False
                ShipmentEdit = False
            End If

            ClearForm()
            BindData()
        End If
        Dim lblUName As Label
        lblUName = CType(Master.FindControl("lblUName"), Label)
        If lblUName.Text = "" And IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Shipment_Local.aspx';", True)
        End If

    End Sub

    Private Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        If txtPONo.Text.Trim = "" Then
            SQL = "SELECT * FROM VW_SHIPMENT_LOCAL_NONE_PO" & vbLf
            If Session("User_Type_ID") = 0 Then
                SQL &= "WHERE COMPANY_ID = " & Session("User_ID") & vbLf
            End If
            SQL &= "ORDER BY CREATE_DATE DESC"
        Else
            SQL = "SELECT * FROM VW_SHIPMENT_LOCAL" & vbLf
            If Session("User_Type_ID") = 0 Then
                SQL &= "WHERE COMPANY_ID = " & Session("User_ID") & vbLf
            End If
            SQL &= "ORDER BY CREATE_DATE DESC"
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Session("ShipmentLocal") = DT
        FilterData()
    End Sub

    Sub FilterData()
        Dim DT As New DataTable
        DT = Session("ShipmentLocal")
        If Not (DT Is Nothing) Then
            '--------------- Filter ----------------------
            Dim Filter As String = ""
            If ddlCompany.SelectedIndex > 0 Then
                Filter &= "COMPANY_ID =" & ddlCompany.SelectedValue & " AND "
            End If
            If ddlStatus.SelectedIndex > 0 Then
                Filter &= "STATUS_ID =" & ddlStatus.SelectedValue & " AND "
            End If
            If txtWorkOrderNo.Text.Trim <> "" Then
                Filter &= "WORK_ORDER_NO like '%" & txtWorkOrderNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtPONo.Text.Trim <> "" Then
                Filter &= "ALL_PO like '%" & txtPONo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtReceivedFrom.Text.Trim <> "" Then
                If MC.CheckDate(txtReceivedFrom.Text) Then
                    Filter &= "PRE_ALERT >=" & txtReceivedFrom.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtReceivedTo.Text.Trim <> "" Then
                If MC.CheckDate(txtReceivedTo.Text) Then
                    Filter &= "PRE_ALERT <=" & txtReceivedTo.Text.Replace("-", "") & " AND "
                End If
            End If

            If Filter <> "" Then Filter = Filter.Substring(0, Filter.Length - 4)
            DT.DefaultView.RowFilter = Filter
            DT = DT.DefaultView.ToTable
            If DT.Rows.Count <= 15 Then
                Navigation.Visible = False
            Else
                Navigation.Visible = True
            End If

            Session("ShipmentLocalFilter") = DT
            Navigation.SesssionSourceName = "ShipmentLocalFilter"
            Navigation.RenderLayout()
        End If
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblShipmentID As Label = e.Item.FindControl("lblShipmentID")
        Dim lblShipmentCode As Label = e.Item.FindControl("lblShipmentCode")
        Dim lblStatusID As Label = e.Item.FindControl("lblStatusID")
        Dim lblWorkOrder As Label = e.Item.FindControl("lblWorkOrder")
        Dim lblCompany As Label = e.Item.FindControl("lblCompany")
        Dim lblSupplier As Label = e.Item.FindControl("lblSupplier")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnEdit As HtmlAnchor = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnView As HtmlAnchor = e.Item.FindControl("btnView")
        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblShipmentID.Text = e.Item.DataItem("SHIPMENT_ID").ToString
        lblShipmentCode.Text = e.Item.DataItem("SHIPMENT_CODE").ToString
        lblStatusID.Text = e.Item.DataItem("STATUS_ID").ToString
        lblWorkOrder.Text = e.Item.DataItem("WORK_ORDER_NO").ToString
        lblCompany.Text = e.Item.DataItem("COMPANY_NAME").ToString
        lblSupplier.Text = e.Item.DataItem("SUPPLIER_NAME").ToString
        lblStatus.Text = e.Item.DataItem("STATUS_NAME").ToString

        If ShipmentEdit = True Then
            btnEdit.Visible = True
        Else
            btnEdit.Visible = False
        End If

        Dim URL As String = ""
        URL = "Local" & lblStatusID.Text & ".aspx?ShipmentCode=" & lblShipmentCode.Text
        btnEdit.Attributes("onclick") = "window.open('" & URL & "','','fullscreen, scrollbars');"

        URL = "Local_View.aspx?ShipmentCode=" & lblShipmentCode.Text
        btnView.Attributes("onclick") = "window.open('" & URL & "','','fullscreen, scrollbars');"
    End Sub

    Sub ClearForm()
        ddlCompany.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        txtWorkOrderNo.Text = ""
        txtPONo.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearForm()
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        If IsNothing(Session("ShipmentLocalFilter")) Then
            Exit Sub
        End If

        BindData()
        Dim DT As New DataTable
        DT = Session("ShipmentLocalFilter")

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('None data for Export');", True)
            Exit Sub
        End If

        For i As Int32 = 1 To DT.Columns.Count - 1
            DT.Columns.RemoveAt(1)
        Next

        Dim SQL As String = "SELECT * FROM VW_EXPORT_DATA_LOCAL"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_ As New DataTable
        DA.Fill(DT_)

        Dim DT_Compare As New DataTable
        DT_Compare = GL.JoinDataTable(DT, DT_, "SHIPMENT_ID", "SHIPMENT_ID")
        DT_Compare.Columns(0).ColumnName = "Item"
        DT_Compare.Columns.RemoveAt(1)
        For i As Int32 = 0 To DT_Compare.Rows.Count - 1
            DT_Compare.Rows(i).Item("Item") = i + 1
        Next
        Session("ExportDataToExcel") = DT_Compare
        Dim Filename As String = ""
        Filename = Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
        BL.ExportToExcel(Response, DT_Compare, Filename & ".xlsx")


        'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('ReportViewer.aspx');", True)
    End Sub
End Class

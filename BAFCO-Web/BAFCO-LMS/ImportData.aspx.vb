﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Configuration

Partial Class ImportData
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim CV As New Converter
    Dim MC As New MasterControlClass

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        If Not FileUpload1.HasFile Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select file to upload');", True)
            Exit Sub
        End If

        Dim Extension As String = ""
        If FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("VND.MS-EXCEL") > 0 Then
            Extension = ".xls"
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid File Type');", True)
            PanelACC.Visible = False
            Exit Sub
        End If

        Dim FileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
        Dim DataImport As String = ""
        If InStr(FileName.ToUpper, "ACCOUNTING INFO") > 0 Then
            DataImport = "ACC"
        ElseIf InStr(FileName.ToUpper, "COMPLETE CUSTOMS FORMALITY") > 0 Then
            DataImport = "CCF"
        End If
        Dim FolderPath As String = "Temp/"

        FileName = GetNewFileName()
        Dim FilePath As String = Server.MapPath(FolderPath + FileName)
        FileUpload1.SaveAs(FilePath)
        Try
            Import_To_DT(FilePath, Extension, DataImport)
        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('" & ex.Message & "');", True)
        End Try
        If File.Exists(FilePath) Then
            File.Delete(FilePath)
        End If
    End Sub

    Private Sub Import_To_DT(ByVal FilePath As String, ByVal Extension As String, ByVal DataImport As String)
        Dim conStr As String = ""
        conStr = ConfigurationManager.ConnectionStrings("ConnectionStringExcel").ConnectionString
        conStr = String.Format(conStr, FilePath, "Yes")

        Dim connExcel As New OleDbConnection(conStr)
        Dim cmdExcel As New OleDbCommand()
        Dim oda As New OleDbDataAdapter()
        Dim dt As New DataTable()

        cmdExcel.Connection = connExcel

        'Get the name of First Sheet 
        connExcel.Open()
        Dim dtExcelSchema As DataTable
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        Dim SheetName As String = dtExcelSchema.Rows(0)("TABLE_NAME").ToString()
        connExcel.Close()

        'Read Data from First Sheet 
        connExcel.Open()
        cmdExcel.CommandText = "SELECT * From [" & SheetName & "]"
        oda.SelectCommand = cmdExcel
        oda.Fill(dt)
        connExcel.Close()
        dt.Columns.Add("Status")

        If DataImport = "ACC" Then
            For i As Int32 = 0 To dt.Rows.Count - 1
                If dt.Rows((dt.Rows.Count - 1)).Item("Bafco Job No").ToString.Trim = "" Then
                    dt.Rows.RemoveAt((dt.Rows.Count - 1))
                End If
            Next
            PanelACC.Visible = True
            PanelCCF.Visible = False

            If dt.Rows.Count <= 15 Then
                trACC.Visible = False
            Else
                trACC.Visible = True
            End If
            Session("ACC") = dt
            NavigationACC.SesssionSourceName = "ACC"
            NavigationACC.RenderLayout()
        ElseIf DataImport = "CCF" Then
            'For i As Int32 = 0 To dt.Rows.Count - 1
            '    If dt.Rows((dt.Rows.Count - 1)).Item("Master List No").ToString.Trim = "" Then
            '        dt.Rows.RemoveAt((dt.Rows.Count - 1))
            '    End If
            'Next
            PanelCCF.Visible = True
            PanelACC.Visible = False

            If dt.Rows.Count <= 15 Then
                trCCF.Visible = False
            Else
                trCCF.Visible = True
            End If
            Session("CCF") = dt
            NavigationCCF.SesssionSourceName = "CCF"
            NavigationCCF.RenderLayout()
       
        End If

    End Sub

    Function GetNewFileName() As String
        Return Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
    End Function

    Protected Sub rptACC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptACC.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblJobNO As Label = e.Item.FindControl("lblJobNO")
        Dim lblInvoiceNo As Label = e.Item.FindControl("lblInvoiceNo")
        Dim lblSD As Label = e.Item.FindControl("lblSD")
        Dim lblCWT As Label = e.Item.FindControl("lblCWT")
        Dim lblApproveCWT As Label = e.Item.FindControl("lblApproveCWT")
        Dim lblWorkTicket As Label = e.Item.FindControl("lblWorkTicket")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        lblNo.Text = e.Item.DataItem("Item").ToString
        lblJobNO.Text = e.Item.DataItem("Bafco Job No").ToString
        lblInvoiceNo.Text = e.Item.DataItem("Invoice No").ToString
        If e.Item.DataItem("Billing Date").ToString <> "" Then
            If IsDate(e.Item.DataItem("Billing Date")) Then
                lblSD.Text = GL.ReportProgrammingDate(e.Item.DataItem("Billing Date"))
            Else
                lblSD.Text = "Invalid Format"
                lblStatus.Text = "Billing Date is invalid format."
            End If
        End If
        lblCWT.Text = e.Item.DataItem("CWT").ToString
        lblApproveCWT.Text = e.Item.DataItem("Approved CWT").ToString
        lblWorkTicket.Text = e.Item.DataItem("Work Ticket").ToString
        If lblStatus.Text <> "Billing Date is invalid format." Then
            lblStatus.Text = e.Item.DataItem("Status").ToString
        End If

        Select Case lblStatus.Text.Trim
            Case "Done"
                For i As Int32 = 1 To 8
                    Dim td As HtmlTableCell = e.Item.FindControl("tdACC" & i)
                    td.Style.Item("background-color") = "#33CC33"
                    td.Style.Item("color") = "#FFFFFF"
                Next
            Case Is <> ""
                For i As Int32 = 1 To 8
                    Dim td As HtmlTableCell = e.Item.FindControl("tdACC" & i)
                    td.Style.Item("background-color") = "#FF3333"
                    td.Style.Item("color") = "#FFFFFF"
                Next
        End Select
    End Sub

    Protected Sub NavigationACC_PageChanging(ByVal Sender As PageNavigation) Handles NavigationACC.PageChanging
        NavigationACC.TheRepeater = rptACC
    End Sub

    Protected Sub rptCCF_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCCF.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblJEntryNo As Label = e.Item.FindControl("lblJEntryNo")
        Dim lblListNo As Label = e.Item.FindControl("lblListNo")
        Dim lblLetterNo As Label = e.Item.FindControl("lblLetterNo")
        Dim lblClearanceDate As Label = e.Item.FindControl("lblClearanceDate")
        Dim lblDutySubject As Label = e.Item.FindControl("lblDutySubject")
        Dim lblNumberOfDutiable As Label = e.Item.FindControl("lblNumberOfDutiable")
        Dim lblPackInCase As Label = e.Item.FindControl("lblPackInCase")
        Dim lblExpenses As Label = e.Item.FindControl("lblExpenses")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")

        lblNo.Text = e.Item.DataItem("Item").ToString
        lblJEntryNo.Text = e.Item.DataItem("Import Entry No").ToString
        lblListNo.Text = e.Item.DataItem("Master List No").ToString
        lblLetterNo.Text = e.Item.DataItem("DMF's Letter No").ToString
        If e.Item.DataItem("Absolute Clearance Date").ToString <> "" Then
            If IsDate(e.Item.DataItem("Absolute Clearance Date")) Then
                lblClearanceDate.Text = GL.ReportProgrammingDate(e.Item.DataItem("Absolute Clearance Date"))
            Else
                lblClearanceDate.Text = "Invalid Format"
                lblStatus.Text = "Absolute Clearance Date is invalid format."
            End If
        End If
        lblDutySubject.Text = e.Item.DataItem("Duty Subjected by DMF").ToString
        If IsNumeric(e.Item.DataItem("Number of Dutiable Item").ToString) Then
            lblNumberOfDutiable.Text = FormatNumber(e.Item.DataItem("Number of Dutiable Item"), 0)
        Else
            lblNumberOfDutiable.Text = "Invalid Format"
            If lblStatus.Text <> "Absolute Clearance Date is invalid format." Then
                lblStatus.Text = "Number of Dutiable Item is invalid format."
            End If
        End If

        lblPackInCase.Text = e.Item.DataItem("Pack in Case").ToString
        lblExpenses.Text = e.Item.DataItem("Expenses").ToString
        If lblStatus.Text <> "Absolute Clearance Date is invalid format." AndAlso lblStatus.Text <> "Number of Dutiable Item is invalid format." Then
            lblStatus.Text = e.Item.DataItem("Status").ToString
        End If

        Select Case lblStatus.Text.Trim
            Case "Done"
                For i As Int32 = 1 To 10
                    Dim td As HtmlTableCell = e.Item.FindControl("tdCCF" & i)
                    td.Style.Item("background-color") = "#33CC33"
                    td.Style.Item("color") = "#FFFFFF"
                Next
            Case Is <> ""
                For i As Int32 = 1 To 10
                    Dim td As HtmlTableCell = e.Item.FindControl("tdCCF" & i)
                    td.Style.Item("background-color") = "#FF3333"
                    td.Style.Item("color") = "#FFFFFF"
                Next
        End Select
    End Sub

    Protected Sub NavigationCCF_PageChanging(ByVal Sender As PageNavigation) Handles NavigationCCF.PageChanging
        NavigationCCF.TheRepeater = rptCCF
    End Sub

    Protected Sub btnUpdateACC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateACC.Click
        Dim DT_ACC As New DataTable
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        DT_ACC = NavigationACC.Datasource
        For i As Int32 = 0 To DT_ACC.Rows.Count - 1
            Dim SQL As String = ""
            SQL &= "SELECT	SHIPMENT.SHIPMENT_ID,SHIPMENT_TYPE_ID," & vbNewLine
            SQL &= "ISNULL(IMPORT_PREPARE.JOB_NO,EXPORT_RECEIVE.JOB_NO) AS JOB_NO," & vbNewLine
            SQL &= "ISNULL(IMPORT_RECEIVE.INVOICE_NO,EXPORT_RECEIVE.INVOICE_NO) AS INVOICE_NO" & vbNewLine
            SQL &= "FROM SHIPMENT LEFT JOIN IMPORT_RECEIVE ON IMPORT_RECEIVE.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID " & vbNewLine
            SQL &= "LEFT JOIN IMPORT_PREPARE ON IMPORT_PREPARE.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID" & vbNewLine
            SQL &= "LEFT JOIN EXPORT_RECEIVE ON EXPORT_RECEIVE.SHIPMENT_ID = SHIPMENT.SHIPMENT_ID" & vbNewLine
            SQL &= "WHERE SHIPMENT_TYPE_ID IN (1,2)" & vbNewLine
            SQL &= "AND ISNULL(IMPORT_PREPARE.JOB_NO,EXPORT_RECEIVE.JOB_NO) = '" & DT_ACC.Rows(i).Item("Bafco Job No").ToString.Replace("'", "''") & "'" & vbNewLine
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                Dim ShipmentID As String = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Select Case DT.Rows(0).Item("SHIPMENT_TYPE_ID").ToString
                    Case 1
                        'IMPORT
                        If DT_ACC.Rows(i).Item("Status").ToString <> "Billing Date is invalid format." Then
                            SQL = "SELECT * FROM IMPORT_DELIVER WHERE SHIPMENT_ID = " & ShipmentID
                            DA = New SqlDataAdapter(SQL, ConnStr)
                            DT = New DataTable
                            DA.Fill(DT)
                            Dim DR As DataRow
                            If DT.Rows.Count = 0 Then
                                'Add
                                DT.Rows.Clear()
                                DR = DT.NewRow
                                DR("SHIPMENT_ID") = ShipmentID
                                DR("CREATE_BY") = Session("User_ID")
                                DR("CREATE_DATE") = Now
                            Else
                                'Edit
                                DR = DT.Rows(0)
                                DR("UPDATE_BY") = Session("User_ID")
                                DR("UPDATE_DATE") = Now
                            End If
                            DR("CWT") = DT_ACC.Rows(i).Item("CWT").ToString
                            If DT_ACC.Rows(i).Item("Billing Date").ToString <> "" Then
                                If IsDate(DT_ACC.Rows(i).Item("Billing Date")) Then
                                    DR("STATUS_DELIVER") = DT_ACC.Rows(i).Item("Billing Date")
                                Else
                                    DR("STATUS_DELIVER") = CV.StringToDate(DT_ACC.Rows(i).Item("Billing Date"), "yyyy-MM-dd")
                                End If
                            Else
                                DR("STATUS_DELIVER") = DBNull.Value
                            End If
                            DR("WORK_TICKET") = DT_ACC.Rows(i).Item("Work Ticket").ToString
                            DR("APPROVE_CWT") = DT_ACC.Rows(i).Item("Approved CWT").ToString
                            DR("ACC_INVOICE_NO") = DT_ACC.Rows(i).Item("Invoice No").ToString
                            Dim cmd As New SqlCommandBuilder(DA)
                            If DT.Rows.Count = 0 Then
                                DT.Rows.Add(DR)
                            End If
                            DA.Update(DT)

                            SQL = "SELECT * FROM IMPORT_RECEIVE WHERE SHIPMENT_ID = " & ShipmentID
                            DA = New SqlDataAdapter(SQL, ConnStr)
                            DT = New DataTable
                            DA.Fill(DT)
                            Dim DR_ As DataRow
                            If DT.Rows.Count = 0 Then
                                'Add
                                DT.Rows.Clear()
                                DR_ = DT.NewRow
                                DR_("SHIPMENT_ID") = ShipmentID
                                DR_("CREATE_BY") = Session("User_ID")
                                DR_("CREATE_DATE") = Now
                            Else
                                'Edit
                                DR_ = DT.Rows(0)
                                DR_("UPDATE_BY") = Session("User_ID")
                                DR_("UPDATE_DATE") = Now
                            End If
                            Dim cmd_ As New SqlCommandBuilder(DA)
                            If DT.Rows.Count = 0 Then
                                DT.Rows.Add(DR_)
                            End If
                            DA.Update(DT)

                            '************ Update Status Shipment *************
                            SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
                            Dim DA_SH As New SqlDataAdapter(SQL, ConnStr)
                            Dim DT_SH As New DataTable
                            Dim DR_SH As DataRow
                            DA_SH.Fill(DT_SH)
                            If CInt(DT_SH.Rows(0).Item("STATUS_ID").ToString) < 5 Then
                                DR_SH = DT_SH.Rows(0)
                                DR_SH("STATUS_ID") = "5"
                                Dim cmd_SH As New SqlCommandBuilder(DA_SH)
                                DA_SH.Update(DT_SH)
                            End If
                            '*************************************************
                            DT_ACC.Rows(i).Item("Status") = "Done"
                        End If
                    Case 2
                        'EXPORT
                        If DT_ACC.Rows(i).Item("Status").ToString <> "Billing Date is invalid format." Then
                            SQL = "SELECT * FROM EXPORT_ACCOUNTING WHERE SHIPMENT_ID = " & ShipmentID
                            DA = New SqlDataAdapter(SQL, ConnStr)
                            DT = New DataTable
                            DA.Fill(DT)
                            Dim DR As DataRow
                            If DT.Rows.Count = 0 Then
                                'Add
                                DT.Rows.Clear()
                                DR = DT.NewRow
                                DR("SHIPMENT_ID") = ShipmentID
                                DR("CREATE_BY") = Session("User_ID")
                                DR("CREATE_DATE") = Now
                            Else
                                'Edit
                                DR = DT.Rows(0)
                                DR("UPDATE_BY") = Session("User_ID")
                                DR("UPDATE_DATE") = Now
                            End If
                            DR("CWT") = DT_ACC.Rows(i).Item("CWT").ToString
                            If DT_ACC.Rows(i).Item("Billing Date").ToString <> "" Then
                                If IsDate(DT_ACC.Rows(i).Item("Billing Date")) Then
                                    DR("STATUS_DELIVER") = DT_ACC.Rows(i).Item("Billing Date")
                                Else
                                    DR("STATUS_DELIVER") = CV.StringToDate(DT_ACC.Rows(i).Item("Billing Date"), "yyyy-MM-dd")
                                End If
                            Else
                                DR("STATUS_DELIVER") = DBNull.Value
                            End If
                            DR("WORK_TICKET") = DT_ACC.Rows(i).Item("Work Ticket").ToString
                            DR("APPROVE_CWT") = DT_ACC.Rows(i).Item("Approved CWT").ToString
                            DR("ACC_INVOICE_NO") = DT_ACC.Rows(i).Item("Invoice No").ToString
                            DR("UPDATE_BY") = Session("User_ID")
                            DR("UPDATE_DATE") = Now
                            Dim cmd As New SqlCommandBuilder(DA)
                            If DT.Rows.Count = 0 Then
                                DT.Rows.Add(DR)
                            End If
                            DA.Update(DT)

                            SQL = "SELECT * FROM EXPORT_RECEIVE WHERE SHIPMENT_ID = " & ShipmentID
                            DA = New SqlDataAdapter(SQL, ConnStr)
                            DT = New DataTable
                            DA.Fill(DT)
                            Dim DR_ As DataRow
                            If DT.Rows.Count = 0 Then
                                'Add
                                DT.Rows.Clear()
                                DR_ = DT.NewRow
                                DR_("SHIPMENT_ID") = ShipmentID
                                DR_("CREATE_BY") = Session("User_ID")
                                DR_("CREATE_DATE") = Now
                            Else
                                'Edit
                                DR_ = DT.Rows(0)
                                DR_("UPDATE_BY") = Session("User_ID")
                                DR_("UPDATE_DATE") = Now
                            End If
                            Dim cmd_ As New SqlCommandBuilder(DA)
                            If DT.Rows.Count = 0 Then
                                DT.Rows.Add(DR_)
                            End If
                            DA.Update(DT)

                            '************ Update Status Shipment *************
                            SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
                            Dim DA_SH As New SqlDataAdapter(SQL, ConnStr)
                            Dim DT_SH As New DataTable
                            Dim DR_SH As DataRow
                            DA_SH.Fill(DT_SH)
                            If CInt(DT_SH.Rows(0).Item("STATUS_ID").ToString) < 8 Then
                                DR_SH = DT_SH.Rows(0)
                                DR_SH("STATUS_ID") = "8"
                                Dim cmd_SH As New SqlCommandBuilder(DA_SH)
                                DA_SH.Update(DT_SH)
                            End If
                            '*************************************************

                            DT_ACC.Rows(i).Item("Status") = "Done"
                        End If
                End Select
            Else
                DT_ACC.Rows(i).Item("Status") = "Either Bafco Job No doesn't exists."
            End If
        Next

        If DT_ACC.Rows.Count <= 15 Then
            trACC.Visible = False
        Else
            trACC.Visible = True
        End If

        Session("ACC") = DT_ACC
        NavigationACC.SesssionSourceName = "ACC"
        NavigationACC.RenderLayout()
    End Sub

    Protected Sub btnUpdateCCF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCCF.Click
        Dim DT_CCF As New DataTable
        Dim DT As New DataTable
        Dim DA As New SqlDataAdapter
        DT_CCF = NavigationCCF.Datasource
        For i As Int32 = 0 To DT_CCF.Rows.Count - 1
            Dim SQL As String = ""
            SQL &= "SELECT SHIPMENT_ID FROM IMPORT_PREPARE WHERE IMPORT_ENTRY_NO = '" & DT_CCF.Rows(i).Item("Import Entry No").ToString.Replace("'", "''") & "'"
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                Dim ShipmentID As String = DT.Rows(0).Item("SHIPMENT_ID").ToString
                If DT_CCF.Rows(i).Item("Status").ToString <> "Absolute Clearance Date is invalid format." AndAlso DT_CCF.Rows(i).Item("Status").ToString <> "Number of Dutiable Item is invalid format." Then
                    SQL = "SELECT * FROM IMPORT_COMPLETE WHERE SHIPMENT_ID = " & ShipmentID
                    DA = New SqlDataAdapter(SQL, ConnStr)
                    DT = New DataTable
                    DA.Fill(DT)
                    Dim DR As DataRow
                    If DT.Rows.Count = 0 Then
                        'Add
                        DT.Rows.Clear()
                        DR = DT.NewRow
                        DR("SHIPMENT_ID") = ShipmentID
                        DR("CREATE_BY") = Session("User_ID")
                        DR("CREATE_DATE") = Now
                    Else
                        'Edit
                        DR = DT.Rows(0)
                        DR("UPDATE_BY") = Session("User_ID")
                        DR("UPDATE_DATE") = Now
                    End If
                    DR("LIST_NO") = DT_CCF.Rows(i).Item("Master List No").ToString
                    DR("LETTER_NO") = DT_CCF.Rows(i).Item("DMF's Letter No").ToString
                    If DT_CCF.Rows(i).Item("Absolute Clearance Date").ToString <> "" Then
                        If IsDate(DT_CCF.Rows(i).Item("Absolute Clearance Date")) Then
                            DR("CLEARANCE_DATE") = DT_CCF.Rows(i).Item("Absolute Clearance Date")
                        Else
                            DR("CLEARANCE_DATE") = CV.StringToDate(DT_CCF.Rows(i).Item("Absolute Clearance Date"), "yyyy-MM-dd")
                        End If
                    Else
                        DR("CLEARANCE_DATE") = DBNull.Value
                    End If
                    DR("DUTY_SUBJECT") = DT_CCF.Rows(i).Item("Duty Subjected by DMF").ToString
                    DR("NUMBER_OF_DUTY") = DT_CCF.Rows(i).Item("Number of Dutiable Item").ToString
                    DR("PACK_IN_CASE") = DT_CCF.Rows(i).Item("Pack in Case").ToString
                    DR("EXPENSES") = DT_CCF.Rows(i).Item("Expenses").ToString
                    Dim cmd As New SqlCommandBuilder(DA)
                    If DT.Rows.Count = 0 Then
                        DT.Rows.Add(DR)
                    End If
                    DA.Update(DT)

                    '************ Update Status Shipment *************
                    SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
                    Dim DA_SH As New SqlDataAdapter(SQL, ConnStr)
                    Dim DT_SH As New DataTable
                    Dim DR_SH As DataRow
                    DA_SH.Fill(DT_SH)
                    If CInt(DT_SH.Rows(0).Item("STATUS_ID").ToString) < 6 Then
                        DR_SH = DT_SH.Rows(0)
                        DR_SH("STATUS_ID") = "6"
                        Dim cmd_SH As New SqlCommandBuilder(DA_SH)
                        DA_SH.Update(DT_SH)
                    End If
                    '*************************************************

                    DT_CCF.Rows(i).Item("Status") = "Done"
                End If
            Else
                DT_CCF.Rows(i).Item("Status") = "Either Import Entry No doesn't exists."
            End If
        Next

        If DT_CCF.Rows.Count <= 15 Then
            trCCF.Visible = False
        Else
            trCCF.Visible = True
        End If

        Session("CCF") = DT_CCF
        NavigationCCF.SesssionSourceName = "CCF"
        NavigationCCF.RenderLayout()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FileUpload1.Attributes("onchange") = "document.getElementById('" & btnUpload.ClientID & "').click();"
        End If
    End Sub
End Class

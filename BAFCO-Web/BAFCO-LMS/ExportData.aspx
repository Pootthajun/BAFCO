﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ExportData.aspx.vb" Inherits="ExportData" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<%@ Register src="PageNavigation.ascx" tagname="PageNavigation" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
        <ContentTemplate>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">    
            <tr>
                <td valign="top">
                     <table align="left" cellpadding="0" cellspacing="1">
                        <tr>
                            <td>
                                <div class="Fieldset_Container" >
                                    <div class="Fieldset_Header"><asp:Label ID="Label1" runat="server" BackColor="white" Text="Filter"></asp:Label></div>
                                    <table width="800px" align="center" cellspacing="5px;" style="margin-left:10px; ">
                                        <tr>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Branch</td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlBranch" runat="server" 
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Shipment Type
                                            </td>
                                            <td width="145" height="25" colspan="2">
                                                <asp:CheckBoxList ID="cblShipmentType" runat="server" RepeatColumns="5" AutoPostBack="true"></asp:CheckBoxList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Company
                                            </td>
                                            <td width="145" height="25">
                                                <asp:DropDownList ID="ddlCompany" runat="server" 
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Work Order No
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtWorkOrderNo" runat="server" CssClass="Textbox_Form_White" width="145px"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                PO No</td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtPONo" runat="server" CssClass="Textbox_Form_White" 
                                                    width="145px" ></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Invoice No
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="Textbox_Form_White" 
                                                width="145px" ></asp:TextBox>
                                            </td>
                                            <td width="40" height="25"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="150" class="style1">
                                                Transportation Type
                                            </td>
                                            <td width="145" class="style2">
                                                <asp:DropDownList ID="ddlTransportationType" runat="server" 
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" class="style2"></td>
                                            <td width="150" class="style1">
                                                Port of Destination
                                            </td>
                                            <td width="145" class="style2">
                                                <asp:DropDownList ID="ddlPortOfDestination" runat="server" 
                                                CssClass="Dropdown_Form_White" width="150px" ></asp:DropDownList>
                                            </td>
                                            <td width="40" class="style2"></td>
                                            <td class="style2"></td>
                                        </tr>
                                        <tr>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Pre-alert Date From
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtPreAlertFrom" runat="server" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgPreAlertFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgPreAlertFrom_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgPreAlertFrom"
				                                TargetControlID="txtPreAlertFrom"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Pre-alert Date To
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtPreAlertTo" runat="server" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgPreAlertTo" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgPreAlertTo_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgPreAlertTo"
				                                TargetControlID="txtPreAlertTo"></Ajax:CalendarExtender>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Received Date From
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtReceivedFrom" runat="server" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgReceivedFrom" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgReceivedFrom_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgReceivedFrom"
				                                TargetControlID="txtReceivedFrom"></Ajax:CalendarExtender>
                                            </td>
                                            <td width="150" height="25" class="NormalTextBlack">
                                                Received Date To
                                            </td>
                                            <td width="145" height="25">
                                                <asp:TextBox ID="txtReceivedTo" runat="server" CssClass="Textbox_Form_White" width="145px" placeholder="YYYY-MM-DD"></asp:TextBox>
                                            </td>
                                            <td width="40" height="25" align="left">
                                                <asp:ImageButton ID="imgReceivedTo" runat="server" ImageUrl="images/Calendar.png" />
                                                <Ajax:CalendarExtender ID="imgReceivedTo_CDE" runat="server" 
                                                Format="yyyy-MM-dd"  PopupButtonID="imgReceivedTo"
				                                TargetControlID="txtReceivedTo"></Ajax:CalendarExtender>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>       
                </td>
            </tr>
            <tr>
                <td height="20" colspan="1" valign="middle">
                    <table cellpadding="0" cellspacing="2" style="margin: 0px 0px 10px 10px;">
                        <tr>
                            <td>
                                <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                            <td>
                                <asp:Button ID="btnExport" runat="server" Text="Export" CssClass="Button_Red" Width="100px" Height="30px"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="1" style="margin: 0px 0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                        <tr>
                            <td width="30" class="Grid_Header">
                                No
                            </td>
                            <td width="150" class="Grid_Header">
                                Work Order
                            </td>
                            <td width="100" class="Grid_Header">
                                Company
                            </td>
                            <td width="300" class="Grid_Header">
                                Supplier
                            </td>
                            <td width="80" class="Grid_Header">
                                Type
                            </td>
                            <td width="200" class="Grid_Header">
                                Status
                            </td>
                        </tr>
                        <asp:Repeater ID="rptData" runat="server">
                            <ItemTemplate>
                                <tr id="trRow" runat="server" style="border-bottom:solid 1px #efefef; cursor:pointer;" onmouseover="this.bgColor='#DAE7FC';" onmouseout="this.bgColor='FFFFFF';">
                                    <td class="Grid_Detail" align="center">
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        <asp:Label ID="lblShipmentID" runat="server" visible="false"></asp:Label>
                                        <asp:Label ID="lblShipmentCode" runat="server" visible="false"></asp:Label>
                                        <asp:Label ID="lblStatusID" runat="server" visible="false"></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblWorkOrder" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblCompany" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblSupplier" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail" align="center">
                                        <asp:Label ID="lblType" runat="server" ></asp:Label>
                                    </td>
                                    <td class="Grid_Detail">
                                        <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div style="margin-left:10px">
                        <uc1:PageNavigation ID="Navigation" runat="server" MaximunPageCount="10" PageSize="15" Visible="false"/>
                    </div>
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


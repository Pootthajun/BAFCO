﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Import2
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment_Import.aspx", True)
                Exit Sub
            Else
                Dim SQL_ As String = ""
                SQL_ = "SELECT SHIPMENT_ID,STATUS_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA_ As New SqlDataAdapter(SQL_, ConnStr)
                Dim DT_ As New DataTable
                DA_.Fill(DT_)
                If DT_.Rows.Count > 0 Then
                    ShipmentID = DT_.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment_Import.aspx", True)
                    Exit Sub
                End If
            End If

            MC.BindDDlPortOfDestination(ddlPortOfDestination, "--------- Select ---------", "")
            CL.ImplementJavaIntegerText(txtDutyTax)
            CL.ImplementJavaIntegerText(txtExcisesTax)
            CL.ImplementJavaIntegerText(txtMOI)

            SetMenu()
            BindData()

            txtIssueDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtJobNo.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtETADate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtDODate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtSignDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtImportEntryNo.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtImportEntryDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtDutyTax.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtDutyTaxDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtExcisesTax.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtExcisesTaxDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtMOI.Attributes.Add("onkeydown", "return (event.keyCode!=13);")

            btnStatus1.Attributes("onclick") = "window.location.href='Import1.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus3.Attributes("onclick") = "window.location.href='Import3.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus4.Attributes("onclick") = "window.location.href='Import4.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus5.Attributes("onclick") = "window.location.href='Import5.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus6.Attributes("onclick") = "window.location.href='Import6.aspx?ShipmentCode=" & ShipmentCode & "';"
        End If
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_PREPARE WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("ISSUE_DATE").ToString <> "" Then
                txtIssueDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("ISSUE_DATE"))
            Else
                txtIssueDate.Text = ""
            End If
            txtJobNo.Text = DT.Rows(0).Item("JOB_NO").ToString
            ddlPortOfDestination.SelectedValue = DT.Rows(0).Item("DESTINATION_ID").ToString
            If DT.Rows(0).Item("ETA_DATE").ToString <> "" Then
                txtETADate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("ETA_DATE"))
            Else
                txtETADate.Text = ""
            End If
            If DT.Rows(0).Item("ETA_TIME_H").ToString <> "" Then
                ddlETATime_H.SelectedValue = DT.Rows(0).Item("ETA_TIME_H").ToString
                ddlETATime_M.SelectedValue = DT.Rows(0).Item("ETA_TIME_M").ToString
            Else
                ddlETATime_H.SelectedIndex = 0
                ddlETATime_M.SelectedIndex = 0
            End If
            If DT.Rows(0).Item("DO_DATE").ToString <> "" Then
                txtDODate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("DO_DATE"))
            Else
                txtDODate.Text = ""
            End If
            If DT.Rows(0).Item("SIGN_DATE").ToString <> "" Then
                txtSignDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("SIGN_DATE"))
            Else
                txtSignDate.Text = ""
            End If
            If DT.Rows(0).Item("SIGN_TIME_H").ToString <> "" Then
                ddlSignTime_H.SelectedValue = DT.Rows(0).Item("SIGN_TIME_H").ToString
                ddlSignTime_M.SelectedValue = DT.Rows(0).Item("SIGN_TIME_M").ToString
            Else
                ddlSignTime_H.SelectedIndex = 0
                ddlSignTime_M.SelectedIndex = 0
            End If
            txtImportEntryNo.Text = DT.Rows(0).Item("IMPORT_ENTRY_NO").ToString
            If DT.Rows(0).Item("IMPORT_ENTRY_DATE").ToString <> "" Then
                txtImportEntryDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("IMPORT_ENTRY_DATE"))
            Else
                txtImportEntryDate.Text = ""
            End If
            If DT.Rows(0).Item("IMPORT_ENTRY_TIME_H").ToString <> "" Then
                ddlImportEntryTime_H.SelectedValue = DT.Rows(0).Item("IMPORT_ENTRY_TIME_H").ToString
                ddlImportEntryTime_M.SelectedValue = DT.Rows(0).Item("IMPORT_ENTRY_TIME_M").ToString
            Else
                ddlImportEntryTime_H.SelectedIndex = 0
                ddlImportEntryTime_M.SelectedIndex = 0
            End If
            If DT.Rows(0).Item("DUTY").ToString <> "" Then
                txtDutyTax.Text = FormatNumber(DT.Rows(0).Item("DUTY").ToString, 0)
            Else
                txtDutyTax.Text = ""
            End If
            If DT.Rows(0).Item("DUTY_DATE").ToString <> "" Then
                txtDutyTaxDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("DUTY_DATE"))
            Else
                txtDutyTaxDate.Text = ""
            End If
            If DT.Rows(0).Item("EXCISES").ToString <> "" Then
                txtExcisesTax.Text = FormatNumber(DT.Rows(0).Item("EXCISES").ToString, 0)
            Else
                txtExcisesTax.Text = ""
            End If
            If DT.Rows(0).Item("EXCISES_DATE").ToString <> "" Then
                txtExcisesTaxDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("EXCISES_DATE"))
            Else
                txtExcisesTaxDate.Text = ""
            End If
            If DT.Rows(0).Item("MOI").ToString <> "" Then
                txtMOI.Text = FormatNumber(DT.Rows(0).Item("MOI").ToString, 0)
            Else
                txtMOI.Text = ""
            End If

        End If

    End Sub

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        If txtETADate.Text <> "" Then
            If Not MC.CheckDate(txtETADate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('ETA Date is not valid');", True)
                Exit Sub
            End If
        End If

        If (ddlETATime_H.SelectedIndex = 0 And ddlETATime_M.SelectedIndex > 0) Or (ddlETATime_H.SelectedIndex > 0 And ddlETATime_M.SelectedIndex = 0) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select ETA Time');", True)
            Exit Sub
        End If

        If txtDODate.Text <> "" Then
            If Not MC.CheckDate(txtDODate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('D/O Receive Date is not valid');", True)
                Exit Sub
            End If
        End If

        If txtSignDate.Text <> "" Then
            If Not MC.CheckDate(txtSignDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Sign Date is not valid');", True)
                Exit Sub
            End If
        End If

        If (ddlSignTime_H.SelectedIndex = 0 And ddlSignTime_M.SelectedIndex > 0) Or (ddlSignTime_H.SelectedIndex > 0 And ddlSignTime_M.SelectedIndex = 0) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select Sign Time');", True)
            Exit Sub
        End If

        If txtImportEntryDate.Text <> "" Then
            If Not MC.CheckDate(txtImportEntryDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Import Entry Date is not valid');", True)
                Exit Sub
            End If
        End If

        If (ddlImportEntryTime_H.SelectedIndex = 0 And ddlImportEntryTime_M.SelectedIndex > 0) Or (ddlImportEntryTime_H.SelectedIndex > 0 And ddlImportEntryTime_M.SelectedIndex = 0) Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select Import Entry Time');", True)
            Exit Sub
        End If

        If txtDutyTaxDate.Text <> "" Then
            If Not MC.CheckDate(txtDutyTaxDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Duty Tax Paid On is not valid');", True)
                Exit Sub
            End If
        End If

        If txtExcisesTaxDate.Text <> "" Then
            If Not MC.CheckDate(txtExcisesTaxDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Excises Tax Paid On is not valid');", True)
                Exit Sub
            End If
        End If
        '************************************************
        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_PREPARE WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            '*** Validation Bafco Job No***
            If txtJobNo.Text.Trim <> "" Then
                SQL = ""
                SQL &= "SELECT * FROM (" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM IMPORT_PREPARE" & vbNewLine
                SQL &= "UNION ALL" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM EXPORT_RECEIVE" & vbNewLine
                SQL &= ") AS TB" & vbNewLine
                SQL &= "WHERE JOB_NO = '" & txtJobNo.Text.Trim.Replace("'", "''") & "'"
                Dim DA_VD As New SqlDataAdapter(SQL, ConnStr)
                Dim DT_VD As New DataTable
                DA_VD.Fill(DT_VD)
                If DT_VD.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('BAFCO Job No is already exists');", True)
                    Exit Sub
                End If
            End If
            '******************************
            '*** Validation Import Entry No ***
            If txtImportEntryNo.Text.Trim <> "" Then
                SQL = ""
                SQL &= "SELECT * FROM IMPORT_PREPARE" & vbNewLine
                SQL &= "WHERE IMPORT_ENTRY_NO = '" & txtImportEntryNo.Text.Trim.Replace("'", "''") & "'"
                Dim DA_VD As New SqlDataAdapter(SQL, ConnStr)
                Dim DT_VD As New DataTable
                DA_VD.Fill(DT_VD)
                If DT_VD.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Import Entry No is already exists');", True)
                    Exit Sub
                End If
            End If
            '*********************************
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            '*** Validation Bafco Job No***
            If txtJobNo.Text.Trim <> "" Then
                SQL = ""
                SQL &= "SELECT * FROM (" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM IMPORT_PREPARE" & vbNewLine
                SQL &= "UNION ALL" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM EXPORT_RECEIVE" & vbNewLine
                SQL &= ") AS TB" & vbNewLine
                SQL &= "WHERE JOB_NO = '" & txtJobNo.Text.Trim.Replace("'", "''") & "'" & vbNewLine
                SQL &= "AND SHIPMENT_ID <> " & ShipmentID
                Dim DA_VD As New SqlDataAdapter(SQL, ConnStr)
                Dim DT_VD As New DataTable
                DA_VD.Fill(DT_VD)
                If DT_VD.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('BAFCO Job No is already exists');", True)
                    Exit Sub
                End If
            End If
            '******************************
            '*** Validation Import Entry No ***
            If txtImportEntryNo.Text.Trim <> "" Then
                SQL = ""
                SQL &= "SELECT * FROM IMPORT_PREPARE" & vbNewLine
                SQL &= "WHERE IMPORT_ENTRY_NO = '" & txtImportEntryNo.Text.Trim.Replace("'", "''") & "'" & vbNewLine
                SQL &= "AND SHIPMENT_ID <> " & ShipmentID
                Dim DA_VD As New SqlDataAdapter(SQL, ConnStr)
                Dim DT_VD As New DataTable
                DA_VD.Fill(DT_VD)
                If DT_VD.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Import Entry No is already exists');", True)
                    Exit Sub
                End If
            End If
            '*********************************
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If

        If txtIssueDate.Text.Trim <> "" Then
            DR("ISSUE_DATE") = CV.StringToDate(txtIssueDate.Text, "yyyy-MM-dd")
        Else
            DR("ISSUE_DATE") = DBNull.Value
        End If
        DR("JOB_NO") = txtJobNo.Text
        DR("DESTINATION_ID") = ddlPortOfDestination.SelectedValue.ToString
        If txtETADate.Text.Trim <> "" Then
            DR("ETA_DATE") = CV.StringToDate(txtETADate.Text, "yyyy-MM-dd")
        Else
            DR("ETA_DATE") = DBNull.Value
        End If
        If ddlETATime_H.SelectedIndex > 0 And ddlETATime_M.SelectedIndex > 0 Then
            DR("ETA_TIME_H") = ddlETATime_H.SelectedValue.ToString
            DR("ETA_TIME_M") = ddlETATime_M.SelectedValue.ToString
        Else
            DR("ETA_TIME_H") = DBNull.Value
            DR("ETA_TIME_M") = DBNull.Value
        End If
        If txtDODate.Text.Trim <> "" Then
            DR("DO_DATE") = CV.StringToDate(txtDODate.Text, "yyyy-MM-dd")
        Else
            DR("DO_DATE") = DBNull.Value
        End If
        If txtSignDate.Text.Trim <> "" Then
            DR("SIGN_DATE") = CV.StringToDate(txtSignDate.Text, "yyyy-MM-dd")
        Else
            DR("SIGN_DATE") = DBNull.Value
        End If
        If ddlSignTime_H.SelectedIndex > 0 And ddlSignTime_M.SelectedIndex > 0 Then
            DR("SIGN_TIME_H") = ddlSignTime_H.SelectedValue.ToString
            DR("SIGN_TIME_M") = ddlSignTime_M.SelectedValue.ToString
        Else
            DR("SIGN_TIME_H") = DBNull.Value
            DR("SIGN_TIME_M") = DBNull.Value
        End If
        DR("IMPORT_ENTRY_NO") = txtImportEntryNo.Text
        If txtImportEntryDate.Text.Trim <> "" Then
            DR("IMPORT_ENTRY_DATE") = CV.StringToDate(txtImportEntryDate.Text, "yyyy-MM-dd")
        Else
            DR("IMPORT_ENTRY_DATE") = DBNull.Value
        End If
        If ddlImportEntryTime_H.SelectedIndex > 0 And ddlImportEntryTime_M.SelectedIndex > 0 Then
            DR("IMPORT_ENTRY_TIME_H") = ddlImportEntryTime_H.SelectedValue.ToString
            DR("IMPORT_ENTRY_TIME_M") = ddlImportEntryTime_M.SelectedValue.ToString
        Else
            DR("IMPORT_ENTRY_TIME_H") = DBNull.Value
            DR("IMPORT_ENTRY_TIME_M") = DBNull.Value
        End If
        If txtDutyTax.Text.Trim <> "" Then
            DR("DUTY") = txtDutyTax.Text.Replace(",", "")
        Else
            DR("DUTY") = DBNull.Value
        End If
        If txtDutyTaxDate.Text.Trim <> "" Then
            DR("DUTY_DATE") = CV.StringToDate(txtDutyTaxDate.Text, "yyyy-MM-dd")
        Else
            DR("DUTY_DATE") = DBNull.Value
        End If
        If txtExcisesTax.Text.Trim <> "" Then
            DR("EXCISES") = txtExcisesTax.Text.Replace(",", "")
        Else
            DR("EXCISES") = DBNull.Value
        End If
        If txtExcisesTaxDate.Text.Trim <> "" Then
            DR("EXCISES_DATE") = CV.StringToDate(txtExcisesTaxDate.Text, "yyyy-MM-dd")
        Else
            DR("EXCISES_DATE") = DBNull.Value
        End If
        If txtMOI.Text.Trim <> "" Then
            DR("MOI") = txtMOI.Text.Replace(",", "")
        Else
            DR("MOI") = DBNull.Value
        End If

        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        GL.UpdateStatusShipmentImport(ShipmentID)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

End Class

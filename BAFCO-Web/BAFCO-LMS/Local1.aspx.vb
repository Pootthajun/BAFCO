﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Local1
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment.aspx", True)
                Exit Sub
            Else
                Dim SQL As String = ""
                SQL = "SELECT SHIPMENT_ID,STATUS_ID,REF_WORK_ORDER_NO FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ShipmentID = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment.aspx", True)
                    Exit Sub
                End If
            End If

            MC.BindDDlUOM(ddlUOM, "-Select-", "")
            CL.ImplementJavaIntegerText(txtVolume)
            SetMenu()
            BindData()

            txtDescriptionOfGoods.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtVolume.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            btnStatus2.Attributes("onclick") = "window.location.href='Local2.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus3.Attributes("onclick") = "window.location.href='Local3.aspx?ShipmentCode=" & ShipmentCode & "';"
        End If
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL &= "SELECT SHIPMENT_WMS.SHIPMENT_ID,UOM_ID,RECEIVE_DATE,SHIPMENT_WMS.inv_Lot_No," & vbNewLine
        SQL &= "Consignee, Supplier, No_Of_Package, Weight, VOLUME, DESCRIPTION" & vbNewLine
        SQL &= "FROM SHIPMENT_WMS " & vbNewLine
        SQL &= "LEFT JOIN LOCAL_RECEIVE ON LOCAL_RECEIVE.SHIPMENT_ID = SHIPMENT_WMS.SHIPMENT_ID" & vbNewLine
        SQL &= "LEFT JOIN WMS_WO ON SHIPMENT_WMS.inv_Lot_No = WMS_WO.inv_Lot_No" & vbNewLine
        SQL &= "WHERE SHIPMENT_WMS.SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("RECEIVE_DATE").ToString <> "" Then
                'txtReeiveDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("RECEIVE_DATE"))
                lblReeiveDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("RECEIVE_DATE"))
            End If
            'txtRefLotNo.Text = DT.Rows(0).Item("inv_Lot_No").ToString
            'txtConsignee.Text = DT.Rows(0).Item("Consignee").ToString
            'txtSupplier.Text = DT.Rows(0).Item("Supplier").ToString
            If DT.Rows(0).Item("inv_Lot_No").ToString <> "" Then
                lblRefLotNo.Text = DT.Rows(0).Item("inv_Lot_No").ToString
            End If
            If DT.Rows(0).Item("Consignee").ToString <> "" Then
                lblConsignee.Text = DT.Rows(0).Item("Consignee").ToString
            End If
            If DT.Rows(0).Item("Supplier").ToString <> "" Then
                lblSupplier.Text = DT.Rows(0).Item("Supplier").ToString
            End If
            If DT.Rows(0).Item("No_Of_Package").ToString <> "" Then
                'txtQty.Text = FormatNumber(DT.Rows(0).Item("No_Of_Package").ToString, 0)
                lblQty.Text = FormatNumber(DT.Rows(0).Item("No_Of_Package").ToString, 0)
            End If
            ddlUOM.SelectedValue = DT.Rows(0).Item("UOM_ID").ToString
            If DT.Rows(0).Item("WEIGHT").ToString <> "" Then
                'txtWeight.Text = FormatNumber(DT.Rows(0).Item("WEIGHT").ToString)
                lblWeight.Text = FormatNumber(DT.Rows(0).Item("WEIGHT").ToString)
            End If
            If DT.Rows(0).Item("VOLUME").ToString <> "" Then
                txtVolume.Text = FormatNumber(DT.Rows(0).Item("VOLUME").ToString, 0)
            End If
            txtDescriptionOfGoods.Text = DT.Rows(0).Item("DESCRIPTION").ToString
        End If

        SQL = ""
        SQL &= "Select PO_NO" & vbNewLine
        SQL &= "FROM SHIPMENT_WMS LEFT JOIN WMS_PO ON SHIPMENT_WMS.inv_Lot_No = WMS_PO.inv_Lot_No" & vbNewLine
        SQL &= "WHERE SHIPMENT_WMS.SHIPMENT_ID = " & ShipmentID & " ORDER BY PO_NO"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

#Region "PO"
    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblPONo As Label = e.Item.FindControl("lblPONo")
        lblPONo.Text = e.Item.DataItem("PO_NO").ToString
    End Sub
#End Region

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable

        '************************************************
        SQL = "SELECT * FROM LOCAL_RECEIVE WHERE SHIPMENT_ID = " & ShipmentID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
        Else
            'Edit
            DR = DT.Rows(0)
        End If
        DR("UOM_ID") = ddlUOM.SelectedValue.ToString
        If txtVolume.Text.Trim <> "" Then
            DR("VOLUME") = txtVolume.Text.Replace(",", "")
        Else
            DR("VOLUME") = DBNull.Value
        End If
        DR("DESCRIPTION") = txtDescriptionOfGoods.Text
        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        '*** Reference Work Order No ***
        SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        cmd = New SqlCommandBuilder(DA)
        DR = DT.Rows(0)
        'DR("REF_WORK_ORDER_NO") = txtRefWorkOrderNo.Text
        DA.Update(DT)

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

End Class

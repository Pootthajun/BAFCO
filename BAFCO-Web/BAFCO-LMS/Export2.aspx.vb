﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Export2
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment_Export.aspx", True)
                Exit Sub
            Else
                Dim SQL As String = ""
                SQL = "SELECT SHIPMENT_ID,STATUS_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ShipmentID = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment_Export.aspx", True)
                    Exit Sub
                End If
            End If
            SetMenu()
            BindData()

            txtAppDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtDocDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtLetterNo.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtPerDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            btnStatus1.Attributes("onclick") = "window.location.href='Export1.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus3.Attributes("onclick") = "window.location.href='Export3.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus4.Attributes("onclick") = "window.location.href='Export4.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus5.Attributes("onclick") = "window.location.href='Export5.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus6.Attributes("onclick") = "window.location.href='Export6.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus7.Attributes("onclick") = "window.location.href='Export7.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus8.Attributes("onclick") = "window.location.href='Export8.aspx?ShipmentCode=" & ShipmentCode & "';"
        End If
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = "SELECT * FROM EXPORT_SUBMIT WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("DOC_DATE").ToString <> "" Then
                txtDocDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("DOC_DATE"))
            Else
                txtDocDate.Text = ""
            End If
            If DT.Rows(0).Item("PER_DATE").ToString <> "" Then
                txtPerDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("PER_DATE"))
            Else
                txtPerDate.Text = ""
            End If
            If DT.Rows(0).Item("APP_DATE").ToString <> "" Then
                txtAppDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("APP_DATE"))
            Else
                txtAppDate.Text = ""
            End If
            txtLetterNo.Text = DT.Rows(0).Item("LETTER_NO").ToString
        End If
    End Sub

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        If txtDocDate.Text <> "" Then
            If Not MC.CheckDate(txtDocDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Date of Documented Submitted to AOT/PAT is not valid');", True)
                Exit Sub
            End If
        End If

        If txtPerDate.Text <> "" Then
            If Not MC.CheckDate(txtPerDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Date of Permission by AOT/PAT Released is not valid');", True)
                Exit Sub
            End If
        End If

        If txtAppDate.Text <> "" Then
            If Not MC.CheckDate(txtAppDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('DMF's Approval/Date is not valid');", True)
                Exit Sub
            End If
        End If
        '************************************************
        Dim SQL As String = ""
        SQL = "SELECT * FROM EXPORT_SUBMIT WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If

        If txtDocDate.Text.Trim <> "" Then
            DR("DOC_DATE") = CV.StringToDate(txtDocDate.Text, "yyyy-MM-dd")
        Else
            DR("DOC_DATE") = DBNull.Value
        End If
        If txtPerDate.Text.Trim <> "" Then
            DR("PER_DATE") = CV.StringToDate(txtPerDate.Text, "yyyy-MM-dd")
        Else
            DR("PER_DATE") = DBNull.Value
        End If
        If txtAppDate.Text.Trim <> "" Then
            DR("APP_DATE") = CV.StringToDate(txtAppDate.Text, "yyyy-MM-dd")
        Else
            DR("APP_DATE") = DBNull.Value
        End If
        DR("LETTER_NO") = txtLetterNo.Text

        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        GL.UpdateStatusShipmentExport(ShipmentID)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

End Class

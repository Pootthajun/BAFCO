﻿Imports Microsoft.VisualBasic

Public Class textControlLib

    Dim GL As New GenericLib

    Public Sub ImplementJavaMoneyText(ByRef Obj As TextBox)
        Obj.Attributes("OnChange") = "this.value=formatmoney(this.value,'0','999999999999');"
        Obj.Style.Item("Text-Align") = "Right"
    End Sub

    Public Sub ImplementJavaIntegerText(ByRef Obj As TextBox, Optional ByVal Align As String = "Right")
        Obj.Attributes("OnChange") = "this.value=formatinteger(this.value,'0','999999999999');"
        Obj.Style.Item("Text-Align") = Align
    End Sub

    Public Sub ImplementJavaOnlyNumberText(ByRef Obj As TextBox)
        Obj.Attributes("OnChange") = "this.value=formatonlynumber(this.value);"
    End Sub

    Public Sub ImplementPreventCalendarMinDate(ByVal mindate As DateTime, ByVal TextBox As TextBox, ByVal captionText As String)
        Dim tmpDate As String = GL.ReportProgrammingDate(mindate)
        TextBox.Attributes("onChange") = "preventCalendarMinDate('" & tmpDate & "','" & TextBox.ClientID & "','" & captionText & "');"
    End Sub

    Public Sub ImplementPreventCalendarMinDateNotClearText(ByVal mindate As DateTime, ByVal TextBox As TextBox, ByVal captionText As String, ByVal oldValue As String)
        Dim tmpDate As String = GL.ReportProgrammingDate(mindate)
        TextBox.Attributes("onChange") = "preventCalendarMinDateNotClearText('" & tmpDate & "','" & TextBox.ClientID & "','" & captionText & "','" & oldValue & "');"
    End Sub

End Class

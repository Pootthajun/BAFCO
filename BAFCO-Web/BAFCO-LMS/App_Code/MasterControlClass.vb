﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient


Public Class MasterControlClass

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString

    Public Sub BindCBLShipmentType(ByRef cbl As CheckBoxList)
        Dim SQL As String = "SELECT SHIPMENT_TYPE_ID,SHIPMENT_TYPE_NAME FROM MS_SHIPMENT_TYPE" & vbLf
        SQL &= "WHERE ACTIVE_STATUS = 1 " & vbNewLine
        SQL &= "ORDER BY SHIPMENT_TYPE_ID ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbl.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            cbl.DataSource = DT
            cbl.DataTextField = "SHIPMENT_TYPE_NAME"
            cbl.DataValueField = "SHIPMENT_TYPE_ID"
            cbl.DataBind()
        Next
    End Sub

    Public Sub BindCBLBranch(ByRef cbl As CheckBoxList)
        Dim SQL As String = "SELECT BRANCH_ID,BRANCH_NAME FROM MS_BRANCH" & vbLf
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY BRANCH_NAME ASC" & vbLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbl.Items.Clear()
        For i As Integer = 0 To DT.Rows.Count - 1
            cbl.DataSource = DT
            cbl.DataTextField = "BRANCH_NAME"
            cbl.DataValueField = "BRANCH_ID"
            cbl.DataBind()
        Next
    End Sub

#Region "BindDropDownList"

    Public Sub BindDDlBranch(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT BRANCH_ID,BRANCH_NAME FROM MS_BRANCH" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY BRANCH_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("BRANCH_NAME"), DT.Rows(i).Item("BRANCH_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlCompany(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT COMPANY_ID,COMPANY_NAME FROM MS_COMPANY" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY COMPANY_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("COMPANY_NAME"), DT.Rows(i).Item("COMPANY_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlShipmentType(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT SHIPMENT_TYPE_ID,SHIPMENT_TYPE_NAME FROM MS_SHIPMENT_TYPE" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 AND SHIPMENT_TYPE_ID <> 3" & vbNewLine
        SQL &= "ORDER BY SHIPMENT_TYPE_ID ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("SHIPMENT_TYPE_NAME"), DT.Rows(i).Item("SHIPMENT_TYPE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlStatus(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "", Optional ByRef ShipmentType As String = "")
        'SELECT STATUS_ID,   
        Dim SQL As String = "SELECT STATUS_ID,STATUS_NAME" & vbNewLine
        SQL &= "FROM MS_STATUS LEFT JOIN MS_SHIPMENT_TYPE" & vbNewLine
        SQL &= "ON MS_STATUS.SHIPMENT_TYPE_ID = MS_SHIPMENT_TYPE.SHIPMENT_TYPE_ID" & vbNewLine
        If ShipmentType <> "" Then
            SQL &= "WHERE MS_STATUS.SHIPMENT_TYPE_ID in (" & ShipmentType & ")" & vbNewLine
        End If
        SQL &= "ORDER BY MS_STATUS.SHIPMENT_TYPE_ID,SORT"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem
            Item = New ListItem(DT.Rows(i).Item("STATUS_NAME"), DT.Rows(i).Item("STATUS_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlTransportationType(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT TRANSPORT_TYPE_ID,TRANSPORT_TYPE_NAME FROM MS_TRANSPORT_TYPE" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY TRANSPORT_TYPE_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TRANSPORT_TYPE_NAME"), DT.Rows(i).Item("TRANSPORT_TYPE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlPortOfDestination(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DESTINATION_ID,DESTINATION_NAME FROM MS_PORT_OF_DESTINATION" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY DESTINATION_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("DESTINATION_NAME"), DT.Rows(i).Item("DESTINATION_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlProjectOwner(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT PROJECT_OWNER_ID,PROJECT_OWNER_NAME FROM MS_PROJECT_OWNER" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY PROJECT_OWNER_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PROJECT_OWNER_NAME"), DT.Rows(i).Item("PROJECT_OWNER_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlContractor(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT CONTRACTOR_ID,CONTRACTOR_NAME FROM MS_CONTRACTOR" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY CONTRACTOR_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("CONTRACTOR_NAME"), DT.Rows(i).Item("CONTRACTOR_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlSupplier(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT SUPPLIER_ID,SUPPLIER_NAME FROM MS_SUPPLIER" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY SUPPLIER_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("SUPPLIER_NAME"), DT.Rows(i).Item("SUPPLIER_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlSectionBillTo(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT BILL_TO_ID,BILL_TO_NAME FROM MS_BILL_TO" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY BILL_TO_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("BILL_TO_NAME"), DT.Rows(i).Item("BILL_TO_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlCurrency(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT CURRENCY_ID,CURRENCY_NAME FROM MS_CURRENCY" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY CURRENCY_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("CURRENCY_NAME"), DT.Rows(i).Item("CURRENCY_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlTerm(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT TERM_ID,TERM_NAME FROM MS_TERM" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY TERM_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("TERM_NAME"), DT.Rows(i).Item("TERM_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlPortOfDeparture(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DEPARTURE_ID,DEPARTURE_NAME FROM MS_PORT_OF_DEPARTURE" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY DEPARTURE_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("DEPARTURE_NAME"), DT.Rows(i).Item("DEPARTURE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlUOM(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT UOM_ID,UOM_NAME FROM MS_UOM" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY UOM_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("UOM_NAME"), DT.Rows(i).Item("UOM_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlPlaceOfDelivery(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT DELIVERY_ID,DELIVERY_NAME FROM MS_PLACE_OF_DELIVERY" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY DELIVERY_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("DELIVERY_NAME"), DT.Rows(i).Item("DELIVERY_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlPickUpPlace(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT PICK_UP_ID,PICK_UP_NAME FROM MS_PICK_UP_PLACE" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1 ORDER BY PICK_UP_NAME ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("PICK_UP_NAME").ToString, DT.Rows(i).Item("PICK_UP_ID").ToString)
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

    Public Sub BindDDlUserType(ByRef ddl As DropDownList, Optional ByVal ShowTextIndex As String = "", Optional ByVal SelectedValue As String = "")
        Dim SQL As String = "SELECT USER_TYPE_ID,USER_TYPE_NAME FROM MS_USER_TYPE" & vbNewLine
        SQL &= "ORDER BY SORT ASC"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        ddl.Items.Clear()
        If ShowTextIndex <> "" Then
            ddl.Items.Add(New ListItem(ShowTextIndex, 0))
        End If
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim Item As New ListItem(DT.Rows(i).Item("USER_TYPE_NAME"), DT.Rows(i).Item("USER_TYPE_ID"))
            ddl.Items.Add(Item)
        Next
        If ddl.SelectedIndex < 1 And SelectedValue <> "" Then
            For i As Integer = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Value.ToString = SelectedValue.ToString Then
                    ddl.SelectedIndex = i
                    Exit For
                End If
            Next
        End If
    End Sub

#End Region

#Region "JavascriptButton"

    Public Sub ImplementConfirmButton(ByVal MarkButton As HtmlAnchor, ByVal ActionButton As Button, ByVal RequireTextBox As TextBox, ByVal ConfirmText As String, ByVal InvalidText As String)
        Dim Script As String = ""
        '-------------- Check Require Text-----------
        If Not IsNothing(RequireTextBox) Then
            Script &= "if(document.getElementById('" & RequireTextBox.ClientID & "').value==''){alert('" & InvalidText & "'); return;}"
        End If
        Script &= "if(confirm('" & ConfirmText & "'))document.getElementById('" & ActionButton.ClientID & "').click();"
        MarkButton.Attributes("onClick") = Script
    End Sub

#End Region

    Public Function CheckDate(ByVal txtDate As String) As Boolean
        Try
            Dim TmpDate() As String = Split(txtDate, "-")
            If TmpDate.Length = 3 Then

                Return True
            End If
        Catch ex As Exception : End Try
        Return False
    End Function

End Class

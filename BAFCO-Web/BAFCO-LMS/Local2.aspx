﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageNewWindows.master" AutoEventWireup="false" CodeFile="Local2.aspx.vb" Inherits="Local2" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload"/>
    </Triggers>
    <ContentTemplate>
        <div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Local
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table> 
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;">
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:130;">
		                            <input runat="server" value="Received Shipment" id="btnStatus1" class="Button_White" style="height:30px;width:130px;" type="button" />
		                        </td>
		                        <td style="width:190;">
		                             <input runat="server" value="Store in Warehouse & Packing" id="btnStatus2" class="Button_Red" style="height:30px;width:190px;" type="button" />
		                        </td>
		                        <td style="width:120;">
		                             <input runat="server" value="Delivery Info" id="btnStatus3" class="Button_White" style="height:30px;width:120px;" type="button" />
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table> 
		    <table cellspacing="5px;" style="margin-left:10px; ">
		        <%--<tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Pick Up Date
                    </td>
                    <td width="200" height="25" class="NormalTextNoneBorder">
                        <asp:Label ID="lblPickupDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                        
                    </td>
                    <td colspan="3"></td>
                </tr>--%>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Packing
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPacking" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25">   
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Repacking
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtRepacking" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        Remarks
                    </td>
                    <td width="200" height="25" colspan="4">
                        <asp:TextBox ID="txtRemark" runat="server" CssClass="Textbox_Form_White" width="610px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Only .pdf, .png, .jpg, .jpeg"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Insert Attachment
                    </td>
                    <td height="25" colspan="3"> 
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" BackColor="#CCCCCC" />
                    </td>
                    <td width="50" height="25" style="display:none;">
                        <asp:ImageButton ID="btnUpload" runat="server" ImageUrl="images/upload.png" 
                            Height="25px" Width="75px"/>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                    </td>
                    <td width="200" height="25" colspan="3"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:170px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td2" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                            <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="150" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment" runat="server" CssClass="Textbox_Form_White" width="610px" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr><td></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


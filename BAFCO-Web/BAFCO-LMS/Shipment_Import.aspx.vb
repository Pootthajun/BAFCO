﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO


Partial Class Shipment_Import
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim BL As New ExportExcel

    Private Property ShipmentEdit() As Boolean
        Get
            Return ViewState("ShipmentEdit")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShipmentEdit") = value
        End Set
    End Property

    Private Property ShipmentDelete() As Boolean
        Get
            Return ViewState("ShipmentDelete")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ShipmentDelete") = value
        End Set
    End Property

    Private Property EditAllShipment() As Boolean
        Get
            Return ViewState("EditAllShipment")
        End Get
        Set(ByVal value As Boolean)
            ViewState("EditAllShipment") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindDDlBranch(ddlBranch, "-------- All --------", "")
            MC.BindDDlCompany(ddlCompany, "-------- All --------", "")
            MC.BindDDlStatus(ddlStatus, "-------- All --------", "", 1)
            MC.BindDDlTransportationType(ddlTransportationType, "-------- All --------", "")
            MC.BindDDlPortOfDestination(ddlPortOfDestination, "-------- All --------", "")

            MC.BindDDlBranch(ddl_N_Branch, "", "")
            MC.BindDDlCompany(ddl_N_Company, "", "")
            MC.BindDDlTransportationType(ddl_N_TransportationType, "", "")

            If Session("User_Type_ID") > 0 Then
                Dim Sql As String = ""
                Sql = "Select NewShipment,ExportData,Edit,[Delete],EditAllShipment From MS_USER_TYPE_MENU Where USER_TYPE_ID = " & Session("User_Type_ID") & " And MENU_ID = 1"
                Dim DA As New SqlDataAdapter(Sql, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows(0).Item("NewShipment").ToString.ToUpper = "TRUE" Then
                    btnNewShipment.Visible = True
                Else
                    btnNewShipment.Visible = False
                End If
                If DT.Rows(0).Item("ExportData").ToString.ToUpper = "TRUE" Then
                    btnExport.Visible = True
                Else
                    btnExport.Visible = False
                End If
                If DT.Rows(0).Item("Edit").ToString.ToUpper = "TRUE" Then
                    ShipmentEdit = True
                Else
                    ShipmentEdit = False
                End If
                If DT.Rows(0).Item("Delete").ToString.ToUpper = "TRUE" Then
                    ShipmentDelete = True
                Else
                    ShipmentDelete = False
                End If
                If DT.Rows(0).Item("EditAllShipment").ToString.ToUpper = "TRUE" Then
                    EditAllShipment = True
                Else
                    EditAllShipment = False
                End If
            ElseIf Session("User_Type_ID") = 0 Then
                'ลูกค้า
                btnNewShipment.Visible = False
                ShipmentEdit = False
                ShipmentDelete = False
            Else
                btnNewShipment.Visible = False
                btnExport.Visible = False
                ShipmentEdit = False
                ShipmentDelete = False
            End If

            ClearForm()
            BindData()
        End If
        Dim lblUName As Label
        lblUName = CType(Master.FindControl("lblUName"), Label)
        If lblUName.Text = "" And IsPostBack Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Shipment_Import.aspx';", True)
        End If

    End Sub

    Private Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = "SELECT * FROM VW_SHIPMENT_IMPORT" & vbLf
        If Session("User_Type_ID") = 0 Then
            SQL &= "WHERE COMPANY_ID = " & Session("User_ID") & vbLf
        End If
        SQL &= "ORDER BY CREATE_DATE DESC"

        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Session("ShipmentImport") = DT
        FilterData()
    End Sub

    Sub FilterData()
        Dim DT As New DataTable
        DT = Session("ShipmentImport")
        If Not (DT Is Nothing) Then
            '--------------- Filter ----------------------
            Dim Filter As String = ""

            If ddlBranch.SelectedIndex > 0 Then
                Filter &= "BRANCH_ID =" & ddlBranch.SelectedValue & " AND "
            End If
            If ddlCompany.SelectedIndex > 0 Then
                Filter &= "COMPANY_ID =" & ddlCompany.SelectedValue & " AND "
            End If
            If ddlStatus.SelectedIndex > 0 Then
                Filter &= "STATUS_ID =" & ddlStatus.SelectedValue & " AND "
            End If
            If txtWorkOrderNo.Text.Trim <> "" Then
                Filter &= "WORK_ORDER_NO like '%" & txtWorkOrderNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtPONo.Text.Trim <> "" Then
                Filter &= "ALL_PO like '%" & txtPONo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If txtInvoiceNo.Text.Trim <> "" Then
                Filter &= "INVOICE_NO like '%" & txtInvoiceNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If ddlTransportationType.SelectedIndex > 0 Then
                Filter &= "TRANSPORT_TYPE_ID =" & ddlTransportationType.SelectedValue & " AND "
            End If
            If txtHAWB.Text.Trim <> "" Then
                Filter &= "HAWB like '%" & txtHAWB.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If ddlPortOfDestination.SelectedIndex > 0 Then
                Filter &= "DESTINATION_ID =" & ddlPortOfDestination.SelectedValue & " AND "
            End If
            If ddlPending.SelectedIndex > 0 Then
                Filter &= "PENDING =" & ddlPending.SelectedValue & " AND "
            End If
            If txtPreAlertFrom.Text.Trim <> "" Then
                If MC.CheckDate(txtPreAlertFrom.Text) Then
                    Filter &= "PRE_ALERT >=" & txtPreAlertFrom.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtPreAlertTo.Text.Trim <> "" Then
                If MC.CheckDate(txtPreAlertTo.Text) Then
                    Filter &= "PRE_ALERT <=" & txtPreAlertTo.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtClearedFrom.Text.Trim <> "" Then
                If MC.CheckDate(txtClearedFrom.Text) Then
                    Filter &= "CLEAR_DATE >=" & txtClearedFrom.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtClearedTo.Text.Trim <> "" Then
                If MC.CheckDate(txtClearedTo.Text) Then
                    Filter &= "CLEAR_DATE <=" & txtClearedTo.Text.Replace("-", "") & " AND "
                End If
            End If
            If txtJobNo.Text.Trim <> "" Then
                Filter &= "JOB_NO like '%" & txtJobNo.Text.Replace("'", "''").Replace("%", "[%]") & "%' AND "
            End If
            If Filter <> "" Then Filter = Filter.Substring(0, Filter.Length - 4)
            DT.DefaultView.RowFilter = Filter
            DT = DT.DefaultView.ToTable
            If DT.Rows.Count <= 15 Then
                Navigation.Visible = False
            Else
                Navigation.Visible = True
            End If

            Session("ShipmentImportFilter") = DT
            Navigation.SesssionSourceName = "ShipmentImportFilter"
            Navigation.RenderLayout()
        End If
    End Sub

    Protected Sub Navigation_PageChanging(ByVal Sender As PageNavigation) Handles Navigation.PageChanging
        Navigation.TheRepeater = rptData
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub

        Dim lblNo As Label = e.Item.FindControl("lblNo")
        Dim lblShipmentID As Label = e.Item.FindControl("lblShipmentID")
        Dim lblShipmentCode As Label = e.Item.FindControl("lblShipmentCode")
        Dim lblCreateBy As Label = e.Item.FindControl("lblCreateBy")
        Dim lblStatusID As Label = e.Item.FindControl("lblStatusID")
        Dim lblWorkOrder As Label = e.Item.FindControl("lblWorkOrder")
        Dim lblCompany As Label = e.Item.FindControl("lblCompany")
        Dim lblSupplier As Label = e.Item.FindControl("lblSupplier")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim btnEdit As HtmlAnchor = e.Item.FindControl("btnEdit")
        Dim btnDelete As ImageButton = e.Item.FindControl("btnDelete")
        Dim btnView As HtmlAnchor = e.Item.FindControl("btnView")

        lblNo.Text = ((Navigation.CurrentPage - 1) * Navigation.PageSize) + e.Item.ItemIndex + 1
        lblShipmentID.Text = e.Item.DataItem("SHIPMENT_ID").ToString
        lblShipmentCode.Text = e.Item.DataItem("SHIPMENT_CODE").ToString
        lblCreateBy.Text = e.Item.DataItem("CREATE_BY").ToString
        lblStatusID.Text = e.Item.DataItem("STATUS_ID").ToString
        lblWorkOrder.Text = e.Item.DataItem("WORK_ORDER_NO").ToString
        lblCompany.Text = e.Item.DataItem("COMPANY_NAME").ToString
        lblSupplier.Text = e.Item.DataItem("SUPPLIER_NAME").ToString
        lblStatus.Text = e.Item.DataItem("STATUS_NAME").ToString

        If ShipmentEdit = True Then
            btnEdit.Visible = True
        Else
            btnEdit.Visible = False
        End If
        If ShipmentDelete = True Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If
        If EditAllShipment = False Then
            If lblCreateBy.Text <> Session("User_ID") Then
                btnEdit.Visible = False
                btnDelete.Visible = False
            End If
        End If

        '------------------
        Dim btnDelete_Confirm As AjaxControlToolkit.ConfirmButtonExtender = e.Item.FindControl("btnDelete_Confirm")
        btnDelete_Confirm.ConfirmText = "Delete action will be affected for all related information !!" & vbLf & vbLf & "Are you sure you want to delete '" & e.Item.DataItem("WORK_ORDER_NO").ToString & "' permanently !?"

        Dim URL As String = ""
        URL = "Import" & lblStatusID.Text & ".aspx?ShipmentCode=" & lblShipmentCode.Text
        btnEdit.Attributes("onclick") = "window.open('" & URL & "','','fullscreen, scrollbars');"

        URL = "Import_View.aspx?ShipmentCode=" & lblShipmentCode.Text
        btnView.Attributes("onclick") = "window.open('" & URL & "','','fullscreen, scrollbars');"
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim lblShipmentID As Label = e.Item.FindControl("lblShipmentID")
                Dim SQL As String = ""
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable

                SQL &= "SELECT FILE_NAME_NEW FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "UNION ALL" & vbNewLine
                SQL &= "SELECT FILE_NAME_NEW FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "UNION ALL" & vbNewLine
                SQL &= "SELECT FILE_NAME_NEW FROM IMPORT_COMPLETE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                DA = New SqlDataAdapter(SQL, ConnStr)
                DT = New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    For i As Int32 = 0 To DT.Rows.Count - 1
                        Dim Pathfile As String = Server.MapPath("~/Upload/" + DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                        If File.Exists(Pathfile) Then
                            File.Delete(Pathfile)
                        End If
                    Next
                End If
                SQL = ""
                SQL &= "DELETE FROM IMPORT_COMPLETE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_COMPLETE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_CUSTOMS WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_DELIVER WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_PO WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_PREPARE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_RECEIVE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_STORE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM IMPORT_VEHICLE WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                SQL &= "DELETE FROM SHIPMENT WHERE SHIPMENT_ID = " & lblShipmentID.Text & vbNewLine
                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                Dim Cmd As SqlCommand
                Cmd = New SqlCommand(SQL, conn)
                Cmd.ExecuteNonQuery()
                conn.Close()

                BindData()
        End Select
    End Sub

    Sub ClearForm()
        ddlBranch.SelectedIndex = 0
        ddlCompany.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
        ddlTransportationType.SelectedIndex = 0
        ddlPortOfDestination.SelectedIndex = 0
        ddlPending.SelectedIndex = 0
        txtWorkOrderNo.Text = ""
        txtPONo.Text = ""
        txtInvoiceNo.Text = ""
        txtHAWB.Text = ""
        txtJobNo.Text = ""
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearForm()
        BindData()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Protected Sub btn_N_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_N_Cancel.Click
        divNewShipment.Visible = False
        ddl_N_Branch.SelectedIndex = 0
        ddl_N_Company.SelectedIndex = 0
        txt_N_Branch.Text = ""
        txt_N_WorkOrderNo.Text = ""
    End Sub

    Protected Sub btnNewShipment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewShipment.Click
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = "SELECT MS_USER_BRANCH.BRANCH_ID,BRANCH_NAME,BRANCH_CODE FROM MS_USER_BRANCH LEFT JOIN MS_BRANCH ON MS_USER_BRANCH.BRANCH_ID = MS_BRANCH.BRANCH_ID WHERE USER_ID = " & Session("USER_ID") & vbNewLine
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 1 Then
            ddl_N_Branch.Visible = True
            txt_N_Branch.Visible = False
            lblBranch.Text = DT.Rows(0).Item("BRANCH_CODE").ToString
        Else
            ddl_N_Branch.SelectedValue = DT.Rows(0).Item("BRANCH_ID").ToString
            ddl_N_Branch.Visible = False
            txt_N_Branch.Visible = True
            txt_N_Branch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblBranch.Text = DT.Rows(0).Item("BRANCH_CODE").ToString
        End If

        SQL = "SELECT TRANSPORT_TYPE_CODE FROM MS_TRANSPORT_TYPE WHERE TRANSPORT_TYPE_ID = " & ddl_N_TransportationType.SelectedValue.ToString
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        lblTransportationType.Text = DT.Rows(0).Item("TRANSPORT_TYPE_CODE").ToString
        txt_N_WorkOrderNo.Text = GetWorkOrderSample()
        divNewShipment.Visible = True
    End Sub

    Function GetShipmentCode() As String
        Dim ShipmentCode As String = ""
        Dim YY As Integer
        YY = Now.Year
        If YY > 2500 Then
            YY = YY - 543
        End If
        ShipmentCode = YY.ToString.Substring(2, 2) & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
        Return ShipmentCode
    End Function

    Protected Sub btn_N_OK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_N_OK.Click
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable

        Dim YY As Integer
        YY = Now.Year
        If YY > 2500 Then
            YY = YY - 543
        End If
        Dim AutoNumber As String = ""
        Dim WO As String = ""

        SQL = "SELECT ISNULL(CONVERT(INTEGER,MAX(RIGHT(WORK_ORDER_NO,5))) + 1,1) AS AUTO_NO FROM SHIPMENT WHERE SHIPMENT_TYPE_ID = 1 AND BRANCH_ID = " & ddl_N_Branch.SelectedValue.ToString & " AND YEAR(CREATE_DATE) = " & YY & " AND TRANSPORT_TYPE_ID = " & ddl_N_TransportationType.SelectedValue.ToString & " AND CREATE_BY IS NOT NULL"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        AutoNumber = DT.Rows(0).Item("AUTO_NO").ToString.PadLeft(5, "0")
        WO = GetWorkOrderSample.Replace("-----", "") & AutoNumber

        SQL = "SELECT * FROM SHIPMENT WHERE 1=0"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        Dim ShipmentID As Integer = GL.FindID("SHIPMENT", "SHIPMENT_ID")
        Dim ShipmentCode As String = GetShipmentCode()
        Dim DR As DataRow = DT.NewRow
        DR("SHIPMENT_ID") = ShipmentID
        DR("SHIPMENT_CODE") = ShipmentCode
        DR("BRANCH_ID") = ddl_N_Branch.SelectedValue.ToString
        DR("BRANCH_CODE") = lblBranch.Text
        DR("SHIPMENT_TYPE_ID") = 1
        DR("TRANSPORT_TYPE_ID") = ddl_N_TransportationType.SelectedValue.ToString
        DR("TRANSPORT_TYPE_CODE") = lblTransportationType.Text
        DR("COMPANY_ID") = ddl_N_Company.SelectedValue.ToString
        DR("WORK_ORDER_NO") = WO
        DR("STATUS_ID") = "1"
        DR("ACTIVE_STATUS") = 1
        DR("CREATE_BY") = Session("User_ID")
        DR("CREATE_DATE") = Now
        DT.Rows.Add(DR)
        Dim cmd As New SqlCommandBuilder(DA)
        DA.Update(DT)

        'SQL = "SELECT * FROM IMPORT_RECEIVE WHERE SHIPMENT_ID = " & ShipmentID
        'DA = New SqlDataAdapter(SQL, ConnStr)
        'DT = New DataTable
        'DA.Fill(DT)
        'cmd = New SqlCommandBuilder(DA)
        'DT.Rows.Clear()
        'DR = DT.NewRow
        'DR("SHIPMENT_ID") = ShipmentID
        'DR("PRE_ALERT_DATE") = Now
        'DR("CREATE_BY") = Session("User_ID")
        'DR("CREATE_DATE") = Now
        'DT.Rows.Add(DR)
        'DA.Update(DT)

        'SQL = "SELECT SHIPMENT_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
        'DA = New SqlDataAdapter(SQL, ConnStr)
        'DT = New DataTable
        'DA.Fill(DT)
        'Session("SHIPMENT_ID") = DT.Rows(0).Item("SHIPMENT_ID").ToString
        divNewShipment.Visible = False
        BindData()
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "script", "window.open('Import1.aspx?ShipmentCode=" & ShipmentCode & "','','fullscreen, scrollbars');", True)
    End Sub

    Protected Sub ddl_N_TransportationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_N_TransportationType.SelectedIndexChanged
        Dim SQL As String = ""
        SQL = "SELECT TRANSPORT_TYPE_CODE FROM MS_TRANSPORT_TYPE WHERE TRANSPORT_TYPE_ID = " & ddl_N_TransportationType.SelectedValue.ToString
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        lblTransportationType.Text = DT.Rows(0).Item("TRANSPORT_TYPE_CODE").ToString
        txt_N_WorkOrderNo.Text = GetWorkOrderSample()
    End Sub
    Protected Sub ddl_N_Branch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_N_Branch.SelectedIndexChanged
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_CODE FROM MS_BRANCH WHERE BRANCH_ID = " & ddl_N_Branch.SelectedValue.ToString
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        DA.Fill(DT)
        lblBranch.Text = DT.Rows(0).Item("BRANCH_CODE").ToString
        txt_N_WorkOrderNo.Text = GetWorkOrderSample()
    End Sub
    
    Function GetWorkOrderSample() As String
        Dim YY As Integer
        YY = Now.Year
        If YY > 2500 Then
            YY = YY - 543
        End If
        Return "BAF" & lblBranch.Text & "-IM" & lblTransportationType.Text & "-" & YY.ToString.Substring(2, 2) & "-----"
    End Function

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        If IsNothing(Session("ShipmentImportFilter")) Then
            Exit Sub
        End If

        BindData()
        Dim DT As New DataTable
        DT = Session("ShipmentImportFilter")

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('None data for Export');", True)
            Exit Sub
        End If

        For i As Int32 = 1 To DT.Columns.Count - 1
            DT.Columns.RemoveAt(1)
        Next

        Dim SQL As String = "SELECT * FROM VW_EXPORT_DATA_IMPORT"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_ As New DataTable
        DA.Fill(DT_)

        Dim DT_Compare As New DataTable
        DT_Compare = GL.JoinDataTable(DT, DT_, "SHIPMENT_ID", "SHIPMENT_ID")
        DT_Compare.Columns(0).ColumnName = "Item"
        DT_Compare.Columns.RemoveAt(1)
        For i As Int32 = 0 To DT_Compare.Rows.Count - 1
            DT_Compare.Rows(i).Item("Item") = i + 1
        Next
        Session("ExportDataToExcel") = DT_Compare

        Dim Filename As String = ""
        Filename = Now.Year.ToString & Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
        BL.ExportToExcel(Response, DT_Compare, Filename & ".xlsx")


        'ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Print", "window.open('ReportViewer.aspx');", True)
    End Sub
End Class

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageNewWindows.master" AutoEventWireup="false" CodeFile="Export_View.aspx.vb" Inherits="Export_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
            <asp:Panel ID="PanelForm" runat="server">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Export
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="height:90px; width:100%">
		        <tr align="center" valign="middle">
		            <td>
		                <table cellpadding="0" cellspacing="1" style="background-color:#CCCCCC; border:1px;">
		                    <tr>
		                        <td align="center" style="font-size: 13px; height:30px; width:120px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Receive Pre-Alert
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:190px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Submit Document to AOT/PAT 
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:190px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Store in Warehouse & Packing 
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:90px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Booking
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:170px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Prepare Export Entry Form 
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:120px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Release Shipment 
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:90px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Pre Alert 
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:110px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Accounting Info
		                        </td>
		                    </tr>
		                    <tr>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport1" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport2" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport3" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport4" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport5" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport6" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport7" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport8" runat="server" Text=""></asp:Label>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Receive Pre-Alert
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Pre-Alert Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPreAlertDate" runat="server" Text="-"></asp:Label>
                    </td>
                   <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        BAFCO Job No
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblJobNo" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Consignee
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblProjectOwner" runat="server" Text="-"></asp:Label>
                    </td>
                    <td height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Contractor
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblContractor" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Supplier
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblSupplier" runat="server" Text="-"></asp:Label>
                    </td>
                    <td height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Section Bill to
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblSectionBillTo" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Cost Center/AFE
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblCostCenter" runat="server" Text="-"></asp:Label>
                    </td>
                    <td colspan="4" height="25"></td>
                </tr>
                <tr valign="top" runat="server" id="Tr1">
                    <td width="150" height="25" class="NormalTextBlack">
                        Invoice No
                    </td>
                    <td width="20px" height="25"></td>
                    <td height="25" colspan="5" align="left" valign="top">
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <tr style="font-family: Tahoma; font-size: 12px; color:White; text-align:center; background-color:Gray; height:25px;">
                                <td width="200" align="center">
                                    Invoice No                    
                                </td>
                            </tr>
                            <asp:Repeater ID="rptInvoice" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                            <asp:Label ID="lblInvoice" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr valign="top" runat="server" id="PO_E1">
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        PO No
                    </td>
                    <td width="20px" height="25"></td>
                    <td height="25" colspan="5" align="left" valign="top">
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <tr style="font-family: Tahoma; font-size: 12px; color:White; text-align:center; background-color:Gray; height:25px;">
                                <td width="200" align="center">
                                    PO No                    
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData_E1" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                            <asp:Label ID="lblPONo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Cargo Value
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblCargoValue" runat="server" Text="-"></asp:Label>
                                </td>
                                <td width="10px"></td>
                                <td>
                                    <asp:Label ID="lblCurrency" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        No. of Package
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblQty" runat="server" Text="-"></asp:Label>
                                </td>
                                <td width="10px"></td>
                                <td>
                                    <asp:Label ID="lblUOM" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Term
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblTerm" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Weight
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblWeight" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Volume
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblVolume" runat="server" Text="-"></asp:Label>
                    </td>
                    <td colspan="4"></td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Description of Goods
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top" colspan="5">
                        <asp:Label ID="lblDescriptionOfGoods" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr><td colspan="7"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Submit Document to AOT/PAT
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Submitted Documents to AOT/PAT Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblDocDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Released Permission by AOT/PAT Date 	
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPerDate" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        DMF Approval Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblAppDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        DMF's Letter No
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblLetterNo" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr><td colspan="7"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Store in Warehouse & Packing
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Type of Vehicle
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="600" height="25" colspan="5">
                        <table cellpadding="0" cellspacing="0" bgColor="#CCCCCC" class="NormalTextBlack" width="100%">
                            <asp:Repeater ID="rptTypeOfVehicle" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" > 
                                        <td style="font-family: Tahoma; font-size: 12px; color:White; background-color:#999; height:25px; width:150px; padding-left:20px;">
                                            <asp:Label ID="lblTypeOfVehicleName" runat="server"></asp:Label>
                                            <asp:Label ID="lblTypeOfVehicleID" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:100px;">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtTypeOfVehicle" runat="server" CssClass="Textbox_Form_White" Width="50px" MaxLength="5" style="text-align:center;" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:White; background-color:#999; height:25px; width:100%;" align="center">
                                                        <asp:Label ID="lblUnit" runat="server" Text="Unit"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLicensePlate" runat="server" CssClass="Textbox_Form_White" Width="100%" MaxLength="200" style="padding-left:20px;" ReadOnly="true"></asp:TextBox>
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Pick Up Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPickupDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Pick Up Place
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPickUpPlace" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>     
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Packing
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPacking" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">   
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Repacking
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblRepacking" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>    
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Fumigate
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top" colspan="5">
                        <asp:Label ID="lblFumigate" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>  
                <tr><td colspan="7"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Booking
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Booking Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblBookingDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        MAWB/BL
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblMAWB" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        HAWB/BL
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblHAWB" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Flight No./Vessel
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblFlightNo" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        ETD Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblETDDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        ETA Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblETADate" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Port of Departure
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPortOfDeparture" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Port of Destination
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPortOfDeatination" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr><td colspan="7"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Prepare Export Entry Form
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Export Entry No
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top" colspan="5">
                        <asp:Label ID="lblExportEntryNo" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" class="NormalTextBlack" valign="top">
                        Sign Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top" colspan="5">
                        <asp:Label ID="lblSignDate" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" class="NormalTextBlack"></td>
                    <td width="20px" ></td>
                    <td width="200" class="NormalText" valign="top"></td>
                    <td width="40" ></td>
                    <td width="150" class="NormalTextBlack"></td>
                    <td width="20px" ></td>
                    <td width="200" " class="NormalText" valign="top"></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Release Shipment
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Clear Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblClearDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Duration of Clear day
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblDuration" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Delivery Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblDeliveryDate" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Status Pending
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPending" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Place of Delivery
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblPlaceOfDelivery" runat="server" Text="-"></asp:Label>
                    </td>
                    <td height="25" colspan="4"></td>
                </tr>
                <tr valign="top" runat="server" id="Upload_E6">
                    <td width="150" class="NormalTextBlack" valign="top">
                        Insert Attachment
                    </td>
                    <td width="20px" height="25"></td>
                    <td height="25" colspan="5"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptData_E6" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:160px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Comment
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top" colspan="5">
                        <asp:Label ID="lblComment" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr><td colspan="7"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Pre Alert
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Alert Date
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top" colspan="5">
                        <asp:Label ID="lblAlertDate" runat="server" Text="-"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" class="NormalTextBlack"></td>
                    <td width="20px" ></td>
                    <td width="200" class="NormalText" valign="top"></td>
                    <td width="40" ></td>
                    <td width="150" class="NormalTextBlack"></td>
                    <td width="20px" ></td>
                    <td width="200" class="NormalText" valign="top"></td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Accounting Info
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        CWT
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblCWT" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Status Delivered
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblStatusDeliver" runat="server" Text="-"></asp:Label>
                    </td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Work Ticket
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblWorkTicket" runat="server" Text="-"></asp:Label>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Approved CWT
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblApprovedCWT" runat="server" Text="-"></asp:Label>
                    </td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack" valign="top">
                        Acc. Invoice Number
                    </td>
                    <td width="20px" height="25"></td>
                    <td width="200" height="25" class="NormalText" valign="top">
                        <asp:Label ID="lblInvNo" runat="server" Text="-"></asp:Label>
                    </td>
                    <td colspan="4"></td>
                </tr>         
                <tr><td colspan="7"></td></tr>
            </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Export1
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim GL As New GenericLib
    Dim CL As New textControlLib
    Dim CV As New Converter

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment_Export.aspx", True)
                Exit Sub
            Else
                Dim SQL As String = ""
                SQL = "SELECT SHIPMENT_ID,STATUS_ID,REF_WORK_ORDER_NO FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ShipmentID = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment_Export.aspx", True)
                    Exit Sub
                End If
            End If

            MC.BindDDlProjectOwner(ddlProjectOwner, "--------- Select ---------", "")
            MC.BindDDlContractor(ddlContractor, "--------- Select ---------", "")
            MC.BindDDlSupplier(ddlSupplier, "--------- Select ---------", "")
            MC.BindDDlSectionBillTo(ddlSectionBillTo, "--------- Select ---------", "")
            MC.BindDDlCurrency(ddlCurrency, "-Select-", "")
            MC.BindDDlUOM(ddlUOM, "-Select-", "")
            MC.BindDDlTerm(ddlTerm, "--------- Select ---------", "")

            CL.ImplementJavaMoneyText(txtCargoValue)
            CL.ImplementJavaIntegerText(txtQty)
            CL.ImplementJavaMoneyText(txtWeight)
            CL.ImplementJavaIntegerText(txtVolume)
            SetMenu()
            BindData()

            txtPreAlertDate.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtJobNo.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtCostCenter.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtCargoValue.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtQty.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtWeight.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtVolume.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            txtDescriptionOfGoods.Attributes.Add("onkeydown", "return (event.keyCode!=13);")
            btnStatus2.Attributes("onclick") = "window.location.href='Export2.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus3.Attributes("onclick") = "window.location.href='Export3.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus4.Attributes("onclick") = "window.location.href='Export4.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus5.Attributes("onclick") = "window.location.href='Export5.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus6.Attributes("onclick") = "window.location.href='Export6.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus7.Attributes("onclick") = "window.location.href='Export7.aspx?ShipmentCode=" & ShipmentCode & "';"
            btnStatus8.Attributes("onclick") = "window.location.href='Export8.aspx?ShipmentCode=" & ShipmentCode & "';"
        End If
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = "SELECT * FROM EXPORT_RECEIVE WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("PRE_ALERT_DATE").ToString <> "" Then
                txtPreAlertDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("PRE_ALERT_DATE"))
            Else
                txtPreAlertDate.Text = ""
            End If
            txtJobNo.Text = DT.Rows(0).Item("JOB_NO").ToString
            ddlProjectOwner.SelectedValue = DT.Rows(0).Item("PROJECT_OWNER_ID").ToString
            ddlContractor.SelectedValue = DT.Rows(0).Item("CONTRACTOR_ID").ToString
            ddlSupplier.SelectedValue = DT.Rows(0).Item("SUPPLIER_ID").ToString
            ddlSectionBillTo.SelectedValue = DT.Rows(0).Item("BILL_TO_ID").ToString
            txtCostCenter.Text = DT.Rows(0).Item("COST_CENTER").ToString
            'txtInvoiceNo.Text = DT.Rows(0).Item("INVOICE_NO").ToString
            If DT.Rows(0).Item("CARGO_VALUE").ToString <> "" Then
                txtCargoValue.Text = FormatNumber(DT.Rows(0).Item("CARGO_VALUE").ToString)
            Else
                txtCargoValue.Text = ""
            End If
            ddlCurrency.SelectedValue = DT.Rows(0).Item("CURRENCY_ID").ToString
            ddlTerm.SelectedValue = DT.Rows(0).Item("TERM_ID").ToString
            If DT.Rows(0).Item("QTY").ToString <> "" Then
                txtQty.Text = FormatNumber(DT.Rows(0).Item("QTY").ToString, 0)
            Else
                txtQty.Text = ""
            End If
            ddlUOM.SelectedValue = DT.Rows(0).Item("UOM_ID").ToString
            If DT.Rows(0).Item("WEIGHT").ToString <> "" Then
                txtWeight.Text = FormatNumber(DT.Rows(0).Item("WEIGHT").ToString)
            Else
                txtWeight.Text = ""
            End If
            If DT.Rows(0).Item("VOLUME").ToString <> "" Then
                txtVolume.Text = FormatNumber(DT.Rows(0).Item("VOLUME").ToString, 0)
            Else
                txtVolume.Text = ""
            End If
            txtDescriptionOfGoods.Text = DT.Rows(0).Item("DESCRIPTION").ToString

            If DT.Rows(0).Item("INVOICE_NO").ToString <> "" Then
                Dim DT_INV As New DataTable
                DT_INV.Columns.Add("INV_NO")
                If InStr(DT.Rows(0).Item("INVOICE_NO").ToString, " ||||| ") > 0 Then
                    Dim inv() As String
                    inv = Split(DT.Rows(0).Item("INVOICE_NO").ToString, " ||||| ")
                    For i As Int32 = 0 To inv.Length - 1
                        Dim DR As DataRow = DT_INV.NewRow
                        DR("INV_NO") = inv(i)
                        DT_INV.Rows.Add(DR)
                    Next
                Else
                    Dim DR As DataRow = DT_INV.NewRow
                    DR("INV_NO") = DT.Rows(0).Item("INVOICE_NO").ToString
                    DT_INV.Rows.Add(DR)
                End If
                rptInvoice.DataSource = DT_INV
                rptInvoice.DataBind()
            End If
        Else
            For i As Integer = 0 To ddlCurrency.Items.Count - 1
                If ddlCurrency.Items(i).Text.ToString = "USD" Then
                    ddlCurrency.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

        If ddlCurrency.SelectedIndex = 0 And ddlCurrency.Items.Count > 0 Then
            For i As Integer = 0 To ddlCurrency.Items.Count - 1
                If ddlCurrency.Items(i).Text = "USD" Then
                    ddlCurrency.SelectedIndex = i
                    Exit For
                End If
            Next
        End If

        SQL = "SELECT * FROM EXPORT_PO WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)

        Dim DT_PO As New DataTable
        DT_PO.Columns.Add("PO_NO")
        For i As Int32 = 0 To DT.Rows.Count - 1
            Dim DR As DataRow = DT_PO.NewRow
            DR("PO_NO") = DT.Rows(i).Item("PO").ToString
            DT_PO.Rows.Add(DR)
        Next
        rptData.DataSource = DT_PO
        rptData.DataBind()
    End Sub

#Region "PO"
    Protected Sub btnAddPO_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPO.Click
        If txtPONo.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter PO No');", True)
            txtPONo.Focus()
            Exit Sub
        End If

        Dim DT As New DataTable
        DT = GetDataPO()
        DT.DefaultView.RowFilter = "PO_NO = '" & txtPONo.Text & "'"
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This PO No is already exists');", True)
            txtPONo.Focus()
            Exit Sub
        End If
        DT.DefaultView.RowFilter = ""

        Dim DR As DataRow = DT.NewRow
        DR("PO_NO") = txtPONo.Text
        DT.Rows.Add(DR)
        rptData.DataSource = DT
        rptData.DataBind()

        txtPONo.Text = ""
        txtPONo.Focus()
    End Sub

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblPONo As Label = e.Item.FindControl("lblPONo")
        lblPONo.Text = e.Item.DataItem("PO_NO").ToString
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand

        Select Case e.CommandName
            Case "Delete"
                Dim DT As New DataTable
                DT = GetDataPO()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
        End Select


    End Sub

    Function GetDataPO() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("PO_NO")

        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblPONo As Label = ri.FindControl("lblPONo")

            Dim DR As DataRow = DT.NewRow
            DR("PO_NO") = lblPONo.Text
            DT.Rows.Add(DR)
        Next

        Return DT

    End Function
#End Region

#Region "Invoice"
    Protected Sub btnInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInvoice.Click
        If txtInvoice.Text.Trim = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please enter Invoice No');", True)
            txtInvoice.Focus()
            Exit Sub
        End If

        Dim DT As New DataTable
        DT = GetDataINV()
        DT.DefaultView.RowFilter = "INV_NO = '" & txtInvoice.Text & "'"
        If DT.DefaultView.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This Invoice No is already exists');", True)
            txtInvoice.Focus()
            Exit Sub
        End If
        DT.DefaultView.RowFilter = ""

        Dim DR As DataRow = DT.NewRow
        DR("INV_NO") = txtInvoice.Text
        DT.Rows.Add(DR)
        rptInvoice.DataSource = DT
        rptInvoice.DataBind()

        txtInvoice.Text = ""
        txtInvoice.Focus()
    End Sub

    Protected Sub rptInvoice_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptInvoice.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblInvoice As Label = e.Item.FindControl("lblInvoice")
        lblInvoice.Text = e.Item.DataItem("INV_NO").ToString
    End Sub

    Protected Sub rptInvoice_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptInvoice.ItemCommand

        Select Case e.CommandName
            Case "Delete"
                Dim DT As New DataTable
                DT = GetDataINV()
                Try
                    DT.Rows.RemoveAt(e.Item.ItemIndex)
                Catch : End Try
                rptInvoice.DataSource = DT
                rptInvoice.DataBind()
        End Select
    End Sub

    Function GetDataINV() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("INV_NO")

        For Each ri As RepeaterItem In rptInvoice.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblInvoice As Label = ri.FindControl("lblInvoice")

            Dim DR As DataRow = DT.NewRow
            DR("INV_NO") = lblInvoice.Text
            DT.Rows.Add(DR)
        Next

        Return DT

    End Function
#End Region

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable

        '************************************************

        SQL = "SELECT * FROM EXPORT_RECEIVE WHERE SHIPMENT_ID = " & ShipmentID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            '*** Validation Bafco Job No***
            If txtJobNo.Text.Trim <> "" Then
                SQL = ""
                SQL &= "SELECT * FROM (" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM IMPORT_PREPARE" & vbNewLine
                SQL &= "UNION ALL" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM EXPORT_RECEIVE" & vbNewLine
                SQL &= ") AS TB" & vbNewLine
                SQL &= "WHERE JOB_NO = '" & txtJobNo.Text.Trim.Replace("'", "''") & "'"
                Dim DA_VD As New SqlDataAdapter(SQL, ConnStr)
                Dim DT_VD As New DataTable
                DA_VD.Fill(DT_VD)
                If DT_VD.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('BAFCO Job No is already exists');", True)
                    Exit Sub
                End If
            End If
            '******************************
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            '*** Validation Bafco Job No***
            If txtJobNo.Text.Trim <> "" Then
                SQL = ""
                SQL &= "SELECT * FROM (" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM IMPORT_PREPARE" & vbNewLine
                SQL &= "UNION ALL" & vbNewLine
                SQL &= "SELECT SHIPMENT_ID,JOB_NO FROM EXPORT_RECEIVE" & vbNewLine
                SQL &= ") AS TB" & vbNewLine
                SQL &= "WHERE JOB_NO = '" & txtJobNo.Text.Trim.Replace("'", "''") & "'"
                SQL &= "AND SHIPMENT_ID <> " & ShipmentID
                Dim DA_VD As New SqlDataAdapter(SQL, ConnStr)
                Dim DT_VD As New DataTable
                DA_VD.Fill(DT_VD)
                If DT_VD.Rows.Count > 0 Then
                    ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('BAFCO Job No is already exists');", True)
                    Exit Sub
                End If
            End If
            '******************************
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If
        If txtPreAlertDate.Text.Trim <> "" Then
            DR("PRE_ALERT_DATE") = CV.StringToDate(txtPreAlertDate.Text, "yyyy-MM-dd")
        Else
            DR("PRE_ALERT_DATE") = DBNull.Value
        End If
        DR("JOB_NO") = txtJobNo.Text
        DR("PROJECT_OWNER_ID") = ddlProjectOwner.SelectedValue.ToString
        DR("CONTRACTOR_ID") = ddlContractor.SelectedValue.ToString
        DR("SUPPLIER_ID") = ddlSupplier.SelectedValue.ToString
        DR("BILL_TO_ID") = ddlSectionBillTo.SelectedValue.ToString
        DR("COST_CENTER") = txtCostCenter.Text

        Dim DT_INV As New DataTable
        Dim INV As String = ""
        DT_INV = GetDataINV()
        If DT_INV.Rows.Count > 0 Then
            For i As Int32 = 0 To DT_INV.Rows.Count - 1
                INV = INV & DT_INV.Rows(i).Item("INV_NO").ToString & " ||||| "
            Next
            INV = INV.Substring(0, INV.Length - 7)
            DR("INVOICE_NO") = INV
        Else
            DR("INVOICE_NO") = DBNull.Value
        End If

        If txtCargoValue.Text.Trim <> "" Then
            DR("CARGO_VALUE") = txtCargoValue.Text.Replace(",", "")
        Else
            DR("CARGO_VALUE") = DBNull.Value
        End If
        DR("CURRENCY_ID") = ddlCurrency.SelectedValue.ToString
        DR("TERM_ID") = ddlTerm.SelectedValue.ToString
        If txtQty.Text.Trim <> "" Then
            DR("QTY") = txtQty.Text.Replace(",", "")
        Else
            DR("QTY") = DBNull.Value
        End If
        DR("UOM_ID") = ddlUOM.SelectedValue.ToString
        If txtWeight.Text.Trim <> "" Then
            DR("WEIGHT") = txtWeight.Text.Replace(",", "")
        Else
            DR("WEIGHT") = DBNull.Value
        End If
        If txtVolume.Text.Trim <> "" Then
            DR("VOLUME") = txtVolume.Text.Replace(",", "")
        Else
            DR("VOLUME") = DBNull.Value
        End If
        DR("DESCRIPTION") = txtDescriptionOfGoods.Text

        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        '*** Reference Work Order No ***
        SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        cmd = New SqlCommandBuilder(DA)
        DR = DT.Rows(0)
        'DR("REF_WORK_ORDER_NO") = txtRefWorkOrderNo.Text
        DA.Update(DT)

        '************* PO ***************
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim strSQL As String = "DELETE FROM EXPORT_PO WHERE SHIPMENT_ID = " & ShipmentID
        Dim objCmd As SqlCommand
        objCmd = New SqlCommand(strSQL, conn)
        objCmd.ExecuteNonQuery()
        conn.Close()

        strSQL = "SELECT * FROM EXPORT_PO WHERE SHIPMENT_ID = " & ShipmentID
        DA = New SqlDataAdapter(strSQL, conn)
        Dim DT_PO As New DataTable
        DA.Fill(DT_PO)

        DT = New DataTable
        DT = GetDataPO()
        For i As Int32 = 0 To DT.Rows.Count - 1
            DR = DT_PO.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("PO") = DT.Rows(i).Item("PO_NO").ToString
            DR("SORT") = i + 1
            DT_PO.Rows.Add(DR)
            '------------ To Database ------
            Dim cb As New SqlCommandBuilder(DA)
            DA.Update(DT_PO)
        Next
        '********************************
        GL.UpdateStatusShipmentExport(ShipmentID)
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub

End Class

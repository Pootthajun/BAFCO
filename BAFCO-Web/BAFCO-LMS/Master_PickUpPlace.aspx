﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Master_PickUpPlace.aspx.vb" Inherits="Master_PickUpPlace" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="40" colspan="4" class="Top_Menu_Bar">
                <ul class="shortcut-buttons-set">
                    <li>
                        <asp:LinkButton ID="btnNew" runat="server" CssClass="shortcut-button">
                            <span><img src="images/icon/41.png"/><br />New</span>
                        </asp:LinkButton>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="900" valign="top">
                <asp:UpdatePanel ID="udp1" runat="server" >
                    <ContentTemplate>
                        <table cellpadding="0" cellspacing="1" style="margin:20px  0px 0px 10px;" bgColor="#CCCCCC" class="NormalText">
                            <tr>
                                <td width="50" class="Grid_Header" align="center">
                                    No                    
                                </td>
                                <td width="200" class="Grid_Header">                    
                                    Pick Up Place
                                </td>
                                <td width="100" class="Grid_Header">                  
                                    Status
                                </td>
                                <td width="100" class="Grid_Header">                  
                                    Action
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" 
                                        style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblNo" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail">
                                            <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center">
                                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                        </td>
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnEdit" CommandName="Edit" runat="server" ToolTip="Edit" ImageUrl="images/icon/61.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                    <tr id="trEdit" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td class="Grid_Detail" align="center" valign="middle">
                                            <asp:Image ImageUrl="images/arrow_blue_right.png" ID="imgEdit" runat="server"></asp:Image>
                                        </td>
                                        <td class="Grid_Detail">
                                            <asp:TextBox ID="txtId" runat="server" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtName" runat="server" MaxLength="50" CssClass="Textbox_Form_Required" Width="200px"></asp:TextBox>
                                        </td> 
                                        <td class="Grid_Detail" align="center">
                                            <asp:DropDownList ID="ddlStatus" runat="server" 
                                            CssClass="Dropdown_Form_Required" width="100px">
                                            <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                            <asp:ListItem Text="Inactive" Value="Inactive"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="Grid_Detail" align="center" valign="middle" width="100">
                                            <asp:ImageButton ID="btnSave" CommandName="Save" runat="server" ToolTip="Save" ImageUrl="images/icon/59.png" Height="25px" />
                                            <asp:ImageButton ID="btnCancel" CommandName="Cancel" runat="server" ToolTip="Cancel" ImageUrl="images/icon/21.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                <div style="width:800px;overflow:auto;">
                </div>
                    </ContentTemplate>
                </asp:UpdatePanel>           
            </td>
            <td></td>
        </tr>
    </table>
</asp:Content>


﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Import4
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim CL As New textControlLib
    Dim CV As New Converter
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment.aspx", True)
                Exit Sub
            Else
                Dim SQL As String = ""
                SQL = "SELECT SHIPMENT_ID,STATUS_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ShipmentID = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment.aspx", True)
                    Exit Sub
                End If
            End If

            SetMenu()
            GetTypeOfVehicle()
            BindData()
            BindDataUpload()
        End If
    End Sub

    Sub BindDataUpload()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub

    Sub GetTypeOfVehicle()
        Dim SQL As String = ""
        SQL &= "SELECT MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID,TYPE_OF_VEHICLE_NAME,AMOUNT,LICENSE_PLATE FROM MS_TYPE_OF_VEHICLE" & vbNewLine
        SQL &= "LEFT JOIN (SELECT TYPE_OF_VEHICLE_ID,AMOUNT,LICENSE_PLATE FROM IMPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID & ") AMOUNT" & vbNewLine
        SQL &= "ON MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID = AMOUNT.TYPE_OF_VEHICLE_ID" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptTypeOfVehicle.DataSource = DT
        rptTypeOfVehicle.DataBind()
    End Sub

    Protected Sub rptTypeOfVehicle_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTypeOfVehicle.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim txtTypeOfVehicle As TextBox = e.Item.FindControl("txtTypeOfVehicle")
        Dim lblTypeOfVehicleID As Label = e.Item.FindControl("lblTypeOfVehicleID")
        Dim lblTypeOfVehicleName As Label = e.Item.FindControl("lblTypeOfVehicleName")
        Dim txtLicensePlate As TextBox = e.Item.FindControl("txtLicensePlate")
        lblTypeOfVehicleID.Text = e.Item.DataItem("TYPE_OF_VEHICLE_ID").ToString
        lblTypeOfVehicleName.Text = e.Item.DataItem("TYPE_OF_VEHICLE_NAME").ToString
        CL.ImplementJavaIntegerText(txtTypeOfVehicle, "center")
        txtTypeOfVehicle.Text = e.Item.DataItem("AMOUNT").ToString
        txtLicensePlate.Text = e.Item.DataItem("LICENSE_PLATE").ToString
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_STORE WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("STORE_DATE").ToString <> "" Then
                txtStoreDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("STORE_DATE"))
            Else
                txtStoreDate.Text = ""
            End If
            If DT.Rows(0).Item("DELIVERY_DATE").ToString <> "" Then
                txtDeliveryDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("DELIVERY_DATE"))
            Else
                txtDeliveryDate.Text = ""
            End If
            txtReferenceDoc.Text = DT.Rows(0).Item("REF_DOC").ToString
            txtComment.Text = DT.Rows(0).Item("COMMENT").ToString
            cbA.Checked = DT.Rows(0).Item("A")
            cbB.Checked = DT.Rows(0).Item("B")
            cbC.Checked = DT.Rows(0).Item("C")
            txtRemark.Text = DT.Rows(0).Item("REMARK").ToString
        End If

    End Sub

    Function GetDataVehicle() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("TYPE_OF_VEHICLE_ID")
        DT.Columns.Add("AMOUNT")
        DT.Columns.Add("LICENSE_PLATE")
        For Each ri As RepeaterItem In rptTypeOfVehicle.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblTypeOfVehicleID As Label = ri.FindControl("lblTypeOfVehicleID")
            Dim txtTypeOfVehicle As TextBox = ri.FindControl("txtTypeOfVehicle")
            Dim lblStatus As Label = ri.FindControl("lblStatus")
            Dim txtLicensePlate As TextBox = ri.FindControl("txtLicensePlate")

            Dim DR As DataRow = DT.NewRow
            DR("TYPE_OF_VEHICLE_ID") = lblTypeOfVehicleID.Text
            DR("AMOUNT") = txtTypeOfVehicle.Text
            DR("LICENSE_PLATE") = txtLicensePlate.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
    Protected Sub btnStatus1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus1.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import1.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus2.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import2.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus3.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import3.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus4.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import4.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus5.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import5.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus6.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import6.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
#End Region

#Region "Upload"
    Function GetDataFileUpload() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim td1 As HtmlTableCell = e.Item.FindControl("td1")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")
        Dim td3 As HtmlTableCell = e.Item.FindControl("td3")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
        lblStatus.Text = e.Item.DataItem("STATUS").ToString
        If e.Item.DataItem("STATUS").ToString = "Delete" Then
            td1.Visible = False
            td2.Visible = False
            td3.Visible = False
        End If
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim DT As New DataTable
                DT = GetDataFileUpload()
                Try
                    Dim pathfile As String = Server.MapPath("~/Temp/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                    If File.Exists(pathfile) Then
                        File.Delete(pathfile)
                    End If
                    DT.Rows(e.Item.ItemIndex).Item("STATUS") = "Delete"
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload()
                Dim pathfile As String = ""
                If DT.Rows(e.Item.ItemIndex).Item("STATUS").ToString = "New" Then
                    pathfile = Server.MapPath("~/Temp/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Else
                    pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                End If

                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetNewFileName() As String
        Return ShipmentID.ToString.PadLeft(8, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
    End Function

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        If Not FileUpload1.HasFile Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select file to update');", True)
            Exit Sub
        End If

        Dim Extension As String = ""
        If FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("PDF") > 0 Then
            Extension = ".pdf"
        ElseIf FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("JPEG") > 0 Then
            Extension = ".jpeg"
        ElseIf FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("JPG") > 0 Then
            Extension = ".jpg"
        ElseIf FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("PNG") > 0 Then
            Extension = ".png"
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid File Type');", True)
            Exit Sub
        End If

        Dim DT As New DataTable
        DT = GetDataFileUpload()
        DT.DefaultView.RowFilter = "FILE_NAME_OLD = '" & FileUpload1.FileName & "' AND STATUS <> 'Delete'"
        If DT.DefaultView.Count = 0 Then
            DT.DefaultView.RowFilter = ""
            Dim NewFilename As String = GetNewFileName()
            Dim DR As DataRow
            DR = DT.NewRow
            DR("FILE_NAME_OLD") = FileUpload1.FileName
            DR("FILE_NAME_NEW") = NewFilename
            DR("STATUS") = "New"
            DT.Rows.Add(DR)
            rptData.DataSource = DT
            rptData.DataBind()
            FileUpload1.SaveAs(Server.MapPath("~/Temp/" + NewFilename))
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This file is already exists');", True)
        End If


    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        If txtDeliveryDate.Text <> "" Then
            If Not MC.CheckDate(txtDeliveryDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Delivery Date is not valid');", True)
                Exit Sub
            End If
        End If
        '************************************************
        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_STORE WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If
        If txtStoreDate.Text.Trim <> "" Then
            DR("STORE_DATE") = CV.StringToDate(txtStoreDate.Text, "yyyy-MM-dd")
        Else
            DR("STORE_DATE") = DBNull.Value
        End If
        If txtDeliveryDate.Text.Trim <> "" Then
            DR("DELIVERY_DATE") = CV.StringToDate(txtDeliveryDate.Text, "yyyy-MM-dd")
        Else
            DR("DELIVERY_DATE") = DBNull.Value
        End If
        DR("REF_DOC") = txtReferenceDoc.Text
        DR("COMMENT") = txtComment.Text
        DR("A") = cbA.Checked
        DR("B") = cbB.Checked
        DR("C") = cbC.Checked
        DR("REMARK") = txtRemark.Text

        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        '************ Update Status Shipment *************
        SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA_SH As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_SH As New DataTable
        Dim DR_SH As DataRow
        DA_SH.Fill(DT_SH)
        If CInt(DT_SH.Rows(0).Item("STATUS_ID").ToString) < 4 Then
            DR_SH = DT_SH.Rows(0)
            DR_SH("STATUS_ID") = "4"
            Dim cmd_SH As New SqlCommandBuilder(DA_SH)
            DA_SH.Update(DT_SH)
        End If
        '*************************************************

        '************* Vehicle ***********
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        Dim strSQL As String = "DELETE FROM IMPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID
        Dim objCmd As SqlCommand
        objCmd = New SqlCommand(strSQL, conn)
        objCmd.ExecuteNonQuery()
        conn.Close()

        strSQL = "SELECT * FROM IMPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID
        DA = New SqlDataAdapter(strSQL, conn)
        Dim DT_VEHICLE As New DataTable
        DA.Fill(DT_VEHICLE)

        DT = New DataTable
        DT = GetDataVehicle()
        For i As Int32 = 0 To DT.Rows.Count - 1
            If DT.Rows(i).Item("AMOUNT").ToString <> "" Then
                DR = DT_VEHICLE.NewRow
                DR("SHIPMENT_ID") = ShipmentID
                DR("TYPE_OF_VEHICLE_ID") = DT.Rows(i).Item("TYPE_OF_VEHICLE_ID").ToString
                DR("AMOUNT") = DT.Rows(i).Item("AMOUNT").ToString
                DR("LICENSE_PLATE") = DT.Rows(i).Item("LICENSE_PLATE").ToString
                DT_VEHICLE.Rows.Add(DR)
                Dim cmb As New SqlCommandBuilder(DA)
                DA.Update(DT_VEHICLE)
            End If
        Next

        '*********** File Upload *********
        SQL = ""
        DT = New DataTable
        DT = GetDataFileUpload()
        For i As Int32 = 0 To DT.Rows.Count - 1
            Select Case DT.Rows(i).Item("STATUS").ToString
                Case "Delete"
                    conn = New SqlConnection(ConnStr)
                    conn.Open()
                    SQL = "DELETE FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " AND FILE_NAME_NEW = '" & DT.Rows(i).Item("FILE_NAME_NEW").ToString & "'"
                    objCmd = New SqlCommand(SQL, conn)
                    objCmd.ExecuteNonQuery()
                    conn.Close()
                    '******** ลบไฟล์ *********
                    Dim pathfile As String = Server.MapPath("~/Upload/" + DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                    If File.Exists(pathfile) Then
                        File.Delete(pathfile)
                    End If
                Case "New"
                    Dim pathNew As String = Server.MapPath("~/Upload/" & DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                    Dim pathOld As String = Server.MapPath("~/Temp/" & DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                    Dim Extension As String = Path.GetExtension(pathOld)
                    Dim Filename As String = Path.GetFileNameWithoutExtension(pathOld)
                    File.Move(pathOld, pathNew)

                    SQL = "SELECT * FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID
                    Dim DA_ As New SqlDataAdapter(SQL, ConnStr)
                    Dim DT_ As New DataTable
                    Dim DR_ As DataRow
                    DA_.Fill(DT_)
                    Dim cmd_ As New SqlCommandBuilder(DA_)
                    DT_.Rows.Clear()
                    DR_ = DT_.NewRow
                    DR_("SHIPMENT_ID") = ShipmentID
                    DR_("FILE_NAME_NEW") = DT.Rows(i).Item("FILE_NAME_NEW").ToString
                    DR_("FILE_NAME_OLD") = DT.Rows(i).Item("FILE_NAME_OLD").ToString
                    DR_("SORT") = i + 1
                    DT_.Rows.Add(DR_)
                    DA_.Update(DT_)
            End Select
        Next

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
        BindDataUpload()
    End Sub
End Class

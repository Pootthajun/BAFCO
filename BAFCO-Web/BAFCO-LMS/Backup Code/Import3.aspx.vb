﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Import3
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim MC As New MasterControlClass
    Dim CL As New textControlLib
    Dim CV As New Converter
    Dim GL As New GenericLib

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Private Property ETADate() As String
        Get
            If ViewState("ETADate").ToString <> "" Then
                Return ViewState("ETADate")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ETADate") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment.aspx", True)
                Exit Sub
            Else
                Dim SQL_ As String = ""
                SQL_ = "SELECT SHIPMENT_ID,STATUS_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA_ As New SqlDataAdapter(SQL_, ConnStr)
                Dim DT_ As New DataTable
                DA_.Fill(DT_)
                If DT_.Rows.Count > 0 Then
                    ShipmentID = DT_.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment.aspx", True)
                    Exit Sub
                End If
            End If
            Dim SQL As String = ""
            SQL = "SELECT CONVERT(VARCHAR(10),ETA_DATE,120) AS ETA_DATE FROM IMPORT_PREPARE WHERE SHIPMENT_ID = " & ShipmentID
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                If DT.Rows(0).Item("ETA_DATE").ToString <> "" Then
                    ETADate = DT.Rows(0).Item("ETA_DATE").ToString
                End If
            End If
            MC.BindDDlPlaceOfDelivery(ddlPlaceOfDelivery, "--------- Select ---------", "")
            CL.ImplementJavaIntegerText(txtDuration, "center")
            SetMenu()
            BindData()
            BindDataUpload()

        End If
    End Sub

    Sub BindDataUpload()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If

        Dim SQL As String = ""
        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        rptData.DataSource = DT
        rptData.DataBind()
    End Sub
    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_CUSTOMS WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("CLEAR_DATE").ToString <> "" Then
                txtClearDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("CLEAR_DATE"))
            Else
                txtClearDate.Text = ""
            End If
            ddlPlaceOfDelivery.SelectedValue = DT.Rows(0).Item("DELIVERY_ID").ToString
            If DT.Rows(0).Item("PENDING").ToString = "" Then
                ddlPending.SelectedIndex = 0
            Else
                If CBool(DT.Rows(0).Item("PENDING")) = True Then
                    ddlPending.SelectedIndex = 1
                Else
                    ddlPending.SelectedIndex = 2
                End If
            End If
            txtComment.Text = DT.Rows(0).Item("COMMENT").ToString

            txtClearDate_TextChanged(Nothing, Nothing)
        End If

    End Sub

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
    Protected Sub btnStatus1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus1.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import1.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus2.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import2.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus3.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import3.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus4.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import4.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus5.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import5.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus6.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import6.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
#End Region

#Region "Upload"
    Function GetDataFileUpload() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptData.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Protected Sub rptData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        Dim lblStatus As Label = e.Item.FindControl("lblStatus")
        Dim td1 As HtmlTableCell = e.Item.FindControl("td1")
        Dim td2 As HtmlTableCell = e.Item.FindControl("td2")
        Dim td3 As HtmlTableCell = e.Item.FindControl("td3")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
        lblStatus.Text = e.Item.DataItem("STATUS").ToString
        If e.Item.DataItem("STATUS").ToString = "Delete" Then
            td1.Visible = False
            td2.Visible = False
            td3.Visible = False
        End If
    End Sub

    Protected Sub rptData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData.ItemCommand
        Select Case e.CommandName
            Case "Delete"
                Dim DT As New DataTable
                DT = GetDataFileUpload()
                Try
                    Dim pathfile As String = Server.MapPath("~/Temp/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                    If File.Exists(pathfile) Then
                        File.Delete(pathfile)
                    End If
                    DT.Rows(e.Item.ItemIndex).Item("STATUS") = "Delete"
                Catch : End Try
                rptData.DataSource = DT
                rptData.DataBind()
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload()
                Dim pathfile As String = ""
                If DT.Rows(e.Item.ItemIndex).Item("STATUS").ToString = "New" Then
                    pathfile = Server.MapPath("~/Temp/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Else
                    pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                End If

                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetNewFileName() As String
        Return ShipmentID.ToString.PadLeft(8, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0") & Now.Millisecond.ToString.PadLeft(3, "0")
    End Function

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnUpload.Click
        If Not FileUpload1.HasFile Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select file to update');", True)
            Exit Sub
        End If

        Dim Extension As String = ""
        If FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("PDF") > 0 Then
            Extension = ".pdf"
        ElseIf FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("JPEG") > 0 Then
            Extension = ".jpeg"
        ElseIf FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("JPG") > 0 Then
            Extension = ".jpg"
        ElseIf FileUpload1.PostedFile.ContentType.ToUpper.IndexOf("PNG") > 0 Then
            Extension = ".png"
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Invalid File Type');", True)
            Exit Sub
        End If

        Dim DT As New DataTable
        DT = GetDataFileUpload()
        DT.DefaultView.RowFilter = "FILE_NAME_OLD = '" & FileUpload1.FileName & "' AND STATUS <> 'Delete'"
        If DT.DefaultView.Count = 0 Then
            DT.DefaultView.RowFilter = ""
            Dim NewFilename As String = GetNewFileName()
            Dim DR As DataRow
            DR = DT.NewRow
            DR("FILE_NAME_OLD") = FileUpload1.FileName
            DR("FILE_NAME_NEW") = NewFilename
            DR("STATUS") = "New"
            DT.Rows.Add(DR)
            rptData.DataSource = DT
            rptData.DataBind()
            FileUpload1.SaveAs(Server.MapPath("~/Temp/" + NewFilename))
        Else
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This file is already exists');", True)
        End If


    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        If txtClearDate.Text <> "" Then
            If Not MC.CheckDate(txtClearDate.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Clear Date is not valid');", True)
                Exit Sub
            End If
        End If

        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_CUSTOMS WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If

        If txtClearDate.Text.Trim <> "" Then
            DR("CLEAR_DATE") = CV.StringToDate(txtClearDate.Text, "yyyy-MM-dd")
        Else
            DR("CLEAR_DATE") = DBNull.Value
        End If
        If ddlPlaceOfDelivery.SelectedIndex > 0 Then
            DR("DELIVERY_ID") = ddlPlaceOfDelivery.SelectedValue.ToString
        End If
        If ddlPending.SelectedIndex = 0 Then
            DR("PENDING") = DBNull.Value
        Else
            Dim Pending As Boolean = False
            If ddlPending.SelectedIndex = 1 Then
                Pending = True
            End If
            DR("PENDING") = Pending
        End If
        DR("COMMENT") = txtComment.Text

        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        '************ Update Status Shipment *************
        SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA_SH As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_SH As New DataTable
        Dim DR_SH As DataRow
        DA_SH.Fill(DT_SH)
        If CInt(DT_SH.Rows(0).Item("STATUS_ID").ToString) < 3 Then
            DR_SH = DT_SH.Rows(0)
            DR_SH("STATUS_ID") = "3"
            Dim cmd_SH As New SqlCommandBuilder(DA_SH)
            DA_SH.Update(DT_SH)
        End If
        '*************************************************


        '*********** File Upload *********
        SQL = ""
        DT = New DataTable
        DT = GetDataFileUpload()
        For i As Int32 = 0 To DT.Rows.Count - 1
            Select Case DT.Rows(i).Item("STATUS").ToString
                Case "Delete"
                    Dim conn As New SqlConnection(ConnStr)
                    conn.Open()
                    Dim strSQL As String = "DELETE FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " AND FILE_NAME_NEW = '" & DT.Rows(i).Item("FILE_NAME_NEW").ToString & "'"
                    Dim objCmd As SqlCommand
                    objCmd = New SqlCommand(strSQL, conn)
                    objCmd.ExecuteNonQuery()
                    conn.Close()
                    '******** ลบไฟล์ *********
                    Dim pathfile As String = Server.MapPath("~/Upload/" + DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                    If File.Exists(pathfile) Then
                        File.Delete(pathfile)
                    End If
                Case "New"
                    Dim pathNew As String = Server.MapPath("~/Upload/" & DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                    Dim pathOld As String = Server.MapPath("~/Temp/" & DT.Rows(i).Item("FILE_NAME_NEW").ToString)
                    Dim Extension As String = Path.GetExtension(pathOld)
                    Dim Filename As String = Path.GetFileNameWithoutExtension(pathOld)
                    File.Move(pathOld, pathNew)

                    SQL = "SELECT * FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID
                    Dim DA_ As New SqlDataAdapter(SQL, ConnStr)
                    Dim DT_ As New DataTable
                    Dim DR_ As DataRow
                    DA_.Fill(DT_)
                    Dim cmd_ As New SqlCommandBuilder(DA_)
                    DT_.Rows.Clear()
                    DR_ = DT_.NewRow
                    DR_("SHIPMENT_ID") = ShipmentID
                    DR_("FILE_NAME_NEW") = DT.Rows(i).Item("FILE_NAME_NEW").ToString
                    DR_("FILE_NAME_OLD") = DT.Rows(i).Item("FILE_NAME_OLD").ToString
                    DR_("SORT") = i + 1
                    DT_.Rows.Add(DR_)
                    DA_.Update(DT_)
            End Select
        Next

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
        BindDataUpload()
    End Sub

    Protected Sub txtClearDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtClearDate.TextChanged
        If txtClearDate.Text <> "" Then
            If MC.CheckDate(txtClearDate.Text) Then
                If ETADate <> "" Then
                    txtDuration.Text = CStr(CInt(txtClearDate.Text.Replace("-", "")) - CInt(ETADate.Replace("-", "")))
                    txtDuration.ToolTip = "From ETA Date ( " & ETADate & " )"
                End If
            Else
                txtDuration.Text = ""
            End If
        End If
    End Sub
End Class

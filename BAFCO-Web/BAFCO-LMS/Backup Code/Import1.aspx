﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Import1.aspx.vb" Inherits="Import1"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td colspan="2" style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" align="center" valign="middle">
		                Import
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;">
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:119;">
		                            <asp:Button ID="btnStatus1" runat="server" Text="Receive Pre-Alert" CssClass="Button_Red" Width="119px" Enabled="true" Height="30px"/>
		                        </td>
		                        <td style="width:170;">
		                            <asp:Button ID="btnStatus2" runat="server" Text="Prepare Import Entry Form" CssClass="Button_White" Enabled="true" Width="170px" Height="30px"/>
		                        </td>
		                        <td style="width:130;">
		                            <asp:Button ID="btnStatus3" runat="server" Text="Customs Clearance" CssClass="Button_White" Enabled="true" Width="130px" Height="30px"/>
		                        </td>
		                        <td style="width:190;">
		                             <asp:Button ID="btnStatus4" runat="server" Text="Store in Warehouse & Packing" CssClass="Button_White" Enabled="true" Width="190px" Height="30px"/>
		                        </td>
		                        <td style="width:120;">
		                             <asp:Button ID="btnStatus5" runat="server" Text="Accounting Info" CssClass="Button_White" Enabled="true" Width="120px" Height="30px"/>
		                        </td>
		                        <td style="width:180;">
		                             <asp:Button ID="btnStatus6" runat="server" Text="Complete Customs Formality" CssClass="Button_White" Enabled="true" Width="180px" Height="30px"/> 
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Pre-Alert Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPreAlertDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgPreAlertDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgPreAlertDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgPreAlertDate"
                        TargetControlID="txtPreAlertDate"></Ajax:CalendarExtender>
                    </td>
                    <td colspan="5"></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Consignee
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlProjectOwner" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Contractor
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlContractor" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Supplier
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlSupplier" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Section Bill to
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlSectionBillTo" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Cost Center/AFE
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtCostCenter" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Invoice No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        PO No
                    </td>
                    <td width="200" height="25">
                        <asp:Panel ID="Panel1" runat="server" DefaultButton="btnAddPO">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtPONo" runat="server" CssClass="Textbox_Form_White" width="120px"></asp:TextBox>
                                    </td>
                                    <td width="10px"></td>
                                    <td width="70px">
                                        <asp:ImageButton ID="btnAddPO" runat="server"  Width="25px" Height="26px" ToolTip="Add" ImageUrl="~/images/Icon/18.png"/>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>

                    <td height="25" colspan="5" align="left" valign="top">
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <tr style="font-family: Tahoma; font-size: 12px; color:White; text-align:center; background-color:Gray; height:25px;">
                                <td width="120" align="center">
                                    PO No                    
                                </td>
                                <td width="60" align="center">                  
                                    Action
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                            <asp:Label ID="lblPONo" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                            <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                            <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Cargo Value
                    </td>
                    <td width="200" height="25">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCargoValue" runat="server" CssClass="Textbox_Form_White" width="120px"></asp:TextBox>
                                </td>
                                <td width="10px"></td>
                                <td align="right">
                                    <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="Dropdown_Form_White" width="70px" ></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Term
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlTerm" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        No. of Package
                    </td>
                    <td width="200" height="25">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtQty" runat="server" CssClass="Textbox_Form_White" width="120px"></asp:TextBox>
                                </td>
                                <td width="10px"></td>
                                <td align="right">
                                    <asp:DropDownList ID="ddlUOM" runat="server" CssClass="Dropdown_Form_White" width="70px" ></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Flight No./Vessel
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtFlightNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Weight
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtWeight" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Volume
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtVolume" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        MAWB/BL
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtMAWB" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        HAWB/BL
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtHAWB" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Port of Departure
                    </td>
                    <td width="200" height="25">
                        <asp:DropDownList ID="ddlPortOfDeparture" runat="server"
                        CssClass="Dropdown_Form_White" width="205px" ></asp:DropDownList>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        ETD Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtETDDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgETDDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgETDDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgETDDate"
                        TargetControlID="txtETDDate"></Ajax:CalendarExtender>
                    </td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        Description of Goods
                    </td>
                    <td width="200" height="25" colspan="4">
                        <asp:TextBox ID="txtDescriptionOfGoods" runat="server" CssClass="Textbox_Form_White" width="610px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr><td colspan="7"></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Import_View.aspx.vb" Inherits="Import_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
            <asp:Panel ID="PanelForm" runat="server">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Import
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="height:90px; width:100%">
		        <tr align="center" valign="middle">
		            <td>
		                <table cellpadding="0" cellspacing="1" style="background-color:#CCCCCC; border:1px; solid #999999;">
		                    <tr>
		                        <td align="center" style="font-size: 13px; height:30px; width:120px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Receive Pre-Alert
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:170px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Prepare Import Entry Form
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:130px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Customs Clearance
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:190px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Store in Warehouse & Packing
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:110px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Accounting Info
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:180px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Complete Customs Formality
		                        </td>
		                    </tr>
		                    <tr>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport1" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport2" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport3" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport4" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport5" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport6" runat="server" Text=""></asp:Label>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Receive Pre-Alert
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Pre-Alert Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPreAlertDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Reference Work Order No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtRefWorkOrderNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30" Text=""></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Consignee
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtProjectOwner" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Contractor
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtContractor" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Supplier
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtSupplier" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Section Bill to
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtSectionBillTo" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Cost Center/AFE
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtCostCenter" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Invoice No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr valign="top" runat="server" id="PO_I1">
                    <td width="120" height="25" class="NormalTextBlack">
                        PO No
                    </td>
                    <td height="25" colspan="5" align="left" valign="top">
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <tr style="font-family: Tahoma; font-size: 12px; color:White; text-align:center; background-color:Gray; height:25px;">
                                <td width="120" align="center">
                                    PO No                    
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData_I1" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                            <asp:Label ID="lblPONo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td height="25">
                    </td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Cargo Value
                    </td>
                    <td width="200" height="25">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCargoValue" runat="server" CssClass="Textbox_Form_White" width="120px"></asp:TextBox>
                                </td>
                                <td width="10px"></td>
                                <td align="right">
                                    <asp:TextBox ID="txtCurrency" runat="server" CssClass="Textbox_Form_White" width="65px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td height="25" class="style1"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Term
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtTerm" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        No. of Package
                    </td>
                    <td width="200" height="25">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtQty" runat="server" CssClass="Textbox_Form_White" width="120px"></asp:TextBox>
                                </td>
                                <td width="10px"></td>
                                <td align="right">
                                    <asp:TextBox ID="txtUOM" runat="server" CssClass="Textbox_Form_White" width="65px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Flight No./Vessel
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtFlightNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Weight
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtWeight" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Volume
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtVolume" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        MAWB/BL
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtMAWB" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        HAWB/BL
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtHAWB" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Port of Departure
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPortOfDeparture" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        ETD Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtETDDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Description of Goods
                    </td>
                    <td width="200" height="25" colspan="4">
                        <asp:TextBox ID="txtDescriptionOfGoods" runat="server" CssClass="Textbox_Form_White" width="580px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr><td colspan="7"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Prepare Import Entry Form
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Job Issued Date
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtIssueDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        BAFCO Job No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtJobNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Port of Destination
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtPortOfDestination" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        ETA Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtETADate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="70" height="25">
                        <asp:TextBox ID="txtETATime" runat="server" CssClass="Textbox_Form_White" width="70px" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        D/O Receive Date
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtDODate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Sign Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtSignDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="70" height="25">
                        <asp:TextBox ID="txtSignTime" runat="server" CssClass="Textbox_Form_White" width="70px" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Import Entry No
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtImportEntryNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Import Entry Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtImportEntryDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="70" height="25">
                        <asp:TextBox ID="txtImportEntryTime" runat="server" CssClass="Textbox_Form_White" width="70px" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Duty Tax
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtDutyTax" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Duty Tax Paid On
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDutyTaxDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Excises Tax
                    </td>
                    <td height="25">
                        <asp:TextBox ID="txtExcisesTax" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Excises Tax Paid On
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtExcisesTaxDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" class="NormalTextBlack">
                        M.O.I. Tax
                    </td>
                    <td>
                        <asp:TextBox ID="txtMOI" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40"></td>
                    <td width="120">
                    </td>
                    <td width="200">
                    </td>
                    <td width="40">
                    </td>
                    <td></td>
                </tr>
                <tr><td></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Customs Clearance
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Clear Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtClearDate" runat="server" CssClass="Textbox_Form_White" width="200px" AutoPostBack="true"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Duration of Clear day
                    </td>
                    <td width="100" height="25">
                        <asp:TextBox ID="txtDuration" runat="server" CssClass="Textbox_Form_White" width="100px" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr >
                    <td width="120" height="25" class="NormalTextBlack">
                        Place of Delivery
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPlaceOfDelivery" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Status Pending</td>
                    <td width="100" height="25">
                        <asp:TextBox ID="txtPending" runat="server" CssClass="Textbox_Form_White" width="100px" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr valign="top" runat="server" id="Upload_I3">
                    <td width="120" height="25" class="NormalTextBlack">
                        Insert Attachment</td>
                    <td height="25" colspan="3"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptData_I3" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:160px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="120" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment_I3" runat="server" CssClass="Textbox_Form_White" width="480px" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr><td></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Store in Warehouse & Packing
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Stored in Warehouse
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtStoreDate" runat="server" CssClass="Textbox_Form_White" width="200px" AutoPostBack="true"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        
                    </td>
                    <td colspan="4"></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Type of Vehicle
                    </td>
                    <td width="600" height="25" colspan="4">
                        <table cellpadding="0" cellspacing="0" bgColor="#CCCCCC" class="NormalTextBlack" width="100%">
                            <asp:Repeater ID="rptTypeOfVehicle" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" > 
                                        <td style="font-family: Tahoma; font-size: 12px; color:White; background-color:#999; height:25px; width:150px; padding-left:20px;">
                                            <asp:Label ID="lblTypeOfVehicleName" runat="server"></asp:Label>
                                            <asp:Label ID="lblTypeOfVehicleID" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:100px;">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtTypeOfVehicle" runat="server" CssClass="Textbox_Form_White" Width="50px" MaxLength="5" style="text-align:center;" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:White; background-color:#999; height:25px; width:100%;" align="center">
                                                        <asp:Label ID="lblUnit" runat="server" Text="Unit"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLicensePlate" runat="server" CssClass="Textbox_Form_White" Width="100%" MaxLength="200" style="padding-left:20px;" ReadOnly="true"></asp:TextBox>
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Delivery Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="Textbox_Form_White" width="200px" ></asp:TextBox>  
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Reference Doc
                    </td>
                    <td width="200px" height="25" class="NormalTextBlack">
                        <asp:TextBox ID="txtReferenceDoc" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr valign="top" runat="server" id="Upload_I4">
                    <td width="120" height="25" class="NormalTextBlack">
                        Insert Attachment</td>
                    <td height="25" colspan="3"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptData_I4" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:160px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="120" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment_I4" runat="server" CssClass="Textbox_Form_White" width="100%" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Full Count Check
                    </td>
                    <td height="25" colspan="4"> 
                        <asp:CheckBox ID="cbA" Text="Level A" runat="server" Enabled="false" ForeColor="Black"/>
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="cbB" Text="Level B" runat="server" Enabled="false" ForeColor="Black"/>
                        &nbsp;&nbsp;
                        <asp:CheckBox ID="cbC" Text="Level C" runat="server" Enabled="false" ForeColor="Black"/>
                    </td>
                    <td></td>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Remark
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtRemark" runat="server" CssClass="Textbox_Form_White" width="100%" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>     
                <tr><td></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Accounting Info
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        CWT
                    </td>
                    <td width="200" height="25"> 
                         <asp:TextBox ID="txtCWT" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Status Delivered
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtStatusDeliver" runat="server" CssClass="Textbox_Form_White" width="200px" AutoPostBack="true"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr> 
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Work Ticket
                    </td>
                    <td width="200" height="25">
                         <asp:TextBox ID="txtWorkTicket" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Approved CWT
                    </td>
                    <td width="200" height="25"> 
                         <asp:TextBox ID="txtApprovedCWT" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                </tr> 
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Acc. Invoice Number
                    </td>
                    <td width="200" height="25">
                         <asp:TextBox ID="txtInvNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td colspan="5"></td>
                </tr>    
                <tr><td></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Complete Customs Formality
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Import Entry No
                    </td>
                    <td width="200" height="25"> 
                         <asp:TextBox ID="txtImportEntryNo_I6" runat="server" CssClass="Textbox_Form_White" ReadOnly="true" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Import Entry Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtImportEntryDate_I6" runat="server" CssClass="Textbox_Form_White" ReadOnly="true" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:TextBox ID="txtImportEntryTime_I6" runat="server" CssClass="Textbox_Form_White" ReadOnly="true" width="70px" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr> 
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Master List No
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtListNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>     
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Due Date 120 Days
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDueDate" runat="server" CssClass="Textbox_Form_White" ReadOnly="true" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>  
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        DMF's Letter No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtLetterNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Absolute Clearance Date
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtClearanceDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>     
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Duty Subjected by DMF
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtDutySubject" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>     
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Number of Dutiable Item(s)
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtNumberOfDutiable" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Pack in Case
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtPackInCase" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>     
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Expenses
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtExpenses" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr valign="top" runat="server" id="Upload_I6">
                    <td width="120" height="25" class="NormalTextBlack">
                        Insert Attachment</td>
                    <td height="25" colspan="3"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptData_I6" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:160px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="120" height="25">
                    </td>
                    <td></td>
                </tr>   
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment_I6" runat="server" CssClass="Textbox_Form_White" width="100%" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>  
                <tr><td></td></tr>
            </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


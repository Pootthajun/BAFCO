﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Import5.aspx.vb" Inherits="Import5"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Import
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;">
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:119;">
		                            <asp:Button ID="btnStatus1" runat="server" Text="Receive Pre-Alert" CssClass="Button_White" Width="119px" Enabled="true" Height="30px"/>
		                        </td>
		                        <td style="width:170;">
		                            <asp:Button ID="btnStatus2" runat="server" Text="Prepare Import Entry Form" CssClass="Button_White" Enabled="true" Width="170px" Height="30px"/>
		                        </td>
		                        <td style="width:130;">
		                            <asp:Button ID="btnStatus3" runat="server" Text="Customs Clearance" CssClass="Button_White" Enabled="true" Width="130px" Height="30px"/>
		                        </td>
		                        <td style="width:190;">
		                             <asp:Button ID="btnStatus4" runat="server" Text="Store in Warehouse & Packing" CssClass="Button_White" Enabled="true" Width="190px" Height="30px"/>
		                        </td>
		                        <td style="width:120;">
		                             <asp:Button ID="btnStatus5" runat="server" Text="Accounting Info" CssClass="Button_Red" Enabled="true" Width="120px" Height="30px"/>
		                        </td>
		                        <td style="width:180;">
		                             <asp:Button ID="btnStatus6" runat="server" Text="Complete Customs Formality" CssClass="Button_White" Enabled="true" Width="180px" Height="30px"/> 
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        CWT
                    </td>
                    <td width="200" height="25"> 
                         <asp:TextBox ID="txtCWT" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Status Delivered
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtStatusDeliver" runat="server" CssClass="Textbox_Form_White" width="200px" AutoPostBack="true" placeholder="YYYY-MM-DD"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgStatusDeliver" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgStatusDeliver_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgStatusDeliver"
                        TargetControlID="txtStatusDeliver"></Ajax:CalendarExtender>
                    </td>
                    <td></td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Work Ticket
                    </td>
                    <td width="200" height="25">
                         <asp:TextBox ID="txtWorkTicket" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Approved CWT
                    </td>
                    <td width="200" height="25"> 
                         <asp:TextBox ID="txtApprovedCWT" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Acc. Invoice Number
                    </td>
                    <td width="200" height="25">
                         <asp:TextBox ID="txtInvNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td colspan="5"></td>
                </tr>      
                <tr><td></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


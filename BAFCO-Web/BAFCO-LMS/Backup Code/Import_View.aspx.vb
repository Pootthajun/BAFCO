﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Import_View
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment.aspx", True)
                Exit Sub
            Else
                Dim SQL_ As String = ""
                SQL_ = "SELECT SHIPMENT_ID,STATUS_ID,REF_WORK_ORDER_NO FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA_ As New SqlDataAdapter(SQL_, ConnStr)
                Dim DT_ As New DataTable
                DA_.Fill(DT_)
                If DT_.Rows.Count > 0 Then
                    ShipmentID = DT_.Rows(0).Item("SHIPMENT_ID").ToString
                    txtRefWorkOrderNo.Text = DT_.Rows(0).Item("REF_WORK_ORDER_NO").ToString
                Else
                    Response.Redirect("Shipment.aspx", True)
                    Exit Sub
                End If
            End If

            Dim SQL As String = ""
            SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
                lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
                lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
            End If

            BindDataImport1()
            BindDataImport2()
            BindDataImport3()
            BindDataImport4()
            BindDataImport5()
            BindDataImport6()

            For Each ctrl As Control In PanelForm.Controls
                If TypeOf ctrl Is TextBox Then
                    CType(ctrl, TextBox).ReadOnly = True
                End If
            Next

        End If
    End Sub

#Region "Import1"
    Sub BindDataImport1()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_IMPORT1 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            txtPreAlertDate.Text = DT.Rows(0).Item("PRE_ALERT_DATE").ToString
            lblImport1.Text = DT.Rows(0).Item("PRE_ALERT_DATE").ToString
            txtProjectOwner.Text = DT.Rows(0).Item("PROJECT_OWNER_NAME").ToString
            txtContractor.Text = DT.Rows(0).Item("CONTRACTOR_NAME").ToString
            txtSupplier.Text = DT.Rows(0).Item("SUPPLIER_NAME").ToString
            txtSectionBillTo.Text = DT.Rows(0).Item("BILL_TO_NAME").ToString
            txtCostCenter.Text = DT.Rows(0).Item("COST_CENTER").ToString
            txtInvoiceNo.Text = DT.Rows(0).Item("INVOICE_NO").ToString
            If DT.Rows(0).Item("CARGO_VALUE").ToString <> "" Then
                txtCargoValue.Text = FormatNumber(DT.Rows(0).Item("CARGO_VALUE").ToString)
            Else
                txtCargoValue.Text = ""
            End If
            txtCurrency.Text = DT.Rows(0).Item("CURRENCY_NAME").ToString
            txtTerm.Text = DT.Rows(0).Item("TERM_NAME").ToString
            If DT.Rows(0).Item("QTY").ToString <> "" Then
                txtQty.Text = FormatNumber(DT.Rows(0).Item("QTY").ToString, 0)
            Else
                txtQty.Text = ""
            End If
            txtUOM.Text = DT.Rows(0).Item("UOM_NAME").ToString
            txtFlightNo.Text = DT.Rows(0).Item("FLIGHT_NO").ToString
            If DT.Rows(0).Item("WEIGHT").ToString <> "" Then
                txtWeight.Text = FormatNumber(DT.Rows(0).Item("WEIGHT").ToString)
            Else
                txtWeight.Text = ""
            End If
            If DT.Rows(0).Item("VOLUME").ToString <> "" Then
                txtVolume.Text = FormatNumber(DT.Rows(0).Item("VOLUME").ToString, 0)
            Else
                txtVolume.Text = ""
            End If
            txtMAWB.Text = DT.Rows(0).Item("MAWB").ToString
            txtHAWB.Text = DT.Rows(0).Item("HAWB").ToString
            txtPortOfDeparture.Text = DT.Rows(0).Item("DEPARTURE_NAME").ToString
            txtETDDate.Text = DT.Rows(0).Item("ETD_DATE").ToString
            txtDescriptionOfGoods.Text = DT.Rows(0).Item("DESCRIPTION").ToString
        End If

        SQL = "SELECT * FROM IMPORT_PO WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            Dim DT_PO As New DataTable
            DT_PO.Columns.Add("PO_NO")
            For i As Int32 = 0 To DT.Rows.Count - 1
                Dim DR As DataRow = DT_PO.NewRow
                DR("PO_NO") = DT.Rows(i).Item("PO").ToString
                DT_PO.Rows.Add(DR)
            Next
            rptData_I1.DataSource = DT_PO
            rptData_I1.DataBind()
        Else
            PO_I1.Visible = False
        End If
    End Sub

    Protected Sub rptData_I1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_I1.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblPONo As Label = e.Item.FindControl("lblPONo")
        lblPONo.Text = e.Item.DataItem("PO_NO").ToString
    End Sub
#End Region
#Region "Import2"
    Sub BindDataImport2()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_IMPORT2 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            txtIssueDate.Text = DT.Rows(0).Item("ISSUE_DATE").ToString
            lblImport2.Text = DT.Rows(0).Item("ISSUE_DATE").ToString
            txtJobNo.Text = DT.Rows(0).Item("JOB_NO").ToString
            txtPortOfDestination.Text = DT.Rows(0).Item("DESTINATION_NAME").ToString
            txtETADate.Text = DT.Rows(0).Item("ETA_DATE").ToString
            If DT.Rows(0).Item("ETA_TIME_H").ToString <> "" Then
                txtETATime.Text = DT.Rows(0).Item("ETA_TIME_H").ToString.PadLeft(2, "0") & ":" & DT.Rows(0).Item("ETA_TIME_M").ToString.PadLeft(2, "0")
            End If
            txtDODate.Text = DT.Rows(0).Item("DO_DATE").ToString
            txtSignDate.Text = DT.Rows(0).Item("SIGN_DATE").ToString
            If DT.Rows(0).Item("SIGN_TIME_H").ToString <> "" Then
                txtSignTime.Text = DT.Rows(0).Item("SIGN_TIME_H").ToString.PadLeft(2, "0") & ":" & DT.Rows(0).Item("SIGN_TIME_M").ToString.PadLeft(2, "0")
            End If
            txtImportEntryNo.Text = DT.Rows(0).Item("IMPORT_ENTRY_NO").ToString
            txtImportEntryDate.Text = DT.Rows(0).Item("IMPORT_ENTRY_DATE").ToString
            If DT.Rows(0).Item("IMPORT_ENTRY_TIME_H").ToString <> "" Then
                txtImportEntryTime.Text = DT.Rows(0).Item("IMPORT_ENTRY_TIME_H").ToString.PadLeft(2, "0") & ":" & DT.Rows(0).Item("IMPORT_ENTRY_TIME_M").ToString.PadLeft(2, "0")
            End If
            If DT.Rows(0).Item("DUTY").ToString <> "" Then
                txtDutyTax.Text = FormatNumber(DT.Rows(0).Item("DUTY").ToString, 0)
            Else
                txtDutyTax.Text = ""
            End If
            If DT.Rows(0).Item("DUTY_DATE").ToString <> "" Then
                txtDutyTaxDate.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("DUTY_DATE"))
            Else
                txtDutyTaxDate.Text = ""
            End If
            If DT.Rows(0).Item("EXCISES").ToString <> "" Then
                txtExcisesTax.Text = FormatNumber(DT.Rows(0).Item("EXCISES").ToString, 0)
            Else
                txtExcisesTax.Text = ""
            End If
            txtExcisesTaxDate.Text = DT.Rows(0).Item("EXCISES_DATE").ToString
            If DT.Rows(0).Item("MOI").ToString <> "" Then
                txtMOI.Text = FormatNumber(DT.Rows(0).Item("MOI").ToString, 0)
            Else
                txtMOI.Text = ""
            End If
        End If

    End Sub
#End Region
#Region "Import3"
    Sub BindDataImport3()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_IMPORT3 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            txtClearDate.Text = DT.Rows(0).Item("CLEAR_DATE").ToString
            lblImport3.Text = DT.Rows(0).Item("CLEAR_DATE").ToString
            txtPlaceOfDelivery.Text = DT.Rows(0).Item("DELIVERY_NAME").ToString
            If DT.Rows(0).Item("PENDING").ToString <> "" Then
                If CBool(DT.Rows(0).Item("PENDING")) = True Then
                    txtPending.Text = "Yes"
                Else
                    txtPending.Text = "No"
                End If
            End If
            txtComment_I3.Text = DT.Rows(0).Item("COMMENT").ToString
            txtDuration.Text = DT.Rows(0).Item("DURATION").ToString
        End If

        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM IMPORT_CUSTOMS_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            rptData_I3.DataSource = DT
            rptData_I3.DataBind()
        Else
            Upload_I3.Visible = False
        End If

    End Sub

    Protected Sub rptData_I3_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_I3.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
    End Sub

    Protected Sub rptData_I3_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData_I3.ItemCommand
        Select Case e.CommandName
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload_I3()
                Dim pathfile As String = ""
                pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetDataFileUpload_I3() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptData_I3.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function
#End Region
#Region "Import4"
    Sub BindDataImport4()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_IMPORT4 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            txtStoreDate.Text = DT.Rows(0).Item("STORE_DATE").ToString
            lblImport4.Text = DT.Rows(0).Item("STORE_DATE").ToString
            txtDeliveryDate.Text = DT.Rows(0).Item("DELIVERY_DATE").ToString
            txtReferenceDoc.Text = DT.Rows(0).Item("REF_DOC").ToString
            txtComment_I4.Text = DT.Rows(0).Item("COMMENT").ToString
            cbA.Checked = DT.Rows(0).Item("A")
            cbB.Checked = DT.Rows(0).Item("B")
            cbC.Checked = DT.Rows(0).Item("C")
            txtRemark.Text = DT.Rows(0).Item("REMARK").ToString
        End If

        SQL = "SELECT MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID,TYPE_OF_VEHICLE_NAME,AMOUNT,LICENSE_PLATE FROM MS_TYPE_OF_VEHICLE" & vbNewLine
        SQL &= "LEFT JOIN (SELECT TYPE_OF_VEHICLE_ID,AMOUNT,LICENSE_PLATE FROM IMPORT_VEHICLE WHERE SHIPMENT_ID = " & ShipmentID & ") AMOUNT" & vbNewLine
        SQL &= "ON MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID = AMOUNT.TYPE_OF_VEHICLE_ID" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        rptTypeOfVehicle.DataSource = DT
        rptTypeOfVehicle.DataBind()

        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM IMPORT_STORE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            rptData_I4.DataSource = DT
            rptData_I4.DataBind()
        Else
            Upload_I4.Visible = False
        End If
    End Sub

    Protected Sub rptTypeOfVehicle_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTypeOfVehicle.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim txtTypeOfVehicle As TextBox = e.Item.FindControl("txtTypeOfVehicle")
        Dim lblTypeOfVehicleID As Label = e.Item.FindControl("lblTypeOfVehicleID")
        Dim lblTypeOfVehicleName As Label = e.Item.FindControl("lblTypeOfVehicleName")
        Dim txtLicensePlate As TextBox = e.Item.FindControl("txtLicensePlate")
        lblTypeOfVehicleID.Text = e.Item.DataItem("TYPE_OF_VEHICLE_ID").ToString
        lblTypeOfVehicleName.Text = e.Item.DataItem("TYPE_OF_VEHICLE_NAME").ToString
        txtTypeOfVehicle.Text = e.Item.DataItem("AMOUNT").ToString
        txtLicensePlate.Text = e.Item.DataItem("LICENSE_PLATE").ToString
    End Sub

    Protected Sub rptData_I4_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_I4.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
    End Sub

    Protected Sub rptData_I4_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData_I4.ItemCommand
        Select Case e.CommandName
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload_I4()
                Dim pathfile As String = ""
                pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetDataFileUpload_I4() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptData_I4.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function
#End Region
#Region "Import5"
    Sub BindDataImport5()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_IMPORT5 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            txtStatusDeliver.Text = DT.Rows(0).Item("STATUS_DELIVER").ToString
            lblImport5.Text = DT.Rows(0).Item("STATUS_DELIVER").ToString
            txtCWT.Text = DT.Rows(0).Item("CWT").ToString
            txtWorkTicket.Text = DT.Rows(0).Item("WORK_TICKET").ToString
            txtApprovedCWT.Text = DT.Rows(0).Item("APPROVE_CWT").ToString
            txtInvNo.Text = DT.Rows(0).Item("ACC_INVOICE_NO").ToString
        End If

    End Sub
#End Region
#Region "Import6"
    Sub BindDataImport6()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_IMPORT6 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("IMPORT_ENTRY_DATE").ToString <> "" Then
                txtImportEntryDate_I6.Text = DT.Rows(0).Item("IMPORT_ENTRY_DATE").ToString
                txtDueDate.Text = DT.Rows(0).Item("DUE_DATE").ToString
            End If
            If DT.Rows(0).Item("IMPORT_ENTRY_TIME_H").ToString <> "" Then
                txtImportEntryTime_I6.Text = DT.Rows(0).Item("IMPORT_ENTRY_TIME_H").ToString.PadLeft(2, "0") & ":" & DT.Rows(0).Item("IMPORT_ENTRY_TIME_M").ToString.PadLeft(2, "0")
            End If
            txtImportEntryNo_I6.Text = DT.Rows(0).Item("IMPORT_ENTRY_NO").ToString
            txtListNo.Text = DT.Rows(0).Item("LIST_NO").ToString
            txtClearanceDate.Text = DT.Rows(0).Item("CLEARANCE_DATE").ToString
            lblImport6.Text = DT.Rows(0).Item("CLEARANCE_DATE").ToString
            txtLetterNo.Text = DT.Rows(0).Item("LETTER_NO").ToString
            txtDutySubject.Text = DT.Rows(0).Item("DUTY_SUBJECT").ToString
            If DT.Rows(0).Item("NUMBER_OF_DUTY").ToString <> "" Then
                txtNumberOfDutiable.Text = FormatNumber(DT.Rows(0).Item("NUMBER_OF_DUTY").ToString, 0)
            Else
                txtNumberOfDutiable.Text = ""
            End If
            txtPackInCase.Text = DT.Rows(0).Item("PACK_IN_CASE").ToString
            txtExpenses.Text = DT.Rows(0).Item("EXPENSES").ToString
            txtComment_I6.Text = DT.Rows(0).Item("COMMENT").ToString

        End If

        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM IMPORT_COMPLETE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        rptData_I6.DataSource = DT
        rptData_I6.DataBind()
    End Sub

    Protected Sub rptData_I6_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_I6.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
    End Sub

    Protected Sub rptData_I6_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData_I6.ItemCommand
        Select Case e.CommandName
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload_I6()
                Dim pathfile As String = ""
                pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetDataFileUpload_I6() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptData_I6.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function
#End Region

End Class

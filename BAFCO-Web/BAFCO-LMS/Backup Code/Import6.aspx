﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Import6.aspx.vb" Inherits="Import6"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="udp1" runat="server" >
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpload" />
    </Triggers>
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Import
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; 
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: #999999; width: 100%; height:30px;">
		        <tr>
		            <td align="right">
		                <table cellpadding="0" cellspacing="0">
		                    <tr>
		                        <td style="width:119;">
		                            <asp:Button ID="btnStatus1" runat="server" Text="Receive Pre-Alert" CssClass="Button_White" Width="119px" Enabled="true" Height="30px"/>
		                        </td>
		                        <td style="width:170;">
		                            <asp:Button ID="btnStatus2" runat="server" Text="Prepare Import Entry Form" CssClass="Button_White" Enabled="true" Width="170px" Height="30px"/>
		                        </td>
		                        <td style="width:130;">
		                            <asp:Button ID="btnStatus3" runat="server" Text="Customs Clearance" CssClass="Button_White" Enabled="true" Width="130px" Height="30px"/>
		                        </td>
		                        <td style="width:190;">
		                             <asp:Button ID="btnStatus4" runat="server" Text="Store in Warehouse & Packing" CssClass="Button_White" Enabled="true" Width="190px" Height="30px"/>
		                        </td>
		                        <td style="width:120;">
		                             <asp:Button ID="btnStatus5" runat="server" Text="Accounting Info" CssClass="Button_White" Enabled="true" Width="120px" Height="30px"/>
		                        </td>
		                        <td style="width:180;">
		                             <asp:Button ID="btnStatus6" runat="server" Text="Complete Customs Formality" CssClass="Button_Red" Enabled="true" Width="180px" Height="30px"/> 
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Import Entry No
                    </td>
                    <td width="200" height="25"> 
                         <asp:TextBox ID="txtImportEntryNo" runat="server" CssClass="Textbox_Form_Disable" ReadOnly="true" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Import Entry Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtImportEntryDate" runat="server" CssClass="Textbox_Form_Disable" ReadOnly="true" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                        <asp:TextBox ID="txtImportEntryTime" runat="server" CssClass="Textbox_Form_Disable" ReadOnly="true" width="40px" style="text-align:center;"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Master List No
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtListNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>     
                    </td>
                    <td width="40" height="25"></td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Due Date 120 Days
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDueDate" runat="server" CssClass="Textbox_Form_Disable" ReadOnly="true" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>  
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        DMF's Letter No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtLetterNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Absolute Clearance Date
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtClearanceDate" runat="server" CssClass="Textbox_Form_White" width="200px" placeholder="YYYY-MM-DD"></asp:TextBox>     
                    </td>
                    <td width="40" height="25">
                        <asp:ImageButton ID="imgClearanceDate" runat="server" ImageUrl="images/Calendar.png" />
                        <Ajax:CalendarExtender ID="imgClearanceDate_CDE" runat="server" 
                        Format="yyyy-MM-dd"  PopupButtonID="imgClearanceDate"
                        TargetControlID="txtClearanceDate"></Ajax:CalendarExtender>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Duty Subjected by DMF
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtDutySubject" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>     
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Number of Dutiable Item(s)
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtNumberOfDutiable" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="10"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Pack in Case
                    </td>
                    <td width="200" height="25"> 
                        <asp:TextBox ID="txtPackInCase" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>     
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="150" height="25" class="NormalTextBlack">
                        Expenses
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtExpenses" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Only .pdf, .png, .jpg, .jpeg"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                        Insert Attachment
                    </td>
                    <td height="25" colspan="2"> 
                        <asp:FileUpload ID="FileUpload1" runat="server" Width="100%" BackColor="#CCCCCC" />
                    </td>
                   
                    <td width="50" height="25">
                        <asp:ImageButton ID="btnUpload" runat="server" ImageUrl="images/upload.png" 
                            Height="25px" Width="75px" />
                    </td>
                </tr> 
                <tr>
                    <td width="150" height="25" class="NormalTextBlack">
                    </td>
                    <td height="25" colspan="2"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack" width="100%">
                            <asp:Repeater ID="rptData" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td2" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnDelete" CommandName="Delete" runat="server" ToolTip="Delete" ImageUrl="images/icon/57.png" Height="25px" />
                                            <Ajax:ConfirmButtonExtender ID="btnDelete_Confirm" runat="server" Enabled="true" ConfirmText="Are you sure you want to delete?" TargetControlID="btnDelete"></Ajax:ConfirmButtonExtender>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="150" height="25">
                    </td>
                </tr> 
                <tr valign="top">
                    <td width="150" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment" runat="server" CssClass="Textbox_Form_White" width="100%" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>    
                <tr><td></td></tr>
                <tr>
                    <td colspan="5" align="right">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Red" Width="130px" Height="30px"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


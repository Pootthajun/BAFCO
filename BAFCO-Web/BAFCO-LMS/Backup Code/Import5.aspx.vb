﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Import5
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim CL As New textControlLib
    Dim CV As New Converter
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass


    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment.aspx", True)
                Exit Sub
            Else
                Dim SQL As String = ""
                SQL = "SELECT SHIPMENT_ID,STATUS_ID FROM SHIPMENT WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA As New SqlDataAdapter(SQL, ConnStr)
                Dim DT As New DataTable
                DA.Fill(DT)
                If DT.Rows.Count > 0 Then
                    ShipmentID = DT.Rows(0).Item("SHIPMENT_ID").ToString
                Else
                    Response.Redirect("Shipment.aspx", True)
                    Exit Sub
                End If
            End If
            SetMenu()
            BindData()
        End If
    End Sub

    Sub BindData()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_DELIVER WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("STATUS_DELIVER").ToString <> "" Then
                txtStatusDeliver.Text = GL.ReportProgrammingDate(DT.Rows(0).Item("STATUS_DELIVER"))
            Else
                txtStatusDeliver.Text = ""
            End If
            txtCWT.Text = DT.Rows(0).Item("CWT").ToString
            txtWorkTicket.Text = DT.Rows(0).Item("WORK_TICKET").ToString
            txtApprovedCWT.Text = DT.Rows(0).Item("APPROVE_CWT").ToString
            txtInvNo.Text = DT.Rows(0).Item("ACC_INVOICE_NO").ToString
        End If

    End Sub

#Region "Menu"
    Sub SetMenu()
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        Dim SQL As String = ""
        SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            lblBeanch.Text = DT.Rows(0).Item("BRANCH_NAME").ToString
            lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
            lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
        End If
    End Sub
    Protected Sub btnStatus1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus1.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import1.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus2.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import2.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus3.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import3.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus4.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import4.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus5.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import5.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
    Protected Sub btnStatus6_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatus6.Click
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Redirect", "window.location.href='Import6.aspx?ShipmentCode=" & ShipmentCode & "';", True)
    End Sub
#End Region

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '****************** Validation ******************
        If Session("User_ID") = "" Or IsNothing(Session("User_ID")) Then
            Exit Sub
        End If
        If txtStatusDeliver.Text <> "" Then
            If Not MC.CheckDate(txtStatusDeliver.Text) Then
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Status Deliver is not valid');", True)
                Exit Sub
            End If
        End If
        '************************************************
        Dim SQL As String = ""
        SQL = "SELECT * FROM IMPORT_DELIVER WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        Dim DR As DataRow
        DA.Fill(DT)
        Dim cmd As New SqlCommandBuilder(DA)

        If DT.Rows.Count = 0 Then
            'Add
            DT.Rows.Clear()
            DR = DT.NewRow
            DR("SHIPMENT_ID") = ShipmentID
            DR("CREATE_BY") = Session("User_ID")
            DR("CREATE_DATE") = Now
        Else
            'Edit
            DR = DT.Rows(0)
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
        End If

        If txtStatusDeliver.Text.Trim <> "" Then
            DR("STATUS_DELIVER") = CV.StringToDate(txtStatusDeliver.Text, "yyyy-MM-dd")
        Else
            DR("STATUS_DELIVER") = DBNull.Value
        End If
        DR("CWT") = txtCWT.Text
        DR("WORK_TICKET") = txtWorkTicket.Text
        DR("APPROVE_CWT") = txtApprovedCWT.Text
        DR("ACC_INVOICE_NO") = txtInvNo.Text

        If DT.Rows.Count = 0 Then
            DT.Rows.Add(DR)
        End If
        DA.Update(DT)

        '************ Update Status Shipment *************
        SQL = "SELECT * FROM SHIPMENT WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA_SH As New SqlDataAdapter(SQL, ConnStr)
        Dim DT_SH As New DataTable
        Dim DR_SH As DataRow
        DA_SH.Fill(DT_SH)
        If CInt(DT_SH.Rows(0).Item("STATUS_ID").ToString) < 5 Then
            DR_SH = DT_SH.Rows(0)
            DR_SH("STATUS_ID") = "5"
            Dim cmd_SH As New SqlCommandBuilder(DA_SH)
            DA_SH.Update(DT_SH)
        End If
        '*************************************************

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
    End Sub
End Class

﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Local_View.aspx.vb" Inherits="Local_View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="udp1" runat="server" >
    <ContentTemplate>
		<div style="background-color:White; border:2px solid #999999; padding:-20px 20px 20px 20px; margin: 10px 10px 10px 10px;">
            <asp:Panel ID="PanelForm" runat="server">
		    <table cellpadding="0" cellspacing="0" style="background: gray; width: 100%; height:60px;">
		        <tr>
		            <td style="font-size: 26px; font-weight:bold; color:White; padding-left:10px;" 
                        align="center" valign="middle">
		                Local
		            </td>
		        </tr>
		        <tr>
		            <td style="font-size: 16px; font-weight:bold; color:White; padding-left:10px; height:30px;" valign="top">
		                <%--Branch : 
                        <asp:Label ID="lblBeanch" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp; --%>
                        Company : 
                        <asp:Label ID="lblCompany" runat="server" Text="-"></asp:Label> 
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Work Order No :
                        <asp:Label ID="lblWorkOrderNo" runat="server" Text="-"></asp:Label> 
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="height:90px; width:100%">
		        <tr align="center" valign="middle">
		            <td>
		                <table cellpadding="0" cellspacing="1" style="background-color:#CCCCCC; border:1px; solid #999999;">
		                    <tr>
		                        <td align="center" style="font-size: 13px; height:30px; width:130px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Received Shipment
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:190px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Store in Warehouse & Packing
		                        </td>
		                        <td align="center" style="font-size: 13px; height:30px; width:100px; background-color:#CCCCCC; border-style: solid; border-width: thin;">
		                            Delivery Info
		                        </td>
		                    </tr>
		                    <tr>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport1" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport2" runat="server" Text=""></asp:Label>
		                        </td>
		                        <td style="font-size: 13px; height:30px; background-color:White; border-style: solid; border-width: thin" align="center">
                                    <asp:Label ID="lblImport3" runat="server" Text=""></asp:Label>
		                        </td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		    </table>
		    <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Received Shipment
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px;">
		        <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Shipment Received Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtReceivedDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td colspan="4"></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack" valign="top">
                        Reference Lot No
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtRefLotNo" runat="server" CssClass="Textbox_Form_White" width="200px" Height="50" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack" valign="top">
                        Reference Work Order No
                    </td>
                    <td width="200" height="25" valign="top">
                        <asp:TextBox ID="txtRefWorkOrderNo" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="30" Text=""></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack" valign="top">
                        Consignee
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtProjectOwner" runat="server" CssClass="Textbox_Form_White" width="200px" Height="40" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack" valign="top">
                        Supplier
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtSupplier" runat="server" CssClass="Textbox_Form_White" width="200px" Height="40" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr valign="top" runat="server" id="PO_L1">
                    <td width="120" height="25" class="NormalTextBlack">
                        PO No
                    </td>
                    <td height="25" colspan="5" align="left" valign="top">
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalText">
                            <tr style="font-family: Tahoma; font-size: 12px; color:White; text-align:center; background-color:Gray; height:25px;">
                                <td width="200" align="center">
                                    PO No                    
                                </td>
                            </tr>
                            <asp:Repeater ID="rptData_L1" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px;">
                                            <asp:Label ID="lblPONo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td height="25">
                    </td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        No. of Package
                    </td>
                    <td width="200" height="25">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtQty" runat="server" CssClass="Textbox_Form_White" width="120px"></asp:TextBox>
                                </td>
                                <td width="10px"></td>
                                <td align="right">
                                    <asp:TextBox ID="txtUOM" runat="server" CssClass="Textbox_Form_White" width="70px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td width="40" height="25"></td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Weight
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtWeight" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Volume
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtVolume" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25"></td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Description of Goods
                    </td>
                    <td width="200" height="25" colspan="4">
                        <asp:TextBox ID="txtDescriptionOfGoods" runat="server" CssClass="Textbox_Form_White" width="580px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr><td colspan="7"></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Store in Warehouse & Packing
		            </td>
		        </tr>
		    </table>
            <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Pick Up Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPickupDate" runat="server" CssClass="Textbox_Form_White" width="200px" AutoPostBack="true"></asp:TextBox>
                    </td>
                    <td width="40" height="25" colspan="5"></td>
                </tr> 
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Packing
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPacking" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25">   
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Repacking
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtRepacking" runat="server" CssClass="Textbox_Form_White" width="200px" MaxLength="50"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Remarks
                    </td>
                    <td width="200" height="25" colspan="4">
                        <asp:TextBox ID="txtRemark" runat="server" CssClass="Textbox_Form_White" width="580px" Height="50px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    <td colspan="4"></td>
                </tr>
                <tr valign="top" runat="server" id="Upload_L2">
                    <td width="120" height="25" class="NormalTextBlack">
                        Insert Attachment</td>
                    <td height="25" colspan="3"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptData_L2" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:160px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="120" height="25">
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment_L2" runat="server" CssClass="Textbox_Form_White" width="580px" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr><td></td></tr>
            </table>
            <table cellpadding="0" cellspacing="0" style="background: red; width: 100%; height:30px;">
		        <tr>
		            <td style="font-size: 14px; font-weight:bold; color:White; padding-left:10px; height:30px;">
                        Delivery Info
		            </td>
		        </tr>
		    </table>
		    <table cellspacing="5px;" style="margin-left:10px; ">
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Delivery Date
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtDeliveryDate" runat="server" CssClass="Textbox_Form_White" width="200px"></asp:TextBox>
                    </td>
                    <td width="40" height="25">
                    </td>
                    <td width="120" height="25" class="NormalTextBlack">
                        Reference Doc
                    </td>
                    <td width="100" height="25">
                        <asp:TextBox ID="txtReferenceDoc" runat="server" CssClass="Textbox_Form_White" width="200px"  MaxLength="50"></asp:TextBox>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td width="120" height="25" class="NormalTextBlack">
                        Place of Delivery
                    </td>
                    <td width="200" height="25">
                        <asp:TextBox ID="txtPlaceOfDelivery" runat="server" CssClass="Textbox_Form_White" width="200px"  MaxLength="50"></asp:TextBox>
                    </td>
                    <td colspan="4"></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Type of Vehicle
                    </td>
                    <td width="600" height="25" colspan="6">
                        <table cellpadding="0" cellspacing="0" bgColor="#CCCCCC" class="NormalTextBlack" width="100%">
                            <asp:Repeater ID="rptTypeOfVehicle" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" > 
                                        <td style="font-family: Tahoma; font-size: 12px; color:White; background-color:#999; height:25px; width:150px; padding-left:20px;">
                                            <asp:Label ID="lblTypeOfVehicleName" runat="server"></asp:Label>
                                            <asp:Label ID="lblTypeOfVehicleID" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:100px;">
                                            <table cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtTypeOfVehicle" runat="server" CssClass="Textbox_Form_White" Width="50px" MaxLength="5" style="text-align:center;" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                    <td style="font-family: Tahoma; font-size: 12px; color:White; background-color:#999; height:25px; width:100%;" align="center">
                                                        <asp:Label ID="lblUnit" runat="server" Text="Unit"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtLicensePlate" runat="server" CssClass="Textbox_Form_White" Width="100%" MaxLength="200" style="padding-left:20px;" ReadOnly="true"></asp:TextBox>
                                        </td>                                   
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                </tr>
                <tr valign="top" runat="server" id="Upload_L3">
                    <td width="120" height="25" class="NormalTextBlack">
                        Insert Attachment</td>
                    <td height="25" colspan="3"> 
                        <table cellpadding="0" cellspacing="1" bgColor="#CCCCCC" class="NormalTextBlack">
                            <asp:Repeater ID="rptdata_L3" runat="server">
                                <ItemTemplate>
                                    <tr id="trView" runat="server" style="border-bottom:solid 1px #efefef;" >
                                        <td id="td1" runat="server" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:160px; padding-left:10px;">
                                            <asp:Label ID="lblFileNameOld" runat="server"></asp:Label>
                                            <asp:Label ID="lblFileNameNew" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblStatus" runat="server" Visible="false"></asp:Label>
                                        </td>
                                        <td id="td3" runat="server" align="center" style="font-family: Tahoma; font-size: 12px; color:Black; background-color:White; height:25px; width:30px;">
                                            <asp:ImageButton ID="btnView" CommandName="View" runat="server" ToolTip="View" ImageUrl="images/icon/44.png" Height="25px" />
                                        </td>                                    
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </td>
                    <td width="120" height="25">
                    </td>
                    <td></td>
                </tr>
                <tr valign="top">
                    <td width="120" height="25" class="NormalTextBlack">
                        Comment
                    </td>
                    <td height="25" colspan="4"> 
                         <asp:TextBox ID="txtComment_L3" runat="server" CssClass="Textbox_Form_White" width="100%" TextMode="MultiLine" MaxLength="500" Height="60px"></asp:TextBox>
                    </td>
                    <td colspan="2"></td>
                </tr>
                <tr><td></td></tr>
            </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>


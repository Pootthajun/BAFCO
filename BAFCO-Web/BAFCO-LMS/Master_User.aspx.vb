﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Master_User
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib
    Dim MC As New MasterControlClass

    Private Enum Mode
        Add = 0
        Edit = 1
    End Enum

    Private Property EditMode() As Mode
        Get
            Return Math.Abs(CInt(txtCode.ReadOnly))
        End Get
        Set(ByVal value As Mode)
            Select Case value
                Case Mode.Add
                    txtCode.ReadOnly = False
                    txtCode.CssClass = "Textbox_Form_Required"
                Case Mode.Edit
                    txtCode.ReadOnly = True
                    txtCode.CssClass = "Textbox_Form_Disable"
            End Select
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            MC.BindCBLBranch(cblBranch)
            MC.BindDDlUserType(ddlType)
            ClearData()
        End If
    End Sub

    Sub ClearData()
        EditMode = Mode.Add
        lblId.Text = ""
        txtCode.Text = ""
        txtFullname.Text = ""
        txtPassword.Text = ""
        ddlStatus.SelectedIndex = 0
        ddlType.SelectedIndex = 0
        For i As Int32 = 0 To cblBranch.Items.Count - 1
            cblBranch.Items(i).Selected = False
        Next
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        ClearData()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '********* Validation **********
        If txtCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert User Code / Login');", True)
            txtCode.Focus()
            Exit Sub
        End If
        If txtPassword.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Password');", True)
            txtPassword.Focus()
            Exit Sub
        End If
        If txtFullname.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please insert Fullname');", True)
            txtFullname.Focus()
            Exit Sub
        End If

        Dim chk As Boolean = False
        For i As Int32 = 0 To cblBranch.Items.Count - 1
            If cblBranch.Items(i).Selected = True Then
                chk = True
                Exit For
            End If
        Next
        If chk = False Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Please select Branch');", True)
            cblBranch.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        If EditMode = Mode.Add Then
            SQL = "SELECT * FROM MS_USER WHERE USER_CODE='" & txtCode.Text.Replace("'", "''") & "'"
        Else
            SQL = "SELECT * FROM MS_USER WHERE USER_CODE='" & txtCode.Text.Replace("'", "''") & "' AND USER_ID <> " & lblId.Text
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('This User Code / Login is already exists');", True)
            txtCode.Focus()
            Exit Sub
        End If

        Dim ActiveStatus As Boolean = False
        If ddlStatus.SelectedIndex = 0 Then
            ActiveStatus = True
        End If

        If EditMode = Mode.Add Then
            Dim DR As DataRow = DT.NewRow
            lblId.Text = GL.FindID("MS_USER", "USER_ID")
            DR("USER_ID") = lblId.Text
            DR("USER_CODE") = txtCode.Text
            DR("USER_PASSWORD") = txtPassword.Text
            DR("USER_FULLNAME") = txtFullname.Text
            DR("USER_TYPE_ID") = ddlType.SelectedValue.ToString
            DR("ACTIVE_STATUS") = ActiveStatus
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
            DT.Rows.Add(DR)
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)
        Else
            SQL = "SELECT * FROM MS_USER WHERE USER_ID = " & lblId.Text
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            Dim DR As DataRow = DT.Rows(0)
            DR("USER_ID") = lblId.Text
            DR("USER_CODE") = txtCode.Text
            DR("USER_PASSWORD") = txtPassword.Text
            DR("USER_FULLNAME") = txtFullname.Text
            DR("USER_TYPE_ID") = ddlType.SelectedValue.ToString
            DR("ACTIVE_STATUS") = ActiveStatus
            DR("UPDATE_BY") = Session("User_ID")
            DR("UPDATE_DATE") = Now
            Dim cmd As New SqlCommandBuilder(DA)
            DA.Update(DT)

            SQL = "DELETE FROM MS_USER_BRANCH WHERE USER_ID = " & lblId.Text
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            Dim objCmd As SqlCommand
            objCmd = New SqlCommand(SQL, conn)
            objCmd.ExecuteNonQuery()
            conn.Close()
        End If

        SQL = "SELECT * FROM MS_USER_BRANCH WHERE USER_ID = ''"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        For i As Int32 = 0 To cblBranch.Items.Count - 1
            If cblBranch.Items(i).Selected = True Then
                Dim DR As DataRow = DT.NewRow
                DR("USER_ID") = lblId.Text
                DR("BRANCH_ID") = cblBranch.Items(i).Value.ToString
                DT.Rows.Add(DR)
                Dim cmd As New SqlCommandBuilder(DA)
                DA.Update(DT)
            End If
        Next

        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('Save success');", True)
        EditMode = Mode.Edit
    End Sub

    Protected Sub imgCodeDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCodeDialog.Click
        Dim Script As String = "requireTextboxDialog('UserFinding.aspx',875,540,'" & txtTmpCode.ClientID & "','" & btnCodeDialog.ClientID & "');"
        ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
    End Sub

    Protected Sub btnCodeDialog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCodeDialog.Click
        Dim SQL As String = "SELECT * FROM MS_USER" & vbNewLine
        SQL &= "WHERE MS_USER.USER_ID = " & txtTmpCode.Text
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Validation", "alert('User has not found');", True)
            Exit Sub
        End If
        lblId.Text = DT.Rows(0).Item("USER_ID").ToString
        txtCode.Text = DT.Rows(0).Item("USER_CODE").ToString
        txtFullname.Text = DT.Rows(0).Item("USER_FULLNAME").ToString
        txtPassword.Text = DT.Rows(0).Item("USER_PASSWORD").ToString
        ddlType.SelectedValue = DT.Rows(0).Item("USER_TYPE_ID").ToString.Trim
        If DT.Rows(0).Item("ACTIVE_STATUS") = "True" Then
            ddlStatus.SelectedIndex = 0
        Else
            ddlStatus.SelectedIndex = 1
        End If

        SQL = "SELECT * FROM MS_USER_BRANCH WHERE USER_ID = " & txtTmpCode.Text
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        For i As Int32 = 0 To cblBranch.Items.Count - 1
            For j As Int32 = 0 To DT.Rows.Count - 1
                If cblBranch.Items(i).Value = DT.Rows(j).Item("BRANCH_ID") Then
                    cblBranch.Items(i).Selected = True
                End If
            Next
        Next
        EditMode = Mode.Edit

    End Sub
End Class

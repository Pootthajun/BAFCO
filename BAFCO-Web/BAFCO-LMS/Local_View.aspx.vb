﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Local_View
    Inherits System.Web.UI.Page

    Dim ConnStr As String = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
    Dim GL As New GenericLib

    Private Property ShipmentID() As Integer
        Get
            If IsNumeric(ViewState("ShipmentID")) Then
                Return ViewState("ShipmentID")
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ShipmentID") = value
        End Set
    End Property

    Private Property ShipmentCode() As String
        Get
            If ViewState("ShipmentCode").ToString <> "" Then
                Return ViewState("ShipmentCode")
            Else
                Return ""
            End If

        End Get
        Set(ByVal value As String)
            ViewState("ShipmentCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ShipmentCode = Request.QueryString("ShipmentCode")
            If ShipmentCode = "" Then
                Response.Redirect("Shipment.aspx", True)
                Exit Sub
            Else
                Dim SQL_ As String = ""
                SQL_ = "SELECT SHIPMENT.SHIPMENT_ID,STATUS_ID,REF_WORK_ORDER_NO,inv_Lot_No FROM SHIPMENT LEFT JOIN SHIPMENT_WMS ON SHIPMENT.SHIPMENT_ID = SHIPMENT_WMS.SHIPMENT_ID WHERE SHIPMENT_CODE = '" & ShipmentCode & "'"
                Dim DA_ As New SqlDataAdapter(SQL_, ConnStr)
                Dim DT_ As New DataTable
                DA_.Fill(DT_)
                If DT_.Rows.Count > 0 Then
                    ShipmentID = DT_.Rows(0).Item("SHIPMENT_ID").ToString
                    lblRefLotNo.Text = DT_.Rows(0).Item("inv_Lot_No").ToString
                Else
                    Response.Redirect("Shipment.aspx", True)
                    Exit Sub
                End If
            End If

            Dim SQL As String = ""
            SQL = "SELECT BRANCH_NAME,COMPANY_NAME,WORK_ORDER_NO,STATUS_ID FROM SHIPMENT LEFT JOIN MS_BRANCH ON SHIPMENT.BRANCH_ID = MS_BRANCH.BRANCH_ID LEFT JOIN MS_COMPANY ON SHIPMENT.COMPANY_ID = MS_COMPANY.COMPANY_ID WHERE SHIPMENT_ID = " & ShipmentID
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                lblCompany.Text = DT.Rows(0).Item("COMPANY_NAME").ToString
                lblWorkOrderNo.Text = DT.Rows(0).Item("WORK_ORDER_NO").ToString
            End If

            BindDataLocal1()
            BindDataLocal2()
            BindDataLocal3()

            For Each ctrl As Control In PanelForm.Controls
                If TypeOf ctrl Is TextBox Then
                    CType(ctrl, TextBox).ReadOnly = True
                End If
            Next

        End If
    End Sub

#Region "Local1"
    Sub BindDataLocal1()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_LOCAL1 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("RECEIVE_DATE").ToString <> "" Then
                lblReceivedDate.Text = DT.Rows(0).Item("RECEIVE_DATE").ToString
                lblImport1.Text = DT.Rows(0).Item("RECEIVE_DATE").ToString
            End If
            If DT.Rows(0).Item("PROJECT_OWNER_NAME").ToString <> "" Then
                lblProjectOwner.Text = DT.Rows(0).Item("PROJECT_OWNER_NAME").ToString
            End If
            If DT.Rows(0).Item("SUPPLIER_NAME").ToString <> "" Then
                lblSupplier.Text = DT.Rows(0).Item("SUPPLIER_NAME").ToString
            End If
            If DT.Rows(0).Item("QTY").ToString <> "" Then
                lblQty.Text = FormatNumber(DT.Rows(0).Item("QTY").ToString, 0)
            End If
            lblUOM.Text = DT.Rows(0).Item("UOM_NAME").ToString
            If DT.Rows(0).Item("WEIGHT").ToString <> "" Then
                lblWeight.Text = FormatNumber(DT.Rows(0).Item("WEIGHT").ToString)
            End If
            If DT.Rows(0).Item("VOLUME").ToString <> "" Then
                lblVolume.Text = FormatNumber(DT.Rows(0).Item("VOLUME").ToString, 0)
            End If
            If DT.Rows(0).Item("DESCRIPTION").ToString <> "" Then
                lblDescriptionOfGoods.Text = DT.Rows(0).Item("DESCRIPTION").ToString
            End If
        End If

        SQL = "SELECT PO_NO FROM SHIPMENT_WMS WMS LEFT JOIN WMS_PO PO ON WMS.inv_Lot_No = PO.inv_Lot_No WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY PO_NO"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            rptData_L1.DataSource = DT
            rptData_L1.DataBind()
        Else
            PO_L1.Visible = False
        End If
    End Sub

    Protected Sub rptData_L1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_L1.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblPONo As Label = e.Item.FindControl("lblPONo")
        lblPONo.Text = e.Item.DataItem("PO_NO").ToString
    End Sub
#End Region
#Region "Local2"
    Sub BindDataLocal2()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_LOCAL2 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("PICK_DATE").ToString <> "" Then
                'lblPickupDate.Text = DT.Rows(0).Item("PICK_DATE").ToString
                lblImport2.Text = DT.Rows(0).Item("PICK_DATE").ToString
            End If
            If DT.Rows(0).Item("PACKING").ToString <> "" Then
                lblPacking.Text = DT.Rows(0).Item("PACKING").ToString
            End If
            If DT.Rows(0).Item("REPACKING").ToString <> "" Then
                lblRepacking.Text = DT.Rows(0).Item("REPACKING").ToString
            End If
            If DT.Rows(0).Item("REMARK").ToString <> "" Then
                lblRemark.Text = DT.Rows(0).Item("REMARK").ToString
            End If
            If DT.Rows(0).Item("COMMENT").ToString <> "" Then
                lblComment_L2.Text = DT.Rows(0).Item("COMMENT").ToString
            End If
        End If

        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM LOCAL_STORE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            rptData_L2.DataSource = DT
            rptData_L2.DataBind()
        Else
            Upload_L2.Visible = False
        End If

        SQL = "SELECT MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID,TYPE_OF_VEHICLE_NAME,AMOUNT,LICENSE_PLATE FROM MS_TYPE_OF_VEHICLE" & vbNewLine
        SQL &= "Left Join" & vbNewLine
        SQL &= "(" & vbNewLine
        SQL &= "    SELECT TYPE_OF_VEHICLE_ID,AMOUNT,truck_license AS LICENSE_PLATE " & vbNewLine
        SQL &= "    FROM SHIPMENT_WMS LEFT JOIN WMS_VEHICLE" & vbNewLine
        SQL &= "    ON SHIPMENT_WMS.inv_Lot_No = WMS_VEHICLE.inv_Lot_No" & vbNewLine
        SQL &= "    WHERE SHIPMENT_ID = " & ShipmentID & vbNewLine
        SQL &= ") AMOUNT" & vbNewLine
        SQL &= "ON MS_TYPE_OF_VEHICLE.TYPE_OF_VEHICLE_ID = AMOUNT.TYPE_OF_VEHICLE_ID" & vbNewLine
        SQL &= "WHERE ACTIVE_STATUS = 1"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        rptTypeOfVehicle.DataSource = DT
        rptTypeOfVehicle.DataBind()
    End Sub

    Protected Sub rptData_L2_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptData_L2.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
    End Sub

    Protected Sub rptData_L2_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptData_L2.ItemCommand
        Select Case e.CommandName
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload_L2()
                Dim pathfile As String = ""
                pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetDataFileUpload_L2() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptData_L2.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

#End Region
#Region "Local3"
    Sub BindDataLocal3()
        Dim SQL As String = ""
        SQL = "SELECT * FROM VW_LOCAL3 WHERE SHIPMENT_ID = " & ShipmentID
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        If DT.Rows.Count > 0 Then
            If DT.Rows(0).Item("DELIVERY_DATE").ToString <> "" Then
                lblDeliveryDate.Text = DT.Rows(0).Item("DELIVERY_DATE").ToString
                lblImport3.Text = DT.Rows(0).Item("DELIVERY_DATE").ToString
            End If
            If DT.Rows(0).Item("COMMENT").ToString <> "" Then
                lblComment_L3.Text = DT.Rows(0).Item("COMMENT").ToString
            End If
            If DT.Rows(0).Item("DELIVERY_NAME").ToString <> "" Then
                lblPlaceOfDelivery.Text = DT.Rows(0).Item("DELIVERY_NAME").ToString
            End If
            If DT.Rows(0).Item("REF_DOC").ToString <> "" Then
                lblReferenceDoc.Text = DT.Rows(0).Item("REF_DOC").ToString
            End If
        End If

        SQL = "SELECT FILE_NAME_OLD,FILE_NAME_NEW,'Old' as STATUS FROM LOCAL_RELEASE_UPLOAD WHERE SHIPMENT_ID = " & ShipmentID & " ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            rptdata_L3.DataSource = DT
            rptdata_L3.DataBind()
        Else
            Upload_L3.Visible = False
        End If

    End Sub

    Protected Sub rptData_L3_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptdata_L3.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim lblFileNameNew As Label = e.Item.FindControl("lblFileNameNew")
        Dim lblFileNameOld As Label = e.Item.FindControl("lblFileNameOld")
        lblFileNameNew.Text = e.Item.DataItem("FILE_NAME_NEW").ToString
        lblFileNameOld.Text = e.Item.DataItem("FILE_NAME_OLD").ToString
    End Sub

    Protected Sub rptData_L3_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptdata_L3.ItemCommand
        Select Case e.CommandName
            Case "View"
                Dim DT As New DataTable
                DT = GetDataFileUpload_L3()
                Dim pathfile As String = ""
                pathfile = Server.MapPath("~/Upload/" + DT.Rows(e.Item.ItemIndex).Item("FILE_NAME_NEW").ToString)
                Session("PathImage") = pathfile
                Dim Script As String = "openPrintWindow('Viewfile.aspx',800,500);"
                ScriptManager.RegisterStartupScript(Me.Page, GetType(String), "Dialog", Script, True)
        End Select
    End Sub

    Function GetDataFileUpload_L3() As DataTable
        Dim DT As New DataTable
        DT.Columns.Add("FILE_NAME_NEW")
        DT.Columns.Add("FILE_NAME_OLD")
        DT.Columns.Add("STATUS")
        For Each ri As RepeaterItem In rptdata_L3.Items
            If ri.ItemType <> ListItemType.Item And ri.ItemType <> ListItemType.AlternatingItem Then Continue For
            Dim lblFileNameNew As Label = ri.FindControl("lblFileNameNew")
            Dim lblFileNameOld As Label = ri.FindControl("lblFileNameOld")
            Dim lblStatus As Label = ri.FindControl("lblStatus")

            Dim DR As DataRow = DT.NewRow
            DR("FILE_NAME_NEW") = lblFileNameNew.Text
            DR("FILE_NAME_OLD") = lblFileNameOld.Text
            DR("STATUS") = lblStatus.Text
            DT.Rows.Add(DR)
        Next
        Return DT
    End Function

    Protected Sub rptTypeOfVehicle_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptTypeOfVehicle.ItemDataBound
        If e.Item.ItemType <> ListItemType.AlternatingItem And e.Item.ItemType <> ListItemType.Item Then Exit Sub
        Dim txtTypeOfVehicle As TextBox = e.Item.FindControl("txtTypeOfVehicle")
        Dim lblTypeOfVehicleID As Label = e.Item.FindControl("lblTypeOfVehicleID")
        Dim lblTypeOfVehicleName As Label = e.Item.FindControl("lblTypeOfVehicleName")
        Dim txtLicensePlate As TextBox = e.Item.FindControl("txtLicensePlate")
        lblTypeOfVehicleID.Text = e.Item.DataItem("TYPE_OF_VEHICLE_ID").ToString
        lblTypeOfVehicleName.Text = e.Item.DataItem("TYPE_OF_VEHICLE_NAME").ToString
        txtTypeOfVehicle.Text = e.Item.DataItem("AMOUNT").ToString
        txtLicensePlate.Text = e.Item.DataItem("LICENSE_PLATE").ToString
    End Sub
#End Region

End Class

﻿namespace AutoCompleteComboBox
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.suggestComboBox1 = new AutoCompleteComboBox.SuggestComboBox();
            this.SuspendLayout();
            // 
            // suggestComboBox1
            // 
            this.suggestComboBox1.FilterRule = null;
            this.suggestComboBox1.FormattingEnabled = true;
            this.suggestComboBox1.Location = new System.Drawing.Point(37, 12);
            this.suggestComboBox1.Name = "suggestComboBox1";
            this.suggestComboBox1.Size = new System.Drawing.Size(175, 21);
            this.suggestComboBox1.SuggestBoxHeight = 96;
            this.suggestComboBox1.SuggestListOrderRule = null;
            this.suggestComboBox1.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 172);
            this.Controls.Add(this.suggestComboBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Custom SuggestComboBox";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SuggestComboBox suggestComboBox1;




    }
}


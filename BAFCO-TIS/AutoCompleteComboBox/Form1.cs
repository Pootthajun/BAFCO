﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace AutoCompleteComboBox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            suggestComboBox1.DataSource = new[]
                {
                    "Janean Mcgaha",
                    "Tama Gaitan",
                    "Jacque Tinnin",
                    "Elvira Woolfolk",
                    "Fransisca Owens",
                    "Minnie Ardoin",
                    "Renay Bentler",
                    "Joye Boyter",
                    "Jaime Flannery",
                    "Maryland Arai",
                    "Walton Edelstein",
                    "Nereida Storrs",
                    "Theron Zinn",
                    "Katharyn Estrella",
                    "Alline Dubin",
                    "Edra Bhatti",
                    "Willa Jeppson",
                    "Chelsea Revel",
                    "Sonya Lowy",
                    "Danelle Kapoor"
                };
            
            TrySomeThings(); // <-- comment this to see the standard behavior
            suggestComboBox1.SelectedIndex = -1;
        }

        private void TrySomeThings()
        {
            // assume you bind a list of persons to the ComboBox with 'Name' as DisplayMember:
            suggestComboBox1.DataSource = suggestComboBox1.Items.Cast<string>().Select(i => new Person{Name = i}).ToList();
            suggestComboBox1.DisplayMember = "Name";

            // then you have to set the PropertySelector like this:
            suggestComboBox1.PropertySelector = collection => collection.Cast<Person>().Select(p => p.Name);

            // filter rule can be customized: e.g. a StartsWith search:
            //suggestComboBox1.FilterRule = (item, text) => item.StartsWith(text.Trim(), StringComparison.CurrentCultureIgnoreCase);
            suggestComboBox1.FilterRule = (item, text) => item.Contains(text.Trim());

            // ordering rule can also be customized: e.g. order by the surname:
            suggestComboBox1.SuggestListOrderRule = s => s.Split(' ')[1];
            
        }
    }

    class Person
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Height { get; set; }
    }
}

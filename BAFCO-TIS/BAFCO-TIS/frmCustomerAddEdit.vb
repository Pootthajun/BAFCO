﻿Imports System.Data
Imports System.Data.SqlClient
Imports OfficeOpenXml

Public Class frmCustomerAddEdit

    'Dim DA As New SqlDataAdapter
    Public CusID As Long = 0

    Private Sub frmCustomerAddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub frmCustomerAddEdit_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown

        ClearForm()
        If CusID > 0 Then
            FillInData()
        End If

    End Sub

    Private Sub ClearForm()
        lblCusID.Text = "0"
        txtAccountNo.Text = ""
        txtTaxID.Text = ""
        txtCustomerName.Text = ""
        txtCustomerAddr.Text = ""
        BindCBB_CustomerType(cbbCustomerType)
        BindCBB_CreditTerm(cbbCreditTerm)
        txtBillMethod.Text = ""
        txtPaymentMethod.Text = ""
        txtOperContactName.Text = ""
        txtOperTelNo.Text = ""
        txtOperMobileNo.Text = ""
        txtOperEmail.Text = ""

        txtAccContactName.Text = ""
        txtAccTelNo.Text = ""
        txtAccMobileNo.Text = ""
        txtAccEmail.Text = ""
        txtCustomerNote.Text = ""

        SetGridviewBranch()
        SetGridviewScope()
    End Sub

    Private Sub SetGridviewScope()
        dgvScope.AutoGenerateColumns = False
        Dim p(1) As SqlParameter
        Dim sql As String = ""

        sql = "select 0 id, bb.id ms_customer_scope_id, bb.scope_name,  "
        sql += " 'N' chk_status "
        sql += " from MS_CUSTOMER_SCOPE bb "
        sql += " where bb.active_status='Y'"

        Dim dt As DataTable = Execute_DataTable(sql, p)
        dgvScope.DataSource = dt
    End Sub

    Private Sub SetGridviewBranch()
        dgvBranch.AutoGenerateColumns = False

        Dim p(1) As SqlParameter
        Dim sql As String = ""

        sql = "select 0 id, bb.id ms_bafco_branch_id, bb.branch_name,  "
        sql += " 'N' chk_status "
        sql += " from MS_BAFCO_BRANCH bb "
        sql += " where bb.active_status='Y'"

        Dim dt As DataTable = Execute_DataTable(sql, p)
        dgvBranch.DataSource = dt

    End Sub

    'Private Sub SetDDLScope()
    '    Dim sql As String = "select id,scope_name from ms_customer_scope order by scope_name"
    '    Dim dt As DataTable = Execute_DataTable(sql)
    '    Dim dr As DataRow = dt.NewRow
    '    dr("id") = 0
    '    dr("scope_name") = "Select"
    '    dt.Rows.InsertAt(dr, 0)

    '    cbbScope.ValueMember = "id"
    '    cbbScope.DisplayMember = "scope_name"
    '    cbbScope.DataSource = dt
    'End Sub

    
    


    Private Sub FillInData()

        Dim SQL As String = ""
        SQL = "SELECT * FROM CUSTOMER WHERE CUS_ID=@_CUS_ID"
        Dim p(1) As SqlParameter
        p(0) = SetBigInt("@_CUS_ID", CusID)
        Dim dt As DataTable = Execute_DataTable(SQL, p)
        If dt.Rows.Count > 0 Then
            Dim dr As DataRow = dt.Rows(0)

            lblCusID.Text = CusID
            If Convert.IsDBNull(dr("cus_account")) = False Then txtAccountNo.Text = dr("cus_account")
            If Convert.IsDBNull(dr("tax_id")) = False Then txtTaxID.Text = dr("tax_id")
            If Convert.IsDBNull(dr("cus_name")) = False Then txtCustomerName.Text = dr("cus_name")
            If Convert.IsDBNull(dr("cus_address")) = False Then txtCustomerAddr.Text = dr("cus_address")
            If Convert.IsDBNull(dr("cus_type_id")) = False Then cbbCustomerType.SelectedValue = dr("cus_type_id")
            If Convert.IsDBNull(dr("ms_credit_term_id")) = False Then cbbCreditTerm.SelectedValue = dr("ms_credit_term_id")
            If Convert.IsDBNull(dr("billing_method")) = False Then txtBillMethod.Text = dr("billing_method")
            If Convert.IsDBNull(dr("payment_method")) = False Then txtPaymentMethod.Text = dr("payment_method")
            If Convert.IsDBNull(dr("customer_note")) = False Then txtCustomerNote.Text = dr("customer_note")

            'Fill in Gridview Branch
            SQL = " select bc.ms_bafco_branch_id "
            SQL += " from MS_BRANCH_CUSTOMER bc "
            SQL += " where bc.cus_id=@_CUS_ID  "
            ReDim p(1)
            p(0) = SetBigInt("@_CUS_ID", CusID)
            Dim bDt As DataTable = Execute_DataTable(SQL, p)
            If bDt.Rows.Count > 0 Then
                For Each dgv As DataGridViewRow In dgvBranch.Rows
                    bDt.DefaultView.RowFilter = "ms_bafco_branch_id='" & dgv.Cells("colBranchID").Value & "'"
                    If bDt.DefaultView.Count > 0 Then
                        dgv.Cells("colChk").Value = "Y"
                    End If
                    bDt.DefaultView.RowFilter = ""
                Next
            End If
            bDt.Dispose()


            'Fill in Gridview Scope
            SQL = "select ms_customer_scope_id "
            SQL += " from MS_CUSTOMER_ASSIGN_SCOPE "
            SQL += " where cus_id=@_CUS_ID"
            ReDim p(1)
            p(0) = SetBigInt("@_CUS_ID", CusID)
            Dim sDt As DataTable = Execute_DataTable(SQL, p)
            If sDt.Rows.Count > 0 Then
                For Each dgv As DataGridViewRow In dgvScope.Rows
                    sDt.DefaultView.RowFilter = "ms_customer_scope_id='" & dgv.Cells("colCustomerScopeID").Value & "'"
                    If sDt.DefaultView.Count > 0 Then
                        dgv.Cells("colScopeChk").Value = "Y"
                    End If
                    sDt.DefaultView.RowFilter = ""
                Next
            End If
            sDt.Dispose()


            SQL = "select contact_type, contact_name, tel_no, mobile_no, email "
            SQL += " from CUSTOMER_CONTACT_PERSON "
            SQL += " where customer_id=@_CUS_ID"
            ReDim p(1)
            p(0) = SetBigInt("@_CUS_ID", CusID)

            dt = Execute_DataTable(SQL, p)
            If dt.Rows.Count > 0 Then
                dt.DefaultView.RowFilter = "contact_type=" & Convert.ToInt16(CustomerContactType.Operation)
                If dt.DefaultView.Count > 0 Then
                    If Convert.IsDBNull(dt.DefaultView(0)("contact_name")) = False Then txtOperContactName.Text = dt.DefaultView(0)("contact_name")
                    If Convert.IsDBNull(dt.DefaultView(0)("tel_no")) = False Then txtOperTelNo.Text = dt.DefaultView(0)("tel_no")
                    If Convert.IsDBNull(dt.DefaultView(0)("mobile_no")) = False Then txtOperMobileNo.Text = dt.DefaultView(0)("mobile_no")
                    If Convert.IsDBNull(dt.DefaultView(0)("email")) = False Then txtOperEmail.Text = dt.DefaultView(0)("email")
                End If
                dt.DefaultView.RowFilter = ""
                
                dt.DefaultView.RowFilter = "contact_type=" & Convert.ToInt16(CustomerContactType.Account)
                If dt.DefaultView.Count > 0 Then
                    If Convert.IsDBNull(dt.DefaultView(0)("contact_name")) = False Then txtAccContactName.Text = dt.DefaultView(0)("contact_name")
                    If Convert.IsDBNull(dt.DefaultView(0)("tel_no")) = False Then txtAccTelNo.Text = dt.DefaultView(0)("tel_no")
                    If Convert.IsDBNull(dt.DefaultView(0)("mobile_no")) = False Then txtAccMobileNo.Text = dt.DefaultView(0)("mobile_no")
                    If Convert.IsDBNull(dt.DefaultView(0)("email")) = False Then txtAccEmail.Text = dt.DefaultView(0)("email")
                End If
                dt.DefaultView.RowFilter = ""
            End If
        End If
        dt.Dispose()
    End Sub

    Private Function ValidateData()
        '---------------- Validate---------------
        If txtAccountNo.Text.Trim = "" Then
            MessageBox.Show("กรุณาระบุรหัสลูกค้า", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtTaxID.Focus()
            Return False
        End If
        If txtTaxID.Text.Trim = "" Then
            MessageBox.Show("กรุณาระบุเลขประจำตัวผู้เสียภาษี", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtTaxID.Focus()
            Return False
        End If
        If txtCustomerName.Text.Trim = "" Then
            MessageBox.Show("กรุณาระบุชื่อลูกค้า", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtCustomerName.Focus()
            Return False
        End If
        If txtCustomerAddr.Text.Trim = "" Then
            MessageBox.Show("กรุณาระบุที่อยู่ลูกค้า", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtCustomerAddr.Focus()
            Return False
        End If
        If cbbCustomerType.SelectedValue = "0" Then
            MessageBox.Show("กรุณาเลือกประเภทลูกค้า", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cbbCustomerType.Focus()
            Return False
        End If
        'If cbbScope.SelectedValue = "0" Then
        '    MessageBox.Show("กรุณาเลือก Scope", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '    cbbScope.Focus()
        '    Return False
        'End If
        If cbbCreditTerm.SelectedValue = "0" Then
            MessageBox.Show("กรุณาเลือกเงื่อนไขการชำระเงิน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cbbCreditTerm.Focus()
            Return False
        End If
        If txtOperContactName.Text.Trim = "" Then
            MessageBox.Show("กรุณาระบุชื่อผู้ติดต่อ Operation", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txtOperContactName.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function GetNewAccountNo() As String
        Dim ret As String = DateTime.Now.ToString("yyyyMMddHHmmssfff")


        Return ret
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If ValidateData() = True Then
            Dim sql As String = ""
            Dim p(11) As SqlParameter
            Dim _ID As Integer = 0
            'Dim _AccountNo As String = ""
            If lblCusID.Text = "0" Then
                _ID = GetNewID("CUSTOMER", "cus_id")
                '_AccountNo = GetNewAccountNo()

                sql = "insert into customer (cus_id, cus_name, cus_type_id, update_by, update_date, cus_account, cus_address, tax_id,  ms_credit_term_id, billing_method, payment_method, customer_note) "
                sql += " values (@_CUS_ID, @_CUS_NAME, @_CUS_TYPE_ID, @_UPDATE_BY, getdate(), @_CUS_ACCOUNT, @_CUS_ADDRESS, @_TAX_ID,  @_MS_CREDIT_TERM_ID, @_BILLING_METHOD, @_PAYMENT_METHOD, @_CUSTOMER_NOTE)"

                p(0) = SetInt("@_CUS_ID", _ID)
                p(1) = SetText("@_CUS_NAME", txtCustomerName.Text)
                p(2) = SetInt("@_CUS_TYPE_ID", cbbCustomerType.SelectedValue)
                p(3) = SetInt("@_UPDATE_BY", myUser.user_id)
                p(4) = SetText("@_CUS_ACCOUNT", txtAccountNo.Text)
                p(5) = SetText("@_CUS_ADDRESS", txtCustomerAddr.Text)
                p(6) = SetText("@_TAX_ID", txtTaxID.Text)
                p(7) = SetBigInt("@_MS_CREDIT_TERM_ID", cbbCreditTerm.SelectedValue)
                p(8) = SetText("@_BILLING_METHOD", txtBillMethod.Text)
                p(9) = SetText("@_PAYMENT_METHOD", txtPaymentMethod.Text)
                p(10) = SetText("@_CUSTOMER_NOTE", txtCustomerNote.Text)
            Else
                _ID = lblCusID.Text
                '_AccountNo = txtAccountNo.Text

                sql = " update customer "
                sql += " set cus_name=@_CUS_NAME "
                sql += ", cus_type_id=@_CUS_TYPE_ID "
                sql += ", update_by=@_UPDATE_BY "
                sql += ", update_date=getdate() "
                sql += ", cus_account=@_CUS_ACCOUNT "
                sql += ", cus_address=@_CUS_ADDRESS "
                sql += ", tax_id=@_TAX_ID "
                sql += ", ms_credit_term_id=@_MS_CREDIT_TERM_ID "
                sql += ", billing_method=@_BILLING_METHOD "
                sql += ", payment_method=@_PAYMENT_METHOD "
                sql += ", customer_note=@_CUSTOMER_NOTE "
                sql += " where cus_id=@_CUS_ID"

                p(0) = SetInt("@_CUS_ID", _ID)
                p(1) = SetText("@_CUS_NAME", txtCustomerName.Text)
                p(2) = SetInt("@_CUS_TYPE_ID", cbbCustomerType.SelectedValue)
                p(3) = SetInt("@_UPDATE_BY", myUser.user_id)
                p(4) = SetText("@_CUS_ACCOUNT", txtAccountNo.Text)
                p(5) = SetText("@_CUS_ADDRESS", txtCustomerAddr.Text)
                p(6) = SetText("@_TAX_ID", txtTaxID.Text)
                p(7) = SetBigInt("@_MS_CREDIT_TERM_ID", cbbCreditTerm.SelectedValue)
                p(8) = SetText("@_BILLING_METHOD", txtBillMethod.Text)
                p(9) = SetText("@_PAYMENT_METHOD", txtPaymentMethod.Text)
                p(10) = SetText("@_CUSTOMER_NOTE", txtCustomerNote.Text)
            End If

            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            If conn.State <> ConnectionState.Open Then
                MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                Exit Sub
            End If
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction

            Dim ret As String = Execute_Command(sql, trans, p)
            If ret = "true" Then
                sql = " delete from MS_BRANCH_CUSTOMER where cus_id=@_CUS_ID"
                ReDim p(1)
                p(0) = SetInt("@_CUS_ID", _ID)
                ret = Execute_Command(sql, trans, p)
                If ret = "true" Then
                    For Each grv As DataGridViewRow In dgvBranch.Rows
                        If grv.Cells("colChk").Value = "Y" Then
                            sql = " insert into MS_BRANCH_CUSTOMER(created_by, created_date, ms_bafco_branch_id, cus_id)"
                            sql += " values(@_CREATED_BY, getdate(), @_BRANCH_ID, @_CUS_ID)"

                            ReDim p(3)
                            p(0) = SetText("@_CREATED_BY", myUser.us_code)
                            p(1) = SetBigInt("@_BRANCH_ID", grv.Cells("colBranchID").Value)
                            p(2) = SetInt("@_CUS_ID", _ID)

                            ret = Execute_Command(sql, trans, p)
                            If ret <> "true" Then
                                Exit For
                            End If
                        End If
                    Next
                End If

                If ret = "true" Then
                    sql = " delete from MS_CUSTOMER_ASSIGN_SCOPE where cus_id=@_CUS_ID"
                    ReDim p(1)
                    p(0) = SetInt("@_CUS_ID", _ID)
                    ret = Execute_Command(sql, trans, p)

                    If ret = "true" Then
                        For Each grv As DataGridViewRow In dgvScope.Rows
                            If grv.Cells("colScopeChk").Value = "Y" Then
                                sql = " insert into MS_CUSTOMER_ASSIGN_SCOPE(created_by, created_date, ms_customer_scope_id, cus_id)"
                                sql += " values (@_CREATED_BY, getdate(), @_CUSTOMER_SCOPE_ID, @_CUS_ID)"

                                ReDim p(3)
                                p(0) = SetText("@_CREATED_BY", myUser.us_code)
                                p(1) = SetBigInt("@_CUSTOMER_SCOPE_ID", grv.Cells("colCustomerScopeID").Value)
                                p(2) = SetInt("@_CUS_ID", _ID)

                                ret = Execute_Command(sql, trans, p)
                                If ret <> "true" Then
                                    Exit For
                                End If
                            End If
                        Next
                    End If
                End If


                If ret = "true" Then
                    sql = " delete from customer_contact_person where customer_id=@_CUS_ID"
                    ReDim p(1)
                    p(0) = SetInt("@_CUS_ID", _ID)
                    Execute_Command(sql, trans, p)


                    sql = "insert into customer_contact_person (created_by, created_date, customer_id, contact_type, contact_name, email, tel_no, mobile_no)"
                    sql += " values (@_CREATED_BY, getdate(), @_CUS_ID, @_CONTACT_TYPE, @_CONTACT_NAME, @_EMAIL, @_TEL_NO, @_MOBILE_NO)"
                    ReDim p(7)
                    p(0) = SetText("@_CREATED_BY", myUser.us_code)
                    p(1) = SetInt("@_CUS_ID", _ID)
                    p(2) = SetText("@_CONTACT_TYPE", Convert.ToInt16(CustomerContactType.Operation))
                    p(3) = SetText("@_CONTACT_NAME", txtOperContactName.Text)
                    p(4) = SetText("@_EMAIL", txtOperEmail.Text)
                    p(5) = SetText("@_TEL_NO", txtOperTelNo.Text)
                    p(6) = SetText("@_MOBILE_NO", txtOperMobileNo.Text)

                    ret = Execute_Command(sql, trans, p)
                    If ret = "true" Then
                        If txtAccContactName.Text.Trim <> "" Then
                            sql = "insert into customer_contact_person (created_by, created_date, customer_id, contact_type, contact_name, email, tel_no, mobile_no)"
                            sql += " values (@_CREATED_BY, getdate(), @_CUS_ID, @_CONTACT_TYPE, @_CONTACT_NAME, @_EMAIL, @_TEL_NO, @_MOBILE_NO)"
                            ReDim p(7)
                            p(0) = SetText("@_CREATED_BY", myUser.us_code)
                            p(1) = SetInt("@_CUS_ID", _ID)
                            p(2) = SetText("@_CONTACT_TYPE", Convert.ToInt16(CustomerContactType.Account))
                            p(3) = SetText("@_CONTACT_NAME", txtAccContactName.Text)
                            p(4) = SetText("@_EMAIL", txtAccEmail.Text)
                            p(5) = SetText("@_TEL_NO", txtAccTelNo.Text)
                            p(6) = SetText("@_MOBILE_NO", txtAccMobileNo.Text)

                            ret = Execute_Command(sql, trans, p)
                        End If
                    End If
                End If
            End If

            If ret = "true" Then
                trans.Commit()
                lblCusID.Text = _ID
                'txtAccountNo.Text = _AccountNo
                MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                trans.Rollback()
                MessageBox.Show(ret, "Save Fail", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
    End Sub

    Private Sub txtTaxID_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtTaxID.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        ClearForm()
        FillInData()
    End Sub

    Private Sub btnExport_Click(sender As System.Object, e As System.EventArgs) Handles btnExport.Click
        If lblCusID.Text = "0" Then
            MessageBox.Show("Please Save Customer Data", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Using ep As New ExcelPackage
            Dim ws As ExcelWorksheet = ep.Workbook.Worksheets.Add("CustomerData")
            ws.Cells("A1").Value = "Customer Data"
            ws.Cells("A1").Style.Font.Bold = True

            AddCustomerExcelData("Customer Account", txtAccountNo.Text, 3, ws)
            ws.Cells("C3").Value = "Tax ID"
            ws.Cells("D3").Value = txtTaxID.Text

            AddCustomerExcelData("Customer Name", txtCustomerName.Text, 4, ws)
            AddCustomerExcelData("Address", txtCustomerAddr.Text, 5, ws)
            AddCustomerExcelData("Customer Type", cbbCustomerType.Text, 6, ws)
            'AddCustomerExcelData("Scope", cbbScope.Text, 7, ws)
            AddCustomerExcelData("Credit Term", cbbCreditTerm.Text, 8, ws)
            AddCustomerExcelData("Billing Method", txtBillMethod.Text, 9, ws)
            AddCustomerExcelData("Payment Method", txtPaymentMethod.Text, 10, ws)

            ws.Cells("A12").Value = "Operation Contact"
            ws.Cells("A12").Style.Font.Bold = True

            AddCustomerExcelData("Contact Name", txtOperContactName.Text, 13, ws)
            ws.Cells("C13").Value = "Mobile No"
            ws.Cells("D13").Value = txtOperMobileNo.Text
            AddCustomerExcelData("Tel No", txtOperTelNo.Text, 14, ws)
            AddCustomerExcelData("Email", txtOperEmail.Text, 15, ws)

            ws.Cells("A17").Value = "Account Contact"
            ws.Cells("A17").Style.Font.Bold = True

            AddCustomerExcelData("Contact Name", txtAccContactName.Text, 18, ws)
            ws.Cells("C18").Value = "Mobile No"
            ws.Cells("D18").Value = txtAccMobileNo.Text
            AddCustomerExcelData("Tel No", txtAccTelNo.Text, 19, ws)
            AddCustomerExcelData("Email", txtAccEmail.Text, 20, ws)

            AddCustomerExcelData("Note", txtCustomerNote.Text, 22, ws)

            ws.Cells(1, 1, 22, 5).AutoFitColumns()

            'Dim TempExcelCustomer As String = Application.StartupPath & "\TempExcelCustomer\"
            'If IO.Directory.Exists(TempExcelCustomer) = False Then
            '    IO.Directory.CreateDirectory(TempExcelCustomer)
            'End If

            Using fs As New SaveFileDialog
                fs.Filter = "Excel File(*.xlsx)|*.xlsx"
                If fs.ShowDialog = Windows.Forms.DialogResult.OK Then
                    ep.SaveAs(New IO.FileInfo(fs.FileName))
                End If
            End Using


        End Using
    End Sub

    Private Sub AddCustomerExcelData(txtLabel As String, txtData As String, row As Integer, ws As ExcelWorksheet)
        ws.Cells("A" & row).Value = txtLabel
        ws.Cells("B" & row).Value = txtData

        ws.Cells("A" & row).Style.Font.Bold = True
    End Sub
End Class
﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmSupplier

    Dim DA As New SqlDataAdapter

    Private Sub FormServiceType_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        ShowData()
    End Sub

    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT * FROM SUPPLIER ORDER BY SUP_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT
        lblRec.Text = "Total   " & DT.Rows.Count & "   Record"

        SQL = "SELECT SUP_TYPE_ID,SUP_TYPE_NAME FROM SUPPLIER_TYPE ORDER BY SUP_TYPE_NAME"
        Dim DT_TYPE As New DataTable
        Dim DA_TYPE As New SqlDataAdapter(SQL, ConnStr)
        DA_TYPE.Fill(DT_TYPE)

        Dim SUP_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        SUP_ID.Name = "SUP_ID"
        SUP_ID.DataPropertyName = "SUP_ID"
        SUP_ID.Visible = False
        grdMain.Columns.Add(SUP_ID)
        grdMain.Columns("SUP_ID").Visible = False

        Dim SUP_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        SUP_NAME.DataPropertyName = "SUP_NAME"
        SUP_NAME.HeaderText = "Supplier"
        SUP_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        SUP_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(SUP_NAME)

        Dim SUP_TYPE_ID As New System.Windows.Forms.DataGridViewComboBoxColumn()
        SUP_TYPE_ID.DataSource = DT_TYPE
        SUP_TYPE_ID.DisplayMember = "SUP_TYPE_NAME"
        SUP_TYPE_ID.ValueMember = "SUP_TYPE_ID"
        SUP_TYPE_ID.DataPropertyName = "SUP_TYPE_ID"
        SUP_TYPE_ID.HeaderText = "Supplier Group"
        SUP_TYPE_ID.Width = 220
        grdMain.Columns.Add(SUP_TYPE_ID)

        Dim UPDATE_BY As New System.Windows.Forms.DataGridViewTextBoxColumn
        UPDATE_BY.DataPropertyName = "UPDATE_BY"
        UPDATE_BY.Visible = False
        grdMain.Columns.Add(UPDATE_BY)

        Dim UPDATE_DATE As New System.Windows.Forms.DataGridViewTextBoxColumn
        UPDATE_DATE.DataPropertyName = "UPDATE_DATE"
        UPDATE_DATE.Visible = False
        grdMain.Columns.Add(UPDATE_DATE)

        SQL = "SELECT SUP_TYPE_ID,SUP_TYPE_NAME FROM SUPPLIER_TYPE ORDER BY SUP_TYPE_NAME"
        Dim DT_SUP_TYPE As New DataTable
        Dim DA_SUP_TYPE As New SqlDataAdapter(SQL, ConnStr)
        DA_SUP_TYPE.Fill(DT_SUP_TYPE)
        Dim DR As DataRow
        DR = DT_SUP_TYPE.NewRow
        DT_SUP_TYPE.Rows.Add(DR)
        DT_SUP_TYPE.DefaultView.Sort = "SUP_TYPE_NAME ASC"
        cbbSupType.DataSource = DT_SUP_TYPE
        cbbSupType.DisplayMember = "SUP_TYPE_NAME"
        cbbSupType.ValueMember = "SUP_TYPE_ID"

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("SUP_NAME")) OrElse tmp.Rows(i).Item("SUP_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("SUP_TYPE_ID")) OrElse tmp.Rows(i).Item("SUP_TYPE_ID").ToString = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "SUP_NAME='" & tmp.Rows(i).Item("SUP_NAME") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        '------------------Check Deleted Record-----
        Dim SQL As String = ""
        SQL &= "SELECT DISTINCT * FROM (" & vbCrLf
        SQL &= "SELECT DISTINCT SUP_ID FROM COST_SUP" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT DISTINCT SUP_ID FROM SERVICE_SUP" & vbCrLf
        SQL &= ") AS TB" & vbCrLf
        Dim TA As New SqlDataAdapter(SQL, ConnStr)
        Dim TT As New DataTable
        TA.Fill(TT)
        For i As Integer = 0 To TT.Rows.Count - 1
            If IsDBNull(TT.Rows(i).Item("SUP_ID")) Then Continue For
            tmp.DefaultView.RowFilter = "SUP_ID=" & TT.Rows(i).Item("SUP_ID")
            If tmp.DefaultView.Count = 0 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลที่คุณต้องการลบ ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ShowData()
                Exit Sub
            End If
        Next
        '------------------Batch Save---------------
        Dim ID As Integer = 0
        ID = GetNewID("SUPPLIER", "SUP_ID")
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(DT.Rows(i).Item("SUP_ID")) Then
                DT.Rows(i).Item("SUP_ID") = ID
                ID = ID + 1
                DT.Rows(i).Item("UPDATE_BY") = myUser.user_id
                DT.Rows(i).Item("UPDATE_DATE") = Now
            End If

        Next

        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            Exit Sub
        End Try

        txtSearch.Text = ""
        cbbSupType.SelectedIndex = 0
        ShowData()
        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp, cbbSupType.SelectionChangeCommitted
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= "SUP_ID IS NULL OR  "
        End If
        Filter &= "SUP_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' AND "
        If cbbSupType.Text <> "" Then
            Filter &= "SUP_TYPE_ID = " & cbbSupType.SelectedValue & " AND "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        FormServiceType_Load(Nothing, Nothing)
    End Sub
End Class
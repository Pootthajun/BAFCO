﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmQuotationCaseByCase
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rp_cus_all = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cbbCusBillTo = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.lblAbbQuotation = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.lblID = New System.Windows.Forms.Label()
        Me.cbbTerm = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbbScope = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cbbMode = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCustomerAcc = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCustomerAddress = New System.Windows.Forms.TextBox()
        Me.cbbCustomer = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtCommodity = New System.Windows.Forms.TextBox()
        Me.txtPortNameDest = New System.Windows.Forms.TextBox()
        Me.txtPortNameOrigin = New System.Windows.Forms.TextBox()
        Me.txtCityNameDest = New System.Windows.Forms.TextBox()
        Me.txtCityNameOrigin = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtDimension = New System.Windows.Forms.TextBox()
        Me.txtPivot = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtVol = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtGW = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.cbbCountryDest = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cbbCountryOrigin = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.txtRemark = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblQtStatus = New System.Windows.Forms.Label()
        Me.txtStatusName = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtReferenceNo = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtSaleName = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtJobNo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtQuotationNo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtQuotationDate = New System.Windows.Forms.TextBox()
        Me.btnDeny = New System.Windows.Forms.Button()
        Me.btnApprove = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.UcByCaseQuotationTruckDescription1 = New BAFCO_TIS.UCByCaseQuotationTruckDescription()
        Me.UcByCaseQuotationTermCondition1 = New BAFCO_TIS.UCByCaseQuotationTermCondition()
        Me.UcByCaseQuotationDescription1 = New BAFCO_TIS.UCByCaseQuotationDescription()
        Me.UcByCaseQuotationEstimateTotal1 = New BAFCO_TIS.UCByCaseQuotationEstimateTotal()
        Me.btnOverwrite = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'rp_cus_all
        '
        Me.rp_cus_all.AutoSize = True
        Me.rp_cus_all.Checked = True
        Me.rp_cus_all.Location = New System.Drawing.Point(64, 36)
        Me.rp_cus_all.Name = "rp_cus_all"
        Me.rp_cus_all.Size = New System.Drawing.Size(36, 17)
        Me.rp_cus_all.TabIndex = 63
        Me.rp_cus_all.TabStop = True
        Me.rp_cus_all.Text = "All"
        Me.rp_cus_all.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cbbCusBillTo)
        Me.Panel1.Controls.Add(Me.Label29)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.lblAbbQuotation)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.lblID)
        Me.Panel1.Controls.Add(Me.cbbTerm)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.cbbScope)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.cbbMode)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.txtCustomerAcc)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.txtCustomerAddress)
        Me.Panel1.Controls.Add(Me.cbbCustomer)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Location = New System.Drawing.Point(5, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(720, 102)
        Me.Panel1.TabIndex = 114
        '
        'cbbCusBillTo
        '
        Me.cbbCusBillTo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbbCusBillTo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbCusBillTo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbCusBillTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbCusBillTo.FormattingEnabled = True
        Me.cbbCusBillTo.Location = New System.Drawing.Point(103, 76)
        Me.cbbCusBillTo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbCusBillTo.Name = "cbbCusBillTo"
        Me.cbbCusBillTo.Size = New System.Drawing.Size(362, 24)
        Me.cbbCusBillTo.TabIndex = 143
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label29.Location = New System.Drawing.Point(39, 81)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(62, 13)
        Me.Label29.TabIndex = 142
        Me.Label29.Text = "BILL TO :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.BackColor = System.Drawing.Color.Transparent
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label28.ForeColor = System.Drawing.Color.Red
        Me.Label28.Location = New System.Drawing.Point(470, 61)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(12, 13)
        Me.Label28.TabIndex = 101
        Me.Label28.Text = "*"
        '
        'lblAbbQuotation
        '
        Me.lblAbbQuotation.AutoSize = True
        Me.lblAbbQuotation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblAbbQuotation.Location = New System.Drawing.Point(352, 46)
        Me.lblAbbQuotation.Name = "lblAbbQuotation"
        Me.lblAbbQuotation.Size = New System.Drawing.Size(0, 16)
        Me.lblAbbQuotation.TabIndex = 100
        Me.lblAbbQuotation.Visible = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.Red
        Me.Label26.Location = New System.Drawing.Point(474, 33)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(12, 13)
        Me.Label26.TabIndex = 99
        Me.Label26.Text = "*"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Red
        Me.Label25.Location = New System.Drawing.Point(7, 6)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(12, 13)
        Me.Label25.TabIndex = 98
        Me.Label25.Text = "*"
        '
        'lblID
        '
        Me.lblID.AutoSize = True
        Me.lblID.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblID.Location = New System.Drawing.Point(121, 30)
        Me.lblID.Name = "lblID"
        Me.lblID.Size = New System.Drawing.Size(16, 16)
        Me.lblID.TabIndex = 97
        Me.lblID.Text = "0"
        Me.lblID.Visible = False
        '
        'cbbTerm
        '
        Me.cbbTerm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbTerm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbTerm.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbTerm.FormattingEnabled = True
        Me.cbbTerm.Location = New System.Drawing.Point(541, 76)
        Me.cbbTerm.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbTerm.Name = "cbbTerm"
        Me.cbbTerm.Size = New System.Drawing.Size(172, 24)
        Me.cbbTerm.TabIndex = 3
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.Location = New System.Drawing.Point(484, 81)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 95
        Me.Label9.Text = "TERM :"
        '
        'cbbScope
        '
        Me.cbbScope.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbScope.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbScope.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbScope.FormattingEnabled = True
        Me.cbbScope.Location = New System.Drawing.Point(541, 51)
        Me.cbbScope.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbScope.Name = "cbbScope"
        Me.cbbScope.Size = New System.Drawing.Size(172, 24)
        Me.cbbScope.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(478, 56)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 13)
        Me.Label8.TabIndex = 94
        Me.Label8.Text = "SCOPE :"
        '
        'cbbMode
        '
        Me.cbbMode.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbMode.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbMode.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbMode.FormattingEnabled = True
        Me.cbbMode.Location = New System.Drawing.Point(541, 26)
        Me.cbbMode.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbMode.Name = "cbbMode"
        Me.cbbMode.Size = New System.Drawing.Size(172, 24)
        Me.cbbMode.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(483, 32)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 13)
        Me.Label7.TabIndex = 92
        Me.Label7.Text = "MODE :"
        '
        'txtCustomerAcc
        '
        Me.txtCustomerAcc.Enabled = False
        Me.txtCustomerAcc.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCustomerAcc.Location = New System.Drawing.Point(597, 2)
        Me.txtCustomerAcc.MaxLength = 100
        Me.txtCustomerAcc.Name = "txtCustomerAcc"
        Me.txtCustomerAcc.Size = New System.Drawing.Size(116, 23)
        Me.txtCustomerAcc.TabIndex = 90
        Me.txtCustomerAcc.Text = "12345678"
        Me.txtCustomerAcc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.Location = New System.Drawing.Point(483, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(108, 13)
        Me.Label6.TabIndex = 89
        Me.Label6.Text = "CUSTOMER ACC:"
        '
        'txtCustomerAddress
        '
        Me.txtCustomerAddress.Enabled = False
        Me.txtCustomerAddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCustomerAddress.Location = New System.Drawing.Point(7, 26)
        Me.txtCustomerAddress.MaxLength = 100
        Me.txtCustomerAddress.Multiline = True
        Me.txtCustomerAddress.Name = "txtCustomerAddress"
        Me.txtCustomerAddress.Size = New System.Drawing.Size(458, 49)
        Me.txtCustomerAddress.TabIndex = 88
        Me.txtCustomerAddress.Text = "ADDRESS"
        '
        'cbbCustomer
        '
        Me.cbbCustomer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbbCustomer.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbCustomer.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbCustomer.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbCustomer.FormattingEnabled = True
        Me.cbbCustomer.Location = New System.Drawing.Point(103, 2)
        Me.cbbCustomer.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbCustomer.Name = "cbbCustomer"
        Me.cbbCustomer.Size = New System.Drawing.Size(362, 24)
        Me.cbbCustomer.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(17, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 13)
        Me.Label5.TabIndex = 87
        Me.Label5.Text = "CUSTOMER :"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtCommodity)
        Me.Panel2.Controls.Add(Me.txtPortNameDest)
        Me.Panel2.Controls.Add(Me.txtPortNameOrigin)
        Me.Panel2.Controls.Add(Me.txtCityNameDest)
        Me.Panel2.Controls.Add(Me.txtCityNameOrigin)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Me.Label24)
        Me.Panel2.Controls.Add(Me.txtDimension)
        Me.Panel2.Controls.Add(Me.txtPivot)
        Me.Panel2.Controls.Add(Me.Label19)
        Me.Panel2.Controls.Add(Me.txtVol)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Me.txtGW)
        Me.Panel2.Controls.Add(Me.Label17)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Me.txtQty)
        Me.Panel2.Controls.Add(Me.Label15)
        Me.Panel2.Controls.Add(Me.Label14)
        Me.Panel2.Controls.Add(Me.cbbCountryDest)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.Label12)
        Me.Panel2.Controls.Add(Me.cbbCountryOrigin)
        Me.Panel2.Controls.Add(Me.Label3)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Location = New System.Drawing.Point(5, 106)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(720, 102)
        Me.Panel2.TabIndex = 115
        '
        'txtCommodity
        '
        Me.txtCommodity.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCommodity.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCommodity.Location = New System.Drawing.Point(167, 76)
        Me.txtCommodity.MaxLength = 100
        Me.txtCommodity.Name = "txtCommodity"
        Me.txtCommodity.Size = New System.Drawing.Size(151, 23)
        Me.txtCommodity.TabIndex = 12
        '
        'txtPortNameDest
        '
        Me.txtPortNameDest.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPortNameDest.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPortNameDest.Location = New System.Drawing.Point(432, 52)
        Me.txtPortNameDest.MaxLength = 100
        Me.txtPortNameDest.Name = "txtPortNameDest"
        Me.txtPortNameDest.Size = New System.Drawing.Size(117, 23)
        Me.txtPortNameDest.TabIndex = 11
        '
        'txtPortNameOrigin
        '
        Me.txtPortNameOrigin.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPortNameOrigin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPortNameOrigin.Location = New System.Drawing.Point(167, 52)
        Me.txtPortNameOrigin.MaxLength = 100
        Me.txtPortNameOrigin.Name = "txtPortNameOrigin"
        Me.txtPortNameOrigin.Size = New System.Drawing.Size(151, 23)
        Me.txtPortNameOrigin.TabIndex = 10
        '
        'txtCityNameDest
        '
        Me.txtCityNameDest.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCityNameDest.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCityNameDest.Location = New System.Drawing.Point(432, 27)
        Me.txtCityNameDest.MaxLength = 100
        Me.txtCityNameDest.Name = "txtCityNameDest"
        Me.txtCityNameDest.Size = New System.Drawing.Size(117, 23)
        Me.txtCityNameDest.TabIndex = 9
        '
        'txtCityNameOrigin
        '
        Me.txtCityNameOrigin.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCityNameOrigin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCityNameOrigin.Location = New System.Drawing.Point(432, 2)
        Me.txtCityNameOrigin.MaxLength = 100
        Me.txtCityNameOrigin.Name = "txtCityNameOrigin"
        Me.txtCityNameOrigin.Size = New System.Drawing.Size(117, 23)
        Me.txtCityNameOrigin.TabIndex = 7
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label21.Location = New System.Drawing.Point(345, 81)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(85, 13)
        Me.Label21.TabIndex = 137
        Me.Label21.Text = "DIMENSION :"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label20.Location = New System.Drawing.Point(559, 81)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(52, 13)
        Me.Label20.TabIndex = 135
        Me.Label20.Text = "PIVOT :"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(332, 57)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(99, 13)
        Me.Label13.TabIndex = 123
        Me.Label13.Text = "DESTINATION :"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label24.Location = New System.Drawing.Point(517, 81)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(29, 13)
        Me.Label24.TabIndex = 140
        Me.Label24.Text = "cms"
        '
        'txtDimension
        '
        Me.txtDimension.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtDimension.Location = New System.Drawing.Point(433, 76)
        Me.txtDimension.MaxLength = 100
        Me.txtDimension.Name = "txtDimension"
        Me.txtDimension.Size = New System.Drawing.Size(81, 23)
        Me.txtDimension.TabIndex = 13
        Me.txtDimension.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtPivot
        '
        Me.txtPivot.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPivot.Location = New System.Drawing.Point(612, 76)
        Me.txtPivot.MaxLength = 100
        Me.txtPivot.Name = "txtPivot"
        Me.txtPivot.Size = New System.Drawing.Size(58, 23)
        Me.txtPivot.TabIndex = 17
        Me.txtPivot.Text = "0000"
        Me.txtPivot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label19.Location = New System.Drawing.Point(571, 57)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(39, 13)
        Me.Label19.TabIndex = 133
        Me.Label19.Text = "VOL :"
        '
        'txtVol
        '
        Me.txtVol.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtVol.Location = New System.Drawing.Point(612, 52)
        Me.txtVol.MaxLength = 100
        Me.txtVol.Name = "txtVol"
        Me.txtVol.Size = New System.Drawing.Size(58, 23)
        Me.txtVol.TabIndex = 16
        Me.txtVol.Text = "0000"
        Me.txtVol.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label18.Location = New System.Drawing.Point(575, 32)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(36, 13)
        Me.Label18.TabIndex = 131
        Me.Label18.Text = "GW :"
        '
        'txtGW
        '
        Me.txtGW.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtGW.Location = New System.Drawing.Point(612, 27)
        Me.txtGW.MaxLength = 100
        Me.txtGW.Name = "txtGW"
        Me.txtGW.Size = New System.Drawing.Size(58, 23)
        Me.txtGW.TabIndex = 15
        Me.txtGW.Text = "0000"
        Me.txtGW.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label17.Location = New System.Drawing.Point(673, 8)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 129
        Me.Label17.Text = "UNITS"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label16.Location = New System.Drawing.Point(571, 8)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(40, 13)
        Me.Label16.TabIndex = 128
        Me.Label16.Text = "QTY :"
        '
        'txtQty
        '
        Me.txtQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtQty.Location = New System.Drawing.Point(612, 2)
        Me.txtQty.MaxLength = 100
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(58, 23)
        Me.txtQty.TabIndex = 14
        Me.txtQty.Text = "0"
        Me.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label15.Location = New System.Drawing.Point(78, 81)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(90, 13)
        Me.Label15.TabIndex = 126
        Me.Label15.Text = "COMMODITY :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label14.Location = New System.Drawing.Point(71, 57)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(97, 13)
        Me.Label14.TabIndex = 122
        Me.Label14.Text = "PORT ORIGIN :"
        '
        'cbbCountryDest
        '
        Me.cbbCountryDest.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbCountryDest.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbCountryDest.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbCountryDest.FormattingEnabled = True
        Me.cbbCountryDest.Location = New System.Drawing.Point(167, 27)
        Me.cbbCountryDest.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbCountryDest.Name = "cbbCountryDest"
        Me.cbbCountryDest.Size = New System.Drawing.Size(153, 24)
        Me.cbbCountryDest.TabIndex = 8
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(388, 32)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(43, 13)
        Me.Label11.TabIndex = 119
        Me.Label11.Text = "CITY :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(5, 32)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(163, 13)
        Me.Label12.TabIndex = 118
        Me.Label12.Text = "DESTINATION COUNTRY :"
        '
        'cbbCountryOrigin
        '
        Me.cbbCountryOrigin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbCountryOrigin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbCountryOrigin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbCountryOrigin.FormattingEnabled = True
        Me.cbbCountryOrigin.Location = New System.Drawing.Point(167, 2)
        Me.cbbCountryOrigin.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbCountryOrigin.Name = "cbbCountryOrigin"
        Me.cbbCountryOrigin.Size = New System.Drawing.Size(153, 24)
        Me.cbbCountryOrigin.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(388, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 115
        Me.Label3.Text = "CITY :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(45, 7)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(123, 13)
        Me.Label10.TabIndex = 114
        Me.Label10.Text = "ORIGIN COUNTRY :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label22.Location = New System.Drawing.Point(28, 427)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(67, 13)
        Me.Label22.TabIndex = 139
        Me.Label22.Text = "REMARK :"
        '
        'txtRemark
        '
        Me.txtRemark.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtRemark.Location = New System.Drawing.Point(95, 424)
        Me.txtRemark.MaxLength = 100
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRemark.Size = New System.Drawing.Size(564, 62)
        Me.txtRemark.TabIndex = 20
        Me.txtRemark.Text = "REMARK"
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.Controls.Add(Me.lblQtStatus)
        Me.Panel3.Controls.Add(Me.txtStatusName)
        Me.Panel3.Controls.Add(Me.Label31)
        Me.Panel3.Controls.Add(Me.txtReferenceNo)
        Me.Panel3.Controls.Add(Me.Label30)
        Me.Panel3.Controls.Add(Me.Label23)
        Me.Panel3.Controls.Add(Me.Label27)
        Me.Panel3.Controls.Add(Me.txtSaleName)
        Me.Panel3.Controls.Add(Me.Label4)
        Me.Panel3.Controls.Add(Me.txtJobNo)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.txtQuotationNo)
        Me.Panel3.Controls.Add(Me.Label1)
        Me.Panel3.Controls.Add(Me.txtQuotationDate)
        Me.Panel3.Location = New System.Drawing.Point(731, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(288, 154)
        Me.Panel3.TabIndex = 116
        '
        'lblQtStatus
        '
        Me.lblQtStatus.AutoSize = True
        Me.lblQtStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblQtStatus.Location = New System.Drawing.Point(20, 127)
        Me.lblQtStatus.Name = "lblQtStatus"
        Me.lblQtStatus.Size = New System.Drawing.Size(16, 16)
        Me.lblQtStatus.TabIndex = 143
        Me.lblQtStatus.Text = "0"
        Me.lblQtStatus.Visible = False
        '
        'txtStatusName
        '
        Me.txtStatusName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStatusName.Enabled = False
        Me.txtStatusName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtStatusName.Location = New System.Drawing.Point(115, 126)
        Me.txtStatusName.MaxLength = 100
        Me.txtStatusName.Name = "txtStatusName"
        Me.txtStatusName.Size = New System.Drawing.Size(170, 23)
        Me.txtStatusName.TabIndex = 125
        Me.txtStatusName.Text = "Status"
        '
        'Label31
        '
        Me.Label31.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label31.Location = New System.Drawing.Point(64, 130)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(51, 13)
        Me.Label31.TabIndex = 124
        Me.Label31.Text = "Status :"
        '
        'txtReferenceNo
        '
        Me.txtReferenceNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtReferenceNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtReferenceNo.Location = New System.Drawing.Point(115, 76)
        Me.txtReferenceNo.MaxLength = 100
        Me.txtReferenceNo.Name = "txtReferenceNo"
        Me.txtReferenceNo.Size = New System.Drawing.Size(170, 23)
        Me.txtReferenceNo.TabIndex = 123
        Me.txtReferenceNo.Text = "REFERENCE NO"
        '
        'Label30
        '
        Me.Label30.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label30.Location = New System.Drawing.Point(20, 81)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(94, 13)
        Me.Label30.TabIndex = 122
        Me.Label30.Text = "Reference No :"
        '
        'Label23
        '
        Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label23.Location = New System.Drawing.Point(66, 7)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(48, 13)
        Me.Label23.TabIndex = 121
        Me.Label23.Text = "DATE :"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.BackColor = System.Drawing.Color.Transparent
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Red
        Me.Label27.Location = New System.Drawing.Point(42, 58)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(12, 13)
        Me.Label27.TabIndex = 100
        Me.Label27.Text = "*"
        '
        'txtSaleName
        '
        Me.txtSaleName.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSaleName.Enabled = False
        Me.txtSaleName.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSaleName.Location = New System.Drawing.Point(115, 101)
        Me.txtSaleName.MaxLength = 100
        Me.txtSaleName.Name = "txtSaleName"
        Me.txtSaleName.Size = New System.Drawing.Size(170, 23)
        Me.txtSaleName.TabIndex = 120
        Me.txtSaleName.Text = "FIRSTNAME S"
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(68, 106)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 119
        Me.Label4.Text = "SALE :"
        '
        'txtJobNo
        '
        Me.txtJobNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtJobNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtJobNo.Location = New System.Drawing.Point(115, 51)
        Me.txtJobNo.MaxLength = 100
        Me.txtJobNo.Name = "txtJobNo"
        Me.txtJobNo.Size = New System.Drawing.Size(170, 23)
        Me.txtJobNo.TabIndex = 5
        Me.txtJobNo.Text = "BIMSHXXXXXX"
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(56, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 117
        Me.Label2.Text = "JOB No :"
        '
        'txtQuotationNo
        '
        Me.txtQuotationNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtQuotationNo.Enabled = False
        Me.txtQuotationNo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtQuotationNo.Location = New System.Drawing.Point(115, 26)
        Me.txtQuotationNo.MaxLength = 100
        Me.txtQuotationNo.Name = "txtQuotationNo"
        Me.txtQuotationNo.Size = New System.Drawing.Size(170, 23)
        Me.txtQuotationNo.TabIndex = 116
        Me.txtQuotationNo.Text = "AUTO"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(4, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 13)
        Me.Label1.TabIndex = 114
        Me.Label1.Text = "QUOTATION NO :"
        '
        'txtQuotationDate
        '
        Me.txtQuotationDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtQuotationDate.Enabled = False
        Me.txtQuotationDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtQuotationDate.Location = New System.Drawing.Point(115, 2)
        Me.txtQuotationDate.MaxLength = 100
        Me.txtQuotationDate.Name = "txtQuotationDate"
        Me.txtQuotationDate.Size = New System.Drawing.Size(170, 23)
        Me.txtQuotationDate.TabIndex = 115
        Me.txtQuotationDate.Text = "dd/MM/yyyy"
        Me.txtQuotationDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnDeny
        '
        Me.btnDeny.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeny.BackColor = System.Drawing.Color.Maroon
        Me.btnDeny.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnDeny.ForeColor = System.Drawing.Color.White
        Me.btnDeny.Location = New System.Drawing.Point(807, 616)
        Me.btnDeny.Name = "btnDeny"
        Me.btnDeny.Size = New System.Drawing.Size(103, 32)
        Me.btnDeny.TabIndex = 124
        Me.btnDeny.Text = "Deny"
        Me.btnDeny.UseVisualStyleBackColor = False
        Me.btnDeny.Visible = False
        '
        'btnApprove
        '
        Me.btnApprove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApprove.BackColor = System.Drawing.Color.Navy
        Me.btnApprove.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnApprove.ForeColor = System.Drawing.Color.White
        Me.btnApprove.Location = New System.Drawing.Point(696, 616)
        Me.btnApprove.Name = "btnApprove"
        Me.btnApprove.Size = New System.Drawing.Size(103, 32)
        Me.btnApprove.TabIndex = 123
        Me.btnApprove.Text = "Approve"
        Me.btnApprove.UseVisualStyleBackColor = False
        Me.btnApprove.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.Green
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.Location = New System.Drawing.Point(917, 616)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(103, 32)
        Me.btnSave.TabIndex = 122
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackColor = System.Drawing.Color.Salmon
        Me.btnPrint.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.btnPrint.ForeColor = System.Drawing.Color.White
        Me.btnPrint.Location = New System.Drawing.Point(22, 616)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(84, 32)
        Me.btnPrint.TabIndex = 144
        Me.btnPrint.Text = "Print"
        Me.btnPrint.UseVisualStyleBackColor = False
        '
        'UcByCaseQuotationTruckDescription1
        '
        Me.UcByCaseQuotationTruckDescription1.BackColor = System.Drawing.Color.Silver
        Me.UcByCaseQuotationTruckDescription1.Location = New System.Drawing.Point(5, 317)
        Me.UcByCaseQuotationTruckDescription1.Name = "UcByCaseQuotationTruckDescription1"
        Me.UcByCaseQuotationTruckDescription1.Size = New System.Drawing.Size(1014, 104)
        Me.UcByCaseQuotationTruckDescription1.TabIndex = 19
        '
        'UcByCaseQuotationTermCondition1
        '
        Me.UcByCaseQuotationTermCondition1.BackColor = System.Drawing.Color.Silver
        Me.UcByCaseQuotationTermCondition1.Location = New System.Drawing.Point(5, 517)
        Me.UcByCaseQuotationTermCondition1.Name = "UcByCaseQuotationTermCondition1"
        Me.UcByCaseQuotationTermCondition1.Size = New System.Drawing.Size(1014, 96)
        Me.UcByCaseQuotationTermCondition1.TabIndex = 22
        '
        'UcByCaseQuotationDescription1
        '
        Me.UcByCaseQuotationDescription1.BackColor = System.Drawing.Color.Silver
        Me.UcByCaseQuotationDescription1.Location = New System.Drawing.Point(5, 209)
        Me.UcByCaseQuotationDescription1.Name = "UcByCaseQuotationDescription1"
        Me.UcByCaseQuotationDescription1.Size = New System.Drawing.Size(1014, 107)
        Me.UcByCaseQuotationDescription1.TabIndex = 18
        '
        'UcByCaseQuotationEstimateTotal1
        '
        Me.UcByCaseQuotationEstimateTotal1.Location = New System.Drawing.Point(676, 424)
        Me.UcByCaseQuotationEstimateTotal1.Name = "UcByCaseQuotationEstimateTotal1"
        Me.UcByCaseQuotationEstimateTotal1.Size = New System.Drawing.Size(328, 90)
        Me.UcByCaseQuotationEstimateTotal1.TabIndex = 21
        '
        'btnOverwrite
        '
        Me.btnOverwrite.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOverwrite.BackColor = System.Drawing.Color.Navy
        Me.btnOverwrite.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnOverwrite.ForeColor = System.Drawing.Color.White
        Me.btnOverwrite.Location = New System.Drawing.Point(112, 616)
        Me.btnOverwrite.Name = "btnOverwrite"
        Me.btnOverwrite.Size = New System.Drawing.Size(103, 32)
        Me.btnOverwrite.TabIndex = 147
        Me.btnOverwrite.Text = "Overwrite"
        Me.btnOverwrite.UseVisualStyleBackColor = False
        Me.btnOverwrite.Visible = False
        '
        'frmQuotationCaseByCase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.ClientSize = New System.Drawing.Size(1024, 650)
        Me.Controls.Add(Me.btnOverwrite)
        Me.Controls.Add(Me.UcByCaseQuotationTruckDescription1)
        Me.Controls.Add(Me.UcByCaseQuotationTermCondition1)
        Me.Controls.Add(Me.UcByCaseQuotationDescription1)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.UcByCaseQuotationEstimateTotal1)
        Me.Controls.Add(Me.btnDeny)
        Me.Controls.Add(Me.btnApprove)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.txtRemark)
        Me.Controls.Add(Me.Label22)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmQuotationCaseByCase"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Case by Case Quotation"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rp_cus_all As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbbTerm As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cbbScope As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbbMode As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCustomerAcc As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCustomerAddress As System.Windows.Forms.TextBox
    Friend WithEvents cbbCustomer As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtRemark As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtDimension As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtPivot As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtVol As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtGW As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cbbCountryDest As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cbbCountryOrigin As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtSaleName As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtJobNo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtQuotationNo As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtQuotationDate As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents btnDeny As System.Windows.Forms.Button
    Friend WithEvents btnApprove As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lblID As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents UcByCaseQuotationDescription1 As BAFCO_TIS.UCByCaseQuotationDescription
    Friend WithEvents UcByCaseQuotationTermCondition1 As BAFCO_TIS.UCByCaseQuotationTermCondition
    Friend WithEvents UcByCaseQuotationEstimateTotal1 As BAFCO_TIS.UCByCaseQuotationEstimateTotal
    Friend WithEvents txtCommodity As System.Windows.Forms.TextBox
    Friend WithEvents txtPortNameDest As System.Windows.Forms.TextBox
    Friend WithEvents txtPortNameOrigin As System.Windows.Forms.TextBox
    Friend WithEvents txtCityNameDest As System.Windows.Forms.TextBox
    Friend WithEvents txtCityNameOrigin As System.Windows.Forms.TextBox
    Friend WithEvents lblAbbQuotation As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents btnPrint As System.Windows.Forms.Button
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents UcByCaseQuotationTruckDescription1 As BAFCO_TIS.UCByCaseQuotationTruckDescription
    Friend WithEvents lblQtStatus As System.Windows.Forms.Label
    Friend WithEvents cbbCusBillTo As System.Windows.Forms.ComboBox
    Friend WithEvents txtStatusName As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtReferenceNo As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents btnOverwrite As System.Windows.Forms.Button
End Class

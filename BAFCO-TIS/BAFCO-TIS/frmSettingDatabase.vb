﻿Imports BAFCO_TIS.Org.Mentalis.Files

Public Class frmSettingDatabase

    Private Sub frmDatabaseConfig_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Me.DialogResult <> Windows.Forms.DialogResult.Yes Then
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If
    End Sub

    Private Sub frmSettingDatabase_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        txtServer.Focus()
        Dim ini As New IniReader(INIFile)
        ini.Section = "Setting"
        txtServer.Text = CStr(ini.ReadString("Server"))
        txtDatabase.Text = CStr(ini.ReadString("Database"))
        txtUsername.Text = CStr(ini.ReadString("Username"))
        txtPassword.Text = CStr(ini.ReadString("Password"))
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If CkeckConnection(getConnectionStrTest) = False Then
            MessageBox.Show("The Database cannot be connected." & vbCrLf & "Please check its connection.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtServer.Focus()
            Exit Sub
        End If

        Dim ini As New IniReader(INIFile)
        ini.Section = "Setting"
        ini.Write("Server", txtServer.Text.Trim)
        ini.Write("Database", txtDatabase.Text.Trim)
        ini.Write("Username", txtUsername.Text.Trim)
        ini.Write("Password", txtPassword.Text.Trim)

        MessageBox.Show("Your input data is successfully been saved.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.Yes
        Application.Restart()
    End Sub

    Function getConnectionStrTest() As String
        Return "Data Source=" & txtServer.Text & ";Initial Catalog=" & txtDatabase.Text & ";User ID=" & txtUsername.Text & ";Password=" & txtPassword.Text & ";Connect Timeout=1;"
    End Function

    Private Sub txtServer_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtServer.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtDatabase.Focus()
        End If
    End Sub

    Private Sub txtDatabasr_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDatabase.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtUsername.Focus()
        End If
    End Sub

    Private Sub txtUsername_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUsername.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtPassword.Focus()
        End If
    End Sub

    Private Sub txtPassword_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnSave.PerformClick()
        End If
    End Sub
End Class
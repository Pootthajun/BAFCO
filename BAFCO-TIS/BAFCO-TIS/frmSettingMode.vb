﻿Imports System.Data
Imports System.Data.SqlClient
Public Class frmSettingMode

    Private Sub frmMode_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        ShowData(txtSearch.Text)
    End Sub

    Function GetData(txtSearch As String) As DataTable
        Dim da As New SqlDataAdapter
        Dim sql As String = ""
        sql = "select id,mode_name, default_qt,active_status from MS_MODE where 1=1 "
        If txtSearch <> "" Then
            sql += " and mode_name like '%" & txtSearch.Replace("'", "''") & "%'"
        End If
        If rdiSearchActive.Checked = True Then
            sql += " and active_status='Y'"
        End If
        If rdiSearchInactive.Checked = True Then
            sql += " and active_status='N'"
        End If

        sql += " order by mode_name"
        da = New SqlDataAdapter(sql, ConnStr)
        Dim dt As New DataTable
        da.Fill(dt)
        Return dt
    End Function

    Private Sub ShowData(txtSearch As String)
        Dim dt As DataTable = GetData(txtSearch)
        grdMain.DataSource = DT
        lblRec.Text = "Total   " & dt.Rows.Count & "   Record"

        grdMain.Columns.Clear()
        Dim commodity_id As New System.Windows.Forms.DataGridViewTextBoxColumn
        commodity_id.Name = "id"
        commodity_id.DataPropertyName = "id"
        commodity_id.Visible = False
        grdMain.Columns.Add(commodity_id)
        grdMain.Columns("id").Visible = False

        Dim mode_name As New System.Windows.Forms.DataGridViewTextBoxColumn
        mode_name.DataPropertyName = "mode_name"
        mode_name.HeaderText = "Mode Name"
        mode_name.SortMode = DataGridViewColumnSortMode.NotSortable
        mode_name.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(mode_name)

        Dim active_status As New System.Windows.Forms.DataGridViewCheckBoxColumn
        active_status.DataPropertyName = "active_status"
        active_status.TrueValue = "Y"
        active_status.FalseValue = "N"
        active_status.HeaderText = "Active"
        active_status.Width = 100
        grdMain.Columns.Add(active_status)

        Dim default_qt As New System.Windows.Forms.DataGridViewCheckBoxColumn
        default_qt.Name = "default_qt"
        default_qt.DataPropertyName = "default_qt"
        default_qt.TrueValue = "Y"
        default_qt.FalseValue = "N"
        default_qt.HeaderText = "Default"
        default_qt.Width = 100
        grdMain.Columns.Add(default_qt)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dt As DataTable = grdMain.DataSource
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(dt.Rows(i).Item("mode_name")) OrElse dt.Rows(i).Item("mode_name") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            Dim tmpdr As DataRow() = dt.Select("mode_name = '" & dt.Rows(i).Item("mode_name").ToString.Replace("'", "''") & "'")
            If tmpdr.Length > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        '== Save ==

        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction

        Dim ret As String = "false"
        Try
            Dim tmpedit As DataRow() = dt.Select("id <> 0")
            '== ลบ ==
            Dim _id As String = ""
            For i As Int32 = 0 To tmpedit.Length - 1
                Application.DoEvents()
                _id &= tmpedit(i)("id").ToString
                If i < tmpedit.Length - 1 Then
                    _id &= ","
                End If
            Next
            Dim sql As String = ""

            If _id <> "" Then
                sql = "delete from MS_MODE where id not in (" & _id & ")"
                ret = Execute_Command(sql, trans, Nothing)
                If ret <> "true" Then
                    trans.Rollback()
                    MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If
            

            '=== แก้ไข
            For i As Int32 = 0 To tmpedit.Length - 1
                Application.DoEvents()
                Dim _commodity_id As String = tmpedit(i)("id").ToString
                Dim _mode_name As String = tmpedit(i)("mode_name").ToString.Replace("'", "''")
                Dim _active_status As String = tmpedit(i)("active_status").ToString
                Dim _default_qt As String = tmpedit(i)("default_qt").ToString

                sql = "Update MS_MODE set mode_name = @_mode_name, default_qt=@_DEFAULT_QT,active_status = @_active_status,updated_date = getdate(),"
                sql += " updated_by= @_updated_by where id = @_id"
                Dim p_e(5) As SqlParameter
                p_e(0) = SetText("@_mode_name", _mode_name)
                p_e(1) = SetText("@_DEFAULT_QT", _default_qt)
                p_e(2) = SetText("@_active_status", _active_status)
                p_e(3) = SetText("@_updated_by", myUser.us_code)
                p_e(4) = SetText("@_ID", _commodity_id)
                ret = Execute_Command(sql, trans, p_e)
                If ret <> "true" Then
                    trans.Rollback()
                    MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            Next

            '== เพิ่ม
            Dim tmpadd As DataRow() = dt.Select("id = 0")
            For i As Int32 = 0 To tmpadd.Length - 1
                Application.DoEvents()
                Dim _mode_name As String = tmpadd(i)("mode_name").ToString.Replace("'", "''")
                Dim _active_status As String = tmpadd(i)("active_status").ToString
                Dim _default_qt As String = tmpadd(i)("default_qt").ToString

                sql = "Insert Into MS_MODE(mode_name,default_qt,active_status,created_date,created_by)"
                sql &= " Values(@_mode_name,@_DEFAULT_QT,@_active_status,getdate(),@_create_by)"
                Dim p_i(4) As SqlParameter
                p_i(0) = SetText("@_mode_name", _mode_name)
                p_i(1) = SetText("@_DEFAULT_QT", _default_qt)
                p_i(2) = SetText("@_active_status", _active_status)
                p_i(3) = SetText("@_create_by", myUser.us_code)
                ret = Execute_Command(sql, trans, p_i)
                If ret <> "true" Then
                    trans.Rollback()
                    MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            Next

            trans.Commit()
            conn.Close()
            '== End Save

            txtSearch.Text = ""
            ShowData(txtSearch.Text)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            trans.Rollback()
            If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
                ShowData(txtSearch.Text)
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim dt As DataTable = grdMain.DataSource
        Dim dr As DataRow = dt.NewRow
        dr("id") = "0"
        dr("mode_name") = ""
        dr("active_status") = "Y"
        dr("default_qt") = "N"
        dt.Rows.Add(dr)
        grdMain.DataSource = dt
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        ShowData(txtSearch.Text)
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        ShowData(txtSearch.Text)
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        ShowData(txtSearch.Text)
    End Sub

    Private Sub grdMain_CellContentClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMain.CellContentClick
        If grdMain.Columns(e.ColumnIndex).Name = "default_qt" Then
            For Each grv As DataGridViewRow In grdMain.Rows
                grv.Cells(e.ColumnIndex).Value = "N"
            Next

            grdMain.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = "Y"
        End If
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchService
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GridCus = New System.Windows.Forms.DataGridView()
        Me.GridSup = New System.Windows.Forms.DataGridView()
        Me.S_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SER_TYPE_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SER_TYPE_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SUP_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SUP_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_COST = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostUnit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.flowSupplier = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAddSupplier = New System.Windows.Forms.Button()
        Me.btnPrintSupplier = New System.Windows.Forms.Button()
        Me.btnS_Add = New System.Windows.Forms.Button()
        Me.tvService = New System.Windows.Forms.TreeView()
        Me.flowCustomer = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAddCustomer = New System.Windows.Forms.Button()
        Me.btnPrintAll = New System.Windows.Forms.Button()
        Me.btnPrintCustomer = New System.Windows.Forms.Button()
        Me.btnC_Add = New System.Windows.Forms.Button()
        Me.txtSearchService = New System.Windows.Forms.TextBox()
        Me.C_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_SER_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_SER_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_SER_TYPE_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_SER_TYPE_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_CUS_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_CUS_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_COST = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValueUnit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colQtStatus = New System.Windows.Forms.DataGridViewImageColumn()
        Me.colQtStatusValue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.GridCus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridSup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GridCus
        '
        Me.GridCus.AllowUserToAddRows = False
        Me.GridCus.AllowUserToDeleteRows = False
        Me.GridCus.AllowUserToResizeRows = False
        Me.GridCus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridCus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.GridCus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridCus.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.C_ID, Me.C_SER_ID, Me.C_SER_NAME, Me.C_SER_TYPE_ID, Me.C_SER_TYPE_NAME, Me.C_CUS_ID, Me.C_CUS_NAME, Me.C_COST, Me.ValueUnit, Me.colQtStatus, Me.colQtStatusValue})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridCus.DefaultCellStyle = DataGridViewCellStyle2
        Me.GridCus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.GridCus.Location = New System.Drawing.Point(4, 36)
        Me.GridCus.Margin = New System.Windows.Forms.Padding(4)
        Me.GridCus.MultiSelect = False
        Me.GridCus.Name = "GridCus"
        Me.GridCus.ReadOnly = True
        Me.GridCus.RowHeadersVisible = False
        Me.GridCus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridCus.Size = New System.Drawing.Size(506, 515)
        Me.GridCus.TabIndex = 55
        '
        'GridSup
        '
        Me.GridSup.AllowUserToAddRows = False
        Me.GridSup.AllowUserToDeleteRows = False
        Me.GridSup.AllowUserToResizeRows = False
        Me.GridSup.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridSup.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.GridSup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridSup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.S_ID, Me.S_SER_ID, Me.S_SER_NAME, Me.S_SER_TYPE_ID, Me.S_SER_TYPE_NAME, Me.S_SUP_ID, Me.S_SUP_NAME, Me.S_COST, Me.CostUnit})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridSup.DefaultCellStyle = DataGridViewCellStyle4
        Me.GridSup.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.GridSup.Location = New System.Drawing.Point(4, 36)
        Me.GridSup.Margin = New System.Windows.Forms.Padding(4)
        Me.GridSup.MultiSelect = False
        Me.GridSup.Name = "GridSup"
        Me.GridSup.ReadOnly = True
        Me.GridSup.RowHeadersVisible = False
        Me.GridSup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridSup.Size = New System.Drawing.Size(480, 515)
        Me.GridSup.TabIndex = 44
        '
        'S_ID
        '
        Me.S_ID.DataPropertyName = "S_ID"
        Me.S_ID.HeaderText = "S_ID"
        Me.S_ID.Name = "S_ID"
        Me.S_ID.ReadOnly = True
        Me.S_ID.Visible = False
        '
        'S_SER_ID
        '
        Me.S_SER_ID.DataPropertyName = "S_SER_ID"
        Me.S_SER_ID.HeaderText = "S_SER_ID"
        Me.S_SER_ID.Name = "S_SER_ID"
        Me.S_SER_ID.ReadOnly = True
        Me.S_SER_ID.Visible = False
        '
        'S_SER_NAME
        '
        Me.S_SER_NAME.DataPropertyName = "S_SER_NAME"
        Me.S_SER_NAME.HeaderText = "Type Of Service"
        Me.S_SER_NAME.Name = "S_SER_NAME"
        Me.S_SER_NAME.ReadOnly = True
        '
        'S_SER_TYPE_ID
        '
        Me.S_SER_TYPE_ID.DataPropertyName = "S_SER_TYPE_ID"
        Me.S_SER_TYPE_ID.HeaderText = "S_SER_TYPE_ID"
        Me.S_SER_TYPE_ID.Name = "S_SER_TYPE_ID"
        Me.S_SER_TYPE_ID.ReadOnly = True
        Me.S_SER_TYPE_ID.Visible = False
        '
        'S_SER_TYPE_NAME
        '
        Me.S_SER_TYPE_NAME.DataPropertyName = "S_SER_TYPE_NAME"
        Me.S_SER_TYPE_NAME.HeaderText = "S_SER_TYPE_NAME"
        Me.S_SER_TYPE_NAME.Name = "S_SER_TYPE_NAME"
        Me.S_SER_TYPE_NAME.ReadOnly = True
        Me.S_SER_TYPE_NAME.Visible = False
        '
        'S_SUP_ID
        '
        Me.S_SUP_ID.DataPropertyName = "S_SUP_ID"
        Me.S_SUP_ID.HeaderText = "S_SUP_ID"
        Me.S_SUP_ID.Name = "S_SUP_ID"
        Me.S_SUP_ID.ReadOnly = True
        Me.S_SUP_ID.Visible = False
        '
        'S_SUP_NAME
        '
        Me.S_SUP_NAME.DataPropertyName = "S_SUP_NAME"
        Me.S_SUP_NAME.HeaderText = "Supplier"
        Me.S_SUP_NAME.Name = "S_SUP_NAME"
        Me.S_SUP_NAME.ReadOnly = True
        '
        'S_COST
        '
        Me.S_COST.DataPropertyName = "S_COST"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "C2"
        DataGridViewCellStyle3.NullValue = "0"
        Me.S_COST.DefaultCellStyle = DataGridViewCellStyle3
        Me.S_COST.HeaderText = "Cost"
        Me.S_COST.Name = "S_COST"
        Me.S_COST.ReadOnly = True
        '
        'CostUnit
        '
        Me.CostUnit.DataPropertyName = "CostUnit"
        Me.CostUnit.HeaderText = "Unit"
        Me.CostUnit.Name = "CostUnit"
        Me.CostUnit.ReadOnly = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(2, 8)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 17)
        Me.Label4.TabIndex = 51
        Me.Label4.Text = "Supplier : "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 10)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 17)
        Me.Label3.TabIndex = 49
        Me.Label3.Text = "Customer : "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(11, 14)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 17)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Service : "
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 47)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.flowSupplier)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnAddSupplier)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnPrintSupplier)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnS_Add)
        Me.SplitContainer1.Panel1.Controls.Add(Me.tvService)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GridSup)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.flowCustomer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnAddCustomer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnPrintAll)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnPrintCustomer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnC_Add)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GridCus)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Size = New System.Drawing.Size(999, 587)
        Me.SplitContainer1.SplitterDistance = 484
        Me.SplitContainer1.TabIndex = 56
        '
        'flowSupplier
        '
        Me.flowSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flowSupplier.AutoScroll = True
        Me.flowSupplier.BackColor = System.Drawing.Color.Transparent
        Me.flowSupplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flowSupplier.Location = New System.Drawing.Point(78, 2)
        Me.flowSupplier.Name = "flowSupplier"
        Me.flowSupplier.Size = New System.Drawing.Size(369, 31)
        Me.flowSupplier.TabIndex = 68
        '
        'btnAddSupplier
        '
        Me.btnAddSupplier.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddSupplier.BackColor = System.Drawing.Color.Transparent
        Me.btnAddSupplier.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources._1485859723811
        Me.btnAddSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAddSupplier.ForeColor = System.Drawing.Color.White
        Me.btnAddSupplier.Location = New System.Drawing.Point(453, 3)
        Me.btnAddSupplier.Name = "btnAddSupplier"
        Me.btnAddSupplier.Size = New System.Drawing.Size(29, 29)
        Me.btnAddSupplier.TabIndex = 67
        Me.btnAddSupplier.UseVisualStyleBackColor = False
        '
        'btnPrintSupplier
        '
        Me.btnPrintSupplier.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintSupplier.BackColor = System.Drawing.Color.Salmon
        Me.btnPrintSupplier.ForeColor = System.Drawing.Color.White
        Me.btnPrintSupplier.Location = New System.Drawing.Point(155, 556)
        Me.btnPrintSupplier.Name = "btnPrintSupplier"
        Me.btnPrintSupplier.Size = New System.Drawing.Size(150, 32)
        Me.btnPrintSupplier.TabIndex = 59
        Me.btnPrintSupplier.Text = "Print Supplier"
        Me.btnPrintSupplier.UseVisualStyleBackColor = False
        '
        'btnS_Add
        '
        Me.btnS_Add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnS_Add.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnS_Add.ForeColor = System.Drawing.Color.White
        Me.btnS_Add.Location = New System.Drawing.Point(4, 556)
        Me.btnS_Add.Name = "btnS_Add"
        Me.btnS_Add.Size = New System.Drawing.Size(150, 32)
        Me.btnS_Add.TabIndex = 53
        Me.btnS_Add.Text = "Add"
        Me.btnS_Add.UseVisualStyleBackColor = False
        '
        'tvService
        '
        Me.tvService.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.tvService.CheckBoxes = True
        Me.tvService.Location = New System.Drawing.Point(9, -27)
        Me.tvService.Name = "tvService"
        Me.tvService.Size = New System.Drawing.Size(12, 21)
        Me.tvService.TabIndex = 57
        Me.tvService.Visible = False
        '
        'flowCustomer
        '
        Me.flowCustomer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flowCustomer.AutoScroll = True
        Me.flowCustomer.BackColor = System.Drawing.Color.Transparent
        Me.flowCustomer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flowCustomer.Location = New System.Drawing.Point(97, 2)
        Me.flowCustomer.Name = "flowCustomer"
        Me.flowCustomer.Size = New System.Drawing.Size(361, 31)
        Me.flowCustomer.TabIndex = 66
        '
        'btnAddCustomer
        '
        Me.btnAddCustomer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddCustomer.BackColor = System.Drawing.Color.Transparent
        Me.btnAddCustomer.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources._1485859723811
        Me.btnAddCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAddCustomer.ForeColor = System.Drawing.Color.White
        Me.btnAddCustomer.Location = New System.Drawing.Point(466, 3)
        Me.btnAddCustomer.Name = "btnAddCustomer"
        Me.btnAddCustomer.Size = New System.Drawing.Size(29, 29)
        Me.btnAddCustomer.TabIndex = 65
        Me.btnAddCustomer.UseVisualStyleBackColor = False
        '
        'btnPrintAll
        '
        Me.btnPrintAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintAll.BackColor = System.Drawing.Color.SandyBrown
        Me.btnPrintAll.ForeColor = System.Drawing.Color.White
        Me.btnPrintAll.Location = New System.Drawing.Point(306, 555)
        Me.btnPrintAll.Name = "btnPrintAll"
        Me.btnPrintAll.Size = New System.Drawing.Size(150, 32)
        Me.btnPrintAll.TabIndex = 61
        Me.btnPrintAll.Text = "Print All"
        Me.btnPrintAll.UseVisualStyleBackColor = False
        '
        'btnPrintCustomer
        '
        Me.btnPrintCustomer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintCustomer.BackColor = System.Drawing.Color.Salmon
        Me.btnPrintCustomer.ForeColor = System.Drawing.Color.White
        Me.btnPrintCustomer.Location = New System.Drawing.Point(154, 555)
        Me.btnPrintCustomer.Name = "btnPrintCustomer"
        Me.btnPrintCustomer.Size = New System.Drawing.Size(150, 32)
        Me.btnPrintCustomer.TabIndex = 60
        Me.btnPrintCustomer.Text = "Print Customer"
        Me.btnPrintCustomer.UseVisualStyleBackColor = False
        '
        'btnC_Add
        '
        Me.btnC_Add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnC_Add.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnC_Add.ForeColor = System.Drawing.Color.White
        Me.btnC_Add.Location = New System.Drawing.Point(3, 556)
        Me.btnC_Add.Name = "btnC_Add"
        Me.btnC_Add.Size = New System.Drawing.Size(150, 32)
        Me.btnC_Add.TabIndex = 54
        Me.btnC_Add.Text = "Add"
        Me.btnC_Add.UseVisualStyleBackColor = False
        '
        'txtSearchService
        '
        Me.txtSearchService.Location = New System.Drawing.Point(81, 11)
        Me.txtSearchService.Name = "txtSearchService"
        Me.txtSearchService.Size = New System.Drawing.Size(406, 23)
        Me.txtSearchService.TabIndex = 0
        '
        'C_ID
        '
        Me.C_ID.DataPropertyName = "C_ID"
        Me.C_ID.HeaderText = "C_ID"
        Me.C_ID.Name = "C_ID"
        Me.C_ID.ReadOnly = True
        Me.C_ID.Visible = False
        '
        'C_SER_ID
        '
        Me.C_SER_ID.DataPropertyName = "C_SER_ID"
        Me.C_SER_ID.HeaderText = "C_SER_ID"
        Me.C_SER_ID.Name = "C_SER_ID"
        Me.C_SER_ID.ReadOnly = True
        Me.C_SER_ID.Visible = False
        '
        'C_SER_NAME
        '
        Me.C_SER_NAME.DataPropertyName = "C_SER_NAME"
        Me.C_SER_NAME.FillWeight = 105.9645!
        Me.C_SER_NAME.HeaderText = "Type Of Service"
        Me.C_SER_NAME.Name = "C_SER_NAME"
        Me.C_SER_NAME.ReadOnly = True
        '
        'C_SER_TYPE_ID
        '
        Me.C_SER_TYPE_ID.DataPropertyName = "C_SER_TYPE_ID"
        Me.C_SER_TYPE_ID.HeaderText = "C_SER_TYPE_ID"
        Me.C_SER_TYPE_ID.Name = "C_SER_TYPE_ID"
        Me.C_SER_TYPE_ID.ReadOnly = True
        Me.C_SER_TYPE_ID.Visible = False
        '
        'C_SER_TYPE_NAME
        '
        Me.C_SER_TYPE_NAME.DataPropertyName = "C_SER_TYPE_NAME"
        Me.C_SER_TYPE_NAME.HeaderText = "C_SER_TYPE_NAME"
        Me.C_SER_TYPE_NAME.Name = "C_SER_TYPE_NAME"
        Me.C_SER_TYPE_NAME.ReadOnly = True
        Me.C_SER_TYPE_NAME.Visible = False
        '
        'C_CUS_ID
        '
        Me.C_CUS_ID.DataPropertyName = "C_CUS_ID"
        Me.C_CUS_ID.HeaderText = "C_CUS_ID"
        Me.C_CUS_ID.Name = "C_CUS_ID"
        Me.C_CUS_ID.ReadOnly = True
        Me.C_CUS_ID.Visible = False
        '
        'C_CUS_NAME
        '
        Me.C_CUS_NAME.DataPropertyName = "C_CUS_NAME"
        Me.C_CUS_NAME.FillWeight = 105.9645!
        Me.C_CUS_NAME.HeaderText = "Customer"
        Me.C_CUS_NAME.Name = "C_CUS_NAME"
        Me.C_CUS_NAME.ReadOnly = True
        '
        'C_COST
        '
        Me.C_COST.DataPropertyName = "C_COST"
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "C2"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.C_COST.DefaultCellStyle = DataGridViewCellStyle1
        Me.C_COST.FillWeight = 105.9645!
        Me.C_COST.HeaderText = "Price"
        Me.C_COST.Name = "C_COST"
        Me.C_COST.ReadOnly = True
        '
        'ValueUnit
        '
        Me.ValueUnit.DataPropertyName = "ValueUnit"
        Me.ValueUnit.FillWeight = 105.9645!
        Me.ValueUnit.HeaderText = "Unit"
        Me.ValueUnit.Name = "ValueUnit"
        Me.ValueUnit.ReadOnly = True
        '
        'colQtStatus
        '
        Me.colQtStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colQtStatus.FillWeight = 30.0!
        Me.colQtStatus.HeaderText = ""
        Me.colQtStatus.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom
        Me.colQtStatus.Name = "colQtStatus"
        Me.colQtStatus.ReadOnly = True
        Me.colQtStatus.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colQtStatus.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.colQtStatus.Width = 30
        '
        'colQtStatusValue
        '
        Me.colQtStatusValue.DataPropertyName = "qt_status"
        Me.colQtStatusValue.HeaderText = ""
        Me.colQtStatusValue.Name = "colQtStatusValue"
        Me.colQtStatusValue.ReadOnly = True
        Me.colQtStatusValue.Visible = False
        '
        'frmSearchService
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.ClientSize = New System.Drawing.Size(1006, 644)
        Me.ControlBox = False
        Me.Controls.Add(Me.txtSearchService)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Label5)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSearchService"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.GridCus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridSup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GridCus As System.Windows.Forms.DataGridView
    Friend WithEvents GridSup As System.Windows.Forms.DataGridView
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnS_Add As System.Windows.Forms.Button
    Friend WithEvents btnC_Add As System.Windows.Forms.Button
    Friend WithEvents tvService As System.Windows.Forms.TreeView
    Friend WithEvents btnPrintSupplier As System.Windows.Forms.Button
    Friend WithEvents btnPrintAll As System.Windows.Forms.Button
    Friend WithEvents btnPrintCustomer As System.Windows.Forms.Button
    Friend WithEvents txtSearchService As System.Windows.Forms.TextBox
    Friend WithEvents S_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SER_TYPE_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SER_TYPE_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SUP_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SUP_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_COST As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAddCustomer As System.Windows.Forms.Button
    Friend WithEvents flowCustomer As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents flowSupplier As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnAddSupplier As System.Windows.Forms.Button
    Friend WithEvents C_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_SER_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_SER_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_SER_TYPE_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_SER_TYPE_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_CUS_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_CUS_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_COST As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValueUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colQtStatus As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colQtStatusValue As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

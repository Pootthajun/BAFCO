﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFilterUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFilterUser))
        Me.Grid = New System.Windows.Forms.DataGridView()
        Me.US_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.US_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.US_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACTIVE_STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbbSearch = New System.Windows.Forms.ComboBox()
        Me.txtSearch = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Grid
        '
        Me.Grid.AllowUserToAddRows = False
        Me.Grid.AllowUserToDeleteRows = False
        Me.Grid.AllowUserToOrderColumns = True
        Me.Grid.AllowUserToResizeColumns = False
        Me.Grid.AllowUserToResizeRows = False
        Me.Grid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Grid.ColumnHeadersHeight = 30
        Me.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.US_CODE, Me.US_NAME, Me.US_ID, Me.ACTIVE_STATUS})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid.DefaultCellStyle = DataGridViewCellStyle3
        Me.Grid.Location = New System.Drawing.Point(15, 47)
        Me.Grid.MultiSelect = False
        Me.Grid.Name = "Grid"
        Me.Grid.ReadOnly = True
        Me.Grid.RowHeadersVisible = False
        Me.Grid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.Grid.Size = New System.Drawing.Size(761, 470)
        Me.Grid.TabIndex = 49
        '
        'US_CODE
        '
        Me.US_CODE.DataPropertyName = "US_CODE"
        Me.US_CODE.FillWeight = 50.65964!
        Me.US_CODE.HeaderText = "User Code / Login"
        Me.US_CODE.Name = "US_CODE"
        Me.US_CODE.ReadOnly = True
        Me.US_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.US_CODE.Width = 200
        '
        'US_NAME
        '
        Me.US_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.US_NAME.DataPropertyName = "US_NAME"
        Me.US_NAME.FillWeight = 27.89655!
        Me.US_NAME.HeaderText = "Name"
        Me.US_NAME.Name = "US_NAME"
        Me.US_NAME.ReadOnly = True
        Me.US_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'US_ID
        '
        Me.US_ID.DataPropertyName = "US_ID"
        Me.US_ID.HeaderText = "id"
        Me.US_ID.Name = "US_ID"
        Me.US_ID.ReadOnly = True
        Me.US_ID.Visible = False
        Me.US_ID.Width = 44
        '
        'ACTIVE_STATUS
        '
        Me.ACTIVE_STATUS.DataPropertyName = "ACTIVE_STATUS"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ACTIVE_STATUS.DefaultCellStyle = DataGridViewCellStyle2
        Me.ACTIVE_STATUS.FillWeight = 221.4439!
        Me.ACTIVE_STATUS.HeaderText = "Status"
        Me.ACTIVE_STATUS.Name = "ACTIVE_STATUS"
        Me.ACTIVE_STATUS.ReadOnly = True
        Me.ACTIVE_STATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.ACTIVE_STATUS.Width = 150
        '
        'cbbSearch
        '
        Me.cbbSearch.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbbSearch.FormattingEnabled = True
        Me.cbbSearch.Location = New System.Drawing.Point(398, 14)
        Me.cbbSearch.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbSearch.Name = "cbbSearch"
        Me.cbbSearch.Size = New System.Drawing.Size(140, 24)
        Me.cbbSearch.TabIndex = 47
        '
        'txtSearch
        '
        Me.txtSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtSearch.Location = New System.Drawing.Point(78, 15)
        Me.txtSearch.MaxLength = 100
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(313, 23)
        Me.txtSearch.TabIndex = 46
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 17)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Search :"
        '
        'frmFilterUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(788, 529)
        Me.Controls.Add(Me.Grid)
        Me.Controls.Add(Me.cbbSearch)
        Me.Controls.Add(Me.txtSearch)
        Me.Controls.Add(Me.Label3)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmFilterUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Filter"
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Grid As System.Windows.Forms.DataGridView
    Friend WithEvents cbbSearch As System.Windows.Forms.ComboBox
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents US_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents US_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents US_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACTIVE_STATUS As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Imports System.Data
Imports System.Data.SqlClient
Public Class popupSelectSupplier
    Public ReadOnly Property GetData() As DataTable
        Get
            Dim dt As New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("name")

            Dim dr As DataRow
            For Each row As DataGridViewRow In grdItems.Rows
                If row.Cells(0).Value = True Then
                    dr = dt.NewRow
                    dr("id") = row.Cells(1).Value
                    dr("name") = row.Cells(2).Value
                    dt.Rows.Add(dr)
                End If
            Next

            Return dt
        End Get
    End Property

    Dim _sup_id As String

    Public WriteOnly Property SetSpplierID As String
        Set(value As String)
            _sup_id = value
        End Set
    End Property


    Private Sub popupSelectCustomer_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        CreateGrdColumn()
        ShowData(txtSearch.Text)
    End Sub

    Sub CreateGrdColumn()
        grdItems.AutoGenerateColumns = False
        Dim SUP_CHECK As New System.Windows.Forms.DataGridViewCheckBoxColumn
        SUP_CHECK.Width = 50
        SUP_CHECK.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdItems.Columns.Add(SUP_CHECK)

        Dim SUP_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        SUP_ID.Name = "SUP_ID"
        SUP_ID.DataPropertyName = "SUP_ID"
        SUP_ID.Visible = False
        grdItems.Columns.Add(SUP_ID)
        grdItems.Columns("SUP_ID").Visible = False

        Dim SUP_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        SUP_NAME.DataPropertyName = "SUP_NAME"
        SUP_NAME.HeaderText = "Supplier"
        SUP_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        SUP_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdItems.Columns.Add(SUP_NAME)
    End Sub

    Private Sub ShowData(strSearch As String)
        Dim SQL As String = ""
        SQL = "SELECT SUP_ID,SUP_NAME FROM SUPPLIER  where 1=1"
        If strSearch.Trim <> "" Then
            SQL &= " and SUP_NAME LIKE '%" & strSearch.Replace("'", "''") & "%'"
        End If

        If _sup_id <> "" Then
            SQL &= " and SUP_ID not in (" & _sup_id & ")"
        End If

        SQL &= " ORDER BY SUP_NAME"

        Dim DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdItems.DataSource = DT
        lblRec.Text = "Total   " & DT.Rows.Count & "   Record"

        For Each row As DataGridViewRow In grdItems.Rows
            row.Cells(0).Value = False
        Next
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As System.EventArgs) Handles txtSearch.TextChanged
        ShowData(txtSearch.Text)
    End Sub
End Class
﻿Imports System.Data
Imports System.Data.SqlClient
Imports BAFCO_TIS.Org.Mentalis.Files

Public Class Login

    Public ExitApp As Boolean = False

    Private Sub btnLogin_Click(sender As System.Object, e As System.EventArgs) Handles btnLogin.Click
        Dim Sql As String = ""

        Sql = "SELECT * FROM [USER] WHERE US_CODE='" & txtUsername.Text.Replace("'", "''") & "'"
        Dim DA As New SqlDataAdapter(Sql, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count = 0 Then
            MessageBox.Show("Invalid username", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtUsername.Focus()
            Exit Sub
        End If

        If DT.Rows(0).Item("US_PASS").ToString <> txtPassword.Text Then
            MessageBox.Show("Invalid password", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtPassword.Focus()
            Exit Sub
        End If

        If DT.Rows(0).Item("ACTIVE_STATUS").ToString <> "True" Then
            MessageBox.Show("This username is unavailable", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtUsername.Focus()
            Exit Sub
        End If

        Dim ini As New IniReader(INIFile)
        ini.Section = "SETTING"
        ini.Write("userlogin", txtUsername.Text)
        myUser.user_id = DT.Rows(0).Item("US_ID").ToString
        myUser.fulllname = DT.Rows(0).Item("US_NAME").ToString
        myUser.us_code = DT.Rows(0)("US_CODE").ToString

        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub txtUsername_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUsername.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtPassword.Focus()
        End If
    End Sub

    Private Sub txtPassword_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPassword.KeyPress
        If Asc(e.KeyChar) = 13 Then
            btnLogin.PerformClick()
        End If
    End Sub

    Private Sub Login_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Dim ini As New IniReader(INIFile)
        ini.Section = "SETTING"
        If CStr(ini.ReadString("userlogin")) <> "" Then
            txtUsername.Text = CStr(ini.ReadString("userlogin"))
            txtPassword.Focus()
        Else
            txtUsername.Text = ""
            txtPassword.Text = ""
            txtUsername.Focus()
        End If


        ''*************** Test
        'txtPassword.Text = "1234"
        'btnLogin.PerformClick()
    End Sub

    Private Sub Login_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
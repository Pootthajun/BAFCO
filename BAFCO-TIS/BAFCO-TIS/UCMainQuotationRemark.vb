﻿Public Class UCMainQuotationRemark

    Public QTCategoryID As Integer = 0
    Public Event HeightChanged(h As Integer)

    Public WriteOnly Property EnableRemark As Boolean
        Set(value As Boolean)
            btnAddRemark.Visible = value

            If flpRemark.Controls.Count > 0 Then
                For Each ctl As UCMainQuotationRemarkItem In flpRemark.Controls
                    ctl.EnableRemarkItem = value
                Next
            End If
        End Set
    End Property


    Public Sub AddRemarkItem()
        If QTCategoryID = 0 Then Exit Sub

        Dim ctl As New UCMainQuotationRemarkItem
        ctl.SetDDlRemark(QTCategoryID)
        AddHandler ctl.DeleteRemarkItem, AddressOf btnDeleteRemark_Click
        flpRemark.Controls.Add(ctl)

        ExpandRemarkForm(ctl)
    End Sub

    Private Sub ExpandRemarkForm(ctl As UCMainQuotationRemarkItem)
        If ctl.Height * flpRemark.Controls.Count > flpRemark.Height Then
            Dim plusHeight As Integer = ctl.Height

            flpRemark.Height += plusHeight
            Me.Height += plusHeight

            RaiseEvent HeightChanged(plusHeight)
        End If
    End Sub


    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        'If flpRemark.Controls.Count > 0 Then
        '    For Each rm As UCMainQuotationRemarkItem In flpRemark.Controls
        '        'If rm.GetType() Is GetType(ComboBox) Then
        '        If rm.cbbRemarks.SelectedValue = 0 Then
        '            MessageBox.Show("กรุณาเลือก Remark", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        '            rm.cbbRemarks.Focus()
        '            Return False
        '        End If
        '        'End If
        '    Next
        'End If
        Return ret
    End Function

    Public Function SaveMainQuotationRemarks(QuotationMainDetailID As Long, trans As SqlClient.SqlTransaction) As String
        Dim re As String = "false"
        If flpRemark.Controls.Count > 0 Then
            For Each rm As UCMainQuotationRemarkItem In flpRemark.Controls
                If rm.cbbRemarks.SelectedValue > 0 Then
                    Dim Sql As String = "insert into QUOTATION_MAIN_REMARK(created_by,created_date,quotation_main_detail_id,qt_category_remark_id) "
                    Sql += " values(@_CREATED_BY, getdate(), @_MAIN_DETAIL_ID, @_REMARK_ID)"

                    Dim p(3) As SqlClient.SqlParameter
                    p(0) = SetText("@_CREATED_BY", myUser.fulllname)
                    p(1) = SetBigInt("@_MAIN_DETAIL_ID", QuotationMainDetailID)
                    p(2) = SetBigInt("@_REMARK_ID", rm.cbbRemarks.SelectedValue)

                    re = Execute_Command(Sql, trans, p)
                    If re = "false" Then
                        Exit For
                    End If
                Else
                    re = "true"
                End If
            Next
        Else
            re = "true"
        End If

        Return re
    End Function

    Public Sub FillInRemarkData(MainQuotationDetailID As Long)
        Dim sql As String = " select * from QUOTATION_MAIN_REMARK"
        sql += " where quotation_main_detail_id=@_MAIN_QUOTATION_DETAIL_ID"
        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_MAIN_QUOTATION_DETAIL_ID", MainQuotationDetailID)

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim uc As New UCMainQuotationRemarkItem
                uc.SetDDlRemark(QTCategoryID)
                uc.cbbRemarks.SelectedValue = dr("qt_category_remark_id")

                AddHandler uc.DeleteRemarkItem, AddressOf btnDeleteRemark_Click
                flpRemark.Controls.Add(uc)

                ExpandRemarkForm(uc)
            Next
        End If
        dt.Dispose()
    End Sub

    Private Sub btnAddRemark_Click(sender As System.Object, e As System.EventArgs) Handles btnAddRemark.Click
        AddRemarkItem()
    End Sub

    Public Sub btnDeleteRemark_Click(ctl As UCMainQuotationRemarkItem)
        Dim ctlHeight As Integer = ctl.Height
        flpRemark.Controls.Remove(ctl)

        flpRemark.Height -= ctlHeight
        Me.Height -= ctlHeight

        RaiseEvent HeightChanged(ctlHeight * -1)
    End Sub

    Private Sub SetDDlRemark(ddl As ComboBox)
        Dim sql As String = "select id, remarks "
        sql += " from QT_CATEGORY_REMARK "
        sql += " where active_status='Y' "
        sql += " and qt_category_id=@_CATEGORY_ID"
        sql += " order by remarks"

        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_CATEGORY_ID", QTCategoryID)

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt Is Nothing Then
            dt = New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("remarks")
        End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("remarks") = ""
        dt.Rows.InsertAt(dr, 0)

        ddl.DataSource = dt
        ddl.DisplayMember = "remarks"
        ddl.ValueMember = "id"
    End Sub
End Class

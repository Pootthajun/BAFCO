﻿Imports System.Data
Imports System.Data.SqlClient
Imports OfficeOpenXml

Public Class frmCustomer

    Dim DA As New SqlDataAdapter

    Private Sub frmCustomer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        ShowData()
    End Sub

    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT c.cus_id, c.cus_account, c.cus_name, c.cus_address, c.tax_id, ct.cus_type_name, cr.credit_term, c.update_by,c.update_date, "
        SQL += "	(select count(qt.id) from QUOTATION_MAIN qt where qt.cus_id=c.CUS_ID) qt_main,"
        SQL += "    (select count(qt.id) from QUOTATION_BYCASE qt where qt.cus_id=c.CUS_ID) qt_bycase"
        SQL += " FROM CUSTOMER c "
        SQL += " inner join CUSTOMER_TYPE ct on ct.cus_type_id=c.cus_type_id"
        SQL += " left join MS_CREDIT_TERM cr on cr.id=c.ms_credit_term_id"
        SQL += "  ORDER BY c.CUS_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT
        lblRec.Text = "Total   " & DT.Rows.Count & "   Record"

        'SQL = "SELECT CUS_TYPE_ID,CUS_TYPE_NAME FROM CUSTOMER_TYPE ORDER BY CUS_TYPE_NAME"
        'Dim DT_TYPE As New DataTable
        'Dim DA_TYPE As New SqlDataAdapter(SQL, ConnStr)
        'DA_TYPE.Fill(DT_TYPE)

        Dim CUS_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_ID.Name = "CUS_ID"
        CUS_ID.DataPropertyName = "CUS_ID"
        CUS_ID.Visible = False
        grdMain.Columns.Add(CUS_ID)
        grdMain.Columns("CUS_ID").Visible = False

        Dim CUS_ACC_NO As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_ACC_NO.Name = "CUS_ACC_NO"
        CUS_ACC_NO.DataPropertyName = "CUS_ACCOUNT"
        CUS_ACC_NO.HeaderText = "Account No"
        CUS_ACC_NO.SortMode = DataGridViewColumnSortMode.NotSortable
        CUS_ACC_NO.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CUS_ACC_NO.ReadOnly = True
        grdMain.Columns.Add(CUS_ACC_NO)

        Dim CUS_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_NAME.Name = "CUS_NAME"
        CUS_NAME.DataPropertyName = "CUS_NAME"
        CUS_NAME.HeaderText = "Customer"
        CUS_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        CUS_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CUS_NAME.ReadOnly = True
        grdMain.Columns.Add(CUS_NAME)


        Dim CUS_TYPE As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_TYPE.Name = "CUS_TYPE_NAME"
        CUS_TYPE.DataPropertyName = "CUS_TYPE_NAME"
        CUS_TYPE.HeaderText = "Customer Group"
        CUS_TYPE.SortMode = DataGridViewColumnSortMode.NotSortable
        CUS_TYPE.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CUS_TYPE.Width = 180
        CUS_TYPE.ReadOnly = True
        grdMain.Columns.Add(CUS_TYPE)

        Dim CUS_ADDR As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_ADDR.Name = "CUS_ADDR"
        CUS_ADDR.DataPropertyName = "CUS_ADDRESS"
        CUS_ADDR.HeaderText = "Address"
        CUS_ADDR.SortMode = DataGridViewColumnSortMode.NotSortable
        CUS_ADDR.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CUS_ADDR.ReadOnly = True
        grdMain.Columns.Add(CUS_ADDR)

        Dim CUS_CREDIT_TERM As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_CREDIT_TERM.Name = "CUS_CREDIT_TERM"
        CUS_CREDIT_TERM.DataPropertyName = "CREDIT_TERM"
        CUS_CREDIT_TERM.HeaderText = "Credit Term"
        CUS_CREDIT_TERM.SortMode = DataGridViewColumnSortMode.NotSortable
        CUS_CREDIT_TERM.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        CUS_CREDIT_TERM.ReadOnly = True
        grdMain.Columns.Add(CUS_CREDIT_TERM)

        Dim TAX_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        TAX_ID.Name = "TAX_ID"
        TAX_ID.DataPropertyName = "TAX_ID"
        TAX_ID.HeaderText = "Tax ID"
        TAX_ID.SortMode = DataGridViewColumnSortMode.NotSortable
        TAX_ID.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        TAX_ID.ReadOnly = True
        grdMain.Columns.Add(TAX_ID)

        Dim QT_MAIN As New System.Windows.Forms.DataGridViewTextBoxColumn
        QT_MAIN.Name = "QT_MAIN"
        QT_MAIN.DataPropertyName = "QT_MAIN"
        QT_MAIN.HeaderText = "Main QT"
        QT_MAIN.SortMode = DataGridViewColumnSortMode.NotSortable
        QT_MAIN.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        QT_MAIN.ReadOnly = True
        grdMain.Columns.Add(QT_MAIN)

        Dim QT_ByCase As New System.Windows.Forms.DataGridViewTextBoxColumn
        QT_ByCase.Name = "QT_BYCASE"
        QT_ByCase.DataPropertyName = "QT_BYCASE"
        QT_ByCase.HeaderText = "Case by Case QT"
        QT_ByCase.SortMode = DataGridViewColumnSortMode.NotSortable
        QT_ByCase.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        QT_ByCase.ReadOnly = True
        grdMain.Columns.Add(QT_ByCase)

        Dim UPDATE_BY As New System.Windows.Forms.DataGridViewTextBoxColumn
        UPDATE_BY.DataPropertyName = "UPDATE_BY"
        UPDATE_BY.Visible = False
        grdMain.Columns.Add(UPDATE_BY)

        Dim UPDATE_DATE As New System.Windows.Forms.DataGridViewTextBoxColumn
        UPDATE_DATE.DataPropertyName = "UPDATE_DATE"
        UPDATE_DATE.Visible = False
        grdMain.Columns.Add(UPDATE_DATE)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("CUS_NAME")) OrElse tmp.Rows(i).Item("CUS_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("CUS_TYPE_ID")) OrElse tmp.Rows(i).Item("CUS_TYPE_ID").ToString = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "CUS_NAME='" & tmp.Rows(i).Item("CUS_NAME").ToString.Replace("'", "''") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        '------------------Check Deleted Record-----
        Dim SQL As String = ""
        SQL &= "SELECT DISTINCT * FROM (" & vbCrLf
        SQL &= "SELECT DISTINCT CUS_ID FROM COST_CUS" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT DISTINCT CUS_ID FROM SERVICE_CUS" & vbCrLf
        SQL &= ") AS TB" & vbCrLf
        Dim TA As New SqlDataAdapter(SQL, ConnStr)
        Dim TT As New DataTable
        TA.Fill(TT)
        For i As Integer = 0 To TT.Rows.Count - 1
            If IsDBNull(TT.Rows(i).Item("CUS_ID")) Then Continue For
            tmp.DefaultView.RowFilter = "CUS_ID=" & TT.Rows(i).Item("CUS_ID")
            If tmp.DefaultView.Count > 0 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลที่คุณต้องการลบ ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ShowData()
                Exit Sub
            End If
        Next
        '------------------Batch Save---------------
        Dim ID As Integer = 0
        ID = GetNewID("CUSTOMER", "CUS_ID")
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(DT.Rows(i).Item("CUS_ID")) Then
                DT.Rows(i).Item("CUS_ID") = ID
                ID = ID + 1
                DT.Rows(i).Item("UPDATE_BY") = myUser.user_id
                DT.Rows(i).Item("UPDATE_DATE") = Now
            End If

        Next

        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            Exit Sub
        End Try

        txtSearch.Text = ""
        'cbbSupType.SelectedIndex = 0
        ShowData()
        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmCustomerAddEdit
        frm.ShowDialog()
        ShowData()
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        Filter()
    End Sub

    Sub Filter()
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = " 1=1 "
        'If Add = True Then
        '    Filter &= "CUS_ID IS NULL OR  "
        'End If
        Filter &= " AND (CUS_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'  "
        Filter += " or CUS_ACCOUNT LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'"
        Filter += " or CUS_ADDRESS LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'"
        Filter += " or TAX_ID LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'"
        Filter += " or CUS_TYPE_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'"
        Filter += " or CREDIT_TERM LIKE '%" & txtSearch.Text.Replace("'", "''") & "%'"
        Filter += " )"
        'If cbbSupType.Text <> "" Then
        '    Filter &= "CUS_TYPE_ID = " & cbbSupType.SelectedValue & " AND "
        'End If
        'If Filter <> "" Then
        '    Filter = Filter.Substring(0, Filter.Length - 4)
        'End If
        DT.DefaultView.RowFilter = Filter
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        frmCustomer_Load(Nothing, Nothing)
    End Sub

    Private Sub grdMain_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMain.CellDoubleClick
        'If e.ColumnIndex = grdMain.Columns("CUS_NAME").Index Then
        Dim frm As New frmCustomerAddEdit
        frm.CusID = grdMain.Rows(e.RowIndex).Cells("CUS_ID").Value
        frm.ShowDialog()
        ShowData()
        'End If
    End Sub


    Private Sub btnExport_Click(sender As System.Object, e As System.EventArgs) Handles btnExport.Click
        Dim cusID As String = ""
        For Each dgv As DataGridViewRow In grdMain.Rows
            If cusID = "" Then
                cusID = dgv.Cells("CUS_ID").Value
            Else
                cusID += ", " & dgv.Cells("CUS_ID").Value
            End If
        Next

        Dim sql As String = "select c.cus_id, c.CUS_NAME, ct.CUS_TYPE_NAME, c.tax_id, c.cus_account, c.cus_address, crt.credit_term, c.billing_method,c.payment_method, c.customer_note,"
        sql += " cp.contact_type, cp.contact_name, cp.tel_no, cp.mobile_no, cp.email,"
        sql += " bb.branch_name, cs.scope_name"
        sql += " from CUSTOMER c"
        sql += " inner join CUSTOMER_TYPE ct on ct.CUS_TYPE_ID=c.CUS_TYPE_ID"
        sql += " left join MS_CREDIT_TERM crt on crt.id=c.ms_credit_term_id"
        sql += " left join CUSTOMER_CONTACT_PERSON cp on c.cus_id=cp.customer_id"
        sql += " left join MS_BRANCH_CUSTOMER bc on c.cus_id= bc.cus_id"
        sql += " left join MS_BAFCO_BRANCH bb on bb.id=bc.ms_bafco_branch_id"
        sql += " left join MS_CUSTOMER_ASSIGN_SCOPE cas on cas.cus_id=c.cus_id"
        sql += " left join MS_CUSTOMER_SCOPE cs on cs.id=cas.ms_customer_scope_id"
        sql += " where c.cus_id in (" & cusID & ")"
        sql += " order by c.CUS_NAME"

        Dim tmpDt As DataTable = Execute_DataTable(sql)
        If tmpDt.Rows.Count > 0 Then
            Dim cusDt As New DataTable
            cusDt = tmpDt.DefaultView.ToTable(True, "cus_id", "cus_name", "cus_type_name", "tax_id", "cus_account", "cus_address", "credit_term", "billing_method", "payment_method", "customer_note").Copy
            If cusDt.Rows.Count > 0 Then
                Using ep As New ExcelPackage
                    Dim ws As ExcelWorksheet = ep.Workbook.Worksheets.Add("CustomerList")
                    ws.Cells("A2").Value = "Account No"
                    ws.Cells("B2").Value = "Customer Name"
                    ws.Cells("C2").Value = "Tax ID"
                    ws.Cells("D2").Value = "Address"
                    ws.Cells("E2").Value = "Type Name"
                    ws.Cells("F2").Value = "Branch"
                    ws.Cells("G2").Value = "Scope"
                    ws.Cells("H2").Value = "Credit Term"
                    ws.Cells("I2").Value = "Billing Method"
                    ws.Cells("J2").Value = "Payment Method"

                    ws.Cells("K1").Value = "Operation Contact"
                    ws.Cells("K1:N1").Merge = True
                    ws.Cells("K2").Value = "Contact Name"
                    ws.Cells("L2").Value = "Tel"
                    ws.Cells("M2").Value = "Mobile No"
                    ws.Cells("N2").Value = "Email"

                    ws.Cells("O1").Value = "Account Contact"
                    ws.Cells("O1:R1").Merge = True
                    ws.Cells("O2").Value = "Contact Name"
                    ws.Cells("P2").Value = "Tel"
                    ws.Cells("Q2").Value = "Mobile No"
                    ws.Cells("R2").Value = "Email"

                    ws.Cells("S2").Value = "Note"

                    ws.Cells("A1:S2").Style.Font.Bold = True
                    Using RowHeader As ExcelRange = ws.Cells("A1:S2")
                        RowHeader.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin
                        RowHeader.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin
                        RowHeader.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin
                        RowHeader.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin
                        RowHeader.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center
                    End Using

                    Dim iStart As Integer = 3
                    Dim i As Integer = iStart
                    For Each cusDr As DataRow In cusDt.Rows
                        If Convert.IsDBNull(cusDr("cus_account")) = False Then ws.Cells("A" & i).Value = cusDr("cus_account")
                        If Convert.IsDBNull(cusDr("cus_name")) = False Then ws.Cells("B" & i).Value = cusDr("cus_name")
                        If Convert.IsDBNull(cusDr("tax_id")) = False Then ws.Cells("C" & i).Value = cusDr("tax_id")
                        If Convert.IsDBNull(cusDr("cus_address")) = False Then ws.Cells("D" & i).Value = cusDr("cus_address")
                        If Convert.IsDBNull(cusDr("cus_type_name")) = False Then ws.Cells("E" & i).Value = cusDr("cus_type_name")

                        'Branch Data
                        Dim bDt As New DataTable
                        tmpDt.DefaultView.RowFilter = "cus_id='" & cusDr("cus_id") & "'"
                        bDt = tmpDt.DefaultView.ToTable(True, "branch_name").Copy
                        If bDt.Rows.Count > 0 Then
                            Dim BranchName As String = ""
                            For Each bDr As DataRow In bDt.Rows
                                BranchName += bDr("branch_name") & Chr(13)
                            Next
                            ws.Cells("F" & i).Value = BranchName
                        End If
                        bDt.Dispose()
                        tmpDt.DefaultView.RowFilter = ""

                        'Scope Data
                        Dim sDt As New DataTable
                        tmpDt.DefaultView.RowFilter = "cus_id='" & cusDr("cus_id") & "'"
                        sDt = tmpDt.DefaultView.ToTable(True, "scope_name").Copy
                        If sDt.Rows.Count > 0 Then
                            Dim ScopeName As String = ""
                            For Each sDr As DataRow In sDt.Rows
                                ScopeName += sDr("scope_name") & Chr(13)
                            Next
                            ws.Cells("G" & i).Value = ScopeName
                            ws.Cells("G" & i).AutoFitColumns()
                            ws.Cells("G" & i).Style.WrapText = True
                        End If
                        sDt.Dispose()


                        If Convert.IsDBNull(cusDr("credit_term")) = False Then ws.Cells("H" & i).Value = cusDr("credit_term")
                        If Convert.IsDBNull(cusDr("billing_method")) = False Then ws.Cells("I" & i).Value = cusDr("billing_method")
                        If Convert.IsDBNull(cusDr("payment_method")) = False Then ws.Cells("J" & i).Value = cusDr("payment_method")

                        Dim ocDt As New DataTable
                        tmpDt.DefaultView.RowFilter = "cus_id='" & cusDr("cus_id") & "' and contact_type=1"  'Operation Contact
                        ocDt = tmpDt.DefaultView.ToTable(True, "contact_name", "tel_no", "mobile_no", "email").Copy
                        If ocDt.Rows.Count > 0 Then
                            Dim ocDr As DataRow = ocDt.Rows(0)
                            If Convert.IsDBNull(ocDr("contact_name")) = False Then ws.Cells("K" & i).Value = ocDr("contact_name")
                            If Convert.IsDBNull(ocDr("tel_no")) = False Then ws.Cells("L" & i).Value = ocDr("tel_no")
                            If Convert.IsDBNull(ocDr("mobile_no")) = False Then ws.Cells("M" & i).Value = ocDr("mobile_no")
                            If Convert.IsDBNull(ocDr("email")) = False Then ws.Cells("N" & i).Value = ocDr("email")
                        End If
                        ocDt.Dispose()
                        tmpDt.DefaultView.RowFilter = ""


                        Dim acDt As New DataTable
                        tmpDt.DefaultView.RowFilter = "cus_id='" & cusDr("cus_id") & "' and contact_type=2"  'Account Contact
                        acDt = tmpDt.DefaultView.ToTable(True, "contact_name", "tel_no", "mobile_no", "email").Copy
                        If acDt.Rows.Count > 0 Then
                            Dim acDr As DataRow = acDt.Rows(0)
                            If Convert.IsDBNull(acDr("contact_name")) = False Then ws.Cells("O" & i).Value = acDr("contact_name")
                            If Convert.IsDBNull(acDr("tel_no")) = False Then ws.Cells("P" & i).Value = acDr("tel_no")
                            If Convert.IsDBNull(acDr("mobile_no")) = False Then ws.Cells("Q" & i).Value = acDr("mobile_no")
                            If Convert.IsDBNull(acDr("email")) = False Then ws.Cells("R" & i).Value = acDr("email")
                        End If
                        acDt.Dispose()
                        tmpDt.DefaultView.RowFilter = ""

                        If Convert.IsDBNull(cusDr("customer_note")) = False Then ws.Cells("S" & i).Value = cusDr("customer_note")

                        i += 1
                    Next
                    ws.Cells("A" & (iStart - 1) & ":S" & i).AutoFitColumns()
                    ws.Cells("F" & iStart & ":G" & i).Style.WrapText = True

                    Using fs As New SaveFileDialog
                        fs.Filter = "Excel File(*.xlsx)|*.xlsx"
                        If fs.ShowDialog = Windows.Forms.DialogResult.OK Then
                            ep.SaveAs(New IO.FileInfo(fs.FileName))
                        End If
                    End Using
                End Using
            End If


        End If
        tmpDt.Dispose()
    End Sub
End Class
﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmSettingQTDescription

    Dim DA As New SqlDataAdapter

    Private Sub frmSettingQTDescription_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        txtSearch.Text = ""
        rdiSearchAll.Checked = True
        cbbCategory.SelectedValue = 0
        ShowData()
        Filter()
    End Sub

    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT d.id, d.desc_name, d.active_status,d.qt_category_id, c.category_name,d.used_by_case_qt "
        SQL += " FROM QT_CATEGORY_DESCRIPTION d"
        SQL += " inner join QT_CATEGORY c on c.id=d.qt_category_id "
        SQL += "  ORDER BY c.category_name,d.desc_name"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT

        SQL = "SELECT id,category_name FROM qt_category where active_status='Y' ORDER BY category_name"
        Dim DT_CAT As DataTable = Execute_DataTable(SQL)
        Dim dr_cat As DataRow = DT_CAT.NewRow
        dr_cat("id") = 0
        dr_cat("category_name") = ""
        DT_CAT.Rows.InsertAt(dr_cat, 0)


        Dim DESC_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        DESC_ID.Name = "DESC_ID"
        DESC_ID.DataPropertyName = "id"
        DESC_ID.Visible = False
        grdMain.Columns.Add(DESC_ID)
        grdMain.Columns("DESC_ID").Visible = False

        Dim DESC_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        DESC_NAME.DataPropertyName = "DESC_NAME"
        DESC_NAME.HeaderText = "Description"
        DESC_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        DESC_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(DESC_NAME)

        Dim CAT_ID As New System.Windows.Forms.DataGridViewComboBoxColumn
        CAT_ID.DataSource = DT_CAT
        CAT_ID.DisplayMember = "category_name"
        CAT_ID.ValueMember = "id"
        CAT_ID.DataPropertyName = "qt_category_id"
        CAT_ID.HeaderText = "Category"
        CAT_ID.Width = 300
        grdMain.Columns.Add(CAT_ID)



        Dim BYCASE As New System.Windows.Forms.DataGridViewCheckBoxColumn
        BYCASE.DataPropertyName = "used_by_case_qt"
        BYCASE.TrueValue = "Y"
        BYCASE.FalseValue = "N"
        BYCASE.HeaderText = "Case By Case QT"
        BYCASE.Width = 120
        grdMain.Columns.Add(BYCASE)

        Dim CAT_ACTIVE As New System.Windows.Forms.DataGridViewCheckBoxColumn
        CAT_ACTIVE.DataPropertyName = "active_status"
        CAT_ACTIVE.TrueValue = "Y"
        CAT_ACTIVE.FalseValue = "N"
        CAT_ACTIVE.HeaderText = "Active"
        CAT_ACTIVE.Width = 100
        grdMain.Columns.Add(CAT_ACTIVE)

        Dim DT_SearchCat As New DataTable
        DT_SearchCat = DT_CAT.Copy
        cbbCategory.DisplayMember = "category_name"
        cbbCategory.ValueMember = "id"
        cbbCategory.DataSource = DT_SearchCat
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("DESC_NAME")) OrElse tmp.Rows(i).Item("DESC_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("QT_CATEGORY_ID")) OrElse tmp.Rows(i).Item("QT_CATEGORY_ID").ToString = "0" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "DESC_NAME='" & tmp.Rows(i).Item("DESC_NAME").ToString.Replace("'", "''") & "' and QT_CATEGORY_ID='" & tmp(i)("QT_CATEGORY_ID") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next

        '------------------Batch Save---------------
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction

        Dim ret As String = "false"
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim sql As String = ""

            Dim p(6) As SqlParameter
            If DT.Rows(i)("id") > 0 Then
                sql = "update qt_category_description "
                sql += " set desc_name=@_DESC_NAME"
                sql += ", qt_category_id=@_CATEGORY_ID"
                sql += ", used_by_case_qt=@_USED_BY_CASE_QT"
                sql += ", active_status=@_ACTIVE_STATUS "
                sql += ", updated_by=@_UPDATED_BY "
                sql += ", updated_date=getdate()"
                sql += " where id=@_ID"

                p(0) = SetBigInt("@_ID", DT.Rows(i)("id"))
                p(1) = SetText("@_UPDATED_BY", myUser.us_code)
            Else
                sql = "insert into qt_category_description(created_by, created_date, desc_name, qt_category_id, used_by_case_qt,  active_status)"
                sql += " values(@_CREATED_BY, getdate(), @_DESC_NAME, @_CATEGORY_ID, @_USED_BY_CASE_QT,  @_ACTIVE_STATUS) "

                p(0) = SetText("@_CREATED_BY", myUser.us_code)
            End If

            p(2) = SetText("@_DESC_NAME", DT.Rows(i)("desc_name"))
            p(3) = SetBigInt("@_CATEGORY_ID", DT.Rows(i)("qt_category_id"))
            p(4) = SetText("@_USED_BY_CASE_QT", DT.Rows(i)("used_by_case_qt"))
            p(5) = SetText("@_ACTIVE_STATUS", DT.Rows(i)("active_status"))

            ret = Execute_Command(sql, trans, p)
            If ret.ToLower <> "true" Then
                Exit For
            End If
        Next

        If ret.ToLower = "true" Then
            trans.Commit()

            txtSearch.Text = ""
            rdiSearchAll.Checked = True
            frmSettingQTDescription_Load(Nothing, Nothing)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            trans.Rollback()
            MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DR("id") = 0
        DR("qt_category_id") = 0
        DR("used_by_case_qt") = "N"
        DR("active_status") = "Y"

        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_KeyUp(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        Filter()
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        Filter()
    End Sub

    Private Sub cbbCategory_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbbCategory.SelectedIndexChanged
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        If grdMain.DataSource Is Nothing Then Exit Sub
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= " ID = 0 OR  "
        End If
        Filter &= "DESC_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' AND "
        If cbbCategory.SelectedIndex > 0 Then
            Filter += "QT_CATEGORY_ID = " & cbbCategory.SelectedValue & " AND "
        End If

        If rdiSearchActive.Checked = True Then
            Filter += " active_status='Y' AND "
        End If
        If rdiSearchInactive.Checked = True Then
            Filter += " active_status='N' and "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter

        lblRec.Text = "Total   " & DT.DefaultView.Count & "   Record"
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        frmSettingQTDescription_Load(Nothing, Nothing)
    End Sub
End Class
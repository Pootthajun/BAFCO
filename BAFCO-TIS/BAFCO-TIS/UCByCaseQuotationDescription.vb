﻿Public Class UCByCaseQuotationDescription

    Public Event HeightChanged(h As Integer)
    Public Event PriceTotalChange()
    Public Event SelectDescription(ddlDescription As ComboBox, txtPrice As TextBox, ddlBasis As ComboBox, ddlCurrency As ComboBox)

    Public WriteOnly Property EnableDescription As Boolean
        Set(value As Boolean)
            btnAddDescription.Visible = value

            If flpDescription.Controls.Count > 0 Then
                For Each ctl As UCByCaseQuotationDescriptionItem In flpDescription.Controls
                    ctl.EnableDescriptionItem = value
                Next
            End If
        End Set
    End Property


    Public Sub AddDescriptionItem()
        Dim uc As New UCByCaseQuotationDescriptionItem
        uc.SetDDL()
        AddHandler uc.DeleteDescription, AddressOf UCByCaseQuotationDescriptionItem_DeleteDescription
        AddHandler uc.PriceTotalChange, AddressOf UCByCaseQuotationDescriptionItem_PriceTotalChange
        AddHandler uc.SelectDescriptionItem, AddressOf UCByCaseQuotationDescriptionItem_SelectDescriptionItem
        flpDescription.Controls.Add(uc)

        'If uc.Height * flpDescription.Controls.Count > flpDescription.Height Then
        '    Dim plusHeight As Integer = uc.Height

        '    flpDescription.Height += plusHeight
        '    Me.Height += plusHeight

        '    RaiseEvent HeightChanged(plusHeight)
        'End If
    End Sub

    Private Sub UCByCaseQuotationDescriptionItem_SelectDescriptionItem(ddlDescription As ComboBox, txtPrice As TextBox, ddlBasis As ComboBox, ddlCurrency As ComboBox)
        RaiseEvent SelectDescription(ddlDescription, txtPrice, ddlBasis, ddlCurrency)
    End Sub

    Private Sub UCByCaseQuotationDescriptionItem_PriceTotalChange()
        RaiseEvent PriceTotalChange()
    End Sub

    Public Sub ClearDescriptionItem()
        flpDescription.Controls.Clear()
    End Sub

    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If flpDescription.Controls.Count > 0 Then
            For Each des As UCByCaseQuotationDescriptionItem In flpDescription.Controls
                If des.cbbDescription.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Description", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbDescription.Focus()
                    Return False
                End If

                If des.txtUnitRate.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Unit Rate", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtUnitRate.Focus()
                    Return False
                End If

                If des.txtMinRate.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Min Rate", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtMinRate.Focus()
                    Return False
                End If

                If des.cbbBasis.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Basis", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbBasis.Focus()
                    Return False
                End If

                If des.txtQty.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Qty", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtQty.Focus()
                    Return False
                End If

                If des.cbbCurrency.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Currency", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbCurrency.Focus()
                    Return False
                End If
            Next
        End If
        Return ret
    End Function


    Public Sub FillInDescriptionData(ByCaseQuotationDetailID As Long)
        Dim Sql As String = " select qt_category_description_id, ms_basis_id, unit_rate, min_rate, qty, ms_currency_id, item_note"
        Sql += " from QUOTATION_BYCASE_ITEM "
        Sql += " where quotation_bycase_id=@_QUOTATION_BYCASE_ID"
        Sql += " order by id "

        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_QUOTATION_BYCASE_ID", ByCaseQuotationDetailID)
        Dim dt As DataTable = Execute_DataTable(Sql, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim uc As New UCByCaseQuotationDescriptionItem
                uc.SetDDL()
                uc.cbbDescription.SelectedValue = dr("qt_category_description_id")
                uc.txtUnitRate.Text = dr("unit_rate")
                uc.txtMinRate.Text = dr("min_rate")
                uc.cbbBasis.SelectedValue = dr("ms_basis_id")
                uc.txtQty.Text = dr("qty")
                uc.txtTotal.Text = (Convert.ToDouble(dr("unit_rate")) * Convert.ToInt64(dr("qty"))).ToString("#,##0.00")
                uc.cbbCurrency.SelectedValue = dr("ms_currency_id")
                If Convert.IsDBNull(dr("item_note")) = False Then
                    uc.lblNote.Text = dr("item_note")
                    If uc.lblNote.Text.Trim <> "" Then
                        uc.btnNote.BackgroundImage = My.Resources.Edit
                    End If
                End If


                AddHandler uc.DeleteDescription, AddressOf UCByCaseQuotationDescriptionItem_DeleteDescription
                AddHandler uc.PriceTotalChange, AddressOf UCByCaseQuotationDescriptionItem_PriceTotalChange

                flpDescription.Controls.Add(uc)

                If uc.Height * flpDescription.Controls.Count > flpDescription.Height Then
                    Dim plusHeight As Integer = uc.Height

                    flpDescription.Height += plusHeight
                    Me.Height += plusHeight

                    RaiseEvent HeightChanged(plusHeight)
                End If
            Next

            RaiseEvent PriceTotalChange()
        End If
        dt.Dispose()

    End Sub

    Private Sub btnAddDescription_Click(sender As System.Object, e As System.EventArgs) Handles btnAddDescription.Click
        AddDescriptionItem()
    End Sub

    Private Sub UCByCaseQuotationDescriptionItem_DeleteDescription(ctl As UCByCaseQuotationDescriptionItem)
        Dim plusHeight As Integer = ctl.Height

        flpDescription.Controls.Remove(ctl)
        RaiseEvent PriceTotalChange()
    End Sub
End Class

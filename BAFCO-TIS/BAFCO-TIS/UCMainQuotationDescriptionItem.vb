﻿Public Class UCMainQuotationDescriptionItem

    Public Event DeleteDescription(ctl As UCMainQuotationDescriptionItem)

    Public WriteOnly Property EnableDescriptionItem As Boolean
        Set(value As Boolean)
            cbbDescription.Enabled = value
            cbbCurrency.Enabled = value
            cbbBasis.Enabled = value
            txtPrice.Enabled = value
            btnDelete.Visible = value
        End Set
    End Property


    Public Sub SetDDL(QtCategoryID As Long)
        SetDDLBasis()
        SetDDLDescription(QtCategoryID)
        SetDDLCurrency()
    End Sub

    Private Sub SetDDLBasis()
        Dim sql As String = "select id, basis_name from MS_BASIS where active_status='Y' order by basis_name"
        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("basis_name")
        'End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("basis_name") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbBasis.DataSource = dt
        cbbBasis.DisplayMember = "basis_name"
        cbbBasis.ValueMember = "id"
    End Sub

    Private Sub SetDDLCurrency()
        Dim sql As String = "select id, currency_name from MS_CURRENCY where active_status='Y' order by currency_name"
        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("currency_name")
        'End If

        'Dim dr As DataRow = dt.NewRow
        'dr("id") = 0
        'dr("currency_name") = ""
        'dt.Rows.InsertAt(dr, 0)

        cbbCurrency.DataSource = dt
        cbbCurrency.DisplayMember = "currency_name"
        cbbCurrency.ValueMember = "id"

        If dt.Rows.Count > 0 Then
            cbbCurrency.SelectedValue = 1
        End If
    End Sub
    Private Sub SetDDLDescription(QtCategoryID As Long)
        Dim sql As String = "select id, desc_name "
        sql += " from QT_CATEGORY_DESCRIPTION"
        sql += " where active_status='Y'"
        sql += " and qt_category_id=@_CATEGORY_ID"
        sql += " order by desc_name"

        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_CATEGORY_ID", QtCategoryID)

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt Is Nothing Then
            dt.Columns.Add("id")
            dt.Columns.Add("desc_name")
        End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("desc_name") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbDescription.DataSource = dt
        cbbDescription.DisplayMember = "desc_name"
        cbbDescription.ValueMember = "id"
    End Sub


    Private Sub txtPrice_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPrice.KeyPress
        'MessageBox.Show(e.KeyChar)
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        RaiseEvent DeleteDescription(Me)
    End Sub

    Private Sub btnNote_Click(sender As Object, e As System.EventArgs) Handles btnNote.Click
        Dim frm As New frmDialogItemNote
        frm.txtNote.Text = lblNote.Text

        If frm.ShowDialog = DialogResult.OK Then
            lblNote.Text = frm.txtNote.Text
        Else
            btnNote.BackgroundImage = My.Resources.BlankNote
        End If
    End Sub
End Class

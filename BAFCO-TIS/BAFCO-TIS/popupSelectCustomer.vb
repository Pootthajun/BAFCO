﻿Imports System.Data
Imports System.Data.SqlClient
Public Class popupSelectCustomer

    Public ReadOnly Property GetData() As DataTable
        Get
            Dim dt As New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("name")

            Dim dr As DataRow
            For Each row As DataGridViewRow In grdItems.Rows
                If row.Cells(0).Value = True Then
                    dr = dt.NewRow
                    dr("id") = row.Cells(1).Value
                    dr("name") = row.Cells(2).Value
                    dt.Rows.Add(dr)
                End If
            Next

            Return dt
        End Get
    End Property

    Dim _cus_id As String

    Public WriteOnly Property SetCustomerID As String
        Set(value As String)
            _cus_id = value
        End Set
    End Property


    Private Sub popupSelectCustomer_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        CreateGrdColumn()
        ShowData("")
    End Sub

    Sub CreateGrdColumn()
        grdItems.AutoGenerateColumns = False
        Dim CUS_CHECK As New System.Windows.Forms.DataGridViewCheckBoxColumn
        CUS_CHECK.Width = 50
        CUS_CHECK.AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
        grdItems.Columns.Add(CUS_CHECK)

        Dim CUS_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_ID.Name = "CUS_ID"
        CUS_ID.DataPropertyName = "CUS_ID"
        CUS_ID.Visible = False
        grdItems.Columns.Add(CUS_ID)
        grdItems.Columns("CUS_ID").Visible = False

        Dim CUS_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        CUS_NAME.DataPropertyName = "CUS_NAME"
        CUS_NAME.HeaderText = "Customer"
        CUS_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        CUS_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdItems.Columns.Add(CUS_NAME)
    End Sub

    Private Sub ShowData(strSearch As String)
        Dim SQL As String = ""
        SQL = "SELECT CUS_ID,CUS_NAME FROM CUSTOMER  where 1=1"
        If strSearch.Trim <> "" Then
            SQL &= " and CUS_NAME LIKE '%" & strSearch.Replace("'", "''") & "%'"
        End If

        If _cus_id <> "" Then
            SQL &= " and CUS_ID not in (" & _cus_id & ")"
        End If

        SQL &= " ORDER BY CUS_NAME"

        Dim DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdItems.DataSource = DT
        lblRec.Text = "Total   " & DT.Rows.Count & "   Record"

        For Each row As DataGridViewRow In grdItems.Rows
            row.Cells(0).Value = False
        Next
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click

        Me.DialogResult = Windows.Forms.DialogResult.OK

    End Sub
 
    Private Sub txtSearch_TextChanged(sender As Object, e As System.EventArgs) Handles txtSearch.TextChanged
        ShowData(txtSearch.Text)
    End Sub
End Class
﻿Public Class frmTestForm

    Private Sub frmTestForm_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim Sql As String = "SELECT id,category_name FROM qt_category where active_status='Y' ORDER BY category_name"
        Dim DT_CAT As DataTable = Execute_DataTable(Sql)
        Dim dr_cat As DataRow = DT_CAT.NewRow
        dr_cat("id") = 0
        dr_cat("category_name") = ""
        DT_CAT.Rows.InsertAt(dr_cat, 0)

        ComboBox1.DisplayMember = "category_name"
        ComboBox1.ValueMember = "id"
        ComboBox1.DataSource = DT_CAT



    End Sub


    Private Sub SetListBoxoAutoComplete(txt As String)

        Dim Sql As String = "SELECT id,category_name FROM qt_category where active_status='Y' and category_name like '%" & txt & "%' ORDER BY category_name"
        Dim DT_CAT As DataTable = Execute_DataTable(Sql)
        ListBox1.DisplayMember = "category_name"
        ListBox1.ValueMember = "id"
        ListBox1.DataSource = DT_CAT
        ListBox1.Visible = True
    End Sub


    'Private Sub ComboBox1_LostFocus(sender As Object, e As System.EventArgs) Handles ComboBox1.LostFocus
    '    ListBox1.Visible = False
    'End Sub

    'Private Sub ComboBox1_TextChanged(sender As Object, e As System.EventArgs) Handles ComboBox1.TextChanged
    '    SetListBoxoAutoComplete(ComboBox1.Text)
    '    'ListBox1.Focus()
    'End Sub
End Class
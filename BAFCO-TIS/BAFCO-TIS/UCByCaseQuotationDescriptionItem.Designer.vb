﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCByCaseQuotationDescriptionItem
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbbDescription = New System.Windows.Forms.ComboBox()
        Me.cbbCurrency = New System.Windows.Forms.ComboBox()
        Me.cbbBasis = New System.Windows.Forms.ComboBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.txtUnitRate = New System.Windows.Forms.TextBox()
        Me.txtMinRate = New System.Windows.Forms.TextBox()
        Me.lblNote = New System.Windows.Forms.Label()
        Me.ttpNote = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnNote = New System.Windows.Forms.Panel()
        Me.btnDelete = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'cbbDescription
        '
        Me.cbbDescription.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbDescription.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbDescription.Location = New System.Drawing.Point(0, 0)
        Me.cbbDescription.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbDescription.Name = "cbbDescription"
        Me.cbbDescription.Size = New System.Drawing.Size(340, 24)
        Me.cbbDescription.TabIndex = 0
        '
        'cbbCurrency
        '
        Me.cbbCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbCurrency.Location = New System.Drawing.Point(865, 0)
        Me.cbbCurrency.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbCurrency.Name = "cbbCurrency"
        Me.cbbCurrency.Size = New System.Drawing.Size(80, 24)
        Me.cbbCurrency.TabIndex = 6
        '
        'cbbBasis
        '
        Me.cbbBasis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbBasis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbBasis.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbBasis.Location = New System.Drawing.Point(339, 0)
        Me.cbbBasis.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbBasis.Name = "cbbBasis"
        Me.cbbBasis.Size = New System.Drawing.Size(180, 24)
        Me.cbbBasis.TabIndex = 1
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(782, 0)
        Me.txtTotal.MaxLength = 100
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(84, 24)
        Me.txtTotal.TabIndex = 5
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQty
        '
        Me.txtQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtQty.Location = New System.Drawing.Point(718, 0)
        Me.txtQty.MaxLength = 100
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(65, 24)
        Me.txtQty.TabIndex = 4
        Me.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtUnitRate
        '
        Me.txtUnitRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtUnitRate.Location = New System.Drawing.Point(518, 0)
        Me.txtUnitRate.MaxLength = 100
        Me.txtUnitRate.Name = "txtUnitRate"
        Me.txtUnitRate.Size = New System.Drawing.Size(108, 24)
        Me.txtUnitRate.TabIndex = 2
        Me.txtUnitRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMinRate
        '
        Me.txtMinRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtMinRate.Location = New System.Drawing.Point(625, 0)
        Me.txtMinRate.MaxLength = 100
        Me.txtMinRate.Name = "txtMinRate"
        Me.txtMinRate.Size = New System.Drawing.Size(94, 24)
        Me.txtMinRate.TabIndex = 3
        Me.txtMinRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNote
        '
        Me.lblNote.AutoSize = True
        Me.lblNote.Location = New System.Drawing.Point(926, 6)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(0, 13)
        Me.lblNote.TabIndex = 7
        Me.lblNote.Visible = False
        '
        'ttpNote
        '
        Me.ttpNote.AutoPopDelay = 5000
        Me.ttpNote.InitialDelay = 200
        Me.ttpNote.ReshowDelay = 100
        '
        'btnNote
        '
        Me.btnNote.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.BlankNote
        Me.btnNote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNote.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNote.Location = New System.Drawing.Point(948, 2)
        Me.btnNote.Name = "btnNote"
        Me.btnNote.Size = New System.Drawing.Size(20, 20)
        Me.btnNote.TabIndex = 5
        Me.ttpNote.SetToolTip(Me.btnNote, "Note")
        '
        'btnDelete
        '
        Me.btnDelete.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.Cancel
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Location = New System.Drawing.Point(971, 2)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(20, 20)
        Me.btnDelete.TabIndex = 4
        Me.ttpNote.SetToolTip(Me.btnDelete, "Remove")
        '
        'UCByCaseQuotationDescriptionItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblNote)
        Me.Controls.Add(Me.btnNote)
        Me.Controls.Add(Me.txtMinRate)
        Me.Controls.Add(Me.txtUnitRate)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.cbbBasis)
        Me.Controls.Add(Me.cbbCurrency)
        Me.Controls.Add(Me.cbbDescription)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "UCByCaseQuotationDescriptionItem"
        Me.Size = New System.Drawing.Size(995, 24)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbbDescription As System.Windows.Forms.ComboBox
    Friend WithEvents cbbCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents cbbBasis As System.Windows.Forms.ComboBox
    Friend WithEvents btnDelete As System.Windows.Forms.Panel
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents txtUnitRate As System.Windows.Forms.TextBox
    Friend WithEvents txtMinRate As System.Windows.Forms.TextBox
    Friend WithEvents btnNote As System.Windows.Forms.Panel
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents ttpNote As System.Windows.Forms.ToolTip

End Class

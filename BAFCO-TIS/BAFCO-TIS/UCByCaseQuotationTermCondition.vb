﻿Public Class UCByCaseQuotationTermCondition

    'Public QTCategoryID As Integer = 0
    Public Event HeightChanged(h As Integer)

    Public WriteOnly Property EnableTermCondition As Boolean
        Set(value As Boolean)
            btnAddRemark.Visible = value

            If flpTermCondition.Controls.Count > 0 Then
                For Each ctl As UCByCaseQuotationTermConditionsItem In flpTermCondition.Controls
                    ctl.EnableRemarkItem = value
                Next
            End If
        End Set
    End Property


    Public Sub AddConditionItem()

        Dim ctl As New UCByCaseQuotationTermConditionsItem
        ctl.SetDDlTermCondition()
        AddHandler ctl.DeleteConditionItem, AddressOf btnDeleteCondition_Click
        flpTermCondition.Controls.Add(ctl)

        ExpandRemarkForm(ctl)
    End Sub

    Private Sub ExpandRemarkForm(ctl As UCByCaseQuotationTermConditionsItem)
        'If ctl.Height * flpTermCondition.Controls.Count > flpTermCondition.Height Then
        '    Dim plusHeight As Integer = ctl.Height

        '    flpTermCondition.Height += plusHeight
        '    Me.Height += plusHeight

        '    RaiseEvent HeightChanged(plusHeight)
        'End If
    End Sub


    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If flpTermCondition.Controls.Count > 0 Then
            For Each rm As UCByCaseQuotationTermConditionsItem In flpTermCondition.Controls
                If rm.cbbRemarks.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Term & Condition", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    rm.cbbRemarks.Focus()
                    Return False
                End If
            Next
        End If
        Return ret
    End Function

    Public Sub FillInDefaultRemark()
        Dim sql As String = " select id qt_category_remark_id "
        sql += " from QT_CATEGORY_REMARK"
        sql += " where user_by_case_qt='Y' and default_bycase='Y'"
        FillInRemarkList(sql, Nothing)
    End Sub

    Private Sub FillInRemarkList(sql As String, p As SqlClient.SqlParameter())
        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim uc As New UCByCaseQuotationTermConditionsItem
                uc.SetDDlTermCondition()
                uc.cbbRemarks.SelectedValue = dr("qt_category_remark_id")

                AddHandler uc.DeleteConditionItem, AddressOf btnDeleteCondition_Click
                flpTermCondition.Controls.Add(uc)

                ExpandRemarkForm(uc)
            Next
        End If
        dt.Dispose()
    End Sub

    Public Sub FillInRemarkData(ByCaseQuotationDetailID As Long)
        flpTermCondition.Controls.Clear()

        Dim sql As String = " select qt_category_remark_id from QUOTATION_BYCASE_CONDITION"
        sql += " where quotation_bycase_id=@_BYCASE_QUOTATION_ID"
        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_BYCASE_QUOTATION_ID", ByCaseQuotationDetailID)

        FillInRemarkList(sql, p)
    End Sub

    Private Sub btnAddRemark_Click(sender As System.Object, e As System.EventArgs) Handles btnAddRemark.Click
        AddConditionItem()
    End Sub

    Private Sub btnDeleteCondition_Click(ctl As UCByCaseQuotationTermConditionsItem)
        Dim ctlHeight As Integer = ctl.Height
        flpTermCondition.Controls.Remove(ctl)

        'flpTermCondition.Height -= ctlHeight
        'Me.Height -= ctlHeight

        'RaiseEvent HeightChanged(ctlHeight * -1)
    End Sub

    
End Class

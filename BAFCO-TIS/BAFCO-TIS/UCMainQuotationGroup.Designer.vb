﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCMainQuotationGroup
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtSubjectRemark = New System.Windows.Forms.TextBox()
        Me.lblLabelRemarkSubject = New System.Windows.Forms.Label()
        Me.txtFooterNote = New System.Windows.Forms.TextBox()
        Me.lblLabelNote = New System.Windows.Forms.Label()
        Me.txtHeaderDetail = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.flpSubGroup = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAddSubGroup = New System.Windows.Forms.Button()
        Me.cbbQTGroup = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.UcMainQuotationRemark1 = New BAFCO_TIS.UCMainQuotationRemark()
        Me.UcMainQuotationDescription1 = New BAFCO_TIS.UCMainQuotationDescription()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.txtSubjectRemark)
        Me.Panel1.Controls.Add(Me.lblLabelRemarkSubject)
        Me.Panel1.Controls.Add(Me.txtFooterNote)
        Me.Panel1.Controls.Add(Me.lblLabelNote)
        Me.Panel1.Controls.Add(Me.txtHeaderDetail)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.flpSubGroup)
        Me.Panel1.Controls.Add(Me.btnAddSubGroup)
        Me.Panel1.Controls.Add(Me.UcMainQuotationRemark1)
        Me.Panel1.Controls.Add(Me.UcMainQuotationDescription1)
        Me.Panel1.Controls.Add(Me.cbbQTGroup)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(13, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(783, 187)
        Me.Panel1.TabIndex = 68
        '
        'txtSubjectRemark
        '
        Me.txtSubjectRemark.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtSubjectRemark.Location = New System.Drawing.Point(73, 101)
        Me.txtSubjectRemark.MaxLength = 100
        Me.txtSubjectRemark.Name = "txtSubjectRemark"
        Me.txtSubjectRemark.Size = New System.Drawing.Size(675, 20)
        Me.txtSubjectRemark.TabIndex = 3
        '
        'lblLabelRemarkSubject
        '
        Me.lblLabelRemarkSubject.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblLabelRemarkSubject.ForeColor = System.Drawing.Color.White
        Me.lblLabelRemarkSubject.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLabelRemarkSubject.Location = New System.Drawing.Point(15, 99)
        Me.lblLabelRemarkSubject.Name = "lblLabelRemarkSubject"
        Me.lblLabelRemarkSubject.Size = New System.Drawing.Size(62, 22)
        Me.lblLabelRemarkSubject.TabIndex = 134
        Me.lblLabelRemarkSubject.Text = "REMARK"
        Me.lblLabelRemarkSubject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFooterNote
        '
        Me.txtFooterNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtFooterNote.Location = New System.Drawing.Point(73, 78)
        Me.txtFooterNote.MaxLength = 100
        Me.txtFooterNote.Name = "txtFooterNote"
        Me.txtFooterNote.Size = New System.Drawing.Size(675, 20)
        Me.txtFooterNote.TabIndex = 2
        '
        'lblLabelNote
        '
        Me.lblLabelNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblLabelNote.ForeColor = System.Drawing.Color.White
        Me.lblLabelNote.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLabelNote.Location = New System.Drawing.Point(15, 76)
        Me.lblLabelNote.Name = "lblLabelNote"
        Me.lblLabelNote.Size = New System.Drawing.Size(49, 22)
        Me.lblLabelNote.TabIndex = 132
        Me.lblLabelNote.Text = "NOTE"
        Me.lblLabelNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHeaderDetail
        '
        Me.txtHeaderDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtHeaderDetail.Location = New System.Drawing.Point(73, 26)
        Me.txtHeaderDetail.MaxLength = 100
        Me.txtHeaderDetail.Name = "txtHeaderDetail"
        Me.txtHeaderDetail.Size = New System.Drawing.Size(675, 20)
        Me.txtHeaderDetail.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label2.Location = New System.Drawing.Point(12, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 22)
        Me.Label2.TabIndex = 128
        Me.Label2.Text = "DETAIL"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Image = Global.BAFCO_TIS.My.Resources.Resources.close
        Me.btnClose.Location = New System.Drawing.Point(760, 4)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(20, 20)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 84
        Me.btnClose.TabStop = False
        '
        'flpSubGroup
        '
        Me.flpSubGroup.Location = New System.Drawing.Point(3, 155)
        Me.flpSubGroup.Margin = New System.Windows.Forms.Padding(0)
        Me.flpSubGroup.Name = "flpSubGroup"
        Me.flpSubGroup.Size = New System.Drawing.Size(775, 0)
        Me.flpSubGroup.TabIndex = 121
        '
        'btnAddSubGroup
        '
        Me.btnAddSubGroup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddSubGroup.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnAddSubGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnAddSubGroup.ForeColor = System.Drawing.Color.White
        Me.btnAddSubGroup.Location = New System.Drawing.Point(0, 154)
        Me.btnAddSubGroup.Name = "btnAddSubGroup"
        Me.btnAddSubGroup.Size = New System.Drawing.Size(114, 32)
        Me.btnAddSubGroup.TabIndex = 120
        Me.btnAddSubGroup.Text = "Add Sub Group"
        Me.btnAddSubGroup.UseVisualStyleBackColor = False
        '
        'cbbQTGroup
        '
        Me.cbbQTGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbQTGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbQTGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.cbbQTGroup.Location = New System.Drawing.Point(73, 3)
        Me.cbbQTGroup.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbQTGroup.Name = "cbbQTGroup"
        Me.cbbQTGroup.Size = New System.Drawing.Size(675, 21)
        Me.cbbQTGroup.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.Location = New System.Drawing.Point(12, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 20)
        Me.Label1.TabIndex = 67
        Me.Label1.Text = "GROUP"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'UcMainQuotationRemark1
        '
        Me.UcMainQuotationRemark1.BackColor = System.Drawing.Color.SteelBlue
        Me.UcMainQuotationRemark1.Location = New System.Drawing.Point(73, 124)
        Me.UcMainQuotationRemark1.Name = "UcMainQuotationRemark1"
        Me.UcMainQuotationRemark1.Size = New System.Drawing.Size(678, 28)
        Me.UcMainQuotationRemark1.TabIndex = 82
        '
        'UcMainQuotationDescription1
        '
        Me.UcMainQuotationDescription1.BackColor = System.Drawing.Color.Silver
        Me.UcMainQuotationDescription1.Location = New System.Drawing.Point(73, 48)
        Me.UcMainQuotationDescription1.Name = "UcMainQuotationDescription1"
        Me.UcMainQuotationDescription1.Size = New System.Drawing.Size(675, 28)
        Me.UcMainQuotationDescription1.TabIndex = 81
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Red
        Me.Label25.Location = New System.Drawing.Point(4, 7)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(12, 13)
        Me.Label25.TabIndex = 134
        Me.Label25.Text = "*"
        '
        'UCMainQuotationGroup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(1)
        Me.Name = "UCMainQuotationGroup"
        Me.Size = New System.Drawing.Size(799, 191)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbbQTGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents UcMainQuotationDescription1 As BAFCO_TIS.UCMainQuotationDescription
    Friend WithEvents UcMainQuotationRemark1 As BAFCO_TIS.UCMainQuotationRemark
    Friend WithEvents btnAddSubGroup As System.Windows.Forms.Button
    Friend WithEvents flpSubGroup As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents txtHeaderDetail As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSubjectRemark As System.Windows.Forms.TextBox
    Friend WithEvents lblLabelRemarkSubject As System.Windows.Forms.Label
    Friend WithEvents txtFooterNote As System.Windows.Forms.TextBox
    Friend WithEvents lblLabelNote As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label

End Class

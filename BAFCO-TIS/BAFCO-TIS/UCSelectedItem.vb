﻿Public Class UCSelectedItem

    Public Shared Event btnDeleteCusClick(id As String)
    Public Shared Event btnDeleteSubClick(id As String)

    Public Property strID As String
        Get
            Return lblid.Text
        End Get
        Set(value As String)
            lblid.Text = value
        End Set
    End Property

    Public Property strName As String
        Get
            Return lblName.Text
        End Get
        Set(value As String)
            lblName.Text = value
            Dim tt As New ToolTip
            tt.SetToolTip(lblName, value)
        End Set
    End Property

    Dim _type As String
    Public WriteOnly Property type As String      
        Set(value As String)
            _type = value
        End Set
    End Property

    Private Sub pbClose_Click(sender As System.Object, e As System.EventArgs) Handles pbClose.Click
        If _type = "sup" Then
            RaiseEvent btnDeleteSubClick(lblid.Text)
        Else
            RaiseEvent btnDeleteCusClick(lblid.Text)
        End If

    End Sub

End Class

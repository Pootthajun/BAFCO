﻿Imports System.Data.SqlClient

Public Class UCMainQuotationTruck

    Public Event HeightChanged(h As Integer)

    Public WriteOnly Property EnableDescription As Boolean
        Set(value As Boolean)
            btnAddDescription.Visible = value

            If flpTruckDescription.Controls.Count > 0 Then
                For Each ctl As UCByCaseQuotationTruckDescriptionItem In flpTruckDescription.Controls
                    ctl.EnableDescriptionItem = value
                Next
            End If
        End Set
    End Property


    Public Sub AddTruckDescriptionItem()
        Dim uc As New UCMainQuotationTruckItem
        uc.SetDDL()
        AddHandler uc.DeleteTruckItem, AddressOf UCMainQuotationTruckItem_DeleteTruckItem

        flpTruckDescription.Controls.Add(uc)
    End Sub


    Public Sub ClearTruckDescriptionItem()
        flpTruckDescription.Controls.Clear()
    End Sub

    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If flpTruckDescription.Controls.Count > 0 Then
            For Each des As UCMainQuotationTruckItem In flpTruckDescription.Controls
                If des.cbbRouteFrom.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก From", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbRouteFrom.Focus()
                    Return False
                End If

                If des.cbbRouteTo.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก To", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbRouteTo.Focus()
                    Return False
                End If

                If des.txt4w.Text = "" Then
                    MessageBox.Show("กรุณาระบุ 4 Wheel", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txt4w.Focus()
                    Return False
                End If

                If des.txt4ws.Text = "" Then
                    MessageBox.Show("กรุณาระบุ 4 Wheel Special", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txt4ws.Focus()
                    Return False
                End If

                If des.txt6w.Text = "" Then
                    MessageBox.Show("กรุณาระบุ 6 Wheel", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txt6w.Focus()
                    Return False
                End If

                If des.txt6ws.Text = "" Then
                    MessageBox.Show("กรุณาระบุ 6 Wheel Special", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txt6ws.Focus()
                    Return False
                End If

                If des.txt10w.Text = "" Then
                    MessageBox.Show("กรุณาระบุ 10 Wheel", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txt10w.Focus()
                    Return False
                End If
                If des.txt20FtTrailer.Text = "" Then
                    MessageBox.Show("กรุณาระบุ 20 FT Trailer", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txt20FtTrailer.Focus()
                    Return False
                End If
                If des.txt40FtTrailer.Text = "" Then
                    MessageBox.Show("กรุณาระบุ 40 FT Trailer", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txt40FtTrailer.Focus()
                    Return False
                End If
                If des.txtLowBedTrailer2Axel.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Lowbed Trailer 2 Axel", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtLowBedTrailer2Axel.Focus()
                    Return False
                End If
                If des.txtLowBedTrailer3Axel.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Lowbed Trailer 3 Axel", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtLowBedTrailer3Axel.Focus()
                    Return False
                End If
                If des.txtAirSuspension.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Air Suspension", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtAirSuspension.Focus()
                    Return False
                End If
            Next
        End If
        Return ret
    End Function

    Public Function SaveMainQuotationTruckData(MainQuotationDetailID As Long, trans As SqlTransaction) As String
        Dim ret As String = "false"
        For Each des As UCMainQuotationTruckItem In flpTruckDescription.Controls
            Dim sql As String = ""
            sql = "insert into QUOTATION_MAIN_TRUCK_ITEM(created_by,created_date, quotation_main_detail_id, route_id_from, route_id_to,"
            sql += " t4w, t4ws, t6w, t6ws, t10w, ft20Trailer, ft40Trailer, Lowbed2Axel, Lowbed3Axel, AirSuspension, item_note)"
            sql += " values(@_CREATED_BY, getdate(), @_QUOTATION_MAIN_DETAIL_ID, @_ROUTE_FROM, @_ROUTE_TO, "
            sql += " @_T4W, @_T4WS, @_T6W, @_T6WS, @_T10W, @_FT20TRAILER, @_FT40TRAILER, @_LOWBED2AXEL, @_LOWBED3AXEL, @_AIRSUSPENSION, @_ITEM_NOTE)"

            Dim p(15) As SqlParameter
            p(0) = SetText("@_CREATED_BY", myUser.fulllname)
            p(1) = SetBigInt("@_QUOTATION_MAIN_DETAIL_ID", MainQuotationDetailID)
            p(2) = SetInt("@_ROUTE_FROM", des.cbbRouteFrom.SelectedValue)
            p(3) = SetInt("@_ROUTE_TO", des.cbbRouteTo.SelectedValue)
            p(4) = SetDouble("@_T4W", des.txt4w.Text)
            p(5) = SetDouble("@_T4WS", des.txt4ws.Text)
            p(6) = SetDouble("@_T6W", des.txt6w.Text)
            p(7) = SetDouble("@_T6WS", des.txt6ws.Text)
            p(8) = SetDouble("@_T10W", des.txt10w.Text)
            p(9) = SetDouble("@_FT20TRAILER", des.txt20FtTrailer.Text)
            p(10) = SetDouble("@_FT40TRAILER", des.txt40FtTrailer.Text)
            p(11) = SetDouble("@_LOWBED2AXEL", des.txtLowBedTrailer2Axel.Text)
            p(12) = SetDouble("@_LOWBED3AXEL", des.txtLowBedTrailer3Axel.Text)
            p(13) = SetDouble("@_AIRSUSPENSION", des.txtAirSuspension.Text)
            p(14) = SetText("@_ITEM_NOTE", des.lblNote.Text)

            ret = Execute_Command(sql, trans, p)
            If ret = "false" Then
                Exit For
            End If
        Next

        Return ret
    End Function


    Public Sub FillInTruckDescriptionData(MainQuotationDetailID As Long)
        Dim Sql As String = " select id,route_id_from,route_id_to, t4w, t4ws, t6w, t6ws,t10w, ft20Trailer,  ft40Trailer, Lowbed2Axel,"
        Sql += " Lowbed3Axel,  AirSuspension, item_note"
        Sql += " from QUOTATION_MAIN_TRUCK_ITEM "
        Sql += " where quotation_main_detail_id=@_QUOTATION_MAIN_DETAIL_ID"
        Sql += " order by id "

        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_QUOTATION_MAIN_DETAIL_ID", MainQuotationDetailID)
        Dim dt As DataTable = Execute_DataTable(Sql, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim uc As New UCMainQuotationTruckItem
                uc.SetDDL()
                uc.cbbRouteFrom.SelectedValue = dr("route_id_from")
                uc.cbbRouteTo.SelectedValue = dr("route_id_to")
                uc.txt4w.Text = dr("t4w")
                uc.txt4ws.Text = dr("t4ws")
                uc.txt6w.Text = dr("t6w")
                uc.txt6ws.Text = dr("t6ws")
                uc.txt10w.Text = dr("t10w")
                uc.txt20FtTrailer.Text = dr("ft20Trailer")
                uc.txt40FtTrailer.Text = dr("ft40Trailer")
                uc.txtLowBedTrailer2Axel.Text = dr("Lowbed2Axel")
                uc.txtLowBedTrailer3Axel.Text = dr("Lowbed3Axel")
                uc.txtAirSuspension.Text = dr("AirSuspension")
                If Convert.IsDBNull(dr("item_note")) = False Then uc.lblNote.Text = dr("item_note")

                AddHandler uc.DeleteTruckItem, AddressOf UCMainQuotationTruckItem_DeleteTruckItem

                flpTruckDescription.Controls.Add(uc)
            Next
        End If
        dt.Dispose()

    End Sub

    Private Sub btnAddDescription_Click(sender As System.Object, e As System.EventArgs) Handles btnAddDescription.Click
        AddTruckDescriptionItem()
    End Sub

    Private Sub UCMainQuotationTruckItem_DeleteTruckItem(ctl As UCMainQuotationTruckItem)
        Dim plusHeight As Integer = ctl.Height

        flpTruckDescription.Controls.Remove(ctl)
    End Sub
End Class

﻿Public Class UCMainQuotationGroup
    Dim _QTCategory As Long = 0

    Public Event GroupHeightChange(h As Integer)
    Public Event CloseGroup(ctl As UCMainQuotationGroup)
    
    Public WriteOnly Property EnableGroup As Boolean
        Set(value As Boolean)
            cbbQTGroup.Enabled = value
            UcMainQuotationDescription1.EnableDescription = value
            'UcMainQuotationRemark1.EnableRemark = value
            btnClose.Visible = value
            btnAddSubGroup.Visible = value
            txtHeaderDetail.Enabled = value
            'txtFooterNote.Enabled = value
            'txtSubjectRemark.Enabled = value
            If value = False Then
                Panel1.Height -= btnAddSubGroup.Height
                Me.Height -= btnAddSubGroup.Height
                RaiseEvent GroupHeightChange(btnAddSubGroup.Height * -1)
            End If

            For Each ctl As UCMainQuotationSubGroup In flpSubGroup.Controls
                ctl.EnableSubGroup = value
            Next
        End Set
    End Property

    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If cbbQTGroup.SelectedValue > 0 Then
            If UcMainQuotationDescription1.ValidateData = False Then
                Return False
            End If
            If UcMainQuotationRemark1.ValidateData = False Then
                Return False
            End If

            For Each uc As UCMainQuotationSubGroup In flpSubGroup.Controls
                If uc.ValidateData = False Then
                    Return False
                End If
            Next
        End If
        Return ret
    End Function

    Public Sub SetDDLGroup(QTCategoryID As Long)
        Dim GrpDt As DataTable
        If QTCategoryID <> 0 Then
            Dim sql As String = " select id, group_name "
            sql += " from QT_GROUP "
            sql += " where active_status='Y'"
            sql += " and qt_category_id=@_CATEGORY_ID"
            sql += " order by group_name"

            Dim p(1) As SqlClient.SqlParameter
            p(0) = SetBigInt("@_CATEGORY_ID", QTCategoryID)

            GrpDt = Execute_DataTable(sql, p)

            UcMainQuotationDescription1.QTCategoryID = QTCategoryID
            UcMainQuotationRemark1.QTCategoryID = QTCategoryID

            _QTCategory = QTCategoryID
        End If

        If GrpDt Is Nothing Then
            GrpDt = New DataTable
            GrpDt.Columns.Add("id")
            GrpDt.Columns.Add("group_name")
        End If

        Dim dr As DataRow = GrpDt.NewRow
        dr("id") = 0
        dr("group_name") = ""
        GrpDt.Rows.InsertAt(dr, 0)

        cbbQTGroup.DataSource = GrpDt
        cbbQTGroup.DisplayMember = "group_name"
        cbbQTGroup.ValueMember = "id"
        cbbQTGroup.SelectedValue = 0
    End Sub

    Public Function SaveMainQuotationGroup(CusID As Integer, QtDate As Date, QtNo As String, QtStatus As String, MainQuotationID As Long, trans As SqlClient.SqlTransaction) As String
        Dim ret As String = "false"
        Try
            If cbbQTGroup.SelectedValue > 0 Then
                Dim sql As String = ""
                sql = " insert into quotation_main_detail(created_by,created_date,quotation_main_id,"
                sql += " qt_category_id, qt_group_id, qt_group_sub_id,header_detail,footer_detail,subject_remark )"
                sql += " output inserted.id "
                sql += " values(@_CREATED_BY, getdate(), @_QUOTATION_MAIN_ID, @_QT_CATEGORY_ID, @_QT_GROUP_ID, @_QT_GROUP_SUB_ID, "
                sql += " @_HEADER_DETAIL, @_FOOTER_DETAIL, @_SUBJECT_REMARK)"

                Dim p(8) As SqlClient.SqlParameter
                p(0) = SetText("@_CREATED_BY", myUser.fulllname)
                p(1) = SetBigInt("@_QUOTATION_MAIN_ID", MainQuotationID)
                p(2) = SetBigInt("@_QT_CATEGORY_ID", _QTCategory)
                p(3) = SetBigInt("@_QT_GROUP_ID", cbbQTGroup.SelectedValue)
                p(4) = SetBigInt("@_QT_GROUP_SUB_ID", 0)
                p(5) = SetText("@_HEADER_DETAIL", txtHeaderDetail.Text)
                p(6) = SetText("@_FOOTER_DETAIL", txtFooterNote.Text)
                p(7) = SetText("@_SUBJECT_REMARK", txtSubjectRemark.Text)

                Dim dt As DataTable = Execute_DataTable(sql, trans, p)
                If dt.Rows.Count > 0 Then
                    Dim _qtd_id As Long = Convert.ToInt64(dt.Rows(0)("id"))
                    'Group Description
                    If UcMainQuotationDescription1.flpDescription.Controls.Count > 0 Then
                        ret = UcMainQuotationDescription1.SaveMainQuotationDescription(CusID, QtDate, _qtd_id, QtNo, QtStatus, trans)
                    Else
                        ret = "true"
                    End If

                    If ret = "true" Then
                        'Group Remark
                        If UcMainQuotationRemark1.flpRemark.Controls.Count > 0 Then
                            ret = UcMainQuotationRemark1.SaveMainQuotationRemarks(_qtd_id, trans)
                        End If

                        For Each sgQt As UCMainQuotationSubGroup In flpSubGroup.Controls
                            ret = sgQt.SaveMainQuotationSubGroup(CusID, QtDate, QtNo, QtStatus, MainQuotationID, trans)
                            If ret = "false" Then
                                Exit For
                            End If
                        Next
                    End If
                End If
                dt.Dispose()
            Else
                ret = "true"
            End If
        Catch ex As Exception
            ret = "false|" & ex.Message
        End Try
        Return ret
    End Function

    Public Sub FillGroupData(MainQuotationID As Long, MainQuotationDetailID As Long, QTGroupID As Long, HeaderDetail As String, FooterDetail As String, SubjectRemark As String)
        cbbQTGroup.SelectedValue = QTGroupID
        txtHeaderDetail.Text = HeaderDetail
        txtFooterNote.Text = FooterDetail
        txtSubjectRemark.Text = SubjectRemark
        UcMainQuotationDescription1.FillInDescriptionData(MainQuotationDetailID)
        UcMainQuotationRemark1.FillInRemarkData(MainQuotationDetailID)

        Dim sql As String = "select id, quotation_main_id,qt_category_id,qt_group_id, qt_group_sub_id, header_detail, footer_detail, subject_remark "
        sql += " from quotation_main_detail qmd "
        sql += " where quotation_main_id=@_QUOTATION_MAIN_ID and qt_category_id=@_QT_CATEGORY_ID "
        sql += " and qt_group_id=@_QT_GROUP_ID and qt_group_sub_id<>0 "
        Dim p(3) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_QUOTATION_MAIN_ID", MainQuotationID)
        p(1) = SetBigInt("@_QT_CATEGORY_ID", _QTCategory)
        p(2) = SetBigInt("@_QT_GROUP_ID", QTGroupID)

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt.Rows.Count > 0 Then
            flpSubGroup.Controls.Clear()

            For Each dr As DataRow In dt.Rows
                Dim SubGroupHeaderDetail As String = ""
                Dim SubGroupFooterDetail As String = ""
                Dim SubGroupSubjectRemark As String = ""
                If Convert.IsDBNull(dr("header_detail")) = False Then SubGroupHeaderDetail = dr("header_detail")
                If Convert.IsDBNull(dr("footer_detail")) = False Then SubGroupFooterDetail = dr("footer_detail")
                If Convert.IsDBNull(dr("subject_remark")) = False Then SubGroupSubjectRemark = dr("subject_remark")

                Dim uc As New UCMainQuotationSubGroup
                uc.SetDDLSubGroup(cbbQTGroup.SelectedValue, _QTCategory)
                uc.FillInSubGroupData(dr("id"), dr("qt_group_sub_id"), SubGroupHeaderDetail, SubGroupFooterDetail, SubGroupSubjectRemark)

                flpSubGroup.Height += uc.Height
                Panel1.Height += uc.Height
                Me.Height += uc.Height
                AddHandler uc.SubGroupHeightChange, AddressOf SubGroupHeightChange
                AddHandler uc.CloseSubGroup, AddressOf UCMainQuotationSubGroup_CloseSubGroup

                RaiseEvent GroupHeightChange(uc.Height)

                flpSubGroup.Controls.Add(uc)
            Next
        End If
        dt.Dispose()
    End Sub

    Private Sub UcMainQuotationDescription1_HeightChanged(h As Integer) Handles UcMainQuotationDescription1.HeightChanged
        lblLabelNote.Top += h
        lblLabelRemarkSubject.Top += h
        txtFooterNote.Top += h
        txtSubjectRemark.Top += h

        UcMainQuotationRemark1.Top += h
        flpSubGroup.Top += h
        Panel1.Height += h
        Me.Height += h

        RaiseEvent GroupHeightChange(h)
    End Sub

    Private Sub UcMainQuotationRemark1_HeightChanged(h As Integer) Handles UcMainQuotationRemark1.HeightChanged
        flpSubGroup.Top += h
        Panel1.Height += h
        Me.Height += h

        RaiseEvent GroupHeightChange(h)
    End Sub

    Private Sub btnAddSubGroup_Click(sender As System.Object, e As System.EventArgs) Handles btnAddSubGroup.Click
        If cbbQTGroup.SelectedValue > 0 Then
            Dim uc As New UCMainQuotationSubGroup
            uc.SetDDLSubGroup(cbbQTGroup.SelectedValue, _QTCategory)

            flpSubGroup.Height += uc.Height
            Panel1.Height += uc.Height
            Me.Height += uc.Height
            AddHandler uc.SubGroupHeightChange, AddressOf SubGroupHeightChange
            AddHandler uc.CloseSubGroup, AddressOf UCMainQuotationSubGroup_CloseSubGroup

            RaiseEvent GroupHeightChange(uc.Height)

            flpSubGroup.Controls.Add(uc)
        End If
    End Sub

    Public Sub UCMainQuotationSubGroup_CloseSubGroup(ctl As UCMainQuotationSubGroup)
        Dim plusHeight As Integer = ctl.Height

        flpSubGroup.Controls.Remove(ctl)

        flpSubGroup.Height -= plusHeight
        Panel1.Height -= plusHeight
        Me.Height -= plusHeight

        RaiseEvent GroupHeightChange(plusHeight * -1)
    End Sub

    Private Sub SubGroupHeightChange(h As Integer)
        'btnAddSubGroup.Top += h
        flpSubGroup.Height += h
        Panel1.Height += h
        Me.Height += h

        RaiseEvent GroupHeightChange(h)
    End Sub

    Public Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        RaiseEvent CloseGroup(Me)
    End Sub
End Class

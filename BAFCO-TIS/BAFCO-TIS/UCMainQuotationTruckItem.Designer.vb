﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCMainQuotationTruckItem
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbbRouteFrom = New System.Windows.Forms.ComboBox()
        Me.txtAirSuspension = New System.Windows.Forms.TextBox()
        Me.txtLowBedTrailer3Axel = New System.Windows.Forms.TextBox()
        Me.txt4w = New System.Windows.Forms.TextBox()
        Me.lblNote = New System.Windows.Forms.Label()
        Me.ttpNote = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnNote = New System.Windows.Forms.Panel()
        Me.btnDelete = New System.Windows.Forms.Panel()
        Me.cbbRouteTo = New System.Windows.Forms.ComboBox()
        Me.txt4ws = New System.Windows.Forms.TextBox()
        Me.txt6w = New System.Windows.Forms.TextBox()
        Me.txt6ws = New System.Windows.Forms.TextBox()
        Me.txt20FtTrailer = New System.Windows.Forms.TextBox()
        Me.txt40FtTrailer = New System.Windows.Forms.TextBox()
        Me.txtLowBedTrailer2Axel = New System.Windows.Forms.TextBox()
        Me.txt10w = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'cbbRouteFrom
        '
        Me.cbbRouteFrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbRouteFrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbRouteFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbRouteFrom.Location = New System.Drawing.Point(0, 0)
        Me.cbbRouteFrom.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbRouteFrom.Name = "cbbRouteFrom"
        Me.cbbRouteFrom.Size = New System.Drawing.Size(140, 24)
        Me.cbbRouteFrom.TabIndex = 0
        '
        'txtAirSuspension
        '
        Me.txtAirSuspension.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtAirSuspension.Location = New System.Drawing.Point(849, 0)
        Me.txtAirSuspension.MaxLength = 100
        Me.txtAirSuspension.Name = "txtAirSuspension"
        Me.txtAirSuspension.Size = New System.Drawing.Size(80, 24)
        Me.txtAirSuspension.TabIndex = 6
        Me.txtAirSuspension.Text = "0"
        Me.txtAirSuspension.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLowBedTrailer3Axel
        '
        Me.txtLowBedTrailer3Axel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLowBedTrailer3Axel.Location = New System.Drawing.Point(765, 0)
        Me.txtLowBedTrailer3Axel.MaxLength = 100
        Me.txtLowBedTrailer3Axel.Name = "txtLowBedTrailer3Axel"
        Me.txtLowBedTrailer3Axel.Size = New System.Drawing.Size(85, 24)
        Me.txtLowBedTrailer3Axel.TabIndex = 5
        Me.txtLowBedTrailer3Axel.Text = "0"
        Me.txtLowBedTrailer3Axel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt4w
        '
        Me.txt4w.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4w.Location = New System.Drawing.Point(278, 0)
        Me.txt4w.MaxLength = 100
        Me.txt4w.Name = "txt4w"
        Me.txt4w.Size = New System.Drawing.Size(50, 24)
        Me.txt4w.TabIndex = 4
        Me.txt4w.Text = "0"
        Me.txt4w.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNote
        '
        Me.lblNote.AutoSize = True
        Me.lblNote.Location = New System.Drawing.Point(930, 6)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(0, 13)
        Me.lblNote.TabIndex = 7
        Me.lblNote.Visible = False
        '
        'ttpNote
        '
        Me.ttpNote.AutoPopDelay = 5000
        Me.ttpNote.InitialDelay = 200
        Me.ttpNote.ReshowDelay = 100
        '
        'btnNote
        '
        Me.btnNote.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.BlankNote
        Me.btnNote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNote.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNote.Location = New System.Drawing.Point(929, 2)
        Me.btnNote.Name = "btnNote"
        Me.btnNote.Size = New System.Drawing.Size(20, 20)
        Me.btnNote.TabIndex = 5
        Me.ttpNote.SetToolTip(Me.btnNote, "Note")
        '
        'btnDelete
        '
        Me.btnDelete.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.Cancel
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Location = New System.Drawing.Point(950, 2)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(20, 20)
        Me.btnDelete.TabIndex = 4
        Me.ttpNote.SetToolTip(Me.btnDelete, "Remove")
        '
        'cbbRouteTo
        '
        Me.cbbRouteTo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbRouteTo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbRouteTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbRouteTo.Location = New System.Drawing.Point(139, 0)
        Me.cbbRouteTo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbRouteTo.Name = "cbbRouteTo"
        Me.cbbRouteTo.Size = New System.Drawing.Size(140, 24)
        Me.cbbRouteTo.TabIndex = 1
        '
        'txt4ws
        '
        Me.txt4ws.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt4ws.Location = New System.Drawing.Point(327, 0)
        Me.txt4ws.MaxLength = 100
        Me.txt4ws.Name = "txt4ws"
        Me.txt4ws.Size = New System.Drawing.Size(55, 24)
        Me.txt4ws.TabIndex = 8
        Me.txt4ws.Text = "0"
        Me.txt4ws.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt6w
        '
        Me.txt6w.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6w.Location = New System.Drawing.Point(381, 0)
        Me.txt6w.MaxLength = 100
        Me.txt6w.Name = "txt6w"
        Me.txt6w.Size = New System.Drawing.Size(50, 24)
        Me.txt6w.TabIndex = 9
        Me.txt6w.Text = "0"
        Me.txt6w.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt6ws
        '
        Me.txt6ws.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt6ws.Location = New System.Drawing.Point(430, 0)
        Me.txt6ws.MaxLength = 100
        Me.txt6ws.Name = "txt6ws"
        Me.txt6ws.Size = New System.Drawing.Size(55, 24)
        Me.txt6ws.TabIndex = 10
        Me.txt6ws.Text = "0"
        Me.txt6ws.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt20FtTrailer
        '
        Me.txt20FtTrailer.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt20FtTrailer.Location = New System.Drawing.Point(533, 0)
        Me.txt20FtTrailer.MaxLength = 100
        Me.txt20FtTrailer.Name = "txt20FtTrailer"
        Me.txt20FtTrailer.Size = New System.Drawing.Size(75, 24)
        Me.txt20FtTrailer.TabIndex = 11
        Me.txt20FtTrailer.Text = "0"
        Me.txt20FtTrailer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt40FtTrailer
        '
        Me.txt40FtTrailer.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt40FtTrailer.Location = New System.Drawing.Point(607, 0)
        Me.txt40FtTrailer.MaxLength = 100
        Me.txt40FtTrailer.Name = "txt40FtTrailer"
        Me.txt40FtTrailer.Size = New System.Drawing.Size(75, 24)
        Me.txt40FtTrailer.TabIndex = 12
        Me.txt40FtTrailer.Text = "0"
        Me.txt40FtTrailer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLowBedTrailer2Axel
        '
        Me.txtLowBedTrailer2Axel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtLowBedTrailer2Axel.Location = New System.Drawing.Point(681, 0)
        Me.txtLowBedTrailer2Axel.MaxLength = 100
        Me.txtLowBedTrailer2Axel.Name = "txtLowBedTrailer2Axel"
        Me.txtLowBedTrailer2Axel.Size = New System.Drawing.Size(85, 24)
        Me.txtLowBedTrailer2Axel.TabIndex = 13
        Me.txtLowBedTrailer2Axel.Text = "0"
        Me.txtLowBedTrailer2Axel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txt10w
        '
        Me.txt10w.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txt10w.Location = New System.Drawing.Point(484, 0)
        Me.txt10w.MaxLength = 100
        Me.txt10w.Name = "txt10w"
        Me.txt10w.Size = New System.Drawing.Size(50, 24)
        Me.txt10w.TabIndex = 14
        Me.txt10w.Text = "0"
        Me.txt10w.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'UCMainQuotationTruckItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.txt10w)
        Me.Controls.Add(Me.txtLowBedTrailer2Axel)
        Me.Controls.Add(Me.txt40FtTrailer)
        Me.Controls.Add(Me.txt20FtTrailer)
        Me.Controls.Add(Me.txt6ws)
        Me.Controls.Add(Me.txt6w)
        Me.Controls.Add(Me.txt4ws)
        Me.Controls.Add(Me.cbbRouteTo)
        Me.Controls.Add(Me.lblNote)
        Me.Controls.Add(Me.btnNote)
        Me.Controls.Add(Me.txt4w)
        Me.Controls.Add(Me.txtLowBedTrailer3Axel)
        Me.Controls.Add(Me.txtAirSuspension)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.cbbRouteFrom)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "UCMainQuotationTruckItem"
        Me.Size = New System.Drawing.Size(972, 24)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbbRouteFrom As System.Windows.Forms.ComboBox
    Friend WithEvents btnDelete As System.Windows.Forms.Panel
    Friend WithEvents txtAirSuspension As System.Windows.Forms.TextBox
    Friend WithEvents txtLowBedTrailer3Axel As System.Windows.Forms.TextBox
    Friend WithEvents txt4w As System.Windows.Forms.TextBox
    Friend WithEvents btnNote As System.Windows.Forms.Panel
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents ttpNote As System.Windows.Forms.ToolTip
    Friend WithEvents cbbRouteTo As System.Windows.Forms.ComboBox
    Friend WithEvents txt4ws As System.Windows.Forms.TextBox
    Friend WithEvents txt6w As System.Windows.Forms.TextBox
    Friend WithEvents txt6ws As System.Windows.Forms.TextBox
    Friend WithEvents txt20FtTrailer As System.Windows.Forms.TextBox
    Friend WithEvents txt40FtTrailer As System.Windows.Forms.TextBox
    Friend WithEvents txtLowBedTrailer2Axel As System.Windows.Forms.TextBox
    Friend WithEvents txt10w As System.Windows.Forms.TextBox

End Class

﻿Public Class UCMainQuotationRemarkItem

    Public Event DeleteRemarkItem(ctl As UCMainQuotationRemarkItem)
    Public QTCategoryID As Long = 0

    Public Sub SetDDlRemark(_QTCategoryID As Long)
        QTCategoryID = _QTCategoryID
        Dim sql As String = "select id, remarks "
        sql += " from QT_CATEGORY_REMARK "
        sql += " where active_status='Y' "
        sql += " and qt_category_id=@_CATEGORY_ID"
        sql += " order by remarks"

        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_CATEGORY_ID", QTCategoryID)

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt Is Nothing Then
            dt = New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("remarks")
        End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("remarks") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbRemarks.DataSource = dt
        cbbRemarks.DisplayMember = "remarks"
        cbbRemarks.ValueMember = "id"
        'ddlRemarks.SelectedValue = 0
    End Sub

    Public WriteOnly Property EnableRemarkItem() As Boolean
        Set(value As Boolean)
            cbbRemarks.Enabled = value
            btnDelete.Visible = value
        End Set
    End Property




    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        RaiseEvent DeleteRemarkItem(Me)
    End Sub
End Class

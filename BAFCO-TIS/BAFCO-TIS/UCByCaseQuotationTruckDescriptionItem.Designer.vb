﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCByCaseQuotationTruckDescriptionItem
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbbRouteFrom = New System.Windows.Forms.ComboBox()
        Me.cbbCurrency = New System.Windows.Forms.ComboBox()
        Me.cbbBasis = New System.Windows.Forms.ComboBox()
        Me.txtTotal = New System.Windows.Forms.TextBox()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.btnDelete = New System.Windows.Forms.Panel()
        Me.txtUnitRate = New System.Windows.Forms.TextBox()
        Me.btnNote = New System.Windows.Forms.Panel()
        Me.lblNote = New System.Windows.Forms.Label()
        Me.ttpNote = New System.Windows.Forms.ToolTip(Me.components)
        Me.cbbRouteTo = New System.Windows.Forms.ComboBox()
        Me.cbbVehicleType = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'cbbRouteFrom
        '
        Me.cbbRouteFrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbRouteFrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbRouteFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbRouteFrom.Location = New System.Drawing.Point(0, 0)
        Me.cbbRouteFrom.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbRouteFrom.Name = "cbbRouteFrom"
        Me.cbbRouteFrom.Size = New System.Drawing.Size(180, 24)
        Me.cbbRouteFrom.TabIndex = 0
        '
        'cbbCurrency
        '
        Me.cbbCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbCurrency.Location = New System.Drawing.Point(885, 0)
        Me.cbbCurrency.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbCurrency.Name = "cbbCurrency"
        Me.cbbCurrency.Size = New System.Drawing.Size(80, 24)
        Me.cbbCurrency.TabIndex = 7
        '
        'cbbBasis
        '
        Me.cbbBasis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbBasis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbBasis.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbBasis.Location = New System.Drawing.Point(493, 0)
        Me.cbbBasis.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbBasis.Name = "cbbBasis"
        Me.cbbBasis.Size = New System.Drawing.Size(150, 24)
        Me.cbbBasis.TabIndex = 3
        '
        'txtTotal
        '
        Me.txtTotal.Enabled = False
        Me.txtTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtTotal.Location = New System.Drawing.Point(800, 0)
        Me.txtTotal.MaxLength = 100
        Me.txtTotal.Name = "txtTotal"
        Me.txtTotal.Size = New System.Drawing.Size(86, 24)
        Me.txtTotal.TabIndex = 6
        Me.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtQty
        '
        Me.txtQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtQty.Location = New System.Drawing.Point(741, 0)
        Me.txtQty.MaxLength = 100
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(60, 24)
        Me.txtQty.TabIndex = 5
        Me.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnDelete
        '
        Me.btnDelete.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.Cancel
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Location = New System.Drawing.Point(991, 2)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(20, 20)
        Me.btnDelete.TabIndex = 4
        Me.ttpNote.SetToolTip(Me.btnDelete, "Remove")
        '
        'txtUnitRate
        '
        Me.txtUnitRate.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtUnitRate.Location = New System.Drawing.Point(642, 0)
        Me.txtUnitRate.MaxLength = 100
        Me.txtUnitRate.Name = "txtUnitRate"
        Me.txtUnitRate.Size = New System.Drawing.Size(100, 24)
        Me.txtUnitRate.TabIndex = 4
        Me.txtUnitRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnNote
        '
        Me.btnNote.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.BlankNote
        Me.btnNote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNote.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNote.Location = New System.Drawing.Point(968, 2)
        Me.btnNote.Name = "btnNote"
        Me.btnNote.Size = New System.Drawing.Size(20, 20)
        Me.btnNote.TabIndex = 5
        Me.ttpNote.SetToolTip(Me.btnNote, "Note")
        '
        'lblNote
        '
        Me.lblNote.AutoSize = True
        Me.lblNote.Location = New System.Drawing.Point(926, 6)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(0, 13)
        Me.lblNote.TabIndex = 7
        Me.lblNote.Visible = False
        '
        'ttpNote
        '
        Me.ttpNote.AutoPopDelay = 5000
        Me.ttpNote.InitialDelay = 200
        Me.ttpNote.ReshowDelay = 100
        '
        'cbbRouteTo
        '
        Me.cbbRouteTo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbRouteTo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbRouteTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbRouteTo.Location = New System.Drawing.Point(178, 0)
        Me.cbbRouteTo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbRouteTo.Name = "cbbRouteTo"
        Me.cbbRouteTo.Size = New System.Drawing.Size(180, 24)
        Me.cbbRouteTo.TabIndex = 1
        '
        'cbbVehicleType
        '
        Me.cbbVehicleType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbVehicleType.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbVehicleType.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbVehicleType.Location = New System.Drawing.Point(357, 0)
        Me.cbbVehicleType.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbVehicleType.Name = "cbbVehicleType"
        Me.cbbVehicleType.Size = New System.Drawing.Size(137, 24)
        Me.cbbVehicleType.TabIndex = 2
        '
        'UCByCaseQuotationTruckDescriptionItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cbbVehicleType)
        Me.Controls.Add(Me.cbbRouteTo)
        Me.Controls.Add(Me.lblNote)
        Me.Controls.Add(Me.btnNote)
        Me.Controls.Add(Me.txtUnitRate)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(Me.txtTotal)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.cbbBasis)
        Me.Controls.Add(Me.cbbCurrency)
        Me.Controls.Add(Me.cbbRouteFrom)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "UCByCaseQuotationTruckDescriptionItem"
        Me.Size = New System.Drawing.Size(1015, 24)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbbRouteFrom As System.Windows.Forms.ComboBox
    Friend WithEvents cbbCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents cbbBasis As System.Windows.Forms.ComboBox
    Friend WithEvents btnDelete As System.Windows.Forms.Panel
    Friend WithEvents txtTotal As System.Windows.Forms.TextBox
    Friend WithEvents txtQty As System.Windows.Forms.TextBox
    Friend WithEvents txtUnitRate As System.Windows.Forms.TextBox
    Friend WithEvents btnNote As System.Windows.Forms.Panel
    Friend WithEvents lblNote As System.Windows.Forms.Label
    Friend WithEvents ttpNote As System.Windows.Forms.ToolTip
    Friend WithEvents cbbRouteTo As System.Windows.Forms.ComboBox
    Friend WithEvents cbbVehicleType As System.Windows.Forms.ComboBox

End Class

﻿Imports System.Data.SqlClient
Imports System.Globalization

Public Class frmSearchDialogSupplier

    Public VAR_MODE As String = ""
    Public VAR_S_FROM As Int32 = 0
    Public VAR_S_TO As Int32 = 0
    Public VAR_VEH_ID As Int32 = 0
    Public VAR_SUP_ID As Int32 = 0
    Public VAR_COST_ID As Int32 = 0
    Private WithEvents kbHook As New KeyboardHook

    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If
    End Sub
    Private Sub frmSearchDialogSupplier_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Grid.AutoGenerateColumns = False
        btnPrint.Visible = myUser.view_report

        BindCBB_Route(cbbFrom)
        BindCBB_Route(cbbTo)
        BindCBB_Supplier(cbbSup)
        BindCBB_Vehicle(cbbVeh)

        If VAR_S_FROM > 0 Then
            cbbFrom.SelectedValue = VAR_S_FROM
        End If
        If VAR_S_TO > 0 Then
            cbbTo.SelectedValue = VAR_S_TO
        End If
        If VAR_VEH_ID > 0 Then
            cbbVeh.SelectedValue = VAR_VEH_ID
        End If
        If VAR_SUP_ID > 0 Then
            cbbSup.SelectedValue = VAR_SUP_ID
        End If
        If VAR_COST_ID > 0 Then
            cbbFrom.Enabled = False
            cbbTo.Enabled = False
            cbbVeh.Enabled = False
            cbbSup.Enabled = False
        Else
            btnAdd.Enabled = False
            btnSave.Enabled = False
        End If
        ShowData()
    End Sub

    Sub ShowData()
        Dim SQL As String = ""
        If VAR_MODE = "EDIT" Then
            SQL = "SELECT ID,COST_ID,PRICE,ISNULL(CONVERT(VARCHAR(10),QDATE,103),'N/A') AS QDATE,CASE WHEN REVISE = 0 THEN '' "
            SQL &= " ELSE REVISE END AS REVISE,REMARK,UPDATE_BY,UPDATE_DATE,UNIT CostUnit FROM COST_SUP_REVISE WHERE COST_ID = " & VAR_COST_ID & " ORDER BY CONVERT(Int,REVISE)"
        Else
            SQL = "SELECT ID FROM COST_SUP WHERE "
            Dim Filter As String = ""
            Filter &= "ROUTE_FROM = " & cbbFrom.SelectedValue & " AND "
            Filter &= "ROUTE_TO = " & cbbTo.SelectedValue & " AND "
            Filter &= "VEH_ID = " & cbbVeh.SelectedValue & " AND "
            Filter &= "SUP_ID = " & cbbSup.SelectedValue
            SQL = SQL & Filter
            Dim DT_TEMP As New DataTable
            Dim DA_TEMP As New SqlDataAdapter(SQL, ConnStr)
            DA_TEMP.Fill(DT_TEMP)
            Dim ID As Int32 = 0
            If DT_TEMP.Rows.Count > 0 Then
                ID = DT_TEMP.Rows(0).Item("ID")
                SQL = "SELECT ID,COST_ID,PRICE,ISNULL(QDATE,'N/A') QDATE,CASE WHEN REVISE = 0 THEN '' ELSE REVISE END AS REVISE,REMARK,UPDATE_BY,UPDATE_DATE,UNIT CostUnit FROM COST_SUP_REVISE WHERE COST_ID = " & ID & " ORDER BY CONVERT(Int,REVISE)"
            Else
                SQL = "SELECT ID,COST_ID,PRICE,ISNULL(QDATE,'N/A') QDATE,CASE WHEN REVISE = 0 THEN '' ELSE REVISE END AS REVISE,REMARK,UPDATE_BY,UPDATE_DATE,UNIT CostUnit FROM COST_SUP_REVISE WHERE 1=0"
            End If

        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT = New DataTable
        DA.Fill(DT)
        Grid.DataSource = DT

        If cbbFrom.SelectedValue > 0 And cbbTo.SelectedValue > 0 And cbbVeh.SelectedValue > 0 And cbbSup.SelectedValue > 0 Then
            btnAdd.Enabled = True
            btnSave.Enabled = True
        Else
            btnAdd.Enabled = False
            btnSave.Enabled = False
        End If
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = Grid.DataSource
        Dim DR As DataRow = DT.NewRow
        DT.Rows.Add(DR)
    End Sub

    Private Sub Grid_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid.DataError
        MsgBox(e.Exception.Message)
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = Grid.DataSource
        DT.AcceptChanges()
        'Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        If cbbFrom.SelectedIndex = 0 Or cbbTo.SelectedIndex = 0 Or cbbVeh.SelectedIndex = 0 Or cbbSup.SelectedIndex = 0 Then
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Grid.Rows.Count = 0 Then
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(DT.Rows(i).Item("PRICE")) OrElse DT.Rows(i).Item("PRICE").ToString = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
                'ElseIf IsNumeric(DT.Rows(i).Item("PRICE")) = False Then
                '    MessageBox.Show("กรุณากรอกข้อมูล Sort เป็นตัวเลขจำนวนเต็ม", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '    Exit Sub
            End If
            'If IsDBNull(DT.Rows(i).Item("QDATE")) OrElse DT.Rows(i).Item("QDATE").ToString = "" Then
            '    MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            '    Exit Sub
            'End If
            'If IsDate(DT.Rows(i).Item("QDATE")) = False Then
            If DT.Rows(i).Item("QDATE").ToString <> "N/A" And ValidateDate(DT.Rows(i).Item("QDATE").ToString) = False Then
                MessageBox.Show("รูปแบบวันทีไม่ถูกต้อง", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        Dim Sql As String = ""
        Sql = "SELECT ID,ROUTE_FROM,ROUTE_TO,VEH_ID,SUP_ID,PRICE,REVISE,QDATE,UPDATE_BY,UPDATE_DATE FROM COST_SUP WHERE "
        Dim Filter As String = ""
        Filter &= "ROUTE_FROM = " & cbbFrom.SelectedValue & " AND "
        Filter &= "ROUTE_TO = " & cbbTo.SelectedValue & " AND "
        Filter &= "VEH_ID = " & cbbVeh.SelectedValue & " AND "
        Filter &= "SUP_ID = " & cbbSup.SelectedValue
        Sql = Sql & Filter
        Dim DT_TEMP As New DataTable
        Dim DA_TEMP As New SqlDataAdapter(Sql, ConnStr)
        DA_TEMP.Fill(DT_TEMP)
        Dim ID As Int32 = 0
        If DT_TEMP.Rows.Count > 0 Then
            ID = DT_TEMP.Rows(0).Item("ID")
        End If
        Dim COST_ID As Int32 = 0
        If ID = 0 Then
            'Add
            Dim DR As DataRow = DT_TEMP.NewRow
            COST_ID = GetNewID("COST_SUP", "ID")
            DR("ID") = COST_ID
            DR("ROUTE_FROM") = cbbFrom.SelectedValue
            DR("ROUTE_TO") = cbbTo.SelectedValue
            DR("VEH_ID") = cbbVeh.SelectedValue
            DR("SUP_ID") = cbbSup.SelectedValue
            DR("UPDATE_BY") = myUser.user_id
            DR("UPDATE_DATE") = Now
            DT_TEMP.Rows.Add(DR)
            Dim cmd_ As New SqlCommandBuilder(DA_TEMP)
            DA_TEMP.Update(DT_TEMP)
        Else
            'Edit
            COST_ID = ID
        End If

        'Save History
        Dim conn As New SqlConnection(ConnStr)
        Dim sqlcmd As New SqlCommand
        conn.Open()

        Sql = "DELETE COST_SUP_REVISE WHERE COST_ID = " & COST_ID
        sqlcmd = New SqlCommand
        sqlcmd.CommandType = CommandType.Text
        sqlcmd.CommandText = Sql
        sqlcmd.Connection = conn
        sqlcmd.ExecuteNonQuery()

        For i As Integer = 0 To DT.Rows.Count - 1
            ID = GetNewID("COST_SUP_REVISE", "ID")
            Sql = "INSERT INTO COST_SUP_REVISE(ID,COST_ID,PRICE,REVISE,QDATE,REMARK,UNIT,UPDATE_BY,UPDATE_DATE) VALUES(" & vbCrLf
            Sql &= ID & "," & vbCrLf
            Sql &= COST_ID & "," & vbCrLf
            Sql &= "'" & DT.Rows(i).Item("PRICE") & "'," & vbCrLf
            Sql &= i & "," & vbCrLf
            Sql &= IIf(DT.Rows(i).Item("QDATE").ToString() = "N/A", "NULL", "'" & FixDate(DT.Rows(i).Item("QDATE")) & "'") & vbCrLf
            Sql &= ",'" & DT.Rows(i).Item("REMARK").ToString.Replace("'", "''") & "'," & vbCrLf
            Sql &= "'" & DT.Rows(i).Item("CostUnit") & "'," & vbCrLf
            Sql &= myUser.user_id & "," & vbCrLf
            Sql &= "GETDATE()" & vbCrLf
            Sql &= " )"
            sqlcmd = New SqlCommand
            sqlcmd.CommandType = CommandType.Text
            sqlcmd.CommandText = Sql
            sqlcmd.Connection = conn
            sqlcmd.ExecuteNonQuery()
        Next

        'Update Price & Revice ล่าสุด
        Sql = "UPDATE COST_SUP SET REVISE = " & DT.Rows.Count - 1 & vbCrLf
        'Sql &= ",QDATE = '" & FixDate(DT.Rows(DT.Rows.Count - 1).Item("QDATE").ToString) & "'" & vbCrLf
        Sql &= ",QDATE = " & IIf(DT.Rows(DT.Rows.Count - 1).Item("QDATE").ToString() = "N/A", "NULL", "'" & FixDate(DT.Rows(DT.Rows.Count - 1).Item("QDATE")) & "'") & vbCrLf
        Sql &= ",PRICE = '" & DT.Rows(DT.Rows.Count - 1).Item("PRICE").ToString & "'" & vbCrLf
        Sql &= ",UNIT = '" & DT.Rows(DT.Rows.Count - 1).Item("CostUnit").ToString & "'" & vbCrLf
        Sql &= "WHERE ID = " & COST_ID
        sqlcmd = New SqlCommand
        sqlcmd.CommandType = CommandType.Text
        sqlcmd.CommandText = Sql
        sqlcmd.Connection = conn
        sqlcmd.ExecuteNonQuery()

        conn.Close()

        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub cbb_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbFrom.SelectionChangeCommitted, cbbTo.SelectionChangeCommitted, cbbVeh.SelectionChangeCommitted, cbbSup.SelectionChangeCommitted
        VAR_S_FROM = cbbFrom.SelectedValue
        VAR_S_TO = cbbTo.SelectedValue
        VAR_VEH_ID = cbbVeh.SelectedValue
        VAR_SUP_ID = cbbSup.SelectedValue
        ShowData()
    End Sub

    Function GetDataForPrint(id As String) As DataTable
        Dim DT = New DataTable
        Try
            Dim sql As String = "SELECT CONVERT(VARCHAR(10),GETDATE(),103) AS PRINT_DATE,"
            sql &= " COST_SUP.ID,SUP_NAME,F.ROUTE_NAME AS ROUTE_F,T.ROUTE_NAME AS ROUTE_T,VEH_NAME,"
            sql &= " COST_SUP_REVISE.REVISE,COST_SUP_REVISE.PRICE,ISNULL(CONVERT(VARCHAR(10),COST_SUP_REVISE.QDATE,103),'N/A') AS QDATE,"
            sql &= " COST_SUP_REVISE.REMARK, COST_SUP_REVISE.UNIT"
            sql &= " FROM COST_SUP"
            sql &= " LEFT JOIN SUPPLIER ON COST_SUP.SUP_ID = SUPPLIER.SUP_ID"
            sql &= " LEFT JOIN ROUTE F ON COST_SUP.ROUTE_FROM = F.ROUTE_ID"
            sql &= " LEFT JOIN ROUTE T ON COST_SUP.ROUTE_TO = T.ROUTE_ID"
            sql &= " LEFT JOIN VEHICLE ON COST_SUP.VEH_ID = VEHICLE.VEH_ID"
            sql &= " LEFT JOIN COST_SUP_REVISE ON COST_SUP.ID = COST_SUP_REVISE.COST_ID"
            sql &= "  where COST_SUP.ID='" & id & "'"
            Dim DA As New SqlDataAdapter(sql, ConnStr)
            DA.Fill(DT)
        Catch ex As Exception

        End Try

        Return DT
    End Function

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Dim dt As New DataTable
        dt = GetDataForPrint(VAR_COST_ID)
        Dim f As New Transportation_Sup
        f.DTSUP = dt
        f.ReportName = "Transportation_Sup.rpt"
        f.ShowDialog()
    End Sub
End Class
﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmSettingBasis

    Private Sub frmBasis_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        ShowData(txtSearch.Text)
    End Sub

    Function GetData(txtSearch As String) As DataTable
        Dim da As New SqlDataAdapter
        Dim sql As String = ""
        sql = "select id,basis_name,active_status from ms_basis where 1=1 "
        If txtSearch <> "" Then
            sql += " and basis_name like '%" & txtSearch.Replace("'", "''") & "%'"
        End If
        If rdiSearchActive.Checked = True Then
            sql += " and active_status='Y'"
        End If
        If rdiSearchInactive.Checked = True Then
            sql += " and active_status='N'"
        End If

        sql += " order by basis_name"
        da = New SqlDataAdapter(sql, ConnStr)
        Dim dt As New DataTable
        da.Fill(dt)
        Return dt
    End Function

    Private Sub ShowData(txtSearch As String)
        Dim dt As DataTable = GetData(txtSearch)
        grdMain.DataSource = DT
        lblRec.Text = "Total   " & dt.Rows.Count & "   Record"

        grdMain.Columns.Clear()
        Dim basis_id As New System.Windows.Forms.DataGridViewTextBoxColumn
        basis_id.Name = "id"
        basis_id.DataPropertyName = "id"
        basis_id.Visible = False
        grdMain.Columns.Add(basis_id)
        grdMain.Columns("id").Visible = False

        Dim basis_name As New System.Windows.Forms.DataGridViewTextBoxColumn
        basis_name.DataPropertyName = "basis_name"
        basis_name.HeaderText = "Basis Name"
        basis_name.SortMode = DataGridViewColumnSortMode.NotSortable
        basis_name.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(basis_name)

        Dim active_status As New System.Windows.Forms.DataGridViewCheckBoxColumn()
        active_status.DataPropertyName = "active_status"
        active_status.TrueValue = "Y"
        active_status.FalseValue = "N"
        active_status.HeaderText = "Active"
        active_status.Width = 100
        grdMain.Columns.Add(active_status)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dt As DataTable = grdMain.DataSource
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(dt.Rows(i).Item("basis_name")) OrElse dt.Rows(i).Item("basis_name") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            Dim tmpdr As DataRow() = dt.Select("basis_name = '" & dt.Rows(i).Item("basis_name").ToString.Replace("'", "''") & "'")
            If tmpdr.Length > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        '== Save ==
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If

        Dim trans As SqlTransaction
        trans = conn.BeginTransaction
        Try
            Dim cmd As New SqlCommand
            cmd.Connection = trans.Connection
            cmd.Transaction = trans
            cmd.CommandType = CommandType.Text

            Dim tmpedit As DataRow() = dt.Select("id <> 0")
            '== ลบ ==
            Dim _id As String = ""
            For i As Int32 = 0 To tmpedit.Length - 1
                Application.DoEvents()
                _id &= tmpedit(i)("id").ToString
                If i < tmpedit.Length - 1 Then
                    _id &= ","
                End If
            Next
            Dim sql As String = ""
            If _id <> "" Then
                sql = "delete from MS_BASIS where id not in (" & _id & ")"
                cmd.CommandText = sql
                cmd.ExecuteNonQuery()
            End If
            

            '=== แก้ไข
            For i As Int32 = 0 To tmpedit.Length - 1
                Application.DoEvents()
                Dim _basis_id As String = tmpedit(i)("id").ToString
                Dim _basis_name As String = tmpedit(i)("basis_name").ToString.Replace("'", "''")
                Dim _active_status As String = tmpedit(i)("active_status").ToString

                sql = "Update MS_BASIS set basis_name ='" & _basis_name & _
                    "',active_status ='" & _active_status & "',updated_date = getdate(),updated_by='" & myUser.us_code & "' where id = '" & _basis_id & "'"
                cmd.CommandText = sql
                cmd.ExecuteNonQuery()
            Next

            '== เพิ่ม
            Dim tmpadd As DataRow() = dt.Select("id = 0")
            For i As Int32 = 0 To tmpadd.Length - 1
                Application.DoEvents()
                Dim _basis_name As String = tmpadd(i)("basis_name").ToString.Replace("'", "''")
                Dim _active_status As String = tmpadd(i)("active_status").ToString

                sql = "Insert Into MS_BASIS(basis_name,active_status,created_date,created_by)"
                sql &= " Values('" & _basis_name & "','" & _active_status & "',getdate(),'" & myUser.us_code & "')"
                cmd.CommandText = sql
                cmd.ExecuteNonQuery()
            Next

            trans.Commit()
            conn.Close()
            '== End Save

            txtSearch.Text = ""
            ShowData(txtSearch.Text)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            trans.Rollback()
            If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
                ShowData(txtSearch.Text)
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim dt As DataTable = grdMain.DataSource
        Dim dr As DataRow = dt.NewRow
        dr("id") = "0"
        dr("basis_name") = ""
        dr("active_status") = "Y"
        dt.Rows.Add(dr)
        grdMain.DataSource = dt
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        ShowData(txtSearch.Text)
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        ShowData(txtSearch.Text)
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        ShowData(txtSearch.Text)
    End Sub

End Class
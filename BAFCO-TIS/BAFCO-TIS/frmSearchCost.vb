﻿Imports System.Data.SqlClient
Imports System.Text

Public Class frmSearchCost

    Dim DT_SUP As New DataTable
    Dim DT_CUS As New DataTable
    Private WithEvents kbHook As New KeyboardHook

    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If
    End Sub
    Private Sub frmSearchCost_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        Me.WindowState = FormWindowState.Maximized
        
    End Sub


    Private Sub frmSearchCost_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        GridSup.AutoGenerateColumns = False
        GridCus.AutoGenerateColumns = False
        btnPrintSupplier.Visible = myUser.view_report
        btnPrintCustomer.Visible = myUser.view_report
        btnPrintAll.Visible = myUser.view_report

        BindCBB_Route(cbbFrom)
        BindCBB_Route(cbbTo)

        Dim SQL As String = ""
        SQL = "SELECT VEH_ID,VEH_NAME FROM VEHICLE ORDER BY SORT,VEH_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Int32 = 0 To DT.Rows.Count - 1
            Dim cb As New CheckBox
            cb.Text = DT.Rows(i).Item("VEH_NAME").ToString
            cb.Tag = DT.Rows(i).Item("VEH_ID").ToString
            flpVEH.Controls.Add(cb)
            AddHandler cb.CheckedChanged, AddressOf FilterDataVeh
        Next

        BindSupplier()
        BindCustomer()

        AddHandler UCSelectedItem.btnDeleteCusClick, AddressOf UCSelectedItem_btnDeleteCusClick
        AddHandler UCSelectedItem.btnDeleteSubClick, AddressOf UCSelectedItem_btnDeleteSupClick
    End Sub

    Sub FilterDataVeh(ByVal Sender As Object, ByVal e As EventArgs)
        Dim _VehFilter As String = GetVehID()
        FilterSupplier(_VehFilter)
        FilterCustomer(_VehFilter)
    End Sub

    Sub BindSupplier()
        Dim _VehFilter As String = GetVehID()
        Dim SQL As String = ""
        SQL &= "SELECT CONVERT(VARCHAR(10),GETDATE(),103) AS PrintDate ,ID AS S_ID,ROUTE_FROM AS S_FROM_ID,ROUTE_F.ROUTE_NAME AS S_FROM_NAME,ROUTE_T.ROUTE_ID AS S_TO_ID,ROUTE_T.ROUTE_NAME AS S_TO_NAME" & Environment.NewLine
        SQL &= ",COST_SUP.SUP_ID AS S_SUP_ID,SUP_NAME AS S_SUP_NAME,COST_SUP.VEH_ID AS S_VEH_ID,VEH_NAME AS S_VEH_NAME," & Environment.NewLine
        SQL &= "cast(price as decimal) AS S_COST,UNIT CostUnit" & Environment.NewLine
        SQL &= "FROM COST_SUP LEFT JOIN ROUTE ROUTE_F ON COST_SUP.ROUTE_FROM = ROUTE_F.ROUTE_ID" & Environment.NewLine
        SQL &= "LEFT JOIN ROUTE ROUTE_T ON COST_SUP.ROUTE_TO = ROUTE_T.ROUTE_ID" & Environment.NewLine
        SQL &= "LEFT JOIN SUPPLIER ON COST_SUP.SUP_ID = SUPPLIER.SUP_ID" & Environment.NewLine
        SQL &= "LEFT JOIN VEHICLE ON COST_SUP.VEH_ID = VEHICLE.VEH_ID" & Environment.NewLine
        SQL &= "LEFT JOIN (SELECT * FROM USER_SUPPLIER WHERE US_ID = " & myUser.user_id & ") US ON COST_SUP.SUP_ID = US.SUP_ID" & Environment.NewLine
        SQL &= "ORDER BY S_FROM_NAME,S_TO_NAME,VEHICLE.SORT,S_SUP_NAME" & Environment.NewLine
        DT_SUP = New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_SUP)
        GridSup.DataSource = DT_SUP
    End Sub

    Sub BindCustomer()
        Dim _VehFilter As String = GetVehID()
        Dim SQL As String = ""
        SQL &= "SELECT CONVERT(VARCHAR(10),GETDATE(),103) AS PrintDate,ID AS C_ID,ROUTE_FROM AS C_FROM_ID,ROUTE_F.ROUTE_NAME AS C_FROM_NAME,ROUTE_T.ROUTE_ID AS C_TO_ID,ROUTE_T.ROUTE_NAME AS C_TO_NAME" & Environment.NewLine
        SQL &= ",COST_CUS.CUS_ID AS C_CUS_ID,CUS_NAME AS C_CUS_NAME,COST_CUS.VEH_ID AS C_VEH_ID,VEH_NAME AS C_VEH_NAME," & Environment.NewLine
        SQL &= "cast(price as decimal) C_COST,UNIT ValueUnit, isnull(qt_status,1) qt_status" & Environment.NewLine
        SQL &= "FROM COST_CUS LEFT JOIN ROUTE ROUTE_F ON COST_CUS.ROUTE_FROM = ROUTE_F.ROUTE_ID" & Environment.NewLine
        SQL &= "LEFT JOIN ROUTE ROUTE_T ON COST_CUS.ROUTE_TO = ROUTE_T.ROUTE_ID" & Environment.NewLine
        SQL &= "LEFT JOIN VEHICLE ON COST_CUS.VEH_ID = VEHICLE.VEH_ID" & Environment.NewLine
        SQL &= "LEFT JOIN CUSTOMER ON COST_CUS.CUS_ID = CUSTOMER.CUS_ID" & Environment.NewLine
        SQL &= "LEFT JOIN (SELECT * FROM USER_CUSTOMER WHERE US_ID = " & myUser.user_id & ") US ON COST_CUS.CUS_ID = US.CUS_ID" & Environment.NewLine
        SQL &= "ORDER BY C_FROM_NAME,C_TO_NAME,VEHICLE.SORT,C_CUS_NAME" & Environment.NewLine
        DT_CUS = New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_CUS)
        GridCus.DataSource = DT_CUS

        FilterCustomer(_VehFilter)
    End Sub

    Sub FilterSupplier(_VehFilter As String)
        If DT_SUP.Rows.Count > 0 Then
            Dim _sup_id As String = GetSupCusID(flowSupplier)
            Dim Filter As String = "1=1"
            Filter &= "AND S_FROM_NAME LIKE '%" & Replace(cbbFrom.Text, "'", "''") & "%'"
            Filter &= "AND S_TO_NAME LIKE '%" & Replace(cbbTo.Text, "'", "''") & "%'"
            If _sup_id <> "" Then
                Filter &= "AND S_SUP_ID in (" & _sup_id & ")"
            End If

            If _VehFilter <> "0" Then
                Filter &= " AND S_VEH_ID IN (" & _VehFilter & ")"
            End If

            DT_SUP.DefaultView.RowFilter = Filter
        End If
    End Sub

    Sub FilterCustomer(_VehFilter As String)
        If DT_CUS.Rows.Count > 0 Then
            Dim _cus_id As String = GetSupCusID(flowCustomer)

            Dim Filter As String = " 1=1 "
            Filter &= "AND C_FROM_NAME LIKE '%" & Replace(cbbFrom.Text, "'", "''") & "%'"
            Filter &= "AND C_TO_NAME LIKE '%" & Replace(cbbTo.Text, "'", "''") & "%'"
            If _cus_id <> "" Then
                Filter &= "AND C_CUS_ID IN (" & _cus_id & ")"
            End If

            If _VehFilter <> "0" Then
                Filter &= " AND C_VEH_ID IN (" & _VehFilter & ")"
            End If

            DT_CUS.DefaultView.RowFilter = Filter

            If GridCus.RowCount > 0 Then
                For i As Integer = 0 To GridCus.RowCount - 1
                    If GridCus.Rows(i).Cells("colQtStatusValue").Value = "0" Then
                        GridCus.Rows(i).Cells("colQtStatus").Value = My.Resources.cb_g
                    ElseIf GridCus.Rows(i).Cells("colQtStatusValue").Value = "1" Then
                        GridCus.Rows(i).Cells("colQtStatus").Value = My.Resources.cb_gr
                    ElseIf GridCus.Rows(i).Cells("colQtStatusValue").Value = "2" Then
                        GridCus.Rows(i).Cells("colQtStatus").Value = My.Resources.cb_r
                    End If
                Next
                Application.DoEvents()
            End If
        End If
    End Sub

    Private Sub From_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbFrom.SelectedIndexChanged, cbbTo.SelectedIndexChanged, cbbFrom.TextChanged, cbbTo.TextChanged
        Dim _VehFilter As String = GetVehID()
        FilterSupplier(_VehFilter)
        FilterCustomer(_VehFilter)
    End Sub

    Private Sub GridSup_CellMouseDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles GridSup.CellMouseDoubleClick
        If e.RowIndex < 0 Then Exit Sub
        Dim f As New frmSearchDialogSupplier
        f.VAR_MODE = "EDIT"
        f.VAR_COST_ID = GridSup.Rows(e.RowIndex).Cells("S_ID").Value.ToString
        f.VAR_S_FROM = GridSup.Rows(e.RowIndex).Cells("S_FROM_ID").Value.ToString
        f.VAR_S_TO = GridSup.Rows(e.RowIndex).Cells("S_TO_ID").Value.ToString
        f.VAR_VEH_ID = GridSup.Rows(e.RowIndex).Cells("S_VEH_ID").Value.ToString
        f.VAR_SUP_ID = GridSup.Rows(e.RowIndex).Cells("S_SUP_ID").Value.ToString
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindSupplier()
        End If
    End Sub

    Private Sub btnS_Add_Click(sender As System.Object, e As System.EventArgs) Handles btnS_Add.Click
        Dim f As New frmSearchDialogSupplier
        f.VAR_MODE = "ADD"
        f.VAR_COST_ID = 0
        f.VAR_S_FROM = cbbFrom.SelectedValue
        f.VAR_S_TO = cbbTo.SelectedValue

        Dim _VehFilter As String = GetVehID()
        If InStr(_VehFilter, ",") > 0 Then
            f.VAR_VEH_ID = 0
        Else
            f.VAR_VEH_ID = _VehFilter
        End If

        'f.VAR_SUP_ID = "" cbbSup.SelectedValue
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindSupplier()
        End If
    End Sub

    Private Sub GridCus_CellMouseDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles GridCus.CellMouseDoubleClick
        If e.RowIndex < 0 Then Exit Sub
        Dim f As New frmSearchDialogCustomer
        f.VAR_MODE = "EDIT"
        f.VAR_COST_ID = GridCus.Rows(e.RowIndex).Cells("C_ID").Value.ToString
        f.VAR_C_FROM = GridCus.Rows(e.RowIndex).Cells("C_FROM_ID").Value.ToString
        f.VAR_C_TO = GridCus.Rows(e.RowIndex).Cells("C_TO_ID").Value.ToString
        f.VAR_VEH_ID = GridCus.Rows(e.RowIndex).Cells("C_VEH_ID").Value.ToString
        f.VAR_CUS_ID = GridCus.Rows(e.RowIndex).Cells("C_CUS_ID").Value.ToString
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindCustomer()
        End If
    End Sub

    Private Sub btnC_Add_Click(sender As System.Object, e As System.EventArgs) Handles btnC_Add.Click
        Dim f As New frmSearchDialogCustomer
        f.VAR_MODE = "ADD"
        f.VAR_COST_ID = 0
        f.VAR_C_FROM = cbbFrom.SelectedValue
        f.VAR_C_TO = cbbTo.SelectedValue

        Dim _VehFilter As String = GetVehID()
        If InStr(_VehFilter, ",") > 0 Then
            f.VAR_VEH_ID = 0
        Else
            f.VAR_VEH_ID = _VehFilter
        End If

        'f.VAR_CUS_ID = "" cbbCus.SelectedValue
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindCustomer()
        End If
    End Sub

    Private Sub btnPrintSupplier_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintSupplier.Click
        Dim dt As New DataTable
        dt = DT_SUP.DefaultView.ToTable
        Dim _sup_id As StringBuilder = GetSupCusName(flowSupplier)
        Dim f As New Transportation_Sup
        With f
            .ReportName = "COST_SUP.rpt"
            .LocationFrom = IIf(cbbFrom.Text = "", "All", cbbFrom.Text)
            .LocationTo = IIf(cbbTo.Text = "", "All", cbbTo.Text)
            .Vehicle = "" 'VehFilterText
            .Supplier = IIf(_sup_id.ToString() = "", "Transportation Cost from : All", "Transportation Cost from :</br>" & _sup_id.ToString())
            .DTSUP = dt
            .ShowDialog()
        End With

    End Sub

    Private Sub btnPrintCustomer_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintCustomer.Click
        Dim dt As New DataTable
        dt = DT_CUS.DefaultView.ToTable

        Dim _cus_id As StringBuilder = GetSupCusName(flowCustomer)

        Dim f As New Transportation_Cus
        With f
            .ReportName = "COST_CUS.rpt"
            .LocationFrom = IIf(cbbFrom.Text = "", "All", cbbFrom.Text)
            .LocationTo = IIf(cbbTo.Text = "", "All", cbbTo.Text)
            .Vehicle = "" 'VehFilterText
            .Customer = IIf(_cus_id.ToString = "", "Transportation Offered : All", "Transportation Offered :</br>" & _cus_id.ToString)
            .DT = dt
            .ShowDialog()
        End With

    End Sub

    Function GetAllDataForPrint() As DataTable
        Try
            Dim _sup_id As String = GetSupCusID(flowSupplier)
            Dim _cus_id As String = GetSupCusID(flowCustomer)

            Dim _VehFilter As String = GetVehID()
            Dim sql As String = ""
            sql &= "  Select distinct CONVERT(VARCHAR(10),GETDATE(),103) AS PrintDate ,T1.ID,ISNULL(T1.FROM_ID,T2.FROM_ID) FROM_ID," & Environment.NewLine
            sql &= "  ISNULL(T1.FROM_NAME,T2.FROM_NAME) FROM_NAME,ISNULL(T1.TO_ID,T2.TO_ID)TO_ID,ISNULL(T1.TO_NAME,T2.TO_NAME) TO_NAME," & Environment.NewLine
            sql &= "  T1.SUP_ID,T1.SUP_NAME,ISNULL(T1.VEH_ID,T2.VEH_ID) VEH_ID,ISNULL(T1.VEH_NAME,T2.VEH_NAME) VEH_NAME,T1.PRICE COST,T1.UNIT COST_UNIT," & Environment.NewLine
            sql &= "  T2.CUS_ID,T2.CUS_NAME CUS_NAME,T2.PRICE,T2.UNIT PRICE_UNIT" & Environment.NewLine
            sql &= " from ( " & Environment.NewLine

            sql &= " SELECT COST_SUP.ID,COST_SUP.ROUTE_FROM AS FROM_ID," & Environment.NewLine
            sql &= " ROUTE_F.ROUTE_NAME AS FROM_NAME, ROUTE_T.ROUTE_ID AS TO_ID,ROUTE_T.ROUTE_NAME AS TO_NAME ," & Environment.NewLine
            sql &= " COST_SUP.SUP_ID,SUP_NAME,COST_SUP.VEH_ID,VEH_NAME, cast(COST_SUP.price as decimal) PRICE, " & Environment.NewLine
            sql &= " COST_SUP.UNIT " & Environment.NewLine
            sql &= " FROM COST_SUP LEFT JOIN ROUTE ROUTE_F ON COST_SUP.ROUTE_FROM = ROUTE_F.ROUTE_ID " & Environment.NewLine
            sql &= " FULL OUTER JOIN COST_CUS ON  COST_SUP.ROUTE_FROM = COST_CUS.ROUTE_FROM and COST_SUP.ROUTE_TO =COST_CUS.ROUTE_TO and COST_SUP.VEH_ID = COST_CUS.VEH_ID" & Environment.NewLine
            sql &= " LEFT JOIN CUSTOMER ON COST_CUS.CUS_ID = CUSTOMER.CUS_ID " & Environment.NewLine
            sql &= " LEFT JOIN ROUTE ROUTE_T ON COST_SUP.ROUTE_TO = ROUTE_T.ROUTE_ID " & Environment.NewLine
            sql &= " LEFT JOIN SUPPLIER ON COST_SUP.SUP_ID = SUPPLIER.SUP_ID " & Environment.NewLine
            sql &= " LEFT JOIN VEHICLE ON COST_SUP.VEH_ID = VEHICLE.VEH_ID " & Environment.NewLine
            sql &= " LEFT JOIN (SELECT * FROM USER_SUPPLIER WHERE US_ID =  " & myUser.user_id & ") US ON COST_SUP.SUP_ID = US.SUP_ID" & Environment.NewLine
            sql &= " where 1=1"
            If _VehFilter <> "0" Then
                sql &= " AND US.US_ID Is Not NULL AND COST_SUP.VEH_ID IN (" & _VehFilter & ")" & Environment.NewLine
            End If

            sql &= " AND ROUTE_F.ROUTE_NAME LIKE '%" & cbbFrom.Text & "%'" & Environment.NewLine
            sql &= " AND ROUTE_T.ROUTE_NAME LIKE '%" & cbbTo.Text & "%'" & Environment.NewLine
            If _sup_id <> "" Then
                sql &= " AND US.SUP_ID IN (" & _sup_id & ")" & Environment.NewLine
            End If

            sql &= " ) T1" & Environment.NewLine
            sql &= " FULL OUTER JOIN (  " & Environment.NewLine
            sql &= " SELECT ID,ROUTE_FROM AS FROM_ID,ROUTE_F.ROUTE_NAME AS FROM_NAME, " & Environment.NewLine
            sql &= " ROUTE_T.ROUTE_ID AS TO_ID,ROUTE_T.ROUTE_NAME AS TO_NAME ,COST_CUS.CUS_ID,CUS_NAME,COST_CUS.VEH_ID,VEH_NAME, " & Environment.NewLine
            sql &= " cast(price as decimal) PRICE, UNIT,'Customer' QType " & Environment.NewLine
            sql &= " FROM COST_CUS LEFT JOIN ROUTE ROUTE_F ON COST_CUS.ROUTE_FROM = ROUTE_F.ROUTE_ID " & Environment.NewLine
            sql &= " LEFT JOIN ROUTE ROUTE_T ON COST_CUS.ROUTE_TO = ROUTE_T.ROUTE_ID " & Environment.NewLine
            sql &= " LEFT JOIN VEHICLE ON COST_CUS.VEH_ID = VEHICLE.VEH_ID " & Environment.NewLine
            sql &= " LEFT JOIN CUSTOMER ON COST_CUS.CUS_ID = CUSTOMER.CUS_ID " & Environment.NewLine
            sql &= " LEFT JOIN (SELECT * FROM USER_CUSTOMER WHERE US_ID =  " & myUser.user_id & ") US ON COST_CUS.CUS_ID = US.CUS_ID" & Environment.NewLine
            sql &= " WHERE US.US_ID IS NOT NULL " & Environment.NewLine

            If _VehFilter <> "0" Then
                sql &= " AND COST_CUS.VEH_ID IN (" & _VehFilter & ")" & Environment.NewLine
            End If

            sql &= " AND ROUTE_F.ROUTE_NAME LIKE '%" & cbbFrom.Text & "%'" & Environment.NewLine
            sql &= " AND ROUTE_T.ROUTE_NAME LIKE '%" & cbbTo.Text & "%'" & Environment.NewLine
            If _cus_id <> "" Then
                sql &= " AND US.CUS_ID in (" & _cus_id & ")" & Environment.NewLine
            End If

            sql &= " ) T2 on T1.FROM_ID = T2.FROM_ID and T1.TO_ID =T2.TO_ID and T1.VEH_ID = T2.VEH_ID" & Environment.NewLine
            sql &= " ORDER BY FROM_NAME,TO_NAME,SUP_NAME,CUS_NAME"


            Dim DT As New DataTable
            Dim DA As New SqlDataAdapter(sql, ConnStr)
            DA.Fill(DT)

            Return DT
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Sub btnPrintAll_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintAll.Click

        Dim dt As New DataTable
        dt = GetAllDataForPrint()

        Dim _sup_id As StringBuilder = GetSupCusName(flowSupplier)
        Dim _cus_id As StringBuilder = GetSupCusName(flowCustomer)

        Dim f As New Transportation_Sup
        With f
            .ReportName = "COST_ALL.rpt"
            .LocationFrom = IIf(cbbFrom.Text = "", "All", cbbFrom.Text)
            .LocationTo = IIf(cbbTo.Text = "", "All", cbbTo.Text)
            .Vehicle = "" 'VehFilterText
            .Supplier = IIf(_sup_id.ToString() = "", "Transportation Cost from : All", "Transportation Cost from :</br>" & _sup_id.ToString())
            .Customer = IIf(_cus_id.ToString = "", "Transportation Offered : All", "Transportation Offered :</br>" & _cus_id.ToString)
            .DTSUP = dt
            .ShowDialog()
        End With
    End Sub

    Private Sub cbAllVehicle_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cbAllVehicle.CheckedChanged

        For Each cb As CheckBox In flpVEH.Controls
            If cb IsNot Nothing Then
                cb.Checked = cbAllVehicle.Checked
            End If
        Next

    End Sub

    Function GetVehID() As String
        Dim _VehFilter As String = ""
        For Each cb As CheckBox In flpVEH.Controls
            If cb IsNot Nothing Then
                If cb.Checked Then
                    _VehFilter &= cb.Tag & ","
                End If
            End If
        Next

        If _VehFilter <> "" Then
            _VehFilter = _VehFilter.Substring(0, _VehFilter.Length - 1)
        Else
            _VehFilter = "0"
        End If
        Return _VehFilter
    End Function

#Region "SelectCustomer"
    Function GetSupCusName(flow As FlowLayoutPanel) As StringBuilder
        Dim _Name As New StringBuilder
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            _Name.Append(UCItem1.strName.Replace("'", "''"))
            If i < flow.Controls.Count - 1 Then
                _Name.Append("</br>")
            End If
        Next
        Return _Name
    End Function

    Private Sub btnDeleteSupCus(id As String, flow As FlowLayoutPanel, type As String)
        Dim _id As String = id
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("name")

        Dim dr As DataRow
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            If UCItem1.strID <> _id Then
                dr = dt.NewRow
                dr("id") = UCItem1.strID
                dr("name") = UCItem1.strName
                dt.Rows.Add(dr)
            End If
        Next

        If flow.Controls.Count > 0 Then flow.Controls.Clear()
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1.strID = dt.Rows(i)("id")
            UCItem1.strName = dt.Rows(i)("name")
            UCItem1.type = type
            flow.Controls.Add(UCItem1)
        Next
    End Sub

    Function GetSupCusID(flow As FlowLayoutPanel) As String
        Dim _id As String = ""
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            _id &= UCItem1.strID
            If i < flow.Controls.Count - 1 Then
                _id &= ","
            End If
        Next
        Return _id
    End Function

    Private Sub UCSelectedItem_btnDeleteCusClick(id As String)
        btnDeleteSupCus(id, flowCustomer, "cus")
        Dim _VehFilter As String = GetVehID()
        FilterCustomer(_VehFilter)
    End Sub

    Sub BindDataSupCus(dt_selected As DataTable, flow As FlowLayoutPanel, type As String)
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("name")

        Dim dr As DataRow
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            dr = dt.NewRow
            dr("id") = UCItem1.strID
            dr("name") = UCItem1.strName
            dt.Rows.Add(dr)
        Next

        If dt_selected IsNot Nothing Then
            For i As Integer = 0 To dt_selected.Rows.Count - 1
                dr = dt.NewRow
                dr("id") = dt_selected.Rows(i)("id")
                dr("name") = dt_selected.Rows(i)("name")
                dt.Rows.Add(dr)
            Next
        End If

        If flow.Controls.Count > 0 Then flow.Controls.Clear()
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1.strID = dt.Rows(i)("id")
            UCItem1.strName = dt.Rows(i)("name")
            UCItem1.type = type
            flow.Controls.Add(UCItem1)
        Next
    End Sub

    Private Sub btnAddCustomer_Click(sender As System.Object, e As System.EventArgs) Handles btnAddCustomer.Click
        Dim _cus_id As String = GetSupCusID(flowCustomer)
        Dim f As New popupSelectCustomer
        f.SetCustomerID = _cus_id
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim dt As New DataTable
            dt = f.GetData()
            BindDataSupCus(dt, flowCustomer, "cus")
        End If
        Dim _VehFilter As String = GetVehID()
        FilterCustomer(_VehFilter)
    End Sub
#End Region

#Region "SelectSupplier"
    Private Sub UCSelectedItem_btnDeleteSupClick(id As String)
        btnDeleteSupCus(id, flowSupplier, "sup")
        Dim _VehFilter As String = GetVehID()
        FilterSupplier(_VehFilter)
    End Sub

    Private Sub btnAddSupplier_Click(sender As System.Object, e As System.EventArgs) Handles btnAddSupplier.Click
        Dim _sup_id As String = GetSupCusID(flowSupplier)
        Dim f As New popupSelectSupplier
        f.SetSpplierID = _sup_id
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim dt As New DataTable
            dt = f.GetData()
            BindDataSupCus(dt, flowSupplier, "sup")
        End If
        Dim _VehFilter As String = GetVehID()
        FilterSupplier(_VehFilter)
    End Sub
#End Region

End Class
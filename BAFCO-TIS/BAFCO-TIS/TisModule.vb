﻿Imports BAFCO_TIS.Org.Mentalis.Files
Imports System.Data.SqlClient
Imports Join.LeftJoin
Imports System.Globalization
Imports CrystalDecisions.Shared

Module TisModule

    Public TempPath As String = "C:\TIS"
    Public INIFile As String = TempPath & "\TIS.ini"
    Public ConnStr As String = getConnectionString()

    Public Enum ColorStatus
        White = 0
        Red = 1
        Blue = 2
    End Enum

    Public myUser As User
    Public Structure User
        Dim user_id As String
        Dim fulllname As String
        Dim us_code As String
        Dim view_report As Boolean
        Dim export_report As Boolean
        Dim capture_screen As Boolean
    End Structure

    Public Sub getMyVersion()
        Dim version As System.Version = System.Reflection.Assembly.GetExecutingAssembly.GetName().Version
        Dim MyVersion As String = version.Major & "." & version.Minor & "." & version.Build
        frmMain.Text = Replace(frmMain.Text, "[%V%]", MyVersion)
    End Sub

    Function getConnectionString() As String
        Dim ini As New IniReader(INIFile)
        ini.Section = "Setting"
        Return "Data Source=" & ini.ReadString("Server") & ";Initial Catalog=" & ini.ReadString("Database") & ";User ID=" & ini.ReadString("Username") & ";Password=" & ini.ReadString("Password") & ";Connect Timeout=1;"
    End Function

    Public Function GetSysconfig(ConfigName As String) As String
        Dim ret As String = ""
        Dim sql As String = "select config_value from cf_sysconfig where config_name=@_CONFIG_NAME"
        Dim p(1) As SqlParameter
        p(0) = SetText("@_CONFIG_NAME", ConfigName)

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt.Rows.Count > 0 Then
            ret = dt.Rows(0)("config_value")
        End If
        dt.Dispose()

        Return ret
    End Function

    Public Function CkeckConnection(ByVal ConnectionString As String) As Boolean
        Dim Conn As New SqlConnection
        Try
            Conn.ConnectionString = ConnectionString
            Conn.Open()
            Return True
        Catch ex As Exception
            Return False
        End Try
        Return False
    End Function

    Public Function GetNewID(ByVal TableName As String, ByVal ColName As String) As String
        Dim id As String = ""
        Dim SQL As String = ""
        SQL = "select isnull(MAX(" & ColName & " + 1),1) as id from " & TableName.Replace("'", "''")
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            id = DT.Rows(0).Item("id").ToString
        End If
        Return id
    End Function

    Public Function GetNewID(ByVal TableName As String, ByVal ColName As String, cmd As SqlCommand) As String
        Dim id As String = ""
        Dim SQL As String = ""
        SQL = "select isnull(MAX(" & ColName & " + 1),1) as id from " & TableName.Replace("'", "''")
        cmd.CommandText = SQL
        Dim DA As New SqlDataAdapter(cmd)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            id = DT.Rows(0).Item("id").ToString
        End If
        Return id
    End Function

    Public Function JoinDataTable(ByVal Dt1 As System.Data.DataTable, ByVal Dt2 As System.Data.DataTable, ByVal argKey1 As String, ByVal argKey2 As String) As System.Data.DataTable
        Dim joinDt As New DataTable
        'If Dt1.Rows.Count > 0 And Dt2.Rows.Count > 0 Then
        joinDt = Join.LeftJoin.Join(Dt1, Dt2, Dt1.Columns(argKey1), Dt2.Columns(argKey2))
        'End If
        Return joinDt
    End Function


    Public Function StringToDate(ByVal InputString As String, ByVal Format As String) As DateTime
        Dim Provider As CultureInfo = CultureInfo.GetCultureInfo("en-US")
        Return DateTime.ParseExact(InputString, Format, Provider)
    End Function

    Public Function FixDate(ByVal StringDate As String) As String
        If InStr(StringDate, "/") = 3 Then
            Dim D() As String = Split(StringDate, "/")
            D(2) = D(2).Replace("0:00:00", "").Trim
            If CInt(D(2)) > 2500 Then
                D(2) = CInt(D(2)) - 543
            End If
            Dim FullDate As String = D(2) & D(1).PadLeft(2, "0") & D(0).PadLeft(2, "0")
            Return FullDate
        Else
            Return ""
        End If
    End Function

    Public Function DateToString(ByVal InputDate As DateTime, ByVal Format As String) As String
        Return InputDate.ToString(Format)
    End Function

#Region "Binding Combobox"
    Public Sub BindCBB_Status(ByRef cbb As ComboBox, Optional ByVal ShowAll As Boolean = False)
        Dim SQL As String = ""
        If ShowAll = True Then
            SQL &= "SELECT 2 AS ID,'All' AS NAME" & vbCrLf
            SQL &= "UNION ALL" & vbCrLf
        End If
        SQL &= "SELECT 1 AS ID,'Active' AS NAME" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT 0 AS ID,'Inactive' AS NAME" & vbCrLf
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "NAME"
        cbb.ValueMember = "ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_Route(ByRef cbb As ComboBox)
        Dim SQL As String = ""
        SQL = "SELECT 0 SORT, 0 ROUTE_ID,'' ROUTE_NAME UNION ALL" & vbNewLine
        SQL &= "SELECT 1 SORT,ROUTE_ID,ROUTE_NAME FROM ROUTE ORDER BY SORT,ROUTE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "ROUTE_NAME"
        cbb.ValueMember = "ROUTE_ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_User(ByRef cbb As ComboBox)
        Dim SQL As String = ""
        SQL = "SELECT 0 SORT, 0 US_ID,'' US_NAME UNION ALL" & vbNewLine
        SQL &= "SELECT 1 SORT,US_ID,US_NAME FROM [USER] ORDER BY SORT,US_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "US_NAME"
        cbb.ValueMember = "US_ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_Customer(ByRef cbb As ComboBox)
        Dim SQL As String = ""
        SQL = "SELECT 0 SORT, 0 CUS_ID,'' CUS_NAME UNION ALL" & vbNewLine
        SQL &= "SELECT 1 SORT,CUS_ID,CUS_NAME FROM CUSTOMER WHERE CUS_ID IN (SELECT CUS_ID FROM USER_CUSTOMER WHERE US_ID = " & myUser.user_id & ") ORDER BY SORT,CUS_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "CUS_NAME"
        cbb.ValueMember = "CUS_ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_Supplier(ByRef cbb As ComboBox)
        Dim SQL As String = ""
        SQL = "SELECT 0 SORT, 0 SUP_ID,'' SUP_NAME UNION ALL" & vbNewLine
        SQL &= "SELECT 1 SORT,SUP_ID,SUP_NAME FROM SUPPLIER WHERE SUP_ID IN (SELECT SUP_ID FROM USER_SUPPLIER WHERE US_ID = " & myUser.user_id & ") ORDER BY SORT,SUP_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "SUP_NAME"
        cbb.ValueMember = "SUP_ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_Vehicle(ByRef cbb As ComboBox)
        Dim SQL As String = ""
        SQL = "SELECT 0 SORT, 0 VEH_ID,'' VEH_NAME UNION ALL" & vbNewLine
        SQL &= "SELECT SORT,VEH_ID,VEH_NAME FROM VEHICLE ORDER BY SORT,VEH_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "VEH_NAME"
        cbb.ValueMember = "VEH_ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_ServiceType(ByRef cbb As ComboBox)
        Dim SQL As String = ""
        SQL = "SELECT 0 SORT, 0 SER_TYPE_ID,'' SER_TYPE_NAME UNION ALL" & vbNewLine
        SQL &= "SELECT 1 SORT,SER_TYPE_ID,SER_TYPE_NAME FROM SERVICE_TYPE ORDER BY SORT,SER_TYPE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "SER_TYPE_NAME"
        cbb.ValueMember = "SER_TYPE_ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_Service(ByRef cbb As ComboBox, ByVal Type As Int32)
        Dim SQL As String = ""
        SQL = "SELECT 0 SORT, 0 SER_ID,'' SER_NAME UNION ALL" & vbNewLine
        SQL &= "SELECT 1 SORT,SER_ID,SER_NAME FROM SERVICE WHERE SER_TYPE_ID = " & Type & " ORDER BY SORT,SER_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        cbb.DataSource = DT
        cbb.DisplayMember = "SER_NAME"
        cbb.ValueMember = "SER_ID"
        cbb.SelectedIndex = 0
    End Sub

    Public Sub BindCBB_Mode(cbbMode As ComboBox, IsDefault As Boolean)
        Dim sql As String = "select id, mode_name, default_qt from MS_MODE where active_status='Y' order by mode_name"

        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("mode_name")
        'End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("mode_name") = ""
        dt.Rows.InsertAt(dr, 0)

        Dim DefaultModeID As Long = 0
        dt.DefaultView.RowFilter = "default_qt='Y'"
        If dt.DefaultView.Count > 0 Then
            DefaultModeID = dt.DefaultView(0)("id")
        End If
        dt.DefaultView.RowFilter = ""

        cbbMode.DataSource = dt
        cbbMode.DisplayMember = "mode_name"
        cbbMode.ValueMember = "id"

        If IsDefault = True Then
            cbbMode.SelectedValue = DefaultModeID
        End If

    End Sub

    Public Sub BindCBB_Scope(cbbScope As ComboBox)
        Dim sql As String = "select id, scope_name from MS_SCOPE where active_status='Y' order by scope_name"

        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("scope_name")
        'End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("scope_name") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbScope.DataSource = dt
        cbbScope.DisplayMember = "scope_name"
        cbbScope.ValueMember = "id"
    End Sub

    Public Sub BindCBB_Term(cbbTerm As ComboBox)
        Dim sql As String = "select id, term_name from MS_TERM where active_status='Y' order by term_name"

        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("term_name")
        'End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("term_name") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbTerm.DataSource = dt
        cbbTerm.DisplayMember = "term_name"
        cbbTerm.ValueMember = "id"
    End Sub

    Public Sub BindCBB_Country(cbbCountry As ComboBox)
        Dim sql As String = "select id, country_name from MS_COUNTRY where active_status='Y' order by country_name"

        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("country_name")
        'End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("country_name") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbCountry.DataSource = dt
        cbbCountry.DisplayMember = "country_name"
        cbbCountry.ValueMember = "id"
    End Sub

    Public Sub BindCBB_CustomerType(cbbCustomerType As ComboBox)
        Dim sql As String = "select cus_type_id, cus_type_name from customer_type order by cus_type_name"
        Dim dt As DataTable = Execute_DataTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("cus_type_id") = 0
        dr("cus_type_name") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbCustomerType.ValueMember = "cus_type_id"
        cbbCustomerType.DisplayMember = "cus_type_name"
        cbbCustomerType.DataSource = dt
    End Sub

    Public Sub BindCBB_CreditTerm(cbbCreditTerm As ComboBox)
        Dim sql As String = "select id, credit_term "
        sql += " from ms_credit_term "
        sql += " where active_status='Y'"
        sql += " order by credit_term"

        Dim dt As DataTable = Execute_DataTable(sql)
        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("credit_term") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbCreditTerm.ValueMember = "id"
        cbbCreditTerm.DisplayMember = "credit_term"
        cbbCreditTerm.DataSource = dt
    End Sub

    Public Sub SetCaseCadingCountry(cbbCountry As ComboBox, cbbCity As ComboBox)
        If cbbCountry.SelectedIndex > 0 Then
            Dim sql As String = " select id, city_name "
            sql += " from ms_city "
            sql += " where ms_country_id=@_COUNTRY_ID"
            sql += " order by city_name "

            Dim p(1) As SqlParameter
            p(0) = SetBigInt("@_COUNTRY_ID", cbbCountry.SelectedValue)

            Dim dt As DataTable = Execute_DataTable(sql, p)

            Dim dr As DataRow = dt.NewRow
            dr("id") = 0
            dr("city_name") = ""
            dt.Rows.InsertAt(dr, 0)

            cbbCity.ValueMember = "id"
            cbbCity.DisplayMember = "city_name"
            cbbCity.DataSource = dt
        End If
    End Sub
#End Region

    Public Function ValidateDate(ByVal checkInputValue As String) As Boolean
        Dim returnError As Boolean = False
        Dim dateVal As DateTime
        If Date.TryParseExact(checkInputValue, "dd/mm/yyyy",
               System.Globalization.CultureInfo.CurrentCulture,
               DateTimeStyles.None, dateVal) Then
            returnError = True
        End If
        Return returnError
    End Function

    Public ReadOnly Property CrystalLogonInfo() As TableLogOnInfo
        Get
            Dim ini As New IniReader(INIFile)
            ini.Section = "Setting"
            'Return "Data Source=" & ini.ReadString("Server") & ";Initial Catalog=" & ini.ReadString("Database") & ";User ID=" & ini.ReadString("Username") & ";Password=" & ini.ReadString("Password") & ";Connect Timeout=1;"

            Dim logonInfo As New TableLogOnInfo
            logonInfo.ConnectionInfo.ServerName = ini.ReadString("Server")
            logonInfo.ConnectionInfo.DatabaseName = ini.ReadString("Database")
            logonInfo.ConnectionInfo.UserID = ini.ReadString("Username")
            logonInfo.ConnectionInfo.Password = ini.ReadString("Password")
            logonInfo.ConnectionInfo.IntegratedSecurity = False
            Return logonInfo
        End Get
    End Property

    Public Function SaveDataToTransportationCost(trans As SqlTransaction, CusID As Integer, RouteIDFrom As Integer, RouteIDTo As Integer, VehicleID As Integer, Price As Double, UnitText As String, Remarks As String, QtFrom As String, QDate As Date, QtNo As String, QtStatus As String) As Boolean
        Dim ret As Boolean = False
        Dim sql As String = "select * from COST_CUS  "
        sql += " where route_from=@_ROUTE_FROM and route_to=@_ROUTE_TO"
        sql += " and veh_id=@_VEHICLE_ID and cus_id=@_CUS_ID "

        Dim p(4) As SqlParameter
        p(0) = SetInt("@_ROUTE_FROM", RouteIDFrom)
        p(1) = SetInt("@_ROUTE_TO", RouteIDTo)
        p(2) = SetInt("@_VEHICLE_ID", VehicleID)
        p(3) = SetInt("@_CUS_ID", CusID)

        Dim IsRevise As Boolean = False
        Dim _Revise As Integer = 0
        Dim CostID As Integer = 0
        Dim CusReviseLastID As Integer = 0

        Dim dt As DataTable = Execute_DataTable(sql, trans, p)
        If dt.Rows.Count = 0 Then
            sql = "INSERT INTO [COST_CUS]([ID],[ROUTE_FROM],[ROUTE_TO],[VEH_ID],[CUS_ID],[PRICE],[REVISE],[QDATE],[REMARK],"
            sql += "[IMPORT_DATE],[UPDATE_BY],[UPDATE_DATE],[UNIT], qt_from, qt_no, qt_status)"
            sql += " output inserted.id"
            sql += " values((select isnull(max(id),0)+1 from COST_CUS), @_ROUTE_ID_FROM, @_ROUTE_ID_TO, @_VEHICLE_ID, @_CUS_ID, @_PRICE, @_REVISE, "
            sql += " @_QDATE, @_REMARK, getdate(), @_UPDATE_BY, getdate(), @_UNIT, @_QT_FROM, @_QT_NO, @_QT_STATUS)"

            ReDim p(13)
            p(0) = SetInt("@_ROUTE_ID_FROM", RouteIDFrom)
            p(1) = SetInt("@_ROUTE_ID_TO", RouteIDTo)
            p(2) = SetInt("@_VEHICLE_ID", VehicleID)
            p(3) = SetInt("@_CUS_ID", CusID)
            p(4) = SetDouble("@_PRICE", Price)
            p(5) = SetInt("@_REVISE", _Revise)
            p(6) = SetDateTime("@_QDATE", QDate)
            p(7) = SetText("@_REMARK", Remarks)
            p(8) = SetInt("@_UPDATE_BY", myUser.user_id)
            p(9) = SetText("@_UNIT", UnitText)
            p(10) = SetText("@_QT_FROM", QtFrom)
            p(11) = SetText("@_QT_NO", QtNo)
            p(12) = SetText("@_QT_STATUS", QtStatus)

            Dim tmpDt As DataTable = Execute_DataTable(sql, trans, p)
            If tmpDt.Rows.Count > 0 Then
                ret = True
                CostID = tmpDt.Rows(0)("id")
                IsRevise = True
            End If
            tmpDt.Dispose()
        Else
            CostID = dt.Rows(0)("id")

            sql = "select * from COST_CUS_REVISE "
            sql += " where cost_id=@_CUST_ID"
            sql += " order by revise desc"
            ReDim p(1)
            p(0) = SetInt("@_CUST_ID", CostID)

            dt = Execute_DataTable(sql, trans, p)
            If dt.Rows.Count > 0 Then
                CusReviseLastID = Convert.ToInt64(dt.Rows(0)("id"))
                _Revise = Convert.ToInt32(dt.Rows(0)("revise")) + 1
                Dim _Price As Double = Convert.ToDouble(dt.Rows(0)("price"))
                If _Price <> Price Then
                    'ถ้าราคามีการเปลี่ยนแปลง
                    sql = "Update COST_CUS Set PRICE=@_PRICE,UNIT=@_UNIT" & _
                   ",REVISE=@_REVISE,REMARK=@_REMARK" & _
                   ",QDATE=@_QDATE, qt_from=@_QT_FROM, qt_no=@_QT_NO, qt_status=@_QT_STATUS" & _
                   ",IMPORT_DATE=getdate(),UPDATE_BY=@_UPDATE_BY,UPDATE_DATE=getdate() " & _
                   " Where ID = @_ID"

                    ReDim p(10)
                    p(0) = SetText("@_PRICE", Price)
                    p(1) = SetText("@_UNIT", UnitText)
                    p(2) = SetText("@_REVISE", _Revise)
                    p(3) = SetText("@_REMARK", Remarks)
                    p(4) = SetDateTime("@_QDATE", QDate)
                    p(5) = SetInt("@_UPDATE_BY", myUser.user_id)
                    p(6) = SetInt("@_ID", CostID)
                    p(7) = SetText("@_QT_FROM", QtFrom)
                    p(8) = SetText("@_QT_NO", QtNo)
                    p(9) = SetText("@_QT_STATUS", QtStatus)

                    ret = Execute_Command(sql, trans, p)
                    IsRevise = True
                Else
                    'ถ้าราคาไม่เปลี่ยนแปลงให้ Update Status ของ Quotation
                    sql = "Update COST_CUS Set qt_from=@_QT_FROM, qt_no=@_QT_NO, qt_status=@_QT_STATUS "
                    sql += ", UPDATE_BY=@_UPDATE_BY,UPDATE_DATE=getdate()"
                    sql += " Where ID = @_ID"

                    ReDim p(5)
                    p(0) = SetText("@_QT_FROM", QtFrom)
                    p(1) = SetText("@_QT_NO", QtNo)
                    p(2) = SetText("@_QT_STATUS", QtStatus)
                    p(3) = SetInt("@_UPDATE_BY", myUser.user_id)
                    p(4) = SetInt("@_ID", CostID)

                    ret = Execute_Command(sql, trans, p)
                End If
            Else
                IsRevise = True
                ret = True
            End If
        End If

        If IsRevise = True Then
            If ret = True Then
                sql = "INSERT INTO [COST_CUS_REVISE]([ID],[COST_ID],[PRICE],[REVISE],[REMARK],"
                sql += " [QDATE],[UPDATE_BY],[UPDATE_DATE],[UNIT], qt_from,qt_no,qt_status)"
                sql += " values((select isnull(max(id),0)+1 from COST_CUS_REVISE), @_COST_ID, @_PRICE, @_REVISE, @_REMARK,"
                sql += " @_QDATE, @_UPDATE_BY, getdate(), @_UNIT, @_QT_FROM, @_QT_NO, @_QT_STATUS)"

                ReDim p(10)
                p(0) = SetInt("@_COST_ID", CostID)
                p(1) = SetText("@_PRICE", Price)
                p(2) = SetText("@_REVISE", _Revise)
                p(3) = SetText("@_REMARK", Remarks)
                p(4) = SetDateTime("@_QDATE", QDate)
                p(5) = SetInt("@_UPDATE_BY", myUser.user_id)
                p(6) = SetText("@_UNIT", UnitText)
                p(7) = SetText("@_QT_FROM", QtFrom)
                p(8) = SetText("@_QT_NO", QtNo)
                p(9) = SetText("@_QT_STATUS", QtStatus)

                ret = Execute_Command(sql, trans, p)
            End If
        Else
            'ถ้าไม่มีการเปลี่ยนแปลงราคาให้ Update Status ของการ Revise ครั้งล่าสุด
            If CusReviseLastID > 0 Then
                sql = "Update COST_CUS_REVISE Set qt_from=@_QT_FROM, qt_no=@_QT_NO, qt_status=@_QT_STATUS "
                sql += ", UPDATE_BY=@_UPDATE_BY,UPDATE_DATE=getdate()"
                sql += " Where ID = @_ID"

                ReDim p(5)
                p(0) = SetText("@_QT_FROM", QtFrom)
                p(1) = SetText("@_QT_NO", QtNo)
                p(2) = SetText("@_QT_STATUS", QtStatus)
                p(3) = SetInt("@_UPDATE_BY", myUser.user_id)
                p(4) = SetInt("@_ID", CusReviseLastID)

                ret = Execute_Command(sql, trans, p)
            End If
        End If

        Return ret
    End Function

    Public Function SaveDataToServiceCost(trans As SqlTransaction, CusID As Integer, ItemDesc As String, Price As Double, UnitText As String, Remarks As String, QtFrom As String, QDate As Date, QtNo As String, QtStatus As String) As Boolean
        Dim ret As Boolean = False
        Dim SQL As String = "select SER_NAME as chk_Ser,SER_ID as IM_SER_ID from SERVICE where ser_name=@_SERVICE_NAME"
        Dim p(1) As SqlParameter
        p(0) = SetText("@_SERVICE_NAME", ItemDesc)

        Dim ServiceID As Integer = 0
        Dim dt As DataTable = Execute_DataTable(SQL, trans, p)
        If dt.Rows.Count = 0 Then
            'ถ้ายังไม่มีใน Service ก็ Insert ใหม่โลด
            SQL = "insert into SERVICE (ser_id,ser_name,ser_type_id,update_by,update_date )"
            SQL += " output inserted.ser_id "
            SQL += " values((select max(ser_id)+1 from SERVICE), @_SER_NAME, 1, @_UPDATE_BY, getdate())"

            ReDim p(2)
            p(0) = SetText("@_SER_NAME", ItemDesc)
            p(1) = SetInt("@_UPDATE_BY", myUser.user_id)

            Dim TmpDt As DataTable = Execute_DataTable(SQL, trans, p)
            If TmpDt.Rows.Count > 0 Then
                ServiceID = TmpDt.Rows(0)("ser_id")
            End If
            TmpDt.Dispose()
        Else
            ServiceID = Convert.ToInt32(dt.Rows(0)("IM_SER_ID"))
        End If

        SQL = "SELECT * FROM SERVICE_CUS WHERE SER_ID = @_SER_ID AND CUS_ID = @_CUS_ID"
        ReDim p(2)
        p(0) = SetInt("@_SER_ID", ServiceID)
        p(1) = SetInt("@_CUS_ID", CusID)

        Dim re As String = "false"
        Dim IsRevise As Boolean = False
        Dim _revise As Integer = 0
        Dim ServiceCusID As Integer = 0

        Dim CusReviseLastID As Integer = 0

        dt = Execute_DataTable(SQL, trans, p)
        If dt.Rows.Count = 0 Then
            '== กรณีที่ไม่พบข้อมูล ทำการ Insert
            SQL = "Insert Into SERVICE_CUS(ID,SER_ID,CUS_ID,PRICE,UNIT,REVISE,REMARK,QDATE,IMPORT_DATE,UPDATE_DATE,UPDATE_BY, qt_from, qt_no, qt_status)"
            SQL += " output inserted.id"
            SQL &= " Values((select max(id)+1 from SERVICE_CUS),@_SER_ID,@_CUS_ID,@_PRICE,@_UNIT,0,@_REMARK,@_QDATE" & _
                ",getdate(),getdate(),@_UPDATE_BY, @_QT_FROM, @_QT_NO, @_QT_STATUS)"

            ReDim p(10)
            p(0) = SetInt("@_SER_ID", ServiceID)
            p(1) = SetInt("@_CUS_ID", CusID)
            p(2) = SetText("@_PRICE", Price)
            p(3) = SetText("@_UNIT", UnitText)
            p(4) = SetText("@_REMARK", Remarks)
            p(5) = SetDateTime("@_QDATE", QDate)
            p(6) = SetInt("@_UPDATE_BY", myUser.user_id)
            p(7) = SetText("@_QT_FROM", QtFrom)
            p(8) = SetText("@_QT_NO", QtNo)
            p(9) = SetText("@_QT_STATUS", QtStatus)

            Dim tmpDt As DataTable = Execute_DataTable(SQL, trans, p)
            If tmpDt.Rows.Count > 0 Then
                ServiceCusID = tmpDt.Rows(0)("id")
                re = "true"
                IsRevise = True
            Else
                re = "false"
            End If
            tmpDt.Dispose()
        Else
            ServiceCusID = dt.Rows(0)("id")
            SQL = "SELECT * FROM SERVICE_CUS_REVISE WHERE SERVICE_ID = @_SERVICE_CUS_ID ORDER BY REVISE DESC"
            ReDim p(1)
            p(0) = SetInt("@_SERVICE_CUS_ID", ServiceCusID)

            Dim DT_C_REV As DataTable = Execute_DataTable(SQL, trans, p)
            Dim _price As String = DT_C_REV.Rows(0).Item("PRICE").ToString()
            _revise = Convert.ToInt32(DT_C_REV.Rows(0).Item("REVISE")) + 1
            CusReviseLastID = Convert.ToInt64(DT_C_REV.Rows(0).Item("id"))
            If _price <> Price Then
                'ถ้ามีการเปลี่ยนแปลงราคา
                SQL = "Update SERVICE_CUS Set PRICE=@_PRICE,UNIT=@_UNIT" & _
                   ",REVISE=@_REVISE,REMARK=@_REMARK" & _
                   ",QDATE=@_QDATE, qt_from=@_QT_FROM, qt_no=@_QT_NO, qt_status=@_QT_STATUS" & _
                   ",IMPORT_DATE=getdate(),UPDATE_BY=@_UPDATE_BY,UPDATE_DATE=getdate() " & _
                   " Where ID = @_SERVICE_CUS_ID"

                ReDim p(10)
                p(0) = SetText("@_PRICE", Price)
                p(1) = SetText("@_UNIT", UnitText)
                p(2) = SetText("@_REVISE", _revise)
                p(3) = SetText("@_REMARK", Remarks)
                p(4) = SetDateTime("@_QDATE", QDate)
                p(5) = SetInt("@_UPDATE_BY", myUser.user_id)
                p(6) = SetInt("@_SERVICE_CUS_ID", ServiceCusID)
                p(7) = SetText("@_QT_FROM", QtFrom)
                p(8) = SetText("@_QT_NO", QtNo)
                p(9) = SetText("@_QT_STATUS", QtStatus)

                re = Execute_Command(SQL, trans, p)
                IsRevise = True
            Else
                SQL = "update SERVICE_CUS"
                SQL += " set qt_from=@_QT_FROM, qt_no=@_QT_NO, qt_status=@_QT_STATUS "
                SQL += " ,UPDATE_BY=@_UPDATE_BY,UPDATE_DATE=getdate() "
                SQL += " Where ID = @_SERVICE_CUS_ID"

                ReDim p(5)
                p(0) = SetInt("@_UPDATE_BY", myUser.user_id)
                p(1) = SetInt("@_SERVICE_CUS_ID", ServiceCusID)
                p(2) = SetText("@_QT_FROM", QtFrom)
                p(3) = SetText("@_QT_NO", QtNo)
                p(4) = SetText("@_QT_STATUS", QtStatus)

                re = Execute_Command(SQL, trans, p)
                IsRevise = False
            End If
        End If

        If re = "true" Then
            If IsRevise = True Then
                SQL = "Insert Into SERVICE_CUS_REVISE(ID,SERVICE_ID,PRICE,UNIT,REVISE,REMARK,QDATE,UPDATE_BY,UPDATE_DATE, qt_from, qt_no, qt_status)"
                SQL &= " Values((select max(id)+1 from SERVICE_CUS_REVISE),@_SERVICE_CUS_ID,@_PRICE" & _
                    ",@_UNIT,@_REVISE,@_REMARK,@_QDATE,@_UPDATE_BY,getdate(), @_QT_FROM, @_QT_NO, @_QT_STATUS)"

                ReDim p(10)
                p(0) = SetInt("@_SERVICE_CUS_ID", ServiceCusID)
                p(1) = SetText("@_PRICE", Price)
                p(2) = SetText("@_UNIT", UnitText)
                p(3) = SetInt("@_REVISE", _revise)
                p(4) = SetText("@_REMARK", Remarks)
                p(5) = SetDateTime("@_QDATE", QDate)
                p(6) = SetInt("@_UPDATE_BY", myUser.user_id)
                p(7) = SetText("@_QT_FROM", QtFrom)
                p(8) = SetText("@_QT_NO", QtNo)
                p(9) = SetText("@_QT_STATUS", QtStatus)

                re = Execute_Command(SQL, trans, p)
                If re <> "true" Then
                    ret = False
                Else
                    ret = True
                End If
            Else
                'ถ้าไม่มีการเปลี่ยนแปลงราคาให้ Update Status ของการ Revise ครั้งล่าสุด
                If CusReviseLastID > 0 Then
                    SQL = "update SERVICE_CUS_REVISE"
                    SQL += " set qt_from=@_QT_FROM, qt_no=@_QT_NO, qt_status=@_QT_STATUS "
                    SQL += " ,UPDATE_BY=@_UPDATE_BY,UPDATE_DATE=getdate() "
                    SQL += " Where ID = @_ID"

                    ReDim p(5)
                    p(0) = SetInt("@_UPDATE_BY", myUser.user_id)
                    p(1) = SetInt("@_ID", CusReviseLastID)
                    p(2) = SetText("@_QT_FROM", QtFrom)
                    p(3) = SetText("@_QT_NO", QtNo)
                    p(4) = SetText("@_QT_STATUS", QtStatus)

                    re = Execute_Command(SQL, trans, p)
                    If re <> "true" Then
                        ret = False
                    Else
                        ret = True
                    End If
                End If
            End If
        Else
            ret = False
        End If

        Return ret
    End Function



#Region "SQL Execution"
    Public Function Execute_DataTable(sql As String) As DataTable
        Dim DT As New DataTable
        Try
            Dim DA As New SqlDataAdapter(sql, ConnStr)
            DA.Fill(DT)
        Catch ex As Exception
            DT = New DataTable
        End Try

        Return DT
    End Function

    Public Function Execute_DataTable(sql As String, cmdParms() As SqlParameter) As DataTable
        Dim DT As New DataTable
        Try
            Dim DA As New SqlDataAdapter(sql, ConnStr)
            If cmdParms IsNot Nothing Then
                For Each parm As SqlParameter In cmdParms
                    Try
                        If parm IsNot Nothing Then
                            DA.SelectCommand.Parameters.Add(parm)
                        End If
                    Catch ex As ArgumentNullException
                        'Throw New ApplicationException(ErrorNullParameter, ex)
                    Catch ex As ArgumentException
                        'Throw New ApplicationException(ErrorDuplicateParameter, ex)
                    End Try
                Next
            End If
            DA.Fill(DT)
            DA.Dispose()
        Catch ex As Exception
            DT = New DataTable
        End Try

        Return DT
    End Function

    Public Function Execute_DataTable(sql As String, trans As SqlTransaction, cmdParms() As SqlParameter) As DataTable
        Dim cmd As New SqlCommand
        Dim adapter As New SqlDataAdapter
        adapter.SelectCommand = cmd
        Dim LetClose As Boolean = False
        Dim dt As New DataTable
        Try
            Dim conn As SqlConnection
            If trans IsNot Nothing Then
                conn = trans.Connection
            End If

            BuildCommand(cmd, conn, trans, CommandType.Text, sql, cmdParms)
            adapter.Fill(dt)
            adapter.Dispose()
            If LetClose = True Then
                cmd.Dispose()
                conn.Close()
                SqlConnection.ClearAllPools()
            End If
        Catch ex As ApplicationException
            Dim _err As String = sql & " $$$$ " & ex.Message
            adapter.Dispose()
        Catch ex As SqlException
            Dim _err As String = sql & " $$$$ " & ex.Message
            adapter.Dispose()
        Catch ex As Exception
            Dim _err As String = sql & " $$$$ " & ex.Message
            adapter.Dispose()
        End Try

        Return dt
    End Function

    Private Sub BuildCommand(ByVal cmd As SqlCommand, ByVal conn As SqlConnection, ByVal trans As SqlTransaction, ByVal cmdType As CommandType, ByVal cmdText As String, ByVal cmdParms() As SqlParameter)
        If conn.State <> ConnectionState.Open Then
            Try
                conn.Open()
            Catch ex As SqlException
            Catch ex As ApplicationException
            Catch ex As Exception
            End Try
        End If

        Try
            cmd.Connection = conn
        Catch ex As Exception
        End Try
        cmd.CommandText = cmdText

        If trans IsNot Nothing Then
            cmd.Transaction = trans
        End If

        Try
            cmd.CommandType = cmdType
            cmd.CommandTimeout = 240
        Catch ex As ArgumentException
        End Try

        If cmdParms IsNot Nothing Then
            For Each parm As SqlParameter In cmdParms

                Try
                    If parm IsNot Nothing Then
                        cmd.Parameters.Add(parm)
                    End If
                Catch ex As ArgumentNullException

                Catch ex As ArgumentException

                End Try
            Next
        End If
    End Sub

    Public Function Execute_Command(ByVal Command As String, cmdParms() As SqlParameter) As String
        Dim ret As String = "false"
        Try
            Dim Conn As New SqlConnection(ConnStr)
            Conn.Open()

            If Conn.State <> ConnectionState.Open Then
                ret = "false|Connection State is close."
                Return ret
            End If

            Dim trans As SqlTransaction
            trans = Conn.BeginTransaction
            Try
                Dim Comm As New SqlCommand
                With Comm
                    .Connection = Conn
                    .Transaction = trans
                    .CommandType = CommandType.Text
                    .CommandText = Command

                    If cmdParms IsNot Nothing Then
                        For Each parm As SqlParameter In cmdParms
                            Try
                                If parm IsNot Nothing Then
                                    .Parameters.Add(parm)
                                End If
                            Catch ex As ArgumentNullException
                                'Throw New ApplicationException(ErrorNullParameter, ex)
                            Catch ex As ArgumentException
                                'Throw New ApplicationException(ErrorDuplicateParameter, ex)
                            End Try
                        Next
                    End If

                    .ExecuteNonQuery()
                    .Dispose()
                End With

                trans.Commit()
                Conn.Close()
                Conn.Dispose()

                ret = "true"
            Catch ex As Exception
                trans.Rollback()
                ret = "false|Exception " & ex.Message
            End Try

        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try
        Return ret
    End Function

    Public Function Execute_Command(ByVal sql As String, trans As SqlTransaction, cmdParms() As SqlParameter) As String
        Dim ret As String = "false"
        Dim command As New SqlCommand

        Try
            Dim conn As SqlConnection
            If trans IsNot Nothing Then
                conn = trans.Connection
            End If

            If trans IsNot Nothing And conn Is Nothing Then
                conn = trans.Connection
            End If

            BuildCommand(command, trans.Connection, trans, CommandType.Text, sql, cmdParms)
            Dim r As Integer = command.ExecuteNonQuery()

            ret = "true"
        Catch ex As ApplicationException
            ret = "false|ApplicationException " & ex.Message
        Catch ex As SqlException
            ret = "false|SqlException " & ex.Message
        Catch ex As Exception
            ret = "false|Exception " & ex.Message
        End Try

        Return ret
    End Function

    Public Sub Execute_Command(ByVal Command As String)
        Dim Conn As New SqlConnection(ConnStr)
        Dim Comm As New SqlCommand
        Conn.Open()
        With Comm
            .Connection = Conn
            .CommandType = CommandType.Text
            .CommandText = Command
            .ExecuteNonQuery()
            .Dispose()
        End With
        Conn.Close()
        Conn.Dispose()
    End Sub
#End Region

#Region "Set SQL Parameter"

    Private Function SetParameter(ParameterName As String, pType As SqlDbType, ParameterValue As Object) As SqlParameter
        Dim p As New SqlParameter(ParameterName, pType)
        If ParameterValue Is Nothing Then
            p.Value = DBNull.Value
        ElseIf Convert.IsDBNull(ParameterValue) = False Then
            p.Value = ParameterValue
        Else
            p.Value = DBNull.Value
        End If
        Return p


    End Function

    Public Function SetText(ParameterName As String, ParameterValue As String) As SqlParameter
        If ParameterValue.Trim = "" Then
            Return SetParameter(ParameterName, SqlDbType.VarChar, DBNull.Value)
        Else
            Return SetParameter(ParameterName, SqlDbType.VarChar, ParameterValue)
        End If
    End Function
    Public Function SetText(ParameterName As String, ParameterValue As Object) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.VarChar, ParameterValue)
    End Function
    Public Function SetInt(ParameterName As String, ParameterValue As Integer) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.Int, ParameterValue)
    End Function
    Public Function SetInt(ParameterName As String, ParameterValue As String) As SqlParameter
        If ParameterValue.Trim = "" Then
            Return SetParameter(ParameterName, SqlDbType.Int, DBNull.Value)
        Else
            Return SetParameter(ParameterName, SqlDbType.Int, ParameterValue)
        End If
    End Function
    Public Function SetBigInt(ParameterName As String, ParameterValue As Int64) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.BigInt, ParameterValue)
    End Function
    Public Function SetBigInt(ParameterName As String, ParameterValue As String) As SqlParameter
        If ParameterValue.Trim = "" Then
            Return SetParameter(ParameterName, SqlDbType.BigInt, DBNull.Value)
        Else
            Return SetParameter(ParameterName, SqlDbType.BigInt, ParameterValue)
        End If
    End Function

    Public Function SetFloat(ParameterName As String, ParameterValue As Double) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.Float, ParameterValue)
    End Function

    Public Function SetDouble(ParameterName As String, ParameterValue As Double) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.Decimal, ParameterValue)
    End Function

    Public Function SetDateTime(ParameterName As String, ParameterValue As DateTime) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.DateTime, ParameterValue)
    End Function

    Public Function SetDateTime(ParameterName As String, ParameterValue As Object) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.DateTime, ParameterValue)
    End Function

    Public Function SetImage(ParameterName As String, ParameterValue As Image) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.Image, ParameterValue)
    End Function

    Public Function SetImage(ParameterName As String, ParameterValue As Byte()) As SqlParameter
        Return SetParameter(ParameterName, SqlDbType.Image, ParameterValue)
    End Function
#End Region

#Region "Insert Error Log"
    Public Sub CreateLogTrans(LogMsg As String)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber
        CreateLogData(ClassName, FunctionName, LineNo, LogMsg, "TransLog")
    End Sub

    Public Sub CreateLogError(LogMsg As String)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        CreateLogData(ClassName, FunctionName, LineNo, LogMsg, "ErrorLog")
    End Sub

    Public Sub CreateLogException(ex As Exception)
        Dim frame As StackFrame = New StackFrame(1, True)
        Dim ClassName As String = frame.GetMethod.ReflectedType.Name
        Dim FunctionName As String = frame.GetMethod.Name
        Dim LineNo As Integer = frame.GetFileLineNumber

        CreateLogData(ClassName, FunctionName, LineNo, "Exception : " & ex.Message & vbNewLine & ex.StackTrace, "ExceptionLog")
    End Sub


    Private Sub CreateLogData(ClassName As String, FunctionName As String, LineNo As Int16, LogMsg As String, LogType As String)
        ''### Current Class and Function name
        'Dim m As MethodBase = MethodBase.GetCurrentMethod()
        'Dim ThisClassName As String = m.ReflectedType.Name
        'Dim ThisFunctionName As String = m.Name

        Try
            Dim sql As String = "insert into tb_log_error(created_by,created_date, class_name, function_name, client_ip, "
            sql += " [user_id],log_type, error_time, error_desc)"
            sql += " values(@_CREATED_BY, getdate(), @_CLASS_NAME, @_FUNCTION_NAME, @_CLIENT_IP, "
            sql += " @_USER_ID,@_LOG_TYPE, getdate(), @_ERROR_DESC) "

            Dim p(7) As SqlParameter
            p(0) = SetText("@_CREATED_BY", myUser.fulllname)
            p(1) = SetText("@_CLASS_NAME", ClassName)
            p(2) = SetText("@_FUNCTION_NAME", FunctionName)
            p(3) = SetText("@_CLIENT_IP", GetIPAddress())
            p(4) = SetInt("@_USER_ID", myUser.user_id)
            p(5) = SetText("@_LOG_TYPE", LogType)
            p(6) = SetText("@_ERROR_DESC", LogMsg)

            Execute_Command(sql, p)
        Catch ex As Exception

        End Try
    End Sub


#End Region

#Region "Overwrite Quotation"
    Public Function SaveOverwriteQuotation(QtTable As String, QtID As Long, OverwriteReason As String) As String
        Dim ret As String = "false"
        Dim sql As String = "insert into QUOTATION_OVERWRITE_REASON(created_by,ref_qt_table,ref_qt_id,overwrite_date,user_id_sale,overwrite_reason)"
        sql += " values(@_CREATED_BY,@_QUOTATION_TABLE,  @_QUOTATION_ID,getdate(),@_USER_ID_SALE, @_OVERWRITE_REASON)"

        Dim p(5) As SqlParameter
        p(0) = SetText("@_CREATED_BY", myUser.fulllname)
        p(1) = SetText("@_QUOTATION_TABLE", QtTable)
        p(2) = SetBigInt("@_QUOTATION_ID", QtID)
        p(3) = SetInt("@_USER_ID_SALE", myUser.user_id)
        p(4) = SetText("@_OVERWRITE_REASON", OverwriteReason)

        ret = Execute_Command(sql, p)
        Return ret
    End Function
#End Region

    Private Function GetIPAddress() As String
        Dim IP As String = ""
        Dim oAddr As System.Net.IPAddress
        With System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName())
            oAddr = New System.Net.IPAddress(.AddressList(0).Address)
            IP = oAddr.ToString
        End With

        Return IP
    End Function

    Public Enum CustomerContactType
        Operation = 1
        Account = 2
    End Enum
End Module

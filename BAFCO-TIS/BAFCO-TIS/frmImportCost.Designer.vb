﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmImportCost
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmImportCost))
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.Grid = New System.Windows.Forms.DataGridView()
        Me.txtPage = New System.Windows.Forms.TextBox()
        Me.lblTotalPage = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cbbPageSize = New System.Windows.Forms.ComboBox()
        Me.lbl = New System.Windows.Forms.Label()
        Me.pnlProcess = New System.Windows.Forms.Panel()
        Me.lblProcess = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.pgb = New System.Windows.Forms.ProgressBar()
        Me.btnClear = New System.Windows.Forms.PictureBox()
        Me.cbR = New System.Windows.Forms.PictureBox()
        Me.cbB = New System.Windows.Forms.PictureBox()
        Me.cbW = New System.Windows.Forms.PictureBox()
        Me.btnUpdate = New System.Windows.Forms.PictureBox()
        Me.btnDelete = New System.Windows.Forms.PictureBox()
        Me.btnAdd = New System.Windows.Forms.PictureBox()
        Me.btnLast = New System.Windows.Forms.PictureBox()
        Me.btnNext = New System.Windows.Forms.PictureBox()
        Me.btnBack = New System.Windows.Forms.PictureBox()
        Me.btnFirst = New System.Windows.Forms.PictureBox()
        Me.btnBrowse = New System.Windows.Forms.PictureBox()
        Me.btnImport = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblRec = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.IM_FROM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_TO = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_VEH = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_SUP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_COST = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_COST_UNIT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_SUP_QDATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_SUP_REMARK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_CUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_VALUE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_VALUE_Unit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_CUS_QDATE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_CUS_REMARK = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chk_From = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chk_To = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chk_Sup = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chk_Veh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.chk_Cus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Rec_No = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.From_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.From_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.To_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.To_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sup_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sup_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Veh_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Veh_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cost_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cost_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cost_Unit_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cost_Unit_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sup_QDate_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sup_QDate_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sup_Rem_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sup_Rem_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cus_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cus_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Val_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Val_Unit_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Val_Unit_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Val_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cus_QDate_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cus_QDate_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cus_Rem_Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cus_Rem_ToolTip = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_FROM_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_TO_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_VEH_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_SUP_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IM_CUS_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlProcess.SuspendLayout()
        CType(Me.btnClear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cbW, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnUpdate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnAdd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnLast, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnNext, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnBack, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnFirst, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnBrowse, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'ofd
        '
        Me.ofd.FileName = "OpenFileDialog1"
        '
        'txtPath
        '
        Me.txtPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPath.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPath.Location = New System.Drawing.Point(184, 6)
        Me.txtPath.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(31, 30)
        Me.txtPath.TabIndex = 0
        '
        'Grid
        '
        Me.Grid.AllowUserToAddRows = False
        Me.Grid.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Honeydew
        Me.Grid.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.Grid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.Grid.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.Grid.BackgroundColor = System.Drawing.Color.White
        Me.Grid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Grid.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IM_FROM, Me.IM_TO, Me.IM_VEH, Me.IM_SUP, Me.IM_COST, Me.IM_COST_UNIT, Me.IM_SUP_QDATE, Me.IM_SUP_REMARK, Me.IM_CUS, Me.IM_VALUE, Me.IM_VALUE_Unit, Me.IM_CUS_QDATE, Me.IM_CUS_REMARK, Me.chk_From, Me.chk_To, Me.chk_Sup, Me.chk_Veh, Me.chk_Cus, Me.Rec_No, Me.From_Status, Me.From_ToolTip, Me.To_Status, Me.To_ToolTip, Me.Sup_Status, Me.Sup_ToolTip, Me.Veh_Status, Me.Veh_ToolTip, Me.Cost_Status, Me.Cost_ToolTip, Me.Cost_Unit_Status, Me.Cost_Unit_ToolTip, Me.Sup_QDate_Status, Me.Sup_QDate_ToolTip, Me.Sup_Rem_Status, Me.Sup_Rem_ToolTip, Me.Cus_Status, Me.Cus_ToolTip, Me.Val_Status, Me.Val_Unit_Status, Me.Val_Unit_ToolTip, Me.Val_ToolTip, Me.Cus_QDate_Status, Me.Cus_QDate_ToolTip, Me.Cus_Rem_Status, Me.Cus_Rem_ToolTip, Me.IM_STATUS, Me.IM_FROM_ID, Me.IM_TO_ID, Me.IM_VEH_ID, Me.IM_SUP_ID, Me.IM_CUS_ID})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.Padding = New System.Windows.Forms.Padding(10, 0, 10, 0)
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Grid.DefaultCellStyle = DataGridViewCellStyle3
        Me.Grid.Location = New System.Drawing.Point(18, 122)
        Me.Grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Grid.Name = "Grid"
        Me.Grid.RowHeadersWidth = 24
        Me.Grid.Size = New System.Drawing.Size(1205, 444)
        Me.Grid.TabIndex = 49
        '
        'txtPage
        '
        Me.txtPage.Enabled = False
        Me.txtPage.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPage.Location = New System.Drawing.Point(94, 6)
        Me.txtPage.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.txtPage.Name = "txtPage"
        Me.txtPage.Size = New System.Drawing.Size(68, 30)
        Me.txtPage.TabIndex = 55
        Me.txtPage.Text = "0"
        Me.txtPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblTotalPage
        '
        Me.lblTotalPage.AutoSize = True
        Me.lblTotalPage.Enabled = False
        Me.lblTotalPage.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblTotalPage.Location = New System.Drawing.Point(186, 9)
        Me.lblTotalPage.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblTotalPage.Name = "lblTotalPage"
        Me.lblTotalPage.Size = New System.Drawing.Size(23, 25)
        Me.lblTotalPage.TabIndex = 56
        Me.lblTotalPage.Text = "0"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(192, 8)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 35)
        Me.Label2.TabIndex = 64
        Me.Label2.Text = "ข้อมูลที่สมบูรณ์"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(398, 8)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 25)
        Me.Label3.TabIndex = 65
        Me.Label3.Text = "ข้อมูลใหม่"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(561, 8)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(119, 25)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "ข้อมูลที่มีปัญหา"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(336, 9)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(169, 25)
        Me.Label5.TabIndex = 71
        Me.Label5.Text = "Display per page :"
        '
        'cbbPageSize
        '
        Me.cbbPageSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbPageSize.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbPageSize.FormattingEnabled = True
        Me.cbbPageSize.Items.AddRange(New Object() {"25", "50", "100", "500", "1000"})
        Me.cbbPageSize.Location = New System.Drawing.Point(525, 5)
        Me.cbbPageSize.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cbbPageSize.Name = "cbbPageSize"
        Me.cbbPageSize.Size = New System.Drawing.Size(108, 33)
        Me.cbbPageSize.TabIndex = 72
        '
        'lbl
        '
        Me.lbl.AutoSize = True
        Me.lbl.Enabled = False
        Me.lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lbl.Location = New System.Drawing.Point(170, 9)
        Me.lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(18, 25)
        Me.lbl.TabIndex = 73
        Me.lbl.Text = "/"
        '
        'pnlProcess
        '
        Me.pnlProcess.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnlProcess.BackColor = System.Drawing.Color.Gainsboro
        Me.pnlProcess.Controls.Add(Me.lblProcess)
        Me.pnlProcess.Controls.Add(Me.Label1)
        Me.pnlProcess.Controls.Add(Me.pgb)
        Me.pnlProcess.Location = New System.Drawing.Point(346, 264)
        Me.pnlProcess.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.pnlProcess.Name = "pnlProcess"
        Me.pnlProcess.Size = New System.Drawing.Size(548, 114)
        Me.pnlProcess.TabIndex = 74
        Me.pnlProcess.Visible = False
        '
        'lblProcess
        '
        Me.lblProcess.AutoSize = True
        Me.lblProcess.Location = New System.Drawing.Point(114, 17)
        Me.lblProcess.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblProcess.Name = "lblProcess"
        Me.lblProcess.Size = New System.Drawing.Size(40, 20)
        Me.lblProcess.TabIndex = 66
        Me.lblProcess.Text = "1/40"
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 14)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 35)
        Me.Label1.TabIndex = 65
        Me.Label1.Text = "Process"
        '
        'pgb
        '
        Me.pgb.Location = New System.Drawing.Point(16, 54)
        Me.pgb.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.pgb.Name = "pgb"
        Me.pgb.Size = New System.Drawing.Size(514, 40)
        Me.pgb.TabIndex = 0
        '
        'btnClear
        '
        Me.btnClear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnClear.Image = CType(resources.GetObject("btnClear.Image"), System.Drawing.Image)
        Me.btnClear.Location = New System.Drawing.Point(168, 5)
        Me.btnClear.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(36, 37)
        Me.btnClear.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClear.TabIndex = 70
        Me.btnClear.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnClear, "Clear selected cell")
        '
        'cbR
        '
        Me.cbR.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cbR.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_g
        Me.cbR.Location = New System.Drawing.Point(514, 5)
        Me.cbR.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cbR.Name = "cbR"
        Me.cbR.Size = New System.Drawing.Size(36, 37)
        Me.cbR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cbR.TabIndex = 69
        Me.cbR.TabStop = False
        '
        'cbB
        '
        Me.cbB.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cbB.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_g
        Me.cbB.Location = New System.Drawing.Point(351, 5)
        Me.cbB.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cbB.Name = "cbB"
        Me.cbB.Size = New System.Drawing.Size(36, 37)
        Me.cbB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cbB.TabIndex = 68
        Me.cbB.TabStop = False
        '
        'cbW
        '
        Me.cbW.Cursor = System.Windows.Forms.Cursors.Hand
        Me.cbW.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_g
        Me.cbW.Location = New System.Drawing.Point(146, 5)
        Me.cbW.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cbW.Name = "cbW"
        Me.cbW.Size = New System.Drawing.Size(36, 37)
        Me.cbW.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.cbW.TabIndex = 67
        Me.cbW.TabStop = False
        '
        'btnUpdate
        '
        Me.btnUpdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdate.Image = CType(resources.GetObject("btnUpdate.Image"), System.Drawing.Image)
        Me.btnUpdate.Location = New System.Drawing.Point(114, 5)
        Me.btnUpdate.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(36, 37)
        Me.btnUpdate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnUpdate.TabIndex = 60
        Me.btnUpdate.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnUpdate, "Edit selected cells")
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.Location = New System.Drawing.Point(60, 5)
        Me.btnDelete.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(36, 37)
        Me.btnDelete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnDelete.TabIndex = 59
        Me.btnDelete.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnDelete, "Delete selected record(s)")
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(6, 5)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(36, 37)
        Me.btnAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnAdd.TabIndex = 58
        Me.btnAdd.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnAdd, "Add record")
        '
        'btnLast
        '
        Me.btnLast.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLast.Enabled = False
        Me.btnLast.Image = Global.BAFCO_TIS.My.Resources.Resources.btnLsat
        Me.btnLast.Location = New System.Drawing.Point(278, 6)
        Me.btnLast.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(34, 35)
        Me.btnLast.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnLast.TabIndex = 54
        Me.btnLast.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnLast, "Last page")
        '
        'btnNext
        '
        Me.btnNext.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNext.Enabled = False
        Me.btnNext.Image = Global.BAFCO_TIS.My.Resources.Resources.btnNext
        Me.btnNext.Location = New System.Drawing.Point(234, 6)
        Me.btnNext.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(34, 35)
        Me.btnNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnNext.TabIndex = 53
        Me.btnNext.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnNext, "Next")
        '
        'btnBack
        '
        Me.btnBack.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBack.Enabled = False
        Me.btnBack.Image = Global.BAFCO_TIS.My.Resources.Resources.btnBack
        Me.btnBack.Location = New System.Drawing.Point(51, 6)
        Me.btnBack.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(34, 35)
        Me.btnBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnBack.TabIndex = 52
        Me.btnBack.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBack, "Back")
        '
        'btnFirst
        '
        Me.btnFirst.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnFirst.Enabled = False
        Me.btnFirst.Image = Global.BAFCO_TIS.My.Resources.Resources.btnFirst
        Me.btnFirst.Location = New System.Drawing.Point(8, 6)
        Me.btnFirst.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(34, 35)
        Me.btnFirst.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnFirst.TabIndex = 51
        Me.btnFirst.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnFirst, "First page")
        '
        'btnBrowse
        '
        Me.btnBrowse.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBrowse.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnBrowse.Image = Global.BAFCO_TIS.My.Resources.Resources.ExcelFind
        Me.btnBrowse.Location = New System.Drawing.Point(220, 0)
        Me.btnBrowse.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(45, 46)
        Me.btnBrowse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnBrowse.TabIndex = 48
        Me.btnBrowse.TabStop = False
        Me.ToolTip1.SetToolTip(Me.btnBrowse, "Brown file import")
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImport.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnImport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnImport.Image = Global.BAFCO_TIS.My.Resources.Resources.ExcelMaster
        Me.btnImport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImport.Location = New System.Drawing.Point(983, 578)
        Me.btnImport.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(240, 57)
        Me.btnImport.TabIndex = 47
        Me.btnImport.Text = "Save to database"
        Me.btnImport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.SteelBlue
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(0, 0)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(134, 46)
        Me.Label7.TabIndex = 76
        Me.Label7.Text = "Filter"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.cbW)
        Me.Panel1.Controls.Add(Me.cbB)
        Me.Panel1.Controls.Add(Me.cbR)
        Me.Panel1.Location = New System.Drawing.Point(294, 63)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(707, 48)
        Me.Panel1.TabIndex = 77
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.btnClear)
        Me.Panel2.Controls.Add(Me.btnAdd)
        Me.Panel2.Controls.Add(Me.btnDelete)
        Me.Panel2.Controls.Add(Me.btnUpdate)
        Me.Panel2.Location = New System.Drawing.Point(1011, 63)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(210, 48)
        Me.Panel2.TabIndex = 78
        '
        'lblRec
        '
        Me.lblRec.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblRec.AutoSize = True
        Me.lblRec.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec.Location = New System.Drawing.Point(18, 588)
        Me.lblRec.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblRec.Name = "lblRec"
        Me.lblRec.Size = New System.Drawing.Size(159, 25)
        Me.lblRec.TabIndex = 74
        Me.lblRec.Text = "Total   0   Record"
        '
        'ToolTip1
        '
        Me.ToolTip1.AutoPopDelay = 10000
        Me.ToolTip1.InitialDelay = 500
        Me.ToolTip1.ReshowDelay = 100
        '
        'Panel3
        '
        Me.Panel3.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.btnFirst)
        Me.Panel3.Controls.Add(Me.btnBack)
        Me.Panel3.Controls.Add(Me.btnNext)
        Me.Panel3.Controls.Add(Me.btnLast)
        Me.Panel3.Controls.Add(Me.Label5)
        Me.Panel3.Controls.Add(Me.lbl)
        Me.Panel3.Controls.Add(Me.cbbPageSize)
        Me.Panel3.Controls.Add(Me.txtPage)
        Me.Panel3.Controls.Add(Me.lblTotalPage)
        Me.Panel3.Location = New System.Drawing.Point(296, 581)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(644, 50)
        Me.Panel3.TabIndex = 79
        '
        'Panel4
        '
        Me.Panel4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.txtPath)
        Me.Panel4.Controls.Add(Me.btnBrowse)
        Me.Panel4.Location = New System.Drawing.Point(18, 63)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(267, 48)
        Me.Panel4.TabIndex = 80
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.SteelBlue
        Me.Label6.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(0, 0)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(176, 46)
        Me.Label6.TabIndex = 77
        Me.Label6.Text = "Brown File"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Green
        Me.Label8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(0, 0)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(1241, 52)
        Me.Label8.TabIndex = 81
        Me.Label8.Text = "Transportation  Cost Import Data"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'IM_FROM
        '
        Me.IM_FROM.DataPropertyName = "IM_FROM"
        Me.IM_FROM.HeaderText = "From"
        Me.IM_FROM.Name = "IM_FROM"
        Me.IM_FROM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_FROM.Width = 83
        '
        'IM_TO
        '
        Me.IM_TO.DataPropertyName = "IM_TO"
        Me.IM_TO.HeaderText = "To"
        Me.IM_TO.Name = "IM_TO"
        Me.IM_TO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_TO.Width = 62
        '
        'IM_VEH
        '
        Me.IM_VEH.DataPropertyName = "IM_VEH"
        Me.IM_VEH.HeaderText = "Vehicle"
        Me.IM_VEH.Name = "IM_VEH"
        Me.IM_VEH.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_VEH.Width = 103
        '
        'IM_SUP
        '
        Me.IM_SUP.DataPropertyName = "IM_SUP"
        Me.IM_SUP.HeaderText = "Supplier"
        Me.IM_SUP.Name = "IM_SUP"
        Me.IM_SUP.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_SUP.Width = 110
        '
        'IM_COST
        '
        Me.IM_COST.DataPropertyName = "IM_COST"
        Me.IM_COST.HeaderText = "Cost"
        Me.IM_COST.Name = "IM_COST"
        Me.IM_COST.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_COST.Width = 79
        '
        'IM_COST_UNIT
        '
        Me.IM_COST_UNIT.DataPropertyName = "IM_COST_UNIT"
        Me.IM_COST_UNIT.HeaderText = "Unit"
        Me.IM_COST_UNIT.Name = "IM_COST_UNIT"
        Me.IM_COST_UNIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_COST_UNIT.Width = 72
        '
        'IM_SUP_QDATE
        '
        Me.IM_SUP_QDATE.DataPropertyName = "IM_SUP_QDATE"
        Me.IM_SUP_QDATE.HeaderText = "SupplierQDate"
        Me.IM_SUP_QDATE.Name = "IM_SUP_QDATE"
        Me.IM_SUP_QDATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_SUP_QDATE.Width = 167
        '
        'IM_SUP_REMARK
        '
        Me.IM_SUP_REMARK.DataPropertyName = "IM_SUP_REMARK"
        Me.IM_SUP_REMARK.HeaderText = "SupplierRemark"
        Me.IM_SUP_REMARK.Name = "IM_SUP_REMARK"
        Me.IM_SUP_REMARK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_SUP_REMARK.Width = 177
        '
        'IM_CUS
        '
        Me.IM_CUS.DataPropertyName = "IM_CUS"
        Me.IM_CUS.HeaderText = "Customer"
        Me.IM_CUS.Name = "IM_CUS"
        Me.IM_CUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_CUS.Width = 123
        '
        'IM_VALUE
        '
        Me.IM_VALUE.DataPropertyName = "IM_VALUE"
        Me.IM_VALUE.HeaderText = "Value"
        Me.IM_VALUE.Name = "IM_VALUE"
        Me.IM_VALUE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_VALUE.Width = 89
        '
        'IM_VALUE_Unit
        '
        Me.IM_VALUE_Unit.DataPropertyName = "IM_VALUE_Unit"
        Me.IM_VALUE_Unit.HeaderText = "Unit"
        Me.IM_VALUE_Unit.Name = "IM_VALUE_Unit"
        Me.IM_VALUE_Unit.Width = 91
        '
        'IM_CUS_QDATE
        '
        Me.IM_CUS_QDATE.DataPropertyName = "IM_CUS_QDATE"
        Me.IM_CUS_QDATE.HeaderText = "CustomerQDate"
        Me.IM_CUS_QDATE.Name = "IM_CUS_QDATE"
        Me.IM_CUS_QDATE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_CUS_QDATE.Width = 180
        '
        'IM_CUS_REMARK
        '
        Me.IM_CUS_REMARK.DataPropertyName = "IM_CUS_REMARK"
        Me.IM_CUS_REMARK.HeaderText = "CustomerRemark"
        Me.IM_CUS_REMARK.Name = "IM_CUS_REMARK"
        Me.IM_CUS_REMARK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.IM_CUS_REMARK.Width = 190
        '
        'chk_From
        '
        Me.chk_From.DataPropertyName = "chk_From"
        Me.chk_From.HeaderText = "chk_From"
        Me.chk_From.Name = "chk_From"
        Me.chk_From.Visible = False
        Me.chk_From.Width = 144
        '
        'chk_To
        '
        Me.chk_To.DataPropertyName = "chk_To"
        Me.chk_To.HeaderText = "chk_To"
        Me.chk_To.Name = "chk_To"
        Me.chk_To.Visible = False
        Me.chk_To.Width = 123
        '
        'chk_Sup
        '
        Me.chk_Sup.DataPropertyName = "chk_Sup"
        Me.chk_Sup.HeaderText = "chk_Sup"
        Me.chk_Sup.Name = "chk_Sup"
        Me.chk_Sup.Visible = False
        Me.chk_Sup.Width = 135
        '
        'chk_Veh
        '
        Me.chk_Veh.DataPropertyName = "chk_Veh"
        Me.chk_Veh.HeaderText = "chk_Veh"
        Me.chk_Veh.Name = "chk_Veh"
        Me.chk_Veh.Visible = False
        Me.chk_Veh.Width = 135
        '
        'chk_Cus
        '
        Me.chk_Cus.DataPropertyName = "chk_Cus"
        Me.chk_Cus.HeaderText = "chk_Cus"
        Me.chk_Cus.Name = "chk_Cus"
        Me.chk_Cus.Visible = False
        Me.chk_Cus.Width = 135
        '
        'Rec_No
        '
        Me.Rec_No.DataPropertyName = "Rec_No"
        Me.Rec_No.HeaderText = "Rec_No"
        Me.Rec_No.Name = "Rec_No"
        Me.Rec_No.Visible = False
        Me.Rec_No.Width = 127
        '
        'From_Status
        '
        Me.From_Status.DataPropertyName = "From_Status"
        Me.From_Status.HeaderText = "From_Status"
        Me.From_Status.Name = "From_Status"
        Me.From_Status.Visible = False
        Me.From_Status.Width = 169
        '
        'From_ToolTip
        '
        Me.From_ToolTip.DataPropertyName = "From_ToolTip"
        Me.From_ToolTip.HeaderText = "From_ToolTip"
        Me.From_ToolTip.Name = "From_ToolTip"
        Me.From_ToolTip.Visible = False
        Me.From_ToolTip.Width = 180
        '
        'To_Status
        '
        Me.To_Status.DataPropertyName = "To_Status"
        Me.To_Status.HeaderText = "To_Status"
        Me.To_Status.Name = "To_Status"
        Me.To_Status.Visible = False
        Me.To_Status.Width = 148
        '
        'To_ToolTip
        '
        Me.To_ToolTip.DataPropertyName = "To_ToolTip"
        Me.To_ToolTip.HeaderText = "To_ToolTip"
        Me.To_ToolTip.Name = "To_ToolTip"
        Me.To_ToolTip.Visible = False
        Me.To_ToolTip.Width = 159
        '
        'Sup_Status
        '
        Me.Sup_Status.DataPropertyName = "Sup_Status"
        Me.Sup_Status.HeaderText = "Sup_Status"
        Me.Sup_Status.Name = "Sup_Status"
        Me.Sup_Status.Visible = False
        Me.Sup_Status.Width = 160
        '
        'Sup_ToolTip
        '
        Me.Sup_ToolTip.DataPropertyName = "Sup_ToolTip"
        Me.Sup_ToolTip.HeaderText = "Sup_ToolTip"
        Me.Sup_ToolTip.Name = "Sup_ToolTip"
        Me.Sup_ToolTip.Visible = False
        Me.Sup_ToolTip.Width = 171
        '
        'Veh_Status
        '
        Me.Veh_Status.DataPropertyName = "Veh_Status"
        Me.Veh_Status.HeaderText = "Veh_Status"
        Me.Veh_Status.Name = "Veh_Status"
        Me.Veh_Status.Visible = False
        Me.Veh_Status.Width = 160
        '
        'Veh_ToolTip
        '
        Me.Veh_ToolTip.DataPropertyName = "Veh_ToolTip"
        Me.Veh_ToolTip.HeaderText = "Veh_ToolTip"
        Me.Veh_ToolTip.Name = "Veh_ToolTip"
        Me.Veh_ToolTip.Visible = False
        Me.Veh_ToolTip.Width = 171
        '
        'Cost_Status
        '
        Me.Cost_Status.DataPropertyName = "Cost_Status"
        Me.Cost_Status.HeaderText = "Cost_Status"
        Me.Cost_Status.Name = "Cost_Status"
        Me.Cost_Status.Visible = False
        Me.Cost_Status.Width = 165
        '
        'Cost_ToolTip
        '
        Me.Cost_ToolTip.DataPropertyName = "Cost_ToolTip"
        Me.Cost_ToolTip.HeaderText = "Cost_ToolTip"
        Me.Cost_ToolTip.Name = "Cost_ToolTip"
        Me.Cost_ToolTip.Visible = False
        Me.Cost_ToolTip.Width = 176
        '
        'Cost_Unit_Status
        '
        Me.Cost_Unit_Status.DataPropertyName = "Cost_Unit_Status"
        Me.Cost_Unit_Status.HeaderText = "Cost_Unit_Status"
        Me.Cost_Unit_Status.Name = "Cost_Unit_Status"
        Me.Cost_Unit_Status.Visible = False
        Me.Cost_Unit_Status.Width = 210
        '
        'Cost_Unit_ToolTip
        '
        Me.Cost_Unit_ToolTip.DataPropertyName = "Cost_Unit_ToolTip"
        Me.Cost_Unit_ToolTip.HeaderText = "Cost_Unit_ToolTip"
        Me.Cost_Unit_ToolTip.Name = "Cost_Unit_ToolTip"
        Me.Cost_Unit_ToolTip.Visible = False
        Me.Cost_Unit_ToolTip.Width = 221
        '
        'Sup_QDate_Status
        '
        Me.Sup_QDate_Status.DataPropertyName = "Sup_QDate_Status"
        Me.Sup_QDate_Status.HeaderText = "Sup_QDate_Status"
        Me.Sup_QDate_Status.Name = "Sup_QDate_Status"
        Me.Sup_QDate_Status.Visible = False
        Me.Sup_QDate_Status.Width = 228
        '
        'Sup_QDate_ToolTip
        '
        Me.Sup_QDate_ToolTip.DataPropertyName = "Sup_QDate_ToolTip"
        Me.Sup_QDate_ToolTip.HeaderText = "Sup_QDate_ToolTip"
        Me.Sup_QDate_ToolTip.Name = "Sup_QDate_ToolTip"
        Me.Sup_QDate_ToolTip.Visible = False
        Me.Sup_QDate_ToolTip.Width = 239
        '
        'Sup_Rem_Status
        '
        Me.Sup_Rem_Status.DataPropertyName = "Sup_Rem_Status"
        Me.Sup_Rem_Status.HeaderText = "Sup_Rem_Status"
        Me.Sup_Rem_Status.Name = "Sup_Rem_Status"
        Me.Sup_Rem_Status.Visible = False
        Me.Sup_Rem_Status.Width = 211
        '
        'Sup_Rem_ToolTip
        '
        Me.Sup_Rem_ToolTip.DataPropertyName = "Sup_Rem_ToolTip"
        Me.Sup_Rem_ToolTip.HeaderText = "Sup_Rem_ToolTip"
        Me.Sup_Rem_ToolTip.Name = "Sup_Rem_ToolTip"
        Me.Sup_Rem_ToolTip.Visible = False
        Me.Sup_Rem_ToolTip.Width = 222
        '
        'Cus_Status
        '
        Me.Cus_Status.DataPropertyName = "Cus_Status"
        Me.Cus_Status.HeaderText = "Cus_Status"
        Me.Cus_Status.Name = "Cus_Status"
        Me.Cus_Status.Visible = False
        Me.Cus_Status.Width = 160
        '
        'Cus_ToolTip
        '
        Me.Cus_ToolTip.DataPropertyName = "Cus_ToolTip"
        Me.Cus_ToolTip.HeaderText = "Cus_ToolTip"
        Me.Cus_ToolTip.Name = "Cus_ToolTip"
        Me.Cus_ToolTip.Visible = False
        Me.Cus_ToolTip.Width = 171
        '
        'Val_Status
        '
        Me.Val_Status.DataPropertyName = "Val_Status"
        Me.Val_Status.HeaderText = "Val_Status"
        Me.Val_Status.Name = "Val_Status"
        Me.Val_Status.Visible = False
        Me.Val_Status.Width = 153
        '
        'Val_Unit_Status
        '
        Me.Val_Unit_Status.DataPropertyName = "Val_Unit_Status"
        Me.Val_Unit_Status.HeaderText = "Val_Unit_Status"
        Me.Val_Unit_Status.Name = "Val_Unit_Status"
        Me.Val_Unit_Status.Visible = False
        Me.Val_Unit_Status.Width = 198
        '
        'Val_Unit_ToolTip
        '
        Me.Val_Unit_ToolTip.DataPropertyName = "Val_Unit_ToolTip"
        Me.Val_Unit_ToolTip.HeaderText = "Val_Unit_ToolTip"
        Me.Val_Unit_ToolTip.Name = "Val_Unit_ToolTip"
        Me.Val_Unit_ToolTip.Visible = False
        Me.Val_Unit_ToolTip.Width = 209
        '
        'Val_ToolTip
        '
        Me.Val_ToolTip.DataPropertyName = "Val_ToolTip"
        Me.Val_ToolTip.HeaderText = "Val_ToolTip"
        Me.Val_ToolTip.Name = "Val_ToolTip"
        Me.Val_ToolTip.Visible = False
        Me.Val_ToolTip.Width = 164
        '
        'Cus_QDate_Status
        '
        Me.Cus_QDate_Status.DataPropertyName = "Cus_QDate_Status"
        Me.Cus_QDate_Status.HeaderText = "Cus_QDate_Status"
        Me.Cus_QDate_Status.Name = "Cus_QDate_Status"
        Me.Cus_QDate_Status.Visible = False
        Me.Cus_QDate_Status.Width = 228
        '
        'Cus_QDate_ToolTip
        '
        Me.Cus_QDate_ToolTip.DataPropertyName = "Cus_QDate_ToolTip"
        Me.Cus_QDate_ToolTip.HeaderText = "Cus_QDate_ToolTip"
        Me.Cus_QDate_ToolTip.Name = "Cus_QDate_ToolTip"
        Me.Cus_QDate_ToolTip.Visible = False
        Me.Cus_QDate_ToolTip.Width = 239
        '
        'Cus_Rem_Status
        '
        Me.Cus_Rem_Status.DataPropertyName = "Cus_Rem_Status"
        Me.Cus_Rem_Status.HeaderText = "Cus_Rem_Status"
        Me.Cus_Rem_Status.Name = "Cus_Rem_Status"
        Me.Cus_Rem_Status.Visible = False
        Me.Cus_Rem_Status.Width = 211
        '
        'Cus_Rem_ToolTip
        '
        Me.Cus_Rem_ToolTip.DataPropertyName = "Cus_Rem_ToolTip"
        Me.Cus_Rem_ToolTip.HeaderText = "Cus_Rem_ToolTip"
        Me.Cus_Rem_ToolTip.Name = "Cus_Rem_ToolTip"
        Me.Cus_Rem_ToolTip.Visible = False
        Me.Cus_Rem_ToolTip.Width = 222
        '
        'IM_STATUS
        '
        Me.IM_STATUS.DataPropertyName = "IM_STATUS"
        Me.IM_STATUS.HeaderText = "IM_STATUS"
        Me.IM_STATUS.Name = "IM_STATUS"
        Me.IM_STATUS.Visible = False
        Me.IM_STATUS.Width = 172
        '
        'IM_FROM_ID
        '
        Me.IM_FROM_ID.DataPropertyName = "IM_FROM_ID"
        Me.IM_FROM_ID.HeaderText = "IM_FROM_ID"
        Me.IM_FROM_ID.Name = "IM_FROM_ID"
        Me.IM_FROM_ID.Visible = False
        Me.IM_FROM_ID.Width = 178
        '
        'IM_TO_ID
        '
        Me.IM_TO_ID.DataPropertyName = "IM_TO_ID"
        Me.IM_TO_ID.HeaderText = "IM_TO_ID"
        Me.IM_TO_ID.Name = "IM_TO_ID"
        Me.IM_TO_ID.Visible = False
        Me.IM_TO_ID.Width = 149
        '
        'IM_VEH_ID
        '
        Me.IM_VEH_ID.DataPropertyName = "IM_VEH_ID"
        Me.IM_VEH_ID.HeaderText = "IM_VEH_ID"
        Me.IM_VEH_ID.Name = "IM_VEH_ID"
        Me.IM_VEH_ID.Visible = False
        Me.IM_VEH_ID.Width = 161
        '
        'IM_SUP_ID
        '
        Me.IM_SUP_ID.DataPropertyName = "IM_SUP_ID"
        Me.IM_SUP_ID.HeaderText = "IM_SUP_ID"
        Me.IM_SUP_ID.Name = "IM_SUP_ID"
        Me.IM_SUP_ID.Visible = False
        Me.IM_SUP_ID.Width = 161
        '
        'IM_CUS_ID
        '
        Me.IM_CUS_ID.DataPropertyName = "IM_CUS_ID"
        Me.IM_CUS_ID.HeaderText = "IM_CUS_ID"
        Me.IM_CUS_ID.Name = "IM_CUS_ID"
        Me.IM_CUS_ID.Visible = False
        Me.IM_CUS_ID.Width = 163
        '
        'frmImportCost
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.ClientSize = New System.Drawing.Size(1241, 644)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.lblRec)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.pnlProcess)
        Me.Controls.Add(Me.Grid)
        Me.Controls.Add(Me.btnImport)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "frmImportCost"
        Me.Text = "Import Transport  Cost"
        CType(Me.Grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlProcess.ResumeLayout(False)
        Me.pnlProcess.PerformLayout()
        CType(Me.btnClear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cbW, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnUpdate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnDelete, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnAdd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnLast, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnNext, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnBack, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnFirst, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnBrowse, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As System.Windows.Forms.PictureBox
    Friend WithEvents Grid As System.Windows.Forms.DataGridView
    Friend WithEvents btnFirst As System.Windows.Forms.PictureBox
    Friend WithEvents btnBack As System.Windows.Forms.PictureBox
    Friend WithEvents btnNext As System.Windows.Forms.PictureBox
    Friend WithEvents btnLast As System.Windows.Forms.PictureBox
    Friend WithEvents txtPage As System.Windows.Forms.TextBox
    Friend WithEvents lblTotalPage As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.PictureBox
    Friend WithEvents btnDelete As System.Windows.Forms.PictureBox
    Friend WithEvents btnUpdate As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbW As System.Windows.Forms.PictureBox
    Friend WithEvents cbB As System.Windows.Forms.PictureBox
    Friend WithEvents cbR As System.Windows.Forms.PictureBox
    Friend WithEvents btnClear As System.Windows.Forms.PictureBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cbbPageSize As System.Windows.Forms.ComboBox
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents pnlProcess As System.Windows.Forms.Panel
    Friend WithEvents lblProcess As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents pgb As System.Windows.Forms.ProgressBar
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblRec As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents IM_FROM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_TO As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_VEH As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_SUP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_COST As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_COST_UNIT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_SUP_QDATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_SUP_REMARK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_CUS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_VALUE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_VALUE_Unit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_CUS_QDATE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_CUS_REMARK As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chk_From As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chk_To As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chk_Sup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chk_Veh As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chk_Cus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Rec_No As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents From_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents From_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents To_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents To_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sup_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sup_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Veh_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Veh_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cost_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cost_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cost_Unit_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cost_Unit_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sup_QDate_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sup_QDate_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sup_Rem_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sup_Rem_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cus_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cus_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Val_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Val_Unit_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Val_Unit_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Val_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cus_QDate_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cus_QDate_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cus_Rem_Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cus_Rem_ToolTip As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_STATUS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_FROM_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_TO_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_VEH_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_SUP_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IM_CUS_ID As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Imports System.Data.SqlClient

Public Class UCMainQuotationDescription

    Public QTCategoryID As Long = 0
    Public Event HeightChanged(h As Integer)

    Public WriteOnly Property EnableDescription As Boolean
        Set(value As Boolean)
            btnAddDescription.Visible = value

            If flpDescription.Controls.Count > 0 Then
                For Each ctl As UCMainQuotationDescriptionItem In flpDescription.Controls
                    ctl.EnableDescriptionItem = value
                Next
            End If
        End Set
    End Property


    Public Sub AddDescriptionItem()
        If QTCategoryID = 0 Then Exit Sub

        Dim uc As New UCMainQuotationDescriptionItem
        uc.SetDDL(QTCategoryID)
        AddHandler uc.DeleteDescription, AddressOf UCMainQuotationDescription_DeleteDescription
        flpDescription.Controls.Add(uc)

        If uc.Height * flpDescription.Controls.Count > flpDescription.Height Then
            Dim plusHeight As Integer = uc.Height

            flpDescription.Height += plusHeight
            Me.Height += plusHeight

            RaiseEvent HeightChanged(plusHeight)
        End If
    End Sub

    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If flpDescription.Controls.Count > 0 Then
            For Each des As UCMainQuotationDescriptionItem In flpDescription.Controls
                If des.cbbDescription.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Description", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbDescription.Focus()
                    Return False
                End If

                If des.cbbCurrency.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Currency", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbCurrency.Focus()
                    Return False
                End If

                If des.txtPrice.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Price", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtPrice.Focus()
                    Return False
                End If

                If des.cbbBasis.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Basis", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbBasis.Focus()
                    Return False
                End If
            Next
        End If
        Return ret
    End Function

    Public Function SaveMainQuotationDescription(CusID As Integer, QTDate As Date, QuotationMainDetailID As Long, QtNo As String, QtStatus As String, trans As SqlClient.SqlTransaction) As String
        Dim ret As String = "false"
        Try
            If flpDescription.Controls.Count > 0 Then
                For Each gDes As UCMainQuotationDescriptionItem In flpDescription.Controls
                    Dim Sql As String = "insert into quotation_main_desc(created_by,created_date,quotation_main_detail_id,"
                    Sql += " qt_category_description_id, price,ms_currency_id,ms_basis_id,item_note)"
                    Sql += " values (@_CREATED_BY, getdate(), @_MAIN_DETAIL_ID, "
                    Sql += " @_CATEGORY_DESC_ID, @_PRICE, @_CURRENCY_ID, @_BASIS_ID,@_ITEM_NOTE)"

                    Dim p(7) As SqlClient.SqlParameter
                    p(0) = SetText("@_CREATED_BY", myUser.fulllname)
                    p(1) = SetBigInt("@_MAIN_DETAIL_ID", QuotationMainDetailID)
                    p(2) = SetBigInt("@_CATEGORY_DESC_ID", gDes.cbbDescription.SelectedValue)
                    p(3) = SetDouble("@_PRICE", gDes.txtPrice.Text)
                    p(4) = SetBigInt("@_CURRENCY_ID", gDes.cbbCurrency.SelectedValue)
                    p(5) = SetBigInt("@_BASIS_ID", gDes.cbbBasis.SelectedValue)
                    p(6) = SetText("@_ITEM_NOTE", gDes.lblNote.Text)

                    ret = Execute_Command(Sql, trans, p)
                    If ret = "false" Then
                        Exit For
                    Else
                        ret = SaveDataToServiceCost(trans, CusID, gDes.cbbDescription.Text, gDes.txtPrice.Text, gDes.cbbBasis.Text, gDes.lblNote.Text, "Main", QTDate, QtNo, QtStatus).ToString.ToLower
                        If ret = "false" Then
                            Exit For
                        End If
                    End If
                Next
            Else
                ret = "true"
            End If
        Catch ex As Exception
            ret = "false|" & ex.Message
        End Try
        Return ret
    End Function


    

    Public Sub FillInDescriptionData(MainQuotationDetailID As Long)
        Dim sql As String = " select * from quotation_main_desc"
        sql += " where quotation_main_detail_id=@_MAIN_QUOTATION_DETAIL_ID"
        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_MAIN_QUOTATION_DETAIL_ID", MainQuotationDetailID)

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim uc As New UCMainQuotationDescriptionItem
                uc.SetDDL(QTCategoryID)
                uc.cbbDescription.SelectedValue = dr("qt_category_description_id")
                uc.cbbCurrency.SelectedValue = dr("ms_currency_id")
                uc.txtPrice.Text = dr("price")
                uc.cbbBasis.SelectedValue = dr("ms_basis_id")
                If Convert.IsDBNull(dr("item_note")) = False Then
                    uc.lblNote.Text = dr("item_note")
                    If uc.lblNote.Text.Trim <> "" Then
                        uc.btnNote.BackgroundImage = My.Resources.Edit
                    End If
                End If


                AddHandler uc.DeleteDescription, AddressOf UCMainQuotationDescription_DeleteDescription
                flpDescription.Controls.Add(uc)

                If uc.Height * flpDescription.Controls.Count > flpDescription.Height Then
                    Dim plusHeight As Integer = uc.Height

                    flpDescription.Height += plusHeight
                    Me.Height += plusHeight

                    RaiseEvent HeightChanged(plusHeight)
                End If
            Next
        End If
        dt.Dispose()

    End Sub

    Private Sub btnAddDescription_Click(sender As System.Object, e As System.EventArgs) Handles btnAddDescription.Click
        AddDescriptionItem()
    End Sub

    Public Sub UCMainQuotationDescription_DeleteDescription(ctl As UCMainQuotationDescriptionItem)
        Dim plusHeight As Integer = ctl.Height

        flpDescription.Controls.Remove(ctl)

        flpDescription.Height -= plusHeight
        Me.Height -= plusHeight

        RaiseEvent HeightChanged(plusHeight * -1)
    End Sub
End Class

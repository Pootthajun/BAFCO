﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCMainQuotationRemarkItem
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbbRemarks = New System.Windows.Forms.ComboBox()
        Me.btnDelete = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'cbbRemarks
        '
        Me.cbbRemarks.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbRemarks.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbRemarks.Location = New System.Drawing.Point(0, 0)
        Me.cbbRemarks.Name = "cbbRemarks"
        Me.cbbRemarks.Size = New System.Drawing.Size(630, 21)
        Me.cbbRemarks.TabIndex = 0
        '
        'btnDelete
        '
        Me.btnDelete.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.Cancel
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Location = New System.Drawing.Point(637, 0)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(20, 20)
        Me.btnDelete.TabIndex = 5
        '
        'UCMainQuotationRemarkItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.cbbRemarks)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "UCMainQuotationRemarkItem"
        Me.Size = New System.Drawing.Size(660, 21)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbbRemarks As System.Windows.Forms.ComboBox
    Friend WithEvents btnDelete As System.Windows.Forms.Panel

End Class

﻿Imports System.Data.SqlClient
Imports System.Globalization

Public Class frmSearchDialogSupplier_Ser

    Public VAR_MODE As String = ""
    Public VAR_S_SER_TYPE_ID As Int32 = 0
    Public VAR_S_SER_ID As Int32 = 0
    Public VAR_SUP_ID As Int32 = 0
    Public VAR_SERVICE_ID As Int32 = 0
    Private WithEvents kbHook As New KeyboardHook

    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If
    End Sub
    Private Sub frmSearchDialogSupplier_Ser_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Grid.AutoGenerateColumns = False
        btnPrint.Visible = myUser.view_report

        BindCBB_ServiceType(cbbSerType)
        BindCBB_Supplier(cbbSup)

        If VAR_S_SER_TYPE_ID > 0 Then
            cbbSerType.SelectedValue = VAR_S_SER_TYPE_ID
            BindCBB_Service(cbbSer, cbbSerType.SelectedValue)
        End If
        If VAR_S_SER_ID > 0 Then
            cbbSer.SelectedValue = VAR_S_SER_ID
        End If

        If VAR_SUP_ID > 0 Then
            cbbSup.SelectedValue = VAR_SUP_ID
        End If
        If VAR_SERVICE_ID > 0 Then
            cbbSerType.Enabled = False
            cbbSer.Enabled = False
            cbbSup.Enabled = False
        Else
            btnAdd.Enabled = False
            btnSave.Enabled = False
        End If
        ShowData()
    End Sub

    Sub ShowData()
        Dim SQL As String = ""
        If VAR_MODE = "EDIT" Then
            SQL = "SELECT ID,SERVICE_ID,PRICE,ISNULL(CONVERT(VARCHAR(10),QDATE,103),'N/A') AS QDATE,"
            SQL += " CASE WHEN REVISE = 0 THEN '' ELSE REVISE END AS REVISE,REMARK,UPDATE_BY,UPDATE_DATE,Unit "
            SQL += " FROM SERVICE_SUP_REVISE WHERE SERVICE_ID = " & VAR_SERVICE_ID
            SQL += " ORDER BY CONVERT(Int,REVISE)"
        Else
            SQL = "SELECT ID FROM SERVICE_SUP WHERE "
            Dim Filter As String = ""
            Filter &= "SER_ID = " & VAR_S_SER_ID & " AND "
            Filter &= "SUP_ID = " & cbbSup.SelectedValue
            SQL = SQL & Filter
            Dim DT_TEMP As New DataTable
            Dim DA_TEMP As New SqlDataAdapter(SQL, ConnStr)
            DA_TEMP.Fill(DT_TEMP)
            Dim ID As Int32 = 0
            If DT_TEMP.Rows.Count > 0 Then
                ID = DT_TEMP.Rows(0).Item("ID")
                SQL = "SELECT ID,SERVICE_ID,PRICE,ISNULL(QDATE,'N/A') QDATE,"
                SQL += " CASE WHEN REVISE = 0 THEN '' ELSE REVISE END AS REVISE,REMARK,UPDATE_BY,UPDATE_DATE,Unit "
                SQL += " FROM SERVICE_SUP_REVISE WHERE SERVICE_ID = " & ID
                SQL += " ORDER BY CONVERT(Int,REVISE)"
            Else
                SQL = "SELECT ID,SERVICE_ID,PRICE,ISNULL(QDATE,'N/A') QDATE,"
                SQL += " CASE WHEN REVISE = 0 THEN '' ELSE REVISE END AS REVISE,REMARK,UPDATE_BY,UPDATE_DATE,Unit "
                SQL += " FROM SERVICE_SUP_REVISE WHERE 1=0"
                
            End If
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT = New DataTable
        DA.Fill(DT)
        Grid.DataSource = DT

        If cbbSerType.SelectedValue > 0 And cbbSer.SelectedValue > 0 And cbbSup.SelectedValue > 0 Then
            btnAdd.Enabled = True
            btnSave.Enabled = True
        Else
            btnAdd.Enabled = False
            btnSave.Enabled = False
        End If
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = Grid.DataSource
        Dim DR As DataRow = DT.NewRow
        DT.Rows.Add(DR)
    End Sub

    Private Sub Grid_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid.DataError
        MsgBox(e.Exception.Message)
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = Grid.DataSource
        DT.AcceptChanges()
        'Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        If cbbSerType.SelectedIndex = 0 Or cbbSer.SelectedIndex = 0 Or cbbSup.SelectedIndex = 0 Then
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If Grid.Rows.Count = 0 Then
            MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(DT.Rows(i).Item("PRICE")) OrElse DT.Rows(i).Item("PRICE").ToString = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
                'ElseIf IsNumeric(DT.Rows(i).Item("PRICE")) = False Then
                '    MessageBox.Show("กรุณากรอกข้อมูล Sort เป็นตัวเลขจำนวนเต็ม", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                '    Exit Sub
            End If
            'If IsDBNull(DT.Rows(i).Item("QDATE")) OrElse DT.Rows(i).Item("QDATE").ToString = "" Then
            '    MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            '    Exit Sub
            'End If
            If DT.Rows(i).Item("QDATE").ToString <> "N/A" And ValidateDate(DT.Rows(i).Item("QDATE").ToString) = False Then
                MessageBox.Show("รูปแบบวันทีไม่ถูกต้อง", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        Dim Sql As String = ""
        Sql = "SELECT ID,SER_ID,SUP_ID,PRICE,REVISE,QDATE,UPDATE_BY,UPDATE_DATE FROM SERVICE_SUP WHERE "
        Dim Filter As String = ""
        Filter &= "SER_ID = " & cbbSer.SelectedValue & " AND "
        Filter &= "SUP_ID = " & cbbSup.SelectedValue
        Sql = Sql & Filter
        Dim DT_TEMP As New DataTable
        Dim DA_TEMP As New SqlDataAdapter(Sql, ConnStr)
        DA_TEMP.Fill(DT_TEMP)
        Dim ID As Int32 = 0
        If DT_TEMP.Rows.Count > 0 Then
            ID = DT_TEMP.Rows(0).Item("ID")
        End If
        Dim SERVICE_ID As Int32 = 0
        If ID = 0 Then
            'Add
            Dim DR As DataRow = DT_TEMP.NewRow
            SERVICE_ID = GetNewID("SERVICE_SUP", "ID")
            DR("ID") = SERVICE_ID
            DR("SER_ID") = cbbSer.SelectedValue
            DR("SUP_ID") = cbbSup.SelectedValue
            DR("UPDATE_BY") = myUser.user_id
            DR("UPDATE_DATE") = Now
            DT_TEMP.Rows.Add(DR)
            Dim cmd_ As New SqlCommandBuilder(DA_TEMP)
            DA_TEMP.Update(DT_TEMP)
        Else
            'Edit
            SERVICE_ID = ID
        End If

        'Save History
        Dim conn As New SqlConnection(ConnStr)
        Dim sqlcmd As New SqlCommand
        conn.Open()

        Sql = "DELETE SERVICE_SUP_REVISE WHERE SERVICE_ID = " & SERVICE_ID
        sqlcmd = New SqlCommand
        sqlcmd.CommandType = CommandType.Text
        sqlcmd.CommandText = Sql
        sqlcmd.Connection = conn
        sqlcmd.ExecuteNonQuery()

        For i As Integer = 0 To DT.Rows.Count - 1
            ID = GetNewID("SERVICE_SUP_REVISE", "ID")
            Sql = "INSERT INTO SERVICE_SUP_REVISE(ID,SERVICE_ID,PRICE,REVISE,QDATE,REMARK,UNIT,UPDATE_BY,UPDATE_DATE) VALUES(" & vbCrLf
            Sql &= ID & "," & vbCrLf
            Sql &= SERVICE_ID & "," & vbCrLf
            Sql &= "'" & DT.Rows(i).Item("PRICE") & "'," & vbCrLf
            Sql &= i & "," & vbCrLf
            Sql &= IIf(DT.Rows(i).Item("QDATE").ToString() = "N/A", "NULL", "'" & FixDate(DT.Rows(i).Item("QDATE")) & "'") & vbCrLf
            Sql &= ",'" & DT.Rows(i).Item("REMARK").ToString.Replace("'", "''") & "'," & vbCrLf
            Sql &= "'" & DT.Rows(i).Item("UNIT") & "'," & vbCrLf
            Sql &= myUser.user_id & "," & vbCrLf
            Sql &= "GETDATE()" & vbCrLf
            Sql &= " )"
            sqlcmd = New SqlCommand
            sqlcmd.CommandType = CommandType.Text
            sqlcmd.CommandText = Sql
            sqlcmd.Connection = conn
            sqlcmd.ExecuteNonQuery()
        Next

        'Update Price & Revice ล่าสุด
        Sql = "UPDATE SERVICE_SUP SET REVISE = " & DT.Rows.Count - 1 & vbCrLf
        'Sql &= ",QDATE = '" & FixDate(DT.Rows(DT.Rows.Count - 1).Item("QDATE").ToString) & "'," & vbCrLf
        Sql &= ",QDATE = " & IIf(DT.Rows(DT.Rows.Count - 1).Item("QDATE").ToString() = "N/A", "NULL", "'" & FixDate(DT.Rows(DT.Rows.Count - 1).Item("QDATE")) & "'") & vbCrLf
        Sql &= ",PRICE = '" & DT.Rows(DT.Rows.Count - 1).Item("PRICE").ToString & "'" & vbCrLf
        Sql &= ",UNIT = '" & DT.Rows(DT.Rows.Count - 1).Item("UNIT").ToString & "'" & vbCrLf
        Sql &= "WHERE ID = " & SERVICE_ID
        sqlcmd = New SqlCommand
        sqlcmd.CommandType = CommandType.Text
        sqlcmd.CommandText = Sql
        sqlcmd.Connection = conn
        sqlcmd.ExecuteNonQuery()

        conn.Close()

        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub


    Private Sub cbbSerType_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles cbbSerType.SelectionChangeCommitted
        VAR_S_SER_TYPE_ID = cbbSerType.SelectedValue
        BindCBB_Service(cbbSer, cbbSerType.SelectedValue)
        ShowData()
    End Sub

    Private Sub cbbSer_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles cbbSer.SelectionChangeCommitted
        VAR_S_SER_ID = cbbSer.SelectedValue
        ShowData()
    End Sub

    Private Sub cbbSup_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles cbbSup.SelectionChangeCommitted
        VAR_SUP_ID = cbbSup.SelectedValue
        ShowData()
    End Sub

    Function GetDataForPrint(service_id As String) As DataTable
        Dim DT = New DataTable
        Try
            Dim sql As String = ""
            sql &= " SELECT CONVERT(VARCHAR(10),GETDATE(),103) AS PRINT_DATE,"
            sql &= " SERVICE_SUP.ID,SUP_NAME,S.SER_NAME ,T.SER_TYPE_NAME,"
            sql &= " SERVICE_SUP_REVISE.REVISE,SERVICE_SUP_REVISE.PRICE,ISNULL(CONVERT(VARCHAR(10),SERVICE_SUP_REVISE.QDATE,103),'N/A') AS QDATE,"
            sql &= " SERVICE_SUP_REVISE.REMARK, SERVICE_SUP_REVISE.UNIT"
            sql &= " FROM Service_Sup"
            sql &= " LEFT JOIN SUPPLIER ON SERVICE_SUP.SUP_ID = SUPPLIER.SUP_ID"
            sql &= " LEFT JOIN SERVICE S ON SERVICE_SUP.SER_ID = S.SER_ID"
            sql &= " LEFT JOIN SERVICE_TYPE T ON S.SER_TYPE_ID = T.SER_TYPE_ID"
            sql &= " LEFT JOIN SERVICE_SUP_REVISE ON SERVICE_SUP.ID = SERVICE_SUP_REVISE.SERVICE_ID"
            sql &= " where SERVICE_SUP.ID ='" & service_id & "'"

            Dim DA As New SqlDataAdapter(sql, ConnStr)
            DA.Fill(DT)
        Catch ex As Exception

        End Try

        Return DT
    End Function

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Dim dt As New DataTable
        dt = GetDataForPrint(VAR_SERVICE_ID)
        Dim f As New Service_Sup
        f.DTSUP = dt
        f.ReportName = "Service_Sup.rpt"
        f.ShowDialog()
    End Sub
End Class
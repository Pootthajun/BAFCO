﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports OfficeOpenXml

Public Class frmQuotationCaseByCaseSearch

    Private WithEvents kbHook As New KeyboardHook
    Dim dt As New DataTable

    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If
    End Sub

    Private Sub frmQuotationMainSearch_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        BindCBB_Customer(cbbCustomer)
        BindCBB_Mode(cbbMode, False)
        BindCBB_Scope(cbbScope)
        BindCBB_Country(cbbOrginCountry)
        BindCBB_Country(cbbDestCountry)
        ShowData()
    End Sub

    Private Sub frmQuotationCaseByCaseSearch_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        dpDateFrom.Value = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        dpDateTo.Value = DateTime.Today
    End Sub

    Sub ShowData()
        Grid.AutoGenerateColumns = False
        Dim SQL As String = "select q.id, q.qt_no, q.qt_date, convert(varchar(8), q.qt_date, 112) qt_date_str, q.cus_id, c.cus_name, q.user_id_sale, u.us_code," & Environment.NewLine
        SQL += " q.ms_mode_id, md.mode_name, q.ms_scope_id, sc.scope_name, q.ms_term_id, tm.term_name, q.bafco_job_no, q.reference_no, " & Environment.NewLine
        SQL += " cno.country_name + ' ' + q.city_name_origin origin, cnd.country_name + ' ' + q.city_name_dest destination," & Environment.NewLine
        SQL += " case q.qt_status when '0' then 'Draft' when '1' then 'Approve' when '2' then 'Deny' end qt_status_name, q.qt_status, " & Environment.NewLine
        SQL += " cno.id country_id_origin, q.city_name_origin, q.port_name_origin, cnd.id country_id_dest, q.city_name_dest, q.port_name_dest " & Environment.NewLine
        SQL += " from QUOTATION_BYCASE q" & Environment.NewLine
        SQL += " inner join CUSTOMER c on c.cus_id=q.cus_id" & Environment.NewLine
        SQL += " inner join [USER] u on u.us_id=q.user_id_sale" & Environment.NewLine
        SQL += " inner join MS_MODE md on md.id=q.ms_mode_id" & Environment.NewLine
        SQL += " inner join ms_scope sc on sc.id=q.ms_scope_id" & Environment.NewLine
        SQL += " left join ms_term tm on tm.id=q.ms_term_id" & Environment.NewLine
        SQL += " left join MS_COUNTRY cno on cno.id=q.ms_country_id_origin" & Environment.NewLine
        SQL += " left join MS_COUNTRY cnd on cnd.id=q.ms_country_id_dest" & Environment.NewLine
        SQL += " where c.cus_id in (SELECT CUS_ID FROM USER_CUSTOMER WHERE US_ID = " & myUser.user_id & ") "
        SQL += " order by q.qt_date desc "

        dt = Execute_DataTable(SQL)
        Grid.DataSource = dt

        Application.DoEvents()
    End Sub

    Private Sub FilterData()
        If dt.Rows.Count = 0 Then Exit Sub

        Dim wh As String = "1=1 "
        If cbbCustomer.SelectedIndex > 0 Then
            wh += " and cus_id=" & cbbCustomer.SelectedValue
        End If
        If txtQtNo.Text.Trim <> "" Then
            wh += " and qt_no like '%" & txtQtNo.Text.Trim & "%'"
        End If
        If cbbMode.SelectedIndex > 0 Then
            wh += " and ms_mode_id=" & cbbMode.SelectedValue
        End If
        If cbbScope.SelectedIndex > 0 Then
            wh += " and ms_scope_id=" & cbbScope.SelectedValue
        End If
        If txtJobNo.Text.Trim <> "" Then
            wh += " and bafco_job_no like '%" & txtJobNo.Text.Trim & "%'"
        End If
        If txtRefDocNo.Text.Trim <> "" Then
            wh += " and reference_no like '%" & txtRefDocNo.Text.Trim & "%'"
        End If
        If txtSalesName.Text.Trim <> "" Then
            wh += " and us_code like '%" & txtSalesName.Text.Trim & "%'"
        End If
        If cbbOrginCountry.SelectedValue > 0 Then
            wh += " and country_id_origin=" & cbbOrginCountry.SelectedValue
        End If
        If txtOriginCity.Text.Trim <> "" Then
            wh += " and city_name_origin like '%" & txtOriginCity.Text.Trim & "%'"
        End If
        If txtOriginPort.Text.Trim <> "" Then
            wh += " and port_name_origin like '%" & txtOriginPort.Text.Trim & "%'"
        End If
        If cbbDestCountry.SelectedValue > 0 Then
            wh += " and country_id_dest=" & cbbDestCountry.SelectedValue
        End If
        If txtDestCity.Text.Trim <> "" Then
            wh += " and city_name_dest like '%" & txtDestCity.Text.Trim & "%'"
        End If
        If txtDestPort.Text.Trim <> "" Then
            wh += " and port_name_dest like '%" & txtDestPort.Text.Trim & "%'"
        End If


        Dim txtStatus As String = ""
        If chkStatusDraft.Checked = True Then
            txtStatus += ",'0'"
        End If
        If chkStatusApprove.Checked = True Then
            txtStatus += ",'1'"
        End If
        If chkStatusDeny.Checked = True Then
            txtStatus += ",'2'"
        End If
        If txtStatus <> "" Then
            txtStatus = txtStatus.Substring(1)
        End If
        If txtStatus.Trim <> "" Then
            wh += " and qt_status in (" & txtStatus & ")"
        Else
            wh += " and qt_status in ('')"
        End If

        Dim strDateFrom As String = dpDateFrom.Value.ToString("yyyyMMdd", New Globalization.CultureInfo("en-US"))
        Dim strDateTo As String = dpDateTo.Value.ToString("yyyyMMdd", New Globalization.CultureInfo("en-US"))
        wh += " and qt_date_str >= '" & strDateFrom & "'"
        wh += " and qt_date_str <= '" & strDateTo & "'"

        dt.DefaultView.RowFilter = wh
        lblRec.Text = lblRec.Text.Replace("xxx", dt.DefaultView.Count)
    End Sub

    Private Sub Grid_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid.CellDoubleClick
        Dim _id As Long = Grid.Rows(e.RowIndex).Cells("ID").Value
        Dim frm As New frmQuotationCaseByCase
        frm.MdiParent = frmMain
        frm.Show()
        frm.FillInData(_id, False)

        Me.Hide()
    End Sub


    Private Sub Grid_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid.DataError
        MsgBox(e.Exception.Message)
    End Sub


    Private Sub cbbCus_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbCustomer.SelectedIndexChanged
        FilterData()
    End Sub

    Private Sub dpDateFrom_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dpDateFrom.ValueChanged, dpDateTo.ValueChanged
        FilterData()
    End Sub

    Private Sub txtQtNo_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtQtNo.TextChanged, txtSalesName.TextChanged, txtRefDocNo.TextChanged, txtJobNo.TextChanged, txtRefDocNo.TextChanged, txtOriginCity.TextChanged, txtOriginPort.TextChanged, txtDestCity.TextChanged, txtDestPort.TextChanged
        FilterData()
    End Sub

    Private Sub chkStatusDraft_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkStatusDraft.CheckedChanged, chkStatusApprove.CheckedChanged, chkStatusDeny.CheckedChanged
        FilterData()
    End Sub

    Private Sub cbbMode_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbMode.SelectedIndexChanged
        FilterData()
    End Sub

    Private Sub cbbScope_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbScope.SelectedIndexChanged
        FilterData()
    End Sub
    Private Sub cbbOrginCountry_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbOrginCountry.SelectedIndexChanged
        FilterData()
    End Sub

    Private Sub cbbDestCountry_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbDestCountry.SelectedIndexChanged
        FilterData()
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmQuotationCaseByCase
        frm.MdiParent = frmMain
        frm.Show()

        Me.Hide()
    End Sub

    Private Sub btnCopy_Click(sender As System.Object, e As System.EventArgs) Handles btnCopy.Click
        Dim _id As Long = Grid.SelectedRows(0).Cells("ID").Value
        Dim frm As New frmQuotationCaseByCase
        frm.MdiParent = frmMain
        frm.Show()
        frm.FillInData(_id, True)

        Me.Hide()
    End Sub

    'Private Sub frmQuotationCaseByCaseSearch_VisibleChanged(sender As Object, e As System.EventArgs) Handles Me.VisibleChanged
    '    Grid.AutoGenerateColumns = False
    '    ShowData()
    '    FilterData()
    'End Sub



    Private Sub btnExport_Click(sender As System.Object, e As System.EventArgs) Handles btnExport.Click
        If dt.DefaultView.Count > 0 Then
            Using ep As New ExcelPackage
                Dim ws As ExcelWorksheet = ep.Workbook.Worksheets.Add("CaseByCaseQuotationData")
                ws.Cells("A1").Value = "QT No"
                ws.Cells("B1").Value = "QT Date"
                ws.Cells("C1").Value = "Customer"
                ws.Cells("D1").Value = "Sales Name"
                ws.Cells("E1").Value = "Status"
                ws.Cells("F1").Value = "Mode"
                ws.Cells("G1").Value = "Scope"
                ws.Cells("H1").Value = "Term"
                ws.Cells("I1").Value = "Origin"
                ws.Cells("J1").Value = "Destination"

                ws.Cells("A1").Style.Font.Bold = True
                ws.Cells("B1").Style.Font.Bold = True
                ws.Cells("C1").Style.Font.Bold = True
                ws.Cells("D1").Style.Font.Bold = True
                ws.Cells("E1").Style.Font.Bold = True
                ws.Cells("F1").Style.Font.Bold = True
                ws.Cells("G1").Style.Font.Bold = True
                ws.Cells("H1").Style.Font.Bold = True
                ws.Cells("I1").Style.Font.Bold = True
                ws.Cells("J1").Style.Font.Bold = True

                Dim i As Integer = 2
                For Each dr As DataRowView In dt.DefaultView
                    ws.Cells(i, 1).Value = dr("qt_no")
                    ws.Cells(i, 2).Value = Convert.ToDateTime(dr("qt_date")).ToString("dd/MM/yyyy", New CultureInfo("en-US"))
                    ws.Cells(i, 3).Value = dr("cus_name")
                    ws.Cells(i, 4).Value = dr("us_code")
                    ws.Cells(i, 5).Value = dr("qt_status_name")
                    If Convert.IsDBNull(dr("mode_name")) = False Then ws.Cells(i, 6).Value = dr("mode_name")
                    If Convert.IsDBNull(dr("scope_name")) = False Then ws.Cells(i, 7).Value = dr("scope_name")
                    If Convert.IsDBNull(dr("term_name")) = False Then ws.Cells(i, 8).Value = dr("term_name")
                    If Convert.IsDBNull(dr("origin")) = False Then ws.Cells(i, 9).Value = dr("origin")
                    If Convert.IsDBNull(dr("destination")) = False Then ws.Cells(i, 10).Value = dr("destination")
                    i += 1
                Next
                ws.Cells(1, 1, i, 10).AutoFitColumns()

                Using fs As New SaveFileDialog
                    fs.Filter = "Excel File(*.xlsx)|*.xlsx"
                    If fs.ShowDialog = Windows.Forms.DialogResult.OK Then
                        ep.SaveAs(New IO.FileInfo(fs.FileName))
                    End If
                End Using
            End Using
        End If
    End Sub
End Class
﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class frmMain

    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Application.Exit()
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        DisableMenu()
        If (Not Directory.Exists(TempPath)) Then
            Directory.CreateDirectory(TempPath)
        End If

        getMyVersion()

        If CkeckConnection(ConnStr) = False Then
            Dim f As New frmSettingDatabase
            'f.BeginProgram = True
            If f.ShowDialog = Windows.Forms.DialogResult.Cancel Then
                Application.Exit()
                Exit Sub
            End If
        End If

        Dim frmLogin As New Login
        frmLogin.ExitApp = True
        If frmLogin.ShowDialog = Windows.Forms.DialogResult.OK Then
            CheckMenu()
        Else
            Application.Exit()
        End If
    End Sub

#Region "Menu Event"
    Sub CloseAllChildForm()
        For i As Integer = My.Application.OpenForms.Count - 1 To 0 Step -1
            If My.Application.OpenForms.Item(i) IsNot Me Then
                My.Application.OpenForms.Item(i).Close()
            End If
        Next i
    End Sub
    Public Sub ShowChildForm(frm As Form)
        For Each ctl As Form In Me.MdiChildren
            If ctl.Name = frm.Name Then
                ctl.Show()
                Exit Sub
            End If
        Next
    End Sub

    Sub DisableMenu()
        'btnTransportCost.Enabled = False
        'btnServiceCharge.Enabled = False
        'btnImportCost.Enabled = False
        'btnImportService.Enabled = False
        'btnFileImportCost.Enabled = False
        'btnFileImportService.Enabled = False
        'btnUser.Enabled = False
        'btnLocation.Enabled = False
        'btnSupplierType.Enabled = False
        'btnSupplier.Enabled = False
        'btnCustomerType.Enabled = False
        'btnCustomer.Enabled = False
        'btnServiceType.Enabled = False
        'btnService.Enabled = False
        'btnVehicle.Enabled = False


        mnuTransportCost.Enabled = False
        mnuServiceCharge.Enabled = False
        mnuImportCost.Enabled = False
        mnuImportService.Enabled = False
        mnuFileImportCost.Enabled = False
        mnuFileImportService.Enabled = False
        mnuUser.Enabled = False
        mnuLocation.Enabled = False
        mnuSupplierType.Enabled = False
        mnuSupplier.Enabled = False
        mnuCustomerType.Enabled = False
        mnuCustomer.Enabled = False
        mnuServiceType.Enabled = False
        mnuService.Enabled = False
        mnuVehicle.Enabled = False
        mnuCustomerScope.Enabled = False
    End Sub

    Sub CheckMenu()

        Dim Sql As String = ""
        Dim Menu As String = ""
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter
        Sql = "SELECT MENU_ID FROM USER_MENU WHERE US_ID = " & myUser.user_id
        da = New SqlDataAdapter(Sql, ConnStr)
        da.Fill(dt)

        For i As Int32 = 0 To dt.Rows.Count - 1
            Menu = Menu & "|" & dt.Rows(i).Item("MENU_ID").ToString & "|"
        Next

        If InStr(Menu, "|1|") > 0 Then
            'btnTransportCost.Enabled = True
            mnuTransportCost.Enabled = True
        End If
        If InStr(Menu, "|2|") > 0 Then
            'btnServiceCharge.Enabled = True
            mnuServiceCharge.Enabled = True
        End If
        If InStr(Menu, "|3|") > 0 Then
            'btnImportCost.Enabled = True
            mnuImportCost.Enabled = True
        End If
        If InStr(Menu, "|4|") > 0 Then
            'btnImportService.Enabled = True
            mnuImportService.Enabled = True
        End If
        If InStr(Menu, "|5|") > 0 Then
            'btnFileImportCost.Enabled = True
            mnuFileImportCost.Enabled = True
        End If
        If InStr(Menu, "|6|") > 0 Then
            'btnFileImportService.Enabled = True
            mnuFileImportService.Enabled = True
        End If
        If InStr(Menu, "|7|") > 0 Then
            'btnUser.Enabled = True
            mnuUser.Enabled = True
        End If
        If InStr(Menu, "|9|") > 0 Then
            'btnLocation.Enabled = True
            mnuLocation.Enabled = True
        End If
        If InStr(Menu, "|10|") > 0 Then
            'btnSupplierType.Enabled = True
            mnuSupplierType.Enabled = True
        End If
        If InStr(Menu, "|11|") > 0 Then
            'btnSupplier.Enabled = True
            mnuSupplier.Enabled = True
        End If
        If InStr(Menu, "|12|") > 0 Then
            'btnCustomerType.Enabled = True
            mnuCustomerType.Enabled = True
        End If
        If InStr(Menu, "|13|") > 0 Then
            'btnCustomer.Enabled = True
            mnuCustomer.Enabled = True
        End If
        If InStr(Menu, "|14|") > 0 Then
            'btnServiceType.Enabled = True
            mnuServiceType.Enabled = True
        End If
        If InStr(Menu, "|15|") > 0 Then
            'btnService.Enabled = True
            mnuService.Enabled = True
        End If
        If InStr(Menu, "|16|") > 0 Then
            'btnVehicle.Enabled = True
            mnuVehicle.Enabled = True
        End If
        If InStr(Menu, "|17|") > 0 Then
            myUser.view_report = True
        Else
            myUser.view_report = False
        End If
        If InStr(Menu, "|18|") > 0 Then
            myUser.export_report = True
        Else
            myUser.export_report = False
        End If
        If InStr(Menu, "|19|") > 0 Then
            mnuCustomerScope.Enabled = True
        Else
            mnuCustomerScope.Enabled = False
        End If
        If InStr(Menu, "|20|") > 0 Then
            myUser.capture_screen = True
        Else
            myUser.capture_screen = False
        End If

    End Sub

    Private Sub btnDbConfig_Click(sender As System.Object, e As System.EventArgs) Handles mnuDbConfig.Click
        CloseAllChildForm()
        Dim f As New frmSettingDatabase
        f.ShowDialog()
    End Sub

    Private Sub btnUser_Click(sender As System.Object, e As System.EventArgs) Handles mnuUser.Click
        CloseAllChildForm()
        Dim f As New frmUser
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub btnImportCost_Click(sender As System.Object, e As System.EventArgs) Handles mnuImportCost.Click
        CloseAllChildForm()
        Dim f As New frmImportCost
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub btnImportService_Click(sender As System.Object, e As System.EventArgs) Handles mnuImportService.Click
        CloseAllChildForm()
        Dim f As New frmImportService
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub btnSupplierType_Click(sender As System.Object, e As System.EventArgs) Handles mnuSupplierType.Click
        CloseAllChildForm()
        Dim f As New frmSupplierType
        f.ShowDialog()
    End Sub

    Private Sub btnSupplier_Click(sender As System.Object, e As System.EventArgs) Handles mnuSupplier.Click
        CloseAllChildForm()
        Dim f As New frmSupplier
        f.ShowDialog()
    End Sub

    Private Sub btnCustomerType_Click(sender As System.Object, e As System.EventArgs) Handles mnuCustomerType.Click
        CloseAllChildForm()
        Dim f As New frmCustomerType
        f.ShowDialog()
    End Sub

    Private Sub mnuCustomerScope_Click(sender As System.Object, e As System.EventArgs) Handles mnuCustomerScope.Click
        CloseAllChildForm()
        Dim f As New frmSettingCustomerScope
        f.ShowDialog()
    End Sub

    Private Sub btnCustomer_Click(sender As System.Object, e As System.EventArgs) Handles mnuCustomer.Click
        CloseAllChildForm()
        Dim f As New frmCustomer
        f.ShowDialog()
    End Sub

    Private Sub btnServiceType_Click(sender As System.Object, e As System.EventArgs) Handles mnuServiceType.Click
        CloseAllChildForm()
        Dim f As New frmServiceType
        f.ShowDialog()
    End Sub

    Private Sub btnService_Click(sender As System.Object, e As System.EventArgs) Handles mnuService.Click
        CloseAllChildForm()
        Dim f As New frmService
        f.ShowDialog()
    End Sub

    Private Sub btnRoute_Click(sender As System.Object, e As System.EventArgs) Handles mnuLocation.Click
        CloseAllChildForm()
        Dim f As New frmLocation
        f.ShowDialog()
    End Sub

    Private Sub btnVehicle_Click(sender As System.Object, e As System.EventArgs) Handles mnuVehicle.Click
        CloseAllChildForm()
        Dim f As New frmSettingVehicle
        f.ShowDialog()
    End Sub

    Private Sub btnInfo_Click(sender As System.Object, e As System.EventArgs) Handles mnuTransportCost.Click
        CloseAllChildForm()
        Dim f As New frmSearchCost
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub btnServiceCharge_Click(sender As System.Object, e As System.EventArgs) Handles mnuServiceCharge.Click
        CloseAllChildForm()
        Dim f As New frmSearchService
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub RibbonButton1_Click(sender As System.Object, e As System.EventArgs)
        CloseAllChildForm()
        Dim f As New Permission
        f.ShowDialog()
    End Sub

    Private Sub btnFileImportCost_Click(sender As System.Object, e As System.EventArgs) Handles mnuFileImportCost.Click
        sfd.FileName = "Cost Data Import.xlsx"
        sfd.Filter = "Excel File|*.xlsx"
        If sfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim Dest As String = sfd.FileName
            If File.Exists(Dest) Then
                Kill(Dest)
            End If
            File.Copy(Application.StartupPath & "\Cost Data Import.xlsx", Dest)
            If MessageBox.Show("File copied." & vbCrLf & "Do you want to open one ?", "Open File", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Process.Start(Dest)
            End If
        End If

    End Sub

    Private Sub btnFileImportService_Click(sender As System.Object, e As System.EventArgs) Handles mnuFileImportService.Click
        sfd.FileName = "Service Data Import.xlsx"
        sfd.Filter = "Excel File|*.xlsx"
        If sfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim Dest As String = sfd.FileName
            If File.Exists(Dest) Then
                Kill(Dest)
            End If
            File.Copy(Application.StartupPath & "\Service Data Import.xlsx", Dest)
            If MessageBox.Show("File copied." & vbCrLf & "Do you want to open one ?", "Open File", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Process.Start(Dest)
            End If
        End If
    End Sub


    Private Sub btnLogout_Click(sender As System.Object, e As System.EventArgs) Handles mnuLogout.Click
        CloseAllChildForm()
        Dim frmLogin As New Login
        If frmLogin.ShowDialog = Windows.Forms.DialogResult.OK Then
            DisableMenu()
            CheckMenu()
        End If
    End Sub


    Private Sub mnuMainQuotation_Click(sender As System.Object, e As System.EventArgs) Handles mnuMainQuotation.Click
        CloseAllChildForm()
        Dim f As New frmQuotationMainSearch
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub mnuCaseByCaseQuotation_Click(sender As System.Object, e As System.EventArgs) Handles mnuCaseByCaseQuotation.Click
        CloseAllChildForm()
        Dim f As New frmQuotationCaseByCaseSearch
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub mmuSettingQuotationCategory_Click(sender As System.Object, e As System.EventArgs) Handles mmuSettingQuotationCategory.Click
        CloseAllChildForm()
        Dim f As New frmSettingQTCategory
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub mmuSettingQuotationGroup_Click(sender As System.Object, e As System.EventArgs) Handles mmuSettingQuotationGroup.Click
        CloseAllChildForm()
        Dim f As New frmSettingQTGroup
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub mmuSettingQuotationSubGroup_Click(sender As System.Object, e As System.EventArgs) Handles mmuSettingQuotationSubGroup.Click
        CloseAllChildForm()
        Dim f As New frmSettingQTSubGroup
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub mmuSettingQuotationDescription_Click(sender As System.Object, e As System.EventArgs) Handles mmuSettingQuotationDescription.Click
        CloseAllChildForm()
        Dim f As New frmSettingQTDescription
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub mmuSettingQuotationTermConditions_Click(sender As System.Object, e As System.EventArgs) Handles mmuSettingQuotationTermConditions.Click
        CloseAllChildForm()
        Dim f As New frmSettingQTTermCondition
        f.MdiParent = Me
        f.Show()
    End Sub

    Private Sub mnuSettingCountry_Click(sender As System.Object, e As System.EventArgs) Handles mnuSettingCountry.Click
        CloseAllChildForm()
        Dim f As New frmSettingCountry
        f.ShowDialog()
    End Sub
#End Region


#Region "MasterQuatation"
    Private Sub mmuBasis_Click(sender As System.Object, e As System.EventArgs) Handles mmuBasis.Click
        CloseAllChildForm()
        Dim f As New frmSettingBasis
        f.ShowDialog()
    End Sub

    Private Sub mmuCreditTerm_Click(sender As System.Object, e As System.EventArgs) Handles mmuCreditTerm.Click
        CloseAllChildForm()
        Dim f As New frmSettingCreditTerm
        f.ShowDialog()
    End Sub

    Private Sub mmuMode_Click(sender As System.Object, e As System.EventArgs) Handles mmuMode.Click
        CloseAllChildForm()
        Dim f As New frmSettingMode
        f.ShowDialog()
    End Sub

    Private Sub mmuScope_Click(sender As System.Object, e As System.EventArgs)
        CloseAllChildForm()
        Dim f As New frmSettingQuotationScope
        f.ShowDialog()
    End Sub

    Private Sub mmuTerm_Click(sender As System.Object, e As System.EventArgs) Handles mmuTerm.Click
        CloseAllChildForm()
        Dim f As New frmSettingTerm
        f.ShowDialog()
    End Sub

    Private Sub mmuCurrency_Click(sender As System.Object, e As System.EventArgs) Handles mmuCurrency.Click
        CloseAllChildForm()
        Dim f As New frmSettingCurrency
        f.ShowDialog()
    End Sub

    Private Sub mnuQuotationScope_Click(sender As System.Object, e As System.EventArgs) Handles mnuQuotationScope.Click
        CloseAllChildForm()
        Dim f As New frmSettingQuotationScope
        f.ShowDialog()
    End Sub
#End Region


End Class
﻿Imports System.Data.SqlClient

Public Class Permission

    Private Sub Permission_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        GridMenu.AutoGenerateColumns = False
    End Sub


    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        Dim f As New frmFilterUser
        If f.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim SQL As String = ""
            SQL = "SELECT * FROM [USER] WHERE US_ID = " & f.USER_ID
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)

            SQL = ""
            SQL &= "SELECT MENU.MENU_ID,MENU_NAME,CASE WHEN USER_MENU.MENU_ID IS NULL THEN 0 ELSE 1 END AS [CHECK]" & vbCrLf
            SQL &= "FROM MENU LEFT JOIN (SELECT MENU_ID FROM USER_MENU WHERE US_ID = " & f.USER_ID & ") USER_MENU" & vbCrLf
            SQL &= "ON MENU.MENU_ID = USER_MENU.MENU_ID" & vbCrLf
            SQL &= "ORDER BY MENU.SORT" & vbCrLf
            Dim DT_MENU As New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(DT_MENU)

            txtName.Text = DT.Rows(0).Item("US_NAME").ToString
            txtName.Tag = f.USER_ID
            GridMenu.DataSource = DT_MENU
            btnSave.Enabled = True
        Else
            If txtName.Text = "" Then
                btnSave.Enabled = False
            Else
                btnSave.Enabled = True
            End If
        End If
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Dim SQL As String = ""
        Dim conn As New SqlConnection(ConnStr)
        Dim cmd As New SqlCommand
        conn.Open()

        SQL = "DELETE FROM USER_MENU WHERE US_ID = " & txtName.Tag
        cmd = New SqlCommand
        cmd.CommandType = CommandType.Text
        cmd.CommandText = SQL
        cmd.Connection = conn
        cmd.ExecuteNonQuery()

        For i As Int32 = 0 To GridMenu.Rows.Count - 1
            If CBool(GridMenu.Rows(i).Cells("CHECK").Value) = True Then
                SQL = "INSERT INTO USER_MENU(US_ID,MENU_ID) VALUES(" & txtName.Tag & "," & GridMenu.Rows(i).Cells("MENU_ID").Value.ToString & ")"
                cmd = New SqlCommand
                cmd.CommandType = CommandType.Text
                cmd.CommandText = SQL
                cmd.Connection = conn
                cmd.ExecuteNonQuery()
            End If
        Next

        conn.Close()
        MessageBox.Show("Save success.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class
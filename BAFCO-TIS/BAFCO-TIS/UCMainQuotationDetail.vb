﻿Public Class UCMainQuotationDetail

    Public Event CloseCategory(ctl As UCMainQuotationDetail)

    Public WriteOnly Property EnableMainQuotationDetail As Boolean
        Set(value As Boolean)
            cbbQTCategory.Enabled = value
            UcMainQuotationDescription1.EnableDescription = value
            'UcMainQuotationRemark1.EnableRemark = value
            btnClose.Visible = value
            btnAddGroup.Visible = value
            txtHeaderDetail.Enabled = value
            'txtFooterNote.Enabled = value
            'txtSubjectRemark.Enabled = value
            If value = False Then
                Panel1.Height -= btnAddGroup.Height
                pnlDetail.Height -= btnAddGroup.Height
                Me.Height -= btnAddGroup.Height
            End If

            For Each ctl As UCMainQuotationGroup In flpGroup.Controls
                ctl.EnableGroup = value
            Next

            UcMainQuotationTruck1.EnableDescription = value
            Application.DoEvents()
        End Set
    End Property

    Private Sub SetDDLCategory()
        Dim sql As String = " select id, category_name "
        sql += " from QT_CATEGORY "
        sql += " where active_status='Y'"
        sql += " order by category_name"

        Dim CatDt As DataTable = Execute_DataTable(sql)
        If CatDt Is Nothing Then
            CatDt = New DataTable
            CatDt.Columns.Add("id")
            CatDt.Columns.Add("category_name")
        End If

        Dim dr As DataRow = CatDt.NewRow
        dr("id") = 0
        dr("category_name") = ""
        CatDt.Rows.InsertAt(dr, 0)

        cbbQTCategory.DataSource = CatDt
        cbbQTCategory.DisplayMember = "category_name"
        cbbQTCategory.ValueMember = "id"
        cbbQTCategory.SelectedValue = 0

    End Sub

    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If UcMainQuotationDescription1.ValidateData() = False Then
            Return False
        End If
        If UcMainQuotationRemark1.ValidateData() = False Then
            Return False
        End If

        If pnlDetail.Visible = True Then
            For Each uc As UCMainQuotationGroup In flpGroup.Controls
                If uc.ValidateData() = False Then
                    Return False
                End If
            Next
        Else
            If UcMainQuotationTruck1.ValidateData = False Then
                Return False
            End If
        End If

        Return True
    End Function
    Public Sub FillInDetailData(MainQuotationID As Long, MainQuotationDetailID As Long, QTCategoryID As Long, HeaderDetail As String, FooterDetail As String, SubjectRemark As String, gDt As DataTable)
        cbbQTCategory.SelectedValue = QTCategoryID

        If pnlDetail.Visible = True Then
            txtHeaderDetail.Text = HeaderDetail
            txtFooterNote.Text = FooterDetail
            txtSubjectRemark.Text = SubjectRemark
            UcMainQuotationDescription1.FillInDescriptionData(MainQuotationDetailID)
            UcMainQuotationRemark1.FillInRemarkData(MainQuotationDetailID)

            For Each gDr As DataRow In gDt.Rows
                Dim GroupHeaderDetail As String = ""
                Dim GroupFooterDetail As String = ""
                Dim GroupSubjectRemark As String = ""
                If Convert.IsDBNull(gDr("header_detail")) = False Then GroupHeaderDetail = gDr("header_detail")
                If Convert.IsDBNull(gDr("footer_detail")) = False Then GroupFooterDetail = gDr("footer_detail")
                If Convert.IsDBNull(gDr("subject_remark")) = False Then GroupSubjectRemark = gDr("subject_remark")

                Dim uc As New UCMainQuotationGroup
                uc.SetDDLGroup(cbbQTCategory.SelectedValue)
                uc.FillGroupData(MainQuotationID, gDr("id"), gDr("qt_group_id"), GroupHeaderDetail, GroupFooterDetail, GroupSubjectRemark)

                flpGroup.Height += uc.Height
                pnlDetail.Height += uc.Height
                Panel1.Height += uc.Height
                Me.Height += uc.Height
                AddHandler uc.GroupHeightChange, AddressOf GroupHeightChange
                AddHandler uc.CloseGroup, AddressOf UCMainQuotationGroup_CloseGroup

                flpGroup.Controls.Add(uc)
            Next
        Else
            UcMainQuotationTruck1.FillInTruckDescriptionData(MainQuotationDetailID)
        End If

        
    End Sub

    Private Sub UCMainQuotationGroup_CloseGroup(ctl As UCMainQuotationGroup)
        Dim plusHeight As Integer = ctl.Height

        flpGroup.Controls.Remove(ctl)

        flpGroup.Height -= plusHeight
        pnlDetail.Height -= plusHeight
        Panel1.Height -= plusHeight
        Me.Height -= plusHeight

    End Sub

    Private Sub cbbQTCategory_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbQTCategory.SelectedIndexChanged
        

        If cbbQTCategory.SelectedIndex > 0 Then
            If cbbQTCategory.Text.IndexOf(GetSysconfig("CateTransportService")) > -1 Then
                'Transportation Service
                pnlDetail.Visible = False
                UcMainQuotationTruck1.Visible = True


            Else
                pnlDetail.Visible = True
                UcMainQuotationTruck1.Visible = False

                For Each ctl As UCMainQuotationDescriptionItem In UcMainQuotationDescription1.flpDescription.Controls
                    UcMainQuotationDescription1.UCMainQuotationDescription_DeleteDescription(ctl)
                Next

                For Each ctl As UCMainQuotationRemarkItem In UcMainQuotationRemark1.flpRemark.Controls
                    UcMainQuotationRemark1.btnDeleteRemark_Click(ctl)
                Next

                For Each ctl As UCMainQuotationGroup In flpGroup.Controls
                    ctl.btnClose_Click(Nothing, Nothing)
                Next
                UcMainQuotationDescription1.QTCategoryID = cbbQTCategory.SelectedValue
                UcMainQuotationRemark1.QTCategoryID = cbbQTCategory.SelectedValue
            End If
        End If
    End Sub


    Private Sub UcMainQuotationDescription1_HeightChanged(h As Integer) Handles UcMainQuotationDescription1.HeightChanged
        lblLabelNote.Top += h
        lblLabelRemarkSubject.Top += h
        txtFooterNote.Top += h
        txtSubjectRemark.Top += h
        flpGroup.Top += h
        UcMainQuotationRemark1.Top += h
        pnlDetail.Height += h
        Panel1.Height += h
        Me.Height += h

        If UcMainQuotationDescription1.flpDescription.Controls.Count > 0 Then
            Dim uc As UCMainQuotationDescriptionItem = DirectCast(UcMainQuotationDescription1.flpDescription.Controls(0), UCMainQuotationDescriptionItem)
            uc.Focus()
        End If
    End Sub

    Private Sub UcMainQuotationRemark1_HeightChanged(h As Integer) Handles UcMainQuotationRemark1.HeightChanged
        flpGroup.Top += h
        pnlDetail.Height += h
        Panel1.Height += h
        Me.Height += h
    End Sub

    Private Sub btnAddGroup_Click(sender As System.Object, e As System.EventArgs) Handles btnAddGroup.Click
        If cbbQTCategory.SelectedValue > 0 Then
            Dim uc As New UCMainQuotationGroup
            uc.SetDDLGroup(cbbQTCategory.SelectedValue)

            flpGroup.Height += uc.Height
            pnlDetail.Height += uc.Height
            Panel1.Height += uc.Height
            Me.Height += uc.Height
            AddHandler uc.GroupHeightChange, AddressOf GroupHeightChange
            AddHandler uc.CloseGroup, AddressOf UCMainQuotationGroup_CloseGroup

            flpGroup.Controls.Add(uc)
        End If
    End Sub

    Private Sub GroupHeightChange(h As Integer)
        flpGroup.Height += h
        pnlDetail.Height += h
        Panel1.Height += h
        Me.Height += h
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        SetDDLCategory()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        RaiseEvent CloseCategory(Me)
    End Sub



    'Private Sub txtHeaderDetail_PreviewKeyDown(sender As Object, e As System.Windows.Forms.PreviewKeyDownEventArgs) Handles txtHeaderDetail.PreviewKeyDown
    '    If e.KeyCode = Keys.Tab Then
    '        If UcMainQuotationDescription1.flpDescription.Controls.Count > 0 Then
    '            Dim ctl As UCMainQuotationDescriptionItem = UcMainQuotationDescription1.flpDescription.Controls(0)
    '            ctl.cbbDescription.Focus()
    '        End If
    '    End If
    'End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCByCaseQuotationTermCondition
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.flpTermCondition = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAddRemark = New System.Windows.Forms.PictureBox()
        CType(Me.btnAddRemark, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Silver
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label8.Location = New System.Drawing.Point(0, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(1014, 28)
        Me.Label8.TabIndex = 82
        Me.Label8.Text = "TERMS && CONDITIONS"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'flpTermCondition
        '
        Me.flpTermCondition.AutoScroll = True
        Me.flpTermCondition.BackColor = System.Drawing.SystemColors.Control
        Me.flpTermCondition.Location = New System.Drawing.Point(0, 28)
        Me.flpTermCondition.Margin = New System.Windows.Forms.Padding(0)
        Me.flpTermCondition.Name = "flpTermCondition"
        Me.flpTermCondition.Size = New System.Drawing.Size(1014, 67)
        Me.flpTermCondition.TabIndex = 81
        '
        'btnAddRemark
        '
        Me.btnAddRemark.BackColor = System.Drawing.Color.Silver
        Me.btnAddRemark.Image = Global.BAFCO_TIS.My.Resources.Resources.Add
        Me.btnAddRemark.Location = New System.Drawing.Point(4, 2)
        Me.btnAddRemark.Name = "btnAddRemark"
        Me.btnAddRemark.Size = New System.Drawing.Size(25, 23)
        Me.btnAddRemark.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnAddRemark.TabIndex = 83
        Me.btnAddRemark.TabStop = False
        '
        'UCByCaseQuotationTermCondition
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.btnAddRemark)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.flpTermCondition)
        Me.Name = "UCByCaseQuotationTermCondition"
        Me.Size = New System.Drawing.Size(1014, 96)
        CType(Me.btnAddRemark, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAddRemark As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents flpTermCondition As System.Windows.Forms.FlowLayoutPanel

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCMainQuotationDetail
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.pnlDetail = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSubjectRemark = New System.Windows.Forms.TextBox()
        Me.UcMainQuotationDescription1 = New BAFCO_TIS.UCMainQuotationDescription()
        Me.lblLabelRemarkSubject = New System.Windows.Forms.Label()
        Me.UcMainQuotationRemark1 = New BAFCO_TIS.UCMainQuotationRemark()
        Me.txtFooterNote = New System.Windows.Forms.TextBox()
        Me.btnAddGroup = New System.Windows.Forms.Button()
        Me.lblLabelNote = New System.Windows.Forms.Label()
        Me.flpGroup = New System.Windows.Forms.FlowLayoutPanel()
        Me.txtHeaderDetail = New System.Windows.Forms.TextBox()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.cbbQTCategory = New System.Windows.Forms.ComboBox()
        Me.UcMainQuotationTruck1 = New BAFCO_TIS.UCMainQuotationTruck()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.pnlDetail.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.pnlDetail)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.cbbQTCategory)
        Me.Panel1.Controls.Add(Me.UcMainQuotationTruck1)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Location = New System.Drawing.Point(3, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(844, 190)
        Me.Panel1.TabIndex = 68
        '
        'pnlDetail
        '
        Me.pnlDetail.Controls.Add(Me.Label1)
        Me.pnlDetail.Controls.Add(Me.txtSubjectRemark)
        Me.pnlDetail.Controls.Add(Me.UcMainQuotationDescription1)
        Me.pnlDetail.Controls.Add(Me.lblLabelRemarkSubject)
        Me.pnlDetail.Controls.Add(Me.UcMainQuotationRemark1)
        Me.pnlDetail.Controls.Add(Me.txtFooterNote)
        Me.pnlDetail.Controls.Add(Me.btnAddGroup)
        Me.pnlDetail.Controls.Add(Me.lblLabelNote)
        Me.pnlDetail.Controls.Add(Me.flpGroup)
        Me.pnlDetail.Controls.Add(Me.txtHeaderDetail)
        Me.pnlDetail.Location = New System.Drawing.Point(9, 21)
        Me.pnlDetail.Name = "pnlDetail"
        Me.pnlDetail.Size = New System.Drawing.Size(813, 168)
        Me.pnlDetail.TabIndex = 131
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.Location = New System.Drawing.Point(16, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 22)
        Me.Label1.TabIndex = 122
        Me.Label1.Text = "DETAIL"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSubjectRemark
        '
        Me.txtSubjectRemark.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtSubjectRemark.Location = New System.Drawing.Point(70, 80)
        Me.txtSubjectRemark.MaxLength = 100
        Me.txtSubjectRemark.Name = "txtSubjectRemark"
        Me.txtSubjectRemark.Size = New System.Drawing.Size(674, 20)
        Me.txtSubjectRemark.TabIndex = 3
        '
        'UcMainQuotationDescription1
        '
        Me.UcMainQuotationDescription1.BackColor = System.Drawing.Color.Silver
        Me.UcMainQuotationDescription1.Location = New System.Drawing.Point(69, 26)
        Me.UcMainQuotationDescription1.Name = "UcMainQuotationDescription1"
        Me.UcMainQuotationDescription1.Size = New System.Drawing.Size(675, 28)
        Me.UcMainQuotationDescription1.TabIndex = 81
        '
        'lblLabelRemarkSubject
        '
        Me.lblLabelRemarkSubject.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblLabelRemarkSubject.ForeColor = System.Drawing.Color.White
        Me.lblLabelRemarkSubject.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLabelRemarkSubject.Location = New System.Drawing.Point(9, 80)
        Me.lblLabelRemarkSubject.Name = "lblLabelRemarkSubject"
        Me.lblLabelRemarkSubject.Size = New System.Drawing.Size(62, 22)
        Me.lblLabelRemarkSubject.TabIndex = 130
        Me.lblLabelRemarkSubject.Text = "REMARK"
        Me.lblLabelRemarkSubject.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'UcMainQuotationRemark1
        '
        Me.UcMainQuotationRemark1.BackColor = System.Drawing.Color.SteelBlue
        Me.UcMainQuotationRemark1.Location = New System.Drawing.Point(69, 102)
        Me.UcMainQuotationRemark1.Name = "UcMainQuotationRemark1"
        Me.UcMainQuotationRemark1.Size = New System.Drawing.Size(678, 28)
        Me.UcMainQuotationRemark1.TabIndex = 82
        '
        'txtFooterNote
        '
        Me.txtFooterNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtFooterNote.Location = New System.Drawing.Point(70, 56)
        Me.txtFooterNote.MaxLength = 100
        Me.txtFooterNote.Name = "txtFooterNote"
        Me.txtFooterNote.Size = New System.Drawing.Size(674, 20)
        Me.txtFooterNote.TabIndex = 2
        '
        'btnAddGroup
        '
        Me.btnAddGroup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddGroup.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnAddGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnAddGroup.ForeColor = System.Drawing.Color.White
        Me.btnAddGroup.Location = New System.Drawing.Point(1, 136)
        Me.btnAddGroup.Name = "btnAddGroup"
        Me.btnAddGroup.Size = New System.Drawing.Size(91, 32)
        Me.btnAddGroup.TabIndex = 119
        Me.btnAddGroup.Text = "Add Group"
        Me.btnAddGroup.UseVisualStyleBackColor = False
        '
        'lblLabelNote
        '
        Me.lblLabelNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblLabelNote.ForeColor = System.Drawing.Color.White
        Me.lblLabelNote.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLabelNote.Location = New System.Drawing.Point(18, 56)
        Me.lblLabelNote.Name = "lblLabelNote"
        Me.lblLabelNote.Size = New System.Drawing.Size(49, 22)
        Me.lblLabelNote.TabIndex = 128
        Me.lblLabelNote.Text = "NOTE"
        Me.lblLabelNote.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'flpGroup
        '
        Me.flpGroup.Location = New System.Drawing.Point(5, 131)
        Me.flpGroup.Name = "flpGroup"
        Me.flpGroup.Size = New System.Drawing.Size(799, 5)
        Me.flpGroup.TabIndex = 120
        '
        'txtHeaderDetail
        '
        Me.txtHeaderDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtHeaderDetail.Location = New System.Drawing.Point(71, 3)
        Me.txtHeaderDetail.MaxLength = 100
        Me.txtHeaderDetail.Name = "txtHeaderDetail"
        Me.txtHeaderDetail.Size = New System.Drawing.Size(673, 20)
        Me.txtHeaderDetail.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Image = Global.BAFCO_TIS.My.Resources.Resources.close
        Me.btnClose.Location = New System.Drawing.Point(796, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(20, 20)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 121
        Me.btnClose.TabStop = False
        '
        'cbbQTCategory
        '
        Me.cbbQTCategory.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbQTCategory.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbQTCategory.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.cbbQTCategory.Location = New System.Drawing.Point(80, 2)
        Me.cbbQTCategory.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbQTCategory.Name = "cbbQTCategory"
        Me.cbbQTCategory.Size = New System.Drawing.Size(673, 21)
        Me.cbbQTCategory.TabIndex = 0
        '
        'UcMainQuotationTruck1
        '
        Me.UcMainQuotationTruck1.BackColor = System.Drawing.Color.Silver
        Me.UcMainQuotationTruck1.Location = New System.Drawing.Point(1, 24)
        Me.UcMainQuotationTruck1.Name = "UcMainQuotationTruck1"
        Me.UcMainQuotationTruck1.Size = New System.Drawing.Size(1014, 148)
        Me.UcMainQuotationTruck1.TabIndex = 132
        Me.UcMainQuotationTruck1.Visible = False
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label6.Location = New System.Drawing.Point(-6, -2)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 28)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "CATEGORY"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Red
        Me.Label25.Location = New System.Drawing.Point(-2, 4)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(12, 13)
        Me.Label25.TabIndex = 134
        Me.Label25.Text = "*"
        '
        'UCMainQuotationDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(1)
        Me.Name = "UCMainQuotationDetail"
        Me.Size = New System.Drawing.Size(850, 196)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.pnlDetail.ResumeLayout(False)
        Me.pnlDetail.PerformLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbbQTCategory As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents UcMainQuotationDescription1 As BAFCO_TIS.UCMainQuotationDescription
    Friend WithEvents UcMainQuotationRemark1 As BAFCO_TIS.UCMainQuotationRemark
    Friend WithEvents btnAddGroup As System.Windows.Forms.Button
    Friend WithEvents flpGroup As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtHeaderDetail As System.Windows.Forms.TextBox
    Friend WithEvents txtSubjectRemark As System.Windows.Forms.TextBox
    Friend WithEvents lblLabelRemarkSubject As System.Windows.Forms.Label
    Friend WithEvents txtFooterNote As System.Windows.Forms.TextBox
    Friend WithEvents lblLabelNote As System.Windows.Forms.Label
    Friend WithEvents pnlDetail As System.Windows.Forms.Panel
    Friend WithEvents UcMainQuotationTruck1 As BAFCO_TIS.UCMainQuotationTruck
    Friend WithEvents Label25 As System.Windows.Forms.Label

End Class

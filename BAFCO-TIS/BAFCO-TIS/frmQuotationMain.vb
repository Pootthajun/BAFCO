﻿Imports System.Data.SqlClient

Public Class frmQuotationMain

    Private WithEvents kbHook As New KeyboardHook
    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If
    End Sub

    Private Sub frmQuotationMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.ShowChildForm(frmQuotationMainSearch)
        frmQuotationMainSearch.ShowData()
    End Sub
    Private Sub frmMainQuotation_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ClearForm()
    End Sub



    Public Sub FillInData(MainQuotationID As Long, IsCopy As Boolean)
        Try
            Dim sql As String = "select qm.id, qm.qt_no, qm.qt_date, qm.cus_id, qm.user_id_sale, qm.qt_status, qm.approve_date, qm.deny_date,"
            sql += " c.cus_address, c.cus_account, c.tax_id, ct.cus_type_name, cr.credit_term, u.us_name, u.us_code, qm.qt_note, qm.qt_status, "
            sql += " case qm.qt_status when '0' then 'Draft' when '1' then 'Approved' when '2' then 'Deny' end status_name"
            sql += " from quotation_main qm "
            sql += " inner join customer c on c.cus_id=qm.cus_id "
            sql += " inner join customer_type ct on ct.cus_type_id=c.cus_type_id "
            sql += " left join ms_credit_term cr on cr.id=c.ms_credit_term_id "
            sql += " inner join [user] u on u.us_id=qm.user_id_sale "
            sql += " where qm.id=@_MAIN_QUOTATION_ID "
            Dim p(1) As SqlParameter
            p(0) = SetBigInt("@_MAIN_QUOTATION_ID", MainQuotationID)

            Dim dt As DataTable = Execute_DataTable(sql, p)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow = dt.Rows(0)
                lblID.Text = dr("id")
                cbbCustomer.SelectedValue = dr("cus_id")
                If Convert.IsDBNull(dr("cus_address")) = False Then txtCustomerAddress.Text = dr("cus_address")
                If Convert.IsDBNull(dr("cus_account")) = False Then txtCustomerAcc.Text = dr("cus_account")
                If Convert.IsDBNull(dr("credit_term")) = False Then txtCustomerCreditTerm.Text = dr("credit_term")
                txtCustomerType.Text = dr("cus_type_name")

                txtQuotationDate.Text = Convert.ToDateTime(dr("qt_date")).ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))
                txtSaleName.Text = dr("us_code")
                txtQuotationNo.Text = dr("qt_no")
                If Convert.IsDBNull(dr("qt_note")) = False Then txtQtNote.Text = dr("qt_note")
                txtStatusName.Text = dr("status_name")

                flp1.Controls.Clear()

                'Bind Category Data
                sql = "select qmd.id, qmd.quotation_main_id, qmd.qt_category_id, qmd.qt_group_id, qmd.qt_group_sub_id, "
                sql += " qmd.header_detail, qmd.footer_detail, qmd.subject_remark "
                sql += " from quotation_main_detail qmd "
                sql += " where qmd.quotation_main_id = @_MAIN_QUOTATION_ID "
                sql += " order by qmd.id"
                ReDim p(1)
                p(0) = SetBigInt("@_MAIN_QUOTATION_ID", MainQuotationID)

                dt = Execute_DataTable(sql, p)
                If dt.Rows.Count > 0 Then
                    'Category
                    dt.DefaultView.RowFilter = "qt_category_id<>0 and qt_group_id=0 and qt_group_sub_id=0"
                    If dt.DefaultView.Count > 0 Then
                        Dim cDt As New DataTable
                        cDt = dt.DefaultView.ToTable.Copy
                        dt.DefaultView.RowFilter = ""

                        For Each cDr As DataRow In cDt.Rows
                            'Group Data
                            Dim gDt As New DataTable
                            dt.DefaultView.RowFilter = "qt_category_id = " & cDr("qt_category_id") & " and qt_group_id<>0 and qt_group_sub_id=0"
                            If dt.DefaultView.Count > 0 Then
                                gDt = dt.DefaultView.ToTable.Copy
                            End If
                            dt.DefaultView.RowFilter = ""

                            Dim HeaderDetail As String = ""
                            Dim FooterDetail As String = ""
                            Dim SubjectRemark As String = ""
                            If Convert.IsDBNull(cDr("header_detail")) = False Then HeaderDetail = cDr("header_detail")
                            If Convert.IsDBNull(cDr("footer_detail")) = False Then FooterDetail = cDr("footer_detail")
                            If Convert.IsDBNull(cDr("subject_remark")) = False Then SubjectRemark = cDr("subject_remark")

                            Dim uc As New UCMainQuotationDetail
                            uc.FillInDetailData(MainQuotationID, cDr("id"), cDr("qt_category_id"), HeaderDetail, FooterDetail, SubjectRemark, gDt)
                            AddHandler uc.CloseCategory, AddressOf CloseCategory
                            flp1.Controls.Add(uc)
                        Next
                    End If
                    dt.DefaultView.RowFilter = ""
                End If

                cbbCustomer.Enabled = True
                btnAddSection.Visible = True
                btnApprove.Visible = True
                btnDeny.Visible = True
                btnOverwrite.Visible = False

                lblQtStatus.Text = dr("qt_status")

                If IsCopy = True Then
                    lblID.Text = "0"
                    txtQuotationDate.Text = DateTime.Now.ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))
                    txtQuotationNo.Text = "AUTO"
                    txtSaleName.Text = myUser.us_code
                    lblQtStatus.Text = "0"
                    txtStatusName.Text = "Draft"
                    btnApprove.Visible = False
                    btnDeny.Visible = False
                End If
                If lblQtStatus.Text > "0" Then
                    DisableEditData()
                End If
            End If
            dt.Dispose()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CloseCategory(ctl As UCMainQuotationDetail)
        flp1.Controls.Remove(ctl)
    End Sub


    Private Sub ClearForm()
        lblID.Text = "0"
        lblAbbQuotation.Text = ""
        BindCBB_Customer(cbbCustomer)
        txtCustomerAddress.Text = ""
        txtCustomerAcc.Text = ""
        txtCustomerCreditTerm.Text = ""
        txtCustomerType.Text = ""
        txtQtNote.Text = ""
        txtStatusName.Text = "Draft"
        lblQtStatus.Text = "0"

        txtQuotationDate.Text = DateTime.Now.ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))
        txtSaleName.Text = myUser.us_code

        txtQuotationNo.Text = "AUTO"


        flp1.Controls.Clear()
        btnAddSection_Click(Nothing, Nothing)

    End Sub

    Private Sub cbbCustomer_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbbCustomer.SelectedIndexChanged
        If cbbCustomer.SelectedIndex > 0 Then
            Dim sql As String = "select c.cus_id, c.cus_name, c.cus_account, c.cus_address, ct.cus_type_name, cr.credit_term, ct.abb_quotation"
            sql += " from CUSTOMER c "
            sql += " inner join CUSTOMER_TYPE ct on c.cus_type_id=ct.cus_type_id "
            sql += " left join MS_CREDIT_TERM cr on cr.id=c.ms_credit_term_id "
            sql += " where c.cus_id=@_CUSTOMER_ID"

            Dim p(1) As SqlParameter
            p(0) = SetInt("@_CUSTOMER_ID", cbbCustomer.SelectedValue)

            Dim dt As DataTable = Execute_DataTable(sql, p)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow = dt.Rows(0)
                If Convert.IsDBNull(dr("cus_address")) = False Then txtCustomerAddress.Text = dr("cus_address")
                If Convert.IsDBNull(dr("cus_account")) = False Then txtCustomerAcc.Text = dr("cus_account")
                If Convert.IsDBNull(dr("credit_term")) = False Then txtCustomerCreditTerm.Text = dr("credit_term")
                If Convert.IsDBNull(dr("cus_type_name")) = False Then txtCustomerType.Text = dr("cus_type_name")
                If Convert.IsDBNull(dr("abb_quotation")) = False Then lblAbbQuotation.Text = dr("abb_quotation")
            End If
            dt.Dispose()
        End If
    End Sub

    Private Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If cbbCustomer.SelectedValue = 0 Then
            MessageBox.Show("กรุณาเลือกชื่อลูกค้า", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If
        'หา Customer Type เพื่อตรวจสอบค่า app_quotation
        If lblAbbQuotation.Text.Trim = "" Then
            MessageBox.Show("คุณเลือกประเภทลูกค้าที่มีสามารถ Run เลขที่ใบเสนอราคาได้", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cbbCustomer.Focus()
            Return False
        End If

        If flp1.Controls.Count = 0 Then
            MessageBox.Show("กรุณาเพิ่ม Section", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If

        For Each uc As UCMainQuotationDetail In flp1.Controls

            'Category / Group / Sub Group
            If uc.cbbQTCategory.SelectedValue = 0 Then
                MessageBox.Show("กรุณาเลือก Category", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                uc.cbbQTCategory.Focus()
                Return False
            End If

            If uc.ValidateData() = False Then
                Return False
            End If
        Next

        Return True
    End Function

    Private Function GenNewQuatotionNo() As String
        'Format Ex. BAF-MA-ABB-YYYY-MM-XXX  
        Dim ret As String = ""
        Dim yyyyMM As String = DateTime.Now.ToString("yyyy-MM", New Globalization.CultureInfo("en-US"))

        Dim sql As String = "select top 1 qt.id, qt.qt_no"
        sql += " from QUOTATION_MAIN qt "
        sql += " inner join CUSTOMER c on c.cus_id=qt.cus_id "
        sql += " inner join CUSTOMER_TYPE ct on ct.cus_type_id=c.cus_type_id "
        sql += " where substring(qt.qt_no,8,3)=@_APP_QUOTATION "  'ABB
        sql += " and substring(qt.qt_no,12,7)=@_YYYYMM"   'YYYY-MM
        sql += " order by qt.id desc"

        Dim p(2) As SqlParameter
        p(0) = SetText("@_APP_QUOTATION", lblAbbQuotation.Text)
        p(1) = SetText("@_YYYYMM", yyyyMM)

        ret = "BAF-MA-" & lblAbbQuotation.Text & "-" & yyyyMM & "-"

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt.Rows.Count > 0 Then
            Dim qtNo As String = dt.Rows(0)("qt_no")
            Dim runNo As Integer = Convert.ToInt16(Strings.Right(qtNo, 3)) + 1

            ret += runNo.ToString.PadLeft(3, "0")
        Else
            ret += "001"
        End If
        dt.Dispose()

        Return ret
    End Function

    Private Sub btnAddSection_Click(sender As System.Object, e As System.EventArgs) Handles btnAddSection.Click
        Dim uc As New UCMainQuotationDetail
        AddHandler uc.CloseCategory, AddressOf CloseCategory
        flp1.Controls.Add(uc)
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If ValidateData() = True Then
            Try
                Dim sql As String = ""
                Dim p() As SqlParameter
                Dim NewQuotationNo As String = ""
                If lblID.Text = "0" Then
                    sql = "insert into QUOTATION_MAIN(created_by,created_date,qt_no,qt_date,cus_id,user_id_sale, qt_note, qt_status)"
                    sql += " output inserted.id, inserted.qt_date"
                    sql += " values(@_CREATED_BY, getdate(), @_QT_NO, @_QT_DATE, @_CUS_ID, @_USER_ID_SALE,@_QT_NOTE, @_QT_STATUS) "

                    NewQuotationNo = GenNewQuatotionNo()

                    ReDim p(7)
                    p(0) = SetText("@_CREATED_BY", myUser.fulllname)
                    p(1) = SetText("@_QT_NO", NewQuotationNo)
                    p(2) = SetDateTime("@_QT_DATE", DateTime.Now.Date)
                    p(3) = SetText("@_CUS_ID", cbbCustomer.SelectedValue)
                    p(4) = SetText("@_USER_ID_SALE", myUser.user_id)
                    p(5) = SetText("@_QT_NOTE", txtQtNote.Text)
                    p(6) = SetText("@_QT_STATUS", "0")
                Else
                    sql = " update QUOTATION_MAIN "
                    sql += " set updated_by=@_UPDATED_BY "
                    sql += ", updated_date=getdate()"
                    sql += ", cus_id=@_CUS_ID "
                    sql += ", qt_note=@_QT_NOTE "
                    sql += " output inserted.id, inserted.qt_date "
                    sql += " where id=@_ID "

                    NewQuotationNo = txtQuotationNo.Text

                    ReDim p(4)
                    p(0) = SetText("@_UPDATED_BY", myUser.fulllname)
                    p(1) = SetText("@_CUS_ID", cbbCustomer.SelectedValue)
                    p(2) = SetText("@_QT_NOTE", txtQtNote.Text)
                    p(3) = SetBigInt("@_ID", lblID.Text)
                End If

                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                If conn.State <> ConnectionState.Open Then
                    MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                    Exit Sub
                End If
                Dim trans As SqlTransaction
                trans = conn.BeginTransaction

                Dim dt As DataTable = Execute_DataTable(sql, trans, p)
                If dt.Rows.Count > 0 Then
                    Dim MainQuotationID As Long = Convert.ToInt64(dt.Rows(0)("id"))
                    Dim QtDate As Date = Convert.ToDateTime(dt.Rows(0)("qt_date")).Date

                    sql = "delete from QUOTATION_MAIN_REMARK"
                    sql += " where quotation_main_detail_id in (select id from QUOTATION_MAIN_DETAIL where quotation_main_id=@_QT_ID)"
                    ReDim p(1)
                    p(0) = SetBigInt("@_QT_ID", MainQuotationID)
                    Execute_Command(sql, trans, p)

                    sql = "delete from QUOTATION_MAIN_DESC"
                    sql += " where quotation_main_detail_id in (select id from QUOTATION_MAIN_DETAIL where quotation_main_id=@_QT_ID)"
                    ReDim p(1)
                    p(0) = SetBigInt("@_QT_ID", MainQuotationID)
                    Execute_Command(sql, trans, p)

                    sql = "delete from QUOTATION_MAIN_TRUCK_ITEM "
                    sql += " where quotation_main_detail_id in (select id from QUOTATION_MAIN_DETAIL where quotation_main_id=@_QT_ID)"
                    ReDim p(1)
                    p(0) = SetBigInt("@_QT_ID", MainQuotationID)
                    Execute_Command(sql, trans, p)

                    sql = "delete from QUOTATION_MAIN_DETAIL "
                    sql += " where quotation_main_id=@_QT_ID"
                    ReDim p(1)
                    p(0) = SetBigInt("@_QT_ID", MainQuotationID)
                    Execute_Command(sql, trans, p)

                    Dim re As String = "false"
                    For Each dtl As UCMainQuotationDetail In flp1.Controls
                        sql = " insert into quotation_main_detail(created_by,created_date,quotation_main_id,"
                        sql += " qt_category_id, qt_group_id, qt_group_sub_id,header_detail,footer_detail,subject_remark)"
                        sql += " output inserted.id "
                        sql += " values(@_CREATED_BY, getdate(), @_QUOTATION_MAIN_ID, @_QT_CATEGORY_ID, @_QT_GROUP_ID, @_QT_GROUP_SUB_ID,"
                        sql += " @_HEADER_DETAIL, @_FOOTER_DETAIL, @_SUBJECT_REMARK)"

                        ReDim p(8)
                        p(0) = SetText("@_CREATED_BY", myUser.fulllname)
                        p(1) = SetBigInt("@_QUOTATION_MAIN_ID", MainQuotationID)
                        p(2) = SetBigInt("@_QT_CATEGORY_ID", dtl.cbbQTCategory.SelectedValue)
                        p(3) = SetBigInt("@_QT_GROUP_ID", 0)
                        p(4) = SetBigInt("@_QT_GROUP_SUB_ID", 0)
                        p(5) = SetText("@_HEADER_DETAIL", dtl.txtHeaderDetail.Text)
                        p(6) = SetText("@_FOOTER_DETAIL", dtl.txtFooterNote.Text)
                        p(7) = SetText("@_SUBJECT_REMARK", dtl.txtSubjectRemark.Text)

                        dt = Execute_DataTable(sql, trans, p)
                        If dt.Rows.Count > 0 Then
                            Dim _qtd_id As Long = Convert.ToInt64(dt.Rows(0)("id"))
                            'Category Description
                            If dtl.UcMainQuotationDescription1.flpDescription.Controls.Count > 0 Then
                                re = dtl.UcMainQuotationDescription1.SaveMainQuotationDescription(cbbCustomer.SelectedValue, QtDate, _qtd_id, NewQuotationNo, lblQtStatus.Text, trans)
                            Else
                                re = "true"
                            End If

                            If re = "true" Then
                                'Category Remark
                                If dtl.UcMainQuotationRemark1.flpRemark.Controls.Count > 0 Then
                                    re = dtl.UcMainQuotationRemark1.SaveMainQuotationRemarks(_qtd_id, trans)
                                End If

                                If re = "true" Then
                                    If dtl.pnlDetail.Visible = True Then
                                        For Each gQt As UCMainQuotationGroup In dtl.flpGroup.Controls
                                            re = gQt.SaveMainQuotationGroup(cbbCustomer.SelectedValue, QtDate, NewQuotationNo, lblQtStatus.Text, MainQuotationID, trans)
                                            If re = "false" Then
                                                Exit For
                                            End If
                                        Next
                                    Else
                                        re = dtl.UcMainQuotationTruck1.SaveMainQuotationTruckData(_qtd_id, trans)
                                        If re = "false" Then
                                            Exit For
                                        End If
                                    End If
                                Else
                                    Exit For
                                End If
                            Else
                                Exit For
                            End If
                        Else
                            Exit For
                        End If
                    Next
                    If re = "true" Then
                        trans.Commit()
                        'lblID.Text = MainQuotationID
                        'txtQuotationNo.Text = NewQuotationNo
                        'btnApprove.Visible = True
                        'btnDeny.Visible = True
                        FillInData(MainQuotationID, False)

                        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        trans.Rollback()
                        MessageBox.Show(re, "Save Fail", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End If
                Else
                    trans.Rollback()
                End If
                dt.Dispose()
                conn.Close()
            Catch ex As Exception
                CreateLogException(ex)
            End Try
        End If
    End Sub

    

    Private Sub DisableEditData()
        For Each ctl As UCMainQuotationDetail In flp1.Controls
            ctl.EnableMainQuotationDetail = False
        Next

        cbbCustomer.Enabled = False
        'txtQtNote.Enabled = False
        'btnSave.Visible = False
        btnAddSection.Visible = False
        btnApprove.Visible = False
        btnDeny.Visible = False

        If lblQtStatus.Text = "1" Then  'Approved
            btnOverwrite.Visible = True
        End If

    End Sub



    Private Sub btnApprove_Click(sender As System.Object, e As System.EventArgs) Handles btnApprove.Click
        If lblID.Text = "0" Then Exit Sub

        Dim frm As New frmDialogQuotationMainApprove
        'Dim y As DialogResult = MessageBox.Show("ยืนยันการอนุมัติ Main Quotation?", "Confirm?", MessageBoxButtons.OKCancel)
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            If conn.State <> ConnectionState.Open Then
                MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                Exit Sub
            End If
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction


            Dim sql As String = " update QUOTATION_MAIN "
            sql += " set qt_status=1, effect_from_date=@_EFFECT_FROM_DATE, effect_to_date=@_EFFECT_TO_DATE"
            sql += " where id=@_MAIN_QUOTATION_ID "

            Dim p(3) As SqlParameter
            p(0) = SetBigInt("@_MAIN_QUOTATION_ID", lblID.Text)
            p(1) = SetDateTime("@_EFFECT_FROM_DATE", frm.dpDateFrom.Value.Date)
            p(2) = SetDateTime("@_EFFECT_TO_DATE", frm.dpDateTo.Value.Date)

            If Execute_Command(sql, trans, p) = "true" Then
                'อาจจะต้องมีการส่งอข้อมูลไปให้กับระบบ Transportation
                
                Dim ret As Boolean = ChangeStatusToServiceCost(trans)
                If ret = True Then
                    trans.Commit()
                    txtStatusName.Text = "Approved"
                    lblQtStatus.Text = "1"
                    DisableEditData()

                Else
                    trans.Rollback()
                End If
            Else
                trans.Rollback()
            End If
        End If
    End Sub


    Private Sub btnOverwrite_Click(sender As System.Object, e As System.EventArgs) Handles btnOverwrite.Click
        Dim frm As New frmDialogOverwriteReason
        frm.txtSaleName.Text = myUser.fulllname
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim ret As String = SaveOverwriteQuotation("QUOTATION_MAIN", lblID.Text, frm.txtReason.Text.Trim)
            If ret = "true" Then
                Dim sql As String = "update QUOTATION_MAIN "
                sql += " set qt_status=0"
                sql += " where id=@_ID"
                Dim p(1) As SqlParameter
                p(0) = SetBigInt("@_ID", lblID.Text)

                ret = Execute_Command(sql, p)
                If ret = "true" Then
                    Me.Close()

                    Dim f As New frmQuotationMain
                    f.MdiParent = frmMain
                    f.Show()
                    f.FillInData(lblID.Text, False)
                Else
                    MessageBox.Show("Update Quotation Status Fail", "Overwrite Fail", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Else
                MessageBox.Show(ret, "Overwrite Fail", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
    End Sub

    Private Function ChangeStatusToServiceCost(trans As SqlTransaction) As Boolean
        Dim Sql As String = "select q.qt_date, cd.desc_name,bs.basis_name, md.price,md.item_note,q.qt_status"
        Sql += " from QUOTATION_MAIN_DESC md "
        Sql += " inner join QUOTATION_MAIN_DETAIL qmd on qmd.id=md.quotation_main_detail_id"
        Sql += " inner join QUOTATION_MAIN q on q.id=qmd.quotation_main_id"
        Sql += " inner join QT_CATEGORY_DESCRIPTION cd on cd.id=md.qt_category_description_id"
        Sql += " inner join MS_BASIS bs on bs.id=md.ms_basis_id "
        Sql += " where md.quotation_main_detail_id in (select id from QUOTATION_MAIN_DETAIL where quotation_main_id=@_QUOTATION_ID)"
        Dim p(1) As SqlParameter
        p(0) = SetBigInt("@_QUOTATION_ID", lblID.Text)

        Dim ret As Boolean = False
        Dim dt As DataTable = Execute_DataTable(Sql, trans, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim ItemNote As String = ""
                If Convert.IsDBNull(dr("item_note")) = False Then ItemNote = dr("item_note")

                ret = SaveDataToServiceCost(trans, cbbCustomer.SelectedValue, dr("desc_name"), dr("price"), dr("basis_name"), ItemNote, "Main", Convert.ToDateTime(dr("qt_date")), txtQuotationNo.Text, dr("qt_status")).ToString.ToLower
                If ret = False Then
                    Exit For
                End If
            Next
        End If
        dt.Dispose()

        Return ret
    End Function

    Private Sub btnDeny_Click(sender As System.Object, e As System.EventArgs) Handles btnDeny.Click
        If lblID.Text = "0" Then Exit Sub

        Dim y As DialogResult = MessageBox.Show("ยืนยันการยกเลิก Main Quotation?", "Confirm?", MessageBoxButtons.OKCancel)
        If y = Windows.Forms.DialogResult.OK Then

            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            If conn.State <> ConnectionState.Open Then
                MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                Exit Sub
            End If
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction

            Dim sql As String = " update QUOTATION_MAIN "
            sql += " set qt_status=2"
            sql += " where id=@_MAIN_QUOTATION_ID "

            Dim p(1) As SqlParameter
            p(0) = SetBigInt("@_MAIN_QUOTATION_ID", lblID.Text)

            If Execute_Command(sql, trans, p) = "true" Then

                Dim ret As Boolean = ChangeStatusToServiceCost(trans)
                If ret = True Then
                    trans.Commit()
                    DisableEditData()
                    txtStatusName.Text = "Deny"
                Else
                    trans.Rollback()
                End If
            Else
                trans.Rollback()
            End If
        End If
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Dim sql As String = "select q.id, q.qt_no, convert(varchar(10),q.qt_date,103) qt_date, c.cus_name, c.cus_address, q.qt_note subject,qmd.quotation_main_detail_id category_detail_id, qmd.qt_category_id,qmd.category_name,qmd.category_header_note, qmd.category_subject_remark," & Environment.NewLine
        sql += " isnull(qmg.quotation_main_detail_id,0) group_detail_id, qmg.qt_group_id, qmg.group_name, qmg.group_header_note, qmg.group_subject_remark," & Environment.NewLine
        sql += " isnull(qmsg.quotation_main_detail_id,0) subgroup_detail_id, qmsg.qt_group_sub_id, qmsg.subgroup_name, qmsg.subgroup_header_note, qmsg.subgroup_subject_remark, " & Environment.NewLine
        sql += " isnull(qmd.category_count_desc,0) category_count_desc, isnull(qmg.group_count_desc,0) group_count_desc, isnull(qmsg.subgroup_count_desc,0) subgroup_count_desc ," & Environment.NewLine
        sql += " isnull(qmd.category_count_remark,0) category_count_remark, isnull(qmg.group_count_remark,0) group_count_remark, isnull(qmsg.subgroup_count_remark,0) subgroup_count_remark," & Environment.NewLine
        sql += " qmd.count_transport " & Environment.NewLine
        sql += " from QUOTATION_MAIN q" & Environment.NewLine
        sql += " inner join CUSTOMER c on c.cus_id=q.cus_id" & Environment.NewLine
        sql += " inner join (" & Environment.NewLine
        sql += " 	select qmd.id quotation_main_detail_id, qmd.quotation_main_id,qmd.qt_category_id, cat.category_name, " & Environment.NewLine
        sql += "    qmd.header_detail category_header_note, qmd.footer_detail category_footer_note, qmd.subject_remark category_subject_remark, " & Environment.NewLine
        sql += "    (select count(id) from QUOTATION_MAIN_DESC where quotation_main_detail_id=qmd.id) category_count_desc, " & Environment.NewLine
        sql += "    (select count(id) from QUOTATION_MAIN_REMARK where quotation_main_detail_id=qmd.id) category_count_remark, " & Environment.NewLine
        sql += "    (select count(id) from QUOTATION_MAIN_TRUCK_ITEM where quotation_main_detail_id=qmd.id) count_transport " & Environment.NewLine
        sql += " 	from QUOTATION_MAIN_DETAIL qmd" & Environment.NewLine
        sql += " 	inner join QT_CATEGORY cat on cat.id=qmd.qt_category_id" & Environment.NewLine
        sql += "    where qmd.qt_category_id <> 0 And qmd.qt_group_id = 0 And qmd.qt_group_sub_id = 0" & Environment.NewLine
        sql += " ) qmd on q.id=qmd.quotation_main_id " & Environment.NewLine
        sql += " left join (" & Environment.NewLine
        sql += " 	select qmg.id quotation_main_detail_id, qmg.quotation_main_id, qmg.qt_category_id,qmg.qt_group_id, g.group_name, " & Environment.NewLine
        sql += "    qmg.header_detail group_header_note, qmg.footer_detail group_footer_note, qmg.subject_remark group_subject_remark, " & Environment.NewLine
        sql += "    (select count(id) from QUOTATION_MAIN_DESC where quotation_main_detail_id=qmg.id) group_count_desc, " & Environment.NewLine
        sql += "    (select count(id) from QUOTATION_MAIN_REMARK where quotation_main_detail_id=qmg.id) group_count_remark " & Environment.NewLine
        sql += " 	from QUOTATION_MAIN_DETAIL qmg" & Environment.NewLine
        sql += " 	inner join QT_CATEGORY cat on cat.id=qmg.qt_category_id" & Environment.NewLine
        sql += " 	inner join QT_GROUP g on g.id=qmg.qt_group_id" & Environment.NewLine
        sql += "    where qmg.qt_category_id <> 0 And qmg.qt_group_id <> 0 And qmg.qt_group_sub_id = 0" & Environment.NewLine
        sql += " ) qmg on q.id= qmg.quotation_main_id and qmd.qt_category_id=qmg.qt_category_id" & Environment.NewLine
        sql += " left join (" & Environment.NewLine
        sql += " 	select qmsg.id quotation_main_detail_id, qmsg.quotation_main_id, qmsg.qt_category_id,qmsg.qt_group_id, qmsg.qt_group_sub_id, sg.subgroup_name, " & Environment.NewLine
        sql += "    qmsg.header_detail subgroup_header_note, qmsg.footer_detail subgroup_footer_note, qmsg.subject_remark subgroup_subject_remark, " & Environment.NewLine
        sql += "    (select count(id) from QUOTATION_MAIN_DESC where quotation_main_detail_id=qmsg.id) subgroup_count_desc, " & Environment.NewLine
        sql += "    (select count(id) from QUOTATION_MAIN_REMARK where quotation_main_detail_id=qmsg.id) subgroup_count_remark " & Environment.NewLine
        sql += " 	from QUOTATION_MAIN_DETAIL qmsg" & Environment.NewLine
        sql += " 	inner join QT_CATEGORY cat on cat.id=qmsg.qt_category_id" & Environment.NewLine
        sql += " 	inner join QT_GROUP g on g.id=qmsg.qt_group_id" & Environment.NewLine
        sql += " 	inner join QT_GROUP_SUB sg on sg.id=qmsg.qt_group_sub_id" & Environment.NewLine
        sql += " 	where qmsg.qt_category_id<>0 and qmsg.qt_group_id<>0 and qmsg.qt_group_sub_id<>0" & Environment.NewLine
        sql += " ) qmsg on q.id=qmsg.quotation_main_id and qmd.qt_category_id=qmsg.qt_category_id and qmg.qt_group_id=qmsg.qt_group_id" & Environment.NewLine
        sql += " where q.id=@_QUOTATION_ID" & Environment.NewLine
        sql += " order by qmd.quotation_main_detail_id"

        Dim p(1) As SqlParameter
        p(0) = SetBigInt("@_QUOTATION_ID", lblID.Text)

        Dim dt As DataTable = Execute_DataTable(sql, p)

        If dt.Rows.Count > 0 Then
            Dim f As New frmPrintQuotation
            With f
                .QuotationID = lblID.Text
                .ReportName = "Quotation_Main.rpt"
                .DT = dt
                .ShowDialog()
            End With
        End If
    End Sub

End Class
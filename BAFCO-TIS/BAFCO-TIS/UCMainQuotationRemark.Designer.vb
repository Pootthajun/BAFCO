﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCMainQuotationRemark
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.flpRemark = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnAddRemark = New System.Windows.Forms.PictureBox()
        CType(Me.btnAddRemark, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Silver
        Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label8.Location = New System.Drawing.Point(0, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(675, 28)
        Me.Label8.TabIndex = 82
        Me.Label8.Text = "Remark"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'flpRemark
        '
        Me.flpRemark.AutoScroll = True
        Me.flpRemark.BackColor = System.Drawing.SystemColors.Control
        Me.flpRemark.Location = New System.Drawing.Point(0, 28)
        Me.flpRemark.Margin = New System.Windows.Forms.Padding(0)
        Me.flpRemark.Name = "flpRemark"
        Me.flpRemark.Size = New System.Drawing.Size(675, 0)
        Me.flpRemark.TabIndex = 81
        '
        'btnAddRemark
        '
        Me.btnAddRemark.BackColor = System.Drawing.Color.Silver
        Me.btnAddRemark.Image = Global.BAFCO_TIS.My.Resources.Resources.Add
        Me.btnAddRemark.Location = New System.Drawing.Point(4, 2)
        Me.btnAddRemark.Name = "btnAddRemark"
        Me.btnAddRemark.Size = New System.Drawing.Size(25, 23)
        Me.btnAddRemark.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnAddRemark.TabIndex = 83
        Me.btnAddRemark.TabStop = False
        '
        'UCMainQuotationRemark
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.SteelBlue
        Me.Controls.Add(Me.btnAddRemark)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.flpRemark)
        Me.Name = "UCMainQuotationRemark"
        Me.Size = New System.Drawing.Size(675, 28)
        CType(Me.btnAddRemark, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAddRemark As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents flpRemark As System.Windows.Forms.FlowLayoutPanel

End Class

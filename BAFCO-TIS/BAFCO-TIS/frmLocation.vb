﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmLocation

    Dim DA As New SqlDataAdapter

    Private Sub frmLocation_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        grdMain.AutoGenerateColumns = False
        ShowData()
        txtSearch.Focus()
    End Sub

    Private Sub ShowData()
        Dim SQL As String = "SELECT * FROM ROUTE ORDER BY ROUTE_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT
        lblRec.Text = "Total   " & DT.Rows.Count & "   Record"
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("ROUTE_NAME")) OrElse tmp.Rows(i).Item("ROUTE_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "ROUTE_NAME='" & tmp.Rows(i).Item("ROUTE_NAME") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        '------------------Check Deleted Record-----
        Dim SQL As String = ""
        SQL &= "SELECT DISTINCT * FROM (" & vbCrLf
        SQL &= "SELECT DISTINCT ROUTE_FROM AS ROUTE_ID FROM COST_SUP" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT DISTINCT ROUTE_TO AS ROUTE_ID FROM COST_SUP" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT DISTINCT ROUTE_FROM AS ROUTE_ID FROM COST_CUS" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT DISTINCT ROUTE_TO AS ROUTE_ID FROM COST_CUS" & vbCrLf
        SQL &= ") AS TB" & vbCrLf

        Dim TA As New SqlDataAdapter(SQL, ConnStr)
        Dim TT As New DataTable
        TA.Fill(TT)
        For i As Integer = 0 To TT.Rows.Count - 1
            If IsDBNull(TT.Rows(i).Item("ROUTE_ID")) Then Continue For
            tmp.DefaultView.RowFilter = "ROUTE_ID=" & TT.Rows(i).Item("ROUTE_ID")
            If tmp.DefaultView.Count = 0 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลที่คุณต้องการลบ ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ShowData()
                Exit Sub
            End If
        Next

        '------------------Batch Save---------------
        Dim ID As Integer = 0
        ID = GetNewID("ROUTE", "ROUTE_ID")
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(DT.Rows(i).Item("ROUTE_ID")) Then
                DT.Rows(i).Item("ROUTE_ID") = ID
                ID = ID + 1
                DT.Rows(i).Item("UPDATE_BY") = myUser.user_id
                DT.Rows(i).Item("UPDATE_DATE") = Now
            End If
        Next

        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            Exit Sub
        End Try

        txtSearch.Text = ""
        ShowData()
        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= "ROUTE_ID IS NULL OR "
        End If
        Filter &= "ROUTE_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR "
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        frmLocation_Load(Nothing, Nothing)
    End Sub
End Class
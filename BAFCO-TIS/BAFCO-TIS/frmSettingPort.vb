﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmSettingPort

    Dim DA As New SqlDataAdapter

    Private Sub frmSettingPort_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        txtSearch.Text = ""
        rdiSearchAll.Checked = True
        cbbCountry.SelectedValue = 0
        ShowData()
        Filter()
    End Sub

    Dim DT_COUNTRY As New DataTable
    Dim DT_CITY As New DataTable
    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT * FROM MS_PORT ORDER BY port_name"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT

        SQL = "SELECT id, country_name FROM ms_country where active_status='Y' ORDER BY country_name"
        DT_COUNTRY = Execute_DataTable(SQL)
        Dim dr_country As DataRow = DT_COUNTRY.NewRow
        dr_country("id") = 0
        dr_country("country_name") = ""
        DT_COUNTRY.Rows.InsertAt(dr_country, 0)

        SQL = "select id, city_name, ms_country_id from ms_city where active_status='Y' order by city_name"
        DT_CITY = Execute_DataTable(SQL)
        Dim dr_city As DataRow = DT_CITY.NewRow
        dr_city("id") = 0
        dr_city("city_name") = ""
        dr_city("ms_country_id") = 0
        DT_CITY.Rows.InsertAt(dr_city, 0)


        Dim PORT_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        PORT_ID.Name = "PORT_ID"
        PORT_ID.DataPropertyName = "id"
        PORT_ID.Visible = False
        grdMain.Columns.Add(PORT_ID)
        grdMain.Columns("PORT_ID").Visible = False

        Dim PORT_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        PORT_NAME.DataPropertyName = "PORT_NAME"
        PORT_NAME.HeaderText = "Port Name"
        PORT_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        PORT_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(PORT_NAME)

        Dim COUNTRY_ID As New System.Windows.Forms.DataGridViewComboBoxColumn
        COUNTRY_ID.DataSource = DT_COUNTRY
        COUNTRY_ID.Name = "COUNTRY_ID"
        COUNTRY_ID.DisplayMember = "country_name"
        COUNTRY_ID.ValueMember = "id"
        COUNTRY_ID.DataPropertyName = "ms_country_id"
        COUNTRY_ID.HeaderText = "Country"
        COUNTRY_ID.Width = 150
        grdMain.Columns.Add(COUNTRY_ID)

        Dim CITY_ID As New System.Windows.Forms.DataGridViewComboBoxColumn
        CITY_ID.DataSource = DT_CITY
        CITY_ID.Name = "CITY_ID"
        CITY_ID.DisplayMember = "city_name"
        CITY_ID.ValueMember = "id"
        CITY_ID.DataPropertyName = "ms_city_id"
        CITY_ID.HeaderText = "City"
        CITY_ID.Width = 150
        grdMain.Columns.Add(CITY_ID)

        Dim CAT_ACTIVE As New System.Windows.Forms.DataGridViewCheckBoxColumn
        CAT_ACTIVE.DataPropertyName = "active_status"
        CAT_ACTIVE.TrueValue = "Y"
        CAT_ACTIVE.FalseValue = "N"
        CAT_ACTIVE.HeaderText = "Active"
        CAT_ACTIVE.Width = 100
        grdMain.Columns.Add(CAT_ACTIVE)

        BindCBB_Country(cbbCountry)
    End Sub

    'Private Sub grdMain_CellBeginEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles grdMain.CellBeginEdit
    '    If e.ColumnIndex = grdMain.Columns("COUNTRY_ID").Index Then
    '        'Dim dgvCbo As DataGridViewComboBoxCell = TryCast(grdMain(grdMain.Columns("CITY_ID").Index, e.RowIndex), DataGridViewComboBoxCell)
    '        'If dgvCbo IsNot Nothing Then
    '        '    Dim country_id As String = grdMain.Item(grdMain.Columns("COUNTRY_ID").Index, grdMain.CurrentRow.Index).Value.ToString
    '        '    Dim dv As New DataView(DT_CITY)
    '        '    dv.RowFilter = "ms_country_id = " & country_id
    '        '    dgvCbo.DataSource = dv
    '        '    dgvCbo.Value = 0
    '        'End If

    '        BindCBB_CellCity(e.RowIndex, e.ColumnIndex)
    '    End If
    'End Sub

    'Private Sub grdMain_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMain.CellEndEdit
    '    If e.ColumnIndex = grdMain.Columns("COUNTRY_ID").Index Then
    '        'Dim dgvCbo As DataGridViewComboBoxCell = TryCast(grdMain(grdMain.Columns("CITY_ID").Index, e.RowIndex), DataGridViewComboBoxCell)
    '        'If dgvCbo IsNot Nothing Then
    '        '    Dim country_id As String = grdMain.Item(grdMain.Columns("COUNTRY_ID").Index, grdMain.CurrentRow.Index).Value.ToString
    '        '    Dim dv As New DataView(DT_CITY)
    '        '    dv.RowFilter = "ms_country_id = " & country_id
    '        '    dgvCbo.DataSource = dv
    '        '    dgvCbo.Value = 0
    '        'End If

    '        BindCBB_CellCity(e.RowIndex, e.ColumnIndex)
    '    End If
    'End Sub

    Private Sub BindCBB_CellCity(RowIndex As Int16, ColIndex As Integer)
        If ColIndex = grdMain.Columns("COUNTRY_ID").Index Then
            Dim dgvCbo As DataGridViewComboBoxCell = TryCast(grdMain(grdMain.Columns("CITY_ID").Index, RowIndex), DataGridViewComboBoxCell)
            If dgvCbo IsNot Nothing Then
                Dim country_id As String = grdMain.Item(grdMain.Columns("COUNTRY_ID").Index, grdMain.CurrentRow.Index).Value.ToString

                Dim Sql As String = "select id, city_name, ms_country_id from ms_city "
                Sql += " where active_status='Y' "
                Sql += " and ms_country_id=" & country_id
                Sql += " order by city_name"

                Dim dt As DataTable = Execute_DataTable(Sql)
                Dim dr_city As DataRow = dt.NewRow
                dr_city("id") = 0
                dr_city("city_name") = ""
                dr_city("ms_country_id") = 0
                dt.Rows.InsertAt(dr_city, 0)

                dgvCbo.DataSource = dt
                dgvCbo.Value = 0
            End If
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("PORT_NAME")) OrElse tmp.Rows(i).Item("PORT_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("MS_COUNTRY_ID")) OrElse tmp.Rows(i).Item("MS_COUNTRY_ID").ToString = "0" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("MS_CITY_ID")) OrElse tmp.Rows(i).Item("MS_CITY_ID").ToString = "0" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "PORT_NAME='" & tmp.Rows(i).Item("PORT_NAME").ToString.Replace("'", "''") & "' and MS_CITY_ID='" & tmp(i)("MS_CITY_ID") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next

        '------------------Batch Save---------------
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction

        Dim ret As String = "false"
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim sql As String = ""

            Dim p(5) As SqlParameter
            If DT.Rows(i)("id") > 0 Then
                sql = "update ms_port "
                sql += " set port_name=@_PORT_NAME"
                sql += ", ms_city_id=@_CITY_ID"
                sql += ", ms_country_id=@_COUNTRY_ID"
                sql += ", active_status=@_ACTIVE_STATUS "
                sql += ", updated_by=@_UPDATED_BY "
                sql += ", updated_date=getdate()"
                sql += " where id=@_ID"

                p(0) = SetBigInt("@_ID", DT.Rows(i)("id"))
                p(1) = SetText("@_UPDATED_BY", myUser.us_code)
            Else
                sql = "insert into ms_port (created_by, created_date, port_name, ms_city_id, ms_country_id,  active_status)"
                sql += " values(@_CREATED_BY, getdate(), @_PORT_NAME, @_CITY_ID, @_COUNTRY_ID, @_ACTIVE_STATUS) "

                p(0) = SetText("@_CREATED_BY", myUser.us_code)
            End If

            p(2) = SetText("@_PORT_NAME", DT.Rows(i)("port_name"))
            p(3) = SetBigInt("@_CITY_ID", DT.Rows(i)("ms_city_id"))
            p(4) = SetBigInt("@_COUNTRY_ID", DT.Rows(i)("ms_country_id"))
            p(5) = SetText("@_ACTIVE_STATUS", IIf(DT.Rows(i)("active_status") = "Y", "Y", "N"))

            ret = Execute_Command(sql, trans, p)
            If ret.ToLower <> "true" Then
                Exit For
            End If
        Next

        If ret.ToLower = "true" Then
            trans.Commit()

            txtSearch.Text = ""
            rdiSearchAll.Checked = True
            cbbCountry.SelectedValue = 0
            frmSettingPort_Load(Nothing, Nothing)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            trans.Rollback()
            MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DR("id") = 0
        DR("ms_city_id") = 0
        DR("ms_country_id") = 0
        DR("active_status") = "Y"

        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_KeyUp(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        Filter()
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        Filter()
    End Sub

    Private Sub cbbGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbbCountry.SelectedIndexChanged
        SetCaseCadingCountry(cbbCountry, cbbCity)

        Filter()
    End Sub

    Private Sub cbbCity_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbCity.SelectedIndexChanged
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        If grdMain.DataSource Is Nothing Then Exit Sub
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= " ID = 0 OR  "
        End If
        Filter &= " PORT_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' AND "
        If cbbCountry.SelectedIndex > 0 Then
            Filter += " MS_COUNTRY_ID = " & cbbCountry.SelectedValue & " AND "
        End If
        If cbbCity.SelectedIndex > 0 Then
            Filter += " MS_CITY_ID = " & cbbCity.SelectedValue & " AND "
        End If

        If rdiSearchActive.Checked = True Then
            Filter += " active_status='Y' AND "
        End If
        If rdiSearchInactive.Checked = True Then
            Filter += " active_status='N' and "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter

        lblRec.Text = "Total   " & DT.DefaultView.Count & "   Record"
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click

        frmSettingPort_Load(Nothing, Nothing)
    End Sub

    Private Sub grdMain_CurrentCellDirtyStateChanged(sender As Object, e As System.EventArgs) Handles grdMain.CurrentCellDirtyStateChanged


        BindCBB_CellCity(grdMain.CurrentRow.Index, grdMain.CurrentCell.ColumnIndex)
    End Sub

    Private Sub grdMain_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grdMain.DataError

    End Sub

    Private Sub grdMain_DefaultValuesNeeded(sender As Object, e As System.Windows.Forms.DataGridViewRowEventArgs) Handles grdMain.DefaultValuesNeeded
        e.Row.Cells("CITY_ID").Value = 0
    End Sub

End Class
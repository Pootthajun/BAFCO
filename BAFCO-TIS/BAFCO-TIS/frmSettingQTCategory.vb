﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmSettingQTCategory

    Dim DA As New SqlDataAdapter

    Private Sub frmSettingQTCategory_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        txtSearch.Text = ""
        rdiSearchAll.Checked = True
        ShowData()
        Filter()
    End Sub

    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT * FROM QT_CATEGORY ORDER BY category_name"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT

        Dim CAT_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        CAT_ID.Name = "CAT_ID"
        CAT_ID.DataPropertyName = "id"
        CAT_ID.Visible = False
        grdMain.Columns.Add(CAT_ID)
        grdMain.Columns("CAT_ID").Visible = False

        Dim CAT_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        CAT_NAME.DataPropertyName = "CATEGORY_NAME"
        CAT_NAME.HeaderText = "Category Name"
        CAT_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        CAT_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(CAT_NAME)

        Dim CAT_ACTIVE As New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CAT_ACTIVE.DataPropertyName = "active_status"
        CAT_ACTIVE.TrueValue = "Y"
        CAT_ACTIVE.FalseValue = "N"
        CAT_ACTIVE.HeaderText = "Active"
        CAT_ACTIVE.Width = 100
        grdMain.Columns.Add(CAT_ACTIVE)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("CATEGORY_NAME")) OrElse tmp.Rows(i).Item("CATEGORY_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            'If IsDBNull(tmp.Rows(i).Item("ID")) OrElse tmp.Rows(i).Item("ID").ToString = "" Then
            '    MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            '    Exit Sub
            'End If
            tmp.DefaultView.RowFilter = "CATEGORY_NAME='" & tmp.Rows(i).Item("CATEGORY_NAME").ToString.Replace("'", "''") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next

        ''------------------Check Deleted Record-----
        'Dim SQL As String = ""
        'SQL &= "SELECT DISTINCT * FROM (" & vbCrLf
        'SQL &= "SELECT DISTINCT SUP_ID FROM COST_SUP" & vbCrLf
        'SQL &= "UNION ALL" & vbCrLf
        'SQL &= "SELECT DISTINCT SUP_ID FROM SERVICE_SUP" & vbCrLf
        'SQL &= ") AS TB" & vbCrLf
        'Dim TA As New SqlDataAdapter(SQL, ConnStr)
        'Dim TT As New DataTable
        'TA.Fill(TT)
        'For i As Integer = 0 To TT.Rows.Count - 1
        '    If IsDBNull(TT.Rows(i).Item("SUP_ID")) Then Continue For
        '    tmp.DefaultView.RowFilter = "SUP_ID=" & TT.Rows(i).Item("SUP_ID")
        '    If tmp.DefaultView.Count = 0 Then
        '        MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลที่คุณต้องการลบ ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '        ShowData()
        '        Exit Sub
        '    End If
        'Next
        '------------------Batch Save---------------


        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction

        Dim ret As String = "false"
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim sql As String = ""

            Dim p(4) As SqlParameter
            If DT.Rows(i)("id") > 0 Then
                sql = "update qt_category "
                sql += " set category_name=@_CATEGORY_NAME"
                sql += ", active_status=@_ACTIVE_STATUS "
                sql += ", updated_by=@_UPDATED_BY "
                sql += ", updated_date=getdate()"
                sql += " where id=@_ID"

                p(0) = SetBigInt("@_ID", DT.Rows(i)("id"))
                p(1) = SetText("@_UPDATED_BY", myUser.us_code)
            Else
                sql = "insert into qt_category(created_by, created_date, category_name, unique_key, active_status)"
                sql += " values(@_CREATED_BY, getdate(), @_CATEGORY_NAME, @_UNIQUE_KEY, @_ACTIVE_STATUS) "

                p(0) = SetText("@_CREATED_BY", myUser.us_code)
                p(1) = SetText("@_UNIQUE_KEY", DateTime.Now.ToString("yyyyMMddHHmmssfff"))
            End If

            p(2) = SetText("@_CATEGORY_NAME", DT.Rows(i)("category_name"))
            p(3) = SetText("@_ACTIVE_STATUS", IIf(DT.Rows(i)("active_status") = "Y", "Y", "N"))

            ret = Execute_Command(sql, trans, p)
            If ret.ToLower <> "true" Then
                Exit For
            End If
        Next

        If ret.ToLower = "true" Then
            trans.Commit()

            txtSearch.Text = ""
            rdiSearchAll.Checked = True
            frmSettingQTCategory_Load(Nothing, Nothing)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            trans.Rollback()
            MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        'Try
        '    Dim CMD As New SqlCommandBuilder(DA)
        '    DA.Update(DT)
        '    DT.AcceptChanges()
        'Catch ex As Exception
        '    If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
        '        MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    Else
        '        MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    End If
        '    Exit Sub
        'End Try

        
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DR("id") = 0
        DR("active_status") = "Y"

        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_KeyUp(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        Filter()
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        If grdMain.DataSource Is Nothing Then Exit Sub
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= " ID = 0 OR  "
        End If
        Filter &= "CATEGORY_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' AND "

        If rdiSearchActive.Checked = True Then
            Filter += " active_status='Y' AND "
        End If
        If rdiSearchInactive.Checked = True Then
            Filter += " active_status='N' and "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter

        lblRec.Text = "Total   " & DT.DefaultView.Count & "   Record"
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click

        frmSettingQTCategory_Load(Nothing, Nothing)
    End Sub

End Class
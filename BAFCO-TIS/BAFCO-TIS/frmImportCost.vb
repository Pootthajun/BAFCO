﻿Imports System.Data.OleDb
Imports System.IO
Imports System.Data.SqlClient
Imports OfficeOpenXml
Imports System.Collections.Generic

Public Class frmImportCost

    Dim ConnStrExcel As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 8.0;HDR={1}'"
    Dim dt_Import As New DataTable
    Dim dt_route_from As New DataTable
    Dim dt_route_to As New DataTable
    Dim dt_sup As New DataTable
    Dim dt_veh As New DataTable
    Dim dt_cus As New DataTable

    Dim cb_W As String = "N"
    Dim cb_B As String = "N"
    Dim cb_R As String = "N"

    Private Sub frmImport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        Me.WindowState = FormWindowState.Maximized
        cbbPageSize.SelectedIndex = 0
        Grid.AutoGenerateColumns = False
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        'ofd.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        ofd.Filter = "Excel files (*.xlsx)|*.xlsx"
        ofd.ShowDialog()
    End Sub

    Function GetDataFromExcel() As DataTable
        Dim dt As New DataTable
        With dt
            .Columns.Add("IM_FROM")
            .Columns.Add("IM_TO")
            .Columns.Add("IM_VEH")
            .Columns.Add("IM_SUP")
            .Columns.Add("IM_COST")
            .Columns.Add("IM_COST_UNIT")
            .Columns.Add("IM_SUP_QDATE")
            .Columns.Add("IM_SUP_REMARK")
            .Columns.Add("IM_CUS")
            .Columns.Add("IM_VALUE")
            .Columns.Add("IM_VALUE_UNIT")
            .Columns.Add("IM_CUS_QDATE")
            .Columns.Add("IM_CUS_REMARK")
        End With

        Dim dr As DataRow
        Dim fle As New FileInfo(txtPath.Text)
        Using excel = New ExcelPackage(fle)
            Dim worksheet = excel.Workbook.Worksheets.First()
            For row As Integer = 2 To worksheet.Dimension.[End].Row
                Dim IM_FROM As String = worksheet.Cells(row, 1).Value
                Dim IM_TO As String = worksheet.Cells(row, 2).Value
                Dim IM_VEH As String = worksheet.Cells(row, 3).Value
                Dim IM_SUP As String = worksheet.Cells(row, 4).Value
                Dim IM_COST As String = worksheet.Cells(row, 5).Value
                Dim IM_COST_UNIT As String = worksheet.Cells(row, 6).Value
                Dim IM_SUP_QDATE As String = worksheet.Cells(row, 7).Text.Trim()
                Dim IM_SUP_REMARK As String = worksheet.Cells(row, 8).Value
                Dim IM_CUS As String = worksheet.Cells(row, 9).Value
                Dim IM_VALUE As String = worksheet.Cells(row, 10).Value
                Dim IM_VALUE_UNIT As String = worksheet.Cells(row, 11).Value
                Dim IM_CUS_QDATE As String = worksheet.Cells(row, 12).Text.Trim()
                Dim IM_CUS_REMARK As String = worksheet.Cells(row, 13).Value

                dr = dt.NewRow
                dr("IM_FROM") = IM_FROM
                dr("IM_TO") = IM_TO
                dr("IM_VEH") = IM_VEH
                dr("IM_SUP") = IM_SUP
                dr("IM_COST") = IM_COST
                dr("IM_COST_UNIT") = IM_COST_UNIT
                dr("IM_SUP_QDATE") = IM_SUP_QDATE
                dr("IM_SUP_REMARK") = IM_SUP_REMARK
                dr("IM_CUS") = IM_CUS
                dr("IM_VALUE") = IM_VALUE
                dr("IM_VALUE_UNIT") = IM_VALUE_UNIT
                dr("IM_CUS_QDATE") = IM_CUS_QDATE
                dr("IM_CUS_REMARK") = IM_CUS_REMARK
                dt.Rows.Add(dr)
            Next
            worksheet = Nothing
            excel.Dispose()
        End Using

        Return dt
    End Function

    Private Function CheckColumnName() As Boolean
        CheckColumnName = True
        Dim fle As New FileInfo(txtPath.Text)
        Using excel = New ExcelPackage(fle)
            Dim worksheet = excel.Workbook.Worksheets.First()
            Dim columnNames As New List(Of String)()
            For Each firstRowCell As OfficeOpenXml.ExcelRangeBase In worksheet.Cells(worksheet.Dimension.Start.Row, worksheet.Dimension.Start.Column, 1, worksheet.Dimension.[End].Column)
                columnNames.Add(firstRowCell.Text)
            Next

            For i As Integer = 0 To columnNames.Count - 1
                Dim columnname As String = columnNames(i)
                Select Case i
                    Case 0
                        If columnname <> "From" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 1
                        If columnname <> "To" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 2
                        If columnname <> "Vehicle" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 3
                        If columnname <> "Supplier" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 4
                        If columnname <> "Cost" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 5
                        If columnname <> "CostUnit" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 6
                        If columnname <> "SupplierQDate" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 7
                        If columnname <> "SupplierRemark" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 8
                        If columnname <> "Customer" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 9
                        If columnname <> "Value" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 10
                        If columnname <> "ValueUnit" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 11
                        If columnname <> "CustomerQDate" Then
                            CheckColumnName = False
                            Exit For
                        End If
                    Case 12
                        If columnname <> "CustomerRemark" Then
                            CheckColumnName = False
                            Exit For
                        End If
                End Select

            Next
            worksheet = Nothing
            excel.Dispose()
        End Using
        Return CheckColumnName
    End Function

    Private Sub ofd_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ofd.FileOk
        Try
            Dim Extension As String = ""
            If ofd.FileName.ToUpper.IndexOf(".XLSX") = 0 Then
                MessageBox.Show("Invalid File Type", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If

            txtPath.Text = ofd.FileName

            Dim IsCheckColumn As Boolean = CheckColumnName()
            If IsCheckColumn = False Then
                MessageBox.Show("รูปแบบไฟล์ที่ใช้ ไม่ตรงกับรูปแบบที่ระบบรองรับ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Grid.DataSource = ""
                txtPath.Text = ""
                Exit Sub
            End If

            Dim dt As New DataTable()
            dt = GetDataFromExcel()
            dt.DefaultView.RowFilter = "IM_FROM <> '' or IM_TO <> '' or IM_VEH <> ''"
            dt = dt.DefaultView.ToTable
            'connExcel.Close()

            dt_Import = New DataTable
            dt_Import.Columns.Add("IM_FROM")
            dt_Import.Columns.Add("IM_TO")
            dt_Import.Columns.Add("IM_VEH")
            dt_Import.Columns.Add("IM_SUP")
            dt_Import.Columns.Add("IM_COST")
            dt_Import.Columns.Add("IM_COST_UNIT")
            dt_Import.Columns.Add("IM_SUP_QDATE")
            dt_Import.Columns.Add("IM_SUP_REMARK")
            dt_Import.Columns.Add("IM_CUS")
            dt_Import.Columns.Add("IM_VALUE")
            dt_Import.Columns.Add("IM_VALUE_UNIT")
            dt_Import.Columns.Add("IM_CUS_QDATE")
            dt_Import.Columns.Add("IM_CUS_REMARK")

            For i As Int32 = 0 To dt.Rows.Count - 1
                Dim dr As DataRow
                dr = dt_Import.NewRow
                dr("IM_FROM") = dt.Rows(i).Item("IM_FROM").ToString
                dr("IM_TO") = dt.Rows(i).Item("IM_TO").ToString
                dr("IM_VEH") = dt.Rows(i).Item("IM_VEH").ToString
                dr("IM_SUP") = dt.Rows(i).Item("IM_SUP").ToString
                dr("IM_COST") = dt.Rows(i).Item("IM_COST").ToString
                dr("IM_COST_UNIT") = dt.Rows(i).Item("IM_COST_UNIT").ToString
                If IsDate(dt.Rows(i).Item("IM_SUP_QDATE")) Then
                    Dim strDate As String = dt.Rows(i).Item("IM_SUP_QDATE").ToString()
                    Dim strDD As String = ""
                    Dim strMM As String = ""
                    Dim strYY As String = ""
                    Dim str() As String = dt.Rows(i).Item("IM_SUP_QDATE").ToString().Split("/")
                    If str.Length = 3 Then
                        Try
                            strDD = str(0)
                            strMM = str(1)
                            strYY = str(2)
                            If CInt(str(2)) > 2500 Then
                                strYY = CInt(str(2)) - 543
                            End If
                        Catch ex As Exception
                        End Try
                        strDate = strDD & "/" & strMM & "/" & strYY
                    End If
                   
                    dr("IM_SUP_QDATE") = strDate
                Else
                    dr("IM_SUP_QDATE") = dt.Rows(i).Item("IM_SUP_QDATE").ToString
                End If

                dr("IM_SUP_REMARK") = dt.Rows(i).Item("IM_SUP_REMARK").ToString
                dr("IM_CUS") = dt.Rows(i).Item("IM_CUS").ToString
                dr("IM_VALUE") = dt.Rows(i).Item("IM_VALUE").ToString
                dr("IM_VALUE_UNIT") = dt.Rows(i).Item("IM_VALUE_UNIT").ToString

                If IsDate(dt.Rows(i).Item("IM_CUS_QDATE")) Then
                    Dim strDate As String = dt.Rows(i).Item("IM_CUS_QDATE").ToString()
                    Dim strDD As String = ""
                    Dim strMM As String = ""
                    Dim strYY As String = ""
                    Dim str() As String = dt.Rows(i).Item("IM_CUS_QDATE").ToString().Split("/")
                    If str.Length = 3 Then
                        Try
                            strDD = str(0)
                            strMM = str(1)
                            strYY = str(2)
                            If CInt(str(2)) > 2500 Then
                                strYY = CInt(str(2)) - 543
                            End If
                        Catch ex As Exception
                        End Try
                        strDate = strDD & "/" & strMM & "/" & strYY
                    End If

                    dr("IM_CUS_QDATE") = strDate
                Else
                    dr("IM_CUS_QDATE") = dt.Rows(i).Item("IM_CUS_QDATE").ToString
                End If
                dr("IM_CUS_REMARK") = dt.Rows(i).Item("IM_CUS_REMARK").ToString
                dt_Import.Rows.Add(dr)

            Next

            BindData()

            'Join Datatable เพื่อเช็คข้อมูล
            dt_Import = JoinDataTable(dt_Import, dt_route_from, "IM_FROM", "chk_From")
            dt_Import = JoinDataTable(dt_Import, dt_route_to, "IM_TO", "chk_To")
            dt_Import = JoinDataTable(dt_Import, dt_sup, "IM_SUP", "chk_Sup")
            dt_Import = JoinDataTable(dt_Import, dt_veh, "IM_VEH", "chk_Veh")
            dt_Import = JoinDataTable(dt_Import, dt_cus, "IM_CUS", "chk_Cus")
            'เพิ่ม Columns Status ของข้อมูล
            dt_Import.Columns.Add("Rec_No")
            dt_Import.Columns.Add("From_Status")
            dt_Import.Columns.Add("From_ToolTip")
            dt_Import.Columns.Add("To_Status")
            dt_Import.Columns.Add("To_ToolTip")
            dt_Import.Columns.Add("Sup_Status")
            dt_Import.Columns.Add("Sup_ToolTip")
            dt_Import.Columns.Add("Veh_Status")
            dt_Import.Columns.Add("Veh_ToolTip")
            dt_Import.Columns.Add("Cost_Status")
            dt_Import.Columns.Add("Cost_ToolTip")
            dt_Import.Columns.Add("Cost_Unit_Status")
            dt_Import.Columns.Add("Cost_Unit_ToolTip")
            dt_Import.Columns.Add("Sup_QDate_Status")
            dt_Import.Columns.Add("Sup_QDate_ToolTip")
            dt_Import.Columns.Add("Sup_Rem_Status")
            dt_Import.Columns.Add("Sup_Rem_ToolTip")
            dt_Import.Columns.Add("Cus_Status")
            dt_Import.Columns.Add("Cus_ToolTip")
            dt_Import.Columns.Add("Val_Status")
            dt_Import.Columns.Add("Val_ToolTip")
            dt_Import.Columns.Add("Val_Unit_Status")
            dt_Import.Columns.Add("Val_Unit_ToolTip")
            dt_Import.Columns.Add("Cus_QDate_Status")
            dt_Import.Columns.Add("Cus_QDate_ToolTip")
            dt_Import.Columns.Add("Cus_Rem_Status")
            dt_Import.Columns.Add("Cus_Rem_ToolTip")
            dt_Import.Columns.Add("IM_STATUS")
            'จัดข้อมูลให้อยู่ใน Page
            For i As Int32 = 0 To dt_Import.Rows.Count - 1
                dt_Import.Rows(i).Item("Rec_No") = i + 1
                UpdateStatusImportData(i)
            Next

            cb_W = "Y"
            cb_B = "Y"
            cb_R = "Y"
            cbW.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_w_c
            cbB.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_b_c
            cbR.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_r_c

            BindPageData()

            For i As Int32 = 0 To Grid.Rows.Count - 1
                BindGridColor(i)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Connection", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Grid.DataSource = ""
            txtPath.Text = ""
            Exit Sub
        End Try
    End Sub

    Public Sub UpdateFilterColorStatus(ByVal Row As Int32, ByVal Color As Int32)
        Select Case Color
            Case ColorStatus.Red
                If InStr(dt_Import.Rows(Row).Item("IM_STATUS").ToString, ColorStatus.Red) = 0 Then
                    'dt_Import.Rows(Row).Item("IM_STATUS") = dt_Import.Rows(Row).Item("IM_STATUS").ToString & CStr(ColorStatus.Red)
                    dt_Import.Rows(Row).Item("IM_STATUS") = CStr(ColorStatus.Red)
                End If
            Case ColorStatus.Blue
                If InStr(dt_Import.Rows(Row).Item("IM_STATUS").ToString, ColorStatus.Blue) = 0 Then
                    'dt_Import.Rows(Row).Item("IM_STATUS") = dt_Import.Rows(Row).Item("IM_STATUS").ToString & CStr(ColorStatus.Blue)
                    dt_Import.Rows(Row).Item("IM_STATUS") = CStr(ColorStatus.Blue)
                End If
            Case ColorStatus.White
                If InStr(dt_Import.Rows(Row).Item("IM_STATUS").ToString, ColorStatus.White) = 0 Then
                    'dt_Import.Rows(Row).Item("IM_STATUS") = dt_Import.Rows(Row).Item("IM_STATUS").ToString & CStr(ColorStatus.White)
                    dt_Import.Rows(Row).Item("IM_STATUS") = CStr(ColorStatus.White)
                End If
        End Select
    End Sub

    Private Sub BindData()
        'Bind ข้อมูล Master เข้า Datatable เพื่อเอาไว้เช็คข้อมูล
        dt_route_from = New DataTable
        dt_route_to = New DataTable
        dt_sup = New DataTable
        dt_veh = New DataTable
        dt_cus = New DataTable

        Dim sql As String = ""
        Dim da As New SqlDataAdapter
        sql = "select ROUTE_NAME as chk_From,ROUTE_ID as IM_FROM_ID from ROUTE"
        da = New SqlDataAdapter(sql, ConnStr)
        da.Fill(dt_route_from)

        sql = "select ROUTE_NAME as chk_To,ROUTE_ID as IM_TO_ID from ROUTE"
        da = New SqlDataAdapter(sql, ConnStr)
        da.Fill(dt_route_to)

        sql = "select SUP_NAME as chk_Sup,SUP_ID as IM_SUP_ID from SUPPLIER"
        da = New SqlDataAdapter(sql, ConnStr)
        da.Fill(dt_sup)

        sql = "select VEH_NAME as chk_Veh,VEH_ID as IM_VEH_ID from VEHICLE"
        da = New SqlDataAdapter(sql, ConnStr)
        da.Fill(dt_veh)

        sql = "select CUS_NAME as chk_Cus,CUS_ID as IM_CUS_ID from CUSTOMER"
        da = New SqlDataAdapter(sql, ConnStr)
        da.Fill(dt_cus)

    End Sub

    Private Sub UpdateStatusImportData(ByVal RowNo As Integer)
        '**************** From ****************
        If dt_Import.Rows(RowNo).Item("IM_FROM").ToString.Length > 100 Then
            dt_Import.Rows(RowNo).Item("From_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("From_ToolTip") = "จำนวนตัวอักษรมากกว่า 100 ตัวอักษร"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("IM_FROM").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("From_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("From_ToolTip") = "ไม่มีข้อมูล"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("chk_From").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("From_Status") = CStr(ColorStatus.Blue)
            dt_Import.Rows(RowNo).Item("From_ToolTip") = "ข้อมูลใหม่"
            UpdateFilterColorStatus(RowNo, ColorStatus.Blue)
        Else
            dt_Import.Rows(RowNo).Item("From_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("From_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If
        '**************** To ****************
        If dt_Import.Rows(RowNo).Item("IM_TO").ToString.Length > 100 Then
            dt_Import.Rows(RowNo).Item("To_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("To_ToolTip") = "จำนวนตัวอักษรมากกว่า 100 ตัวอักษร"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("IM_TO").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("To_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("To_ToolTip") = "ไม่มีข้อมูล"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("chk_To").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("To_Status") = CStr(ColorStatus.Blue)
            dt_Import.Rows(RowNo).Item("To_ToolTip") = "ข้อมูลใหม่"
            UpdateFilterColorStatus(RowNo, ColorStatus.Blue)
        Else
            dt_Import.Rows(RowNo).Item("To_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("To_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If
        '**************** Vehicle ****************
        If dt_Import.Rows(RowNo).Item("IM_VEH").ToString.Length > 300 Then
            dt_Import.Rows(RowNo).Item("Veh_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("Veh_ToolTip") = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("IM_VEH").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("Veh_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("Veh_ToolTip") = "ไม่มีข้อมูล"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("chk_Veh").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("Veh_Status") = CStr(ColorStatus.Blue)
            dt_Import.Rows(RowNo).Item("Veh_ToolTip") = "ข้อมูลใหม่"
            UpdateFilterColorStatus(RowNo, ColorStatus.Blue)
        Else
            dt_Import.Rows(RowNo).Item("Veh_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Veh_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If
        '**************** Supplier ****************
        If dt_Import.Rows(RowNo).Item("IM_SUP").ToString.Length > 300 Then
            dt_Import.Rows(RowNo).Item("Sup_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("Sup_ToolTip") = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("IM_SUP").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("Sup_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Sup_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'If dt_Import.Rows(RowNo).Item("IM_COST").ToString.Trim = "" And dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString.Trim = "" Then
            '    dt_Import.Rows(RowNo).Item("Sup_Status") = CStr(ColorStatus.White)
            '    dt_Import.Rows(RowNo).Item("Sup_ToolTip") = ""
            '    UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'Else
            '    dt_Import.Rows(RowNo).Item("Sup_Status") = CStr(ColorStatus.Red)
            '    dt_Import.Rows(RowNo).Item("Sup_ToolTip") = "ไม่มีข้อมูล"
            '    UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            'End If
        ElseIf dt_Import.Rows(RowNo).Item("chk_Sup").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("Sup_Status") = CStr(ColorStatus.Blue)
            dt_Import.Rows(RowNo).Item("Sup_ToolTip") = "ข้อมูลใหม่"
            UpdateFilterColorStatus(RowNo, ColorStatus.Blue)
        Else
            dt_Import.Rows(RowNo).Item("Sup_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Sup_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If
        '**************** Cost ****************
        If dt_Import.Rows(RowNo).Item("IM_COST").ToString.Trim <> "" Then
            If IsNumeric(dt_Import.Rows(RowNo).Item("IM_COST").ToString) Then
                dt_Import.Rows(RowNo).Item("Cost_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Cost_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            Else
                dt_Import.Rows(RowNo).Item("Cost_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Cost_ToolTip") = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            End If
        Else
            dt_Import.Rows(RowNo).Item("Cost_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Cost_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'If dt_Import.Rows(RowNo).Item("IM_SUP").ToString.Trim = "" And dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString.Trim = "" Then
            '    dt_Import.Rows(RowNo).Item("Cost_Status") = CStr(ColorStatus.White)
            '    dt_Import.Rows(RowNo).Item("Cost_ToolTip") = ""
            '    UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'Else
            '    dt_Import.Rows(RowNo).Item("Cost_Status") = CStr(ColorStatus.Red)
            '    dt_Import.Rows(RowNo).Item("Cost_ToolTip") = "ไม่มีข้อมูล"
            '    UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            'End If
        End If

        '**************** CostUnit ****************
        If dt_Import.Rows(RowNo).Item("IM_COST_UNIT").ToString.Length > 300 Then
            dt_Import.Rows(RowNo).Item("Cost_Unit_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("Cost_Unit_ToolTip") = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        Else
            dt_Import.Rows(RowNo).Item("Cost_Unit_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Cost_Unit_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If

        '**************** SupplierQDate ****************
        If dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString.Trim <> "" Then
            If dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString.Trim.ToUpper = "N/A" Then
                dt_Import.Rows(RowNo).Item("Sup_QDate_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Sup_QDate_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            ElseIf dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString.Trim.ToUpper <> "N/A" AndAlso ValidateDate(dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString) = False Then
                dt_Import.Rows(RowNo).Item("Sup_QDate_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Sup_QDate_ToolTip") = "ข้อมูลต้องเป็นวันที่เท่านั้น"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            Else
                dt_Import.Rows(RowNo).Item("Sup_QDate_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Sup_QDate_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            End If
        Else
            'dt_Import.Rows(RowNo).Item("Sup_QDate_Status") = CStr(ColorStatus.Red)
            'dt_Import.Rows(RowNo).Item("Sup_QDate_ToolTip") = "ข้อมูลต้องเป็นวันที่เท่านั้น"
            'UpdateFilterColorStatus(RowNo, ColorStatus.Red)

            If dt_Import.Rows(RowNo).Item("IM_SUP").ToString.Trim = "" And dt_Import.Rows(RowNo).Item("IM_COST").ToString.Trim = "" Then
                dt_Import.Rows(RowNo).Item("Sup_QDate_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Sup_QDate_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            Else
                dt_Import.Rows(RowNo).Item("Sup_QDate_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Sup_QDate_ToolTip") = "ไม่มีข้อมูล"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            End If
        End If
        '**************** SupplierRemark ****************
        If dt_Import.Rows(RowNo).Item("IM_SUP").ToString.Trim <> "" Then
            If dt_Import.Rows(RowNo).Item("IM_SUP_REMARK").ToString.Length > 500 Then
                dt_Import.Rows(RowNo).Item("Sup_Rem_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Sup_Rem_ToolTip") = "จำนวนตัวอักษรมากกว่า 500 ตัวอักษร"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            Else
                dt_Import.Rows(RowNo).Item("Sup_Rem_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Sup_Rem_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            End If
        Else
            dt_Import.Rows(RowNo).Item("Sup_Rem_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Sup_Rem_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If
        '**************** Customer ****************
        If dt_Import.Rows(RowNo).Item("IM_CUS").ToString.Length > 300 Then
            dt_Import.Rows(RowNo).Item("Cus_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("Cus_ToolTip") = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        ElseIf dt_Import.Rows(RowNo).Item("IM_CUS").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("Cus_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Cus_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'If dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Trim = "" And dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString.Trim = "" Then
            '    dt_Import.Rows(RowNo).Item("Cus_Status") = CStr(ColorStatus.White)
            '    dt_Import.Rows(RowNo).Item("Cus_ToolTip") = ""
            '    UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'Else
            '    dt_Import.Rows(RowNo).Item("Cus_Status") = CStr(ColorStatus.Red)
            '    dt_Import.Rows(RowNo).Item("Cus_ToolTip") = "ไม่มีข้อมูล"
            '    UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            'End If
        ElseIf dt_Import.Rows(RowNo).Item("chk_Cus").ToString.Trim = "" Then
            dt_Import.Rows(RowNo).Item("Cus_Status") = CStr(ColorStatus.Blue)
            dt_Import.Rows(RowNo).Item("Cus_ToolTip") = "ข้อมูลใหม่"
            UpdateFilterColorStatus(RowNo, ColorStatus.Blue)
        Else
            dt_Import.Rows(RowNo).Item("Cus_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Cus_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If
        '**************** Value ****************
        If dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Trim <> "" Then
            If IsNumeric(dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Trim) Then
                dt_Import.Rows(RowNo).Item("Val_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Val_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            Else
                dt_Import.Rows(RowNo).Item("Val_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Val_ToolTip") = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            End If
        Else
            dt_Import.Rows(RowNo).Item("Val_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Val_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'If dt_Import.Rows(RowNo).Item("IM_CUS").ToString.Trim = "" And dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString.Trim = "" Then
            '    dt_Import.Rows(RowNo).Item("Val_Status") = CStr(ColorStatus.White)
            '    dt_Import.Rows(RowNo).Item("Val_ToolTip") = ""
            '    UpdateFilterColorStatus(RowNo, ColorStatus.White)
            'Else
            '    dt_Import.Rows(RowNo).Item("Val_Status") = CStr(ColorStatus.Red)
            '    dt_Import.Rows(RowNo).Item("Val_ToolTip") = "ไม่มีข้อมูล"
            '    UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            'End If
        End If
        '**************** ValueUnit ****************
        If dt_Import.Rows(RowNo).Item("IM_VALUE_UNIT").ToString.Length > 300 Then
            dt_Import.Rows(RowNo).Item("Val_Unit_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("Val_Unit_ToolTip") = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        Else
            dt_Import.Rows(RowNo).Item("Val_Unit_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Val_Unit_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If

        '**************** CustomerQDate ****************
        If dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString.Trim <> "" Then
            If dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString.Trim.ToUpper = "N/A" Then
                dt_Import.Rows(RowNo).Item("Cus_QDate_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Cus_QDate_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            ElseIf dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString.Trim.ToUpper <> "N/A" AndAlso ValidateDate(dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString) = False Then
                dt_Import.Rows(RowNo).Item("Cus_QDate_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Cus_QDate_ToolTip") = "ข้อมูลต้องเป็นวันที่เท่านั้น"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            Else
                dt_Import.Rows(RowNo).Item("Cus_QDate_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Cus_QDate_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            End If
        ElseIf dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString.Trim = "" Then
            'dt_Import.Rows(RowNo).Item("Cus_QDate_Status") = CStr(ColorStatus.Red)
            'dt_Import.Rows(RowNo).Item("Cus_QDate_ToolTip") = "ข้อมูลต้องเป็นวันที่เท่านั้น"
            'UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            If dt_Import.Rows(RowNo).Item("IM_CUS").ToString.Trim = "" And dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Trim = "" Then
                dt_Import.Rows(RowNo).Item("Cus_QDate_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Cus_QDate_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            Else
                dt_Import.Rows(RowNo).Item("Cus_QDate_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Cus_QDate_ToolTip") = "ไม่มีข้อมูล"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            End If
        End If
        '**************** CustomerRemark ****************
        If dt_Import.Rows(RowNo).Item("IM_CUS").ToString.Trim <> "" Then
            If dt_Import.Rows(RowNo).Item("IM_CUS_REMARK").ToString.Length > 500 Then
                dt_Import.Rows(RowNo).Item("Cus_Rem_Status") = CStr(ColorStatus.Red)
                dt_Import.Rows(RowNo).Item("Cus_Rem_ToolTip") = "จำนวนตัวอักษรมากกว่า 500 ตัวอักษร"
                UpdateFilterColorStatus(RowNo, ColorStatus.Red)
            Else
                dt_Import.Rows(RowNo).Item("Cus_Rem_Status") = CStr(ColorStatus.White)
                dt_Import.Rows(RowNo).Item("Cus_Rem_ToolTip") = ""
                UpdateFilterColorStatus(RowNo, ColorStatus.White)
            End If
        ElseIf dt_Import.Rows(RowNo).Item("IM_CUS").ToString.Trim = "" And dt_Import.Rows(RowNo).Item("IM_CUS_REMARK").ToString <> "" Then
            dt_Import.Rows(RowNo).Item("Cus_Rem_Status") = CStr(ColorStatus.Red)
            dt_Import.Rows(RowNo).Item("Cus_Rem_ToolTip") = "ไม่มีข้อมูล Customer"
            UpdateFilterColorStatus(RowNo, ColorStatus.Red)
        Else
            dt_Import.Rows(RowNo).Item("Cus_Rem_Status") = CStr(ColorStatus.White)
            dt_Import.Rows(RowNo).Item("Cus_Rem_ToolTip") = ""
            UpdateFilterColorStatus(RowNo, ColorStatus.White)
        End If
    End Sub

    Private Sub UpdateStatusGridData(ByVal GridRowNo As Integer)
        If Grid.Rows(GridRowNo).Cells("IM_FROM").Value.ToString.Length > 100 Then
            Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 100 ตัวอักษร"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("IM_FROM").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = "ไม่มีข้อมูล"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("chk_From").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.Blue)
            Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = "ข้อมูลใหม่"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        Else
            Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If
        '**************** To ****************
        If Grid.Rows(GridRowNo).Cells("IM_TO").Value.ToString.Length > 100 Then
            Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 100 ตัวอักษร"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("IM_TO").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = "ไม่มีข้อมูล"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("chk_To").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.Blue)
            Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = "ข้อมูลใหม่"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        Else
            Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If
        '**************** Vehicle ****************
        If Grid.Rows(GridRowNo).Cells("IM_VEH").Value.ToString.Length > 300 Then
            Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("IM_VEH").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = "ไม่มีข้อมูล"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("chk_Veh").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.Blue)
            Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = "ข้อมูลใหม่"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        Else
            Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If
        '**************** Supplier ****************
        If Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Length > 300 Then
            Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        ElseIf Grid.Rows(GridRowNo).Cells("chk_Sup").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.Blue)
            Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = "ข้อมูลใหม่"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        Else
            Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If
        '**************** Cost ****************
        If Grid.Rows(GridRowNo).Cells("IM_COST").Value.ToString.Trim <> "" Then
            If IsNumeric(Grid.Rows(GridRowNo).Cells("IM_COST").Value.ToString) Then
                Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            Else
                Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            End If
        Else
            Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)            
        End If

        '**************** CostUnit ****************
        If Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").Value.ToString.Length > 300 Then
            Grid.Rows(GridRowNo).Cells("Cost_Unit_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("Cost_Unit_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        Else
            Grid.Rows(GridRowNo).Cells("Cost_Unit_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Cost_Unit_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If

        '**************** SupplierQDate ****************
        If Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString.Trim <> "" Then
            If Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString.Trim.ToUpper = "N/A" Then
                Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString.Trim.ToUpper <> "N/A" AndAlso ValidateDate(Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString) = False Then
                Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = "ข้อมูลต้องเป็นวันที่เท่านั้น"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            Else
                Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            End If
        Else
            If Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_COST").Value.ToString.Trim = "" Then
                Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            Else
                Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = "ไม่มีข้อมูล"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            End If
        End If
        '**************** SupplierRemark ****************
        If Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim <> "" Then
            If Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").Value.ToString.Length > 500 Then
                Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 500 ตัวอักษร"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            Else
                Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            End If
        Else
            Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If
        '**************** Customer ****************
        If Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Length > 300 Then
            Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        ElseIf Grid.Rows(GridRowNo).Cells("chk_Cus").Value.ToString.Trim = "" Then
            Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.Blue)
            Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = "ข้อมูลใหม่"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        Else
            Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If
        '**************** Value ****************
        If Grid.Rows(GridRowNo).Cells("IM_VALUE").Value.ToString.Trim <> "" Then
            If IsNumeric(Grid.Rows(GridRowNo).Cells("IM_VALUE").Value.ToString.Trim) Then
                Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            Else
                Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            End If
        Else
            Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            
        End If
        '**************** ValueUnit ****************
        If Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").Value.ToString.Length > 300 Then
            Grid.Rows(GridRowNo).Cells("Val_Unit_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("Val_Unit_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        Else
            Grid.Rows(GridRowNo).Cells("Val_Unit_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Val_Unit_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If

        '**************** CustomerQDate ****************
        If Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString.Trim <> "" Then
            If Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString.Trim.ToUpper = "N/A" Then
                Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString.Trim.ToUpper <> "N/A" AndAlso ValidateDate(Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString) = False Then
                Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = "ข้อมูลต้องเป็นวันที่เท่านั้น"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            Else
                Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            End If
        ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString.Trim = "" Then
            If Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_VALUE").Value.ToString.Trim = "" Then
                Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            Else
                Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = "ไม่มีข้อมูล"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            End If
        End If
        '**************** CustomerRemark ****************
        If Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim <> "" Then
            If Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Value.ToString.Length > 500 Then
                Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.Red)
                Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 500 ตัวอักษร"
                UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
            Else
                Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.White)
                Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = ""
                UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
            End If
        ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Value.ToString <> "" Then
            Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.Red)
            Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = "ไม่มีข้อมูล Customer"
            UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        Else
            Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.White)
            Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = ""
            UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        End If



        ''**************** From ****************
        'If Grid.Rows(GridRowNo).Cells("IM_FROM").Value.ToString.Length > 100 Then
        '    Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 100 ตัวอักษร"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_FROM").Value.ToString.Trim = "" Then
        '    Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = "ไม่มีข้อมูล"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("chk_From").Value.ToString.Trim = "" Then
        '    Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.Blue)
        '    Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = "ข้อมูลใหม่"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        'Else
        '    Grid.Rows(GridRowNo).Cells("From_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("From_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** To ****************
        'If Grid.Rows(GridRowNo).Cells("IM_TO").Value.ToString.Length > 100 Then
        '    Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 100 ตัวอักษร"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_TO").Value.ToString.Trim = "" Then
        '    Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = "ไม่มีข้อมูล"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("chk_To").Value.ToString.Trim = "" Then
        '    Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.Blue)
        '    Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = "ข้อมูลใหม่"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        'Else
        '    Grid.Rows(GridRowNo).Cells("To_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("To_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** Vehicle ****************
        'If Grid.Rows(GridRowNo).Cells("IM_VEH").Value.ToString.Length > 300 Then
        '    Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_VEH").Value.ToString.Trim = "" Then
        '    Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = "ไม่มีข้อมูล"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("chk_Veh").Value.ToString.Trim = "" Then
        '    Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.Blue)
        '    Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = "ข้อมูลใหม่"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Veh_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Veh_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** Supplier ****************
        'If Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Length > 300 Then
        '    Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    'ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim = "" Then
        '    '    Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.Red)
        '    '    Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = "ไม่มีข้อมูล"
        '    '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)

        'ElseIf (Grid.Rows(GridRowNo).Cells("chk_Sup").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString <> "") Then
        '    Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.Blue)
        '    Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = "ข้อมูลใหม่"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Sup_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** Cost ****************
        'If Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim <> "" Then
        '    If Not IsNumeric(Grid.Rows(GridRowNo).Cells("IM_COST").Value.ToString) Then
        '        Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.Red)
        '        Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    Else
        '        Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.White)
        '        Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = ""
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        '    End If
        '    'ElseIf Grid.Rows(GridRowNo).Cells("IM_COST").Value.ToString.Trim = "" Then
        '    '    Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.Red)
        '    '    Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = "ไม่มีข้อมูล"
        '    '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_COST").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = "ไม่มีข้อมูล Supplier"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Cost_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** CostUnit ****************
        'If Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").Value.ToString.Length > 300 Then
        '    Grid.Rows(GridRowNo).Cells("Cost_Unit_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Cost_Unit_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Cost_Unit_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Cost_Unit_ToolTip").Value = "ไม่มีข้อมูล Supplier"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Cost_Unit_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Cost_Unit_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If

        ''**************** SupplierQDate ****************
        'If Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim <> "" Then
        '    If Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString <> "N/A" AndAlso Not IsDate(Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString) Then
        '        Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.Red)
        '        Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = "ข้อมูลต้องเป็นวันที่เท่านั้น"
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    Else
        '        Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.White)
        '        Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = ""
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        '    End If
        '    'ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString.Trim = "" Then
        '    '    Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.Red)
        '    '    Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = "ไม่มีข้อมูล"
        '    '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = "ไม่มีข้อมูล Supplier"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** SupplierRemark ****************
        'If Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim <> "" Then
        '    If Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").Value.ToString.Length > 500 Then
        '        Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value = CStr(ColorStatus.Red)
        '        Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 500 ตัวอักษร"
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    Else
        '        Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value = CStr(ColorStatus.White)
        '        Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value = ""
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        '    End If
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_SUP").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value = "ไม่มีข้อมูล Supplier"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** Customer ****************
        'If Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Length > 300 Then
        '    Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    'ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" Then
        '    '    Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.Red)
        '    '    Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = "ไม่มีข้อมูล"
        '    '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf (Grid.Rows(GridRowNo).Cells("chk_Cus").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString <> "") Then
        '    Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.Blue)
        '    Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = "ข้อมูลใหม่"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Blue)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Cus_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** Value ****************
        'If Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim <> "" Then
        '    If Not IsNumeric(Grid.Rows(GridRowNo).Cells("IM_VALUE").Value.ToString) Then
        '        Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.Red)
        '        Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    Else
        '        Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.White)
        '        Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = ""
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        '    End If
        '    'ElseIf Grid.Rows(GridRowNo).Cells("IM_VALUE").Value.ToString.Trim = "" Then
        '    '    Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.Red)
        '    '    Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = "ไม่มีข้อมูล"
        '    '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_VALUE").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = "ไม่มีข้อมูล Customer"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Val_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** ValueUnit ****************
        'If Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").Value.ToString.Length > 300 Then
        '    Grid.Rows(GridRowNo).Cells("Val_Unit_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Val_Unit_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 300 ตัวอักษร"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Val_Unit_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Val_Unit_ToolTip").Value = "ไม่มีข้อมูล Customer"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Val_Unit_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Val_Unit_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If

        ''**************** CustomerQDate ****************
        'If Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim <> "" Then
        '    If Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString <> "N/A" AndAlso Not IsDate(Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString) Then
        '        Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.Red)
        '        Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = "ข้อมูลต้องเป็นวันที่เท่านั้น"
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    Else
        '        Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.White)
        '        Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = ""
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        '    End If
        '    'ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString.Trim = "" Then
        '    '    Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.Red)
        '    '    Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = "ไม่มีข้อมูล"
        '    '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = "ไม่มีข้อมูล Customer"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
        ''**************** CustomerRemark ****************
        'If Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim <> "" Then
        '    If Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Value.ToString.Length > 500 Then
        '        Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.Red)
        '        Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = "จำนวนตัวอักษรมากกว่า 500 ตัวอักษร"
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        '    Else
        '        Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.White)
        '        Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = ""
        '        UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        '    End If
        'ElseIf Grid.Rows(GridRowNo).Cells("IM_CUS").Value.ToString.Trim = "" And Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Value.ToString.Trim <> "" Then
        '    Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.Red)
        '    Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = "ไม่มีข้อมูล Customer"
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.Red)
        'Else
        '    Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value = CStr(ColorStatus.White)
        '    Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value = ""
        '    UpdateFilterColorStatus(GridRowNo, ColorStatus.White)
        'End If
    End Sub

    Private Sub BindGridColor(ByVal GridRowNo As Integer)
        '**************** From ****************
        Select Case Grid.Rows(GridRowNo).Cells("From_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_FROM").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_FROM").Style.ForeColor = Color.White
            Case ColorStatus.Blue
                Grid.Rows(GridRowNo).Cells("IM_FROM").Style.BackColor = Color.Navy
                Grid.Rows(GridRowNo).Cells("IM_FROM").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_FROM").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_FROM").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_FROM").ToolTipText = Grid.Rows(GridRowNo).Cells("From_ToolTip").Value.ToString
        '**************** To ****************
        Select Case Grid.Rows(GridRowNo).Cells("To_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_TO").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_TO").Style.ForeColor = Color.White
            Case ColorStatus.Blue
                Grid.Rows(GridRowNo).Cells("IM_TO").Style.BackColor = Color.Navy
                Grid.Rows(GridRowNo).Cells("IM_TO").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_TO").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_TO").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_TO").ToolTipText = Grid.Rows(GridRowNo).Cells("To_ToolTip").Value.ToString
        '**************** Vehicle ****************
        Select Case Grid.Rows(GridRowNo).Cells("Veh_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_VEH").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_VEH").Style.ForeColor = Color.White
            Case ColorStatus.Blue
                Grid.Rows(GridRowNo).Cells("IM_VEH").Style.BackColor = Color.Navy
                Grid.Rows(GridRowNo).Cells("IM_VEH").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_VEH").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_VEH").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_VEH").ToolTipText = Grid.Rows(GridRowNo).Cells("veh_ToolTip").Value.ToString
        '**************** Supplier ****************
        Select Case Grid.Rows(GridRowNo).Cells("Sup_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_SUP").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_SUP").Style.ForeColor = Color.White
            Case ColorStatus.Blue
                Grid.Rows(GridRowNo).Cells("IM_SUP").Style.BackColor = Color.Navy
                Grid.Rows(GridRowNo).Cells("IM_SUP").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_SUP").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_SUP").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_SUP").ToolTipText = Grid.Rows(GridRowNo).Cells("Sup_ToolTip").Value.ToString
        '**************** Cost ****************
        Select Case Grid.Rows(GridRowNo).Cells("Cost_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_COST").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_COST").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_COST").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_COST").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_COST").ToolTipText = Grid.Rows(GridRowNo).Cells("Cost_ToolTip").Value.ToString
        '**************** CostUnit ****************
        Select Case Grid.Rows(GridRowNo).Cells("Cost_Unit_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_COST_UNIT").ToolTipText = Grid.Rows(GridRowNo).Cells("Cost_Unit_ToolTip").Value.ToString
        '**************** SupplierQDate ****************
        Select Case Grid.Rows(GridRowNo).Cells("Sup_QDate_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_SUP_QDATE").ToolTipText = Grid.Rows(GridRowNo).Cells("Sup_QDate_ToolTip").Value.ToString
        '**************** SupplierRemark ****************
        Select Case Grid.Rows(GridRowNo).Cells("Sup_Rem_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_SUP_REMARK").ToolTipText = Grid.Rows(GridRowNo).Cells("Sup_Rem_ToolTip").Value.ToString
        '**************** Customer ****************
        Select Case Grid.Rows(GridRowNo).Cells("Cus_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_CUS").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_CUS").Style.ForeColor = Color.White
            Case ColorStatus.Blue
                Grid.Rows(GridRowNo).Cells("IM_CUS").Style.BackColor = Color.Navy
                Grid.Rows(GridRowNo).Cells("IM_CUS").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_CUS").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_CUS").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_CUS").ToolTipText = Grid.Rows(GridRowNo).Cells("Cus_ToolTip").Value.ToString
        '**************** Value ****************
        Select Case Grid.Rows(GridRowNo).Cells("Val_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_VALUE").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_VALUE").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_VALUE").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_VALUE").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_VALUE").ToolTipText = Grid.Rows(GridRowNo).Cells("Val_ToolTip").Value.ToString
        '**************** ValeUnit ****************
        Select Case Grid.Rows(GridRowNo).Cells("Val_Unit_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_VALUE_UNIT").ToolTipText = Grid.Rows(GridRowNo).Cells("Val_Unit_ToolTip").Value.ToString
        '**************** CustomerQDate ****************
        Select Case Grid.Rows(GridRowNo).Cells("Cus_QDate_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_CUS_QDATE").ToolTipText = Grid.Rows(GridRowNo).Cells("Cus_QDate_ToolTip").Value.ToString
        '**************** SupplierRemark ****************
        Select Case Grid.Rows(GridRowNo).Cells("Cus_Rem_Status").Value
            Case ColorStatus.Red
                Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Style.BackColor = Color.Red
                Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Style.ForeColor = Color.White
            Case ColorStatus.White
                Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Style.BackColor = Color.White
                Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").Style.ForeColor = Color.Black
        End Select
        Grid.Rows(GridRowNo).Cells("IM_CUS_REMARK").ToolTipText = Grid.Rows(GridRowNo).Cells("Cus_Rem_ToolTip").Value.ToString
    End Sub

    Private Sub Grid_CellEndEdit(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid.CellEndEdit
        Dim Value As Object = Grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
        Try
            Dim rows As Int32 = 0
            rows = (Val(cbbPageSize.Text) * Val(txtPage.Text)) - Val(cbbPageSize.Text)

            'Dim cell As DataGridViewCell
            'For Each cell In Grid.SelectedCells
            '    cell.Value = Value
            Select Case dt_Import.Columns(e.ColumnIndex).ColumnName
                Case "IM_FROM"
                    dt_route_from.DefaultView.RowFilter = "chk_From = '" & Value & "'"
                    If dt_route_from.DefaultView.Count = 0 Then
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_From") = ""
                        Grid.Rows(e.RowIndex).Cells("chk_From").Value = ""
                    Else
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_From") = Value
                        Grid.Rows(e.RowIndex).Cells("chk_From").Value = Value
                        Dim dt_temp As New DataTable
                        dt_temp = dt_route_from.DefaultView.ToTable
                        dt_Import.Rows(e.RowIndex + rows).Item("IM_FROM_ID") = dt_temp.Rows(0).Item("IM_FROM_ID")
                    End If
                    dt_route_from.DefaultView.RowFilter = ""
                Case "IM_TO"
                    dt_route_to.DefaultView.RowFilter = "chk_To = '" & Value & "'"
                    If dt_route_to.DefaultView.Count = 0 Then
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_To") = ""
                        Grid.Rows(e.RowIndex).Cells("chk_To").Value = ""
                    Else
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_To") = Value
                        Grid.Rows(e.RowIndex).Cells("chk_To").Value = Value
                        Dim dt_temp As New DataTable
                        dt_temp = dt_route_to.DefaultView.ToTable
                        dt_Import.Rows(e.RowIndex + rows).Item("IM_TO_ID") = dt_temp.Rows(0).Item("IM_TO_ID")
                    End If
                    dt_route_to.DefaultView.RowFilter = ""
                Case "IM_SUP"
                    dt_sup.DefaultView.RowFilter = "chk_Sup = '" & Value & "'"
                    If dt_sup.DefaultView.Count = 0 Then
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_Sup") = ""
                        Grid.Rows(e.RowIndex).Cells("chk_Sup").Value = ""
                    Else
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_Sup") = Value
                        Grid.Rows(e.RowIndex).Cells("chk_Sup").Value = Value
                        Dim dt_temp As New DataTable
                        dt_temp = dt_sup.DefaultView.ToTable
                        dt_Import.Rows(e.RowIndex + rows).Item("IM_SUP_ID") = dt_temp.Rows(0).Item("IM_SUP_ID")
                    End If
                    dt_sup.DefaultView.RowFilter = ""
                Case "IM_VEH"
                    dt_veh.DefaultView.RowFilter = "chk_Veh = '" & Value & "'"
                    If dt_veh.DefaultView.Count = 0 Then
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_Veh") = ""
                        Grid.Rows(e.RowIndex).Cells("chk_Veh").Value = ""
                    Else
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_Veh") = Value
                        Grid.Rows(e.RowIndex).Cells("chk_Veh").Value = Value
                        Dim dt_temp As New DataTable
                        dt_temp = dt_veh.DefaultView.ToTable
                        dt_Import.Rows(e.RowIndex + rows).Item("IM_VEH_ID") = dt_temp.Rows(0).Item("IM_VEH_ID")
                    End If
                    dt_veh.DefaultView.RowFilter = ""
                Case "IM_CUS"
                    dt_cus.DefaultView.RowFilter = "chk_Cus = '" & Value & "'"
                    If dt_cus.DefaultView.Count = 0 Then
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_Cus") = ""
                        Grid.Rows(e.RowIndex).Cells("chk_Cus").Value = ""
                    Else
                        dt_Import.Rows(e.RowIndex + rows).Item("chk_Cus") = Value
                        Grid.Rows(e.RowIndex).Cells("chk_Cus").Value = Value
                        Dim dt_temp As New DataTable
                        dt_temp = dt_cus.DefaultView.ToTable
                        dt_Import.Rows(e.RowIndex + rows).Item("IM_CUS_ID") = dt_temp.Rows(0).Item("IM_CUS_ID")
                    End If
                    dt_cus.DefaultView.RowFilter = ""
               
            End Select

            dt_Import.Rows(e.RowIndex + rows).Item(e.ColumnIndex) = Value
            UpdateStatusImportData(e.RowIndex + rows)
            UpdateStatusGridData(e.RowIndex)
            BindGridColor(e.RowIndex)
            'Next
        Catch ex As Exception : End Try
    End Sub

    Private Sub Grid_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid.DataError
        MsgBox(e.Exception.Message)
    End Sub

    Private Sub FilterData()
       
        BindPageData()
        For i As Int32 = 0 To Grid.Rows.Count - 1
            BindGridColor(i)
        Next
    End Sub

    Private Sub cbW_Click(sender As System.Object, e As System.EventArgs) Handles cbW.Click
        If cb_W = "N" Then Exit Sub
        If cb_W = "F" Then
            cb_W = "Y"
            cbW.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_w_c
        Else
            cb_W = "F"
            cbW.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_w
        End If
        FilterData()
    End Sub

    Private Sub cbB_Click(sender As System.Object, e As System.EventArgs) Handles cbB.Click
        If cb_B = "N" Then Exit Sub
        If cb_B = "F" Then
            cb_B = "Y"
            cbB.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_b_c
        Else
            cb_B = "F"
            cbB.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_b
        End If
        FilterData()
    End Sub

    Private Sub cbR_Click(sender As System.Object, e As System.EventArgs) Handles cbR.Click
        If cb_R = "N" Then Exit Sub
        If cb_R = "F" Then
            cb_R = "Y"
            cbR.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_r_c
        Else
            cb_R = "F"
            cbR.Image = Global.BAFCO_TIS.My.Resources.Resources.cb_r
        End If
        FilterData()
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        If IsNothing(Grid.DataSource) Then Exit Sub
        Dim Rec_No As Integer = 1
        If dt_Import.Rows.Count > 0 Then
            Rec_No = Val(dt_Import.Rows(dt_Import.Rows.Count - 1).Item("Rec_No")) + 1
        End If
        Dim dr As DataRow
        dr = dt_Import.NewRow
        dr("Rec_No") = Rec_No
        dr("From_Status") = CStr(ColorStatus.Red)
        dr("From_ToolTip") = "ไม่มีข้อมูล"
        dr("To_Status") = CStr(ColorStatus.Red)
        dr("To_ToolTip") = "ไม่มีข้อมูล"
        dr("Sup_Status") = CStr(ColorStatus.White)
        dr("Sup_ToolTip") = ""
        dr("Veh_Status") = CStr(ColorStatus.Red)
        dr("Veh_ToolTip") = "ไม่มีข้อมูล"
        dr("Cost_Status") = CStr(ColorStatus.Red)
        dr("Cost_ToolTip") = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
        dr("Cost_Unit_Status") = CStr(ColorStatus.White)
        dr("Cost_Unit_ToolTip") = ""
        dr("Sup_QDate_Status") = CStr(ColorStatus.White)
        dr("Sup_QDate_ToolTip") = ""
        dr("Sup_Rem_Status") = CStr(ColorStatus.White)
        dr("Sup_Rem_ToolTip") = ""
        dr("Cus_Status") = CStr(ColorStatus.White)
        dr("Cus_ToolTip") = ""
        dr("Val_Status") = CStr(ColorStatus.Red)
        dr("Val_ToolTip") = "ข้อมูลต้องเป็นตัวเลขเท่านั้น"
        dr("Val_Unit_Status") = CStr(ColorStatus.White)
        dr("Val_Unit_ToolTip") = ""
        dr("Cus_QDate_Status") = CStr(ColorStatus.White)
        dr("Cus_QDate_ToolTip") = ""
        dr("Cus_Rem_Status") = CStr(ColorStatus.White)
        dr("Cus_Rem_ToolTip") = ""
        dr("IM_STATUS") = CStr(ColorStatus.Red)
        dt_Import.Rows.Add(dr)

        BindPageData()
        If Val(txtPage.Text) <> Val(lblTotalPage.Text) Then
            txtPage.Text = lblTotalPage.Text
        End If
        BindPageData()
        For i As Int32 = 0 To Grid.Rows.Count - 1
            BindGridColor(i)
        Next
    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click
        If Grid.SelectedRows.Count = 0 Then Exit Sub
        If MsgBox("Are you sure to delete selected rows?", MsgBoxStyle.YesNo) <> MsgBoxResult.Yes Then Exit Sub
        Dim RowSelect As String = ""
        For Each row As DataGridViewRow In Grid.SelectedRows
            'Grid.Rows.Remove(row)
            RowSelect &= row.Cells("Rec_No").Value.ToString & ","
        Next
        RowSelect = RowSelect.ToString.Substring(0, RowSelect.Length - 1)
        dt_Import.DefaultView.RowFilter = "Rec_No not in (" & RowSelect & ")"
        dt_Import = dt_Import.DefaultView.ToTable
        BindPageData()
        For i As Int32 = 0 To Grid.Rows.Count - 1
            BindGridColor(i)
        Next
    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click
        If Grid.SelectedCells.Count = 0 Then Exit Sub
        Dim Value As Object = InputBox("Please inser value to fill-up", "")
        If IsNothing(Value) Or Value = "" Then Exit Sub
        Try
            Dim rows As Int32 = 0
            rows = (Val(cbbPageSize.Text) * Val(txtPage.Text)) - Val(cbbPageSize.Text)

            Dim cell As DataGridViewCell
            For Each cell In Grid.SelectedCells
                cell.Value = Value
                Select Case dt_Import.Columns(cell.ColumnIndex).ColumnName
                    Case "IM_FROM"
                        dt_route_from.DefaultView.RowFilter = "chk_From = '" & Value & "'"
                        If dt_route_from.DefaultView.Count = 0 Then
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_From") = ""
                            Grid.Rows(cell.RowIndex).Cells("chk_From").Value = ""
                        Else
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_From") = Value
                            Grid.Rows(cell.RowIndex).Cells("chk_From").Value = Value
                        End If
                        dt_route_from.DefaultView.RowFilter = ""
                    Case "IM_TO"
                        dt_route_to.DefaultView.RowFilter = "chk_To = '" & Value & "'"
                        If dt_route_to.DefaultView.Count = 0 Then
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_To") = ""
                            Grid.Rows(cell.RowIndex).Cells("chk_To").Value = ""
                        Else
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_To") = Value
                            Grid.Rows(cell.RowIndex).Cells("chk_To").Value = Value
                        End If
                        dt_route_to.DefaultView.RowFilter = ""
                    Case "IM_SUP"
                        dt_sup.DefaultView.RowFilter = "chk_Sup = '" & Value & "'"
                        If dt_sup.DefaultView.Count = 0 Then
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_Sup") = ""
                            Grid.Rows(cell.RowIndex).Cells("chk_Sup").Value = ""
                        Else
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_Sup") = Value
                            Grid.Rows(cell.RowIndex).Cells("chk_Sup").Value = Value
                        End If
                        dt_sup.DefaultView.RowFilter = ""
                    Case "IM_VEH"
                        dt_veh.DefaultView.RowFilter = "chk_Veh = '" & Value & "'"
                        If dt_veh.DefaultView.Count = 0 Then
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_Veh") = ""
                            Grid.Rows(cell.RowIndex).Cells("chk_Veh").Value = ""
                        Else
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_Veh") = Value
                            Grid.Rows(cell.RowIndex).Cells("chk_Veh").Value = Value
                        End If
                        dt_veh.DefaultView.RowFilter = ""
                    Case "IM_CUS"
                        dt_cus.DefaultView.RowFilter = "chk_Cus = '" & Value & "'"
                        If dt_cus.DefaultView.Count = 0 Then
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_Cus") = ""
                            Grid.Rows(cell.RowIndex).Cells("chk_Cus").Value = ""
                        Else
                            dt_Import.Rows(cell.RowIndex + rows).Item("chk_Cus") = Value
                            Grid.Rows(cell.RowIndex).Cells("chk_Cus").Value = Value
                        End If
                        dt_cus.DefaultView.RowFilter = ""
                    
                End Select

                dt_Import.Rows(cell.RowIndex + rows).Item(cell.ColumnIndex) = Value
                UpdateStatusImportData(cell.RowIndex + rows)
                UpdateStatusGridData(cell.RowIndex)
                BindGridColor(cell.RowIndex)
            Next
        Catch ex As Exception : End Try
    End Sub

    Private Sub setStatusPageControlButton()

        If Val(txtPage.Text) < Val(lblTotalPage.Text) Then
            btnNext.Enabled = True
            btnLast.Enabled = True
            txtPage.Enabled = True
            lbl.Enabled = True
            lblTotalPage.Enabled = True
        Else
            btnNext.Enabled = False
            btnLast.Enabled = False
            txtPage.Enabled = False
        End If

        If Val(txtPage.Text) <= 1 Then
            btnBack.Enabled = False
            btnFirst.Enabled = False
        Else
            btnBack.Enabled = True
            btnFirst.Enabled = True
        End If

    End Sub

    Public Sub BindPageData()
       
        '**************** Page ******************
        Dim Filter As String = ""
        If cb_W <> "N" And cb_W <> "N" And cb_W <> "N" Then
            If cb_W = "Y" Then
                Filter &= "IM_STATUS = '" & ColorStatus.White & "' OR "
                Filter &= "From_Status = '" & ColorStatus.White & "' OR "
                Filter &= "To_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Sup_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Veh_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Cost_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Cost_Unit_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Sup_QDate_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Sup_Rem_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Cus_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Val_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Val_Unit_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Cus_QDate_Status= '" & ColorStatus.White & "' OR "
                Filter &= "Cus_Rem_Status= '" & ColorStatus.White & "' OR "
            End If
            If cb_B = "Y" Then
                Filter &= "IM_STATUS = '" & ColorStatus.Blue & "' OR "
                Filter &= "From_Status = '" & ColorStatus.Blue & "' OR "
                Filter &= "To_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Sup_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Veh_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Cost_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Cost_Unit_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Sup_QDate_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Sup_Rem_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Cus_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Val_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Val_Unit_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Cus_QDate_Status= '" & ColorStatus.Blue & "' OR "
                Filter &= "Cus_Rem_Status= '" & ColorStatus.Blue & "' OR "
            End If
            If cb_R = "Y" Then
                Filter &= "IM_STATUS = '" & ColorStatus.Red & "' OR "
                Filter &= "From_Status = '" & ColorStatus.Red & "' OR "
                Filter &= "To_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Sup_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Veh_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Cost_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Cost_Unit_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Sup_QDate_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Sup_Rem_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Cus_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Val_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Val_Unit_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Cus_QDate_Status= '" & ColorStatus.Red & "' OR "
                Filter &= "Cus_Rem_Status= '" & ColorStatus.Red & "' OR "
            End If
            If Filter <> "" Then
                Filter = Filter.Substring(0, Filter.Length - 4)
            Else
                Filter = "1=0"
            End If
            If dt_Import.Rows.Count > 0 Then
                dt_Import.DefaultView.RowFilter = Filter
            End If
        End If

        Dim dt As New DataTable
        dt = dt_Import.DefaultView.ToTable
        dt_Import.DefaultView.RowFilter = ""

        Dim CurrentPage As Integer = Val(txtPage.Text)
        Dim TotalRecord As Integer = dt.Rows.Count
        Dim TotalPage As Integer = Math.Ceiling(dt.Rows.Count / CInt(cbbPageSize.Text))

        lblTotalPage.Text = TotalPage
        If TotalPage = 0 Then
            CurrentPage = 0
        Else
            If CurrentPage > TotalPage Then
                CurrentPage = TotalPage
            Else
                If CurrentPage = 0 Then CurrentPage = 1
            End If
        End If

        txtPage.Text = CurrentPage
        Dim CurrentDisplay As String = ""
        Dim StartIndex As Integer = 0
        Dim EndIndex As Integer = 0

        If TotalPage = 0 Then
            StartIndex = -1
            EndIndex = -1
        Else
            StartIndex = ((CurrentPage - 1) * CInt(cbbPageSize.Text))
            If CurrentPage = TotalPage Then
                EndIndex &= TotalRecord - 1
            Else
                EndIndex &= CurrentPage * CInt(cbbPageSize.Text) - 1
            End If
        End If
        Dim dt_Page As New DataTable
        dt_Page = dt.Clone
        If StartIndex = -1 And EndIndex = -1 Then
            Grid.DataSource = dt_Page
            setStatusPageControlButton()
            Exit Sub
        End If

        For i As Integer = StartIndex To EndIndex
            Dim DR As DataRow = dt_Page.NewRow
            Try
                DR.ItemArray = dt.DefaultView(i).Row.ItemArray
            Catch ex As Exception
            End Try
            dt_Page.Rows.Add(DR)
        Next
        Grid.DataSource = ""
        Grid.DataSource = dt_Page
        setStatusPageControlButton()
        lblRec.Text = "Total   " & dt_Import.Rows.Count & "   Record"
    End Sub

    Private Sub btnFirst_Click(sender As System.Object, e As System.EventArgs) Handles btnFirst.Click
        If txtPage.Text <> "1" Then
            txtPage.Text = "1"
            BindPageData()
            For i As Int32 = 0 To Grid.Rows.Count - 1
                BindGridColor(i)
            Next
        End If
    End Sub

    Private Sub btnBack_Click(sender As System.Object, e As System.EventArgs) Handles btnBack.Click
        txtPage.Text = Val(txtPage.Text) - 1
        BindPageData()
        For i As Int32 = 0 To Grid.Rows.Count - 1
            BindGridColor(i)
        Next
    End Sub

    Private Sub btnNext_Click(sender As System.Object, e As System.EventArgs) Handles btnNext.Click
        txtPage.Text = Val(txtPage.Text) + 1
        BindPageData()
        For i As Int32 = 0 To Grid.Rows.Count - 1
            BindGridColor(i)
        Next
    End Sub

    Private Sub btnLast_Click(sender As System.Object, e As System.EventArgs) Handles btnLast.Click
        If txtPage.Text <> Val(lblTotalPage.Text) Then
            txtPage.Text = Val(lblTotalPage.Text)
            BindPageData()
            For i As Int32 = 0 To Grid.Rows.Count - 1
                BindGridColor(i)
            Next
        End If
    End Sub

    Private Sub cbbPageSize_SelectionChangeCommitted(sender As Object, e As System.EventArgs) Handles cbbPageSize.SelectionChangeCommitted
        If Grid.Rows.Count > 0 Then
            BindPageData()
            For i As Int32 = 0 To Grid.Rows.Count - 1
                BindGridColor(i)
            Next
        End If
    End Sub

    Private Sub txtPage_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPage.KeyPress
        If (e.KeyChar < "0" Or e.KeyChar > "9" Or Asc(e.KeyChar) = 13) And Asc(e.KeyChar) <> 8 Then
            e.Handled = True
            If Asc(e.KeyChar) = 13 Then
                Dim TotalPage As Integer = Val(lblTotalPage.Text)
                If Val(lblTotalPage.Text) > TotalPage Then
                    txtPage.Text = TotalPage
                ElseIf Val(txtPage.Text) <= 0 Then
                    If TotalPage = 0 Then
                        txtPage.Text = ""
                    Else
                        txtPage.Text = 1
                    End If
                End If
                BindPageData()
                For i As Int32 = 0 To Grid.Rows.Count - 1
                    BindGridColor(i)
                Next
                txtPage.Focus()
                txtPage.SelectAll()
            End If
        End If
    End Sub

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        If Grid.SelectedCells.Count = 0 Then Exit Sub
        If MsgBox("Are you sure to clear selected value fill-up?", MsgBoxStyle.YesNo) <> MsgBoxResult.Yes Then Exit Sub
        Dim cell As DataGridViewCell
        Dim rows As Int32 = 0
        rows = (Val(cbbPageSize.Text) * Val(txtPage.Text)) - Val(cbbPageSize.Text)
        For Each cell In Grid.SelectedCells
            Select Case GetSchemaDataType(cell.ValueType.ToString)
                Case "Integer", "Money", "Float", "DateTime", "Yes/No"
                    cell.Value = DBNull.Value
                    dt_Import.Rows(cell.RowIndex + rows).Item(cell.ColumnIndex) = DBNull.Value
                Case Else
                    cell.Value = ""
                    dt_Import.Rows(cell.RowIndex + rows).Item(cell.ColumnIndex) = "0"
            End Select

            UpdateStatusImportData(cell.RowIndex + rows)
            UpdateStatusGridData(cell.RowIndex)
            BindGridColor(cell.RowIndex)
        Next
    End Sub

    Public Function GetSchemaDataType(ByVal DataType As String) As String
        Select Case DataType
            Case "System.Int32"
                Return "Integer"
            Case "System.Double", "System.Decimal"
                Return "Float"
            Case GetType(DateTime).ToString
                Return "DateTime"
            Case GetType(Boolean).ToString
                Return "Yes/No"
            Case Else ' String
                Return "Text"
        End Select
    End Function

    Private Sub btnImport_Click(sender As System.Object, e As System.EventArgs) Handles btnImport.Click
        If Grid.SelectedCells.Count = 0 Then Exit Sub

        Try
            Dim ret As String = Validation()
            If ret <> "" Then
                MsgBox(ret, MsgBoxStyle.OkOnly)
                pnlProcess.Visible = False
                Exit Sub
            End If
            pnlProcess.Visible = True
            lblProcess.Text = "0/" & dt_Import.Rows.Count.ToString
            pgb.Value = 0
            pgb.Minimum = 0
            pgb.Maximum = dt_Import.Rows.Count


            '== Save ==
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            If conn.State <> ConnectionState.Open Then
                MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                Exit Sub
            End If
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction

            Dim cmd As New SqlCommand
            cmd.Connection = trans.Connection
            cmd.Transaction = trans
            cmd.CommandType = CommandType.Text
            For i As Int32 = 0 To dt_Import.Rows.Count - 1
                Application.DoEvents()
                Try
                    InsertTransectionData(i, cmd)
                    lblProcess.Text = CStr(i + 1) & "/" & dt_Import.Rows.Count.ToString
                    pgb.Value = i + 1
                Catch ex As Exception
                    MsgBox("ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " ไม่สามารถนำเข้าข้อมูลได้", MsgBoxStyle.OkOnly)
                    pnlProcess.Visible = False
                    trans.Rollback()
                    Exit Sub
                End Try
            Next

            trans.Commit()
            conn.Close()
            '== End Save


            pnlProcess.Visible = False
            MsgBox("Import Complete.", MsgBoxStyle.OkOnly)

            Me.Close()
            frmMain.CloseAllChildForm()
            Dim f As New frmImportCost
            f.MdiParent = frmMain
            f.Show()
        Catch ex As Exception
            MsgBox("ไม่สามารถนำเข้าข้อมูลได้" & ex.ToString(), MsgBoxStyle.OkOnly)
        End Try
    End Sub

    'Sub InsertMasterData(ByVal RowNo As Int32)
    '    '**************** From ****************
    '    If dt_Import.Rows(RowNo).Item("chk_From").ToString.Trim = "" Then
    '        Dim Parameter As String = dt_Import.Rows(RowNo).Item("IM_FROM").ToString
    '        Dim SQL As String = "SELECT * FROM ROUTE WHERE ROUTE_NAME='" & Parameter.Replace("'", "''") & "'"
    '        Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '        Dim DT As New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            Dim DR As DataRow = DT.NewRow
    '            DR("ROUTE_ID") = GetNewID("ROUTE", "ROUTE_ID")
    '            DR("ROUTE_NAME") = Parameter
    '            DR("UPDATE_BY") = myUser.user_id
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            Dim cmd As New SqlCommandBuilder(DA)
    '            DA.Update(DT)

    '            For i As Int32 = 0 To dt_Import.Rows.Count - 1
    '                If dt_Import.Rows(i).Item("IM_FROM").ToString = Parameter Then
    '                    dt_Import.Rows(i).Item("chk_From") = Parameter
    '                End If
    '                If dt_Import.Rows(i).Item("IM_TO").ToString = Parameter Then
    '                    dt_Import.Rows(i).Item("chk_To") = Parameter
    '                End If
    '            Next
    '        End If
    '    End If
    '    '**************** To ****************
    '    If dt_Import.Rows(RowNo).Item("chk_To").ToString.Trim = "" Then
    '        Dim Parameter As String = dt_Import.Rows(RowNo).Item("IM_TO").ToString
    '        Dim SQL As String = "SELECT * FROM ROUTE WHERE ROUTE_NAME='" & Parameter.Replace("'", "''") & "'"
    '        Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '        Dim DT As New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            Dim DR As DataRow = DT.NewRow
    '            DR("ROUTE_ID") = GetNewID("ROUTE", "ROUTE_ID")
    '            DR("ROUTE_NAME") = Parameter
    '            DR("UPDATE_BY") = myUser.user_id
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            Dim cmd As New SqlCommandBuilder(DA)
    '            DA.Update(DT)

    '            For i As Int32 = 0 To dt_Import.Rows.Count - 1
    '                If dt_Import.Rows(i).Item("IM_FROM").ToString = Parameter Then
    '                    dt_Import.Rows(i).Item("chk_From") = Parameter
    '                End If
    '                If dt_Import.Rows(i).Item("IM_TO").ToString = Parameter Then
    '                    dt_Import.Rows(i).Item("chk_To") = Parameter
    '                End If
    '            Next
    '        End If
    '    End If
    '    '**************** Supplier ****************
    '    If dt_Import.Rows(RowNo).Item("chk_Sup").ToString.Trim = "" Then
    '        Dim Parameter As String = dt_Import.Rows(RowNo).Item("IM_SUP").ToString
    '        Dim SQL As String = "SELECT * FROM SUPPLIER WHERE SUP_NAME='" & Parameter.Replace("'", "''") & "'"
    '        Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '        Dim DT As New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            Dim DR As DataRow = DT.NewRow
    '            DR("SUP_ID") = GetNewID("SUPPLIER", "SUP_ID")
    '            DR("SUP_NAME") = Parameter
    '            DR("UPDATE_BY") = myUser.user_id
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            Dim cmd As New SqlCommandBuilder(DA)
    '            DA.Update(DT)

    '            For i As Int32 = 0 To dt_Import.Rows.Count - 1
    '                If dt_Import.Rows(i).Item("IM_SUP").ToString = Parameter Then
    '                    dt_Import.Rows(i).Item("chk_Sup") = Parameter
    '                End If
    '            Next
    '        End If
    '    End If
    '    '**************** Vehicle ****************
    '    If dt_Import.Rows(RowNo).Item("chk_Veh").ToString.Trim = "" Then
    '        Dim Parameter As String = dt_Import.Rows(RowNo).Item("IM_VEH").ToString
    '        Dim SQL As String = "SELECT * FROM VEHICLE WHERE VEH_NAME='" & Parameter.Replace("'", "''") & "'"
    '        Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '        Dim DT As New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            Dim DR As DataRow = DT.NewRow
    '            DR("VEH_ID") = GetNewID("VEHICLE", "VEH_ID")
    '            DR("VEH_NAME") = Parameter
    '            DR("UPDATE_BY") = myUser.user_id
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            Dim cmd As New SqlCommandBuilder(DA)
    '            DA.Update(DT)

    '            For i As Int32 = 0 To dt_Import.Rows.Count - 1
    '                If dt_Import.Rows(i).Item("IM_VEH").ToString = Parameter Then
    '                    dt_Import.Rows(i).Item("chk_Veh") = Parameter
    '                End If
    '            Next
    '        End If
    '    End If
    '    '**************** Customer ****************
    '    If dt_Import.Rows(RowNo).Item("chk_Cus").ToString.Trim = "" Then
    '        Dim Parameter As String = dt_Import.Rows(RowNo).Item("IM_CUS").ToString
    '        Dim SQL As String = "SELECT * FROM CUSTOMER WHERE CUS_NAME='" & Parameter.Replace("'", "''") & "'"
    '        Dim DA As New SqlDataAdapter(SQL, ConnStr)
    '        Dim DT As New DataTable
    '        DA.Fill(DT)
    '        If DT.Rows.Count = 0 Then
    '            Dim DR As DataRow = DT.NewRow
    '            DR("CUS_ID") = GetNewID("CUSTOMER", "CUS_ID")
    '            DR("CUS_NAME") = Parameter
    '            DR("UPDATE_BY") = myUser.user_id
    '            DR("UPDATE_DATE") = Now
    '            DT.Rows.Add(DR)
    '            Dim cmd As New SqlCommandBuilder(DA)
    '            DA.Update(DT)

    '            For i As Int32 = 0 To dt_Import.Rows.Count - 1
    '                If dt_Import.Rows(i).Item("IM_CUS").ToString = Parameter Then
    '                    dt_Import.Rows(i).Item("chk_Cus") = Parameter
    '                End If
    '            Next
    '        End If
    '    End If
    'End Sub

    'Dim DR_COST_S As DataRow = DT_COST_S.NewRow
    'DR_COST_S("ID") = COST_ID
    'DR_COST_S("ROUTE_FROM") = R_From
    'DR_COST_S("ROUTE_TO") = R_To
    'DR_COST_S("SUP_ID") = Supplier
    'DR_COST_S("VEH_ID") = Vehicle
    'If dt_Import.Rows(RowNo).Item("IM_COST").ToString.Trim <> "" Then
    '    DR_COST_S("PRICE") = dt_Import.Rows(RowNo).Item("IM_COST").ToString.Replace(",", "")
    'Else
    '    DR_COST_S("PRICE") = DBNull.Value
    'End If
    'DR_COST_S("UNIT") = dt_Import.Rows(RowNo).Item("IM_COST_UNIT").ToString
    'DR_COST_S("REVISE") = 0
    'DR_COST_S("REMARK") = dt_Import.Rows(RowNo).Item("IM_SUP_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "N/A" Then
    '    DR_COST_S("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString, "dd/MM/yyyy")
    'End If
    'DR_COST_S("IMPORT_DATE") = Now
    'DR_COST_S("UPDATE_BY") = myUser.user_id
    'DR_COST_S("UPDATE_DATE") = Now
    'DT_COST_S.Rows.Add(DR_COST_S)
    'Dim cmd As New SqlCommandBuilder(DA_COST_S)
    'DA_COST_S.Update(DT_COST_S)


    ''***************** Add Supplier Revise *****************
    'SQL = "SELECT * FROM COST_SUP_REVISE WHERE 1=0"
    'Dim DA_S_REV As New SqlDataAdapter(SQL, ConnStr)
    'DA_S_REV.SelectCommand.Transaction = trans

    'Dim DT_S_REV As New DataTable
    'DA_S_REV.Fill(DT_S_REV)
    'Dim DR_S_REV As DataRow = DT_S_REV.NewRow
    'DR_S_REV("ID") = GetNewID("COST_SUP_REVISE", "ID")
    'DR_S_REV("COST_ID") = COST_ID
    'DR_S_REV("PRICE") = dt_Import.Rows(RowNo).Item("IM_COST").ToString.Replace(",", "")
    'DR_S_REV("UNIT") = dt_Import.Rows(RowNo).Item("IM_COST_UNIT").ToString
    'DR_S_REV("REVISE") = 0
    'DR_S_REV("REMARK") = dt_Import.Rows(RowNo).Item("IM_SUP_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "N/A" Then
    '    DR_S_REV("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString, "dd/MM/yyyy")
    'End If
    'DR_S_REV("UPDATE_BY") = myUser.user_id
    'DR_S_REV("UPDATE_DATE") = Now
    'DT_S_REV.Rows.Add(DR_S_REV)
    ''Dim cmd_S_REV As New SqlCommandBuilder(DA_S_REV)
    'DA_S_REV.InsertCommand.Transaction = trans
    'DA_S_REV.Update(DT_S_REV)

    ''****************** Update CUST SUP ******************
    'Dim DR_COST_S As DataRow = DT_COST_S.Rows(0)
    'If dt_Import.Rows(RowNo).Item("IM_COST").ToString.Trim <> "" Then
    '    DR_COST_S("PRICE") = dt_Import.Rows(RowNo).Item("IM_COST").ToString.Replace(",", "")
    'Else
    '    DR_COST_S("PRICE") = DBNull.Value
    'End If
    'DR_COST_S("UNIT") = dt_Import.Rows(RowNo).Item("IM_COST_UNIT").ToString
    'DR_COST_S("REVISE") = DT_COST_S.Rows(0).Item("REVISE") + 1
    'DR_COST_S("REMARK") = dt_Import.Rows(RowNo).Item("IM_SUP_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "N/A" Then
    '    DR_COST_S("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString, "dd/MM/yyyy")
    'End If
    'DR_COST_S("IMPORT_DATE") = Now
    'DR_COST_S("UPDATE_BY") = myUser.user_id
    'DR_COST_S("UPDATE_DATE") = Now

    'Dim cmd1 As New SqlCommandBuilder(DA_COST_S)


    'cmd = New SqlCommand
    'cmd.Connection = trans.Connection
    'cmd.Transaction = trans
    'cmd.CommandType = CommandType.TableDirect
    ''DA_COST_S.UpdateCommand = cmd

    'DA_COST_S.Update(DT_COST_S)
    ''***************** Add Supplier Revise *****************
    'Dim DR_S_REV As DataRow = DT_S_REV.NewRow
    'DR_S_REV("ID") = GetNewID("COST_SUP_REVISE", "ID")
    'DR_S_REV("COST_ID") = DT_COST_S.Rows(0).Item("ID")
    'DR_S_REV("PRICE") = dt_Import.Rows(RowNo).Item("IM_COST").ToString.Replace(",", "")
    'DR_S_REV("UNIT") = dt_Import.Rows(RowNo).Item("IM_COST_UNIT").ToString
    'DR_S_REV("REVISE") = DT_S_REV.Rows.Count
    'DR_S_REV("REMARK") = dt_Import.Rows(RowNo).Item("IM_SUP_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString <> "N/A" Then
    '    DR_S_REV("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString, "dd/MM/yyyy")
    'End If
    'DR_S_REV("UPDATE_BY") = myUser.user_id
    'DR_S_REV("UPDATE_DATE") = Now
    'DT_S_REV.Rows.Add(DR_S_REV)
    ''Dim cmd_ As New SqlCommandBuilder(DA_S_REV)
    'DA_S_REV.InsertCommand.Transaction = trans
    'DA_S_REV.Update(DT_S_REV)

    'Dim DR_COST_C As DataRow = DT_COST_C.NewRow
    'Dim COST_ID As Int32 = GetNewID("COST_CUS", "ID")
    'DR_COST_C("ID") = COST_ID
    'DR_COST_C("ROUTE_FROM") = R_From
    'DR_COST_C("ROUTE_TO") = R_To
    'DR_COST_C("CUS_ID") = Customer
    'DR_COST_C("VEH_ID") = Vehicle
    'If dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Trim <> "" Then
    '    DR_COST_C("PRICE") = dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Replace(",", "")
    'Else
    '    DR_COST_C("PRICE") = DBNull.Value
    'End If
    'DR_COST_C("UNIT") = dt_Import.Rows(RowNo).Item("IM_VALUE_UNIT").ToString
    'DR_COST_C("REVISE") = 0
    'DR_COST_C("REMARK") = dt_Import.Rows(RowNo).Item("IM_CUS_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "N/A" Then
    '    DR_COST_C("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString, "dd/MM/yyyy")
    'End If
    'DR_COST_C("IMPORT_DATE") = Now
    'DR_COST_C("UPDATE_BY") = myUser.user_id
    'DR_COST_C("UPDATE_DATE") = Now
    'DT_COST_C.Rows.Add(DR_COST_C)
    'Dim cmd As New SqlCommandBuilder(DA_COST_C)
    'DA_COST_C.Update(DT_COST_C)


    ''***************** Add Supplier Revise *****************
    'SQL = "SELECT * FROM COST_CUS_REVISE WHERE 1=0"
    'Dim DA_C_REV As New SqlDataAdapter(SQL, ConnStr)
    'Dim DT_C_REV As New DataTable
    'DA_C_REV.Fill(DT_C_REV)
    'Dim DR_C_REV As DataRow = DT_C_REV.NewRow
    'DR_C_REV("ID") = GetNewID("COST_CUS_REVISE", "ID")
    'DR_C_REV("COST_ID") = COST_ID
    'DR_C_REV("PRICE") = dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Replace(",", "")
    'DR_C_REV("UNIT") = dt_Import.Rows(RowNo).Item("IM_VALUE_UNIT").ToString
    'DR_C_REV("REVISE") = 0
    'DR_C_REV("REMARK") = dt_Import.Rows(RowNo).Item("IM_CUS_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "N/A" Then
    '    DR_C_REV("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString, "dd/MM/yyyy")
    'End If

    'DR_C_REV("UPDATE_BY") = myUser.user_id
    'DR_C_REV("UPDATE_DATE") = Now
    'DT_C_REV.Rows.Add(DR_C_REV)
    'Dim cmd_S_REV As New SqlCommandBuilder(DA_C_REV)
    'DA_C_REV.Update(DT_C_REV)

    ''****************** Update CUST SUP ******************
    'Dim DR_COST_S As DataRow = DT_COST_C.Rows(0)
    'If dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Trim <> "" Then
    '    DR_COST_S("PRICE") = dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Replace(",", "")
    'Else
    '    DR_COST_S("PRICE") = DBNull.Value
    'End If
    'DR_COST_S("UNIT") = dt_Import.Rows(RowNo).Item("IM_VALUE_UNIT").ToString
    'DR_COST_S("REVISE") = DT_COST_C.Rows(0).Item("REVISE") + 1
    'DR_COST_S("REMARK") = dt_Import.Rows(RowNo).Item("IM_CUS_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "N/A" Then
    '    DR_COST_S("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString, "dd/MM/yyyy")
    'End If

    'DR_COST_S("IMPORT_DATE") = Now
    'DR_COST_S("UPDATE_BY") = myUser.user_id
    'DR_COST_S("UPDATE_DATE") = Now
    'Dim cmd As New SqlCommandBuilder(DA_COST_C)
    'DA_COST_C.Update(DT_COST_C)
    ''***************** Add Supplier Revise *****************
    'Dim DR_C_REV As DataRow = DT_C_REV.NewRow
    'DR_C_REV("ID") = GetNewID("COST_CUS_REVISE", "ID")
    'DR_C_REV("COST_ID") = DT_COST_C.Rows(0).Item("ID")
    'DR_C_REV("PRICE") = dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Replace(",", "")
    'DR_C_REV("UNIT") = dt_Import.Rows(RowNo).Item("IM_VALUE_UNIT").ToString
    'DR_C_REV("REVISE") = DT_C_REV.Rows.Count
    'DR_C_REV("REMARK") = dt_Import.Rows(RowNo).Item("IM_CUS_REMARK").ToString
    'If dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "" And dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString <> "N/A" Then
    '    DR_C_REV("QDATE") = StringToDate(dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString, "dd/MM/yyyy")
    'End If
    'DR_C_REV("UPDATE_BY") = myUser.user_id
    'DR_C_REV("UPDATE_DATE") = Now
    'DT_C_REV.Rows.Add(DR_C_REV)
    'Dim cmd_ As New SqlCommandBuilder(DA_C_REV)
    'DA_C_REV.Update(DT_C_REV)


    Sub InsertTransectionData(ByVal RowNo As Int32, cmd As SqlCommand)
        Dim SQL As String = ""
        Dim DT As New DataTable
        Dim _R_From As String = "0"
        Dim _R_To As String = "0"
        Dim _Supplier As String = "0"
        Dim _Vehicle As String = "0"
        Dim _Customer As String = "0"
        Dim _Import_cost As String = "0"
        Dim _sup_date As String = ""
        Dim _sup_remark As String = ""
        Dim _sup_unit As String = ""

        _R_From = dt_Import.Rows(RowNo).Item("IM_FROM_ID").ToString
        _R_To = dt_Import.Rows(RowNo).Item("IM_TO_ID").ToString
        _Vehicle = dt_Import.Rows(RowNo).Item("IM_VEH_ID").ToString
        _Supplier = dt_Import.Rows(RowNo).Item("IM_SUP_ID").ToString
        _Customer = dt_Import.Rows(RowNo).Item("IM_CUS_ID").ToString
        If dt_Import.Rows(RowNo).Item("IM_COST").ToString <> "" Then
            _Import_cost = dt_Import.Rows(RowNo).Item("IM_COST").ToString.Replace(",", "")
        End If

        _sup_date = dt_Import.Rows(RowNo).Item("IM_SUP_QDATE").ToString
        _sup_remark = dt_Import.Rows(RowNo).Item("IM_SUP_REMARK").ToString
        _sup_unit = dt_Import.Rows(RowNo).Item("IM_COST_UNIT").ToString

        Dim _import_value As String = "0"
        Dim _cus_date As String = ""
        Dim _cus_remark As String = ""
        Dim _cus_unit As String = ""

        If dt_Import.Rows(RowNo).Item("IM_VALUE").ToString <> "" Then
            _import_value = dt_Import.Rows(RowNo).Item("IM_VALUE").ToString.Replace(",", "")
        End If
        _cus_date = dt_Import.Rows(RowNo).Item("IM_CUS_QDATE").ToString
        _cus_remark = dt_Import.Rows(RowNo).Item("IM_CUS_REMARK").ToString
        _cus_unit = dt_Import.Rows(RowNo).Item("IM_VALUE_UNIT").ToString
        Dim qSupDate As String = ""
        If _sup_date IsNot Nothing AndAlso _sup_date <> "" Then
            qSupDate = StringToDate(_sup_date, "dd/MM/yyyy").ToString("yyyy-MM-dd", New System.Globalization.CultureInfo("en-US")) & " 00:00:00.000"
        End If

        Dim qCusDate As String = ""
        If _cus_date IsNot Nothing AndAlso _cus_date <> "" Then
            qCusDate = StringToDate(_cus_date, "dd/MM/yyyy").ToString("yyyy-MM-dd", New System.Globalization.CultureInfo("en-US")) & " 00:00:00.000"
        End If

        If _Supplier <> "" Then
            SQL = "SELECT * FROM COST_SUP WHERE ROUTE_FROM = " & _R_From & " AND ROUTE_TO = " & _R_To & _
                " AND SUP_ID = " & _Supplier & " AND VEH_ID = " & _Vehicle
            cmd.CommandText = SQL

            Dim DA_COST_S As New SqlDataAdapter(cmd)
            Dim DT_COST_S = New DataTable
            DA_COST_S.Fill(DT_COST_S)
            If DT_COST_S.Rows.Count = 0 Then
                '== กรณีที่ไม่พบข้อมูล ทำการ Insert

                Dim _COST_ID As Int32 = GetNewID("COST_SUP", "ID", cmd)
                SQL = "Insert Into COST_SUP(ID,ROUTE_FROM,ROUTE_TO,SUP_ID,VEH_ID,PRICE,REVISE,QDATE,REMARK,IMPORT_DATE,UNIT,UPDATE_DATE,UPDATE_BY)"
                SQL &= " Values('" & _COST_ID & "','" & _R_From & "','" & _R_To & "','" & _Supplier & _
                    "','" & _Vehicle & "'," & IIf(_Import_cost = "", "null", "'" & _Import_cost & "'") & _
                    ",0," & IIf(_sup_date = "" Or _sup_date = "N/A", "null", "'" & qSupDate & "'") & ",'" & _sup_remark & _
                    "',getdate(),'" & _sup_unit & "',getdate(),'" & myUser.user_id & "')"
                cmd.CommandText = SQL
                cmd.ExecuteNonQuery()

                Dim _COST_SUP_REVISE As String = GetNewID("COST_SUP_REVISE", "ID", cmd)
                SQL = "Insert Into COST_SUP_REVISE(ID,COST_ID,PRICE,UNIT,REVISE,REMARK,QDATE,UPDATE_BY,UPDATE_DATE)"
                SQL &= " Values('" & _COST_SUP_REVISE & "','" & _COST_ID & "'," & IIf(_Import_cost = "", "null", "'" & _Import_cost & "'") & _
                    ",'" & _sup_unit & "',0,'" & _sup_remark & "'," & IIf(_sup_date = "" Or _sup_date = "N/A", "null", "'" & qSupDate & "'") & ",'" & myUser.user_id & "',getdate())"
                cmd.CommandText = SQL
                cmd.ExecuteNonQuery()

            Else

                '== กรณีที่มีข้อมูลเดิมอยู่แล้ว เพิ่มข้อมูลใน Revise ถ้าต้นทุนเปลี่ยน
                Dim _COST_ID As Int32 = DT_COST_S.Rows(0).Item("ID")
                SQL = "SELECT * FROM COST_SUP_REVISE WHERE COST_ID = " & _COST_ID & " ORDER BY REVISE DESC"
                cmd.CommandText = SQL
                Dim DA_S_REV As New SqlDataAdapter(cmd)
                Dim DT_S_REV As New DataTable
                DA_S_REV.Fill(DT_S_REV)

                Dim _price As String = DT_S_REV.Rows(0).Item("PRICE").ToString()
                If _price <> _Import_cost Then
                    SQL = "Update COST_SUP Set PRICE='" & _Import_cost & "',UNIT='" & _sup_unit & _
                        "',REVISE='" & DT_COST_S.Rows(0).Item("REVISE") + 1 & "',REMARK='" & _sup_remark & _
                        "',QDATE=" & IIf(_sup_date = "" Or _sup_date = "N/A", "null", "'" & qSupDate & "'") & _
                        ",IMPORT_DATE=getdate(),UPDATE_BY='" & myUser.user_id & "',UPDATE_DATE=getdate() Where ID = " & _COST_ID & ""
                    cmd.CommandText = SQL
                    cmd.ExecuteNonQuery()

                    Dim _COST_SUP_REVISE As String = GetNewID("COST_SUP_REVISE", "ID", cmd)
                    SQL = "Insert Into COST_SUP_REVISE(ID,COST_ID,PRICE,UNIT,REVISE,REMARK,QDATE,UPDATE_BY,UPDATE_DATE)"
                    SQL &= " Values('" & _COST_SUP_REVISE & "','" & _COST_ID & "'," & IIf(_Import_cost = "", "null", "'" & _Import_cost & "'") & _
                        ",'" & _sup_unit & "'," & DT_S_REV.Rows.Count & ",'" & _sup_remark & "'," & IIf(_sup_date = "" Or _sup_date = "N/A", "null", "'" & qSupDate & "'") & ",'" & myUser.user_id & "',getdate())"
                    cmd.CommandText = SQL
                    cmd.ExecuteNonQuery()
                End If
            End If
        End If

        If _Customer <> "" Then
            SQL = "SELECT * FROM COST_CUS WHERE ROUTE_FROM = " & _R_From & " AND ROUTE_TO = " & _R_To & " AND CUS_ID = " & _Customer & " AND VEH_ID = " & _Vehicle
            cmd.CommandText = SQL
            Dim DA_COST_C As New SqlDataAdapter(cmd)
            Dim DT_COST_C = New DataTable
            DA_COST_C.Fill(DT_COST_C)
            If DT_COST_C.Rows.Count = 0 Then
                '== กรณีที่ไม่พบข้อมูล ทำการ Insert

                Dim _COST_ID As Int32 = GetNewID("COST_CUS", "ID", cmd)
                SQL = "Insert Into COST_CUS(ID,ROUTE_FROM,ROUTE_TO,CUS_ID,VEH_ID,PRICE,REVISE,QDATE,REMARK,IMPORT_DATE,UNIT,UPDATE_DATE,UPDATE_BY)"
                SQL &= " Values('" & _COST_ID & "','" & _R_From & "','" & _R_To & "','" & _Customer & _
                    "','" & _Vehicle & "'," & IIf(_import_value = "", "null", "'" & _import_value & "'") & _
                    ",0," & IIf(_cus_date = "" Or _cus_date = "N/A", "null", "'" & qCusDate & "'") & ",'" & _cus_remark & _
                    "',getdate(),'" & _cus_unit & "',getdate(),'" & myUser.user_id & "')"
                cmd.CommandText = SQL
                cmd.ExecuteNonQuery()

                Dim _COST_CUS_REVISE As String = GetNewID("COST_CUS_REVISE", "ID", cmd)
                SQL = "Insert Into COST_CUS_REVISE(ID,COST_ID,PRICE,UNIT,REVISE,REMARK,QDATE,UPDATE_BY,UPDATE_DATE)"
                SQL &= " Values('" & _COST_CUS_REVISE & "','" & _COST_ID & "'," & IIf(_import_value = "", "null", "'" & _import_value & "'") & _
                    ",'" & _cus_unit & "',0,'" & _cus_remark & "'," & IIf(_cus_date = "" Or _cus_date = "N/A", "null", "'" & qCusDate & "'") & ",'" & myUser.user_id & "',getdate())"
                cmd.CommandText = SQL
                cmd.ExecuteNonQuery()
            Else
                Dim _COST_ID As Int32 = DT_COST_C.Rows(0).Item("ID")
                SQL = "SELECT * FROM COST_CUS_REVISE WHERE COST_ID = " & _COST_ID & " ORDER BY REVISE DESC"
                cmd.CommandText = SQL
                Dim DA_C_REV As New SqlDataAdapter(cmd)
                Dim DT_C_REV As New DataTable
                DA_C_REV.Fill(DT_C_REV)

                Dim _price As String = DT_C_REV.Rows(0).Item("PRICE").ToString()
                If _price <> dt_Import.Rows(RowNo).Item("IM_VALUE").ToString() Then

                    SQL = "Update COST_CUS Set PRICE='" & _import_value & "',UNIT='" & _cus_unit & _
                        "',REVISE='" & DT_COST_C.Rows(0).Item("REVISE") + 1 & "',REMARK='" & _cus_remark & _
                        "',QDATE=" & IIf(_cus_date = "" Or _cus_date = "N/A", "null", "'" & qCusDate & "'") & _
                        ",IMPORT_DATE=getdate(),UPDATE_BY='" & myUser.user_id & "',UPDATE_DATE=getdate() where ID='" & _COST_ID & "'"
                    cmd.CommandText = SQL
                    cmd.ExecuteNonQuery()

                    Dim _COST_CUS_REVISE As String = GetNewID("COST_CUS_REVISE", "ID", cmd)
                    SQL = "Insert Into COST_CUS_REVISE(ID,COST_ID,PRICE,UNIT,REVISE,REMARK,QDATE,UPDATE_BY,UPDATE_DATE)"
                    SQL &= " Values('" & _COST_CUS_REVISE & "','" & _COST_ID & "'," & IIf(_import_value = "", "null", "'" & _import_value & "'") & _
                        ",'" & _cus_unit & "'," & DT_C_REV.Rows.Count & ",'" & _cus_remark & "'," & IIf(_cus_date = "" Or _cus_date = "N/A", "null", "'" & qCusDate & "'") & ",'" & myUser.user_id & "',getdate())"
                    cmd.CommandText = SQL
                    cmd.ExecuteNonQuery()

                End If
            End If
        End If
    End Sub

    Function Validation() As String
        Dim Validate As String = ""
        'For i As Int32 = 0 To Grid.Rows.Count - 1
        '    For j As Int32 = 0 To 10
        '        If Grid.Rows(i).Cells(j).Style.BackColor <> Color.White Then
        '            Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ที่ " & j + 1 & " ไม่สามารถนำเข้าข้อมูลได้"
        '        End If
        '    Next
        'Next

        For i As Int32 = 0 To dt_Import.Rows.Count - 1
            'Dim IM_FROM_ID As String = dt_Import.Rows(i)("IM_FROM_ID").ToString
            'Dim IM_TO_ID As String = dt_Import.Rows(i)("IM_TO_ID").ToString
            'Dim IM_VEH_ID As String = dt_Import.Rows(i)("IM_VEH_ID").ToString

            'If IM_FROM_ID = "" Then
            '    Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ From ไม่สามารถนำเข้าข้อมูลได้"
            'End If

            'If IM_TO_ID = "" Then
            '    Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ To ไม่สามารถนำเข้าข้อมูลได้"
            'End If

            'If IM_VEH_ID = "" Then
            '    Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ VEH ไม่สามารถนำเข้าข้อมูลได้"
            'End If



            Dim From_Status As String = dt_Import.Rows(i)("From_Status").ToString
            Dim To_Status As String = dt_Import.Rows(i)("To_Status").ToString
            Dim Sup_Status As String = dt_Import.Rows(i)("Sup_Status").ToString
            Dim Veh_Status As String = dt_Import.Rows(i)("Veh_Status").ToString
            Dim Cost_Status As String = dt_Import.Rows(i)("Cost_Status").ToString
            Dim Cost_Unit_Status As String = dt_Import.Rows(i)("Cost_Unit_Status").ToString
            Dim Sup_QDate_Status As String = dt_Import.Rows(i)("Sup_QDate_Status").ToString
            Dim Sup_Rem_Status As String = dt_Import.Rows(i)("Sup_Rem_Status").ToString
            Dim Cus_Status As String = dt_Import.Rows(i)("Cus_Status").ToString
            Dim Val_Status As String = dt_Import.Rows(i)("Val_Status").ToString
            Dim Val_Unit_Status As String = dt_Import.Rows(i)("Val_Unit_Status").ToString
            Dim Cus_QDate_Status As String = dt_Import.Rows(i)("Cus_QDate_Status").ToString
            Dim Cus_Rem_Status As String = dt_Import.Rows(i)("Cus_Rem_Status").ToString
            Dim IM_STATUS As String = dt_Import.Rows(i)("IM_STATUS").ToString

            If From_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ From ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If To_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ To ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Sup_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ Supplier ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Veh_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ Vehicle ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Cost_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ Cost ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Cost_Unit_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ Cost Unit ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Sup_QDate_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ SupplierQDate ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Sup_Rem_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ SupplierRemark ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Cus_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ Customer ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Val_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ Value ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Val_Unit_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ Value Unit ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Cus_QDate_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ CustomerQDate ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If Cus_Rem_Status <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " คอลัมน์ CustomerRemark ไม่สามารถนำเข้าข้อมูลได้"
            End If
            If IM_STATUS <> "0" Then
                Return "ข้อมูลมีปัญหา" & "แถว ที่" & i + 1 & " ไม่สามารถนำเข้าข้อมูลได้"
            End If

        Next
        Return Validate
    End Function

End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCMainQuotationDescriptionItem
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbbDescription = New System.Windows.Forms.ComboBox()
        Me.cbbCurrency = New System.Windows.Forms.ComboBox()
        Me.cbbBasis = New System.Windows.Forms.ComboBox()
        Me.txtPrice = New System.Windows.Forms.TextBox()
        Me.btnDelete = New System.Windows.Forms.Panel()
        Me.btnNote = New System.Windows.Forms.Panel()
        Me.lblNote = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cbbDescription
        '
        Me.cbbDescription.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbDescription.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbDescription.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbDescription.Location = New System.Drawing.Point(0, 0)
        Me.cbbDescription.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbDescription.Name = "cbbDescription"
        Me.cbbDescription.Size = New System.Drawing.Size(290, 24)
        Me.cbbDescription.TabIndex = 0
        '
        'cbbCurrency
        '
        Me.cbbCurrency.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbCurrency.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbCurrency.Location = New System.Drawing.Point(289, 0)
        Me.cbbCurrency.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbCurrency.Name = "cbbCurrency"
        Me.cbbCurrency.Size = New System.Drawing.Size(88, 24)
        Me.cbbCurrency.TabIndex = 1
        '
        'cbbBasis
        '
        Me.cbbBasis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbBasis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbBasis.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbBasis.Location = New System.Drawing.Point(469, 0)
        Me.cbbBasis.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbBasis.Name = "cbbBasis"
        Me.cbbBasis.Size = New System.Drawing.Size(161, 24)
        Me.cbbBasis.TabIndex = 3
        '
        'txtPrice
        '
        Me.txtPrice.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPrice.Location = New System.Drawing.Point(376, 0)
        Me.txtPrice.MaxLength = 100
        Me.txtPrice.Name = "txtPrice"
        Me.txtPrice.Size = New System.Drawing.Size(94, 24)
        Me.txtPrice.TabIndex = 2
        Me.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnDelete
        '
        Me.btnDelete.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.Cancel
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDelete.Location = New System.Drawing.Point(654, 2)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(20, 20)
        Me.btnDelete.TabIndex = 4
        '
        'btnNote
        '
        Me.btnNote.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources.BlankNote
        Me.btnNote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnNote.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNote.Location = New System.Drawing.Point(633, 2)
        Me.btnNote.Name = "btnNote"
        Me.btnNote.Size = New System.Drawing.Size(20, 20)
        Me.btnNote.TabIndex = 6
        '
        'lblNote
        '
        Me.lblNote.AutoSize = True
        Me.lblNote.Location = New System.Drawing.Point(574, 8)
        Me.lblNote.Name = "lblNote"
        Me.lblNote.Size = New System.Drawing.Size(0, 13)
        Me.lblNote.TabIndex = 7
        Me.lblNote.Visible = False
        '
        'UCMainQuotationDescriptionItem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.lblNote)
        Me.Controls.Add(Me.btnNote)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.txtPrice)
        Me.Controls.Add(Me.cbbBasis)
        Me.Controls.Add(Me.cbbCurrency)
        Me.Controls.Add(Me.cbbDescription)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "UCMainQuotationDescriptionItem"
        Me.Size = New System.Drawing.Size(675, 24)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbbDescription As System.Windows.Forms.ComboBox
    Friend WithEvents cbbCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents cbbBasis As System.Windows.Forms.ComboBox
    Friend WithEvents txtPrice As System.Windows.Forms.TextBox
    Friend WithEvents btnDelete As System.Windows.Forms.Panel
    Friend WithEvents btnNote As System.Windows.Forms.Panel
    Friend WithEvents lblNote As System.Windows.Forms.Label

End Class

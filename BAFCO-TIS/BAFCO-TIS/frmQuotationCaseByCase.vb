﻿Imports System.Data.SqlClient

Public Class frmQuotationCaseByCase
    Private WithEvents kbHook As New KeyboardHook
    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If        
    End Sub

    Private Sub frmQuotationCaseByCase_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.ShowChildForm(frmQuotationCaseByCaseSearch)
        frmQuotationCaseByCaseSearch.ShowData()
    End Sub
    Private Sub frmMainQuotation_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.ControlBox = False
        'Me.WindowState = FormWindowState.Maximized
        ClearForm()
    End Sub

    Private Sub ClearForm()
        lblID.Text = "0"
        lblAbbQuotation.Text = ""
        txtCustomerAcc.Text = ""
        txtCustomerAddress.Text = ""
        txtQuotationDate.Text = DateTime.Now.ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))
        txtQuotationNo.Text = "AUTO"
        txtJobNo.Text = ""
        txtSaleName.Text = myUser.us_code
        txtReferenceNo.Text = ""
        txtStatusName.Text = "Draft"
        txtQty.Text = "0"
        txtGW.Text = "0"
        txtVol.Text = "0"
        txtPivot.Text = "0"
        txtDimension.Text = ""
        txtRemark.Text = ""

        BindCBB_Customer(cbbCustomer)
        BindCBB_Customer(cbbCusBillTo)
        BindCBB_Mode(cbbMode, True)
        BindCBB_Scope(cbbScope)
        BindCBB_Term(cbbTerm)
        BindCBB_Country(cbbCountryOrigin)
        BindCBB_Country(cbbCountryDest)

        UcByCaseQuotationTermCondition1.FillInDefaultRemark()

    End Sub

    Public Sub FillInData(ByCaseQuotationID As Long, IsCopy As Boolean)
        Try
            Dim sql As String = "select q.id, q.qt_no, q.qt_date, q.bafco_job_no,  "
            sql += " q.cus_id, q.cus_id_bill_to, q.ms_mode_id,  q.ms_scope_id,  q.ms_term_id, "
            sql += " q.city_name_origin, q.ms_country_id_origin, q.city_name_dest, q.ms_country_id_dest,"
            sql += " q.port_name_origin, q.port_name_dest, "
            sql += " q.commodity, q.qty, q.gw, q.vol, q.[pivot], q.dimension,"
            sql += " q.user_id_sale, q.qt_note,  c.cus_account, c.cus_address, u.us_code, "
            sql += " q.estimate_total, q.estimate_currency_id, q.reference_no, "
            sql += " case q.qt_status when '0' then 'Draft' when '1' then 'Approve' when '2' then 'Deny' end qt_status_name, q.qt_status"
            sql += " from QUOTATION_BYCASE q "
            sql += " inner join CUSTOMER c on c.cus_id=q.cus_id"
            sql += " inner join [USER] u on u.us_id=q.user_id_sale"
            sql += " where q.id=@_ID"

            Dim p(1) As SqlParameter
            p(0) = SetBigInt("@_ID", ByCaseQuotationID)

            Dim dt As DataTable = Execute_DataTable(sql, p)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow = dt.Rows(0)
                lblID.Text = ByCaseQuotationID
                cbbCustomer.SelectedValue = dr("cus_id")
                If Convert.IsDBNull(dr("cus_id_bill_to")) = False Then cbbCusBillTo.SelectedValue = dr("cus_id_bill_to")
                cbbMode.SelectedValue = dr("ms_mode_id")
                cbbScope.SelectedValue = dr("ms_scope_id")
                If Convert.IsDBNull(dr("ms_term_id")) = False Then cbbTerm.SelectedValue = dr("ms_term_id")
                If Convert.IsDBNull(dr("cus_account")) = False Then txtCustomerAcc.Text = dr("cus_account")
                If Convert.IsDBNull(dr("cus_address")) = False Then txtCustomerAddress.Text = dr("cus_address")

                txtQuotationDate.Text = Convert.ToDateTime(dr("qt_date")).ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))
                txtQuotationNo.Text = dr("qt_no")
                txtJobNo.Text = dr("bafco_job_no")
                txtSaleName.Text = dr("us_code")

                If Convert.IsDBNull(dr("qty")) = False Then txtQty.Text = dr("qty")
                If Convert.IsDBNull(dr("gw")) = False Then txtGW.Text = dr("gw")
                If Convert.IsDBNull(dr("vol")) = False Then txtVol.Text = dr("vol")
                If Convert.IsDBNull(dr("pivot")) = False Then txtPivot.Text = dr("pivot")
                If Convert.IsDBNull(dr("dimension")) = False Then txtDimension.Text = dr("dimension")
                If Convert.IsDBNull(dr("qt_note")) = False Then txtRemark.Text = dr("qt_note")
                If Convert.IsDBNull(dr("commodity")) = False Then txtCommodity.Text = dr("commodity")

                If Convert.IsDBNull(dr("ms_country_id_origin")) = False Then cbbCountryOrigin.SelectedValue = dr("ms_country_id_origin")
                If Convert.IsDBNull(dr("city_name_origin")) = False Then txtCityNameOrigin.Text = dr("city_name_origin")
                If Convert.IsDBNull(dr("port_name_origin")) = False Then txtPortNameOrigin.Text = dr("port_name_origin")

                If Convert.IsDBNull(dr("ms_country_id_dest")) = False Then cbbCountryDest.SelectedValue = dr("ms_country_id_dest")
                If Convert.IsDBNull(dr("city_name_dest")) = False Then txtCityNameDest.Text = dr("city_name_dest")
                If Convert.IsDBNull(dr("port_name_dest")) = False Then txtPortNameDest.Text = dr("port_name_dest")
                If Convert.IsDBNull(dr("reference_no")) = False Then txtReferenceNo.Text = dr("reference_no")

                UcByCaseQuotationDescription1.FillInDescriptionData(ByCaseQuotationID)
                UcByCaseQuotationTruckDescription1.FillInTruckDescriptionData(ByCaseQuotationID)
                UcByCaseQuotationTermCondition1.FillInRemarkData(ByCaseQuotationID)

                Dim EstimateTotal As Double = 0
                Dim EstimateCurrencyID As Long = 1   'Default THB   value=1
                If Convert.IsDBNull(dr("estimate_total")) = False Then EstimateTotal = dr("estimate_total")
                If Convert.IsDBNull(dr("estimate_currency_id")) = False Then EstimateCurrencyID = dr("estimate_currency_id")
                UcByCaseQuotationEstimateTotal1.SetDDLCurrency()
                UcByCaseQuotationEstimateTotal1.FillInEstimateTotal(EstimateTotal, EstimateCurrencyID)

                btnSave.Visible = True
                btnApprove.Visible = True
                btnDeny.Visible = True
                btnOverwrite.Visible = False

                lblQtStatus.Text = dr("qt_status")
                txtStatusName.Text = dr("qt_status_name")

                If IsCopy = True Then
                    lblID.Text = "0"
                    txtQuotationDate.Text = DateTime.Now.ToString("dd/MM/yyyy", New Globalization.CultureInfo("en-US"))
                    txtQuotationNo.Text = "AUTO"
                    txtJobNo.Text = ""
                    txtSaleName.Text = myUser.us_code
                    lblQtStatus.Text = "0"
                    txtStatusName.Text = "Draft"
                    btnApprove.Visible = False
                    btnDeny.Visible = False
                End If

                If lblQtStatus.Text > "0" Then
                    DisableEditData()
                End If
            End If
            dt.Dispose()
        Catch ex As Exception
            CreateLogException(ex)
        End Try

    End Sub

    Private Sub DisableEditData()
        txtJobNo.Enabled = False
        txtQty.Enabled = False
        txtGW.Enabled = False
        txtVol.Enabled = False
        txtPivot.Enabled = False
        txtDimension.Enabled = False
        'txtRemark.Enabled = False

        cbbCustomer.Enabled = False
        cbbCusBillTo.Enabled = False
        cbbMode.Enabled = False
        cbbScope.Enabled = False
        cbbTerm.Enabled = False
        txtCommodity.Enabled = False

        cbbCountryOrigin.Enabled = False
        txtCityNameOrigin.Enabled = False
        txtPortNameOrigin.Enabled = False

        cbbCountryDest.Enabled = False
        txtCityNameDest.Enabled = False
        txtPortNameDest.Enabled = False
        txtReferenceNo.Enabled = False

        'btnSave.Visible = False
        btnApprove.Visible = False
        btnDeny.Visible = False

        UcByCaseQuotationDescription1.EnableDescription = False
        UcByCaseQuotationTruckDescription1.EnableDescription = False
        'UcByCaseQuotationTermCondition1.EnableTermCondition = False
        UcByCaseQuotationEstimateTotal1.EnableEstimateTotal = False

        If lblQtStatus.Text = "1" Then  'Approved
            btnOverwrite.Visible = True
        End If
    End Sub


#Region "Set Data Combobox"
    

    Private Sub cbbCustomer_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbbCustomer.SelectedIndexChanged
        If cbbCustomer.SelectedIndex > 0 Then
            Dim sql As String = "select c.cus_id, c.cus_name, c.cus_account, c.cus_address, ct.abb_quotation "
            sql += " from CUSTOMER c "
            sql += " inner join CUSTOMER_TYPE ct on ct.cus_type_id=c.cus_type_id"
            sql += " where c.cus_id=@_CUSTOMER_ID"

            Dim p(1) As SqlParameter
            p(0) = SetInt("@_CUSTOMER_ID", cbbCustomer.SelectedValue)

            Dim dt As DataTable = Execute_DataTable(sql, p)
            If dt.Rows.Count > 0 Then
                Dim dr As DataRow = dt.Rows(0)
                If Convert.IsDBNull(dr("cus_address")) = False Then txtCustomerAddress.Text = dr("cus_address")
                If Convert.IsDBNull(dr("cus_account")) = False Then txtCustomerAcc.Text = dr("cus_account")
                If Convert.IsDBNull(dr("abb_quotation")) = False Then lblAbbQuotation.Text = dr("abb_quotation")

                If cbbCusBillTo.SelectedValue = 0 Then
                    cbbCusBillTo.SelectedValue = dr("cus_id")
                End If
            End If
            dt.Dispose()
        End If
    End Sub

#End Region

    Private Function ValidateData() As Boolean
        If cbbCustomer.SelectedValue = "0" Then
            MessageBox.Show("กรุณาเลือกชื่อลูกค้า", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If
        'หา Customer Type เพื่อตรวจสอบค่า app_quotation
        If lblAbbQuotation.Text.Trim = "" Then
            MessageBox.Show("คุณเลือกประเภทลูกค้าที่มีสามารถ Run เลขที่ใบเสนอราคาได้", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cbbCustomer.Focus()
            Return False
        End If
        If cbbMode.SelectedValue = "0" Then
            MessageBox.Show("กรุณาเลือก Mode", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If
        If cbbScope.SelectedValue = "0" Then
            MessageBox.Show("กรุณาเลือก Scope", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If
        If txtJobNo.Text.Trim = "" Then
            MessageBox.Show("กรุณากรอก JOB No", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If
        
        Return True
    End Function

    Private Function GetNewQuotationNo() As String
        'Format Ex. BAF-CA-ABB-YYYY-MM-XXX  
        Dim ret As String = ""
        Dim yyyyMM As String = DateTime.Now.ToString("yyyy-MM", New Globalization.CultureInfo("en-US"))

        Dim sql As String = "select top 1 qt.id, qt.qt_no"
        sql += " from QUOTATION_BYCASE qt "
        sql += " inner join CUSTOMER c on c.cus_id=qt.cus_id "
        sql += " inner join CUSTOMER_TYPE ct on ct.cus_type_id=c.cus_type_id "
        sql += " where substring(qt.qt_no,8,3)=@_APP_QUOTATION "  'ABB
        sql += " and substring(qt.qt_no,12,7)=@_YYYYMM"   'YYYY-MM
        sql += " order by qt.id desc"

        Dim p(2) As SqlParameter
        p(0) = SetText("@_APP_QUOTATION", lblAbbQuotation.Text)
        p(1) = SetText("@_YYYYMM", yyyyMM)

        ret = "BAF-CA-" & lblAbbQuotation.Text & "-" & yyyyMM & "-"

        Dim dt As DataTable = Execute_DataTable(sql, p)
        If dt.Rows.Count > 0 Then
            Dim qtNo As String = dt.Rows(0)("qt_no")
            Dim runNo As Integer = Convert.ToInt16(Strings.Right(qtNo, 3)) + 1

            ret += runNo.ToString.PadLeft(3, "0")
        Else
            ret += "001"
        End If
        dt.Dispose()

        Return ret
    End Function

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If ValidateData() = True Then
            Try
                Dim sql As String = ""
                Dim p() As SqlParameter
                Dim NewQuotationNo As String = ""
                If lblID.Text = "0" Then
                    sql = " insert into quotation_bycase (created_by, created_date, qt_no, qt_date, bafco_job_no, cus_id, cus_id_bill_to,"
                    sql += " ms_mode_id, ms_scope_id, ms_term_id, ms_country_id_origin, city_name_origin, ms_country_id_dest, city_name_dest, "
                    sql += " port_name_origin, port_name_dest, commodity, dimension, qty, gw, vol, [pivot], user_id_sale, qt_note, qt_status, "
                    sql += " estimate_total, estimate_currency_id, reference_no)"
                    sql += " output inserted.id, inserted.qt_date "
                    sql += " values(@_CREATED_BY, getdate(), @_QT_NO, @_QT_DATE, @_BAFCO_JOB_NO, @_CUS_ID, @_CUS_ID_BILL_TO,"
                    sql += " @_MS_MODE_ID, @_MS_SCOPE_ID, @_MS_TERM_ID, @_MS_COUNTRY_ID_ORIGIN, @_CITY_NAME_ORIGIN, @_MS_COUNTRY_ID_DEST, @_CITY_NAME_DEST, "
                    sql += " @_PORT_NAME_ORIGIN, @_PORT_NAME_DEST, @_COMMODITY, @_DIMENSION, @_QTY, @_GW, @_VOL, @_PIVOT, @_USER_ID_SALE, @_QT_NOTE, @_QT_STATUS, "
                    sql += " @_ESTIMATE_TOTAL, @_ESTIMATE_CURRENCY_ID, @_REFERENCE_NO)"

                    NewQuotationNo = GetNewQuotationNo()
                    ReDim p(27)
                    p(0) = SetText("@_CREATED_BY", myUser.us_code)
                    p(1) = SetText("@_QT_NO", NewQuotationNo)
                    p(2) = SetDateTime("@_QT_DATE", DateTime.Now)
                    p(3) = SetText("@_BAFCO_JOB_NO", txtJobNo.Text.Trim)
                    p(4) = SetBigInt("@_CUS_ID", cbbCustomer.SelectedValue)
                    p(5) = SetBigInt("@_CUS_ID_BILL_TO", cbbCusBillTo.SelectedValue)
                    p(6) = SetBigInt("@_MS_MODE_ID", cbbMode.SelectedValue)
                    p(7) = SetBigInt("@_MS_SCOPE_ID", cbbScope.SelectedValue)
                    p(8) = SetBigInt("@_MS_TERM_ID", IIf(cbbTerm.SelectedValue = 0, "", cbbTerm.SelectedValue))
                    p(9) = SetBigInt("@_MS_COUNTRY_ID_ORIGIN", IIf(cbbCountryOrigin.SelectedValue = 0, "", cbbCountryOrigin.SelectedValue))
                    p(10) = SetText("@_CITY_NAME_ORIGIN", txtCityNameOrigin.Text)
                    p(11) = SetBigInt("@_MS_COUNTRY_ID_DEST", IIf(cbbCountryDest.SelectedValue = 0, "", cbbCountryDest.SelectedValue))
                    p(12) = SetText("@_CITY_NAME_DEST", txtCityNameDest.Text)
                    p(13) = SetText("@_PORT_NAME_ORIGIN", txtPortNameOrigin.Text)
                    p(14) = SetText("@_PORT_NAME_DEST", txtPortNameDest.Text)
                    p(15) = SetText("@_COMMODITY", txtCommodity.Text)
                    p(16) = SetText("@_DIMENSION", txtDimension.Text)
                    p(17) = SetInt("@_QTY", txtQty.Text)
                    p(18) = SetInt("@_GW", txtGW.Text)
                    p(19) = SetInt("@_VOL", txtVol.Text)
                    p(20) = SetInt("@_PIVOT", txtPivot.Text)
                    p(21) = SetInt("@_USER_ID_SALE", myUser.user_id)
                    p(22) = SetText("@_QT_NOTE", txtRemark.Text)
                    p(23) = SetText("@_QT_STATUS", "0")
                    p(24) = SetFloat("@_ESTIMATE_TOTAL", UcByCaseQuotationEstimateTotal1.txtEstimateGrandTotal.Text)
                    p(25) = SetBigInt("@_ESTIMATE_CURRENCY_ID", UcByCaseQuotationEstimateTotal1.cbbEstimateCurrency.SelectedValue)
                    p(26) = SetText("@_REFERENCE_NO", txtReferenceNo.Text)
                Else
                    sql = " update quotation_bycase"
                    sql += " set updated_by=@_UPDATED_BY "
                    sql += ", updated_date=getdate() "
                    sql += ", cus_id=@_CUS_ID"
                    sql += ", bafco_job_no=@_BAFCO_JOB_NO"
                    sql += ", ms_mode_id=@_MS_MODE_ID "
                    sql += ", ms_scope_id=@_MS_SCOPE_ID "
                    sql += ", ms_term_id=@_MS_TERM_ID"
                    sql += ", ms_country_id_origin=@_MS_COUNTRY_ID_ORIGIN"
                    sql += ", city_name_origin=@_CITY_NAME_ORIGIN"
                    sql += ", ms_country_id_dest=@_MS_COUNTRY_ID_DEST"
                    sql += ", city_name_dest=@_CITY_NAME_DEST"
                    sql += ", port_name_origin=@_PORT_NAME_ORIGIN"
                    sql += ", port_name_dest=@_PORT_NAME_DEST"
                    sql += ", commodity=@_COMMODITY"
                    sql += ", dimension=@_DIMENSION"
                    sql += ", qty=@_QTY"
                    sql += ", gw=@_GW"
                    sql += ", vol=@_VOL"
                    sql += ", [pivot]=@_PIVOT"
                    sql += ", qt_note=@_QT_NOTE"
                    sql += ", estimate_total=@_ESTIMATE_TOTAL "
                    sql += ", estimate_currency_id=@_ESTIMATE_CURRENCY_ID "
                    sql += ", cus_id_bill_to=@_CUS_ID_BILL_TO "
                    sql += ", reference_no=@_REFERENCE_NO "
                    sql += " output inserted.id, inserted.qt_date "
                    sql += " where id=@_ID"

                    NewQuotationNo = txtQuotationNo.Text

                    ReDim p(24)
                    p(0) = SetText("@_UPDATED_BY", myUser.us_code)
                    p(1) = SetBigInt("@_CUS_ID", cbbCustomer.SelectedValue)
                    p(2) = SetText("@_BAFCO_JOB_NO", txtJobNo.Text.Trim)
                    p(3) = SetBigInt("@_MS_MODE_ID", cbbMode.SelectedValue)
                    p(4) = SetBigInt("@_MS_SCOPE_ID", cbbScope.SelectedValue)
                    p(5) = SetBigInt("@_MS_TERM_ID", IIf(cbbTerm.SelectedValue = 0, "", cbbTerm.SelectedValue))
                    p(6) = SetBigInt("@_MS_COUNTRY_ID_ORIGIN", IIf(cbbCountryOrigin.SelectedValue = 0, "", cbbCountryOrigin.SelectedValue))
                    p(7) = SetText("@_CITY_NAME_ORIGIN", txtCityNameOrigin.Text)
                    p(8) = SetBigInt("@_MS_COUNTRY_ID_DEST", IIf(cbbCountryDest.SelectedValue = 0, "", cbbCountryDest.SelectedValue))
                    p(9) = SetText("@_CITY_NAME_DEST", txtCityNameDest.Text)
                    p(10) = SetText("@_PORT_NAME_ORIGIN", txtPortNameOrigin.Text)
                    p(11) = SetText("@_PORT_NAME_DEST", txtPortNameDest.Text)
                    p(12) = SetText("@_COMMODITY", txtCommodity.Text)
                    p(13) = SetText("@_DIMENSION", txtDimension.Text)
                    p(14) = SetInt("@_QTY", txtQty.Text)
                    p(15) = SetInt("@_GW", txtGW.Text)
                    p(16) = SetInt("@_VOL", txtVol.Text)
                    p(17) = SetInt("@_PIVOT", txtPivot.Text)
                    p(18) = SetText("@_QT_NOTE", txtRemark.Text)
                    p(19) = SetText("@_ID", lblID.Text)
                    p(20) = SetFloat("@_ESTIMATE_TOTAL", UcByCaseQuotationEstimateTotal1.txtEstimateGrandTotal.Text)
                    p(21) = SetBigInt("@_ESTIMATE_CURRENCY_ID", UcByCaseQuotationEstimateTotal1.cbbEstimateCurrency.SelectedValue)
                    p(22) = SetBigInt("@_CUS_ID_BILL_TO", cbbCusBillTo.SelectedValue)
                    p(23) = SetText("@_REFERENCE_NO", txtReferenceNo.Text)
                End If

                Dim conn As New SqlConnection(ConnStr)
                conn.Open()
                If conn.State <> ConnectionState.Open Then
                    MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                    Exit Sub
                End If
                Dim trans As SqlTransaction
                trans = conn.BeginTransaction

                Dim dt As DataTable = Execute_DataTable(sql, trans, p)
                If dt.Rows.Count > 0 Then
                    Dim QuotationID As Long = dt.Rows(0)("id")
                    Dim QuotationDate As Date = Convert.ToDateTime(dt.Rows(0)("qt_date")).Date
                    sql = " delete from QUOTATION_BYCASE_ITEM where quotation_bycase_id=@_BYCASE_QUOTATION_ID"
                    ReDim p(1)
                    p(0) = SetBigInt("@_BYCASE_QUOTATION_ID", QuotationID)

                    Dim ret As String = Execute_Command(sql, trans, p)
                    If ret.ToLower = "true" Then

                        sql = "delete from QUOTATION_BYCASE_TRUCK_ITEM where quotation_bycase_id=@_BYCASE_QUOTATION_ID"
                        ReDim p(1)
                        p(0) = SetBigInt("@_BYCASE_QUOTATION_ID", QuotationID)

                        ret = Execute_Command(sql, trans, p)
                        If ret.ToLower = "true" Then
                            sql = " delete from QUOTATION_BYCASE_CONDITION where quotation_bycase_id=@_BYCASE_QUOTATION_ID"
                            ReDim p(1)
                            p(0) = SetBigInt("@_BYCASE_QUOTATION_ID", QuotationID)
                            ret = Execute_Command(sql, trans, p)

                            If ret.ToLower = "true" Then
                                Dim re As String = "false"
                                For Each ctl As UCByCaseQuotationDescriptionItem In UcByCaseQuotationDescription1.flpDescription.Controls
                                    sql = "insert into QUOTATION_BYCASE_ITEM(created_by, created_date, quotation_bycase_id, qt_category_description_id, ms_basis_id, unit_rate, min_rate, qty, ms_currency_id,item_note)"
                                    sql += " values(@_CREATED_BY, getdate(), @_BYCASE_QUOTATION_ID, @_DESCRIPTION_ID, @_BASIS_ID, @_UNIT_RATE, @_MIN_RATE, @_QTY, @_CURRENCY_ID,@_ITEM_NOTE) "
                                    ReDim p(9)
                                    p(0) = SetText("@_CREATED_BY", myUser.us_code)
                                    p(1) = SetBigInt("@_BYCASE_QUOTATION_ID", QuotationID)
                                    p(2) = SetBigInt("@_DESCRIPTION_ID", ctl.cbbDescription.SelectedValue)
                                    p(3) = SetBigInt("@_BASIS_ID", ctl.cbbBasis.SelectedValue)
                                    p(4) = SetFloat("@_UNIT_RATE", ctl.txtUnitRate.Text)
                                    p(5) = SetFloat("@_MIN_RATE", ctl.txtMinRate.Text)
                                    p(6) = SetInt("@_QTY", ctl.txtQty.Text)
                                    p(7) = SetBigInt("@_CURRENCY_ID", ctl.cbbCurrency.SelectedValue)
                                    p(8) = SetText("@_ITEM_NOTE", ctl.lblNote.Text)

                                    ret = Execute_Command(sql, trans, p)
                                    If ret.ToLower <> "true" Then
                                        Exit For
                                    Else
                                        re = SaveDataToServiceCost(trans, cbbCustomer.SelectedValue, ctl.cbbDescription.Text, ctl.txtUnitRate.Text, ctl.cbbBasis.Text, ctl.lblNote.Text, "ByCase", QuotationDate, NewQuotationNo, lblQtStatus.Text)
                                        If re = False Then
                                            ret = "false"
                                            Exit For
                                        Else
                                            ret = "true"
                                        End If
                                    End If
                                Next

                                If ret.ToLower = "true" Then
                                    For Each ctl As UCByCaseQuotationTruckDescriptionItem In UcByCaseQuotationTruckDescription1.flpTruckDescription.Controls
                                        sql = "insert into QUOTATION_BYCASE_TRUCK_ITEM(created_by, created_date, quotation_bycase_id, route_id_from, route_id_to,vehicle_type_id,  ms_basis_id, unit_rate,  qty, ms_currency_id,item_note)"
                                        sql += " values(@_CREATED_BY, getdate(), @_BYCASE_QUOTATION_ID, @_ROUTE_ID_FROM, @_ROUTE_ID_TO, @_VEHICLE_TYPE_ID, @_BASIS_ID, @_UNIT_RATE,  @_QTY, @_CURRENCY_ID,@_ITEM_NOTE) "
                                        ReDim p(10)
                                        p(0) = SetText("@_CREATED_BY", myUser.us_code)
                                        p(1) = SetBigInt("@_BYCASE_QUOTATION_ID", QuotationID)
                                        p(2) = SetInt("@_ROUTE_ID_FROM", ctl.cbbRouteFrom.SelectedValue)
                                        p(3) = SetInt("@_ROUTE_ID_TO", ctl.cbbRouteTo.SelectedValue)
                                        p(4) = SetInt("@_VEHICLE_TYPE_ID", ctl.cbbVehicleType.SelectedValue)
                                        p(5) = SetBigInt("@_BASIS_ID", ctl.cbbBasis.SelectedValue)
                                        p(6) = SetFloat("@_UNIT_RATE", ctl.txtUnitRate.Text)
                                        p(7) = SetInt("@_QTY", ctl.txtQty.Text)
                                        p(8) = SetBigInt("@_CURRENCY_ID", ctl.cbbCurrency.SelectedValue)
                                        p(9) = SetText("@_ITEM_NOTE", ctl.lblNote.Text)

                                        ret = Execute_Command(sql, trans, p)
                                        If ret.ToLower <> "true" Then
                                            Exit For
                                        Else
                                            re = SaveDataToTransportationCost(trans, cbbCustomer.SelectedValue, ctl.cbbRouteFrom.SelectedValue, ctl.cbbRouteTo.SelectedValue, ctl.cbbVehicleType.SelectedValue, ctl.txtUnitRate.Text, ctl.cbbBasis.Text, ctl.lblNote.Text, "ByCase", QuotationDate, NewQuotationNo, lblQtStatus.Text)
                                            If re = False Then
                                                ret = "false"
                                                Exit For
                                            Else
                                                ret = "true"
                                            End If
                                        End If
                                    Next

                                    If ret.ToLower = "true" Then
                                        For Each ctl As UCByCaseQuotationTermConditionsItem In UcByCaseQuotationTermCondition1.flpTermCondition.Controls
                                            sql = "insert into QUOTATION_BYCASE_CONDITION (created_by, created_date, quotation_bycase_id, qt_category_remark_id) "
                                            sql += " values(@_CREATED_BY, getdate(), @_BYCASE_QUOTATION_ID, @_CATEGORY_REMARK_ID)"
                                            ReDim p(3)
                                            p(0) = SetText("@_CREATED_BY", myUser.us_code)
                                            p(1) = SetBigInt("@_BYCASE_QUOTATION_ID", QuotationID)
                                            p(2) = SetBigInt("@_CATEGORY_REMARK_ID", ctl.cbbRemarks.SelectedValue)

                                            ret = Execute_Command(sql, trans, p)
                                            If ret.ToLower <> "true" Then
                                                Exit For
                                            End If
                                        Next
                                    End If
                                End If
                            End If
                        End If
                    End If

                    If ret.ToLower = "true" Then
                        lblID.Text = QuotationID
                        txtQuotationNo.Text = NewQuotationNo
                        btnApprove.Visible = True
                        btnDeny.Visible = True
                        trans.Commit()

                        If lblQtStatus.Text > "0" Then
                            DisableEditData()
                        End If
                        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        trans.Rollback()
                    End If
                Else
                    trans.Rollback()

                End If
                dt.Dispose()
            Catch ex As Exception
                CreateLogException(ex)
            End Try
        End If
    End Sub

    Private Sub UcByCaseQuotationDescription1_PriceTotalChange() Handles UcByCaseQuotationDescription1.PriceTotalChange
        CalPriceTotal()
    End Sub

    Private Sub UcByCaseQuotationTruckDescription1_PriceTotalChange() Handles UcByCaseQuotationTruckDescription1.PriceTotalChange
        CalPriceTotal()
    End Sub

    Private Sub CalPriceTotal()
        UcByCaseQuotationEstimateTotal1.ClearEstimateTotal()

        For Each ctl As UCByCaseQuotationDescriptionItem In UcByCaseQuotationDescription1.flpDescription.Controls
            If ctl.txtUnitRate.Text.Trim = "" Then Continue For
            If ctl.txtQty.Text.Trim = "" Then Continue For

            Dim MoneyValue As Double = Convert.ToDouble(ctl.txtUnitRate.Text) * Convert.ToInt64(ctl.txtQty.Text)
            Dim CurrencyText As String = ctl.cbbCurrency.Text
            UcByCaseQuotationEstimateTotal1.UpdateEstimateTotal(MoneyValue, CurrencyText)
        Next

        For Each ctl As UCByCaseQuotationTruckDescriptionItem In UcByCaseQuotationTruckDescription1.flpTruckDescription.Controls
            If ctl.txtUnitRate.Text.Trim = "" Then Continue For
            If ctl.txtQty.Text.Trim = "" Then Continue For

            Dim MoneyValue As Double = Convert.ToDouble(ctl.txtUnitRate.Text) * Convert.ToInt64(ctl.txtQty.Text)
            Dim CurrencyText As String = ctl.cbbCurrency.Text
            UcByCaseQuotationEstimateTotal1.UpdateEstimateTotal(MoneyValue, CurrencyText)
        Next
    End Sub

    Private Sub btnApprove_Click(sender As Object, e As System.EventArgs) Handles btnApprove.Click
        If lblID.Text = "0" Then Exit Sub

        Dim y As DialogResult = MessageBox.Show("ยืนยันการอนุมัติ Case by Case Quotation?", "Confirm?", MessageBoxButtons.OKCancel)
        If y = Windows.Forms.DialogResult.OK Then
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            If conn.State <> ConnectionState.Open Then
                MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                Exit Sub
            End If
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction

            Dim sql As String = " update QUOTATION_BYCASE "
            sql += " set qt_status=1"
            sql += " where id=@_BYCASE_QUOTATION_ID "

            Dim p(1) As SqlParameter
            p(0) = SetBigInt("@_BYCASE_QUOTATION_ID", lblID.Text)

            If Execute_Command(sql, trans, p) = "true" Then
                If ChangeStatusToServiceCost(trans) = True Then
                    If ChangeStatusToTransportationCost(trans) = True Then
                        trans.Commit()
                        lblQtStatus.Text = "1"
                        txtStatusName.Text = "Approved"
                        DisableEditData()
                    Else
                        trans.Rollback()
                    End If
                Else
                    trans.Rollback()
                End If
            Else
                trans.Rollback()
            End If
        End If
    End Sub

    Private Function ChangeStatusToServiceCost(trans As SqlTransaction) As Boolean
        Dim ret As Boolean = False
        Dim sql As String = "select q.qt_date,q.qt_no,q.qt_status, qd.desc_name, qi.unit_rate,bs.basis_name, qi.item_note"
        sql += " from QUOTATION_BYCASE_ITEM qi"
        sql += " inner join QUOTATION_BYCASE q on q.id=qi.quotation_bycase_id"
        sql += " inner join QT_CATEGORY_DESCRIPTION qd on qd.id=qt_category_description_id"
        sql += " inner join MS_BASIS bs on bs.id=qi.ms_basis_id"
        sql += " where qi.quotation_bycase_id=@_QUOTATION_ID"

        Dim p(1) As SqlParameter
        p(0) = SetBigInt("@_QUOTATION_ID", lblID.Text)
        Dim dt As DataTable = Execute_DataTable(Sql, trans, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim ItemNote As String = ""
                If Convert.IsDBNull(dr("item_note")) = False Then ItemNote = dr("item_note")

                ret = SaveDataToServiceCost(trans, cbbCustomer.SelectedValue, dr("desc_name"), dr("unit_rate"), dr("basis_name"), ItemNote, "ByCase", Convert.ToDateTime(dr("qt_date")), dr("qt_no"), dr("qt_status"))
                If ret = False Then
                    Exit For
                End If
            Next
        End If
        dt.Dispose()

        Return ret
    End Function

    Private Function ChangeStatusToTransportationCost(trans As SqlTransaction) As Boolean
        Dim ret As Boolean = False
        Dim sql As String = ""
        sql += " select q.qt_date, q.qt_status, q.qt_no, qi.route_id_from, qi.route_id_to, qi.vehicle_type_id, "
        sql += " bs.basis_name, qi.unit_rate, qi.item_note" & Environment.NewLine
        sql += " from QUOTATION_BYCASE_TRUCK_ITEM qi" & Environment.NewLine
        sql += " inner join QUOTATION_BYCASE q on q.id=qi.quotation_bycase_id" & Environment.NewLine
        sql += " inner join MS_BASIS bs on bs.id=qi.ms_basis_id" & Environment.NewLine
        sql += " where qi.quotation_bycase_id=@_QUOTATION_ID " & Environment.NewLine

        Dim p(1) As SqlParameter
        p(0) = SetBigInt("@_QUOTATION_ID", lblID.Text)
        Dim dt As DataTable = Execute_DataTable(sql, trans, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim ItemNote As String = ""
                If Convert.IsDBNull(dr("item_note")) = False Then ItemNote = dr("item_note")

                ret = SaveDataToTransportationCost(trans, cbbCustomer.SelectedValue, dr("route_id_from"), dr("route_id_to"), dr("vehicle_type_id"), dr("unit_rate"), dr("basis_name"), ItemNote, "ByCase", Convert.ToDateTime(dr("qt_date")), dr("qt_no"), dr("qt_status"))
                If ret = False Then
                    Exit For
                End If
            Next
        End If
        dt.Dispose()
        Return ret
    End Function

    Private Sub btnDeny_Click(sender As Object, e As System.EventArgs) Handles btnDeny.Click
        If lblID.Text = "0" Then Exit Sub

        Dim frm As New frmDialogDenyReason
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim conn As New SqlConnection(ConnStr)
            conn.Open()
            If conn.State <> ConnectionState.Open Then
                MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
                Exit Sub
            End If
            Dim trans As SqlTransaction
            trans = conn.BeginTransaction

            Dim sql As String = " update QUOTATION_BYCASE "
            sql += " set qt_status=2, reject_reason=@_REJECT_REASON "
            sql += " where id=@_BYCASE_QUOTATION_ID "

            Dim p(2) As SqlParameter
            p(0) = SetBigInt("@_BYCASE_QUOTATION_ID", lblID.Text)
            p(1) = SetText("@_REJECT_REASON", frm.txtReason.Text)

            If Execute_Command(sql, trans, p) = "true" Then
                If ChangeStatusToServiceCost(trans) = True Then
                    If ChangeStatusToTransportationCost(trans) = True Then
                        trans.Commit()
                        lblQtStatus.Text = "2"
                        txtStatusName.Text = "Deny"
                        DisableEditData()
                    Else
                        trans.Rollback()
                    End If
                Else
                    trans.Rollback()
                End If
            Else
                trans.Rollback()
            End If
        End If
    End Sub

    Private Sub btnPrint_Click(sender As System.Object, e As System.EventArgs) Handles btnPrint.Click
        Dim sql As String = "select q.id, q.qt_no,convert(varchar(10), q.qt_date,103) qt_date, u.us_name, q.bafco_job_no, c.cus_name,cb.cus_name bill_to," & Environment.NewLine
        sql += " md.mode_name, q.qty, q.gw, q.dimension, q.qt_note  remarks," & Environment.NewLine
        sql += " q.city_name_origin, cno.country_name country_name_origin," & Environment.NewLine
        sql += " q.city_name_dest, cnd.country_name  country_name_dest," & Environment.NewLine
        sql += " qi.desc_name, qi.basis_name, qi.unit_rate, qi.min_rate,qi.item_qty, qi.currency_name, " & Environment.NewLine
        sql += " isnull(q.estimate_total,0) estimate_total, isnull(cre.currency_name,'') estimate_currency_name, " & Environment.NewLine
        sql += " isnull((select count(id) from QUOTATION_BYCASE_CONDITION where quotation_bycase_id=q.id),0) count_term " & Environment.NewLine
        sql += " from QUOTATION_BYCASE q" & Environment.NewLine
        sql += " inner join [USER] u on u.us_id=q.user_id_sale" & Environment.NewLine
        sql += " inner join CUSTOMER c on c.cus_id=q.cus_id" & Environment.NewLine
        sql += " left join CUSTOMER cb on cb.cus_id=q.cus_id_bill_to " & Environment.NewLine
        sql += " inner join MS_MODE md on md.id=q.ms_mode_id" & Environment.NewLine
        sql += " left join MS_COUNTRY cno on cno.id=q.ms_country_id_origin" & Environment.NewLine
        sql += " left join MS_COUNTRY cnd on cnd.id=q.ms_country_id_dest" & Environment.NewLine
        sql += " left join MS_CURRENCY cre on cre.id=q.estimate_currency_id" & Environment.NewLine
        sql += " inner join (" & Environment.NewLine
        sql += "            select qi.quotation_bycase_id, cd.desc_name, bs.basis_name, qi.unit_rate, qi.min_rate,qi.qty item_qty, crn.currency_name" & Environment.NewLine
        sql += "            from QUOTATION_BYCASE_ITEM qi" & Environment.NewLine
        sql += "            inner join QT_CATEGORY_DESCRIPTION cd on cd.id=qi.qt_category_description_id " & Environment.NewLine
        sql += "            inner join MS_BASIS bs on bs.id=qi.ms_basis_id" & Environment.NewLine
        sql += "            inner join MS_CURRENCY crn on crn.id=qi.ms_currency_id" & Environment.NewLine
        sql += "            where qi.quotation_bycase_id=@_QUOTATION_ID " & Environment.NewLine
        sql += "            UNION ALL " & Environment.NewLine

        sql += "            select qi.quotation_bycase_id, 'From ' + rf.ROUTE_NAME + ' To ' + rt.ROUTE_NAME + ' By ' + v.VEH_NAME desc_name, "
        sql += "            bs.basis_name, qi.unit_rate, 0 min_rate,qi.qty item_qty, crn.currency_name" & Environment.NewLine
        sql += "            from QUOTATION_BYCASE_TRUCK_ITEM qi" & Environment.NewLine
        sql += "            inner join [ROUTE] rf on rf.ROUTE_ID=qi.route_id_from" & Environment.NewLine
        sql += "            inner join [ROUTE] rt on rt.ROUTE_ID=qi.route_id_to" & Environment.NewLine
        sql += "            inner join VEHICLE v on v.VEH_ID=qi.vehicle_type_id" & Environment.NewLine
        sql += "            inner join MS_BASIS bs on bs.id=qi.ms_basis_id" & Environment.NewLine
        sql += "            inner join MS_CURRENCY crn on crn.id=qi.ms_currency_id" & Environment.NewLine
        sql += "            where qi.quotation_bycase_id=@_QUOTATION_ID " & Environment.NewLine
        sql += " ) qi on q.id=qi.quotation_bycase_id" & Environment.NewLine
        sql += " where q.id=@_QUOTATION_ID"

        Dim p(1) As SqlParameter
        p(0) = SetBigInt("@_QUOTATION_ID", lblID.Text)

        Dim dt As DataTable = Execute_DataTable(sql, p)

        If dt.Rows.Count > 0 Then
            Dim q As String = "select sum(a.unit_rate*a.item_qty) total_amt, a.currency_name, a.estimate_total,a.estimate_currency_name "
            q += " from (" & sql & ") a "
            q += " group by a.currency_name, a.estimate_total,a.estimate_currency_name"

            ReDim p(1)
            p(0) = SetBigInt("@_QUOTATION_ID", lblID.Text)

            Dim totalDt As DataTable = Execute_DataTable(q, p)
            totalDt.Columns.Add("count_currency", GetType(Integer))
            For i As Integer = 0 To totalDt.Rows.Count - 1
                totalDt.Rows(i)("count_currency") = totalDt.Rows.Count
            Next

            Dim f As New frmPrintQuotation
            With f
                .QuotationID = lblID.Text
                .ReportName = "Quotation_ByCase.rpt"
                '.LocationFrom = IIf(cbbFrom.Text = "", "All", cbbFrom.Text)
                '.LocationTo = IIf(cbbTo.Text = "", "All", cbbTo.Text)
                '.Vehicle = "" 'VehFilterText
                '.Supplier = IIf(_sup_id.ToString() = "", "Transportation Cost from : All", "Transportation Cost from :</br>" & _sup_id.ToString())
                '.DTSUP = dt
                .DT = dt
                .TotalDt = totalDt
                .ShowDialog()
            End With
        End If
    End Sub

    Private Sub UcByCaseQuotationDescription1_SelectDescription(ddlDescription As System.Windows.Forms.ComboBox, txtPrice As System.Windows.Forms.TextBox, ddlBasis As System.Windows.Forms.ComboBox, ddlCurrency As System.Windows.Forms.ComboBox) Handles UcByCaseQuotationDescription1.SelectDescription
        If ddlDescription.SelectedValue > 0 Then
            Dim sql As String = "select top 1 md.price,md.ms_basis_id,md.ms_currency_id " & Environment.NewLine
            sql += " from QUOTATION_MAIN_DESC md " & Environment.NewLine
            sql += " inner join QUOTATION_MAIN_DETAIL qmd on qmd.id=md.quotation_main_detail_id " & Environment.NewLine
            sql += " inner join QUOTATION_MAIN q on q.id=qmd.quotation_main_id " & Environment.NewLine
            sql += " where q.cus_id = @_CUS_ID " & Environment.NewLine
            sql += " and md.qt_category_description_id=@_CATEGORY_DESC_ID " & Environment.NewLine
            sql += " order by q.qt_date desc, q.id desc "

            Dim p(2) As SqlParameter
            p(0) = SetInt("@_CUS_ID", cbbCustomer.SelectedValue)
            p(1) = SetBigInt("@_CATEGORY_DESC_ID", ddlDescription.SelectedValue)

            Dim dt As DataTable = Execute_DataTable(sql, p)
            If dt.Rows.Count > 0 Then
                txtPrice.Text = Convert.ToDouble(dt.Rows(0)("price")).ToString("#,##0.00")
                ddlBasis.SelectedValue = Convert.ToInt64(dt.Rows(0)("ms_basis_id"))
                ddlCurrency.SelectedValue = Convert.ToInt64(dt.Rows(0)("ms_currency_id"))
            End If
            dt.Dispose()
        End If
    End Sub

    Private Sub btnOverwrite_Click(sender As System.Object, e As System.EventArgs) Handles btnOverwrite.Click
        Dim frm As New frmDialogOverwriteReason
        frm.txtSaleName.Text = myUser.fulllname
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim ret As String = SaveOverwriteQuotation("QUOTATION_BYCASE", lblID.Text, frm.txtReason.Text.Trim)
            If ret = "true" Then
                Dim sql As String = "update QUOTATION_BYCASE "
                sql += " set qt_status=0"
                sql += " where id=@_ID"
                Dim p(1) As SqlParameter
                p(0) = SetBigInt("@_ID", lblID.Text)

                ret = Execute_Command(sql, p)
                If ret = "true" Then
                    Me.Close()

                    Dim f As New frmQuotationCaseByCase
                    f.MdiParent = frmMain
                    f.Show()
                    f.FillInData(lblID.Text, False)
                Else
                    MessageBox.Show("Update Quotation Status Fail", "Overwrite Fail", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Else
                MessageBox.Show(ret, "Overwrite Fail", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
    End Sub
End Class
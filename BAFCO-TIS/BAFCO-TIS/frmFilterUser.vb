﻿Imports System.Data.SqlClient

Public Class frmFilterUser

    Public USER_ID As String = ""

    Private Sub frmFilterUser_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        BindCBB_Status(cbbSearch, True)
        Dim SQL As String = ""
        SQL = "SELECT US_ID,US_CODE,US_NAME,CASE WHEN ACTIVE_STATUS = 1 THEN 'Active' ELSE 'Inactive' END ACTIVE_STATUS FROM [USER]"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        Grid.DataSource = DT
        cbbSearch.SelectedIndex = 1
        Filter()
        txtSearch.Focus()
    End Sub

    Private Sub Grid_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid.CellDoubleClick
        If e.RowIndex < 0 Then Exit Sub
        Me.DialogResult = Windows.Forms.DialogResult.OK
        USER_ID = Grid.Rows(e.RowIndex).Cells("US_ID").Value.ToString
        Me.Close()
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp, cbbSearch.SelectionChangeCommitted
        Filter()
    End Sub

    Sub Filter()
        Dim DT As New DataTable
        DT = Grid.DataSource
        Dim Filter As String = ""
        If txtSearch.Text <> "" Then
            Filter &= "(US_CODE LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' OR US_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%') AND "
        End If
        If cbbSearch.SelectedValue <> 2 Then
            Filter &= "ACTIVE_STATUS = '" & cbbSearch.Text & "' AND "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter
    End Sub
End Class
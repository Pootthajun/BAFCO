﻿Public Class UCMainQuotationSubGroup

    Dim _QTCategoryID As Long = 0
    Dim _QTGroupID As Long = 0

    Public Event SubGroupHeightChange(h As Integer)
    Public Event CloseSubGroup(ctl As UCMainQuotationSubGroup)

    Public WriteOnly Property EnableSubGroup As Boolean
        Set(value As Boolean)
            cbbQTSubGroup.Enabled = value
            txtHeaderDetail.Enabled = value
            'txtFooterNote.Enabled = value
            'txtSubjectRemark.Enabled = value
            UcMainQuotationDescription1.EnableDescription = value
            'UcMainQuotationRemark1.EnableRemark = value
            btnClose.Visible = value
        End Set
    End Property

    Public Sub SetDDLSubGroup(QTGroupID As Long, QTCategoryID As Long)
        _QTCategoryID = QTCategoryID
        _QTGroupID = QTGroupID

        Dim SGrpDt As DataTable
        If QTGroupID <> 0 Then
            Dim sql As String = "select id, subgroup_name"
            sql += " from QT_GROUP_SUB "
            sql += " where active_status='Y'"
            sql += " and qt_group_id=@_GROUP_ID"

            Dim p(1) As SqlClient.SqlParameter
            p(0) = SetBigInt("@_GROUP_ID", QTGroupID)

            SGrpDt = Execute_DataTable(sql, p)

            UcMainQuotationDescription1.QTCategoryID = QTCategoryID
            UcMainQuotationRemark1.QTCategoryID = QTCategoryID
        End If

        If SGrpDt Is Nothing Then
            SGrpDt = New DataTable
            SGrpDt.Columns.Add("id")
            SGrpDt.Columns.Add("subgroup_name")
        End If

        Dim dr As DataRow = SGrpDt.NewRow
        dr("id") = 0
        dr("subgroup_name") = ""
        SGrpDt.Rows.InsertAt(dr, 0)

        cbbQTSubGroup.DataSource = SGrpDt
        cbbQTSubGroup.DisplayMember = "subgroup_name"
        cbbQTSubGroup.ValueMember = "id"
        cbbQTSubGroup.SelectedValue = 0
    End Sub

    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If cbbQTSubGroup.SelectedValue > 0 Then
            If UcMainQuotationDescription1.ValidateData = False Then
                Return False
            End If
            If UcMainQuotationRemark1.ValidateData = False Then
                Return False
            End If
        End If

        Return ret
    End Function

    Public Function SaveMainQuotationSubGroup(CusID As Integer, QtDate As Date, QtNo As String, QtStatus As String, MainQuotationID As Long, trans As SqlClient.SqlTransaction) As String
        Dim ret As String = "false"
        Try
            If cbbQTSubGroup.SelectedValue > 0 Then
                Dim sql As String = ""
                sql = " insert into quotation_main_detail(created_by,created_date,quotation_main_id,"
                sql += " qt_category_id, qt_group_id, qt_group_sub_id,header_detail,footer_detail,subject_remark )"
                sql += " output inserted.id "
                sql += " values(@_CREATED_BY, getdate(), @_QUOTATION_MAIN_ID, @_QT_CATEGORY_ID, @_QT_GROUP_ID, @_QT_GROUP_SUB_ID, "
                sql += " @_HEADER_DETAIL, @_FOOTER_DETAIL, @_SUBJECT_REMARK)"

                Dim p(8) As SqlClient.SqlParameter
                p(0) = SetText("@_CREATED_BY", myUser.fulllname)
                p(1) = SetBigInt("@_QUOTATION_MAIN_ID", MainQuotationID)
                p(2) = SetBigInt("@_QT_CATEGORY_ID", _QTCategoryID)
                p(3) = SetBigInt("@_QT_GROUP_ID", _QTGroupID)
                p(4) = SetBigInt("@_QT_GROUP_SUB_ID", cbbQTSubGroup.SelectedValue)
                p(5) = SetText("@_HEADER_DETAIL", txtHeaderDetail.Text)
                p(6) = SetText("@_FOOTER_DETAIL", txtFooterNote.Text)
                p(7) = SetText("@_SUBJECT_REMARK", txtSubjectRemark.Text)

                Dim dt As DataTable = Execute_DataTable(sql, trans, p)
                If dt.Rows.Count > 0 Then
                    Dim _qtd_id As Long = Convert.ToInt64(dt.Rows(0)("id"))
                    ret = "true"
                    'Subgroup Description
                    If UcMainQuotationDescription1.flpDescription.Controls.Count > 0 Then
                        ret = UcMainQuotationDescription1.SaveMainQuotationDescription(CusID, QtDate, _qtd_id, QtNo, QtStatus, trans)
                    End If

                    If ret = "true" Then
                        'Subgroup Remark
                        If UcMainQuotationRemark1.flpRemark.Controls.Count > 0 Then
                            ret = UcMainQuotationRemark1.SaveMainQuotationRemarks(_qtd_id, trans)
                        End If
                    End If
                End If
                dt.Dispose()
            Else
                ret = "true"
            End If
        Catch ex As Exception
            ret = "false|" & ex.Message
        End Try
        Return ret
    End Function

    Public Sub FillInSubGroupData(MainQuotationDetailID As Long, QTSubGroupID As Long, HeaderDetail As String, FooterDetail As String, SubjectRemark As String)
        cbbQTSubGroup.SelectedValue = QTSubGroupID
        txtHeaderDetail.Text = HeaderDetail
        txtFooterNote.Text = FooterDetail
        txtSubjectRemark.Text = SubjectRemark

        UcMainQuotationDescription1.FillInDescriptionData(MainQuotationDetailID)
        UcMainQuotationRemark1.FillInRemarkData(MainQuotationDetailID)
    End Sub

    Private Sub UcMainQuotationDescription1_HeightChanged(h As Integer) Handles UcMainQuotationDescription1.HeightChanged
        lblLabelNote.Top += h
        lblLabelRemarkSubject.Top += h
        txtFooterNote.Top += h
        txtSubjectRemark.Top += h

        UcMainQuotationRemark1.Top += h
        Panel1.Height += h
        Me.Height += h

        RaiseEvent SubGroupHeightChange(h)
    End Sub

    Private Sub UcMainQuotationRemark1_HeightChanged(h As Integer) Handles UcMainQuotationRemark1.HeightChanged
        Panel1.Height += h
        Me.Height += h

        RaiseEvent SubGroupHeightChange(h)
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs)
        RaiseEvent SubGroupHeightChange(24)

        UcMainQuotationRemark1.Top += 24
        Panel1.Height += 24
        Me.Height += 24
    End Sub

    Private Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        RaiseEvent CloseSubGroup(Me)
    End Sub
End Class

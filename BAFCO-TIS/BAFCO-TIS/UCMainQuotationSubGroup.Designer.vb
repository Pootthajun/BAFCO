﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCMainQuotationSubGroup
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtSubjectRemark = New System.Windows.Forms.TextBox()
        Me.lblLabelRemarkSubject = New System.Windows.Forms.Label()
        Me.txtFooterNote = New System.Windows.Forms.TextBox()
        Me.lblLabelNote = New System.Windows.Forms.Label()
        Me.txtHeaderDetail = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.PictureBox()
        Me.UcMainQuotationRemark1 = New BAFCO_TIS.UCMainQuotationRemark()
        Me.UcMainQuotationDescription1 = New BAFCO_TIS.UCMainQuotationDescription()
        Me.cbbQTSubGroup = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SteelBlue
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.txtSubjectRemark)
        Me.Panel1.Controls.Add(Me.lblLabelRemarkSubject)
        Me.Panel1.Controls.Add(Me.txtFooterNote)
        Me.Panel1.Controls.Add(Me.lblLabelNote)
        Me.Panel1.Controls.Add(Me.txtHeaderDetail)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.UcMainQuotationRemark1)
        Me.Panel1.Controls.Add(Me.UcMainQuotationDescription1)
        Me.Panel1.Controls.Add(Me.cbbQTSubGroup)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(11, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(761, 155)
        Me.Panel1.TabIndex = 68
        '
        'txtSubjectRemark
        '
        Me.txtSubjectRemark.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtSubjectRemark.Location = New System.Drawing.Point(83, 101)
        Me.txtSubjectRemark.MaxLength = 100
        Me.txtSubjectRemark.Name = "txtSubjectRemark"
        Me.txtSubjectRemark.Size = New System.Drawing.Size(616, 20)
        Me.txtSubjectRemark.TabIndex = 3
        '
        'lblLabelRemarkSubject
        '
        Me.lblLabelRemarkSubject.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblLabelRemarkSubject.ForeColor = System.Drawing.Color.White
        Me.lblLabelRemarkSubject.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLabelRemarkSubject.Location = New System.Drawing.Point(3, 99)
        Me.lblLabelRemarkSubject.Name = "lblLabelRemarkSubject"
        Me.lblLabelRemarkSubject.Size = New System.Drawing.Size(62, 22)
        Me.lblLabelRemarkSubject.TabIndex = 138
        Me.lblLabelRemarkSubject.Text = "REMARK"
        Me.lblLabelRemarkSubject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFooterNote
        '
        Me.txtFooterNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtFooterNote.Location = New System.Drawing.Point(83, 78)
        Me.txtFooterNote.MaxLength = 100
        Me.txtFooterNote.Name = "txtFooterNote"
        Me.txtFooterNote.Size = New System.Drawing.Size(616, 20)
        Me.txtFooterNote.TabIndex = 2
        '
        'lblLabelNote
        '
        Me.lblLabelNote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.lblLabelNote.ForeColor = System.Drawing.Color.White
        Me.lblLabelNote.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblLabelNote.Location = New System.Drawing.Point(3, 76)
        Me.lblLabelNote.Name = "lblLabelNote"
        Me.lblLabelNote.Size = New System.Drawing.Size(49, 22)
        Me.lblLabelNote.TabIndex = 136
        Me.lblLabelNote.Text = "NOTE"
        Me.lblLabelNote.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtHeaderDetail
        '
        Me.txtHeaderDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.txtHeaderDetail.Location = New System.Drawing.Point(84, 25)
        Me.txtHeaderDetail.MaxLength = 100
        Me.txtHeaderDetail.Name = "txtHeaderDetail"
        Me.txtHeaderDetail.Size = New System.Drawing.Size(616, 20)
        Me.txtHeaderDetail.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label1.Location = New System.Drawing.Point(3, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 22)
        Me.Label1.TabIndex = 130
        Me.Label1.Text = "DETAIL"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnClose
        '
        Me.btnClose.Image = Global.BAFCO_TIS.My.Resources.Resources.close
        Me.btnClose.Location = New System.Drawing.Point(735, 4)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(20, 20)
        Me.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.btnClose.TabIndex = 83
        Me.btnClose.TabStop = False
        '
        'UcMainQuotationRemark1
        '
        Me.UcMainQuotationRemark1.BackColor = System.Drawing.Color.SteelBlue
        Me.UcMainQuotationRemark1.Location = New System.Drawing.Point(82, 123)
        Me.UcMainQuotationRemark1.Name = "UcMainQuotationRemark1"
        Me.UcMainQuotationRemark1.Size = New System.Drawing.Size(678, 28)
        Me.UcMainQuotationRemark1.TabIndex = 82
        '
        'UcMainQuotationDescription1
        '
        Me.UcMainQuotationDescription1.BackColor = System.Drawing.Color.Silver
        Me.UcMainQuotationDescription1.Location = New System.Drawing.Point(83, 47)
        Me.UcMainQuotationDescription1.Name = "UcMainQuotationDescription1"
        Me.UcMainQuotationDescription1.Size = New System.Drawing.Size(675, 28)
        Me.UcMainQuotationDescription1.TabIndex = 81
        '
        'cbbQTSubGroup
        '
        Me.cbbQTSubGroup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbbQTSubGroup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbQTSubGroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.cbbQTSubGroup.Location = New System.Drawing.Point(84, 2)
        Me.cbbQTSubGroup.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbQTSubGroup.Name = "cbbQTSubGroup"
        Me.cbbQTSubGroup.Size = New System.Drawing.Size(616, 21)
        Me.cbbQTSubGroup.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Bold)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 28)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "SUB GROUP"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Red
        Me.Label25.Location = New System.Drawing.Point(-5, 8)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(12, 13)
        Me.Label25.TabIndex = 134
        Me.Label25.Text = "*"
        '
        'UCMainQuotationSubGroup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "UCMainQuotationSubGroup"
        Me.Size = New System.Drawing.Size(775, 160)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.btnClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cbbQTSubGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UcMainQuotationDescription1 As BAFCO_TIS.UCMainQuotationDescription
    Friend WithEvents UcMainQuotationRemark1 As BAFCO_TIS.UCMainQuotationRemark
    Friend WithEvents btnClose As System.Windows.Forms.PictureBox
    Friend WithEvents txtHeaderDetail As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSubjectRemark As System.Windows.Forms.TextBox
    Friend WithEvents lblLabelRemarkSubject As System.Windows.Forms.Label
    Friend WithEvents txtFooterNote As System.Windows.Forms.TextBox
    Friend WithEvents lblLabelNote As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label

End Class

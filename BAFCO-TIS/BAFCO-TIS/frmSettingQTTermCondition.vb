﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmSettingQTTermCondition

    Dim DA As New SqlDataAdapter

    Private Sub frmSettingQTTermCondition_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        txtSearch.Text = ""
        rdiSearchAll.Checked = True
        cbbCategory.SelectedValue = 0
        ShowData()
        Filter()
    End Sub

    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT r.id, r.remarks, r.active_status,r.qt_category_id, c.category_name, r.user_by_case_qt,r.default_bycase "
        SQL += " FROM QT_CATEGORY_REMARK r"
        SQL += " inner join QT_CATEGORY c on c.id=r.qt_category_id "
        SQL += "  ORDER BY c.category_name,r.remarks"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT

        SQL = "SELECT id,category_name FROM qt_category where active_status='Y' ORDER BY category_name"
        Dim DT_CAT As DataTable = Execute_DataTable(SQL)
        Dim dr_cat As DataRow = DT_CAT.NewRow
        dr_cat("id") = 0
        dr_cat("category_name") = ""
        DT_CAT.Rows.InsertAt(dr_cat, 0)


        Dim REM_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        REM_ID.Name = "REM_ID"
        REM_ID.DataPropertyName = "id"
        REM_ID.Visible = False
        grdMain.Columns.Add(REM_ID)
        grdMain.Columns("REM_ID").Visible = False

        Dim REM_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        REM_NAME.DataPropertyName = "remarks"
        REM_NAME.HeaderText = "Term & Conditions"
        REM_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        REM_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(REM_NAME)

        Dim CAT_ID As New System.Windows.Forms.DataGridViewComboBoxColumn
        CAT_ID.DataSource = DT_CAT
        CAT_ID.DisplayMember = "category_name"
        CAT_ID.ValueMember = "id"
        CAT_ID.DataPropertyName = "qt_category_id"
        CAT_ID.HeaderText = "Category"
        CAT_ID.Width = 300
        grdMain.Columns.Add(CAT_ID)

        Dim BYCASE As New System.Windows.Forms.DataGridViewCheckBoxColumn
        BYCASE.DataPropertyName = "user_by_case_qt"
        BYCASE.TrueValue = "Y"
        BYCASE.FalseValue = "N"
        BYCASE.HeaderText = "Case By Case QT"
        BYCASE.Width = 100
        grdMain.Columns.Add(BYCASE)

        Dim FLD_DEFAULT As New System.Windows.Forms.DataGridViewCheckBoxColumn
        FLD_DEFAULT.DataPropertyName = "default_bycase"
        FLD_DEFAULT.TrueValue = "Y"
        FLD_DEFAULT.FalseValue = "N"
        FLD_DEFAULT.HeaderText = "Default"
        FLD_DEFAULT.Width = 100
        grdMain.Columns.Add(FLD_DEFAULT)

        Dim CAT_ACTIVE As New System.Windows.Forms.DataGridViewCheckBoxColumn
        CAT_ACTIVE.DataPropertyName = "active_status"
        CAT_ACTIVE.TrueValue = "Y"
        CAT_ACTIVE.FalseValue = "N"
        CAT_ACTIVE.HeaderText = "Active"
        CAT_ACTIVE.Width = 100
        grdMain.Columns.Add(CAT_ACTIVE)

        Dim DT_SearchCat As New DataTable
        DT_SearchCat = DT_CAT.Copy
        cbbCategory.DisplayMember = "category_name"
        cbbCategory.ValueMember = "id"
        cbbCategory.DataSource = DT_SearchCat
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("REMARKS")) OrElse tmp.Rows(i).Item("REMARKS") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("QT_CATEGORY_ID")) OrElse tmp.Rows(i).Item("QT_CATEGORY_ID").ToString = "0" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "REMARKS='" & tmp.Rows(i).Item("REMARKS").ToString.Replace("'", "''") & "' and QT_CATEGORY_ID='" & tmp(i)("QT_CATEGORY_ID") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next

        '------------------Batch Save---------------
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction

        Dim ret As String = "false"
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim sql As String = ""

            Dim p(7) As SqlParameter
            If DT.Rows(i)("id") > 0 Then
                sql = "update qt_category_remark"
                sql += " set remarks=@_REMARKS"
                sql += ", qt_category_id=@_CATEGORY_ID"
                sql += ", user_by_case_qt=@_USE_BY_CASE_QT"
                sql += ", default_bycase=@_DEFAULT_BYCASE"
                sql += ", active_status=@_ACTIVE_STATUS "
                sql += ", updated_by=@_UPDATED_BY "
                sql += ", updated_date=getdate()"
                sql += " where id=@_ID"

                p(0) = SetBigInt("@_ID", DT.Rows(i)("id"))
                p(1) = SetText("@_UPDATED_BY", myUser.us_code)
            Else
                sql = "insert into qt_category_remark(created_by, created_date, remarks, qt_category_id, user_by_case_qt, default_bycase,  active_status)"
                sql += " values(@_CREATED_BY, getdate(), @_REMARKS, @_CATEGORY_ID, @_USE_BY_CASE_QT, @_DEFAULT_BYCASE,  @_ACTIVE_STATUS) "

                p(0) = SetText("@_CREATED_BY", myUser.us_code)
            End If

            p(2) = SetText("@_REMARKS", DT.Rows(i)("remarks"))
            p(3) = SetBigInt("@_CATEGORY_ID", DT.Rows(i)("qt_category_id"))
            p(4) = SetText("@_ACTIVE_STATUS", IIf(DT.Rows(i)("active_status") = "Y", "Y", "N"))
            p(5) = SetText("@_USE_BY_CASE_QT", IIf(DT.Rows(i)("user_by_case_qt") = "Y", "Y", "N"))
            p(6) = SetText("@_DEFAULT_BYCASE", IIf(DT.Rows(i)("default_bycase") = "Y", "Y", "N"))

            ret = Execute_Command(sql, trans, p)
            If ret.ToLower <> "true" Then
                Exit For
            End If
        Next

        If ret.ToLower = "true" Then
            trans.Commit()

            txtSearch.Text = ""
            rdiSearchAll.Checked = True
            frmSettingQTTermCondition_Load(Nothing, Nothing)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            trans.Rollback()
            MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DR("id") = 0
        DR("qt_category_id") = 0
        DR("active_status") = "Y"

        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_KeyUp(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        Filter()
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        Filter()
    End Sub

    Private Sub cbbCategory_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbbCategory.SelectedIndexChanged
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        If grdMain.DataSource Is Nothing Then Exit Sub
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= " ID = 0 OR  "
        End If
        Filter &= "REMARKS LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' AND "
        If cbbCategory.SelectedIndex > 0 Then
            Filter += "QT_CATEGORY_ID = " & cbbCategory.SelectedValue & " AND "
        End If

        If rdiSearchActive.Checked = True Then
            Filter += " active_status='Y' AND "
        End If
        If rdiSearchInactive.Checked = True Then
            Filter += " active_status='N' and "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter

        lblRec.Text = "Total   " & DT.DefaultView.Count & "   Record"
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click

        frmSettingQTTermCondition_Load(Nothing, Nothing)
    End Sub

End Class
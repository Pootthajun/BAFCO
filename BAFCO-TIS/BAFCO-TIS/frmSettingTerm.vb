﻿Imports System.Data
Imports System.Data.SqlClient
Public Class frmSettingTerm

    Private Sub frmTerm_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        ShowData(txtSearch.Text)
    End Sub

    Function GetData(txtSearch As String) As DataTable
        Dim da As New SqlDataAdapter
        Dim sql As String = ""
        sql = "select id,term_name,active_status from MS_TERM where 1=1 "
        If txtSearch <> "" Then
            sql += " and term_name like '%" & txtSearch.Replace("'", "''") & "%'"
        End If
        If rdiSearchActive.Checked = True Then
            sql += " and active_status='Y'"
        End If
        If rdiSearchInactive.Checked = True Then
            sql += " and active_status='N'"
        End If

        sql += " order by term_name"
        da = New SqlDataAdapter(sql, ConnStr)
        Dim dt As New DataTable
        da.Fill(dt)
        Return dt
    End Function

    Private Sub ShowData(txtSearch As String)
        Dim dt As DataTable = GetData(txtSearch)
        grdMain.DataSource = DT
        lblRec.Text = "Total   " & dt.Rows.Count & "   Record"

        grdMain.Columns.Clear()
        Dim commodity_id As New System.Windows.Forms.DataGridViewTextBoxColumn
        commodity_id.Name = "id"
        commodity_id.DataPropertyName = "id"
        commodity_id.Visible = False
        grdMain.Columns.Add(commodity_id)
        grdMain.Columns("id").Visible = False

        Dim term_name As New System.Windows.Forms.DataGridViewTextBoxColumn
        term_name.DataPropertyName = "term_name"
        term_name.HeaderText = "Term Name"
        term_name.SortMode = DataGridViewColumnSortMode.NotSortable
        term_name.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(term_name)

        Dim active_status As New System.Windows.Forms.DataGridViewCheckBoxColumn()
        active_status.DataPropertyName = "active_status"
        active_status.TrueValue = "Y"
        active_status.FalseValue = "N"
        active_status.HeaderText = "Active"
        active_status.Width = 100
        grdMain.Columns.Add(active_status)

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dt As DataTable = grdMain.DataSource
        For i As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(dt.Rows(i).Item("term_name")) OrElse dt.Rows(i).Item("term_name") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            Dim tmpdr As DataRow() = dt.Select("term_name = '" & dt.Rows(i).Item("term_name").ToString.Replace("'", "''") & "'")
            If tmpdr.Length > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        '== Save ==

        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction

        Dim ret As String = "false"
        Try
            Dim tmpedit As DataRow() = dt.Select("id <> 0")
            '== ลบ ==
            Dim _id As String = ""
            For i As Int32 = 0 To tmpedit.Length - 1
                Application.DoEvents()
                _id &= tmpedit(i)("id").ToString
                If i < tmpedit.Length - 1 Then
                    _id &= ","
                End If
            Next
            Dim sql As String = ""
            If _id <> "" Then
                sql = "delete from MS_TERM where id not in (" & _id & ")"
                ret = Execute_Command(sql, trans, Nothing)
                If ret <> "true" Then
                    trans.Rollback()
                    MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            End If
            

            '=== แก้ไข
            For i As Int32 = 0 To tmpedit.Length - 1
                Application.DoEvents()
                Dim _commodity_id As String = tmpedit(i)("id").ToString
                Dim _term_name As String = tmpedit(i)("term_name").ToString.Replace("'", "''")
                Dim _active_status As String = tmpedit(i)("active_status").ToString

                sql = "Update MS_TERM set term_name = @_term_name,active_status = @_active_status,updated_date = getdate(),"
                sql += " updated_by= @_updated_by where id = @_id"
                Dim p_e(4) As SqlParameter
                p_e(0) = SetText("@_term_name", _term_name)
                p_e(1) = SetText("@_active_status", _active_status)
                p_e(2) = SetText("@_updated_by", myUser.us_code)
                p_e(3) = SetText("@_ID", _commodity_id)
                ret = Execute_Command(sql, trans, p_e)
                If ret <> "true" Then
                    trans.Rollback()
                    MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            Next

            '== เพิ่ม
            Dim tmpadd As DataRow() = dt.Select("id = 0")
            For i As Int32 = 0 To tmpadd.Length - 1
                Application.DoEvents()
                Dim _term_name As String = tmpadd(i)("term_name").ToString.Replace("'", "''")
                Dim _active_status As String = tmpadd(i)("active_status").ToString

                sql = "Insert Into MS_TERM(term_name,active_status,created_date,created_by)"
                sql &= " Values(@_term_name,@_active_status,getdate(),@_create_by)"
                Dim p_i(3) As SqlParameter
                p_i(0) = SetText("@_term_name", _term_name)
                p_i(1) = SetText("@_active_status", _active_status)
                p_i(2) = SetText("@_create_by", myUser.us_code)
                ret = Execute_Command(sql, trans, p_i)
                If ret <> "true" Then
                    trans.Rollback()
                    MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Exit Sub
                End If
            Next

            trans.Commit()
            conn.Close()
            '== End Save

            txtSearch.Text = ""
            ShowData(txtSearch.Text)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            trans.Rollback()
            If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
                ShowData(txtSearch.Text)
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim dt As DataTable = grdMain.DataSource
        Dim dr As DataRow = dt.NewRow
        dr("id") = "0"
        dr("term_name") = ""
        dr("active_status") = "Y"
        dt.Rows.Add(dr)
        grdMain.DataSource = dt
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        ShowData(txtSearch.Text)
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        ShowData(txtSearch.Text)
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        ShowData(txtSearch.Text)
    End Sub
End Class
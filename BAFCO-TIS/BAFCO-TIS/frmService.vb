﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmService

    Dim DA As New SqlDataAdapter

    Private Sub frmService_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        ShowData()
    End Sub

    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT * FROM SERVICE ORDER BY SER_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT
        lblRec.Text = "Total   " & DT.Rows.Count & "   Record"

        SQL = "SELECT SER_TYPE_ID,SER_TYPE_NAME FROM SERVICE_TYPE ORDER BY SER_TYPE_NAME"
        Dim DT_TYPE As New DataTable
        Dim DA_TYPE As New SqlDataAdapter(SQL, ConnStr)
        DA_TYPE.Fill(DT_TYPE)

        Dim SER_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        SER_ID.Name = "SER_ID"
        SER_ID.DataPropertyName = "SER_ID"
        SER_ID.Visible = False
        grdMain.Columns.Add(SER_ID)
        grdMain.Columns("SER_ID").Visible = False

        Dim SER_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        SER_NAME.DataPropertyName = "SER_NAME"
        SER_NAME.HeaderText = "Service"
        SER_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        SER_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(SER_NAME)

        Dim SER_TYPE_ID As New System.Windows.Forms.DataGridViewComboBoxColumn()
        SER_TYPE_ID.DataSource = DT_TYPE
        SER_TYPE_ID.DisplayMember = "SER_TYPE_NAME"
        SER_TYPE_ID.ValueMember = "SER_TYPE_ID"
        SER_TYPE_ID.DataPropertyName = "SER_TYPE_ID"
        SER_TYPE_ID.HeaderText = "Service Type"
        SER_TYPE_ID.Width = 220
        grdMain.Columns.Add(SER_TYPE_ID)

        Dim UPDATE_BY As New System.Windows.Forms.DataGridViewTextBoxColumn
        UPDATE_BY.DataPropertyName = "UPDATE_BY"
        UPDATE_BY.Visible = False
        grdMain.Columns.Add(UPDATE_BY)

        Dim UPDATE_DATE As New System.Windows.Forms.DataGridViewTextBoxColumn
        UPDATE_DATE.DataPropertyName = "UPDATE_DATE"
        UPDATE_DATE.Visible = False
        grdMain.Columns.Add(UPDATE_DATE)

        SQL = "SELECT SER_TYPE_ID,SER_TYPE_NAME FROM SERVICE_TYPE ORDER BY SER_TYPE_NAME"
        Dim DT_SER_TYPE As New DataTable
        Dim DA_SER_TYPE As New SqlDataAdapter(SQL, ConnStr)
        DA_SER_TYPE.Fill(DT_SER_TYPE)
        Dim DR As DataRow
        DR = DT_SER_TYPE.NewRow
        DT_SER_TYPE.Rows.Add(DR)
        DT_SER_TYPE.DefaultView.Sort = "SER_TYPE_NAME ASC"
        cbbSupType.DataSource = DT_SER_TYPE
        cbbSupType.DisplayMember = "SER_TYPE_NAME"
        cbbSupType.ValueMember = "SER_TYPE_ID"

    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("SER_NAME")) OrElse tmp.Rows(i).Item("SER_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("SER_TYPE_ID")) OrElse tmp.Rows(i).Item("SER_TYPE_ID").ToString = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "SER_NAME='" & Replace(tmp.Rows(i).Item("SER_NAME").ToString, "'", "''") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        Next

        '------------------Check Deleted Record-----
        Dim SQL As String = ""
        SQL &= "SELECT DISTINCT * FROM (" & vbCrLf
        SQL &= "SELECT DISTINCT SER_ID FROM SERVICE_CUS" & vbCrLf
        SQL &= "UNION ALL" & vbCrLf
        SQL &= "SELECT DISTINCT SER_ID FROM SERVICE_SUP" & vbCrLf
        SQL &= ") AS TB" & vbCrLf
        Dim TA As New SqlDataAdapter(SQL, ConnStr)
        Dim TT As New DataTable
        TA.Fill(TT)
        For i As Integer = 0 To TT.Rows.Count - 1
            If IsDBNull(TT.Rows(i).Item("SER_ID")) Then Continue For
            tmp.DefaultView.RowFilter = "SER_ID=" & TT.Rows(i).Item("SER_ID")
            If tmp.DefaultView.Count = 0 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลที่คุณต้องการลบ ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ShowData()
                Exit Sub
            End If
        Next

        '------------------Batch Save---------------
        Dim ID As Integer = 0
        ID = GetNewID("SERVICE", "SER_ID")
        For i As Integer = 0 To DT.Rows.Count - 1
            If DT.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(DT.Rows(i).Item("SER_ID")) Then
                DT.Rows(i).Item("SER_ID") = ID
                ID = ID + 1
                DT.Rows(i).Item("UPDATE_BY") = myUser.user_id
                DT.Rows(i).Item("UPDATE_DATE") = Now
            End If

        Next

        Try
            Dim CMD As New SqlCommandBuilder(DA)
            DA.Update(DT)
            DT.AcceptChanges()
        Catch ex As Exception
            If ex.Message.ToUpper.IndexOf("CONSTRAIN") > -1 Then
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & "ข้อมูลนี้ถูกใช้งานอยู่", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else
                MessageBox.Show("ไม่สามารถบันทึกได้" & vbNewLine & vbNewLine & ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            Exit Sub
        End Try

        txtSearch.Text = ""
        cbbSupType.SelectedIndex = 0
        ShowData()
        MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp, cbbSupType.SelectionChangeCommitted
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= "SER_ID IS NULL OR  "
        End If
        Filter &= "SER_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' AND "
        If cbbSupType.Text <> "" Then
            Filter &= "SER_TYPE_ID = " & cbbSupType.SelectedValue & " AND "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click
        frmService_Load(Nothing, Nothing)
    End Sub
End Class
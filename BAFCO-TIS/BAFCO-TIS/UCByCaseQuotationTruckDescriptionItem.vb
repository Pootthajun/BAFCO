﻿Public Class UCByCaseQuotationTruckDescriptionItem

    Public Event DeleteTruckItem(ctl As UCByCaseQuotationTruckDescriptionItem)
    Public Event PriceTotalChange()
    'Public Event SelectDescriptionItem(ddlDescription As ComboBox, txtPrice As TextBox, ddlBasis As ComboBox, ddlCurrency As ComboBox)

    Public WriteOnly Property EnableDescriptionItem As Boolean
        Set(value As Boolean)
            cbbRouteFrom.Enabled = value
            cbbRouteTo.Enabled = value
            cbbVehicleType.Enabled = value
            txtUnitRate.Enabled = value
            cbbBasis.Enabled = value
            txtQty.Enabled = value
            cbbCurrency.Enabled = value
            btnNote.Visible = value
            btnDelete.Visible = value
        End Set
    End Property


    Public Sub SetDDL()
        BindCBB_Route(cbbRouteFrom)
        BindCBB_Route(cbbRouteTo)
        BindCBB_Vehicle(cbbVehicleType)

        SetDDLBasis()
        SetDDLCurrency()

    End Sub

    Private Sub SetDDLBasis()
        Dim sql As String = "select id, basis_name from MS_BASIS where active_status='Y' order by basis_name"
        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("basis_name")
        'End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("basis_name") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbBasis.DataSource = dt
        cbbBasis.DisplayMember = "basis_name"
        cbbBasis.ValueMember = "id"
    End Sub

    Private Sub SetDDLCurrency()
        Dim sql As String = "select id, currency_name from MS_CURRENCY where active_status='Y' order by currency_name"
        Dim dt As DataTable = Execute_DataTable(sql)
        'If dt.Rows.Count = 0 Then
        '    dt.Columns.Add("id")
        '    dt.Columns.Add("currency_name")
        'End If

        cbbCurrency.DataSource = dt
        cbbCurrency.DisplayMember = "currency_name"
        cbbCurrency.ValueMember = "id"

        If dt.Rows.Count > 0 Then
            cbbCurrency.SelectedValue = 1
        End If
    End Sub
    

    Private Sub txtPrice_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUnitRate.KeyPress, txtQty.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        RaiseEvent DeleteTruckItem(Me)
    End Sub

    Private Sub txtUnitRate_TextChanged(sender As Object, e As System.EventArgs) Handles txtUnitRate.TextChanged, txtQty.TextChanged
        If txtUnitRate.Text <> "" And txtQty.Text <> "" Then
            txtTotal.Text = (Convert.ToDouble(txtUnitRate.Text) * Convert.ToInt64(txtQty.Text)).ToString("#,##0.00")
            If cbbCurrency.SelectedValue > 0 Then
                RaiseEvent PriceTotalChange()
            End If
        End If
    End Sub

    Private Sub cbbCurrency_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbbCurrency.SelectedIndexChanged
        If txtUnitRate.Text <> "" And txtQty.Text <> "" Then
            If cbbCurrency.SelectedValue > 0 Then
                RaiseEvent PriceTotalChange()
            End If
        End If
    End Sub

    Private Sub btnNote_Click(sender As Object, e As System.EventArgs) Handles btnNote.Click
        Dim frm As New frmDialogItemNote
        frm.txtNote.Text = lblNote.Text

        If frm.ShowDialog = DialogResult.OK Then
            lblNote.Text = frm.txtNote.Text
            If lblNote.Text.Trim <> "" Then
                btnNote.BackgroundImage = My.Resources.Edit
            Else
                btnNote.BackgroundImage = My.Resources.BlankNote
            End If
        End If
    End Sub
End Class

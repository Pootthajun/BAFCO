﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.sfd = New System.Windows.Forms.SaveFileDialog()
        Me.dlMenu = New MBMenuStrip.MBMenuStrip()
        Me.CostInformationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTransportCost = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuServiceCharge = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuImportData = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImportCost = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImportService = New System.Windows.Forms.ToolStripMenuItem()
        Me.DownloadFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileImportCost = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFileImportService = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuotationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMainQuotation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCaseByCaseQuotation = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLocation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSupplierType = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSupplier = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCustomerType = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCustomer = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuServiceType = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuService = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuVehicle = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSettingCountry = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuBasis = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuCreditTerm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuMode = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuTerm = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuCurrency = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuSettingQuotation = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuSettingQuotationCategory = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuSettingQuotationGroup = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuSettingQuotationSubGroup = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuSettingQuotationDescription = New System.Windows.Forms.ToolStripMenuItem()
        Me.mmuSettingQuotationTermConditions = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDbConfig = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuLogout = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuQuotationScope = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCustomerScope = New System.Windows.Forms.ToolStripMenuItem()
        Me.dlMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'dlMenu
        '
        Me.dlMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CostInformationToolStripMenuItem, Me.QuotationToolStripMenuItem, Me.SettingToolStripMenuItem, Me.mnuLogout})
        Me.dlMenu.Location = New System.Drawing.Point(0, 0)
        Me.dlMenu.Name = "dlMenu"
        Me.dlMenu.Size = New System.Drawing.Size(907, 24)
        Me.dlMenu.TabIndex = 21
        Me.dlMenu.Text = "MbMenuStrip1"
        '
        'CostInformationToolStripMenuItem
        '
        Me.CostInformationToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuTransportCost, Me.mnuServiceCharge, Me.ToolStripSeparator2, Me.mnuImportData, Me.DownloadFileToolStripMenuItem})
        Me.CostInformationToolStripMenuItem.Name = "CostInformationToolStripMenuItem"
        Me.CostInformationToolStripMenuItem.Size = New System.Drawing.Size(109, 20)
        Me.CostInformationToolStripMenuItem.Text = "Cost Information"
        '
        'mnuTransportCost
        '
        Me.mnuTransportCost.Image = Global.BAFCO_TIS.My.Resources.Resources.Vehicle
        Me.mnuTransportCost.Name = "mnuTransportCost"
        Me.mnuTransportCost.Size = New System.Drawing.Size(179, 22)
        Me.mnuTransportCost.Text = "Transportation Cost"
        '
        'mnuServiceCharge
        '
        Me.mnuServiceCharge.Image = Global.BAFCO_TIS.My.Resources.Resources.Service_Charge
        Me.mnuServiceCharge.Name = "mnuServiceCharge"
        Me.mnuServiceCharge.Size = New System.Drawing.Size(179, 22)
        Me.mnuServiceCharge.Text = "Service Charge"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(176, 6)
        '
        'mnuImportData
        '
        Me.mnuImportData.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuImportCost, Me.mnuImportService})
        Me.mnuImportData.Image = Global.BAFCO_TIS.My.Resources.Resources.ExcelImport
        Me.mnuImportData.Name = "mnuImportData"
        Me.mnuImportData.Size = New System.Drawing.Size(179, 22)
        Me.mnuImportData.Text = "Import Data"
        '
        'mnuImportCost
        '
        Me.mnuImportCost.Image = Global.BAFCO_TIS.My.Resources.Resources.Excel
        Me.mnuImportCost.Name = "mnuImportCost"
        Me.mnuImportCost.Size = New System.Drawing.Size(182, 22)
        Me.mnuImportCost.Text = "Transportation  Cost"
        '
        'mnuImportService
        '
        Me.mnuImportService.Image = Global.BAFCO_TIS.My.Resources.Resources.Excel
        Me.mnuImportService.Name = "mnuImportService"
        Me.mnuImportService.Size = New System.Drawing.Size(182, 22)
        Me.mnuImportService.Text = "Service Cost"
        '
        'DownloadFileToolStripMenuItem
        '
        Me.DownloadFileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFileImportCost, Me.mnuFileImportService})
        Me.DownloadFileToolStripMenuItem.Image = Global.BAFCO_TIS.My.Resources.Resources.ExcelMaster
        Me.DownloadFileToolStripMenuItem.Name = "DownloadFileToolStripMenuItem"
        Me.DownloadFileToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.DownloadFileToolStripMenuItem.Text = "Download File"
        '
        'mnuFileImportCost
        '
        Me.mnuFileImportCost.Image = Global.BAFCO_TIS.My.Resources.Resources.ExcelMaster
        Me.mnuFileImportCost.Name = "mnuFileImportCost"
        Me.mnuFileImportCost.Size = New System.Drawing.Size(182, 22)
        Me.mnuFileImportCost.Text = "Transportation  Cost"
        '
        'mnuFileImportService
        '
        Me.mnuFileImportService.Image = Global.BAFCO_TIS.My.Resources.Resources.ExcelMaster
        Me.mnuFileImportService.Name = "mnuFileImportService"
        Me.mnuFileImportService.Size = New System.Drawing.Size(182, 22)
        Me.mnuFileImportService.Text = "Service Cost"
        '
        'QuotationToolStripMenuItem
        '
        Me.QuotationToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuMainQuotation, Me.mnuCaseByCaseQuotation})
        Me.QuotationToolStripMenuItem.Name = "QuotationToolStripMenuItem"
        Me.QuotationToolStripMenuItem.Size = New System.Drawing.Size(73, 20)
        Me.QuotationToolStripMenuItem.Text = "Quotation"
        '
        'mnuMainQuotation
        '
        Me.mnuMainQuotation.Image = Global.BAFCO_TIS.My.Resources.Resources.icon_quotation
        Me.mnuMainQuotation.Name = "mnuMainQuotation"
        Me.mnuMainQuotation.Size = New System.Drawing.Size(200, 22)
        Me.mnuMainQuotation.Text = "Main Quotaion"
        '
        'mnuCaseByCaseQuotation
        '
        Me.mnuCaseByCaseQuotation.Image = Global.BAFCO_TIS.My.Resources.Resources.icon_quotation
        Me.mnuCaseByCaseQuotation.Name = "mnuCaseByCaseQuotation"
        Me.mnuCaseByCaseQuotation.Size = New System.Drawing.Size(200, 22)
        Me.mnuCaseByCaseQuotation.Text = "Case By Case Quotation"
        '
        'SettingToolStripMenuItem
        '
        Me.SettingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUser, Me.mnuLocation, Me.mnuSupplierType, Me.mnuSupplier, Me.mnuCustomerType, Me.mnuCustomerScope, Me.mnuCustomer, Me.mnuServiceType, Me.mnuService, Me.mnuVehicle, Me.mnuSettingCountry, Me.mmuBasis, Me.mmuCreditTerm, Me.mmuMode, Me.mmuTerm, Me.mmuCurrency, Me.mmuSettingQuotation, Me.ToolStripSeparator1, Me.mnuDbConfig})
        Me.SettingToolStripMenuItem.Name = "SettingToolStripMenuItem"
        Me.SettingToolStripMenuItem.Size = New System.Drawing.Size(56, 20)
        Me.SettingToolStripMenuItem.Text = "Setting"
        '
        'mnuUser
        '
        Me.mnuUser.Image = Global.BAFCO_TIS.My.Resources.Resources.User
        Me.mnuUser.Name = "mnuUser"
        Me.mnuUser.Size = New System.Drawing.Size(162, 22)
        Me.mnuUser.Text = "User"
        '
        'mnuLocation
        '
        Me.mnuLocation.Image = Global.BAFCO_TIS.My.Resources.Resources.Route
        Me.mnuLocation.Name = "mnuLocation"
        Me.mnuLocation.Size = New System.Drawing.Size(162, 22)
        Me.mnuLocation.Text = "Location"
        '
        'mnuSupplierType
        '
        Me.mnuSupplierType.Image = Global.BAFCO_TIS.My.Resources.Resources.Supplier_Type
        Me.mnuSupplierType.Name = "mnuSupplierType"
        Me.mnuSupplierType.Size = New System.Drawing.Size(162, 22)
        Me.mnuSupplierType.Text = "Supplier Group"
        '
        'mnuSupplier
        '
        Me.mnuSupplier.Image = Global.BAFCO_TIS.My.Resources.Resources.Supplier
        Me.mnuSupplier.Name = "mnuSupplier"
        Me.mnuSupplier.Size = New System.Drawing.Size(162, 22)
        Me.mnuSupplier.Text = "Supplier"
        '
        'mnuCustomerType
        '
        Me.mnuCustomerType.Image = Global.BAFCO_TIS.My.Resources.Resources.Customer_Type
        Me.mnuCustomerType.Name = "mnuCustomerType"
        Me.mnuCustomerType.Size = New System.Drawing.Size(162, 22)
        Me.mnuCustomerType.Text = "Customer Group"
        '
        'mnuCustomer
        '
        Me.mnuCustomer.Image = Global.BAFCO_TIS.My.Resources.Resources.Customer
        Me.mnuCustomer.Name = "mnuCustomer"
        Me.mnuCustomer.Size = New System.Drawing.Size(162, 22)
        Me.mnuCustomer.Text = "Customer"
        '
        'mnuServiceType
        '
        Me.mnuServiceType.Image = Global.BAFCO_TIS.My.Resources.Resources.Service_Type
        Me.mnuServiceType.Name = "mnuServiceType"
        Me.mnuServiceType.Size = New System.Drawing.Size(162, 22)
        Me.mnuServiceType.Text = "Service Type"
        '
        'mnuService
        '
        Me.mnuService.Image = Global.BAFCO_TIS.My.Resources.Resources.Service_Charge
        Me.mnuService.Name = "mnuService"
        Me.mnuService.Size = New System.Drawing.Size(162, 22)
        Me.mnuService.Text = "Service"
        '
        'mnuVehicle
        '
        Me.mnuVehicle.Image = Global.BAFCO_TIS.My.Resources.Resources.Vehicle
        Me.mnuVehicle.Name = "mnuVehicle"
        Me.mnuVehicle.Size = New System.Drawing.Size(162, 22)
        Me.mnuVehicle.Text = "Vehicle"
        '
        'mnuSettingCountry
        '
        Me.mnuSettingCountry.Name = "mnuSettingCountry"
        Me.mnuSettingCountry.Size = New System.Drawing.Size(162, 22)
        Me.mnuSettingCountry.Text = "Country"
        '
        'mmuBasis
        '
        Me.mmuBasis.Name = "mmuBasis"
        Me.mmuBasis.Size = New System.Drawing.Size(162, 22)
        Me.mmuBasis.Text = "Basis"
        '
        'mmuCreditTerm
        '
        Me.mmuCreditTerm.Name = "mmuCreditTerm"
        Me.mmuCreditTerm.Size = New System.Drawing.Size(162, 22)
        Me.mmuCreditTerm.Text = "Credit Term"
        '
        'mmuMode
        '
        Me.mmuMode.Name = "mmuMode"
        Me.mmuMode.Size = New System.Drawing.Size(162, 22)
        Me.mmuMode.Text = "Mode"
        '
        'mmuTerm
        '
        Me.mmuTerm.Name = "mmuTerm"
        Me.mmuTerm.Size = New System.Drawing.Size(162, 22)
        Me.mmuTerm.Text = "Term"
        '
        'mmuCurrency
        '
        Me.mmuCurrency.Name = "mmuCurrency"
        Me.mmuCurrency.Size = New System.Drawing.Size(162, 22)
        Me.mmuCurrency.Text = "Currency"
        '
        'mmuSettingQuotation
        '
        Me.mmuSettingQuotation.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mmuSettingQuotationCategory, Me.mmuSettingQuotationGroup, Me.mmuSettingQuotationSubGroup, Me.mmuSettingQuotationDescription, Me.mmuSettingQuotationTermConditions, Me.mnuQuotationScope})
        Me.mmuSettingQuotation.Image = Global.BAFCO_TIS.My.Resources.Resources.icon_quotation
        Me.mmuSettingQuotation.Name = "mmuSettingQuotation"
        Me.mmuSettingQuotation.Size = New System.Drawing.Size(162, 22)
        Me.mmuSettingQuotation.Text = "Quotation"
        '
        'mmuSettingQuotationCategory
        '
        Me.mmuSettingQuotationCategory.Name = "mmuSettingQuotationCategory"
        Me.mmuSettingQuotationCategory.Size = New System.Drawing.Size(176, 22)
        Me.mmuSettingQuotationCategory.Text = "Category"
        '
        'mmuSettingQuotationGroup
        '
        Me.mmuSettingQuotationGroup.Name = "mmuSettingQuotationGroup"
        Me.mmuSettingQuotationGroup.Size = New System.Drawing.Size(176, 22)
        Me.mmuSettingQuotationGroup.Text = "Group"
        '
        'mmuSettingQuotationSubGroup
        '
        Me.mmuSettingQuotationSubGroup.Name = "mmuSettingQuotationSubGroup"
        Me.mmuSettingQuotationSubGroup.Size = New System.Drawing.Size(176, 22)
        Me.mmuSettingQuotationSubGroup.Text = "Sub Group"
        '
        'mmuSettingQuotationDescription
        '
        Me.mmuSettingQuotationDescription.Name = "mmuSettingQuotationDescription"
        Me.mmuSettingQuotationDescription.Size = New System.Drawing.Size(176, 22)
        Me.mmuSettingQuotationDescription.Text = "Description"
        '
        'mmuSettingQuotationTermConditions
        '
        Me.mmuSettingQuotationTermConditions.Name = "mmuSettingQuotationTermConditions"
        Me.mmuSettingQuotationTermConditions.Size = New System.Drawing.Size(176, 22)
        Me.mmuSettingQuotationTermConditions.Text = "Term && Conditions"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(159, 6)
        '
        'mnuDbConfig
        '
        Me.mnuDbConfig.Image = Global.BAFCO_TIS.My.Resources.Resources.Databaase
        Me.mnuDbConfig.Name = "mnuDbConfig"
        Me.mnuDbConfig.Size = New System.Drawing.Size(162, 22)
        Me.mnuDbConfig.Text = "Database Config"
        '
        'mnuLogout
        '
        Me.mnuLogout.Name = "mnuLogout"
        Me.mnuLogout.Size = New System.Drawing.Size(57, 20)
        Me.mnuLogout.Text = "Logout"
        '
        'mnuQuotationScope
        '
        Me.mnuQuotationScope.Name = "mnuQuotationScope"
        Me.mnuQuotationScope.Size = New System.Drawing.Size(176, 22)
        Me.mnuQuotationScope.Text = "Quotation Scope"
        '
        'mnuCustomerScope
        '
        Me.mnuCustomerScope.Name = "mnuCustomerScope"
        Me.mnuCustomerScope.Size = New System.Drawing.Size(162, 22)
        Me.mnuCustomerScope.Text = "Customer Scope"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(907, 482)
        Me.Controls.Add(Me.dlMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.dlMenu
        Me.Name = "frmMain"
        Me.Text = "BAFCO-TIS v[%V%]"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.dlMenu.ResumeLayout(False)
        Me.dlMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents sfd As System.Windows.Forms.SaveFileDialog
    Friend WithEvents dlMenu As MBMenuStrip.MBMenuStrip
    Friend WithEvents CostInformationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTransportCost As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DownloadFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileImportCost As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFileImportService As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents QuotationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLocation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSupplierType As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSupplier As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCustomerType As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCustomer As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuServiceType As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuService As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuVehicle As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDbConfig As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuServiceCharge As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuImportCost As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportService As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLogout As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMainQuotation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCaseByCaseQuotation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuSettingQuotation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuSettingQuotationCategory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuSettingQuotationGroup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuSettingQuotationSubGroup As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuBasis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuCreditTerm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuMode As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuTerm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuCurrency As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuSettingQuotationDescription As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mmuSettingQuotationTermConditions As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSettingCountry As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuQuotationScope As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCustomerScope As System.Windows.Forms.ToolStripMenuItem

End Class

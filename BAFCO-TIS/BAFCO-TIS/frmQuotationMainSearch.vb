﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports OfficeOpenXml

Public Class frmQuotationMainSearch

    Private WithEvents kbHook As New KeyboardHook
    Dim dt As New DataTable

    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If
    End Sub

    Private Sub frmQuotationMainSearch_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        BindCBB_Customer(cbbCustomer)
        BindCBB_CustomerType(cbbCustType)
        BindCBB_CreditTerm(cbbCreditTerm)
        ShowData()
    End Sub

    Private Sub frmQuotationMainSearch_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        dpDateFrom.Value = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        dpDateTo.Value = DateTime.Today
    End Sub


    Private Sub btnExport_Click(sender As System.Object, e As System.EventArgs) Handles btnExport.Click
        If dt.DefaultView.Count > 0 Then
            Using ep As New ExcelPackage
                Dim ws As ExcelWorksheet = ep.Workbook.Worksheets.Add("MainQuotationData")
                ws.Cells("A1").Value = "QT No"
                ws.Cells("B1").Value = "QT Date"
                ws.Cells("C1").Value = "Customer"
                ws.Cells("D1").Value = "Sales Name"
                ws.Cells("E1").Value = "Status"
                ws.Cells("F1").Value = "Effective Date"

                ws.Cells("A1").Style.Font.Bold = True
                ws.Cells("B1").Style.Font.Bold = True
                ws.Cells("C1").Style.Font.Bold = True
                ws.Cells("D1").Style.Font.Bold = True
                ws.Cells("E1").Style.Font.Bold = True
                ws.Cells("F1").Style.Font.Bold = True

                Dim i As Integer = 2
                For Each dr As DataRowView In dt.DefaultView
                    ws.Cells(i, 1).Value = dr("qt_no")
                    ws.Cells(i, 2).Value = Convert.ToDateTime(dr("qt_date")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    ws.Cells(i, 3).Value = dr("cus_name")
                    ws.Cells(i, 4).Value = dr("us_code")
                    ws.Cells(i, 5).Value = dr("qt_status_name")
                    ws.Cells(i, 6).Value = dr("effective_date")
                    i += 1
                Next

                ws.Cells(1, 1, i, 6).AutoFitColumns()

                Using fs As New SaveFileDialog
                    fs.Filter = "Excel File(*.xlsx)|*.xlsx"
                    If fs.ShowDialog = Windows.Forms.DialogResult.OK Then
                        ep.SaveAs(New IO.FileInfo(fs.FileName))
                    End If
                End Using
            End Using
        Else
            MessageBox.Show("Data not found", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)

        End If
    End Sub

    Public Sub ShowData()
        Grid.AutoGenerateColumns = False

        Dim SQL As String = "select q.id, q.qt_no, q.qt_date, convert(varchar(8), q.qt_date, 112) qt_date_str, q.cus_id, c.cus_name, q.user_id_sale, u.us_code, "
        SQL += " case q.qt_status when '0' then 'Draft' when '1' then 'Approve' when '2' then 'Deny' end qt_status_name, q.qt_status, "
        SQL += " isnull(case q.qt_status when '1' then convert(varchar(10),effect_from_date,103) + ' - ' + convert(varchar(10),effect_to_date,103) else '' end,'') effective_date, "
        SQL += " c.cus_type_id, isnull(c.ms_credit_term_id,0) ms_credit_term_id, isnull(c.cus_account,'') account_no, isnull(q.qt_note,'') qt_note, "
        SQL += " isnull(q.reference_no,'') reference_no"
        SQL += " from QUOTATION_MAIN q "
        SQL += " inner join CUSTOMER c on c.cus_id=q.cus_id "
        SQL += " inner join [USER] u on u.us_id=q.user_id_sale "
        SQL += " where c.cus_id in (SELECT CUS_ID FROM USER_CUSTOMER WHERE US_ID = " & myUser.user_id & ") "
        SQL += " order by q.qt_date desc "

        dt = Execute_DataTable(SQL)
        Grid.DataSource = dt

        Application.DoEvents()
    End Sub

    Private Sub FilterData()
        If dt.Rows.Count = 0 Then Exit Sub

        Dim wh As String = "1=1 "
        If cbbCustomer.SelectedIndex > 0 Then
            wh += " and cus_id=" & cbbCustomer.SelectedValue
        End If
        If txtQtNo.Text.Trim <> "" Then
            wh += " and qt_no like '%" & txtQtNo.Text.Trim & "%'"
        End If
        If txtSalesName.Text.Trim <> "" Then
            wh += " and us_code like '%" & txtSalesName.Text.Trim & "%'"
        End If
        If txtAccountNo.Text.Trim <> "" Then
            wh += " and account_no like '%" & txtAccountNo.Text.Trim & "%'"
        End If
        If cbbCustType.SelectedIndex > 0 Then
            wh += " and cus_type_id=" & cbbCustType.SelectedValue
        End If
        If cbbCreditTerm.SelectedIndex > 0 Then
            wh += " and ms_credit_term_id=" & cbbCreditTerm.SelectedValue
        End If
        If txtRefNo.Text.Trim <> "" Then
            wh += " and reference_no like '%" & txtRefNo.Text.Trim & "%'"
        End If
        If txtNote.Text.Trim <> "" Then
            wh += " and qt_note like '%" & txtNote.Text.Trim & "%'"
        End If


        Dim txtStatus As String = ""
        If chkStatusDraft.Checked = True Then
            txtStatus += ",'0'"
        End If
        If chkStatusApprove.Checked = True Then
            txtStatus += ",'1'"
        End If
        If chkStatusDeny.Checked = True Then
            txtStatus += ",'2'"
        End If
        If txtStatus <> "" Then
            txtStatus = txtStatus.Substring(1)
        End If
        If txtStatus.Trim <> "" Then
            wh += " and qt_status in (" & txtStatus & ")"
        Else
            wh += " and qt_status in ('')"
        End If

        Dim strDateFrom As String = dpDateFrom.Value.ToString("yyyyMMdd", New Globalization.CultureInfo("en-US"))
        Dim strDateTo As String = dpDateTo.Value.ToString("yyyyMMdd", New Globalization.CultureInfo("en-US"))
        wh += " and qt_date_str >= '" & strDateFrom & "'"
        wh += " and qt_date_str <= '" & strDateTo & "'"

        dt.DefaultView.RowFilter = wh
        lblRec.Text = lblRec.Text.Replace("xxx", dt.DefaultView.Count)
    End Sub

    Private Sub Grid_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid.CellDoubleClick
        Dim _id As Long = Grid.Rows(e.RowIndex).Cells("ID").Value
        Dim frm As New frmQuotationMain
        frm.MdiParent = frmMain
        frm.Show()
        frm.FillInData(_id, False)

        Me.Hide()
    End Sub


    Private Sub Grid_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid.DataError
        MsgBox(e.Exception.Message)
    End Sub


    Private Sub cbbCus_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbCustomer.SelectedIndexChanged
        If cbbCustomer.SelectedIndex > 0 Then
            FilterData()
        End If
    End Sub

    Private Sub cbbCreditTerm_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbCreditTerm.SelectedIndexChanged
        If cbbCreditTerm.SelectedIndex > 0 Then
            FilterData()
        End If
    End Sub

    Private Sub cbbCustType_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbCustType.SelectedIndexChanged
        If cbbCustType.SelectedIndex > 0 Then
            FilterData()
        End If
    End Sub

    Private Sub dpDateFrom_ValueChanged(sender As System.Object, e As System.EventArgs) Handles dpDateFrom.ValueChanged, dpDateTo.ValueChanged
        Grid.AutoGenerateColumns = False
        FilterData()
    End Sub

    Private Sub txtQtNo_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtQtNo.TextChanged, txtSalesName.TextChanged, txtAccountNo.TextChanged, txtNote.TextChanged
        Grid.AutoGenerateColumns = False
        FilterData()
    End Sub

    Private Sub chkStatusDraft_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkStatusDraft.CheckedChanged, chkStatusApprove.CheckedChanged, chkStatusDeny.CheckedChanged
        Grid.AutoGenerateColumns = False
        FilterData()
    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmQuotationMain
        frm.MdiParent = frmMain
        frm.Show()

        Me.Hide()
    End Sub

    'Private Sub frmQuotationMainSearch_VisibleChanged(sender As Object, e As System.EventArgs) Handles Me.VisibleChanged
    '    Grid.AutoGenerateColumns = False
    '    ShowData()
    '    FilterData()
    'End Sub

    Private Sub btnCopy_Click(sender As System.Object, e As System.EventArgs) Handles btnCopy.Click
        Dim _id As Long = Grid.SelectedRows(0).Cells("ID").Value
        Dim frm As New frmQuotationMain
        frm.MdiParent = frmMain
        frm.Show()
        frm.FillInData(_id, True)

        Me.Hide()
    End Sub

    
End Class
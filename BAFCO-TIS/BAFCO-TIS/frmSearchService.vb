﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.Text

Public Class frmSearchService

    'Dim ServiceFilter As String = ""
    'Dim ServiceFilterText As String = ""
    Dim DT_SUP As New DataTable
    Dim DT_CUS As New DataTable
    Private WithEvents kbHook As New KeyboardHook

    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If myUser.capture_screen = False Then
            If Key = 44 Then
                Clipboard.Clear()
            End If
        End If
    End Sub

    Private Sub frmSearchService_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        Me.WindowState = FormWindowState.Maximized
        
    End Sub


    Private Sub frmSearchService_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        GridSup.AutoGenerateColumns = False
        GridCus.AutoGenerateColumns = False
        btnPrintSupplier.Visible = myUser.view_report
        btnPrintCustomer.Visible = myUser.view_report
        btnPrintAll.Visible = myUser.view_report

        'BindCBB_Supplier(cbbSup)
        'BindCBB_Customer(cbbCus)

        Dim SQL As String = ""
        SQL = "SELECT SER_TYPE_ID,SER_TYPE_NAME FROM SERVICE_TYPE ORDER BY SER_TYPE_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)

        For i As Int32 = 0 To DT.Rows.Count - 1
            SQL = "SELECT SER_ID,SER_NAME FROM [SERVICE] WHERE SER_TYPE_ID = " & DT.Rows(i).Item("SER_TYPE_ID").ToString & " ORDER BY SER_NAME"
            Dim DA_S As New SqlDataAdapter(SQL, ConnStr)
            Dim DT_S As New DataTable
            DA_S.Fill(DT_S)
        Next

        BindSupplier()
        BindCustomer()

        AddHandler UCSelectedItem.btnDeleteCusClick, AddressOf UCSelectedItem_btnDeleteCusClick
        AddHandler UCSelectedItem.btnDeleteSubClick, AddressOf UCSelectedItem_btnDeleteSupClick
    End Sub

    Sub BindSupplier()
        Dim SQL As String = ""
        SQL &= "SELECT CONVERT(VARCHAR(10),GETDATE(),103) AS PrintDate ,SERVICE_SUP.ID AS S_ID,SERVICE.SER_ID AS S_SER_ID,SERVICE.SER_NAME AS S_SER_NAME,SERVICE_TYPE.SER_TYPE_ID AS S_SER_TYPE_ID,SERVICE_TYPE.SER_TYPE_NAME AS S_SER_TYPE_NAME" & vbCrLf
        SQL &= ",SERVICE_SUP.SUP_ID AS S_SUP_ID,SUP_NAME AS S_SUP_NAME,cast(price as decimal)  AS S_COST,UNIT CostUnit" & vbCrLf
        SQL &= "FROM SERVICE_SUP LEFT JOIN SERVICE ON SERVICE_SUP.SER_ID = SERVICE.SER_ID" & vbCrLf
        SQL &= "LEFT JOIN SERVICE_TYPE ON SERVICE.SER_TYPE_ID = SERVICE_TYPE.SER_TYPE_ID" & vbCrLf
        SQL &= "LEFT JOIN SUPPLIER ON SERVICE_SUP.SUP_ID = SUPPLIER.SUP_ID" & vbCrLf
        SQL &= "LEFT JOIN (SELECT * FROM USER_SUPPLIER WHERE US_ID = " & myUser.user_id & ") US ON SERVICE_SUP.SUP_ID = US.SUP_ID" & vbCrLf
        SQL &= "WHERE US.US_ID IS NOT NULL" & vbCrLf
        SQL &= "ORDER BY S_SER_NAME,S_SUP_NAME" & vbCrLf
        DT_SUP = New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_SUP)
        GridSup.DataSource = DT_SUP

    End Sub

    Sub BindCustomer()
        Dim SQL As String = ""
        SQL &= "SELECT CONVERT(VARCHAR(10),GETDATE(),103) AS PrintDate ,SERVICE_CUS.ID AS C_ID,SERVICE.SER_ID AS C_SER_ID,SERVICE.SER_NAME AS C_SER_NAME,SERVICE_TYPE.SER_TYPE_ID AS C_SER_TYPE_ID,SERVICE_TYPE.SER_TYPE_NAME AS C_SER_TYPE_NAME" & vbCrLf
        SQL &= ",SERVICE_CUS.CUS_ID AS C_CUS_ID,CUS_NAME AS C_CUS_NAME,cast(price as decimal) AS C_COST,UNIT ValueUnit, isnull(SERVICE_CUS.qt_status,1) qt_status" & vbCrLf
        SQL &= "FROM SERVICE_CUS LEFT JOIN SERVICE ON SERVICE_CUS.SER_ID = SERVICE.SER_ID" & vbCrLf
        SQL &= "LEFT JOIN SERVICE_TYPE ON SERVICE.SER_TYPE_ID = SERVICE_TYPE.SER_TYPE_ID" & vbCrLf
        SQL &= "LEFT JOIN CUSTOMER ON SERVICE_CUS.CUS_ID = CUSTOMER.CUS_ID" & vbCrLf
        SQL &= "LEFT JOIN (SELECT * FROM USER_CUSTOMER WHERE US_ID = " & myUser.user_id & ") US ON SERVICE_CUS.CUS_ID = US.CUS_ID" & vbCrLf
        SQL &= "WHERE US.US_ID IS NOT NULL" & vbCrLf
        SQL &= "ORDER BY C_SER_NAME,C_CUS_NAME" & vbCrLf
        DT_CUS = New DataTable
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_CUS)
        GridCus.DataSource = DT_CUS

        FilterCustomer()
    End Sub

    Sub FilterSupplier()
        If DT_SUP.Rows.Count > 0 Then
            Dim _sup_id As String = GetSupCusID(flowSupplier)
            Dim Filter As String = "1=1"
            If _sup_id <> "" Then
                Filter &= " AND S_SUP_ID IN (" & _sup_id & ")"
            End If

            If txtSearchService.Text.Trim() <> "" Then
                Filter &= " AND S_SER_NAME LIKE '%" & txtSearchService.Text & "%'  "
            End If

            DT_SUP.DefaultView.RowFilter = Filter
        End If
    End Sub

    Sub FilterCustomer()
        If DT_CUS.Rows.Count > 0 Then
            Dim _cus_id As String = GetSupCusID(flowCustomer)
            Dim Filter As String = "1=1"
            If _cus_id <> "" Then
                Filter &= " AND C_CUS_ID IN (" & _cus_id & ")"
            End If

            If txtSearchService.Text.Trim() <> "" Then
                Filter &= " AND C_SER_NAME LIKE '%" & txtSearchService.Text & "%' "
            End If

            DT_CUS.DefaultView.RowFilter = Filter

            If GridCus.RowCount > 0 Then
                For i As Integer = 0 To GridCus.RowCount - 1
                    If GridCus.Rows(i).Cells("colQtStatusValue").Value = "0" Then
                        GridCus.Rows(i).Cells("colQtStatus").Value = My.Resources.cb_g
                    ElseIf GridCus.Rows(i).Cells("colQtStatusValue").Value = "1" Then
                        GridCus.Rows(i).Cells("colQtStatus").Value = My.Resources.cb_gr
                    ElseIf GridCus.Rows(i).Cells("colQtStatusValue").Value = "2" Then
                        GridCus.Rows(i).Cells("colQtStatus").Value = My.Resources.cb_r
                    End If
                Next
                Application.DoEvents()
            End If
        End If
    End Sub


    Private Sub GridSup_CellMouseDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles GridSup.CellMouseDoubleClick
        If e.RowIndex < 0 Then Exit Sub
        Dim f As New frmSearchDialogSupplier_Ser
        f.VAR_MODE = "EDIT"
        f.VAR_SERVICE_ID = GridSup.Rows(e.RowIndex).Cells("S_ID").Value.ToString
        f.VAR_S_SER_ID = GridSup.Rows(e.RowIndex).Cells("S_SER_ID").Value.ToString
        f.VAR_S_SER_TYPE_ID = GridSup.Rows(e.RowIndex).Cells("S_SER_TYPE_ID").Value.ToString
        f.VAR_SUP_ID = GridSup.Rows(e.RowIndex).Cells("S_SUP_ID").Value.ToString
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindSupplier()
        End If
    End Sub

    Private Sub btnS_Add_Click(sender As System.Object, e As System.EventArgs) Handles btnS_Add.Click
        Dim f As New frmSearchDialogSupplier_Ser
        f.VAR_MODE = "ADD"
        f.VAR_SERVICE_ID = 0
        'f.VAR_SUP_ID = "" cbbSup.SelectedValue
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindSupplier()
        End If
    End Sub

    Private Sub GridCus_CellMouseDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles GridCus.CellMouseDoubleClick
        If e.RowIndex < 0 Then Exit Sub
        Dim f As New frmSearchDialogCustomer_Ser
        f.VAR_MODE = "EDIT"
        f.VAR_SERVICE_ID = GridCus.Rows(e.RowIndex).Cells("C_ID").Value.ToString
        f.VAR_C_SER_ID = GridCus.Rows(e.RowIndex).Cells("C_SER_ID").Value.ToString
        f.VAR_C_SER_TYPE_ID = GridCus.Rows(e.RowIndex).Cells("C_SER_TYPE_ID").Value.ToString
        f.VAR_CUS_ID = GridCus.Rows(e.RowIndex).Cells("C_CUS_ID").Value.ToString
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindCustomer()
        End If
    End Sub

    Private Sub btnC_Add_Click(sender As System.Object, e As System.EventArgs) Handles btnC_Add.Click
        Dim f As New frmSearchDialogCustomer_Ser
        f.VAR_MODE = "ADD"
        f.VAR_SERVICE_ID = 0
        'f.VAR_CUS_ID = "" cbbCus.SelectedValue
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            BindCustomer()
        End If
    End Sub

    Private Sub tvService_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles tvService.AfterSelect
        If e.Action <> TreeViewAction.Unknown Then
            If e.Node.Nodes.Count > 0 Then
                ' Calls the CheckAllChildNodes method, passing in the current  
                ' Checked value of the TreeNode whose checked state changed.  
                Me.CheckAllChildNodes(e.Node, e.Node.Checked)
            End If
        End If

    End Sub

    Private Sub CheckAllChildNodes(treeNode As TreeNode, nodeChecked As Boolean)
        Dim node As TreeNode
        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            If node.Nodes.Count > 0 Then
                ' If the current node has child nodes, call the CheckAllChildsNodes method recursively. 
                Me.CheckAllChildNodes(node, nodeChecked)
            End If
        Next node
    End Sub



    Private Sub btnPrintSupplier_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintSupplier.Click
        Dim dt As New DataTable
        dt = DT_SUP.DefaultView.ToTable
        Dim _sup_id As StringBuilder = GetSupCusName(flowSupplier)
        Dim f As New Service_Sup
        With f
            .ReportName = "SERV_SUP.rpt"
            .ServiceType = ""
            .Service = IIf(txtSearchService.Text.Trim() = "", "All", txtSearchService.Text.Trim())
            .Supplier = IIf(_sup_id.ToString() = "", "Supplier : All", "Supplier :</br>" & _sup_id.ToString())
            .DTSUP = dt
            .ShowDialog()
        End With
    End Sub

    Private Sub btnPrintCustomer_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintCustomer.Click
        Dim dt As New DataTable
        dt = DT_CUS.DefaultView.ToTable

        Dim _cus_id As StringBuilder = GetSupCusName(flowCustomer)

        Dim f As New Service_Cus
        With f
            .ReportName = "SERV_CUS.rpt"
            .ServiceType = ""
            .Service = IIf(txtSearchService.Text.Trim() = "", "All", txtSearchService.Text.Trim())
            .Customer = IIf(_cus_id.ToString = "", "Customer : All", "Customer :</br>" & _cus_id.ToString)
            .DT = dt
            .ShowDialog()
        End With
    End Sub

    Function GetAllDataForPrint() As DataTable
        Try
            Dim _sup_id As String = GetSupCusID(flowSupplier)
            Dim _cus_id As String = GetSupCusID(flowCustomer)

            Dim sql As String = ""
            sql &= " Select CONVERT(VARCHAR(10),GETDATE(),103) AS PrintDate ,ISNULL(T1.S_SER_ID,T2.C_SER_ID) SER_ID,ISNULL(T1.SER_NAME,T2.SER_NAME) SER_NAME," & Environment.NewLine
            sql &= " ISNULL(T1.SER_TYPE_ID,T2.SER_TYPE_ID) SER_TYPE_ID,ISNULL(T1.SER_TYPE_NAME,T2.SER_TYPE_NAME) SER_TYPE_NAME,T1.SUP_ID,T1.SUP_NAME,T1.PRICE COST,T1.UNIT COST_UNIT," & Environment.NewLine
            sql &= " T2.CUS_ID,T2.CUS_NAME,T2.PRICE ,T2.UNIT PRICE_UNIT from " & Environment.NewLine
            sql &= " ( SELECT SERVICE_SUP.ID,SERVICE.SER_ID AS S_SER_ID," & Environment.NewLine
            sql &= " SERVICE.SER_NAME,SERVICE_TYPE.SER_TYPE_ID ,SERVICE_TYPE.SER_TYPE_NAME ,SERVICE_SUP.SUP_ID,SUP_NAME, cast(price as decimal) PRICE ,UNIT" & Environment.NewLine
            sql &= " FROM Service_Sup" & Environment.NewLine
            sql &= " LEFT JOIN SERVICE ON SERVICE_SUP.SER_ID = SERVICE.SER_ID " & Environment.NewLine
            sql &= " LEFT JOIN SERVICE_TYPE ON SERVICE.SER_TYPE_ID = SERVICE_TYPE.SER_TYPE_ID " & Environment.NewLine
            sql &= " LEFT JOIN SUPPLIER ON SERVICE_SUP.SUP_ID = SUPPLIER.SUP_ID " & Environment.NewLine
            sql &= " LEFT JOIN (SELECT * FROM USER_SUPPLIER WHERE US_ID =" & myUser.user_id & ") US ON SERVICE_SUP.SUP_ID = US.SUP_ID " & Environment.NewLine
            sql &= " WHERE US.US_ID Is Not NULL " & Environment.NewLine
            If _sup_id <> "" Then
                sql &= " AND US.SUP_ID IN (" & _sup_id & ")" & Environment.NewLine
            End If
            sql &= " ) T1" & Environment.NewLine
            sql &= " FULL OUTER JOIN" & Environment.NewLine
            sql &= " (SELECT SERVICE_CUS.ID,SERVICE.SER_ID AS C_SER_ID,SERVICE.SER_NAME," & Environment.NewLine
            sql &= " SERVICE_TYPE.SER_TYPE_ID ,SERVICE_TYPE.SER_TYPE_NAME ,SERVICE_CUS.CUS_ID,CUS_NAME,cast(price as decimal) PRICE,UNIT" & Environment.NewLine
            sql &= " FROM Service_Cus" & Environment.NewLine
            sql &= " LEFT JOIN SERVICE ON SERVICE_CUS.SER_ID = SERVICE.SER_ID " & Environment.NewLine
            sql &= " LEFT JOIN SERVICE_TYPE ON SERVICE.SER_TYPE_ID = SERVICE_TYPE.SER_TYPE_ID " & Environment.NewLine
            sql &= " LEFT JOIN CUSTOMER ON SERVICE_CUS.CUS_ID = CUSTOMER.CUS_ID "
            sql &= " LEFT JOIN (SELECT * FROM USER_CUSTOMER WHERE US_ID = " & myUser.user_id & ") US ON SERVICE_CUS.CUS_ID = US.CUS_ID WHERE US.US_ID IS NOT NULL " & Environment.NewLine
            If _cus_id <> "" Then
                sql &= " AND US.CUS_ID in (" & _cus_id & ")" & Environment.NewLine
            End If
            sql &= " ) T2 " & Environment.NewLine

            sql &= " ON T1.S_SER_ID=T2.C_SER_ID" & Environment.NewLine
            If txtSearchService.Text.Trim() <> "" Then
                sql &= " Where 1=1 AND ISNULL(T1.SER_NAME,T2.SER_NAME) LIKE '%" & txtSearchService.Text & "%' "
            End If

            Dim DT As New DataTable
            Dim DA As New SqlDataAdapter(sql, ConnStr)
            DA.Fill(DT)
            Return DT
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Sub btnPrintAll_Click(sender As System.Object, e As System.EventArgs) Handles btnPrintAll.Click
        Dim dt As New DataTable
        dt = GetAllDataForPrint()

        Dim _sup_id As StringBuilder = GetSupCusName(flowSupplier)
        Dim _cus_id As StringBuilder = GetSupCusName(flowCustomer)

        Dim f As New Service_Sup
        With f
            .ReportName = "SERV_ALL.rpt"
            .ServiceType = ""
            .Service = IIf(txtSearchService.Text.Trim() = "", "All", txtSearchService.Text.Trim())
            .Supplier = IIf(_sup_id.ToString() = "", "Supplier : All", "Supplier :</br>" & _sup_id.ToString())
            .Customer = IIf(_cus_id.ToString = "", "Customer : All", "Customer :</br>" & _cus_id.ToString)
            .DTSUP = dt
            .ShowDialog()
        End With
    End Sub

    Private Sub txtSearchService_KeyUp(sender As System.Object, e As System.Windows.Forms.KeyEventArgs) Handles txtSearchService.KeyUp
        FilterSupplier()
        FilterCustomer()
    End Sub

#Region "SelectCustomer"
    Private Sub btnDeleteSupCus(id As String, flow As FlowLayoutPanel, type As String)
        Dim _id As String = id
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("name")

        Dim dr As DataRow
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            If UCItem1.strID <> _id Then
                dr = dt.NewRow
                dr("id") = UCItem1.strID
                dr("name") = UCItem1.strName
                dt.Rows.Add(dr)
            End If
        Next

        If flow.Controls.Count > 0 Then flow.Controls.Clear()
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1.strID = dt.Rows(i)("id")
            UCItem1.strName = dt.Rows(i)("name")
            UCItem1.type = type
            flow.Controls.Add(UCItem1)
        Next
    End Sub

    Function GetSupCusID(flow As FlowLayoutPanel) As String
        Dim _id As String = ""
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            _id &= UCItem1.strID
            If i < flow.Controls.Count - 1 Then
                _id &= ","
            End If
        Next
        Return _id
    End Function

    Function GetSupCusName(flow As FlowLayoutPanel) As StringBuilder
        Dim _Name As New StringBuilder
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            _Name.Append(UCItem1.strName.Replace("'", "''"))
            If i < flow.Controls.Count - 1 Then
                _Name.Append("</br>")
            End If
        Next
        Return _Name
    End Function

    Private Sub UCSelectedItem_btnDeleteCusClick(id As String)
        btnDeleteSupCus(id, flowCustomer, "cus")
        FilterCustomer()
    End Sub

    Sub BindDataSupCus(dt_selected As DataTable, flow As FlowLayoutPanel, type As String)
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("name")

        Dim dr As DataRow
        For i As Integer = 0 To flow.Controls.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1 = flow.Controls(i)
            dr = dt.NewRow
            dr("id") = UCItem1.strID
            dr("name") = UCItem1.strName
            dt.Rows.Add(dr)
        Next

        If dt_selected IsNot Nothing Then
            For i As Integer = 0 To dt_selected.Rows.Count - 1
                dr = dt.NewRow
                dr("id") = dt_selected.Rows(i)("id")
                dr("name") = dt_selected.Rows(i)("name")
                dt.Rows.Add(dr)
            Next
        End If

        If flow.Controls.Count > 0 Then flow.Controls.Clear()
        For i As Integer = 0 To dt.Rows.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1.strID = dt.Rows(i)("id")
            UCItem1.strName = dt.Rows(i)("name")
            UCItem1.type = type
            flow.Controls.Add(UCItem1)
        Next
    End Sub

    Private Sub btnAddCustomer_Click(sender As System.Object, e As System.EventArgs) Handles btnAddCustomer.Click
        Dim _cus_id As String = GetSupCusID(flowCustomer)
        Dim f As New popupSelectCustomer
        f.SetCustomerID = _cus_id
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim dt As New DataTable
            dt = f.GetData()
            BindDataSupCus(dt, flowCustomer, "cus")
        End If
        FilterCustomer()
    End Sub
#End Region

#Region "SelectSupplier"
    Private Sub UCSelectedItem_btnDeleteSupClick(id As String)
        btnDeleteSupCus(id, flowSupplier, "sup")
        FilterSupplier()
    End Sub

    Private Sub btnAddSupplier_Click(sender As System.Object, e As System.EventArgs) Handles btnAddSupplier.Click
        Dim _sup_id As String = GetSupCusID(flowSupplier)
        Dim f As New popupSelectSupplier
        f.SetSpplierID = _sup_id
        If f.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim dt As New DataTable
            dt = f.GetData()
            BindDataSupCus(dt, flowSupplier, "sup")
        End If
        FilterSupplier()
    End Sub
#End Region

End Class
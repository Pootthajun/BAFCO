﻿Public Class UCMainQuotationTruckItem
    Public Event DeleteTruckItem(ctl As UCMainQuotationTruckItem)
    Public Sub SetDDL()
        BindCBB_Route(cbbRouteFrom)
        BindCBB_Route(cbbRouteTo)
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        RaiseEvent DeleteTruckItem(Me)
    End Sub
End Class

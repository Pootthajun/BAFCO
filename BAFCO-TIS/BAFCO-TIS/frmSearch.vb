﻿Imports System.Data.SqlClient

Public Class frmSearch

    Dim DT_COST As New DataTable
    Dim CostFilter As String = ""

    Private Sub frmSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        Me.WindowState = FormWindowState.Maximized

        BindCBB_Route(cbbFrom)
        BindCBB_Route(cbbTo)
        BindCBB_Supplier(cbbSup)
        BindCBB_Customer(cbbServiceCus)
        BindCBB_Customer(cbbCostCus)

        Dim SQL As String = ""
        SQL = "SELECT VEH_ID,VEH_NAME FROM VEHICLE ORDER BY VEH_NAME"
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        For i As Int32 = 0 To DT.Rows.Count - 1
            Dim cb As New CheckBox
            cb.Text = DT.Rows(i).Item("VEH_NAME").ToString
            cb.Tag = DT.Rows(i).Item("VEH_ID").ToString
            cb.Checked = True
            flpVEH.Controls.Add(cb)
            AddHandler cb.CheckedChanged, AddressOf FilterDataCost
            CostFilter &= cb.Tag & ","
        Next
        If CostFilter <> "" Then
            CostFilter = CostFilter.Substring(0, CostFilter.Length - 1)
        End If


        SQL = "SELECT SER_ID,SER_NAME FROM SERVICE ORDER BY SER_NAME"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT = New DataTable
        DA.Fill(DT)
        For i As Int32 = 0 To DT.Rows.Count - 1
            Dim cb As New CheckBox
            cb.Text = DT.Rows(i).Item("SER_NAME").ToString
            cb.Tag = DT.Rows(i).Item("SER_ID").ToString
            cb.Checked = True
            flpSER.Controls.Add(cb)
        Next

        SQL = ""
        SQL &= "SELECT ID,ROUTE_FROM AS ROUTE_FROM_ID,ROUTE_F.ROUTE_NAME AS ROUTE_FROM_NAME,ROUTE_FROM AS ROUTE_TO_ID,ROUTE_T.ROUTE_NAME AS ROUTE_TO_NAME" & vbCrLf
        SQL &= ",COST.SUP_ID,SUP_NAME,COST.VEH_ID,VEH_NAME,CONVERT(VARCHAR(20),COST) COST,S_REVISE,S_REMARK,COST.CUS_ID,CUS_NAME,CONVERT(VARCHAR(20),VALUE) VALUE,C_REVISE,C_REMARK,CONVERT(VARCHAR(20),VALUE - COST) AS MARGIN,COST_DATE" & vbCrLf
        SQL &= "FROM COST LEFT JOIN ROUTE ROUTE_F ON COST.ROUTE_FROM = ROUTE_F.ROUTE_ID" & vbCrLf
        SQL &= "LEFT JOIN ROUTE ROUTE_T ON COST.ROUTE_TO = ROUTE_T.ROUTE_ID" & vbCrLf
        SQL &= "LEFT JOIN SUPPLIER ON COST.SUP_ID = SUPPLIER.SUP_ID" & vbCrLf
        SQL &= "LEFT JOIN VEHICLE ON COST.VEH_ID = VEHICLE.VEH_ID" & vbCrLf
        SQL &= "LEFT JOIN CUSTOMER ON COST.CUS_ID = CUSTOMER.CUS_ID" & vbCrLf
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_COST)
        With GridCost
            .DataSource = DT_COST
            Dim cs As New DataGridViewCellStyle
            cs.Font = New Font("Ms Sans Serif", 10, FontStyle.Bold)
            .ColumnHeadersDefaultCellStyle = cs
            .Columns("ID").Visible = False
            .Columns("ROUTE_FROM_ID").Visible = False
            .Columns("ROUTE_FROM_NAME").HeaderText = "From"
            .Columns("ROUTE_TO_ID").Visible = False
            .Columns("ROUTE_TO_NAME").HeaderText = "To"
            .Columns("SUP_ID").Visible = False
            .Columns("SUP_NAME").HeaderText = "Supplier"
            .Columns("VEH_ID").Visible = False
            .Columns("VEH_NAME").HeaderText = "Vehicle"
            .Columns("COST").HeaderText = "Cost"
            .Columns("S_REVISE").HeaderText = "SupplierRevise"
            .Columns("S_REMARK").HeaderText = "SupplierRemark"
            .Columns("CUS_ID").Visible = False
            .Columns("CUS_NAME").HeaderText = "Customer"
            .Columns("VALUE").HeaderText = "Value"
            .Columns("C_REVISE").HeaderText = "CustomerRevise"
            .Columns("C_REMARK").HeaderText = "CustomerRemark"
            .Columns("MARGIN").HeaderText = "Margin"
            .Columns("COST_DATE").HeaderText = "Update Date"

            .Columns("SUP_NAME").DefaultCellStyle.BackColor = Color.Khaki
            .Columns("VEH_NAME").DefaultCellStyle.BackColor = Color.Khaki
            .Columns("COST").DefaultCellStyle.BackColor = Color.Khaki
            .Columns("S_REVISE").DefaultCellStyle.BackColor = Color.Khaki
            .Columns("S_REMARK").DefaultCellStyle.BackColor = Color.Khaki

            .Columns("CUS_NAME").DefaultCellStyle.BackColor = Color.LightGreen
            .Columns("VALUE").DefaultCellStyle.BackColor = Color.LightGreen
            .Columns("C_REVISE").DefaultCellStyle.BackColor = Color.LightGreen
            .Columns("C_REMARK").DefaultCellStyle.BackColor = Color.LightGreen

            .Columns("MARGIN").DefaultCellStyle.BackColor = Color.SkyBlue

            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .AllowUserToOrderColumns = True
            .AllowUserToResizeRows = False
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .EditMode = DataGridViewEditMode.EditProgrammatically
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            .RowHeadersVisible = False
        End With


        SQL = ""
        SQL &= "SELECT ID,SERVICE_CHARGE.CUS_ID,CUS_NAME,SERVICE_CHARGE.SER_ID,SER_NAME," & vbCrLf
        SQL &= "CONVERT(VARCHAR(20),VALUE) VALUE,REVISE,REMARK,SERVICE_CHARGE_DATE FROM SERVICE_CHARGE" & vbCrLf
        SQL &= "LEFT JOIN CUSTOMER ON SERVICE_CHARGE.CUS_ID = CUSTOMER.CUS_ID" & vbCrLf
        SQL &= "LEFT JOIN SERVICE ON SERVICE_CHARGE.SER_ID = SERVICE.SER_ID" & vbCrLf

        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT_SER As New DataTable
        DA.Fill(DT_SER)
        With GridService
            .DataSource = DT_SER
            Dim cs As New DataGridViewCellStyle
            cs.Font = New Font("Ms Sans Serif", 10, FontStyle.Bold)
            .ColumnHeadersDefaultCellStyle = cs
            .Columns("ID").Visible = False
            .Columns("CUS_ID").Visible = False
            .Columns("CUS_NAME").HeaderText = "Customer"
            .Columns("SER_ID").Visible = False
            .Columns("SER_NAME").HeaderText = "Service"
            .Columns("VALUE").HeaderText = "Value"
            .Columns("REVISE").HeaderText = "Revise"
            .Columns("REMARK").HeaderText = "Remark"
            .Columns("SERVICE_CHARGE_DATE").HeaderText = "Update Date"

            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .AllowUserToOrderColumns = True
            .AllowUserToResizeRows = False
            .MultiSelect = False
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .EditMode = DataGridViewEditMode.EditProgrammatically
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            .RowHeadersVisible = False
        End With
        SearchServiceData()

        'Dim TS As Integer = 0
        'Dim TC As Integer = 0
        'For i As Int32 = 0 To DT_COST.Rows.Count - 1
        '    If IsNumeric(CInt(DT_COST.Rows(i).Item("COST").ToString.Replace(",", ""))) Then
        '        TS = TS + DT_COST.Rows(i).Item("COST").ToString.Replace(",", "")
        '    End If
        '    If IsNumeric(CInt(DT_COST.Rows(i).Item("VALUE").ToString.Replace(",", ""))) Then
        '        TC = TC + DT_COST.Rows(i).Item("VALUE").ToString.Replace(",", "")
        '    End If
        'Next
        'lblTS.Text = FormatNumber(TS.ToString, 2)
        'lblTC.Text = FormatNumber(TC.ToString, 2)

        'Dim TT As Integer = 0
        'TT = TC - TS
        'lblTT.Text = FormatNumber(TT.ToString, 2)
    End Sub

    Sub FilterDataCost(ByVal Sender As Object, ByVal e As EventArgs)
        Dim cb As CheckBox = Sender
        Dim dt As DataTable
        dt = DT_COST
        If cb.Checked Then
            CostFilter = CostFilter & "," & cb.Tag
            'CostFilter = CostFilter.Replace("VEH_ID IN " & cb.Tag, "VEH_ID = " & cb.Tag)
        Else
            If InStr(CostFilter, cb.Tag & ",") > 0 Then
                CostFilter = CostFilter.Replace(cb.Tag & ",", "")
            ElseIf InStr(CostFilter, "," & cb.Tag) > 0 Then
                CostFilter = CostFilter.Replace("," & cb.Tag, "")
            End If
        End If

        dt.DefaultView.RowFilter = "VEH_ID IN (" & CostFilter & ")"
        GridCost.DataSource = dt
    End Sub

    Sub SearchData()
        Dim DT_1 As New DataTable

        Dim DT1 As New DataTable
        DT1 = GridCost.DataSource
        If Not DT1 Is Nothing Then
            Dim Filter As String = ""

            Filter &= "ROUTE_FROM_NAME LIKE '%" & cbbFrom.Text & "%' AND "
            Filter &= "ROUTE_TO_NAME LIKE '%" & cbbTo.Text & "%' AND "
            Filter &= "SUP_NAME LIKE '%" & cbbSup.Text & "%' AND "
            Filter &= "CUS_NAME LIKE '%" & cbbCostCus.Text & "%' AND "

            If Filter <> "" Then
                Filter = Filter.Substring(0, Filter.Length - 5)
            End If
            DT1.DefaultView.RowFilter = Filter
            DT_1 = DT1.DefaultView.ToTable
        End If

        Dim TS As Integer = 0
        Dim TC As Integer = 0
        For i As Int32 = 0 To DT_1.Rows.Count - 1
            If IsNumeric(CInt(DT_1.Rows(i).Item("COST").ToString.Replace(",", ""))) Then
                TS = TS + DT_1.Rows(i).Item("COST").ToString.Replace(",", "")
            End If
            If IsNumeric(CInt(DT_1.Rows(i).Item("COST").ToString.Replace(",", ""))) Then
                TC = TC + DT_1.Rows(i).Item("COST").ToString.Replace(",", "")
            End If
        Next
        'lblTS.Text = FormatNumber(TS.ToString, 2)
        'lblTC.Text = FormatNumber(TC.ToString, 2)

        'Dim TSV As Integer = 0
        'If IsNumeric(lblSV.Text) Then
        '    TSV = CInt(lblSV.Text)
        'End If

        'Dim TT As Integer = 0
        'TT = TC - (TS + TSV)
        'lblTT.Text = FormatNumber(TT.ToString, 2)

    End Sub

    Sub SearchServiceData()
        Dim DT_2 As New DataTable
        Dim DT2 As New DataTable
        DT2 = GridService.DataSource
        If Not DT2 Is Nothing Then
            Dim Filter As String = ""
            If cbbServiceCus.SelectedIndex > 0 Then
                Filter &= "CUS_NAME LIKE '%" & cbbServiceCus.Text & "%' AND "
                'If cbCC.Checked = False Then
                '    Filter &= "Type <> 'Customs Clerance' AND "
                'End If
                'If cbIL.Checked = False Then
                '    Filter &= "Type <> 'Import License' AND "
                'End If
                'If cbR.Checked = False Then
                '    Filter &= "Type <> 'Repacking' AND "
                'End If
                'Else
                '    Filter &= "Type = 'XXXXX' AND "
            End If

            If Filter <> "" Then
                Filter = Filter.Substring(0, Filter.Length - 5)
            End If
            DT2.DefaultView.RowFilter = Filter
            DT_2 = DT2.DefaultView.ToTable
        End If

        'Dim SV As Integer = 0
        'For i As Int32 = 0 To DT_2.Rows.Count - 1
        '    If IsNumeric(CInt(DT_2.Rows(i).Item("VALUE").ToString.Replace(",", ""))) Then
        '        SV = SV + DT_2.Rows(i).Item("VALUE").ToString.Replace(",", "")
        '    End If
        'Next
        'lblSV.Text = FormatNumber(SV.ToString, 2)

        'Dim TS As Integer = 0
        'Dim TC As Integer = 0
        'If IsNumeric(lblTS.Text) Then
        '    TS = CInt(lblTS.Text)
        'End If
        'If IsNumeric(lblTC.Text) Then
        '    TC = CInt(lblTC.Text)
        'End If

        'Dim TT As Integer = 0
        'TT = TC - (TS + SV)
        'lblTT.Text = FormatNumber(TT.ToString, 2)
    End Sub

    'Private Sub ddlFrom_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles ddlFrom.KeyDown
    '    SearchData()
    'End Sub

    Private Sub ddlFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbFrom.SelectedIndexChanged, cbbTo.SelectedIndexChanged, cbbCostCus.SelectedIndexChanged, cbbSup.SelectedIndexChanged
        SearchData()
    End Sub

    Private Sub ddlCus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbbCostCus.SelectedIndexChanged
        cbbServiceCus.SelectedIndex = cbbCostCus.SelectedIndex
    End Sub

    Private Sub ddlFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbbFrom.TextChanged, cbbTo.TextChanged, cbbSup.TextChanged, cbbCostCus.TextChanged
        SearchData()
    End Sub

    Private Sub cbbServiceCus_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbServiceCus.SelectedIndexChanged
        SearchServiceData()
    End Sub

    Private Sub cbbServiceCus_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbbServiceCus.TextChanged
        SearchServiceData()
    End Sub
End Class

﻿Public Class UCByCaseQuotationEstimateTotal
    Dim _CurrencyQty As Integer = 0

    Public WriteOnly Property EnableEstimateTotal As Boolean
        Set(value As Boolean)
            txtEstimateGrandTotal.Enabled = value
            cbbEstimateCurrency.Enabled = value
        End Set
    End Property


    Public Sub UpdateEstimateTotal(Total As Double, Currency As String)
        Dim isAddNew As Boolean = True
        If flpEstimateTotal.Controls.Count > 0 Then
            For i As Integer = 0 To flpEstimateTotal.Controls.Count - 1
                If i Mod 2 = 1 Then
                    'แสดงว่าเป็น Control Currency
                    Dim lblCurrency As Label = flpEstimateTotal.Controls(i)
                    Dim lblTotal As Label = flpEstimateTotal.Controls(i - 1)

                    If lblCurrency.Text = Currency Then
                        lblTotal.Text = (Convert.ToDouble(lblTotal.Text.Replace(",", "")) + Convert.ToDouble(Total)).ToString("#,##0.00")
                        isAddNew = False
                    End If
                End If
            Next
        End If

        If isAddNew = True Then
            Dim lblTotal As Label = CreateTotalLabel()
            lblTotal.Text = Total.ToString("#,##0.00")

            Dim lblCurrency As Label = CreateCurrencyLabel()
            lblCurrency.Text = Currency

            flpEstimateTotal.Controls.Add(lblTotal)
            flpEstimateTotal.Controls.Add(lblCurrency)

            _CurrencyQty += 1

            'ขยายขนาดถ้าข้อมูลมากขึ้น
        End If

        If _CurrencyQty > 1 Then
            lblEstimateGrandTotal.Visible = True
            txtEstimateGrandTotal.Visible = True
            cbbEstimateCurrency.Visible = True
        End If

    End Sub

    Public Sub ClearEstimateTotal()
        flpEstimateTotal.Controls.Clear()
        _CurrencyQty = 0
        lblEstimateGrandTotal.Visible = False
        txtEstimateGrandTotal.Visible = False
        cbbEstimateCurrency.Visible = False
        SetDDLCurrency()
    End Sub

    Public Sub FillInEstimateTotal(EstimateTotal As Double, MsCurrencyID As Long)
        txtEstimateGrandTotal.Text = EstimateTotal
        cbbEstimateCurrency.SelectedValue = MsCurrencyID
    End Sub


    Public Sub SetDDLCurrency()
        Dim sql As String = "select id, currency_name from MS_CURRENCY where active_status='Y' order by currency_name"
        Dim dt As DataTable = Execute_DataTable(sql)
        If dt.Rows.Count = 0 Then
            dt.Columns.Add("id")
            dt.Columns.Add("currency_name")
        End If

        'Dim dr As DataRow = dt.NewRow
        'dr("id") = 0
        'dr("currency_name") = ""
        'dt.Rows.InsertAt(dr, 0)

        cbbEstimateCurrency.DataSource = dt
        cbbEstimateCurrency.DisplayMember = "currency_name"
        cbbEstimateCurrency.ValueMember = "id"

        If dt.Rows.Count > 0 Then
            cbbEstimateCurrency.SelectedValue = 1
        End If
    End Sub

    Private Function CreateCurrencyLabel() As Label
        Dim lbl As New Label
        lbl.AutoSize = False
        lbl.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold)
        lbl.Margin = New Padding(0)
        lbl.Size = New Size(78, 24)
        lbl.TextAlign = ContentAlignment.MiddleCenter

        Return lbl
    End Function

    Private Function CreateTotalLabel() As Label
        Dim lbl As New Label
        lbl.AutoSize = False
        lbl.Font = New Font("Microsoft Sans Serif", 12, FontStyle.Bold)
        lbl.Margin = New Padding(0)
        lbl.Size = New Size(124, 24)
        lbl.TextAlign = ContentAlignment.MiddleRight

        Return lbl
    End Function

    Private Sub txtEstimateTotal_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtEstimateGrandTotal.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Asc(e.KeyChar) <> 46 AndAlso Not IsNumeric(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

End Class

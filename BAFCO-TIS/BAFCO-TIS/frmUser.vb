﻿Imports System.Data.SqlClient

Public Class frmUser

    Dim DT_USER As New DataTable
    Dim DT_SUP As New DataTable
    Dim DT_CUS As New DataTable
    Dim DT_MENU As New DataTable
    Dim DT_USER_SUP As New DataTable
    Dim DT_USER_CUS As New DataTable
    Dim DT_USER_MENU As New DataTable

    Private Sub frmSettingUser_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.ControlBox = False
        Me.WindowState = FormWindowState.Maximized

        GridUser.AutoGenerateColumns = False
        GridSUP.AutoGenerateColumns = False
        GridCUS.AutoGenerateColumns = False
        GridMenu.AutoGenerateColumns = False

        BindCBB_User(cbbFilterUser)
        BindCBB_Status(cbbFilterStatus)
        BindCBB_Status(cbbStatus)

        ShowData()
        Clear()

        cbbFilterUser.Focus()

    End Sub

    Private Sub txtName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtName.KeyPress
        If Asc(e.KeyChar) = 13 Then
            cbbStatus.Focus()
        End If
    End Sub

    Private Sub cbbStatus_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles cbbStatus.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtCode.Focus()
        End If
    End Sub

    Private Sub txtCode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCode.KeyPress
        If Asc(e.KeyChar) = 13 Then
            txtPassword.Focus()
        End If
    End Sub

    Private Sub cbbFilterUser_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbbFilterUser.KeyPress
        If Asc(e.KeyChar) = 13 Then
            cbbFilterStatus.Focus()
        End If
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        txtCode.Text = ""
        txtCode.Tag = ""
        txtPassword.Text = ""
        txtName.Text = ""
        cbbStatus.SelectedValue = 1
        EnableForm()
    End Sub

    Private Sub btnEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnEdit.Click
        EnableForm()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        Clear()
    End Sub

    Sub ShowData()
        Dim SQL As String = ""
        Dim DA As New SqlDataAdapter

        SQL = "SELECT MENU.MENU_ID,MENU_NAME,0 AS MENU_CHECK FROM MENU ORDER BY SORT"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT_MENU = New DataTable
        DA.Fill(DT_MENU)
        GridMenu.DataSource = DT_MENU

        SQL = "SELECT *,0 AS SUP_CHECK FROM SUPPLIER"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT_SUP = New DataTable
        DA.Fill(DT_SUP)
        GridSUP.DataSource = DT_SUP

        SQL = "SELECT *,0 AS CUS_CHECK FROM CUSTOMER"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT_CUS = New DataTable
        DA.Fill(DT_CUS)
        GridCUS.DataSource = DT_CUS

        SQL = "SELECT * FROM USER_SUPPLIER"
        DT_USER_SUP = New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_USER_SUP)

        SQL = "SELECT * FROM USER_CUSTOMER"
        DT_USER_CUS = New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_USER_CUS)

        SQL = "SELECT * FROM USER_MENU"
        DT_USER_MENU = New DataTable
        DA = New SqlDataAdapter(SQL, ConnStr)
        DA.Fill(DT_USER_MENU)

        SQL = "SELECT US_ID,US_CODE,US_NAME,US_PASS,ACTIVE_STATUS AS VAL_STATUS,CASE WHEN ACTIVE_STATUS = 1 THEN 'Active' ELSE 'Inactive' END ACTIVE_STATUS FROM [USER]"
        DA = New SqlDataAdapter(SQL, ConnStr)
        DT_USER = New DataTable
        DA.Fill(DT_USER)
        GridUser.DataSource = DT_USER
        lblRec.Text = "" ' "Total   " & DT_USER.Rows.Count & "   Record"

        FilterUser()
    End Sub

    Sub EnableForm()
        pnlUser.Enabled = False
        pnlUserData.Enabled = True
        pnlSup.Enabled = True
        pnlCus.Enabled = True
        pnlMenu.Enabled = True

        GridUser.DefaultCellStyle.ForeColor = Color.Gray
        GridUser.DefaultCellStyle.SelectionForeColor = Color.Gray
        GridSUP.DefaultCellStyle.ForeColor = Color.Black
        GridSUP.DefaultCellStyle.SelectionForeColor = Color.Black
        GridCUS.DefaultCellStyle.ForeColor = Color.Black
        GridCUS.DefaultCellStyle.SelectionForeColor = Color.Black
        GridMenu.DefaultCellStyle.ForeColor = Color.Black
        GridMenu.DefaultCellStyle.SelectionForeColor = Color.Black

        txtName.Focus()
    End Sub

    Sub Clear()
        pnlUser.Enabled = True
        pnlUserData.Enabled = False
        pnlSup.Enabled = False
        pnlCus.Enabled = False
        pnlMenu.Enabled = False

        GridUser.DefaultCellStyle.ForeColor = Color.Black
        GridUser.DefaultCellStyle.SelectionForeColor = Color.White
        GridSUP.DefaultCellStyle.ForeColor = Color.Gray
        GridSUP.DefaultCellStyle.SelectionForeColor = Color.Gray
        GridCUS.DefaultCellStyle.ForeColor = Color.Gray
        GridCUS.DefaultCellStyle.SelectionForeColor = Color.Gray
        GridMenu.DefaultCellStyle.ForeColor = Color.Gray
        GridMenu.DefaultCellStyle.SelectionForeColor = Color.Gray
        cb_sup_all.Checked = False
        cb_sup_all.Checked = False
        cb_menu_all.Checked = False
        rb_sup_all.Checked = True
        rb_cus_all.Checked = True
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If txtCode.Text.Trim = "" Then
            MessageBox.Show("Please enter User Code / Login", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtCode.Focus()
            Exit Sub
        End If
        If txtPassword.Text.Trim = "" Then
            MessageBox.Show("Please enter Password", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtPassword.Focus()
            Exit Sub
        End If
        If txtName.Text.Trim = "" Then
            MessageBox.Show("Please enter Fullname", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtName.Focus()
            Exit Sub
        End If

        Dim SQL As String = ""
        If txtCode.Tag = "" Then
            SQL = "SELECT * FROM [USER] WHERE US_CODE='" & txtCode.Text.Replace("'", "''") & "' AND ACTIVE_STATUS = 1"
        Else
            SQL = "SELECT * FROM [USER] WHERE US_CODE='" & txtCode.Text.Replace("'", "''") & "' AND ACTIVE_STATUS = 1 AND US_ID <> " & txtCode.Tag
        End If
        Dim DA As New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        If DT.Rows.Count > 0 Then
            MessageBox.Show("This User Code / Login is already exists", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            txtCode.Focus()
            Exit Sub
        End If

        Dim DT_SUP As New DataTable
        DT_SUP.Columns.Add("SUP_ID")
        For i As Int32 = 0 To GridSUP.Rows.Count - 1
            If GridSUP.Rows(i).Cells("SUP_CHECK").Value.ToString <> "" Then
                If CBool(GridSUP.Rows(i).Cells("SUP_CHECK").Value) = True Then
                    Dim DR As DataRow
                    DR = DT_SUP.NewRow
                    DR("SUP_ID") = GridSUP.Rows(i).Cells("SUP_ID").Value
                    DT_SUP.Rows.Add(DR)
                End If
            End If

        Next
        If DT_SUP.Rows.Count = 0 Then
            MessageBox.Show("Please select supplier", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim DT_CUS As New DataTable
        DT_CUS.Columns.Add("CUS_ID")
        For i As Int32 = 0 To GridCUS.Rows.Count - 1
            If GridCUS.Rows(i).Cells("CUS_CHECK").Value.ToString <> "" Then
                If CBool(GridCUS.Rows(i).Cells("CUS_CHECK").Value) = True Then
                    Dim DR As DataRow
                    DR = DT_CUS.NewRow
                    DR("CUS_ID") = GridCUS.Rows(i).Cells("CUS_ID").Value
                    DT_CUS.Rows.Add(DR)
                End If
            End If

        Next
        If DT_CUS.Rows.Count = 0 Then
            MessageBox.Show("Please select customer", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim DT_MENU As New DataTable
        DT_MENU.Columns.Add("MENU_ID")
        For i As Int32 = 0 To GridMenu.Rows.Count - 1
            If GridMenu.Rows(i).Cells("MENU_CHECK").Value.ToString <> "" Then
                If CBool(GridMenu.Rows(i).Cells("MENU_CHECK").Value) = True Then
                    Dim DR As DataRow
                    DR = DT_MENU.NewRow
                    DR("MENU_ID") = GridMenu.Rows(i).Cells("MENU_ID").Value
                    DT_MENU.Rows.Add(DR)
                End If
            End If

        Next
        If DT_CUS.Rows.Count = 0 Then
            MessageBox.Show("Please select menu", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim US_ID As String = ""
        If txtCode.Tag = "" Then
            'Save
            SQL = "SELECT * FROM [USER] WHERE 1=0"
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            Dim DR As DataRow
            DR = DT.NewRow
            US_ID = GetNewID("[USER]", "US_ID")
            DR("US_ID") = US_ID
            DR("US_NAME") = txtName.Text
            DR("US_CODE") = txtCode.Text
            DR("US_PASS") = txtPassword.Text
            DR("ACTIVE_STATUS") = cbbStatus.SelectedValue
            DR("UPDATE_BY") = myUser.user_id
            DR("UPDATE_DATE") = Now
            DT.Rows.Add(DR)
            Dim cmb As New SqlCommandBuilder(DA)
            DA.Update(DT)

        Else
            'Update
            US_ID = txtCode.Tag
            SQL = "SELECT * FROM [USER] WHERE US_ID = " & txtCode.Tag
            DA = New SqlDataAdapter(SQL, ConnStr)
            DT = New DataTable
            DA.Fill(DT)
            If DT.Rows.Count > 0 Then
                Dim DR As DataRow
                DR = DT.Rows(0)
                DR("US_NAME") = txtName.Text
                DR("US_CODE") = txtCode.Text
                DR("US_PASS") = txtPassword.Text
                DR("ACTIVE_STATUS") = cbbStatus.SelectedValue
                DR("UPDATE_BY") = myUser.user_id
                DR("UPDATE_DATE") = Now
                Dim cmb As New SqlCommandBuilder(DA)
                DA.Update(DT)
            End If

        End If



        Dim conn As New SqlConnection(ConnStr)
        Dim cmd As New SqlCommand
        conn.Open()

        SQL = ""
        SQL &= "DELETE FROM USER_SUPPLIER WHERE US_ID = " & US_ID & vbCrLf
        SQL &= "DELETE FROM USER_CUSTOMER WHERE US_ID = " & US_ID & vbCrLf
        SQL &= "DELETE FROM USER_MENU WHERE US_ID = " & US_ID & vbCrLf
        cmd = New SqlCommand
        cmd.CommandType = CommandType.Text
        cmd.CommandText = SQL
        cmd.Connection = conn
        cmd.ExecuteNonQuery()

        For i As Int32 = 0 To DT_SUP.Rows.Count - 1
            SQL = "INSERT INTO USER_SUPPLIER(US_ID,SUP_ID) VALUES(" & US_ID & "," & DT_SUP.Rows(i).Item("SUP_ID").ToString & ")"
            cmd = New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = SQL
            cmd.Connection = conn
            cmd.ExecuteNonQuery()
        Next

        For i As Int32 = 0 To DT_CUS.Rows.Count - 1
            SQL = "INSERT INTO USER_CUSTOMER(US_ID,CUS_ID) VALUES(" & US_ID & "," & DT_CUS.Rows(i).Item("CUS_ID").ToString & ")"
            cmd = New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = SQL
            cmd.Connection = conn
            cmd.ExecuteNonQuery()
        Next

        For i As Int32 = 0 To DT_MENU.Rows.Count - 1
            SQL = "INSERT INTO USER_MENU(US_ID,MENU_ID) VALUES(" & US_ID & "," & DT_MENU.Rows(i).Item("MENU_ID").ToString & ")"
            cmd = New SqlCommand
            cmd.CommandType = CommandType.Text
            cmd.CommandText = SQL
            cmd.Connection = conn
            cmd.ExecuteNonQuery()
        Next
        conn.Close()
        MessageBox.Show("Save success.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
        ShowData()
        btnCancel.PerformClick()

    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs)
        Dim f As New frmFilterUser
        If f.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtCode.Tag = f.USER_ID
            Dim SQL As String = ""
            SQL = "SELECT * FROM [USER] WHERE US_ID = " & txtCode.Tag
            Dim DA As New SqlDataAdapter(SQL, ConnStr)
            Dim DT As New DataTable
            DA.Fill(DT)

            SQL = "SELECT SUPPLIER.SUP_ID,SUP_NAME,CASE WHEN US.SUP_ID IS NULL THEN 0 ELSE 1 END AS SUP_CHECK FROM SUPPLIER LEFT JOIN (SELECT SUP_ID FROM USER_SUPPLIER WHERE US_ID = " & txtCode.Tag & ") US ON SUPPLIER.SUP_ID = US.SUP_ID"
            Dim DT_SUP As New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(DT_SUP)

            SQL = "SELECT CUSTOMER.CUS_ID,CUS_NAME,CASE WHEN US.CUS_ID IS NULL THEN 0 ELSE 1 END AS CUS_CHECK FROM CUSTOMER LEFT JOIN (SELECT CUS_ID FROM USER_CUSTOMER WHERE US_ID = " & txtCode.Tag & ") US ON CUSTOMER.CUS_ID = US.CUS_ID"
            Dim DT_CUS As New DataTable
            DA = New SqlDataAdapter(SQL, ConnStr)
            DA.Fill(DT_CUS)

            txtCode.Text = DT.Rows(0).Item("US_CODE").ToString
            txtPassword.Text = DT.Rows(0).Item("US_PASS").ToString
            txtName.Text = DT.Rows(0).Item("US_NAME").ToString
            cbbStatus.SelectedValue = DT.Rows(0).Item("ACTIVE_STATUS")
            GridSUP.DataSource = DT_SUP
            GridCUS.DataSource = DT_CUS
        End If
    End Sub

#Region "Filter"
    Sub FilterUser()
        If DT_USER.Rows.Count > 0 Then
            Dim Filter As String = ""
            Filter &= "US_NAME LIKE '%" & cbbFilterUser.Text & "%' AND "
            Filter &= "VAL_STATUS = " & cbbFilterStatus.SelectedValue
            DT_USER.DefaultView.RowFilter = Filter
        End If
    End Sub
    Private Sub cbbFilterUser_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbFilterUser.SelectedIndexChanged, cbbFilterStatus.SelectedIndexChanged, cbbFilterSup.SelectedIndexChanged
        FilterUser()
    End Sub
    Private Sub cbbFilterUser_TextChanged(sender As Object, e As System.EventArgs) Handles cbbFilterUser.TextChanged, cbbFilterStatus.TextChanged, cbbFilterSup.TextChanged
        FilterUser()
    End Sub

    Sub FilterSupplier()
        If DT_SUP.Rows.Count > 0 Then
            Dim Filter As String = ""
            Filter &= "SUP_NAME LIKE '%" & cbbFilterSup.Text & "%'"
            If rb_sub_check.Checked Then
                Filter &= "AND SUP_CHECK = 1"
            ElseIf rb_sup_notcheck.Checked Then
                Filter &= "AND SUP_CHECK = 0"
            End If
            DT_SUP.DefaultView.RowFilter = Filter
        End If
    End Sub
    Private Sub cbbFilterSup_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbFilterSup.SelectedIndexChanged
        FilterSupplier()
    End Sub
    Private Sub cbbFilterSup_TextChanged(sender As Object, e As System.EventArgs) Handles cbbFilterSup.TextChanged
        FilterSupplier()
    End Sub
    Private Sub rp_sup_all_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rb_sup_all.CheckedChanged, rb_sub_check.CheckedChanged, rb_sup_notcheck.CheckedChanged
        FilterSupplier()
    End Sub

    Sub FilterCustomer()
        If DT_CUS.Rows.Count > 0 Then
            Dim Filter As String = ""
            Filter &= "CUS_NAME LIKE '%" & cbbFilterCus.Text & "%'"
            If rb_cus_check.Checked Then
                Filter &= "AND CUS_CHECK = 1"
            ElseIf rb_cus_notcheck.Checked Then
                Filter &= "AND CUS_CHECK = 0"
            End If
            DT_CUS.DefaultView.RowFilter = Filter
        End If
    End Sub
    Private Sub cbbFilterCus_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbbFilterCus.SelectedIndexChanged
        FilterCustomer()
    End Sub
    Private Sub cbbFilterCus_TextChanged(sender As Object, e As System.EventArgs) Handles cbbFilterCus.TextChanged
        FilterCustomer()
    End Sub
    Private Sub rp_Cus_all_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rb_cus_all.CheckedChanged, rb_cus_check.CheckedChanged, rb_cus_notcheck.CheckedChanged, rp_cus_all.CheckedChanged
        FilterCustomer()
    End Sub
#End Region

    Private Sub GridUser_CellMouseDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles GridUser.CellMouseDoubleClick
        If e.RowIndex < 0 Then Exit Sub
        btnEdit.PerformClick()
    End Sub

    Private Sub GridUser_SelectionChanged(sender As Object, e As System.EventArgs) Handles GridUser.SelectionChanged
        If GridUser.Rows.Count > 0 Then
            Dim Rows As Int32 = GridUser.CurrentRow.Index
            txtCode.Tag = GridUser.Rows(Rows).Cells("US_ID").Value.ToString
            txtCode.Text = GridUser.Rows(Rows).Cells("US_CODE").Value.ToString
            txtPassword.Text = GridUser.Rows(Rows).Cells("US_PASS").Value.ToString
            txtName.Text = GridUser.Rows(Rows).Cells("US_NAME").Value.ToString
            If CBool(GridUser.Rows(Rows).Cells("VAL_STATUS").Value.ToString) = True Then
                cbbStatus.SelectedIndex = 0
            Else
                cbbStatus.SelectedIndex = 1
            End If

            Dim DT As New DataTable
            DT_USER_SUP.DefaultView.RowFilter = "US_ID = " & txtCode.Tag
            DT = DT_USER_SUP.DefaultView.ToTable
            DT_USER_SUP.DefaultView.RowFilter = ""
            For i As Int32 = 0 To DT_SUP.Rows.Count - 1
                DT.DefaultView.RowFilter = "SUP_ID = " & DT_SUP.Rows(i).Item("SUP_ID").ToString
                If DT.DefaultView.Count > 0 Then
                    DT_SUP.Rows(i).Item("SUP_CHECK") = True
                Else
                    DT_SUP.Rows(i).Item("SUP_CHECK") = False
                End If
                DT.DefaultView.RowFilter = ""
            Next

            DT = New DataTable
            DT_USER_CUS.DefaultView.RowFilter = "US_ID = " & txtCode.Tag
            DT = DT_USER_CUS.DefaultView.ToTable
            DT_USER_CUS.DefaultView.RowFilter = ""
            For i As Int32 = 0 To DT_CUS.Rows.Count - 1
                DT.DefaultView.RowFilter = "CUS_ID = " & DT_CUS.Rows(i).Item("CUS_ID")
                If DT.DefaultView.Count > 0 Then
                    DT_CUS.Rows(i).Item("CUS_CHECK") = True
                Else
                    DT_CUS.Rows(i).Item("CUS_CHECK") = False
                End If
                DT.DefaultView.RowFilter = ""
            Next

            DT = New DataTable
            DT_USER_MENU.DefaultView.RowFilter = "US_ID = " & txtCode.Tag
            DT = DT_USER_MENU.DefaultView.ToTable
            DT_USER_MENU.DefaultView.RowFilter = ""
            For i As Int32 = 0 To DT_MENU.Rows.Count - 1
                DT.DefaultView.RowFilter = "MENU_ID = " & DT_MENU.Rows(i).Item("MENU_ID")
                If DT.DefaultView.Count > 0 Then
                    DT_MENU.Rows(i).Item("MENU_CHECK") = True
                Else
                    DT_MENU.Rows(i).Item("MENU_CHECK") = False
                End If
                DT.DefaultView.RowFilter = ""
            Next
        Else
            txtCode.Text = ""
            txtCode.Tag = ""
            txtPassword.Text = ""
            txtName.Text = ""
            cbbStatus.SelectedValue = 1

            For i As Int32 = 0 To DT_SUP.Rows.Count - 1
                DT_SUP.Rows(i).Item("SUP_CHECK") = False
            Next
            For i As Int32 = 0 To DT_CUS.Rows.Count - 1
                DT_CUS.Rows(i).Item("CUS_CHECK") = False
            Next
            For i As Int32 = 0 To DT_MENU.Rows.Count - 1
                DT_MENU.Rows(i).Item("MENU_CHECK") = False
            Next
        End If
    End Sub

    Private Sub cb_sup_all_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cb_sup_all.CheckedChanged
        For i As Int32 = 0 To DT_SUP.Rows.Count - 1
            If cb_sup_all.Checked Then
                DT_SUP.Rows(i).Item("SUP_CHECK") = True
            Else
                DT_SUP.Rows(i).Item("SUP_CHECK") = False
            End If
        Next
    End Sub

    Private Sub cb_cus_all_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cb_cus_all.CheckedChanged
        For i As Int32 = 0 To DT_CUS.Rows.Count - 1
            If cb_cus_all.Checked Then
                DT_CUS.Rows(i).Item("CUS_CHECK") = True
            Else
                DT_CUS.Rows(i).Item("CUS_CHECK") = False
            End If
        Next
    End Sub

    Private Sub cb_menu_all_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles cb_menu_all.CheckedChanged
        For i As Int32 = 0 To DT_MENU.Rows.Count - 1
            If cb_menu_all.Checked Then
                DT_MENU.Rows(i).Item("MENU_CHECK") = True
            Else
                DT_MENU.Rows(i).Item("MENU_CHECK") = False
            End If
        Next
    End Sub
End Class
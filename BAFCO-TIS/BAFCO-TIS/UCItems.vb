﻿Public Class UCItems

    Private Sub UCItems_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        BindData()
        'AddHandler UCSelectedItem.btnClick, AddressOf UCSelectedItem_btnClick
    End Sub

    Private Sub UCSelectedItem_btnClick(id As String)
        Dim _id As String = id
    End Sub

    Sub BindData()
        Dim dt As New DataTable
        dt.Columns.Add("id")
        dt.Columns.Add("name")

        Dim dr As DataRow
        dr = dt.NewRow
        dr("id") = "1"
        dr("name") = "Supplier1"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("id") = "2"
        dr("name") = "Supplier2"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("id") = "3"
        dr("name") = "Supplier3"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("id") = "4"
        dr("name") = "Supplier4"
        dt.Rows.Add(dr)

        dr = dt.NewRow
        dr("id") = "5"
        dr("name") = "Supplier5"
        dt.Rows.Add(dr)

        For i As Integer = 0 To dt.Rows.Count - 1
            Dim UCItem1 As New UCSelectedItem
            UCItem1.strID = dt.Rows(i)("id")
            UCItem1.strName = dt.Rows(i)("name")
            Flow.Controls.Add(UCItem1)
        Next

    End Sub

End Class

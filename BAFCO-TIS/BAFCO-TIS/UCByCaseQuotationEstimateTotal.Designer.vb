﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UCByCaseQuotationEstimateTotal
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.flpEstimateTotal = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.lblEstimateGrandTotal = New System.Windows.Forms.Label()
        Me.txtEstimateGrandTotal = New System.Windows.Forms.TextBox()
        Me.cbbEstimateCurrency = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'flpEstimateTotal
        '
        Me.flpEstimateTotal.AutoScroll = True
        Me.flpEstimateTotal.Location = New System.Drawing.Point(121, 0)
        Me.flpEstimateTotal.Name = "flpEstimateTotal"
        Me.flpEstimateTotal.Size = New System.Drawing.Size(206, 62)
        Me.flpEstimateTotal.TabIndex = 2
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label41.Location = New System.Drawing.Point(71, 7)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(47, 13)
        Me.Label41.TabIndex = 141
        Me.Label41.Text = "TOTAL"
        '
        'lblEstimateGrandTotal
        '
        Me.lblEstimateGrandTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblEstimateGrandTotal.ForeColor = System.Drawing.Color.Red
        Me.lblEstimateGrandTotal.Location = New System.Drawing.Point(1, 64)
        Me.lblEstimateGrandTotal.Name = "lblEstimateGrandTotal"
        Me.lblEstimateGrandTotal.Size = New System.Drawing.Size(114, 24)
        Me.lblEstimateGrandTotal.TabIndex = 142
        Me.lblEstimateGrandTotal.Text = "EST GRAND TOT."
        Me.lblEstimateGrandTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblEstimateGrandTotal.Visible = False
        '
        'txtEstimateGrandTotal
        '
        Me.txtEstimateGrandTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtEstimateGrandTotal.Location = New System.Drawing.Point(121, 63)
        Me.txtEstimateGrandTotal.Name = "txtEstimateGrandTotal"
        Me.txtEstimateGrandTotal.Size = New System.Drawing.Size(124, 26)
        Me.txtEstimateGrandTotal.TabIndex = 0
        Me.txtEstimateGrandTotal.Text = "0"
        Me.txtEstimateGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEstimateGrandTotal.Visible = False
        '
        'cbbEstimateCurrency
        '
        Me.cbbEstimateCurrency.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbEstimateCurrency.FormattingEnabled = True
        Me.cbbEstimateCurrency.Location = New System.Drawing.Point(246, 63)
        Me.cbbEstimateCurrency.Name = "cbbEstimateCurrency"
        Me.cbbEstimateCurrency.Size = New System.Drawing.Size(81, 26)
        Me.cbbEstimateCurrency.TabIndex = 1
        Me.cbbEstimateCurrency.Visible = False
        '
        'UCByCaseQuotationEstimateTotal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.cbbEstimateCurrency)
        Me.Controls.Add(Me.txtEstimateGrandTotal)
        Me.Controls.Add(Me.lblEstimateGrandTotal)
        Me.Controls.Add(Me.Label41)
        Me.Controls.Add(Me.flpEstimateTotal)
        Me.Name = "UCByCaseQuotationEstimateTotal"
        Me.Size = New System.Drawing.Size(328, 90)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents flpEstimateTotal As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents lblEstimateGrandTotal As System.Windows.Forms.Label
    Friend WithEvents txtEstimateGrandTotal As System.Windows.Forms.TextBox
    Friend WithEvents cbbEstimateCurrency As System.Windows.Forms.ComboBox

End Class

﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class Transportation_Sup

    Public ID As Integer

    Dim _ReportName As String
    Dim _Supplier As String
    Dim _Customer As String
    Dim _Vehicle As String
    Dim _From As String
    Dim _To As String
    Dim _DT As New DataTable

    Dim _DTSUP As New DataTable
    Dim _DTCUS As New DataTable

#Region "Property"
    Public WriteOnly Property ReportName As String
        Set(value As String)
            _ReportName = value
        End Set
    End Property

    Public WriteOnly Property DTSUP As DataTable
        Set(value As DataTable)
            _DTSUP = value
        End Set
    End Property

    Public WriteOnly Property DTCUS As DataTable
        Set(value As DataTable)
            _DTCUS = value
        End Set
    End Property

    Public WriteOnly Property Supplier As String
        Set(value As String)
            _Supplier = value
        End Set
    End Property

    Public WriteOnly Property Customer As String
        Set(value As String)
            _Customer = value
        End Set
    End Property

    Public WriteOnly Property Vehicle As String
        Set(value As String)
            _Vehicle = value
        End Set
    End Property

    Public WriteOnly Property LocationFrom As String
        Set(value As String)
            _From = value
        End Set
    End Property

    Public WriteOnly Property LocationTo As String
        Set(value As String)
            _To = value
        End Set
    End Property
#End Region


    Private Sub Transportation_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim cc As New ReportDocument()
        cc.Load(Application.StartupPath & "/Report/" & _ReportName)
        cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        Select Case _ReportName
            Case "COST_SUP.rpt"
                cc.DataDefinition.FormulaFields("FromSup").Text = "'" + _From + "'"
                cc.DataDefinition.FormulaFields("ToSup").Text = "'" + _To + "'"
                cc.DataDefinition.FormulaFields("Vehicle").Text = "'" + _Vehicle + "'"
                cc.DataDefinition.FormulaFields("Supplier").Text = "'" + _Supplier + "'"
                cc.DataDefinition.FormulaFields("CountRow").Text = "'" + _DTSUP.Rows.Count.ToString("##,##0") + "'"
                cc.SetDataSource(_DTSUP)
                cv.ReportSource = cc
            Case "Transportation_Sup.rpt"
                cc.SetDataSource(_DTSUP)
                cv.ReportSource = cc
            Case "COST_ALL.rpt"
                cc.DataDefinition.FormulaFields("FromSup").Text = "'" + _From + "'"
                cc.DataDefinition.FormulaFields("ToSup").Text = "'" + _To + "'"
                cc.DataDefinition.FormulaFields("Vehicle").Text = "'" + _Vehicle + "'"
                cc.DataDefinition.FormulaFields("Supplier").Text = "'" + _Supplier + "'"
                cc.DataDefinition.FormulaFields("Customer").Text = "'" + _Customer + "'"
                cc.DataDefinition.FormulaFields("CountRow").Text = "'" + _DTSUP.Rows.Count.ToString("##,##0") + "'"
                cc.SetDataSource(_DTSUP)
                cv.ReportSource = cc
                
        End Select
        cv.ReportSource = cc
        cv.Refresh()

        If myUser.export_report = False Then
            cv.ShowExportButton = False
            cv.ShowPrintButton = False
        End If

    End Sub
End Class
﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class Service_Cus

    Public ID As Integer
    Dim _ReportName As String
    Dim _ServiceType As String
    Dim _Service As String
    Dim _Customer As String
    Dim _DT As New DataTable

#Region "Property"
    Public WriteOnly Property ReportName As String
        Set(value As String)
            _ReportName = value
        End Set
    End Property

    Public WriteOnly Property DT As DataTable
        Set(value As DataTable)
            _DT = value
        End Set
    End Property

    Public WriteOnly Property Customer As String
        Set(value As String)
            _Customer = value
        End Set
    End Property

    Public WriteOnly Property ServiceType As String
        Set(value As String)
            _ServiceType = value
        End Set
    End Property

    Public WriteOnly Property Service As String
        Set(value As String)
            _Service = value
        End Set
    End Property

   
#End Region

    Private Sub Service_Cus_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'Dim cc As New ReportDocument()
        'cc.Load(Application.StartupPath & "/Report/Service_Cus.rpt")
        'cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        'cc.SetParameterValue("ID", ID)
        'cv.ReportSource = cc

        Dim cc As New ReportDocument()
        cc.Load(Application.StartupPath & "/Report/" & _ReportName)
        cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        Select Case _ReportName
            Case "SERV_CUS.rpt"
                cc.DataDefinition.FormulaFields("ServiceType").Text = "'" + _ServiceType + "'"
                cc.DataDefinition.FormulaFields("Service").Text = "'" + _Service + "'"
                cc.DataDefinition.FormulaFields("Customer").Text = "'" + _Customer + "'"
                cc.DataDefinition.FormulaFields("CountRow").Text = "'" + _DT.Rows.Count.ToString("##,##0") + "'"
            Case "Service_Cus.rpt"
        End Select

        cc.SetDataSource(_DT)
        cv.ReportSource = cc
        cv.Refresh()

        If myUser.export_report = False Then
            cv.ShowExportButton = False
            cv.ShowPrintButton = False
        End If
    End Sub
End Class
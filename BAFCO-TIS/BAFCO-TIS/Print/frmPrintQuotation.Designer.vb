﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrintQuotation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cv = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.SuspendLayout()
        '
        'cv
        '
        Me.cv.ActiveViewIndex = -1
        Me.cv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.cv.Cursor = System.Windows.Forms.Cursors.Default
        Me.cv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cv.Location = New System.Drawing.Point(0, 0)
        Me.cv.Name = "cv"
        Me.cv.SelectionFormula = ""
        Me.cv.Size = New System.Drawing.Size(658, 493)
        Me.cv.TabIndex = 1
        Me.cv.ViewTimeSelectionFormula = ""
        '
        'frmPrintQuotation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(658, 493)
        Me.Controls.Add(Me.cv)
        Me.Name = "frmPrintQuotation"
        Me.ShowInTaskbar = False
        Me.Text = "Quotation"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cv As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class

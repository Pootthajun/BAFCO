﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class Transportation_Cus

    Public ID As Integer

    Dim _ReportName As String
    Dim _Customer As String
    Dim _Vehicle As String
    Dim _From As String
    Dim _To As String
    Dim _DT As New DataTable

#Region "Property"
    Public WriteOnly Property ReportName As String
        Set(value As String)
            _ReportName = value
        End Set
    End Property

    Public WriteOnly Property DT As DataTable
        Set(value As DataTable)
            _DT = value
        End Set
    End Property

    Public WriteOnly Property Customer As String
        Set(value As String)
            _Customer = value
        End Set
    End Property

    Public WriteOnly Property Vehicle As String
        Set(value As String)
            _Vehicle = value
        End Set
    End Property

    Public WriteOnly Property LocationFrom As String
        Set(value As String)
            _From = value
        End Set
    End Property

    Public WriteOnly Property LocationTo As String
        Set(value As String)
            _To = value
        End Set
    End Property
#End Region

    Private Sub Transportation_Cus_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        'Dim cc As New ReportDocument()
        'cc.Load(Application.StartupPath & "/Report/Transportation_Cus.rpt")
        'cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        'cc.SetParameterValue("ID", ID)
        'cv.ReportSource = cc

        Dim cc As New ReportDocument()
        cc.Load(Application.StartupPath & "/Report/" & _ReportName)
        cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        Select Case _ReportName
            Case "COST_CUS.rpt"
                cc.DataDefinition.FormulaFields("FromCus").Text = "'" + _From + "'"
                cc.DataDefinition.FormulaFields("ToCus").Text = "'" + _To + "'"
                cc.DataDefinition.FormulaFields("Vehicle").Text = "'" + _Vehicle + "'"
                cc.DataDefinition.FormulaFields("Customer").Text = "'" + _Customer + "'"
                cc.DataDefinition.FormulaFields("CountRow").Text = "'" + _DT.Rows.Count.ToString("##,##0") + "'"
            Case "Transportation_Cus.rpt"
        End Select

        cc.SetDataSource(_DT)
        cv.ReportSource = cc
        cv.Refresh()

        If myUser.export_report = False Then
            cv.ShowExportButton = False
            cv.ShowPrintButton = False
        End If
    End Sub
End Class
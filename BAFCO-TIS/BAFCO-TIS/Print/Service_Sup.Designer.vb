﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Service_Sup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cv = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.SuspendLayout()
        '
        'cv
        '
        Me.cv.ActiveViewIndex = -1
        Me.cv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.cv.Cursor = System.Windows.Forms.Cursors.Default
        Me.cv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cv.Location = New System.Drawing.Point(0, 0)
        Me.cv.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cv.Name = "cv"
        Me.cv.SelectionFormula = ""
        Me.cv.Size = New System.Drawing.Size(915, 648)
        Me.cv.TabIndex = 1
        Me.cv.ToolPanelWidth = 300
        Me.cv.ViewTimeSelectionFormula = ""
        '
        'Service_Sup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(915, 648)
        Me.Controls.Add(Me.cv)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Service_Sup"
        Me.ShowInTaskbar = False
        Me.Text = "Service"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cv As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class

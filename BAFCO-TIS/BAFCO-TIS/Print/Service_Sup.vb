﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class Service_Sup

    Public ID As Integer
    Dim _ReportName As String
    Dim _ServiceType As String
    Dim _Service As String
    Dim _Supplier As String
    Dim _Customer As String
    Dim _DTSUP As New DataTable
    Dim _DTCUS As New DataTable

#Region "Property"
    Public WriteOnly Property ReportName As String
        Set(value As String)
            _ReportName = value
        End Set
    End Property

    Public WriteOnly Property DTSUP As DataTable
        Set(value As DataTable)
            _DTSUP = value
        End Set
    End Property

    Public WriteOnly Property DTCUS As DataTable
        Set(value As DataTable)
            _DTCUS = value
        End Set
    End Property

    Public WriteOnly Property Supplier As String
        Set(value As String)
            _Supplier = value
        End Set
    End Property

    Public WriteOnly Property Customer As String
        Set(value As String)
            _Customer = value
        End Set
    End Property

    Public WriteOnly Property ServiceType As String
        Set(value As String)
            _ServiceType = value
        End Set
    End Property

    Public WriteOnly Property Service As String
        Set(value As String)
            _Service = value
        End Set
    End Property

   
#End Region

    Private Sub Service_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Dim cc As New ReportDocument()
        'cc.Load(Application.StartupPath & "/Report/Service_Sup.rpt")
        'cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        'cc.SetParameterValue("ID", ID)
        'cv.ReportSource = cc

        Dim cc As New ReportDocument()
        cc.Load(Application.StartupPath & "/Report/" & _ReportName)
        cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        Select Case _ReportName
            Case "SERV_SUP.rpt"
                cc.DataDefinition.FormulaFields("ServiceType").Text = "'" + _ServiceType + "'"
                cc.DataDefinition.FormulaFields("Service").Text = "'" + _Service + "'"
                cc.DataDefinition.FormulaFields("Supplier").Text = "'" + _Supplier + "'"
                cc.DataDefinition.FormulaFields("CountRow").Text = "'" + _DTSUP.Rows.Count.ToString("##,##0") + "'"
                cc.SetDataSource(_DTSUP)
                cv.ReportSource = cc
            Case "Service_Sup.rpt"
                cc.SetDataSource(_DTSUP)
                cv.ReportSource = cc
            Case "SERV_ALL.rpt"
                cc.DataDefinition.FormulaFields("ServiceType").Text = "'" + _ServiceType + "'"
                cc.DataDefinition.FormulaFields("Service").Text = "'" + _Service + "'"
                cc.DataDefinition.FormulaFields("Supplier").Text = "'" + _Supplier + "'"
                cc.DataDefinition.FormulaFields("Customer").Text = "'" + _Customer + "'"
                cc.DataDefinition.FormulaFields("CountRow").Text = "'" + _DTSUP.Rows.Count.ToString("##,##0") + "'"
                cc.SetDataSource(_DTSUP)
                cv.ReportSource = cc

                'cc.Subreports("SERV_SUP.rpt").Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                'cc.Subreports("SERV_SUP.rpt").DataDefinition.FormulaFields("ServiceType").Text = "'" + _ServiceType + "'"
                'cc.Subreports("SERV_SUP.rpt").DataDefinition.FormulaFields("Service").Text = "'" + _Service + "'"
                'cc.Subreports("SERV_SUP.rpt").DataDefinition.FormulaFields("Supplier").Text = "'" + _Supplier + "'"
                'cc.Subreports("SERV_SUP.rpt").SetDataSource(_DTSUP)

                'cc.Subreports("SERV_CUS.rpt").Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
                'cc.Subreports("SERV_CUS.rpt").DataDefinition.FormulaFields("ServiceType").Text = "'" + _ServiceType + "'"
                'cc.Subreports("SERV_CUS.rpt").DataDefinition.FormulaFields("Service").Text = "'" + _Service + "'"
                'cc.Subreports("SERV_CUS.rpt").DataDefinition.FormulaFields("Customer").Text = "'" + _Customer + "'"
                'cc.Subreports("SERV_CUS.rpt").SetDataSource(_DTCUS)

        End Select
        cv.ReportSource = cc
        cv.Refresh()

        If myUser.export_report = False Then
            cv.ShowExportButton = False
            cv.ShowPrintButton = False
        End If
    End Sub
End Class
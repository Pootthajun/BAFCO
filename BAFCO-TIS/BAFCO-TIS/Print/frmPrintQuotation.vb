﻿Imports CrystalDecisions.CrystalReports.Engine

Public Class frmPrintQuotation
    Private WithEvents kbHook As New KeyboardHook

    Dim _QuotationID As Long
    Dim _ReportName As String
    Dim _DT As New DataTable
    Dim _TotalDt As New DataTable

    Private Sub kbHook_KeyUp(ByVal Key As System.Windows.Forms.Keys) Handles kbHook.KeyUp
        If Key = 44 Then
            Clipboard.Clear()
        End If
    End Sub

#Region "Property"
    Public WriteOnly Property QuotationID As Long
        Set(value As Long)
            _QuotationID = value
        End Set
    End Property

    Public WriteOnly Property ReportName As String
        Set(value As String)
            _ReportName = value
        End Set
    End Property

    Public WriteOnly Property DT As DataTable
        Set(value As DataTable)
            _DT = value
        End Set
    End Property
    Public WriteOnly Property TotalDt As DataTable
        Set(value As DataTable)
            _TotalDt = value
        End Set
    End Property

#End Region

    Private Sub Transportation_Cus_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim cc As New ReportDocument()
        cc.Load(Application.StartupPath & "\Report\" & _ReportName)
        cc.Database.Tables(0).ApplyLogOnInfo(CrystalLogonInfo)
        Select Case _ReportName.ToUpper
            Case "QUOTATION_BYCASE.RPT"
                'cc.DataDefinition.FormulaFields("FromCus").Text = "'" + _From + "'"
                'cc.DataDefinition.FormulaFields("ToCus").Text = "'" + _To + "'"
                'cc.DataDefinition.FormulaFields("Vehicle").Text = "'" + _Vehicle + "'"
                'cc.DataDefinition.FormulaFields("Customer").Text = "'" + _Customer + "'"
                'cc.DataDefinition.FormulaFields("CountRow").Text = "'" + _DT.Rows.Count.ToString("##,##0") + "'"

                Dim sql As String = "select qbc.id, qcr.remarks "
                sql += " from QUOTATION_BYCASE_CONDITION qbc"
                sql += " inner join QT_CATEGORY_REMARK qcr on qcr.id=qbc.qt_category_remark_id"
                sql += " where qbc.quotation_bycase_id = @_QUOTATION_ID"
                sql += " order by qbc.id"

                Dim p(1) As SqlClient.SqlParameter
                p(0) = SetBigInt("@_QUOTATION_ID", _QuotationID)

                Dim termDt As DataTable = Execute_DataTable(sql, p)
                If termDt.Rows.Count > 0 Then
                    cc.Subreports("rptTermCondition").SetDataSource(termDt)
                End If

                If _TotalDt.Rows.Count > 0 Then
                    cc.Subreports("rptTotal").SetDataSource(_TotalDt)
                End If


            Case "QUOTATION_MAIN.RPT"
                'Transport Service Information
                Dim sql As String = ""
                sql = "select q.qt_no, qmd.id category_detail_id,rf.ROUTE_NAME route_from,  'From ' + rf.ROUTE_NAME + ' To ' + rt.ROUTE_NAME route_desc, " & Environment.NewLine
                sql += " mti.t4w, mti.t4ws, mti.t6w, mti.t6ws, mti.t10w, mti.ft20Trailer, mti.ft40Trailer, mti.Lowbed2Axel, " & Environment.NewLine
                sql += " mti.Lowbed3Axel, mti.AirSuspension" & Environment.NewLine
                sql += " from quotation_main_truck_item mti" & Environment.NewLine
                sql += " inner join quotation_main_detail qmd on qmd.id=mti.quotation_main_detail_id" & Environment.NewLine
                sql += " inner join quotation_main q on q.id=qmd.quotation_main_id" & Environment.NewLine
                sql += " inner join [ROUTE] rf on rf.ROUTE_ID=mti.route_id_from" & Environment.NewLine
                sql += " inner join [ROUTE] rt on rt.ROUTE_ID=mti.route_id_to" & Environment.NewLine
                sql += " where q.id=@_QUOTATION_ID" & Environment.NewLine
                Dim p(1) As SqlClient.SqlParameter
                p(0) = SetBigInt("@_QUOTATION_ID", _QuotationID)
                Dim truckDt As DataTable = Execute_DataTable(sql, p)
                cc.Subreports("rptCatTransport").SetDataSource(truckDt)


                'Description Information
                Sql = "select d.id, d.quotation_main_detail_id, qd.desc_name,  cr.currency_name, d.price ,bs.basis_name" & Environment.NewLine
                sql += " from QUOTATION_MAIN_DESC d" & Environment.NewLine
                sql += " inner join QT_CATEGORY_DESCRIPTION qd on qd.id=d.qt_category_description_id" & Environment.NewLine
                sql += " inner join MS_CURRENCY cr on cr.id=d.ms_currency_id" & Environment.NewLine
                sql += " inner join MS_BASIS bs on bs.id=d.ms_basis_id" & Environment.NewLine
                sql += " inner join QUOTATION_MAIN_DETAIL qmd on qmd.id=d.quotation_main_detail_id" & Environment.NewLine
                sql += " where qmd.quotation_main_id=@_QUOTATION_ID" & Environment.NewLine
                sql += " order by d.id"

                ReDim p(1)
                p(0) = SetBigInt("@_QUOTATION_ID", _QuotationID)

                Dim descDt As DataTable = Execute_DataTable(sql, p)
                cc.Subreports("rptCategoryDesc").SetDataSource(descDt)
                cc.Subreports("rptGroupDesc").SetDataSource(descDt)
                cc.Subreports("rptSubgroupDesc").SetDataSource(descDt)


                'Remark Information
                sql = " select  mr.quotation_main_detail_id, r.remarks " & Environment.NewLine
                sql += " from QUOTATION_MAIN_REMARK mr " & Environment.NewLine
                sql += " inner join QT_CATEGORY_REMARK r on r.id=mr.qt_category_remark_id " & Environment.NewLine
                sql += " inner join QUOTATION_MAIN_DETAIL qmd on qmd.id=mr.quotation_main_detail_id"
                sql += " where qmd.quotation_main_id=@_QUOTATION_ID" & Environment.NewLine
                sql += " order by mr.id " & Environment.NewLine
                ReDim p(1)
                p(0) = SetBigInt("@_QUOTATION_ID", _QuotationID)

                Dim remDt As DataTable = Execute_DataTable(sql, p)
                cc.Subreports("rptCategoryRemarks").SetDataSource(remDt)
                cc.Subreports("rptGroupRemarks").SetDataSource(remDt)
                cc.Subreports("rptSubgroupRemarks").SetDataSource(remDt)
        End Select

        cc.SetDataSource(_DT)
        cv.ReportSource = cc
        cv.Refresh()

        'If myUser.export_report = False Then
        '    cv.ShowExportButton = False
        '    cv.ShowPrintButton = False
        'End If
    End Sub
End Class
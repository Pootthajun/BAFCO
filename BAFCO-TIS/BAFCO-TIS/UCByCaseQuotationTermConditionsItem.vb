﻿Public Class UCByCaseQuotationTermConditionsItem

    Public Event DeleteConditionItem(ctl As UCByCaseQuotationTermConditionsItem)
    'Public QTCategoryID As Long = 0

    Public Sub SetDDlTermCondition()

        Dim sql As String = "select id, remarks "
        sql += " from QT_CATEGORY_REMARK "
        sql += " where active_status='Y' and user_by_case_qt='Y'"
        sql += " order by remarks"

        Dim dt As DataTable = Execute_DataTable(sql)
        If dt Is Nothing Then
            dt = New DataTable
            dt.Columns.Add("id")
            dt.Columns.Add("remarks")
        End If

        Dim dr As DataRow = dt.NewRow
        dr("id") = 0
        dr("remarks") = ""
        dt.Rows.InsertAt(dr, 0)

        cbbRemarks.DataSource = dt
        cbbRemarks.DisplayMember = "remarks"
        cbbRemarks.ValueMember = "id"
    End Sub

    Public WriteOnly Property EnableRemarkItem() As Boolean
        Set(value As Boolean)
            cbbRemarks.Enabled = value
            btnDelete.Visible = value
        End Set
    End Property

    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        RaiseEvent DeleteConditionItem(Me)
    End Sub
End Class

﻿Public Class UCByCaseQuotationTruckDescription

    Public Event HeightChanged(h As Integer)
    Public Event PriceTotalChange()
    'Public Event SelectDescription(ddlDescription As ComboBox, txtPrice As TextBox, ddlBasis As ComboBox, ddlCurrency As ComboBox)

    Public WriteOnly Property EnableDescription As Boolean
        Set(value As Boolean)
            btnAddDescription.Visible = value

            If flpTruckDescription.Controls.Count > 0 Then
                For Each ctl As UCByCaseQuotationTruckDescriptionItem In flpTruckDescription.Controls
                    ctl.EnableDescriptionItem = value
                Next
            End If
        End Set
    End Property


    Public Sub AddTruckDescriptionItem()
        Dim uc As New UCByCaseQuotationTruckDescriptionItem
        uc.SetDDL()
        AddHandler uc.DeleteTruckItem, AddressOf UCByCaseQuotationTruckDescriptionItem_DeleteTruckItem
        AddHandler uc.PriceTotalChange, AddressOf UCByCaseQuotationTruckDescriptionItem_PriceTotalChange

        flpTruckDescription.Controls.Add(uc)
    End Sub


    Private Sub UCByCaseQuotationTruckDescriptionItem_PriceTotalChange()
        RaiseEvent PriceTotalChange()
    End Sub

    Public Sub ClearTruckDescriptionItem()
        flpTruckDescription.Controls.Clear()
    End Sub

    Public Function ValidateData() As Boolean
        Dim ret As Boolean = True
        If flpTruckDescription.Controls.Count > 0 Then
            For Each des As UCByCaseQuotationTruckDescriptionItem In flpTruckDescription.Controls
                If des.cbbRouteFrom.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก From", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbRouteFrom.Focus()
                    Return False
                End If

                If des.cbbRouteTo.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก To", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbRouteTo.Focus()
                    Return False
                End If

                If des.cbbVehicleType.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Type", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbVehicleType.Focus()
                    Return False
                End If

                If des.txtUnitRate.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Unit Rate", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtUnitRate.Focus()
                    Return False
                End If

                If des.cbbBasis.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Basis", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbBasis.Focus()
                    Return False
                End If

                If des.txtQty.Text = "" Then
                    MessageBox.Show("กรุณาระบุ Qty", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.txtQty.Focus()
                    Return False
                End If

                If des.cbbCurrency.SelectedValue = 0 Then
                    MessageBox.Show("กรุณาเลือก Currency", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    des.cbbCurrency.Focus()
                    Return False
                End If
            Next
        End If
        Return ret
    End Function


    Public Sub FillInTruckDescriptionData(ByCaseQuotationDetailID As Long)
        Dim Sql As String = " select quotation_bycase_id,route_id_from,route_id_to,vehicle_type_id,ms_basis_id,unit_rate,qty,ms_currency_id,item_note"
        Sql += " from QUOTATION_BYCASE_TRUCK_ITEM "
        Sql += " where quotation_bycase_id=@_QUOTATION_BYCASE_ID"
        Sql += " order by id "

        Dim p(1) As SqlClient.SqlParameter
        p(0) = SetBigInt("@_QUOTATION_BYCASE_ID", ByCaseQuotationDetailID)
        Dim dt As DataTable = Execute_DataTable(Sql, p)
        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows
                Dim uc As New UCByCaseQuotationTruckDescriptionItem
                uc.SetDDL()
                uc.cbbRouteFrom.SelectedValue = dr("route_id_from")
                uc.cbbRouteTo.SelectedValue = dr("route_id_to")
                uc.cbbVehicleType.SelectedValue = dr("vehicle_type_id")
                uc.txtUnitRate.Text = dr("unit_rate")
                uc.cbbBasis.SelectedValue = dr("ms_basis_id")
                uc.txtQty.Text = dr("qty")
                uc.txtTotal.Text = (Convert.ToDouble(dr("unit_rate")) * Convert.ToInt64(dr("qty"))).ToString("#,##0.00")
                uc.cbbCurrency.SelectedValue = dr("ms_currency_id")
                If Convert.IsDBNull(dr("item_note")) = False Then uc.lblNote.Text = dr("item_note")

                AddHandler uc.DeleteTruckItem, AddressOf UCByCaseQuotationTruckDescriptionItem_DeleteTruckItem
                AddHandler uc.PriceTotalChange, AddressOf UCByCaseQuotationTruckDescriptionItem_PriceTotalChange

                flpTruckDescription.Controls.Add(uc)

                If uc.Height * flpTruckDescription.Controls.Count > flpTruckDescription.Height Then
                    Dim plusHeight As Integer = uc.Height

                    flpTruckDescription.Height += plusHeight
                    Me.Height += plusHeight

                    RaiseEvent HeightChanged(plusHeight)
                End If
            Next

            RaiseEvent PriceTotalChange()
        End If
        dt.Dispose()

    End Sub

    Private Sub btnAddDescription_Click(sender As System.Object, e As System.EventArgs) Handles btnAddDescription.Click
        AddTruckDescriptionItem()
    End Sub

    Private Sub UCByCaseQuotationTruckDescriptionItem_DeleteTruckItem(ctl As UCByCaseQuotationTruckDescriptionItem)
        Dim plusHeight As Integer = ctl.Height

        flpTruckDescription.Controls.Remove(ctl)
        RaiseEvent PriceTotalChange()
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSearchCost
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GridCus = New System.Windows.Forms.DataGridView()
        Me.cbbTo = New System.Windows.Forms.ComboBox()
        Me.GridSup = New System.Windows.Forms.DataGridView()
        Me.S_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_FROM_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_FROM_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_TO_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_TO_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_VEH_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_VEH_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SUP_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_SUP_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.S_COST = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CostUnit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cbbFrom = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.flpVEH = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.flowSupplier = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnPrintSupplier = New System.Windows.Forms.Button()
        Me.btnS_Add = New System.Windows.Forms.Button()
        Me.flowCustomer = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnPrintCustomer = New System.Windows.Forms.Button()
        Me.btnPrintAll = New System.Windows.Forms.Button()
        Me.btnC_Add = New System.Windows.Forms.Button()
        Me.cbAllVehicle = New System.Windows.Forms.CheckBox()
        Me.btnAddSupplier = New System.Windows.Forms.Button()
        Me.btnAddCustomer = New System.Windows.Forms.Button()
        Me.C_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_FROM_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_FROM_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_TO_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_TO_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_VEH_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_VEH_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_CUS_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_CUS_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.C_COST = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ValueUnit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colQtStatusValue = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colQtStatus = New System.Windows.Forms.DataGridViewImageColumn()
        CType(Me.GridCus, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridSup, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GridCus
        '
        Me.GridCus.AllowUserToAddRows = False
        Me.GridCus.AllowUserToDeleteRows = False
        Me.GridCus.AllowUserToResizeRows = False
        Me.GridCus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridCus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridCus.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GridCus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridCus.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.C_ID, Me.C_FROM_ID, Me.C_FROM_NAME, Me.C_TO_ID, Me.C_TO_NAME, Me.C_VEH_ID, Me.C_VEH_NAME, Me.C_CUS_ID, Me.C_CUS_NAME, Me.C_COST, Me.ValueUnit, Me.colQtStatusValue, Me.colQtStatus})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridCus.DefaultCellStyle = DataGridViewCellStyle3
        Me.GridCus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.GridCus.Location = New System.Drawing.Point(4, 56)
        Me.GridCus.Margin = New System.Windows.Forms.Padding(4)
        Me.GridCus.MultiSelect = False
        Me.GridCus.Name = "GridCus"
        Me.GridCus.ReadOnly = True
        Me.GridCus.RowHeadersVisible = False
        Me.GridCus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridCus.Size = New System.Drawing.Size(506, 434)
        Me.GridCus.TabIndex = 55
        '
        'cbbTo
        '
        Me.cbbTo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbTo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbTo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbTo.FormattingEnabled = True
        Me.cbbTo.Location = New System.Drawing.Point(300, 11)
        Me.cbbTo.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbTo.Name = "cbbTo"
        Me.cbbTo.Size = New System.Drawing.Size(160, 24)
        Me.cbbTo.TabIndex = 48
        '
        'GridSup
        '
        Me.GridSup.AllowUserToAddRows = False
        Me.GridSup.AllowUserToDeleteRows = False
        Me.GridSup.AllowUserToResizeRows = False
        Me.GridSup.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridSup.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridSup.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.GridSup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridSup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.S_ID, Me.S_FROM_ID, Me.S_FROM_NAME, Me.S_TO_ID, Me.S_TO_NAME, Me.S_VEH_ID, Me.S_VEH_NAME, Me.S_SUP_ID, Me.S_SUP_NAME, Me.S_COST, Me.CostUnit})
        Me.GridSup.Cursor = System.Windows.Forms.Cursors.Hand
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridSup.DefaultCellStyle = DataGridViewCellStyle6
        Me.GridSup.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.GridSup.Location = New System.Drawing.Point(4, 56)
        Me.GridSup.Margin = New System.Windows.Forms.Padding(4)
        Me.GridSup.MultiSelect = False
        Me.GridSup.Name = "GridSup"
        Me.GridSup.ReadOnly = True
        Me.GridSup.RowHeadersVisible = False
        Me.GridSup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridSup.Size = New System.Drawing.Size(480, 434)
        Me.GridSup.TabIndex = 44
        '
        'S_ID
        '
        Me.S_ID.DataPropertyName = "S_ID"
        Me.S_ID.HeaderText = "S_ID"
        Me.S_ID.Name = "S_ID"
        Me.S_ID.ReadOnly = True
        Me.S_ID.Visible = False
        '
        'S_FROM_ID
        '
        Me.S_FROM_ID.DataPropertyName = "S_FROM_ID"
        Me.S_FROM_ID.HeaderText = "S_FROM_ID"
        Me.S_FROM_ID.Name = "S_FROM_ID"
        Me.S_FROM_ID.ReadOnly = True
        Me.S_FROM_ID.Visible = False
        '
        'S_FROM_NAME
        '
        Me.S_FROM_NAME.DataPropertyName = "S_FROM_NAME"
        Me.S_FROM_NAME.HeaderText = "From"
        Me.S_FROM_NAME.Name = "S_FROM_NAME"
        Me.S_FROM_NAME.ReadOnly = True
        '
        'S_TO_ID
        '
        Me.S_TO_ID.DataPropertyName = "S_TO_ID"
        Me.S_TO_ID.HeaderText = "S_TO_ID"
        Me.S_TO_ID.Name = "S_TO_ID"
        Me.S_TO_ID.ReadOnly = True
        Me.S_TO_ID.Visible = False
        '
        'S_TO_NAME
        '
        Me.S_TO_NAME.DataPropertyName = "S_TO_NAME"
        Me.S_TO_NAME.HeaderText = "To"
        Me.S_TO_NAME.Name = "S_TO_NAME"
        Me.S_TO_NAME.ReadOnly = True
        '
        'S_VEH_ID
        '
        Me.S_VEH_ID.DataPropertyName = "S_VEH_ID"
        Me.S_VEH_ID.HeaderText = "S_VER_ID"
        Me.S_VEH_ID.Name = "S_VEH_ID"
        Me.S_VEH_ID.ReadOnly = True
        Me.S_VEH_ID.Visible = False
        '
        'S_VEH_NAME
        '
        Me.S_VEH_NAME.DataPropertyName = "S_VEH_NAME"
        Me.S_VEH_NAME.HeaderText = "Vehicle"
        Me.S_VEH_NAME.Name = "S_VEH_NAME"
        Me.S_VEH_NAME.ReadOnly = True
        '
        'S_SUP_ID
        '
        Me.S_SUP_ID.DataPropertyName = "S_SUP_ID"
        Me.S_SUP_ID.HeaderText = "S_SUP_ID"
        Me.S_SUP_ID.Name = "S_SUP_ID"
        Me.S_SUP_ID.ReadOnly = True
        Me.S_SUP_ID.Visible = False
        '
        'S_SUP_NAME
        '
        Me.S_SUP_NAME.DataPropertyName = "S_SUP_NAME"
        Me.S_SUP_NAME.HeaderText = "Supplier"
        Me.S_SUP_NAME.Name = "S_SUP_NAME"
        Me.S_SUP_NAME.ReadOnly = True
        '
        'S_COST
        '
        Me.S_COST.DataPropertyName = "S_COST"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.S_COST.DefaultCellStyle = DataGridViewCellStyle5
        Me.S_COST.HeaderText = "Cost"
        Me.S_COST.Name = "S_COST"
        Me.S_COST.ReadOnly = True
        '
        'CostUnit
        '
        Me.CostUnit.DataPropertyName = "CostUnit"
        Me.CostUnit.HeaderText = "Unit"
        Me.CostUnit.Name = "CostUnit"
        Me.CostUnit.ReadOnly = True
        '
        'cbbFrom
        '
        Me.cbbFrom.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbFrom.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbFrom.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbFrom.FormattingEnabled = True
        Me.cbbFrom.Location = New System.Drawing.Point(82, 11)
        Me.cbbFrom.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbFrom.Name = "cbbFrom"
        Me.cbbFrom.Size = New System.Drawing.Size(160, 24)
        Me.cbbFrom.TabIndex = 46
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(13, 14)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 17)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "From : "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(252, 14)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 17)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "To : "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.Location = New System.Drawing.Point(10, 13)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 17)
        Me.Label4.TabIndex = 51
        Me.Label4.Text = "Supplier : "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 13)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 17)
        Me.Label3.TabIndex = 49
        Me.Label3.Text = "Customer : "
        '
        'flpVEH
        '
        Me.flpVEH.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flpVEH.AutoScroll = True
        Me.flpVEH.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.flpVEH.Location = New System.Drawing.Point(152, 42)
        Me.flpVEH.Margin = New System.Windows.Forms.Padding(4)
        Me.flpVEH.Name = "flpVEH"
        Me.flpVEH.Size = New System.Drawing.Size(838, 67)
        Me.flpVEH.TabIndex = 54
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 50)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 17)
        Me.Label5.TabIndex = 53
        Me.Label5.Text = "Vehicle : "
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.Location = New System.Drawing.Point(3, 108)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.flowSupplier)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnAddSupplier)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnPrintSupplier)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnS_Add)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GridSup)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.flowCustomer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnAddCustomer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnPrintCustomer)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnPrintAll)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnC_Add)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GridCus)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label3)
        Me.SplitContainer1.Size = New System.Drawing.Size(999, 562)
        Me.SplitContainer1.SplitterDistance = 484
        Me.SplitContainer1.TabIndex = 56
        '
        'flowSupplier
        '
        Me.flowSupplier.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flowSupplier.AutoScroll = True
        Me.flowSupplier.BackColor = System.Drawing.Color.Transparent
        Me.flowSupplier.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flowSupplier.Location = New System.Drawing.Point(82, 12)
        Me.flowSupplier.Name = "flowSupplier"
        Me.flowSupplier.Size = New System.Drawing.Size(361, 31)
        Me.flowSupplier.TabIndex = 70
        '
        'btnPrintSupplier
        '
        Me.btnPrintSupplier.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintSupplier.BackColor = System.Drawing.Color.Salmon
        Me.btnPrintSupplier.ForeColor = System.Drawing.Color.White
        Me.btnPrintSupplier.Location = New System.Drawing.Point(154, 497)
        Me.btnPrintSupplier.Name = "btnPrintSupplier"
        Me.btnPrintSupplier.Size = New System.Drawing.Size(150, 32)
        Me.btnPrintSupplier.TabIndex = 54
        Me.btnPrintSupplier.Text = "Print Supplier"
        Me.btnPrintSupplier.UseVisualStyleBackColor = False
        '
        'btnS_Add
        '
        Me.btnS_Add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnS_Add.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnS_Add.ForeColor = System.Drawing.Color.White
        Me.btnS_Add.Location = New System.Drawing.Point(4, 497)
        Me.btnS_Add.Name = "btnS_Add"
        Me.btnS_Add.Size = New System.Drawing.Size(150, 32)
        Me.btnS_Add.TabIndex = 53
        Me.btnS_Add.Text = "Add"
        Me.btnS_Add.UseVisualStyleBackColor = False
        '
        'flowCustomer
        '
        Me.flowCustomer.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.flowCustomer.AutoScroll = True
        Me.flowCustomer.BackColor = System.Drawing.Color.Transparent
        Me.flowCustomer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.flowCustomer.Location = New System.Drawing.Point(107, 13)
        Me.flowCustomer.Name = "flowCustomer"
        Me.flowCustomer.Size = New System.Drawing.Size(361, 31)
        Me.flowCustomer.TabIndex = 68
        '
        'btnPrintCustomer
        '
        Me.btnPrintCustomer.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintCustomer.BackColor = System.Drawing.Color.Salmon
        Me.btnPrintCustomer.ForeColor = System.Drawing.Color.White
        Me.btnPrintCustomer.Location = New System.Drawing.Point(155, 497)
        Me.btnPrintCustomer.Name = "btnPrintCustomer"
        Me.btnPrintCustomer.Size = New System.Drawing.Size(150, 32)
        Me.btnPrintCustomer.TabIndex = 55
        Me.btnPrintCustomer.Text = "Print Customer"
        Me.btnPrintCustomer.UseVisualStyleBackColor = False
        '
        'btnPrintAll
        '
        Me.btnPrintAll.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnPrintAll.BackColor = System.Drawing.Color.SandyBrown
        Me.btnPrintAll.ForeColor = System.Drawing.Color.White
        Me.btnPrintAll.Location = New System.Drawing.Point(305, 497)
        Me.btnPrintAll.Name = "btnPrintAll"
        Me.btnPrintAll.Size = New System.Drawing.Size(150, 32)
        Me.btnPrintAll.TabIndex = 56
        Me.btnPrintAll.Text = "Print All"
        Me.btnPrintAll.UseVisualStyleBackColor = False
        '
        'btnC_Add
        '
        Me.btnC_Add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnC_Add.BackColor = System.Drawing.Color.RoyalBlue
        Me.btnC_Add.ForeColor = System.Drawing.Color.White
        Me.btnC_Add.Location = New System.Drawing.Point(4, 497)
        Me.btnC_Add.Name = "btnC_Add"
        Me.btnC_Add.Size = New System.Drawing.Size(150, 32)
        Me.btnC_Add.TabIndex = 54
        Me.btnC_Add.Text = "Add"
        Me.btnC_Add.UseVisualStyleBackColor = False
        '
        'cbAllVehicle
        '
        Me.cbAllVehicle.AutoSize = True
        Me.cbAllVehicle.Location = New System.Drawing.Point(85, 46)
        Me.cbAllVehicle.Name = "cbAllVehicle"
        Me.cbAllVehicle.Size = New System.Drawing.Size(42, 21)
        Me.cbAllVehicle.TabIndex = 57
        Me.cbAllVehicle.Text = "All"
        Me.cbAllVehicle.UseVisualStyleBackColor = True
        '
        'btnAddSupplier
        '
        Me.btnAddSupplier.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddSupplier.BackColor = System.Drawing.Color.Transparent
        Me.btnAddSupplier.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources._1485859723811
        Me.btnAddSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAddSupplier.ForeColor = System.Drawing.Color.White
        Me.btnAddSupplier.Location = New System.Drawing.Point(449, 13)
        Me.btnAddSupplier.Name = "btnAddSupplier"
        Me.btnAddSupplier.Size = New System.Drawing.Size(29, 29)
        Me.btnAddSupplier.TabIndex = 69
        Me.btnAddSupplier.UseVisualStyleBackColor = False
        '
        'btnAddCustomer
        '
        Me.btnAddCustomer.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddCustomer.BackColor = System.Drawing.Color.Transparent
        Me.btnAddCustomer.BackgroundImage = Global.BAFCO_TIS.My.Resources.Resources._1485859723811
        Me.btnAddCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnAddCustomer.ForeColor = System.Drawing.Color.White
        Me.btnAddCustomer.Location = New System.Drawing.Point(476, 14)
        Me.btnAddCustomer.Name = "btnAddCustomer"
        Me.btnAddCustomer.Size = New System.Drawing.Size(29, 29)
        Me.btnAddCustomer.TabIndex = 67
        Me.btnAddCustomer.UseVisualStyleBackColor = False
        '
        'C_ID
        '
        Me.C_ID.DataPropertyName = "C_ID"
        Me.C_ID.HeaderText = "C_ID"
        Me.C_ID.Name = "C_ID"
        Me.C_ID.ReadOnly = True
        Me.C_ID.Visible = False
        '
        'C_FROM_ID
        '
        Me.C_FROM_ID.DataPropertyName = "C_FROM_ID"
        Me.C_FROM_ID.HeaderText = "C_FROM_ID"
        Me.C_FROM_ID.Name = "C_FROM_ID"
        Me.C_FROM_ID.ReadOnly = True
        Me.C_FROM_ID.Visible = False
        '
        'C_FROM_NAME
        '
        Me.C_FROM_NAME.DataPropertyName = "C_FROM_NAME"
        Me.C_FROM_NAME.FillWeight = 98.90017!
        Me.C_FROM_NAME.HeaderText = "From"
        Me.C_FROM_NAME.Name = "C_FROM_NAME"
        Me.C_FROM_NAME.ReadOnly = True
        '
        'C_TO_ID
        '
        Me.C_TO_ID.DataPropertyName = "C_TO_ID"
        Me.C_TO_ID.HeaderText = "C_TO_ID"
        Me.C_TO_ID.Name = "C_TO_ID"
        Me.C_TO_ID.ReadOnly = True
        Me.C_TO_ID.Visible = False
        '
        'C_TO_NAME
        '
        Me.C_TO_NAME.DataPropertyName = "C_TO_NAME"
        Me.C_TO_NAME.FillWeight = 98.90017!
        Me.C_TO_NAME.HeaderText = "To"
        Me.C_TO_NAME.Name = "C_TO_NAME"
        Me.C_TO_NAME.ReadOnly = True
        '
        'C_VEH_ID
        '
        Me.C_VEH_ID.DataPropertyName = "C_VEH_ID"
        Me.C_VEH_ID.HeaderText = "C_VER_ID"
        Me.C_VEH_ID.Name = "C_VEH_ID"
        Me.C_VEH_ID.ReadOnly = True
        Me.C_VEH_ID.Visible = False
        '
        'C_VEH_NAME
        '
        Me.C_VEH_NAME.DataPropertyName = "C_VEH_NAME"
        Me.C_VEH_NAME.FillWeight = 98.90017!
        Me.C_VEH_NAME.HeaderText = "Vehicle"
        Me.C_VEH_NAME.Name = "C_VEH_NAME"
        Me.C_VEH_NAME.ReadOnly = True
        '
        'C_CUS_ID
        '
        Me.C_CUS_ID.DataPropertyName = "C_CUS_ID"
        Me.C_CUS_ID.HeaderText = "C_CUS_ID"
        Me.C_CUS_ID.Name = "C_CUS_ID"
        Me.C_CUS_ID.ReadOnly = True
        Me.C_CUS_ID.Visible = False
        '
        'C_CUS_NAME
        '
        Me.C_CUS_NAME.DataPropertyName = "C_CUS_NAME"
        Me.C_CUS_NAME.FillWeight = 98.90017!
        Me.C_CUS_NAME.HeaderText = "Customer"
        Me.C_CUS_NAME.Name = "C_CUS_NAME"
        Me.C_CUS_NAME.ReadOnly = True
        '
        'C_COST
        '
        Me.C_COST.DataPropertyName = "C_COST"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "C2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.C_COST.DefaultCellStyle = DataGridViewCellStyle2
        Me.C_COST.FillWeight = 98.90017!
        Me.C_COST.HeaderText = "Price"
        Me.C_COST.Name = "C_COST"
        Me.C_COST.ReadOnly = True
        '
        'ValueUnit
        '
        Me.ValueUnit.DataPropertyName = "ValueUnit"
        Me.ValueUnit.FillWeight = 98.90017!
        Me.ValueUnit.HeaderText = "Unit"
        Me.ValueUnit.Name = "ValueUnit"
        Me.ValueUnit.ReadOnly = True
        '
        'colQtStatusValue
        '
        Me.colQtStatusValue.DataPropertyName = "qt_status"
        Me.colQtStatusValue.HeaderText = "colQtStatusValue"
        Me.colQtStatusValue.Name = "colQtStatusValue"
        Me.colQtStatusValue.ReadOnly = True
        Me.colQtStatusValue.Visible = False
        '
        'colQtStatus
        '
        Me.colQtStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colQtStatus.FillWeight = 30.0!
        Me.colQtStatus.HeaderText = ""
        Me.colQtStatus.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom
        Me.colQtStatus.Name = "colQtStatus"
        Me.colQtStatus.ReadOnly = True
        Me.colQtStatus.Width = 30
        '
        'frmSearchCost
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.ClientSize = New System.Drawing.Size(1006, 644)
        Me.ControlBox = False
        Me.Controls.Add(Me.cbAllVehicle)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.cbbTo)
        Me.Controls.Add(Me.cbbFrom)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.flpVEH)
        Me.Controls.Add(Me.Label5)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSearchCost"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        CType(Me.GridCus, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridSup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GridCus As System.Windows.Forms.DataGridView
    Friend WithEvents cbbTo As System.Windows.Forms.ComboBox
    Friend WithEvents GridSup As System.Windows.Forms.DataGridView
    Friend WithEvents cbbFrom As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents flpVEH As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnS_Add As System.Windows.Forms.Button
    Friend WithEvents btnC_Add As System.Windows.Forms.Button
    Friend WithEvents btnPrintSupplier As System.Windows.Forms.Button
    Friend WithEvents btnPrintCustomer As System.Windows.Forms.Button
    Friend WithEvents btnPrintAll As System.Windows.Forms.Button
    Friend WithEvents cbAllVehicle As System.Windows.Forms.CheckBox
    Friend WithEvents S_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_FROM_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_FROM_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_TO_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_TO_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_VEH_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_VEH_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SUP_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_SUP_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents S_COST As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CostUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents flowCustomer As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnAddCustomer As System.Windows.Forms.Button
    Friend WithEvents flowSupplier As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnAddSupplier As System.Windows.Forms.Button
    Friend WithEvents C_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_FROM_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_FROM_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_TO_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_TO_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_VEH_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_VEH_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_CUS_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_CUS_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents C_COST As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ValueUnit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colQtStatusValue As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colQtStatus As System.Windows.Forms.DataGridViewImageColumn
End Class

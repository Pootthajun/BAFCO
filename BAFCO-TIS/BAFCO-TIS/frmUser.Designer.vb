﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCode = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbbStatus = New System.Windows.Forms.ComboBox()
        Me.GridSUP = New System.Windows.Forms.DataGridView()
        Me.SUP_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SUP_CHECK = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.SUP_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GridCUS = New System.Windows.Forms.DataGridView()
        Me.CUS_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CUS_CHECK = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.CUS_MAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GridUser = New System.Windows.Forms.DataGridView()
        Me.US_CODE = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.US_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.US_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.US_PASS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACTIVE_STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VAL_STATUS = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.pnlUserData = New System.Windows.Forms.Panel()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.pnlSup = New System.Windows.Forms.Panel()
        Me.cb_sup_all = New System.Windows.Forms.CheckBox()
        Me.rb_sup_notcheck = New System.Windows.Forms.RadioButton()
        Me.rb_sub_check = New System.Windows.Forms.RadioButton()
        Me.rb_sup_all = New System.Windows.Forms.RadioButton()
        Me.cbbFilterSup = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GridMenu = New System.Windows.Forms.DataGridView()
        Me.MENU_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MENU_CHECK = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.MENU_NAME = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.pnlCus = New System.Windows.Forms.Panel()
        Me.cb_cus_all = New System.Windows.Forms.CheckBox()
        Me.rb_cus_notcheck = New System.Windows.Forms.RadioButton()
        Me.cbbFilterCus = New System.Windows.Forms.ComboBox()
        Me.rb_cus_check = New System.Windows.Forms.RadioButton()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.rb_cus_all = New System.Windows.Forms.RadioButton()
        Me.pnlMenu = New System.Windows.Forms.Panel()
        Me.cb_menu_all = New System.Windows.Forms.CheckBox()
        Me.pnlUser = New System.Windows.Forms.Panel()
        Me.lblRec = New System.Windows.Forms.Label()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.cbbFilterStatus = New System.Windows.Forms.ComboBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cbbFilterUser = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.rp_cus_all = New System.Windows.Forms.RadioButton()
        CType(Me.GridSUP, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridCUS, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlUserData.SuspendLayout()
        Me.pnlSup.SuspendLayout()
        CType(Me.GridMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlCus.SuspendLayout()
        Me.pnlMenu.SuspendLayout()
        Me.pnlUser.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label7.Location = New System.Drawing.Point(435, 42)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 16)
        Me.Label7.TabIndex = 37
        Me.Label7.Text = "Password :"
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(563, 39)
        Me.txtPassword.MaxLength = 20
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(230, 22)
        Me.txtPassword.TabIndex = 35
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label8.Location = New System.Drawing.Point(435, 10)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(122, 16)
        Me.Label8.TabIndex = 36
        Me.Label8.Text = "User Code / Login :"
        '
        'txtCode
        '
        Me.txtCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtCode.Location = New System.Drawing.Point(563, 7)
        Me.txtCode.MaxLength = 20
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(230, 22)
        Me.txtCode.TabIndex = 34
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.txtName.Location = New System.Drawing.Point(90, 7)
        Me.txtName.MaxLength = 300
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(310, 22)
        Me.txtName.TabIndex = 38
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label2.Location = New System.Drawing.Point(15, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 16)
        Me.Label2.TabIndex = 39
        Me.Label2.Text = "Fullname :"
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label1.Location = New System.Drawing.Point(15, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 16)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Status :"
        '
        'cbbStatus
        '
        Me.cbbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbStatus.FormattingEnabled = True
        Me.cbbStatus.Location = New System.Drawing.Point(90, 39)
        Me.cbbStatus.Name = "cbbStatus"
        Me.cbbStatus.Size = New System.Drawing.Size(310, 24)
        Me.cbbStatus.TabIndex = 41
        '
        'GridSUP
        '
        Me.GridSUP.AllowUserToAddRows = False
        Me.GridSUP.AllowUserToDeleteRows = False
        Me.GridSUP.AllowUserToOrderColumns = True
        Me.GridSUP.AllowUserToResizeColumns = False
        Me.GridSUP.AllowUserToResizeRows = False
        Me.GridSUP.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridSUP.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.GridSUP.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridSUP.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.GridSUP.ColumnHeadersHeight = 30
        Me.GridSUP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.GridSUP.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SUP_ID, Me.SUP_CHECK, Me.SUP_NAME})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridSUP.DefaultCellStyle = DataGridViewCellStyle2
        Me.GridSUP.Location = New System.Drawing.Point(3, 59)
        Me.GridSUP.MultiSelect = False
        Me.GridSUP.Name = "GridSUP"
        Me.GridSUP.RowHeadersVisible = False
        Me.GridSUP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridSUP.Size = New System.Drawing.Size(339, 299)
        Me.GridSUP.TabIndex = 50
        '
        'SUP_ID
        '
        Me.SUP_ID.DataPropertyName = "SUP_ID"
        Me.SUP_ID.FillWeight = 5.076141!
        Me.SUP_ID.HeaderText = "SUP_ID"
        Me.SUP_ID.Name = "SUP_ID"
        Me.SUP_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.SUP_ID.Visible = False
        '
        'SUP_CHECK
        '
        Me.SUP_CHECK.DataPropertyName = "SUP_CHECK"
        Me.SUP_CHECK.HeaderText = ""
        Me.SUP_CHECK.Name = "SUP_CHECK"
        Me.SUP_CHECK.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.SUP_CHECK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.SUP_CHECK.Width = 30
        '
        'SUP_NAME
        '
        Me.SUP_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.SUP_NAME.DataPropertyName = "SUP_NAME"
        Me.SUP_NAME.FillWeight = 194.9239!
        Me.SUP_NAME.HeaderText = "Supplier"
        Me.SUP_NAME.Name = "SUP_NAME"
        Me.SUP_NAME.ReadOnly = True
        Me.SUP_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'GridCUS
        '
        Me.GridCUS.AllowUserToAddRows = False
        Me.GridCUS.AllowUserToDeleteRows = False
        Me.GridCUS.AllowUserToOrderColumns = True
        Me.GridCUS.AllowUserToResizeColumns = False
        Me.GridCUS.AllowUserToResizeRows = False
        Me.GridCUS.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridCUS.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.GridCUS.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridCUS.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.GridCUS.ColumnHeadersHeight = 30
        Me.GridCUS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.GridCUS.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CUS_ID, Me.CUS_CHECK, Me.CUS_MAME})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridCUS.DefaultCellStyle = DataGridViewCellStyle4
        Me.GridCUS.Location = New System.Drawing.Point(3, 59)
        Me.GridCUS.MultiSelect = False
        Me.GridCUS.Name = "GridCUS"
        Me.GridCUS.RowHeadersVisible = False
        Me.GridCUS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridCUS.Size = New System.Drawing.Size(339, 299)
        Me.GridCUS.TabIndex = 51
        '
        'CUS_ID
        '
        Me.CUS_ID.DataPropertyName = "CUS_ID"
        Me.CUS_ID.FillWeight = 5.076141!
        Me.CUS_ID.HeaderText = "CUS_ID"
        Me.CUS_ID.Name = "CUS_ID"
        Me.CUS_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.CUS_ID.Visible = False
        '
        'CUS_CHECK
        '
        Me.CUS_CHECK.DataPropertyName = "CUS_CHECK"
        Me.CUS_CHECK.HeaderText = ""
        Me.CUS_CHECK.Name = "CUS_CHECK"
        Me.CUS_CHECK.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CUS_CHECK.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CUS_CHECK.Width = 30
        '
        'CUS_MAME
        '
        Me.CUS_MAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CUS_MAME.DataPropertyName = "CUS_NAME"
        Me.CUS_MAME.FillWeight = 194.9239!
        Me.CUS_MAME.HeaderText = "Customer"
        Me.CUS_MAME.Name = "CUS_MAME"
        Me.CUS_MAME.ReadOnly = True
        Me.CUS_MAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'GridUser
        '
        Me.GridUser.AllowUserToAddRows = False
        Me.GridUser.AllowUserToDeleteRows = False
        Me.GridUser.AllowUserToOrderColumns = True
        Me.GridUser.AllowUserToResizeColumns = False
        Me.GridUser.AllowUserToResizeRows = False
        Me.GridUser.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridUser.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.GridUser.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridUser.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.GridUser.ColumnHeadersHeight = 30
        Me.GridUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.GridUser.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.US_CODE, Me.US_NAME, Me.US_ID, Me.US_PASS, Me.ACTIVE_STATUS, Me.VAL_STATUS})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridUser.DefaultCellStyle = DataGridViewCellStyle7
        Me.GridUser.Location = New System.Drawing.Point(3, 64)
        Me.GridUser.MultiSelect = False
        Me.GridUser.Name = "GridUser"
        Me.GridUser.ReadOnly = True
        Me.GridUser.RowHeadersVisible = False
        Me.GridUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridUser.Size = New System.Drawing.Size(322, 388)
        Me.GridUser.TabIndex = 52
        '
        'US_CODE
        '
        Me.US_CODE.DataPropertyName = "US_CODE"
        Me.US_CODE.FillWeight = 50.65964!
        Me.US_CODE.HeaderText = "User Code / Login"
        Me.US_CODE.Name = "US_CODE"
        Me.US_CODE.ReadOnly = True
        Me.US_CODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.US_CODE.Visible = False
        Me.US_CODE.Width = 150
        '
        'US_NAME
        '
        Me.US_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.US_NAME.DataPropertyName = "US_NAME"
        Me.US_NAME.FillWeight = 27.89655!
        Me.US_NAME.HeaderText = "Name"
        Me.US_NAME.Name = "US_NAME"
        Me.US_NAME.ReadOnly = True
        Me.US_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'US_ID
        '
        Me.US_ID.DataPropertyName = "US_ID"
        Me.US_ID.HeaderText = "id"
        Me.US_ID.Name = "US_ID"
        Me.US_ID.ReadOnly = True
        Me.US_ID.Visible = False
        Me.US_ID.Width = 44
        '
        'US_PASS
        '
        Me.US_PASS.DataPropertyName = "US_PASS"
        Me.US_PASS.HeaderText = "Password"
        Me.US_PASS.Name = "US_PASS"
        Me.US_PASS.ReadOnly = True
        Me.US_PASS.Visible = False
        '
        'ACTIVE_STATUS
        '
        Me.ACTIVE_STATUS.DataPropertyName = "ACTIVE_STATUS"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.ACTIVE_STATUS.DefaultCellStyle = DataGridViewCellStyle6
        Me.ACTIVE_STATUS.FillWeight = 221.4439!
        Me.ACTIVE_STATUS.HeaderText = "Status"
        Me.ACTIVE_STATUS.Name = "ACTIVE_STATUS"
        Me.ACTIVE_STATUS.ReadOnly = True
        Me.ACTIVE_STATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'VAL_STATUS
        '
        Me.VAL_STATUS.DataPropertyName = "VAL_STATUS"
        Me.VAL_STATUS.HeaderText = "VAL_STATUS"
        Me.VAL_STATUS.Name = "VAL_STATUS"
        Me.VAL_STATUS.ReadOnly = True
        Me.VAL_STATUS.Visible = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.SteelBlue
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(12, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(330, 32)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "User List"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.BackColor = System.Drawing.Color.SteelBlue
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(348, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(1010, 32)
        Me.Label4.TabIndex = 54
        Me.Label4.Text = "User Information"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlUserData
        '
        Me.pnlUserData.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlUserData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlUserData.Controls.Add(Me.txtCode)
        Me.pnlUserData.Controls.Add(Me.Label7)
        Me.pnlUserData.Controls.Add(Me.txtPassword)
        Me.pnlUserData.Controls.Add(Me.Label8)
        Me.pnlUserData.Controls.Add(Me.txtName)
        Me.pnlUserData.Controls.Add(Me.Label2)
        Me.pnlUserData.Controls.Add(Me.btnCancel)
        Me.pnlUserData.Controls.Add(Me.btnSave)
        Me.pnlUserData.Controls.Add(Me.Label1)
        Me.pnlUserData.Controls.Add(Me.cbbStatus)
        Me.pnlUserData.Enabled = False
        Me.pnlUserData.Location = New System.Drawing.Point(348, 45)
        Me.pnlUserData.Name = "pnlUserData"
        Me.pnlUserData.Size = New System.Drawing.Size(1010, 99)
        Me.pnlUserData.TabIndex = 55
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.OrangeRed
        Me.btnCancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.White
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(842, 7)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(152, 37)
        Me.btnCancel.TabIndex = 48
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.Green
        Me.btnSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.White
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.Location = New System.Drawing.Point(842, 50)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(152, 37)
        Me.btnSave.TabIndex = 47
        Me.btnSave.Text = "Save"
        Me.btnSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.BackColor = System.Drawing.Color.SteelBlue
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(349, 147)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(347, 32)
        Me.Label5.TabIndex = 56
        Me.Label5.Text = "Supplier Allowed"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlSup
        '
        Me.pnlSup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlSup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlSup.Controls.Add(Me.cb_sup_all)
        Me.pnlSup.Controls.Add(Me.rb_sup_notcheck)
        Me.pnlSup.Controls.Add(Me.rb_sub_check)
        Me.pnlSup.Controls.Add(Me.rb_sup_all)
        Me.pnlSup.Controls.Add(Me.cbbFilterSup)
        Me.pnlSup.Controls.Add(Me.Label12)
        Me.pnlSup.Controls.Add(Me.GridSUP)
        Me.pnlSup.Enabled = False
        Me.pnlSup.Location = New System.Drawing.Point(348, 182)
        Me.pnlSup.Name = "pnlSup"
        Me.pnlSup.Size = New System.Drawing.Size(347, 363)
        Me.pnlSup.TabIndex = 57
        '
        'cb_sup_all
        '
        Me.cb_sup_all.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cb_sup_all.AutoSize = True
        Me.cb_sup_all.Location = New System.Drawing.Point(13, 68)
        Me.cb_sup_all.Name = "cb_sup_all"
        Me.cb_sup_all.Size = New System.Drawing.Size(15, 14)
        Me.cb_sup_all.TabIndex = 63
        Me.cb_sup_all.UseVisualStyleBackColor = True
        '
        'rb_sup_notcheck
        '
        Me.rb_sup_notcheck.AutoSize = True
        Me.rb_sup_notcheck.Location = New System.Drawing.Point(200, 36)
        Me.rb_sup_notcheck.Name = "rb_sup_notcheck"
        Me.rb_sup_notcheck.Size = New System.Drawing.Size(82, 17)
        Me.rb_sup_notcheck.TabIndex = 62
        Me.rb_sup_notcheck.Text = "Not Allowed"
        Me.rb_sup_notcheck.UseVisualStyleBackColor = True
        '
        'rb_sub_check
        '
        Me.rb_sub_check.AutoSize = True
        Me.rb_sub_check.Location = New System.Drawing.Point(117, 36)
        Me.rb_sub_check.Name = "rb_sub_check"
        Me.rb_sub_check.Size = New System.Drawing.Size(62, 17)
        Me.rb_sub_check.TabIndex = 61
        Me.rb_sub_check.Text = "Allowed"
        Me.rb_sub_check.UseVisualStyleBackColor = True
        '
        'rb_sup_all
        '
        Me.rb_sup_all.AutoSize = True
        Me.rb_sup_all.Checked = True
        Me.rb_sup_all.Location = New System.Drawing.Point(61, 36)
        Me.rb_sup_all.Name = "rb_sup_all"
        Me.rb_sup_all.Size = New System.Drawing.Size(36, 17)
        Me.rb_sup_all.TabIndex = 60
        Me.rb_sup_all.TabStop = True
        Me.rb_sup_all.Text = "All"
        Me.rb_sup_all.UseVisualStyleBackColor = True
        '
        'cbbFilterSup
        '
        Me.cbbFilterSup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbbFilterSup.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbFilterSup.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbFilterSup.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbFilterSup.FormattingEnabled = True
        Me.cbbFilterSup.Location = New System.Drawing.Point(61, 5)
        Me.cbbFilterSup.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbFilterSup.Name = "cbbFilterSup"
        Me.cbbFilterSup.Size = New System.Drawing.Size(281, 24)
        Me.cbbFilterSup.TabIndex = 59
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label12.Location = New System.Drawing.Point(3, 8)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(51, 16)
        Me.Label12.TabIndex = 58
        Me.Label12.Text = "Name :"
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.BackColor = System.Drawing.Color.SteelBlue
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(1055, 147)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(304, 32)
        Me.Label6.TabIndex = 58
        Me.Label6.Text = "Menu Permission"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'GridMenu
        '
        Me.GridMenu.AllowUserToAddRows = False
        Me.GridMenu.AllowUserToDeleteRows = False
        Me.GridMenu.AllowUserToOrderColumns = True
        Me.GridMenu.AllowUserToResizeColumns = False
        Me.GridMenu.AllowUserToResizeRows = False
        Me.GridMenu.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GridMenu.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.GridMenu.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.GridMenu.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.GridMenu.ColumnHeadersHeight = 30
        Me.GridMenu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.GridMenu.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MENU_ID, Me.MENU_CHECK, Me.MENU_NAME})
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.GridMenu.DefaultCellStyle = DataGridViewCellStyle9
        Me.GridMenu.Location = New System.Drawing.Point(3, 5)
        Me.GridMenu.MultiSelect = False
        Me.GridMenu.Name = "GridMenu"
        Me.GridMenu.RowHeadersVisible = False
        Me.GridMenu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GridMenu.Size = New System.Drawing.Size(296, 353)
        Me.GridMenu.TabIndex = 65
        '
        'MENU_ID
        '
        Me.MENU_ID.DataPropertyName = "MENU_ID"
        Me.MENU_ID.FillWeight = 5.076141!
        Me.MENU_ID.HeaderText = "MENU_ID"
        Me.MENU_ID.Name = "MENU_ID"
        Me.MENU_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.MENU_ID.Visible = False
        '
        'MENU_CHECK
        '
        Me.MENU_CHECK.DataPropertyName = "MENU_CHECK"
        Me.MENU_CHECK.HeaderText = ""
        Me.MENU_CHECK.Name = "MENU_CHECK"
        Me.MENU_CHECK.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MENU_CHECK.Width = 30
        '
        'MENU_NAME
        '
        Me.MENU_NAME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.MENU_NAME.DataPropertyName = "MENU_NAME"
        Me.MENU_NAME.FillWeight = 194.9239!
        Me.MENU_NAME.HeaderText = "Menu"
        Me.MENU_NAME.Name = "MENU_NAME"
        Me.MENU_NAME.ReadOnly = True
        Me.MENU_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'Label9
        '
        Me.Label9.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.BackColor = System.Drawing.Color.SteelBlue
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(702, 147)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(347, 32)
        Me.Label9.TabIndex = 66
        Me.Label9.Text = "Customer Allowed"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlCus
        '
        Me.pnlCus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlCus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlCus.Controls.Add(Me.cb_cus_all)
        Me.pnlCus.Controls.Add(Me.rb_cus_notcheck)
        Me.pnlCus.Controls.Add(Me.cbbFilterCus)
        Me.pnlCus.Controls.Add(Me.rb_cus_check)
        Me.pnlCus.Controls.Add(Me.Label13)
        Me.pnlCus.Controls.Add(Me.rb_cus_all)
        Me.pnlCus.Controls.Add(Me.GridCUS)
        Me.pnlCus.Enabled = False
        Me.pnlCus.Location = New System.Drawing.Point(701, 182)
        Me.pnlCus.Name = "pnlCus"
        Me.pnlCus.Size = New System.Drawing.Size(347, 363)
        Me.pnlCus.TabIndex = 67
        '
        'cb_cus_all
        '
        Me.cb_cus_all.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cb_cus_all.AutoSize = True
        Me.cb_cus_all.Location = New System.Drawing.Point(13, 68)
        Me.cb_cus_all.Name = "cb_cus_all"
        Me.cb_cus_all.Size = New System.Drawing.Size(15, 14)
        Me.cb_cus_all.TabIndex = 64
        Me.cb_cus_all.UseVisualStyleBackColor = True
        '
        'rb_cus_notcheck
        '
        Me.rb_cus_notcheck.AutoSize = True
        Me.rb_cus_notcheck.Location = New System.Drawing.Point(203, 36)
        Me.rb_cus_notcheck.Name = "rb_cus_notcheck"
        Me.rb_cus_notcheck.Size = New System.Drawing.Size(82, 17)
        Me.rb_cus_notcheck.TabIndex = 65
        Me.rb_cus_notcheck.Text = "Not Allowed"
        Me.rb_cus_notcheck.UseVisualStyleBackColor = True
        '
        'cbbFilterCus
        '
        Me.cbbFilterCus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbbFilterCus.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbFilterCus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbFilterCus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbFilterCus.FormattingEnabled = True
        Me.cbbFilterCus.Location = New System.Drawing.Point(64, 5)
        Me.cbbFilterCus.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbFilterCus.Name = "cbbFilterCus"
        Me.cbbFilterCus.Size = New System.Drawing.Size(277, 24)
        Me.cbbFilterCus.TabIndex = 61
        '
        'rb_cus_check
        '
        Me.rb_cus_check.AutoSize = True
        Me.rb_cus_check.Location = New System.Drawing.Point(120, 36)
        Me.rb_cus_check.Name = "rb_cus_check"
        Me.rb_cus_check.Size = New System.Drawing.Size(62, 17)
        Me.rb_cus_check.TabIndex = 64
        Me.rb_cus_check.Text = "Allowed"
        Me.rb_cus_check.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label13.Location = New System.Drawing.Point(6, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(51, 16)
        Me.Label13.TabIndex = 60
        Me.Label13.Text = "Name :"
        '
        'rb_cus_all
        '
        Me.rb_cus_all.AutoSize = True
        Me.rb_cus_all.Checked = True
        Me.rb_cus_all.Location = New System.Drawing.Point(64, 36)
        Me.rb_cus_all.Name = "rb_cus_all"
        Me.rb_cus_all.Size = New System.Drawing.Size(36, 17)
        Me.rb_cus_all.TabIndex = 63
        Me.rb_cus_all.TabStop = True
        Me.rb_cus_all.Text = "All"
        Me.rb_cus_all.UseVisualStyleBackColor = True
        '
        'pnlMenu
        '
        Me.pnlMenu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlMenu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlMenu.Controls.Add(Me.cb_menu_all)
        Me.pnlMenu.Controls.Add(Me.GridMenu)
        Me.pnlMenu.Enabled = False
        Me.pnlMenu.Location = New System.Drawing.Point(1054, 182)
        Me.pnlMenu.Name = "pnlMenu"
        Me.pnlMenu.Size = New System.Drawing.Size(304, 363)
        Me.pnlMenu.TabIndex = 68
        '
        'cb_menu_all
        '
        Me.cb_menu_all.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cb_menu_all.AutoSize = True
        Me.cb_menu_all.Location = New System.Drawing.Point(13, 14)
        Me.cb_menu_all.Name = "cb_menu_all"
        Me.cb_menu_all.Size = New System.Drawing.Size(15, 14)
        Me.cb_menu_all.TabIndex = 66
        Me.cb_menu_all.UseVisualStyleBackColor = True
        '
        'pnlUser
        '
        Me.pnlUser.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlUser.Controls.Add(Me.lblRec)
        Me.pnlUser.Controls.Add(Me.btnEdit)
        Me.pnlUser.Controls.Add(Me.cbbFilterStatus)
        Me.pnlUser.Controls.Add(Me.btnAdd)
        Me.pnlUser.Controls.Add(Me.Label11)
        Me.pnlUser.Controls.Add(Me.cbbFilterUser)
        Me.pnlUser.Controls.Add(Me.Label10)
        Me.pnlUser.Controls.Add(Me.GridUser)
        Me.pnlUser.Location = New System.Drawing.Point(12, 45)
        Me.pnlUser.Name = "pnlUser"
        Me.pnlUser.Size = New System.Drawing.Size(330, 500)
        Me.pnlUser.TabIndex = 69
        '
        'lblRec
        '
        Me.lblRec.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblRec.AutoSize = True
        Me.lblRec.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.lblRec.Location = New System.Drawing.Point(4, 467)
        Me.lblRec.Name = "lblRec"
        Me.lblRec.Size = New System.Drawing.Size(128, 17)
        Me.lblRec.TabIndex = 53
        Me.lblRec.Text = "Total   xxx   Record"
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnEdit.Image = Global.BAFCO_TIS.My.Resources.Resources.Edit
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.Location = New System.Drawing.Point(235, 457)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(90, 37)
        Me.btnEdit.TabIndex = 57
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'cbbFilterStatus
        '
        Me.cbbFilterStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbbFilterStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbbFilterStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbFilterStatus.Location = New System.Drawing.Point(62, 33)
        Me.cbbFilterStatus.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbFilterStatus.Name = "cbbFilterStatus"
        Me.cbbFilterStatus.Size = New System.Drawing.Size(263, 24)
        Me.cbbFilterStatus.TabIndex = 56
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.BackColor = System.Drawing.Color.White
        Me.btnAdd.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.btnAdd.Image = Global.BAFCO_TIS.My.Resources.Resources.Add
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.Location = New System.Drawing.Point(139, 457)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(90, 37)
        Me.btnAdd.TabIndex = 46
        Me.btnAdd.Text = "Add"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label11.Location = New System.Drawing.Point(4, 36)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 16)
        Me.Label11.TabIndex = 55
        Me.Label11.Text = "Status :"
        '
        'cbbFilterUser
        '
        Me.cbbFilterUser.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbbFilterUser.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cbbFilterUser.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbbFilterUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.cbbFilterUser.FormattingEnabled = True
        Me.cbbFilterUser.Location = New System.Drawing.Point(62, 4)
        Me.cbbFilterUser.Margin = New System.Windows.Forms.Padding(4)
        Me.cbbFilterUser.Name = "cbbFilterUser"
        Me.cbbFilterUser.Size = New System.Drawing.Size(263, 24)
        Me.cbbFilterUser.TabIndex = 53
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.Label10.Location = New System.Drawing.Point(4, 7)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(51, 16)
        Me.Label10.TabIndex = 50
        Me.Label10.Text = "Name :"
        '
        'rp_cus_all
        '
        Me.rp_cus_all.AutoSize = True
        Me.rp_cus_all.Checked = True
        Me.rp_cus_all.Location = New System.Drawing.Point(64, 36)
        Me.rp_cus_all.Name = "rp_cus_all"
        Me.rp_cus_all.Size = New System.Drawing.Size(36, 17)
        Me.rp_cus_all.TabIndex = 63
        Me.rp_cus_all.TabStop = True
        Me.rp_cus_all.Text = "All"
        Me.rp_cus_all.UseVisualStyleBackColor = True
        '
        'frmUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.ClientSize = New System.Drawing.Size(1370, 557)
        Me.Controls.Add(Me.pnlUser)
        Me.Controls.Add(Me.pnlMenu)
        Me.Controls.Add(Me.pnlCus)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.pnlSup)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.pnlUserData)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmUser"
        CType(Me.GridSUP, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridCUS, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlUserData.ResumeLayout(False)
        Me.pnlUserData.PerformLayout()
        Me.pnlSup.ResumeLayout(False)
        Me.pnlSup.PerformLayout()
        CType(Me.GridMenu, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlCus.ResumeLayout(False)
        Me.pnlCus.PerformLayout()
        Me.pnlMenu.ResumeLayout(False)
        Me.pnlMenu.PerformLayout()
        Me.pnlUser.ResumeLayout(False)
        Me.pnlUser.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCode As System.Windows.Forms.TextBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cbbStatus As System.Windows.Forms.ComboBox
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents GridSUP As System.Windows.Forms.DataGridView
    Friend WithEvents GridCUS As System.Windows.Forms.DataGridView
    Friend WithEvents GridUser As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents pnlUserData As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents pnlSup As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GridMenu As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents pnlCus As System.Windows.Forms.Panel
    Friend WithEvents pnlMenu As System.Windows.Forms.Panel
    Friend WithEvents pnlUser As System.Windows.Forms.Panel
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cbbFilterUser As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cbbFilterStatus As System.Windows.Forms.ComboBox
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents US_CODE As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents US_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents US_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents US_PASS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACTIVE_STATUS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VAL_STATUS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SUP_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SUP_CHECK As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents SUP_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUS_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CUS_CHECK As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents CUS_MAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MENU_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MENU_CHECK As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents MENU_NAME As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbbFilterSup As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cbbFilterCus As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents rb_sup_notcheck As System.Windows.Forms.RadioButton
    Friend WithEvents rb_sub_check As System.Windows.Forms.RadioButton
    Friend WithEvents rb_sup_all As System.Windows.Forms.RadioButton
    Friend WithEvents rb_cus_notcheck As System.Windows.Forms.RadioButton
    Friend WithEvents rb_cus_check As System.Windows.Forms.RadioButton
    Friend WithEvents rb_cus_all As System.Windows.Forms.RadioButton
    Friend WithEvents rp_cus_all As System.Windows.Forms.RadioButton
    Friend WithEvents cb_sup_all As System.Windows.Forms.CheckBox
    Friend WithEvents cb_cus_all As System.Windows.Forms.CheckBox
    Friend WithEvents cb_menu_all As System.Windows.Forms.CheckBox
    Friend WithEvents lblRec As System.Windows.Forms.Label
End Class

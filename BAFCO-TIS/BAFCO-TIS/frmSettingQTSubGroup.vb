﻿Imports System.Data
Imports System.Data.SqlClient

Public Class frmSettingQTSubGroup

    Dim DA As New SqlDataAdapter

    Private Sub frmSettingQTSubGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        grdMain.AutoGenerateColumns = False
        txtSearch.Text = ""
        rdiSearchAll.Checked = True
        cbbGroup.SelectedValue = 0
        ShowData()
        Filter()
    End Sub

    Private Sub ShowData()
        grdMain.Columns.Clear()
        Dim SQL As String = ""
        SQL = "SELECT * FROM QT_GROUP_SUB ORDER BY subgroup_name"
        DA = New SqlDataAdapter(SQL, ConnStr)
        Dim DT As New DataTable
        DA.Fill(DT)
        grdMain.DataSource = DT

        SQL = "SELECT id,group_name FROM qt_group where active_status='Y' ORDER BY group_name"
        Dim DT_GROUP As DataTable = Execute_DataTable(SQL)
        Dim dr_group As DataRow = DT_GROUP.NewRow
        dr_group("id") = 0
        dr_group("group_name") = ""
        DT_GROUP.Rows.InsertAt(dr_group, 0)


        Dim SUBGROUP_ID As New System.Windows.Forms.DataGridViewTextBoxColumn
        SUBGROUP_ID.Name = "SUBGROUP_ID"
        SUBGROUP_ID.DataPropertyName = "id"
        SUBGROUP_ID.Visible = False
        grdMain.Columns.Add(SUBGROUP_ID)
        grdMain.Columns("SUBGROUP_ID").Visible = False

        Dim SUBGROUP_NAME As New System.Windows.Forms.DataGridViewTextBoxColumn
        SUBGROUP_NAME.DataPropertyName = "SUBGROUP_NAME"
        SUBGROUP_NAME.HeaderText = "Sub Group Name"
        SUBGROUP_NAME.SortMode = DataGridViewColumnSortMode.NotSortable
        SUBGROUP_NAME.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
        grdMain.Columns.Add(SUBGROUP_NAME)

        Dim GROUP_ID As New System.Windows.Forms.DataGridViewComboBoxColumn
        GROUP_ID.DataSource = DT_GROUP
        GROUP_ID.DisplayMember = "group_name"
        GROUP_ID.ValueMember = "id"
        GROUP_ID.DataPropertyName = "qt_group_id"
        GROUP_ID.HeaderText = "Group"
        GROUP_ID.Width = 300
        grdMain.Columns.Add(GROUP_ID)

        Dim CAT_ACTIVE As New System.Windows.Forms.DataGridViewCheckBoxColumn
        CAT_ACTIVE.DataPropertyName = "active_status"
        CAT_ACTIVE.TrueValue = "Y"
        CAT_ACTIVE.FalseValue = "N"
        CAT_ACTIVE.HeaderText = "Active"
        CAT_ACTIVE.Width = 100
        grdMain.Columns.Add(CAT_ACTIVE)

        Dim DT_SearchGroup As New DataTable
        DT_SearchGroup = DT_GROUP.Copy
        cbbGroup.DisplayMember = "group_name"
        cbbGroup.ValueMember = "id"
        cbbGroup.DataSource = DT_SearchGroup
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim tmp As DataTable = DT.Copy

        '---------------- Validate---------------
        For i As Integer = 0 To tmp.Rows.Count - 1
            If tmp.Rows(i).RowState = DataRowState.Deleted Then Continue For
            If IsDBNull(tmp.Rows(i).Item("SUBGROUP_NAME")) OrElse tmp.Rows(i).Item("SUBGROUP_NAME") = "" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            If IsDBNull(tmp.Rows(i).Item("QT_GROUP_ID")) OrElse tmp.Rows(i).Item("QT_GROUP_ID").ToString = "0" Then
                MessageBox.Show("กรุณากรอกข้อมูลให้ครบ", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
            tmp.DefaultView.RowFilter = "SUBGROUP_NAME='" & tmp.Rows(i).Item("SUBGROUP_NAME").ToString.Replace("'", "''") & "' and QT_GROUP_ID='" & tmp(i)("QT_GROUP_ID") & "'"
            If tmp.DefaultView.Count > 1 Then
                MessageBox.Show("คุณกรอกข้อมูลซ้ำกัน", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Exit Sub
            End If
        Next

        '------------------Batch Save---------------
        Dim conn As New SqlConnection(ConnStr)
        conn.Open()
        If conn.State <> ConnectionState.Open Then
            MsgBox("Connection State is close.", MsgBoxStyle.OkOnly)
            Exit Sub
        End If
        Dim trans As SqlTransaction
        trans = conn.BeginTransaction

        Dim ret As String = "false"
        For i As Integer = 0 To DT.Rows.Count - 1
            Dim sql As String = ""

            Dim p(5) As SqlParameter
            If DT.Rows(i)("id") > 0 Then
                sql = "update qt_group_sub "
                sql += " set subgroup_name=@_SUBGROUP_NAME"
                sql += ", qt_group_id=@_GROUP_ID"
                sql += ", active_status=@_ACTIVE_STATUS "
                sql += ", updated_by=@_UPDATED_BY "
                sql += ", updated_date=getdate()"
                sql += " where id=@_ID"

                p(0) = SetBigInt("@_ID", DT.Rows(i)("id"))
                p(1) = SetText("@_UPDATED_BY", myUser.us_code)
            Else
                sql = "insert into qt_group_sub (created_by, created_date, subgroup_name, qt_group_id, unique_key, active_status)"
                sql += " values(@_CREATED_BY, getdate(), @_SUBGROUP_NAME, @_GROUP_ID, @_UNIQUE_KEY, @_ACTIVE_STATUS) "

                p(0) = SetText("@_CREATED_BY", myUser.us_code)
                p(1) = SetText("@_UNIQUE_KEY", DateTime.Now.ToString("yyyyMMddHHmmssfff"))
            End If

            p(2) = SetText("@_SUBGROUP_NAME", DT.Rows(i)("subgroup_name"))
            p(3) = SetBigInt("@_GROUP_ID", DT.Rows(i)("qt_group_id"))
            p(4) = SetText("@_ACTIVE_STATUS", IIf(DT.Rows(i)("active_status") = "Y", "Y", "N"))

            ret = Execute_Command(sql, trans, p)
            If ret.ToLower <> "true" Then
                Exit For
            End If
        Next

        If ret.ToLower = "true" Then
            trans.Commit()

            txtSearch.Text = ""
            rdiSearchAll.Checked = True
            frmSettingQTSubGroup_Load(Nothing, Nothing)
            MessageBox.Show("Save success", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            trans.Rollback()
            MessageBox.Show("ไม่สามารถบันทึกได้ " & vbNewLine & ret, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim DT As DataTable = grdMain.DataSource
        Dim DR As DataRow = DT.NewRow
        DR("id") = 0
        DR("qt_group_id") = 0
        DR("active_status") = "Y"

        DT.Rows.Add(DR)
        Filter(True)
    End Sub

    Private Sub txtSearch_KeyUp(sender As System.Object, e As System.EventArgs) Handles txtSearch.KeyUp
        Filter()
    End Sub

    Private Sub rdiSearchAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdiSearchAll.CheckedChanged, rdiSearchActive.CheckedChanged, rdiSearchInactive.CheckedChanged
        Filter()
    End Sub

    Private Sub cbbGroup_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cbbGroup.SelectedIndexChanged
        Filter()
    End Sub

    Sub Filter(Optional ByVal Add As Boolean = False)
        If grdMain.DataSource Is Nothing Then Exit Sub
        Dim DT As New DataTable
        DT = grdMain.DataSource
        Dim Filter As String = ""
        If Add = True Then
            Filter &= " ID = 0 OR  "
        End If
        Filter &= "SUBGROUP_NAME LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' AND "
        If cbbGroup.SelectedIndex > 0 Then
            Filter += "QT_GROUP_ID = " & cbbGroup.SelectedValue & " AND "
        End If

        If rdiSearchActive.Checked = True Then
            Filter += " active_status='Y' AND "
        End If
        If rdiSearchInactive.Checked = True Then
            Filter += " active_status='N' and "
        End If
        If Filter <> "" Then
            Filter = Filter.Substring(0, Filter.Length - 4)
        End If
        DT.DefaultView.RowFilter = Filter

        lblRec.Text = "Total   " & DT.DefaultView.Count & "   Record"
    End Sub

    Private Sub btnRefresh_Click(sender As System.Object, e As System.EventArgs) Handles btnRefresh.Click

        frmSettingQTSubGroup_Load(Nothing, Nothing)
    End Sub

End Class